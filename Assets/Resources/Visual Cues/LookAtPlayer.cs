﻿using System.Collections;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    public float Speed { get; } = 5f;

    public float Height { get; } = 0.2f;

    void Start()
    {
        //transform.LookAt(Camera.main.transform.position);
        transform.rotation = Camera.main.transform.rotation;
        StartCoroutine("KillParent");
    }

    IEnumerator KillParent()
    {

        yield return new WaitForSeconds(3);
        Destroy(transform.parent.gameObject);
    }




    void Update()
    {
        Vector3 pos = transform.position;
        float newY = Mathf.Sin(Time.time * Speed) * Height;
        transform.position = new Vector3(pos.x, newY, pos.z);
    }
}
