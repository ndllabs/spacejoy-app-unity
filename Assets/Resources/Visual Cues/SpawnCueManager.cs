﻿/*
Script that finds Selector attached to the Asset and create a spawning animation based on the Asset's size.
*/

using UnityEngine;
using _unity.Events;

public class SpawnCueManager : MonoBehaviour
{
    /*static GameObject spawnanim_clone;

    private void OnEnable() => Messenger<GameObject>.AddListener(StaticFunctionCalls.AssetSpawned, MakeSpawnCue);

    private void OnDisable() => Messenger<GameObject>.RemoveListener(StaticFunctionCalls.AssetSpawned, MakeSpawnCue);

    private void MakeSpawnCue(GameObject g)
    {// Just a function that adds a Spawning Animation to the assets.

        spawnanim_clone = Instantiate(Resources.Load<GameObject>("Visual Cues/SpawnAnim"), g.transform.position, Quaternion.identity);
        Renderer rend = null;
        GameObject ChildSelector;
        foreach (Transform child in g.transform)
            if (child.name == "Selector(Clone)")
            {
                ChildSelector = child.transform.gameObject;
                rend = ChildSelector.transform.GetChild(0).GetComponent<Renderer>();
                //Debug.Log(" Bounds of rend: " + rend.bounds.extents + " position of selector : " + ChildSelector.transform.position);
                break;
            }

        //Debug.Log("0 is : " + spawnanim_clone.transform.GetChild(0));
        //Debug.Log("1 is : " + spawnanim_clone.transform.GetChild(1));
        //Debug.Log("The bounds are : " + rend.bounds.max);
        spawnanim_clone.transform.GetChild(0).position = new Vector3(g.transform.position.x, g.transform.position.y + 0.5f , g.transform.position.z);//new Vector3(g.transform.position.x, 0.7f, g.transform.position.z);
        //Debug.Log("Position of gameobject: " + g.transform.position);
        //spawnanim_clone.transform.GetChild(1).position = new Vector3(g.transform.position.x, g.transform.position.y/3, g.transform.position.z);

        //Debug.Log("Position of  " + spawnanim_clone.transform.GetChild(1)+ " : " + spawnanim_clone.transform.GetChild(1).position);
        spawnanim_clone.transform.GetChild(1).localScale = rend.bounds.size.x > rend.bounds.size.z
            ? new Vector3(rend.bounds.size.x * 1.1f, spawnanim_clone.transform.GetChild(1).localScale.y, rend.bounds.size.x * 1.1f)
            : new Vector3(rend.bounds.size.z * 1.1f, spawnanim_clone.transform.GetChild(1).localScale.y, rend.bounds.size.z * 1.1f);
    }*/
}
