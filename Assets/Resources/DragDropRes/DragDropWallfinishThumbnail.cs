﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using _unity.Events;
using Homefuly;

public class DragDropWallfinishThumbnail : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler//, IPointerUpHandler, IPointerDownHandler, IPointerExitHandler
{
    //public GameObject DpGO;
    private RectTransform gRt;
    //private Transform canvas;
    //public string id;
    //public Color color;
    //public Transform wall;
    //public string assetName;
    public IWallfinish wallfinish;

    private int fingerID = -1;



    private void Awake()
    {
#if !UNITY_EDITOR
             fingerID = 0; 
#endif
    }

    public void Update()
    {
        if (Input.GetMouseButtonUp(0) | (Input.touchCount == 0 && fingerID == 0))
        {
            Destroy(gameObject);
            Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, false);
        }
    }

    public void OnBeginDrag(PointerEventData e)
    {
        gRt = GetComponent<RectTransform>();
        gRt.position = new Vector3(e.position.x, e.position.y, 1);
        if (wallfinish.wftype == "wallpaper")   //Wallpaper requires bigger thumbnail 
            gRt.sizeDelta = new Vector2(300, 220);
        else
            gRt.sizeDelta = new Vector2(200, 200);

        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, true);

        //Debug.Log("We in BeginDrag 1/5");
        //ExecuteEvents.Execute(gDp, e, ExecuteEvents.beginDragHandler);

    }

    public void OnDrag(PointerEventData e)
    {
        //Debug.Log("We on onDrag 3/5 infitine");
        gRt.position = new Vector3(e.position.x, e.position.y, 1);
    }

    public void OnEndDrag(PointerEventData e)
    {
        //Debug.Log("Drag is dead! 4/5");

        RaycastHit hitCenter;
        //gRt.position = new Vector3(e.position.x, e.position.y, 1);
        //Destroy(gameObject);

        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, false);

        //var layerMask = ~(1 << LayerMask.NameToLayer("Asset"));// | LayerMask.GetMask("Wall"));//(1 << LayerMask.NameToLayer("Wall"));
        //var layerMask = ~(1 << LayerMask.NameToLayer("Wallpaper"));
        //var layerMask = ~(1 << LayerMask.NameToLayer("Roof"));

        int layerMask = ~LayerMask.GetMask("Asset", "Default", "Wallpaper");

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //EventSystem.current.IsPointerOverGameObject
        if (!HomefulyExtensions.PointIsOverUI())
        {
            if (Physics.Raycast(ray, out hitCenter, 50f, layerMask))//, layerMask, QueryTriggerInteraction.Ignore))
            {
                Messenger<IWallfinish, GameObject>.Broadcast(StaticFunctionCalls.DropWallfinishInScene, wallfinish, hitCenter.collider.gameObject);
                // Messenger<string[], Vector3, GameObject>.Broadcast("OnAssetDroppedInsideRoom", new string[] { id, assetName }, hitCenter.point, hitCenter.collider.gameObject);
                //if (hitCenter.collider.gameObject.tag == "Wall")
                //{
                //    wall = hitCenter.collider.gameObject.transform;
                //    wall.transform.DestroyChildren();
                //    Material[] mat = wall.transform.GetComponent<MeshRenderer>().materials;
                //    foreach (Material material in mat)
                //    {
                //        material.color = color;
                //    }
                //    GameObject paint = new GameObject("paint");
                //    paint.transform.SetParent(wall);
                //    GameObject paintID = new GameObject(id);
                //    paintID.transform.SetParent(paint.transform);
                //}
            }
            else
                Debug.LogWarning("Sorry Asset Drop Point Is Not Inside the room. Ignoring Spawn Call");

        }
        else
            Debug.LogWarning("Sorry Asset Drop Point Is over the UI. Ignoring Spawn Call");


        //Debug.Log("destroyed 5/5");
        Destroy(gameObject);
    }
}
