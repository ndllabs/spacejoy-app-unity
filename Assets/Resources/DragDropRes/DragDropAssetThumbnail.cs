﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using _unity.Events;

public class DragDropAssetThumbnail : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler//, IPointerUpHandler, IPointerDownHandler, IPointerExitHandler
{
    //public GameObject DpGO;
    private RectTransform gRt;
    private Transform canvas;
    //public string id;
    public IAsset asset;
    //public string assetName;

    private int fingerID = -1;



    private void Awake()
    {
#if !UNITY_EDITOR
             fingerID = 0; 
#endif
    }

    public void Update()
    {
        //TRY DESTROYING ON 0 touch
        /*if (Input.touchCount >= 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Ended)
            {
                // Restore the regular size of the cube.
                transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }
        }*/
        if (Input.GetMouseButtonUp(0) | (Input.touchCount == 0 && fingerID == 0))
        {
            Destroy(gameObject);
            Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, false);
        }/*
        if ()
        {
            Destroy(gameObject);
            Messenger<bool>.Broadcast(StaticFunctionCalls.OnDragAssetFromStore, false);

        }*/
    }

    public void OnBeginDrag(PointerEventData e)
    {
        gRt = GetComponent<RectTransform>();
        gRt.position = new Vector3(e.position.x, e.position.y, 1);
        gRt.sizeDelta = new Vector2(220, 220);
        
        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, true);

        //Debug.Log("We in BeginDrag 1/5");
        //ExecuteEvents.Execute(gDp, e, ExecuteEvents.beginDragHandler);

    }

    public void OnDrag(PointerEventData e)
    {
        //Debug.Log("We on onDrag 3/5 infitine");
        gRt.position = new Vector3(e.position.x, e.position.y, 1);
    }

    public void OnEndDrag(PointerEventData e)
    {
        //Debug.Log("Drag is dead! 4/5");

        RaycastHit hitCenter;
        //gRt.position = new Vector3(e.position.x, e.position.y, 1);
        //Destroy(gameObject);

        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, false);

        var layerMask = ~(1 << LayerMask.NameToLayer("Water"));

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //EventSystem.current.IsPointerOverGameObject
        if(!EventSystem.current.IsPointerOverGameObject(fingerID))
        {    
            if (Physics.Raycast(ray, out hitCenter, 50f, layerMask, QueryTriggerInteraction.Collide))
            {
                Messenger<IAsset, Vector3, GameObject>.Broadcast(AppEvents.ASSET_DROPPED_INSIDE_ROOM, asset, (hitCenter.point + (hitCenter.normal * 0.01f)), hitCenter.collider.gameObject);
            }
            else
                Debug.LogWarning("Sorry Asset Drop Point Is Not Inside the room. Ignoring Spawn Call");

        }
        else
            Debug.LogWarning("Sorry Asset Drop Point Is over the UI. Ignoring Spawn Call");


        //Debug.Log("destroyed 5/5");
        Destroy(gameObject);
    }
    //public void OnPointerUp(PointerEventData eventData)
    //{
    //    Debug.Log("Pointer up");
    //    //Debug.Log("O5");
    //    Destroy(gameObject);
    //}

    //public void OnPointerDown(PointerEventData eventData)
    //{
    //    //throw new System.NotImplementedException();
    //}
    //public void OnPointerExit(PointerEventData eventData)
    //{
    //    Debug.Log("The cursor exited the selectable UI element.");
    //}
}
