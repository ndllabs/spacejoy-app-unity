﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using _unity.Events;
using UnityEngine.UI;
using System;

public class UIController_ToolTipManager : MonoBehaviour
{

    public GameObject ToolTip;
    public GameObject CanvasToolTip;

    public GameObject swapButton;

    public UIController_PanelPaint panelPaint;
    public UIController_PanelAssetStore panelAssetStore;
    public UIController_PanelEstimate panelEstimate;

    public GameObject MoreButton,SaveButton,ViewButton;

    Animator animator;
    RectTransform rect;


    GameObject g;
    ToolTipClass ttc;

    bool ExpandButtonCalled;
    bool SaveButtonCalled;
    bool ViewButtonCalled;

    private void OnEnable()
    {
        StartCoroutine(waitforten());
        Messenger<ToolTipClass, GameObject>.AddListener(ChallengeUICalls.ShowToolTip, ShowTip);
        Messenger.AddListener(ChallengeUICalls.DestroyToolTip, DestroyTipExternal);

        Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.Action_AssetSwapped, ShowAssetSwapTip);

        Messenger.AddListener(ChallengeUICalls.ExpandButtonClicked, ToggleExpand);
        Messenger.AddListener(ChallengeUICalls.SaveButtonClicked, ToggleSave);
        Messenger.AddListener(ChallengeUICalls.ViewButtonClicked, ToggleView);

    }

    private void OnDisable()
    {
        Messenger<ToolTipClass, GameObject>.RemoveListener(ChallengeUICalls.ShowToolTip, ShowTip);
        Messenger.RemoveListener(ChallengeUICalls.DestroyToolTip, DestroyTipExternal);

        //Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.Action_AssetSwapped, ShowAssetSwapTip);

        Messenger.RemoveListener(ChallengeUICalls.ExpandButtonClicked, ToggleExpand);
        Messenger.RemoveListener(ChallengeUICalls.SaveButtonClicked, ToggleSave);
        Messenger.RemoveListener(ChallengeUICalls.ViewButtonClicked, ToggleView);

    }

    private void Update()
    {
       
    }

    #region Toggles
    void ToggleExpand()
    {

        ExpandButtonCalled = true;
    }

    void ToggleSave()
    {
       
        SaveButtonCalled = true;
    }

    void ToggleView()
    {

        ViewButtonCalled = true;
    }
    #endregion

    /// <summary>
    /// Shows the tool tip.
    /// It takes a message, the gameobject to the left of which the tooltip needs to be shown
    /// and for how long, default is 10.
    /// </summary>

    public void ShowTip(ToolTipClass tipClass, GameObject position)
    {
        if (panelPaint.gameObject.activeSelf || panelEstimate.gameObject.activeSelf || panelAssetStore.gameObject.activeSelf)
            return;
       
       
        Debug.Log(">>>>>>>>> Showing Tooltip");
        RectTransform tempRect = position.GetComponent<RectTransform>();
        Vector3[] corners = new Vector3[4];
        int screenWidth, screenHeight;
        screenWidth = Screen.width;
        screenHeight = Screen.height;

        tempRect.GetWorldCorners(corners);

        g = Instantiate(ToolTip, CanvasToolTip.transform);
        g = g.transform.GetChild(0).gameObject;

        g.transform.Find("imgBackground").transform.Find("txtMessage").GetComponent<TextMeshProUGUI>().text = tipClass.stringMessage;
        animator = g.GetComponent<Animator>();
        animator.Play("Fadein");
        rect = g.transform.parent.gameObject.GetComponent<RectTransform>();
        if (tipClass.toolTip == ToolTipType.LeftSide)
        {
            rect.anchorMin = new Vector2((corners[0].x - 320) / Screen.width, (corners[0].y) / Screen.height);
            rect.anchorMax = new Vector2((corners[0].x - 10) / Screen.width, (corners[1].y) / Screen.height);
        }
        else
        {
            rect.anchorMin = new Vector2((corners[2].x + 10) / Screen.width, (corners[0].y) / Screen.height);
            rect.anchorMax = new Vector2((corners[2].x + 320) / Screen.width, (corners[1].y) / Screen.height);
        }
        rect.offsetMax = new Vector2(0, 0);
        rect.offsetMin = new Vector2(0, 0);
        rect.localScale = new Vector3(1, 1, 1);

        Button b = g.AddComponent<Button>();
        b.onClick.RemoveAllListeners();
        b.onClick.AddListener(() =>
        {
            StartCoroutine(DestroyTheTip(1));
        });

        if (tipClass.toolTipTime < 0)
            return;

            DestroyTip(tipClass.toolTipTime);
    }

    public void DestroyTip(float time)
    {
        StartCoroutine(DestroyTheTip(time));
    }

    public void DestroyTipExternal()
    {
        StartCoroutine(DestroyTheTip(1));
    }

    IEnumerator DestroyTheTip(float time)
    {
     
        yield return new WaitForSeconds(time-1);
        if(animator!=null)
        animator.Play("Fadeout");
        yield return new WaitForSeconds(1);
        if (animator.gameObject.activeSelf)
            Destroy(g.transform.parent.gameObject);
        yield return null;
    }


    private void ShowAssetSwapTip(IAssetEntry hotspot)
    {
        Debug.Log("Show asset swap tool tip");
        ToolTipClass assetSwapTT = new ToolTipClass
        {
            stringMessage = "Select the asset again to do another swap",
            toolTip = ToolTipType.RightSide,
            toolTipTime = 10f
        };
        ShowTip(assetSwapTT, swapButton);
        Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.Action_AssetSwapped, ShowAssetSwapTip);
    }


    IEnumerator waitforten()
    {
        yield return new WaitForSeconds(10);
        SetupToolTipClass("This is the Menu Button, Tap here", ToolTipType.LeftSide, 6f);
        if(!ExpandButtonCalled)
        ShowTip(ttc, MoreButton);
        yield return new WaitForSeconds(10);
        SetupToolTipClass("Tap here to save your Design", ToolTipType.RightSide, 6f);
        if (!SaveButtonCalled)
            ShowTip(ttc, SaveButton);
        yield return new WaitForSeconds(10);
        SetupToolTipClass("Tap here to change the camera view", ToolTipType.LeftSide, 6f);
        if (!ViewButtonCalled)
            ShowTip(ttc, ViewButton);
    }

    private void SetupToolTipClass(string v1, ToolTipType toolTipType, float v2)
    {
        ttc = new ToolTipClass
        {
            stringMessage = v1,
            toolTip = toolTipType,
            toolTipTime = v2
        };
    }
}



public class ToolTipClass
{
    public string stringMessage;
    public ToolTipType toolTip;
    public float toolTipTime;
}

public enum ToolTipType
{
    LeftSide,
    RightSide,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight
}