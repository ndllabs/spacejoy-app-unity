﻿using _unity.Events;
using Homefuly;
using Homefuly.Analytics;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class UIController_ChallengeCompleted : MonoBehaviour
{
    public TextMeshProUGUI txtReward;
    public Button btnHome, btnNextChallenge, btnCollectRewards;
    public GameObject pnlFinalCheckAnimation;
    public GameObject pnlChallengeCompleted;
    public EasyFeedback.FeedbackForm feedbackForm;
    Homefuly.AppComponent app;
    bool isUserFirstTimeLoggedIn = false;

    private void OnEnable()
    {
        app = Homefuly.AppComponent.Instance;
        feedbackForm = GameObject.Find("Feedback").GetComponent<EasyFeedback.FeedbackForm>();
    }

    public void ShowCollectRewards(int coins)
    {
        HomefulyExtensions.SetScreenOrientation(true);

        pnlChallengeCompleted.SetActive(true);
        pnlFinalCheckAnimation.SetActive(false);

        txtReward.text = coins.ToString();
        btnNextChallenge.gameObject.SetActive(false);
        btnHome.gameObject.SetActive(false);

        btnCollectRewards.gameObject.SetActive(true);
        btnCollectRewards.onClick.RemoveAllListeners();
        btnCollectRewards.onClick.AddListener(() =>
        {
            HomefulyExtensions.SetScreenOrientation(false);
            WidgetManager.Instance.AuthPanelOpen((bool success) =>
            {
                if (success)
                {
                    isUserFirstTimeLoggedIn = true;
                    Messenger.Broadcast(StaticFunctionCalls.Request_DesignSubmit);
                }
                else
                {
                    HomefulyExtensions.SetScreenOrientation(true);
                    if (gameObject)
                        gameObject.SetActive(false);
                }
            }, false, true);
            HomefulyAnalyticsEvent.OnChallengeComplete?.Invoke(ChallengeComplete_Behaviour.CollectRewards_Clicked);
        });
    }

    public async void ShowChallengeComplete(int coins)
    {
        if (isUserFirstTimeLoggedIn)
        {
            HomefulyExtensions.SetScreenOrientation(false);

            var ap = AppComponent.Instance;

            if (pnlChallengeCompleted.activeSelf)
                pnlChallengeCompleted.SetActive(false);

            if (!pnlFinalCheckAnimation.activeSelf)
                pnlFinalCheckAnimation.SetActive(true);


            HomefulyAnalyticsEvent.OnChallengeComplete?.Invoke(ChallengeComplete_Behaviour.CheckAnim_Shown);

            /*
             * Ones Animation is completed head back to next home screen automatically
             * For now a temporary delay
             * */
            await Task.Delay(3000);
            Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, app.db.sceneType);
            AppConfig.GameConfig.nextChallenge = false;
            Debug.Log("Here");

            PlayerPrefs.SetString("minimizeUnity", " ");
            app.Navigate(AppPage.AppComponent);
            // app.Navigate(AppPage.Home);
        }
        else
        {
            HomefulyExtensions.SetScreenOrientation(true);


            txtReward.text = coins.ToString();

            pnlChallengeCompleted.SetActive(true);
            pnlFinalCheckAnimation.SetActive(false);

            btnNextChallenge.gameObject.SetActive(true);
            btnHome.gameObject.SetActive(true);
            btnCollectRewards.gameObject.SetActive(false);

            btnHome.onClick.RemoveAllListeners();
            btnNextChallenge.onClick.RemoveAllListeners();

            btnHome.onClick.AddListener(() =>
            {
                HomefulyAnalyticsEvent.OnChallengeComplete?.Invoke(ChallengeComplete_Behaviour.Home_Clicked);
                Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, Homefuly.AppComponent.Instance.db.sceneType);
                HomefulyExtensions.AskAppFeedback(() =>
                {
                    app.Navigate(AppPage.Home);
                });
                //app.Navigate(AppPage.Home);
            });

            btnNextChallenge.onClick.AddListener(() =>
            {
                Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, app.db.sceneType);
                HomefulyAnalyticsEvent.OnChallengeComplete?.Invoke(ChallengeComplete_Behaviour.NextChallenge_Clicked);

                AppConfig.GameConfig.nextChallenge = true;
                
                PlayerPrefs.SetString("minimizeUnity", " ");
                app.Navigate(AppPage.AppComponent);
                // app.Navigate(AppPage.Home);

                // WidgetManager.Instance.LoaderMini("Taking You To The Next Challenge...");

                // // update db for all challenges data
                // if(!app.db.isChallengesSync) await app.db.GetAllDesignChallenges();

                // if(app.db.isChallengesSync)
                // {
                //     app.db.designChallengeList.challenges_Indexed[app.account.CURRENT_CHALLENGE_ID].completed = true;
                //     foreach(KeyValuePair<string, IDesignChallengeObj> entry in app.db.designChallengeList.challenges_Indexed)
                //     {
                //         if(!entry.Value.completed)
                //         {
                //             app.account.CURRENT_CHALLENGE_ID = entry.Key;
                //             await Task.Delay(1000); // redundant delay
                //             WidgetManager.Instance.LoaderMiniClose();
                //             app.Navigate(AppPage.DesignChallenge);
                //             break;
                //         }
                //     }
                // }
                // else
                // {
                //     app.Navigate(AppPage.Home);
                // }
            });
        }
    }


    //    public void Init(string userType, string reward)
    //    {
    //        app = Homefuly.AppComponent.Instance;
    //        txtReward.text = reward;
    //        if (userType == string.Empty)
    //        {
    //            btnNextChallenge.gameObject.SetActive(false);
    //            btnHome.gameObject.SetActive(false);

    //            btnCollectRewards.gameObject.SetActive(true);
    //            btnCollectRewards.onClick.RemoveAllListeners();
    //            btnCollectRewards.onClick.AddListener(() =>
    //            {
    //                Debug.Log("AUth panel opens");
    //                HomefulyExtensions.SetScreenOrientationToLandscape(false);
    //                WidgetManager.Instance.AuthPanelOpen((bool success) =>
    //                {
    //                    if (success)
    //                    {

    //                        Messenger.Broadcast(StaticFunctionCalls.Request_DesignSubmit);
    //                    }
    //                });
    //            });
    //        }
    //        else
    //        {
    //            btnNextChallenge.gameObject.SetActive(true);
    //            btnHome.gameObject.SetActive(true);
    //            btnCollectRewards.gameObject.SetActive(false);

    //            btnHome.onClick.RemoveAllListeners();
    //            btnNextChallenge.onClick.RemoveAllListeners();

    //            btnHome.onClick.AddListener(() =>
    //            {
    //                Messenger<SceneType>.Broadcast(AnalyticsCalls.TimeSpentInDesign, app.db.sceneType);
    //                app.Navigate(AppPage.Home);
    //            });
    //            btnNextChallenge.onClick.AddListener(() =>
    //            {
    //                Messenger<SceneType>.Broadcast(AnalyticsCalls.TimeSpentInDesign, app.db.sceneType);
    //                app.Navigate(AppPage.Home);
    //            });
    //        }
    //    }
}
