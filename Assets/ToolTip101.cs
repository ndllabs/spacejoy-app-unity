﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using _unity.Events;
using UIC = ChallengeUICalls;
using Homefuly.Database;
using Homefuly;
using Homefuly.Events;
using System.Linq;

public class ToolTip101 : MonoBehaviour
{
    private bool isPanelOpen;
    private ToolTipTypeClass currentToolTip;

    public List<ToolTipTypeClass> toolTips;
    public Dictionary<string, bool> toolTipDict;
    private int currentTooltipIndex;
    private int nextTooltipIndex;

    public List<ToolTipTypeClass> AssetStoreToolTips;
    private Dictionary<string, bool> assetStoreTipDict;
    private int assetCategoryTipIndex;

    public List<ToolTipTypeClass> AssetTips;
    private Dictionary<string, bool> assetTipDict;
    private int assetTipIndex;

    public GameObject AllCategoryParent;
    public GameObject CategoryParent;

    public GameObject panelMenu;
    public GameObject WorldButtons;
    public GameObject AllCatObject;
    public GameObject CatObject;

    #region Lans' variables
    public UIController_PanelPaint panelPaint;
    public UIController_PanelAssetStore panelAssetStore;
    public UIController_PanelEstimate panelEstimate;

    public GameObject ToolTip;
    public GameObject CanvasToolTip;
    Animator animator;
    RectTransform rect;


    GameObject g;
    ToolTipClass ttc;

    bool ExpandButtonCalled;
    bool SaveButtonCalled;
    bool ViewButtonCalled;
    #endregion
    private bool isToolTipPaused = false;

    public void Start()
    {
       
        //public static string HotspotTapped = "HotspotTapped";
        currentTooltipIndex = 0;
        assetCategoryTipIndex = 0;
        nextTooltipIndex = 1;
        //AppDatabase.OnDBUpdate += Init;
        HomefulyEvents.OnSceneReady += Init;
        //StartCoroutine(ShowToolTip(toolTips[currentTooltipIndex]));
        Messenger.AddListener(UIC.ExpandButtonClicked, EmptyFunc);
        Messenger.AddListener(UIC.ViewButtonClicked, EmptyFunc);
        Messenger.AddListener(AppEvents.ASSET_DROP_COMPLETE, OnActionAssetDrop);
        toolTipDict = new Dictionary<string, bool>();
        assetStoreTipDict = new Dictionary<string, bool>();
        assetTipDict = new Dictionary<string, bool>();
        
        //Messenger.AddListener(UIC.ViewButtonClicked, OnViewTapped);

        foreach (ToolTipTypeClass tip in toolTips)
        {
            toolTipDict.Add(tip.ButtonType.ToString(), false);
        }

        foreach (ToolTipTypeClass tip in AssetStoreToolTips)
        {
            assetStoreTipDict.Add(tip.ButtonType.ToString(), false);
        }

        foreach(ToolTipTypeClass tip in AssetTips)
        {
            assetTipDict.Add(tip.ButtonType.ToString(), false);
        }
        Debug.Log(toolTipDict.ToString());
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        CancelInvoke();
        HomefulyEvents.OnSceneReady -= Init;
        HomefulyEvents.OnExpandButtonPressed -= OnExpandTapped;
        HomefulyEvents.OnViewButtonPressed -= OnViewTapped;
        HomefulyEvents.OnAddProductPressed -= OnAddProductTapped;
        HomefulyEvents.OnMinimise -= ResetTipCycle;
        Messenger.RemoveListener(AppEvents.ASSET_DROP_COMPLETE, OnActionAssetDrop);
    }

    private void EmptyFunc()
    {

    }

    public void ResetAllTips()
    {
        DestroyTipImmediately();
        CancelInvoke();
        currentTooltipIndex = 0;
        assetCategoryTipIndex = 0;
        assetTipIndex = 0;
        foreach (var key in toolTipDict.Keys.ToList())
        {
            toolTipDict[key] = false;
        }
        foreach (var key in assetStoreTipDict.Keys.ToList())
        {
            assetStoreTipDict[key] = false;
        }
        foreach (var key in assetTipDict.Keys.ToList())
        {
            assetTipDict[key] = false;
        }
        OnValueUpdate();
        ShowToolTip();
    }

    private void Init(string sceneName, SceneType sceneType)
    {
        isToolTipPaused = false;
        if (AppComponent.Instance.db.sceneType == SceneType.ReadOnlyDesign)
        {
            Debug.Log("Read only scene, not setting tooltips");
            return;
        }
        //ResetAllTips();
        Debug.Log("Tool tip, app updated");
        //if (updateType == Db.Whole)
        if (PlayerPrefs.HasKey("WorldButtons"))
        {
            Debug.Log("Player prefs exist");
            toolTipDict = SerializeToolTipDict(PlayerPrefs.GetString("WorldButtons"));
        }
        if (PlayerPrefs.HasKey("AssetCategoryTips"))
            assetStoreTipDict = SerializeToolTipDict(PlayerPrefs.GetString("AssetCategoryTips"));
        if (PlayerPrefs.HasKey("AssetTips"))
            assetTipDict = SerializeToolTipDict(PlayerPrefs.GetString("AssetTips"));
        Invoke("ShowToolTip", 5.0f);

        //if (!toolTipDict[ToolType.ExpandButtonClicked.ToString()])
            HomefulyEvents.OnExpandButtonPressed += OnExpandTapped;
        
        //if (!toolTipDict[ToolType.ViewButtonClicked.ToString()])
            HomefulyEvents.OnViewButtonPressed += OnViewTapped;

        //if (!toolTipDict[ToolType.AssetStoreTapped.ToString()])
            HomefulyEvents.OnAddProductPressed += OnAddProductTapped;

        HomefulyEvents.OnMinimise += ResetTipCycle;
        
    }

    private void OnValueUpdate()
    {
        Debug.Log("player prefs updated");
        PlayerPrefs.SetString("WorldButtons", DeSerializeToolTipDict(toolTipDict));
        PlayerPrefs.SetString("AssetCategoryTips", DeSerializeToolTipDict(assetStoreTipDict));
        PlayerPrefs.SetString("AssetTips", DeSerializeToolTipDict(assetTipDict));
    }

    private string DeSerializeToolTipDict(Dictionary<string, bool> dictionary)
    {
        string result = "";
        if (dictionary != null)
        {
            result = string.Join(";", dictionary.Select(x => x.Key + "=" + x.Value));
        }
        Debug.Log("Deserialized " + result);
        return result;
    }

    private Dictionary<string, bool> SerializeToolTipDict(string dictString)
    {
        Dictionary<string, bool> result = new Dictionary<string, bool>();
        if (!string.IsNullOrEmpty(dictString))
        {
            result = dictString.Split(';').Select(s => s.Split('=')).ToDictionary(a => a[0].Trim(), a => a[1].Trim() == "True");
        }
        return result;
    }

    //private void Init(Db updateType)
    //{
    //    Debug.Log("Tool tip, app updated");
    //    if (updateType == Db.Whole)
    //        Invoke("ShowToolTip", 10.0f);
    //}

    ////void Init()
    //{
    //    Invoke("ShowToolTip", 10.0f);
    //}

    void ShowToolTip()
    {
        // display the tooltip in the current index
        // if asset store is active, show it's tooltips
        // if paint store is active, show it's tooltips
        // else if it's main panel, show the main panel tooltips

        // check if the tooltip in current index is completed
        CancelInvoke();
        StopAllCoroutines();
        if (isToolTipPaused)
        {
            Invoke("ShowToolTip", 5.0f);
        }
        else if (panelMenu.gameObject.activeSelf)
        {
            if (panelAssetStore.gameObject.activeSelf)
            {
                // if all category is active self
                if (AllCatObject.activeSelf && assetCategoryTipIndex < AssetStoreToolTips.Count)
                {
                    Debug.Log("Asset tip index " + assetCategoryTipIndex);
                    Debug.Log(AssetStoreToolTips[assetCategoryTipIndex].ButtonType.ToString());
                    while (assetCategoryTipIndex < AssetStoreToolTips.Count && assetStoreTipDict[AssetStoreToolTips[assetCategoryTipIndex].ButtonType.ToString()] == true)
                    {
                        ++assetCategoryTipIndex;
                    }
                    if (assetCategoryTipIndex < AssetStoreToolTips.Count)
                    {
                        ToolTipTypeClass currentToolTip = AssetStoreToolTips[assetCategoryTipIndex];
                        ShowTip(currentToolTip, currentToolTip.uiObject);
                    }
                }
                else if (CatObject.activeSelf && assetTipIndex < AssetTips.Count)
                {
                    while (assetTipIndex < AssetTips.Count && assetTipDict[AssetTips[assetTipIndex].ButtonType.ToString()] == true)
                    {
                        ++assetTipIndex;
                    }
                    if (assetTipIndex < AssetTips.Count)
                    {
                        ToolTipTypeClass currentToolTip = AssetTips[assetTipIndex];
                        ShowTip(currentToolTip, currentToolTip.uiObject);
                    }
                }
                else
                {
                    Invoke("ShowToolTip", 5.0f);
                }
            }
        }
        // if categoryview is active self
        else if (WorldButtons.gameObject.activeSelf)
        {
            while (currentTooltipIndex < toolTips.Count && toolTipDict[toolTips[currentTooltipIndex].ButtonType.ToString()] == true)
            {
                ++currentTooltipIndex;
            }
            Debug.Log("Current tooltip index " + currentTooltipIndex);
            if (currentTooltipIndex < toolTips.Count)
            {
                ToolTipTypeClass currentToolTip = toolTips[currentTooltipIndex];
                //yield return new WaitForSeconds(currentToolTip.toolTipTime);
                //Destroy current animation and then trigger the new tooltip
                ShowTip(currentToolTip, currentToolTip.uiObject);
            }
            else // if all the tooltips have been shown
                Debug.Log("Completed all tool tips");
            return;
        }
        else
        {
            Invoke("ShowToolTip", 5.0f);
        }
    }

    void DestroyToolTip()
    {

    }

    public void ResetTipCycle()
    {
        DestroyTipImmediately();
        CancelInvoke();
        Invoke("ShowToolTip", 1.0f);
    }

    void OnExpandTapped()
    {
        DestroyTipImmediately();
        CancelInvoke();
        if (!toolTipDict[ToolType.ExpandButtonClicked.ToString()])
        {
            string buttonName = ToolType.ExpandButtonClicked.ToString();
            SetToolTipViewed(buttonName);
            //HomefulyEvents.OnExpandButtonPressed -= OnExpandTapped;
        }
        Invoke("ShowToolTip", 5.0f);
        //nextTooltipIndex = currentTooltipIndex;
        //CancelInvoke("ShowToolTip");
        ////Messenger.RemoveListener(UIC.ExpandButtonClicked, OnExpandTapped);
        //++currentTooltipIndex;
        //Invoke("ShowToolTip", 5.0f);
        //int thisIndex = GetToolTipIndex(UIC.HotspotTapped);
        //var results = toolTips.Find(input => input.TooltipTracker == UIC.HotspotTapped);
        //if (results != null && results.IsComplete != true)
        //    results.IsComplete = true;

        //ShowToolTip();
        //if (thisIndex != -1)
        //    toolTips[thisIndex].IsComplete = true;

        //ShowTip(toolTips[thisIndex], toolTips[thisIndex].uiObject);
    }

    void OnViewTapped()
    {
        DestroyTipImmediately();
        CancelInvoke();
        if (!toolTipDict[ToolType.ViewButtonClicked.ToString()])
        {
            string buttonName = ToolType.ViewButtonClicked.ToString();
            SetToolTipViewed(buttonName);
        }
        //HomefulyEvents.OnViewButtonPressed -= OnViewTapped;
        Debug.Log("View tapped");
        ShowToolTip();
    }

    void OnAddProductTapped()
    {
        Debug.Log("On asset store tapped");
        DestroyTipImmediately();
        CancelInvoke();
        if (!toolTipDict[ToolType.AssetStoreTapped.ToString()])
        {
            string buttonName = ToolType.AssetStoreTapped.ToString();
            SetToolTipViewed(buttonName);
        }
        StartCoroutine("AllCatTip");
        //else
        //    ShowToolTip();
        //HomefulyEvents.OnAddProductPressed -= OnAddProductTapped;
    }

    IEnumerator AllCatTip()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject firstCategory = AllCategoryParent.transform.GetChild(1).gameObject;
        AssetStoreToolTips[0].uiObject = firstCategory;
        firstCategory.GetComponent<Button>().onClick.AddListener(OnAllCategoryClicked);
        ShowToolTip();
    }

    public void OnAllCategoryClicked()
    {
        DestroyTipImmediately();
        CancelInvoke();
        Debug.Log("A category clicked");
        if (!assetStoreTipDict[ToolType.CategoryClicked.ToString()])
        {
            string buttonName = ToolType.CategoryClicked.ToString();
            assetStoreTipDict[buttonName] = true;
            //StartCoroutine("AssetInCat");
        }
        //else
        ShowToolTip();
        //AssetStoreToolTips[0].uiObject.GetComponent<Button>().onClick.RemoveAllListeners();
    }

    IEnumerator AssetInCat()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject firstCategory = CategoryParent.transform.GetChild(1).gameObject;
        AssetTips[0].uiObject = firstCategory;
        //firstCategory.GetComponent<Button>().onClick.AddListener(OnAssetClicked);
        ShowToolTip();
    }

    //public void OnAssetClicked()
    //{
    //    Debug.Log("An asset clicked");
    //    string buttonName = ToolType.AssetSelected.ToString();
    //    if (assetTipDict.ContainsKey(buttonName))
    //        assetTipDict[buttonName] = true;
    //    OnValueUpdate();
    //    DestroyTipImmediately();
    //    CancelInvoke();
    //    AssetTips[0].uiObject.GetComponent<Button>().onClick.RemoveAllListeners();
    //    ShowToolTip();
    //}

    void OnActionAssetDrop()
    {
        Debug.Log("Asset dropped, ui destroying");
        DestroyTipImmediately();
        CancelInvoke();
        if (!assetTipDict[ToolType.AssetSelected.ToString()])
        {
            string buttonName = ToolType.AssetSelected.ToString();
            assetTipDict[buttonName] = true;
        }
        //AssetTips[0].uiObject.GetComponent<Button>().onClick.RemoveAllListeners();
        Invoke("ShowToolTip", 1.0f);
    }

    void SetToolTipViewed(string tipName)
    {
        if (!toolTipDict.ContainsKey(tipName))
            return;
        CancelInvoke();
        toolTipDict[tipName] = true;
        OnValueUpdate();
    }

    int GetToolTipIndex(string tracker)
    {
        for (int i = 0; i < toolTips.Count; ++i)
        {
            ToolTipTypeClass eachToolTip = toolTips[i];
            if (!eachToolTip.IsComplete && eachToolTip.TooltipTracker == UIC.HotspotTapped)
            {
                if (currentTooltipIndex != i)
                {
                    nextTooltipIndex = currentTooltipIndex;
                    currentTooltipIndex = i;
                    return currentTooltipIndex;
                }
            }
        }
        return -1;
    }

    public void ShowTip(ToolTipTypeClass tipClass, GameObject position)
    {
        //if (panelPaint.gameObject.activeSelf || panelEstimate.gameObject.activeSelf || panelAssetStore.gameObject.activeSelf)
        //    return;


        //  Debug.Log(">>>>>>>>> Showing Tooltip");
        RectTransform tempRect = position.GetComponent<RectTransform>();
        Vector3[] corners = new Vector3[4];
        int screenWidth, screenHeight;
        screenWidth = Screen.width;
        screenHeight = Screen.height;

        tempRect.GetWorldCorners(corners);

        g = Instantiate(ToolTip, CanvasToolTip.transform);
        g = g.transform.GetChild(0).gameObject;

        g.transform.Find("imgBackground").transform.Find("txtMessage").GetComponent<TextMeshProUGUI>().text = tipClass.stringMessage;
        animator = g.GetComponent<Animator>();
        animator.Play("Fadein");
        rect = g.transform.parent.gameObject.GetComponent<RectTransform>();
        if (tipClass.toolTip == ToolTipType.LeftSide)
        {
            rect.anchorMin = new Vector2((corners[0].x - 320) / Screen.width, (corners[0].y) / Screen.height);
            rect.anchorMax = new Vector2((corners[0].x - 10) / Screen.width, (corners[1].y) / Screen.height);
        }
        else if (tipClass.toolTip == ToolTipType.TopLeft)
        {
            rect.anchorMin = new Vector2((corners[2].x - 320) / Screen.width, (corners[1].y + 30) / Screen.height);
            rect.anchorMax = new Vector2((corners[2].x - 10) / Screen.width, (corners[1].y + 80) / Screen.height);
        }
        else if (tipClass.toolTip == ToolTipType.TopRight)
        {
            rect.anchorMin = new Vector2((corners[1].x + 10) / Screen.width, (corners[1].y + 30) / Screen.height);
            rect.anchorMax = new Vector2((corners[1].x + 320) / Screen.width, (corners[1].y + 80) / Screen.height);
        }
        else if (tipClass.toolTip == ToolTipType.BottomRight)
        {
            rect.anchorMin = new Vector2((corners[0].x + 10) / Screen.width, (corners[0].y - 80) / Screen.height);
            rect.anchorMax = new Vector2((corners[0].x + 320) / Screen.width, (corners[0].y - 30) / Screen.height);
        }
        else if (tipClass.toolTip == ToolTipType.BottomLeft)
        {
            rect.anchorMin = new Vector2((corners[2].x - 320) / Screen.width, (corners[0].y - 80) / Screen.height);
            rect.anchorMax = new Vector2((corners[2].x - 10) / Screen.width, (corners[0].y - 30) / Screen.height);
        }
        else
        {
            rect.anchorMin = new Vector2((corners[2].x + 10) / Screen.width, (corners[0].y) / Screen.height);
            rect.anchorMax = new Vector2((corners[2].x + 320) / Screen.width, (corners[1].y) / Screen.height);
        }
        rect.offsetMax = new Vector2(0, 0);
        rect.offsetMin = new Vector2(0, 0);
        rect.localScale = new Vector3(1, 1, 1);

        Button b = g.AddComponent<Button>();
        b.onClick.RemoveAllListeners();
        b.onClick.AddListener(() =>
        {
            //StartCoroutine(DestroyTheTip(1));
            DestroyTipImmediately();
        });

        if (tipClass.toolTipTime < 0)
            return;
        //toolTips[currentTooltipIndex].IsComplete = true;
        DestroyTip(tipClass.toolTipTime);
    }

    public void DestroyTip(float time)
    {
        StartCoroutine(DestroyTheTip(time));
    }

    public void DestroyTipExternal()
    {
        StartCoroutine(DestroyTheTip(1));
    }

    IEnumerator DestroyTheTip(float time)
    {
        //StopAllCoroutines();
        Invoke("ShowToolTip", 5.0f+time);
        yield return new WaitForSeconds(time - 1);
        if (animator)
        {
            if (animator.gameObject.activeSelf)
                animator.Play("Fadeout");
            yield return new WaitForSeconds(1);
            if (animator.gameObject.activeSelf)
                Destroy(g.transform.parent.gameObject);
        }
        yield return null;
    }

    void DestroyTipImmediately()
    {
        StopAllCoroutines();
        if (animator != null)
        {
            if (animator.gameObject.activeSelf)
                animator.Play("Fadeout");
            if (animator.gameObject.activeSelf)
                Destroy(g.transform.parent.gameObject);
        }
    }

    public void UpdateToolTipPause(bool toolTipPauseStatus)
    {
        DestroyTipImmediately();
        CancelInvoke();
        isToolTipPaused = toolTipPauseStatus;
        Invoke("ShowToolTip", 1.0f);
    }
}

[Serializable]
public class ToolTipTypeClass
{
    public GameObject uiObject;
    public float toolTipTime;
    public string stringMessage;
    //public List<ToolTipTypeClass> subToolTips;
    public ToolTipType toolTip;
    public string TooltipTracker;
    public ToolType ButtonType;
    public bool IsComplete;
}

public enum ToolType
{
    HotSpotTapped,
    AssetStoreTapped,
    UIApprovesPurchase,
    UIRejectsPurchase,
    UIShowPurchaseButtons,
    HotspotTapped,
    UpdateNavbarText,
    UnselectHotspot,
    CloseSwapPanel,
    OpenPaintBuyPopup,
    ClosePaintBuyPopup,
    ShowToolTip,
    DestroyToolTip,
    ExpandButtonClicked,
    SaveButtonClicked,
    ViewButtonClicked,
    CategoryClicked,
    AssetSelected
}

public enum AssetStoreTip
{
    AssetBuy,
    AssetDrag
}