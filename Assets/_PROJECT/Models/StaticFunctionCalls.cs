﻿public class StaticFunctionCalls
{
    #region settings
    //public static string DEBUG_FPS_VALUE = "DEBUG_FPS_VALUE";
    //public static string DEBUG_FPS_VALUE_SET = "DEBUG_FPS_VALUE_SET";

    //public static string GraphicSettings = "Graphic_Settings";

    public static string PostProcessSettings = "PostProcessSettings";
    #endregion


    #region UI stuff
    public static string openHUDPanel = "openHUDPanel";

    public static string MaximisePanel = "MaximisePanel"; // Not in use
    public static string MaximiseMenuPanel = "MaximiseMenuPanel"; // Only used by Tutorial Manager to know if Menu Panel was Opened
    public static string OpenMenuPanel = "OpenMenuPanel"; // Used to open the menu panel from outside. IKR.
    public static string ToggleLocalWorld_Deprycated = "ToggleLocalWorld";


    public static string ButtonDimension = "ButtonDimension";
    public static string ButtonDistance = "ButtonDistance";
    public static string ButtonRotate = "ButtonRotate";
    public static string ButtonHorizontal = "ButtonHorizontal";
    public static string ButtonVertical = "ButtonVertical";
    public static string ButtonDelete_Deprycated = "ButtonDelete";
    public static string ButtonDuplicate = "ButtonDuplicate";
    public const string ButtonSwap = "ButtonSwap";
    public const string ButtonInfo = "ButtonInfo";
    public const string ShopButtonClicked = "ShopButtonClicked";

    public static string LocalWorldSwithces = "LocalWorldSwticher";

    public static string AssetStoreCategory = "AssetStoreCategory";

    public static string ToggleLocalWallpaperWorld_Deprycated = "ToggleLocalWallpaperWorld";
    public static string WallpaperResize_Deprycated = "ButtonWallpaperResize";
    public static string WallpaperPosition_Deprycated = "ButtonWallpaperPosition";
    public static string WallpaperDeselected = "WallpaperDeselected";
    public static string WallpaperDeleted = "WallpaperDeleted";

    public static string MinimizeMenuPanel = "MinimizeMenuPanel";
    public static string HideUICauseMinimap = "HideUIButtons";
    public static string HideMiniMapandUI = "HideMinimapandUI";

    public const string ShowProductInfo = "ShowProductInfo";
    //public const string OnDatabaseUpdate_Deprycated = "OnDatabaseUpdate";

    //public const string SwapAsset = "SwapAsset";

    #endregion

    #region Element stuff

    #region Do
    public static string SelectAsset = "SelectAsset";
    public static string DeselectAsset = "DeselectAsset";
    public static string DeleteAsset = "DeleteAssetFromScene"; //From Scene Only

    public static string SelectWallfinish = "SelectWallfinish";
    public static string DeselectWallfinish = "DeselectWallfinish";
    #endregion

    #region Done
    public static string OnAssetSelected = "OnAssetSelected";
    public static string OnAssetDeselected = "OnAssetDeselected";
    public static string OnAssetDeleted = "OnAssetDeletedFromScene"; //From Scene Only

    public static string OnWallfinishSelected = "OnWallfinishSelected";
    public static string OnWallfinishDeselected = "OnWallfinishDeselected";
    #endregion


    public static string onGameObjectTapped_Deprycated = "GameObjectTapped";
    public static string AssetDragFromStoreEvents = "OnDragAssetFromStore";

    public static string OnHideAsset = "OnHideAsset";
    public static string OnSetAssetYRotation = "OnSetAssetYRotation";
    public static string OnShowAssetRelatedDistances = "OnShowAssetRelatedDistances";
    public static string OnSaveDesign = "OnSaveDesign";
    public static string OnUpdateFooterText = "OnUpdateFooterText";

    public static string DoDeselectAsset_Deprycated = "DoDeselectAsset";

    public static string OnUpdateTransaction = "OnUpdateTransaction";

    public static string DeselectAllPosters = "DeseletAllPosters";
    public static string GetWallPosterisOn = "GetWallPosterisOn";
    public static string SetMinimapImage = "SetMinimapImage";

    public static string DropWallfinishInScene = "DropWallfinishInScene";
    public static string TransmitRotationValue = "TransmitRotationpls";

    public static string RoomLoaded = "RoomLoaded";
    //public static string AssetSpawned = "AssetSpawned";

    public static string OpenTutorial = "OpenTutorial";
    public static string TutorialRunning = "TutorialRunning";
    public static string AssetPurchased = "AssetPurchased";
    #endregion

    #region dataServices
    public static string GetProductData = "GetProductData";

    public static string GetPaintData = "GetPaintData";
    public static string GetPaintRetailers = "GetPaintRetailers";
    public static string GetPaintGroups = "GetPaintGroups";

    public static string GetWallpaperData = "GetWallpaperData";

    public static string GetProductListData = "ProductListPanel";

    public static string TransferRenderPositionIndex = "RenderLocationIndexData";
    public static string TriggerPositionSave = "TriggerPositionSave";
    public static string RelocatePlayer = "RelocatePlayerPosition";

    public static string DeleteViewpoint = "DeleteViewpoint";

    public static string MovePlayerThroughPortal_Deprycated = "MovePlayerThroughPortal";


    #endregion

    #region Player

    public static string WallposterDroppedOnWall = "WallposterDroppedOnWall";
    public static string WallposterSelected = "WallposterSelected";
    public static string WallposterLongPressed_Deprycated = "WallposterLongPressed";
    public static string ToggleWallPosterEditMode = "WallPosterEditMode";

    public static string PlayerFPSLookAt = "PlayerFPSLookAt";
    public static string PlayerFPSLookCenter = "PlayerFPSLookCenter";

    public static string ToggleCamView = "ToggleCamView";
    #endregion

    #region Challenge/Constraint
    public static string ConstraintUpdate = "ConstraintUpdate";
    public static string ChallengeStatusUpdate = "ChallengeStatusUpdate";
    #endregion


    #region two point o
    public const string Action_HotspotSwapped = "Action_HotspotSwapped";
    public const string Request_HotspotSwap = "Request_HotspotSwap";

    public const string Action_AssetSwapped = "Action_AssetSwapped";
    //public const string Request_AssetSwap = "Request_AssetSwap";

    public const string Action_AssetDeleted = "Action_AssetDeleted";
    public const string Request_AssetDelete = "Request_AssetDelete";

    public const string Action_AssetLoaded = "Action_AssetLoaded";
    public const string Request_AssetLoad = "Request_AssetLoad";

    public const string ActionAssetSelected = "Action_AssetSelected";
    public const string Request_AssetSelect = "Request_AssetSelect";

    public const string Action_WallfinishSelected = "Action_WallfinishSelected";
    public const string Request_WallfinishSelect = "Request_WallfinishSelect";

    public const string Action_WallfinishSwapped = "Action_WallfinishApplied";
    public const string Request_WallfinishApply = "Request_WallfinishApply";

    /*public const string Action_SceneLoadComplete = "Action_SceneLoadComplete";*/

    public const string Action_DesignSubmited = "Action_DesignSubmited";
    public const string Request_DesignSubmit = "Request_DesignSubmited";

    //public const string Action_DesignSaved = "Action_DesignSaved";
    //public const string Request_DesignSave= "Request_DesignSave";

    /*public const string Action_Fail = "Action_Fail";*/

    public const string Action_CoinUpdated = "Action_CoinUpdated";
    public const string Action_BudgetUpdated = "Action_SwapCompleted";

    #endregion
}
