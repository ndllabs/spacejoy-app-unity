﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Homefuly;


public class ICategory
{
    public string _id;
    public string name;
    public string description;
    public List<string> children; //List of Subcat ids
    public string parentCat; //Parent Cat's Id
    public CategoryType type; //"Root","Category","Subcategory","Vertical"

    public ICategory()
    {

    }
    public ICategory(asset_category serverRes, string parentCat, CategoryType type)
    {
        _id = serverRes._id;
        name = serverRes.name;
        description = serverRes.description;
        children = new List<string>();
        this.parentCat = parentCat;
        this.type = type;
    }
    public ICategory(subcategories serverRes, string parentCat, CategoryType type)
    {
        _id = serverRes._id;
        name = serverRes.name;
        description = serverRes.description;
        children = new List<string>();
        this.parentCat = parentCat;
        this.type = type;
    }
    public ICategory(vertical serverRes, string parentCat, CategoryType type)
    {
        _id = serverRes._id;
        name = serverRes.name;
        description = serverRes.description;
        children = null;
        this.parentCat = parentCat;
        this.type = type;
    }
}

[Serializable]
public class ITheme
{
    public string _id;
    public string name;
    public string description;
}

[Serializable]
public class IThemesList
{
    public List<ITheme> asset_themes; //{{host}}/api/asset_themes?limit=all
}



#region Get All Categories Server Api 
/// <summary>
/// For Get All Categories Server Api Only
/// </summary>
[Serializable]
public class asset_category
{
    public string _id;
    public string name;
    public string description;
    public List<subcategories> subcategories; //List of Subcat ids
}
[Serializable]
public class asset_categorysList
{
    public List<asset_category> asset_categorys;
}
[Serializable]
public class subcategories
{
    public string _id;
    public string name;
    public string description;
    public List<vertical> verticals; //List of vert ids
}
[Serializable]
public class vertical
{
    public string _id;
    public string name;
    public string description;
}
#endregion
