﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class IConstraint
{
    /* type
     * text
     * val
     * */

    // public float minBudget;
    // public float maxBudget;
    // public float time;
    // public bool useBudget;
    // public bool useTime;

    // New constraint model
    public string _id;
    public IConstraintChild constraint;
    //public string constraintType; //ConstraintType
    //public float time;
    //public int minBudget;
    //public int maxBudget;
    //public string hotspotIds; 
}


[Serializable]
public class IConstraintList
{
    public List<IConstraint> constraints;
}


[Serializable]
public class IConstraintChild
{
    public string constraintType; //ConstraintType
    public float minBudget;
    public float maxBudget;
}

public enum ConstraintType
{
    budget,
    time,
    swap,
    add
}
