﻿using System;
using System.Collections.Generic;

[Serializable]
public class IUserRoomDesign
{
    public string _id; //Challengeid
    public DesignChallengeType challengeType;
    public string room;
    public string name;
    public string description;
    public string challengeLifetime; //dateTime
    public RewardsList Rewards;
    public List<IHotspot> assets;
    public List<IWallfinishHotspot> paint;
    public List<IConstraintList> constraints;
}

public class IUserRoomDesignList
{
    public List<IUserRoomDesign> userRoomDesigns;
}
