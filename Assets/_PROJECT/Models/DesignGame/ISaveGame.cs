﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class ISaveGame
{
    public string _id;
    public UserProfile userProfile;
    public int currentLevel;
    public float currentCoins;
    public List<string> challengesCompleted;
    public List<string> challengesOngoing;
    public float totalTime;
    public List<string> ownRooms;
    public List<IAsset> assetsOwned;
    public List<string> paintOwned;



    /// <summary>
    /// Warning: Comparatively Expensive. Copy the response to a varibale to avoid repetitive calls.
    /// </summary>
    public Dictionary<string, IAsset> assetsOwned_Indexed { get { return assetsOwned.ToDictionary(x => x._id, x => x); } }

    /// <summary>
    /// Warning: Comparatively Expensive. Copy the response to a varibale to avoid repetitive calls.
    /// </summary>
    public Dictionary<string, string> wallfinishsOwned_Indexed { get { return paintOwned.ToDictionary(x => x, x => x); } }


    public static ISaveGame CreateCopy(ISaveGame x)
    {
        return new ISaveGame()
        {
            _id = x._id,
            userProfile = x.userProfile,
            currentLevel = x.currentLevel,
            currentCoins = x.currentCoins,
            challengesCompleted = x.challengesCompleted,
            challengesOngoing = x.challengesOngoing,
            totalTime = x.totalTime,
            ownRooms = x.ownRooms,
            assetsOwned = x.assetsOwned,
            paintOwned = x.paintOwned
        };
    }
}

[Serializable]
public class UserProfile
{
    public string gender;
    public string firstName;
    public string lastName;
    public string dob;
    public string name;
}
