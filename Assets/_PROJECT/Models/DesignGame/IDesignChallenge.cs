﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class IDesignChallenge
{
    public string _id; //Challengeid
    public DesignChallengeType challengeType;
    public string room;
    public string name;
    public string description;

    public string challengeLifeTime; //dateTime
    public ITransform camera; 
    public List<ChallengeReward> rewards;

    public List<IHotspot> baseAssets;
    public List<IWallfinishHotspot> basePaint;
    public List<IConstraint> constraints;

    /// <summary>
    /// Warning: Comparatively Expensive. Copy the response to a varibale to avoid repetitive calls.
    /// </summary>
    public Dictionary<string, IHotspot> baseAssets_Indexed { get { return baseAssets.ToDictionary(x => x._id, x => x); } }

    /// <summary>
    /// Warning: Comparatively Expensive. Copy the response to a varibale to avoid repetitive calls.
    /// </summary>
    public Dictionary<string, IWallfinishHotspot> basePaints_Indexed { get { return basePaint.ToDictionary(x => x._id, x => x); } }

    /// <summary>
    /// Warning: Comparatively Expensive. Copy the response to a varibale to avoid repetitive calls.
    /// </summary>
    public Dictionary<string, IConstraint> constraints_Indexed { get { return constraints.ToDictionary(x => x._id, x => x); } }



}

[Serializable]
public class ChallengeReward
{
    public string _id;
    public string rewardType; // Parse to enum RewardType and use in your code
    public float rewardValue;
}

[Serializable]
public class IDesignChallengeList
{
    public List<IDesignChallengeObj> challenges;
    public int count;

    public Dictionary<string, IDesignChallengeObj> challenges_Indexed{get {return challenges.ToDictionary(x => x.challenge._id, x => x);}}
}

[Serializable]
public class IDesignChallengeObj
{
    public IDesignChallenge challenge;
    public bool completed;
}

public enum DesignChallengeType
{
    none,
    design
}

public enum RewardType
{
    coin
}

public class RewardsList
{
    public List<Reward> rewards;
}

public class Reward
{
    public string rewardType;
    public int rewardValue;
}