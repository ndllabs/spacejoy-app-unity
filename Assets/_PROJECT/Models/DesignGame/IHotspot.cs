﻿using System;
using System.Collections.Generic;

[Serializable]
public class IHotspot : ISceneObject
{
    public string _id;
    public IAsset asset;
    public bool isHotspot;
    public bool isMandatory;
    public List<SwappableAssets> swapAssets;
}

[Serializable]
public class IWallfinishHotspot : ISceneObject
{
    public string _id;
    public IWallfinish wallfinish;
    public float coins;
    public string wall;
    public ITransform hotspotTransform;
    public bool isHotspot;
    public bool isMandatory;
    public List<SwappableWallFinishes> swapWallFinish;
}

[Serializable]
public class SwappableWallFinishes
{
    public string _id;
    public string wallFinish;
}

[Serializable]
public class SwappableAssets
{
    public string _id;
    public IAsset asset;
}
