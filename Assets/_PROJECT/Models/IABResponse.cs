﻿using UnityEngine;
using UnityEngine.Networking;

public class IABResponse
{
    private string _urlData;


    public IABResponse(string url)
    {
        // this._urlData = System.Uri.UnescapeDataString(url);
        this._urlData = url;
    }

    public bool Err
    {
        get
        {
            return bool.Parse(GetParam("err", this._urlData));
        }
    }

    public string Context
    {
        get
        {
            return GetParam("context", this._urlData);
        }
    }

    public string data
    {
        get
        {
            return GetParam("data", this._urlData);
        }
    }


    public string shopUrl
    {
        get
        {
            return this._urlData;
        }
    }

    public T GetData<T>()
    {
        return JsonUtility.FromJson<T>(GetParam("data", this._urlData));
        // return JsonUtility.FromJson<T>(data.Split('|')[1].Trim().ToString());
    }

    // public T GetContext<T>()
    // {
    //     return JsonUtility.FromJson<T>(data.Split('|')[0].Trim().ToString());
    // }


    private string GetParam(string key, string url)
    {
        string srch = "&" + key + "=";
        int start = url.IndexOf(srch);
        if (start < 0)
            return "";
        start = start + key.Length + 2;

        string heap = url.Substring(start);
        int end = heap.IndexOf('&');
        return end > 0 ? heap.Substring(0, end) : heap.Substring(0);
    }

}