﻿using System;
using System.Collections.Generic;

[Serializable]
public class IUserAuth
{
    public string email;
    public string password;
}

[Serializable]
public class IUserRegister
{
    public string firstName;
    public string lastName;
    public string email;
    public string password;
    public string role = "customer";
}

[Serializable]
public class ISubscriber
{
    public string user;
    public string orgrole = "";
}

[Serializable]
public class IUserTokenAuth
{
    public string provider;
    public string token;
}

[Serializable]
public class IUserAuthResponse
{
    public string token;
    public IUserAuthResponseUser user;
}

[Serializable]
public class IUserAuthResponseUser
{
    public string _id;
    public string name;
    public string email;
    public string role;
}

[Serializable]
public class IApiStatus
{
    public bool err;
    public string data;
}

[Serializable]
public class IGoogleAuth
{
    public string code;
    public string redirect_uri;
    public string client_id;
    public string client_secret;
    public string scope;
    public string grant_type;
}

[Serializable]
public class IGoogleAuthResponse
{
    public string access_token;
    public string id_token;
    public string expires_in;
    public string token_type;
    public string scope;
    public string refresh_token;
}