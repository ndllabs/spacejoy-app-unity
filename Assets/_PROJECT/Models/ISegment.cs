﻿using System;
using System.Collections.Generic;

[Serializable]
public class ISegment
{
    public string _id;
    public string name;

}

[Serializable]
public class ISegmentRes
{
    public List<ISegment> roomversions;
}