﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class IDemoDesign 
{
    public bool demo;
    public string _id; //versionID
    public string name;
    public IDemoDesignRoomField room;
}

[Serializable]
public class IDemoDesignRoomField
{
    public string organization;
    public string _id; //roomID
    public string name;
    public string project;

}

[Serializable]
public class IDemoDesignsRes
{
    public List<IDemoDesign> roomversions;
}


[Serializable]
public class IDesignimage
{
    public string filename;
    public List<IIDesignimageFileField> files;
}

[Serializable]
public class IIDesignimageFileField
{
    public string type;
    public string url;

}

[Serializable]
public class IDesignimagesRes
{
    public List<IDesignimage> designimages;
}
