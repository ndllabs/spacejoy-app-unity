﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPlayerFPSAttributes
{
    public float movementSpeed;
    public float rotationSpeed;
    public float thresholdX;
    public float thresholdY;

    public IPlayerFPSAttributes()
    {
        movementSpeed = 0;
        rotationSpeed = 0;
        thresholdY = 0;
        thresholdX = 0;
    }
    public IPlayerFPSAttributes(float a,float b, float c, float d)
    {
        movementSpeed = a;
        rotationSpeed = b;
        thresholdX = c;
        thresholdY = d;
    }
}
