﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ITransform
{
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;

    public ITransform() { }

    public ITransform(Transform transform)
    {
        this.position = transform.position;
        this.rotation = transform.rotation.eulerAngles;
        this.scale = transform.localScale;
    }

    public static void ToTransform(ITransform itrans,Transform trans)
    {
        trans.position = itrans.position;
        trans.rotation = Quaternion.Euler(itrans.rotation);
        trans.localScale = itrans.scale;
    }
}