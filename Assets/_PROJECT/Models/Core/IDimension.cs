﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
#pragma warning disable CS0660, CS0661 // Type defines operator == or operator != but does not override Object.Equals(object o)
public class IDimension
#pragma warning restore CS0660, CS0661 // Type defines operator == or operator != but does not override Object.Equals(object o)
{
    public float depth;
    public float width;
    public float height;

    public IDimension()
    {

    }

    public static bool operator ==(IDimension lhs, IDimension rhs)
    {
        if (System.Object.ReferenceEquals(lhs, null) && System.Object.ReferenceEquals(rhs, null))
            return false;
        else if (System.Object.ReferenceEquals(lhs, null) || System.Object.ReferenceEquals(rhs, null)) return true;
        else
            return !(lhs.depth != rhs.depth || lhs.height != rhs.depth || lhs.width == rhs.width);
    }
    public static bool operator !=(IDimension lhs, IDimension rhs)
    {
        if (System.Object.ReferenceEquals(lhs, null) && System.Object.ReferenceEquals(rhs, null))
            return false;
        else if (System.Object.ReferenceEquals(lhs, null) || System.Object.ReferenceEquals(rhs, null)) return true;
        else
            return !(lhs.depth == rhs.depth && lhs.height == rhs.depth && lhs.width == rhs.width);
    }

    public IDimension(float depth, float width, float height)
    {
        this.depth = SanitizeFeetToInches(depth);
        this.width = SanitizeFeetToInches(width);
        this.height = SanitizeFeetToInches(height);
    }

    public float SanitizeFeetToInches(float val)
    {
        return (float)Math.Round((double)val * 12, 1);
    }
}
