﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ISceneObject
{
    // [NonSerialized]
    // public string _id;
    
    public ITransform transform;

    [NonSerialized]
    public GameObject objRef;
}
