﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class IWallfinish
{
    public string _id;
    public string name;
    public string wftype; //Enum: wftype //"paint","wallpaper"
    public string category; //Enum: wallfinishCategory //"shade","wallpaper","texture","stencil"
    public string description;
    public IdesignedBy designedBy;
    public Texture texture;
    public string textureUrl;
    public string imgurl;
    public string code; //color's Hex-Code
    public float price;
}

[Serializable]
public class IWallfinishEntry
{
    [NonSerialized]
    public string entryId;
    
    public string wall; //To What Wall Its Being Applied
    public IWallfinish wallfinishData;
    public string wallfinish; //wallfinish id
    public IWallfinishAdjust adjust;
    public GameObject objRef;

    //public static IWallfinishHotspot ToHotspot(this IWallfinishEntry wf)
    //{
    //    return new IWallfinishHotspot
    //    {
    //        asset = wf.wallfinishData,
            
    //    }
    //}

    // public static IWallfinishEntry CreateFromIWallfinishHotspot(IWallfinishHotspot x)
    // {
    //     return new IWallfinishEntry() {
    //         entryId = x._id,
    //         wall = x.wallName,
    //         wallfinishData = x.asset,
    //         wallfinish = x.asset._id
    //     };
    // }

    // Create Assetentry from Ihotspot
    public static List<IWallfinishEntry> CreateFromIWallfinishHotspotList(List<IWallfinishHotspot> ihotspots)
    {
        List<IWallfinishEntry> k = new List<IWallfinishEntry>();
        ihotspots.ForEach(x => {
            if(!x.isHotspot)
            {
                k.Add(new IWallfinishEntry() {
                    // entryId = x._id,
                    wall = x.wall,
                    wallfinishData = x.wallfinish,
                    wallfinish = x.wallfinish._id
                });
            }
        });
        return k;
    }
}




[Serializable]
public class IWallfinishAdjust
{
    public Vector3 tile;
    public Vector3 offset;
    public Vector3 clip;
    public Vector3 scale;
    public float repeat;

    public IWallfinishAdjust() { }
}

[Serializable]
public class IWallfinishGroup
{
    public string _id;
    public string count;

}




[Serializable]
public class IdesignedBy
{
    public string name;
    public string organization;
    public string organizationInternalLink;
    public string productUrl;
}

public class IWallposterEntry_Deprycated
{
    public string wall;
    public string wallfinish;
    public IWallfinishAdjust adjust;

    public IWallposterEntry_Deprycated() { }
}

[Serializable]
public class IWallposterEntryList
{
    public List<IWallposterEntry_Deprycated> paint;
}

