﻿using System;
using System.Collections.Generic;

[Serializable]
public class ICenter
{
    public string _id;
    public string name;
}

[Serializable]
public class ICenterRes
{
    public List<ICenter> projectrooms;
}

[Serializable]
public class IRoomVersions
{
    public ICenter room;
    public List<ICenter> versions;
}

[Serializable]
public class IRoomVersionsRes
{
    public List<IRoomVersions> projectrooms;
}

[Serializable]
public class IProjects
{
    public string _id;
    public string name;
    public bool paymentDone;
    public bool oldProject;
    public IQuiz[] quiz;
    public IPhase[] phase;
}

[Serializable]
public class IProjectsRes
{
    public List<IProjects> projects;
}

[Serializable]
public class IQuiz
{
   public string status; //Check for  "completed"
}

[Serializable]
public class IPhase
{
    public string status; //Check for  "completed"
}