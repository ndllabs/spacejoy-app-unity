﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Homefuly;

[Serializable]
public class IAssetEntry : ISceneObject
{
    [NonSerialized]
    public string _id;
    public string entryId;

    public IAsset asset;

    //public ITransform transform;

    public bool inuse;

    public bool invoice;

    //public GameObject objRef;

    // Create assetentry from ihotspot
    // public static IAssetEntry CreateFromIHotspot(IHotspot x)
    // {
    //     return new IAssetEntry() {
    //         entryId = x._id,
    //         asset = x.asset,
    //         transform = x.transform,
    //         inuse = !x.isHotspot
    //     };
    // }

    // Create Assetentry from Ihotspot
    public static List<IAssetEntry> CreateFromIHotspotList(List<IHotspot> ihotspots)
    {
        List<IAssetEntry> k = new List<IAssetEntry>();
        ihotspots.ForEach(x => {
            if(!x.isHotspot)
            {
                k.Add(new IAssetEntry() {
                    // entryId = x._id,
                    asset = x.asset,
                    transform = x.transform,
                    inuse = !x.isHotspot
                });
            }
        });
        return k;
    }
}
