﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class IRetailer
{
    public string service; //Enum:RetailerType //"furniture","paint",
    public string _id;
    public string name;
    public string description;
    public string url;
    public string country; //Enum:Country //"India","US"
}

[Serializable]
public class IRetailerList
{
    public List<IRetailer> retailers;
}
