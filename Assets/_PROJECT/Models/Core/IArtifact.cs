﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class IArtifactFile
{
    public string _id;
    public ArtifactType artifactType;
    public string downloadLocation;
    public string location;
}

[Serializable]
public class IArtifactFileResponse
{
    public IArtifactFile artifact;
    public bool err;
    public string data;
}

public class IArtifactLoadAsyncOperationResult
{
    public bool err;
    public string data;
    public GameObject gameObject;
}


[Serializable]
public class IAsset
{
    public string _id;
    public string name;
    public string description;

    public string mountType;
    public float clampVal;
    public string roomType;

    public string currency;
    public IAssetPrice price; //use: price.customer

    public string category;
    public string subcategory;
    public string vertical;


    public bool glossy;
    public IDimension dimension;
    public IdesignedBy designedBy;
    public Texture texture;
    public string textureUrl;

    public string updatedAt;

  
}

[Serializable]
public class IAssetPrice
{
    public float customer;
    public float vendor;
}




[Serializable]
public class IPaintStoreElement_Deprycated
{
    public string _id;
    public IdesignedBy designedBy;
    public string wftype; //enum?
    public string category;
    public string group;
    public string name;
    public string code;

    public IPaintStoreElement_Deprycated(string id)
    {
        _id = id;
    }
}

[Serializable]
public class IPaintRetailer_Deprycated
{
    public string wftype; //enum?
    public string _id;
    public string name;
    public string description;
    public string url;
    public string country;
}




#region Enums

public enum PaintType
{
    paint,
    poster
}

public enum ArtifactType
{
    asset,
    room
}
#endregion