﻿using System;
using UnityEngine;

[System.Serializable]
public class IFloorPlanProcessor
{
    public Material matLight;
    public GameObject lightHalo;
    public float haloOffset;

    public GameObject doorPortal_Deprycated;

    public Material segmentMaterial_Deprycated;
}
