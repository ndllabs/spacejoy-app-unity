﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class IViewpointEntry
{
    public ITransform transform;
    public string fov;
    public string meta1 = "";
    public string meta2 = "";
}
