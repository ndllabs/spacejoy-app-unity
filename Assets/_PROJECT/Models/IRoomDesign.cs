﻿using System.Collections.Generic;
using System;
using Homefuly;

[Serializable]
public class IRoomDesign
{

    public ITransform camera;
    public string _id;
    public string name;
    public bool demo;
    public List<IAssetEntry> assets; //entry
    
    public string room; //roomId;
    public string project;
    public string customer; // reference to user 
    public string challenge; // challenge reference

    public DateTime createdAt;
    public DateTime UpdatedAt;

    public List<ITransform> viewPoints; //Deprycated
    public List<IViewpointEntry> viewpoint;
    public List<IWallfinishEntry> paint;


    public void Reset()
    {
        _id = string.Empty;
        name = string.Empty;

        camera = null;

        assets = null;

        room = string.Empty;

        viewPoints = null;

        viewpoint = null;
    }

    public static IRoomDesign CreateCopy(IRoomDesign x)
    {
        return new IRoomDesign() { camera = x.camera,
            _id = x._id,
            name = x.name,
            assets = x.assets,
            project = x.project,
            customer = x.customer,
            room = x.room,
            challenge = null,
            demo = x.demo,
            createdAt = x.createdAt,
            UpdatedAt = x.UpdatedAt,
            viewPoints = x.viewPoints,
            viewpoint = x.viewpoint,
            paint = x.paint
        };
    }

     public static IRoomDesign CreateFromChallenge(IDesignChallenge x)
    {
        return new IRoomDesign() { 
            // _id = HomefulyExtensions.randomIdGenerator().ToString(),
            challenge = x._id,
            name = x.name,
            room = x.room,
            demo = false,
            project = AppConfig.ProjectConfig.PROJ_CHALLENGE,
            customer = AppComponent.Instance.account.CRED_USERID,
            camera = x.camera,
            assets = null, //IAssetEntry.CreateFromIHotspotList(x.baseAssets),
            paint = null //IWallfinishEntry.CreateFromIWallfinishHotspotList(x.basePaint)
        };
    }
}



[Serializable]
public class IRoomDesignList
{
    public List<IRoomDesign> roomversions;
}


[Serializable]
public class IRoomDesignPost
{
    public IRoomDesign roomversion;
}
