﻿using System;
using System.Collections;
using System.Collections.Generic;
#if PLATFORM_ANDROID
using Unity.Notifications.Android;
#elif PLATFORM_IOS
using Unity.Notifications.iOS;
#endif
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
    void Start()
    {
#if PLATFORM_ANDROID
        var c = new AndroidNotificationChannel()
        {
            Id = "channel_id",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
        //SendAndroidNotification();    //to send notification

#endif
#if PLATFORM_IOS
        NotificationPermissionPls();
#endif
    }
#if PLATFORM_ANDROID

    void SendAndroidNotification()
    {
        var notification = new AndroidNotification();
        notification.Title = "SomeTitle";
        notification.Text = "SomeText";
        notification.FireTime = System.DateTime.Now.AddSeconds(5);

        var identifier = AndroidNotificationCenter.SendNotification(notification, "channel_id");

        AndroidNotificationCenter.OnNotificationReceived += Callback;
    }

    void Callback(int identifier, AndroidNotification notification, string channel)
    {
        var msg = "Notification received : " + identifier + "\n";
        msg += "\n Notification received: ";
        msg += "\n .Title: " + notification.Title;
        msg += "\n .Body: " + notification.Text;
        msg += "\n .Channel: " + channel;
        Debug.Log(msg);
    }
    

#endif
#if PLATFORM_IOS

    IEnumerator NotificationPermissionPls()
    {
    
        using (var req = new AuthorizationRequest(AuthorizationOption.AuthorizationOptionAlert | AuthorizationOption.AuthorizationOptionBadge, true))
{
    while (!req.IsFinished)
    {
        yield return null;
    };

    string res = "\n RequestAuthorization: \n";
    res += "\n finished: " + req.IsFinished;
    res += "\n granted :  " + req.Granted;
    res += "\n error:  " + req.Error;
    res += "\n deviceToken:  " + req.DeviceToken;
    Debug.Log(res);
}
        //SendIosNotification();    //to send notification

    }
    iOSNotificationTimeIntervalTrigger timeTrigger = new iOSNotificationTimeIntervalTrigger()
    {
        TimeInterval = new TimeSpan(0, 0, 5),
        Repeats = false
    };
    void SendIosNotification()
    {

    var notification = new iOSNotification()
    {
        // You can optionally specify a custom Identifier which can later be 
        // used to cancel the notification, if you don't set one, an unique 
        // string will be generated automatically.
        Identifier = "_notification_01",
        Title = "Title",
        Body = "Scheduled at: " + DateTime.Now.ToShortDateString() + " triggered in 5 seconds",
        Subtitle = "This is a subtitle, something, something important...",
        ShowInForeground = true,
        ForegroundPresentationOption = PresentationOption.NotificationPresentationOptionAlert,
        CategoryIdentifier = "category_a",
        ThreadIdentifier = "test",//SOS
        Trigger = timeTrigger,
    };

    iOSNotificationCenter.ScheduleNotification(notification);
    }
#endif
}
