﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class MiniMapController : MonoBehaviour
{
    Camera cam;
    public GameObject Minimap;
    float MapWidth = 200, MapHeight = 100;
    Rect minimapRect;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        minimapRect = Minimap.GetComponent<RectTransform>().rect;

        //Debug.Log("Screen: W=" + Screen.width.ToString() + ", H=" + Screen.height);
        //Debug.Log("M Rect:" + minimapRect.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // MinimapClick();
        }
    }


    public void Recenter(Transform trans)
    {
        // trans.position = (Vector3)trans.GetCenter(Vector3.up * 20f);
    }



    public void MinimapClick()
    {
        var miniMapRect = Minimap.GetComponent<RectTransform>().rect;

        MapWidth = Screen.width;
        MapHeight = Screen.height;
        var mousePos = Input.mousePosition;
        var camPos = Vector3.zero;
        var newMousPos = Vector3.zero;


        var screenRect = new Rect(
            Minimap.transform.position.x,
            Minimap.transform.position.y,
            miniMapRect.width, miniMapRect.height);

        newMousPos.y -= screenRect.y;
        newMousPos.x -= screenRect.x;

        /*

        var mousePos = Input.mousePosition;
        mousePos.y -= screenRect.y;
        mousePos.x -= screenRect.x;

        var camPos = new Vector3(
            mousePos.x * (MapWidth / screenRect.width),
            mousePos.y * (MapHeight / screenRect.height),
            Camera.main.transform.position.z);


*/
        camPos.x = Screen.width - miniMapRect.width - mousePos.x;
        camPos.y = Screen.height - miniMapRect.height - mousePos.y;


        camPos.x = Screen.width * (mousePos.x / camPos.x);
        camPos.y = Screen.height * (mousePos.y / camPos.y);


        Debug.Log("mousePos: " + mousePos.ToString());
        Debug.Log("newMousPos: " + newMousPos.ToString());
        Messenger<Vector3>.Broadcast("ACTOR_SPAWN", camPos);
    }


}
