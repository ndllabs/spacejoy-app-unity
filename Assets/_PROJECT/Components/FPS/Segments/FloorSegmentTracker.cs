﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class FloorSegmentTracker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Player")
        {
            Messenger<string>.Broadcast("UI_ONUPDATE_SEGMENTNAME", gameObject.name);
        }
    }

    void OnMouseDown()
    {
        print("Transport to " + gameObject.name);
        Messenger<GameObject>.Broadcast("ACTOR_SPAWN", gameObject);
    }
}
