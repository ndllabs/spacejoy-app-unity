﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
//using TouchScript.Gestures;
using _unity;

public class DoorPortalLauncher_Deprycated : MonoBehaviour
{
    public bool keepItReady = true;
    MeshRenderer rend;
    Color originalColor;
    Color highlightColor;

    //TapGesture tapGesture;

    private void Awake()
    {
        //tapGesture = transform.GetOrAddComponent<TapGesture>();
    }
    // Start is called before the first frame update
    void Start()
    {
        ColorUtility.TryParseHtmlString("#cccccc", out highlightColor);
        rend = GetComponent<MeshRenderer>();
        originalColor = rend.material.color;


    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        rend.material.color = keepItReady ? highlightColor : originalColor;
    }

    void OnEnable()
    {
        //tapGesture.Tapped += OnTapped;
    }

    void OnDisable()
    {
        //tapGesture.Tapped -= OnTapped;
    }

    void OnTapped(object sender, System.EventArgs e)
    {
        Debug.Log("Goota call brother sam");
        if (keepItReady)
        {
            //Messenger.Broadcast(AppEvents.OP_PORTAL_TRANS_INIT);
            //keepItReady = false;
        }
    }
}
