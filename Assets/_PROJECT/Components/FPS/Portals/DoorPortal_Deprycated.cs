﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
//using TouchScript.Gestures;
using _unity;
using UnityEngine.EventSystems;

using Homefuly;

//[RequireComponent(typeof(LongPressGesture))]
public class DoorPortal_Deprycated : MonoBehaviour
{
    public string group;
    public string side;
    private int fingerID = -1;

    const string E_DoorPortal = "Move_DoorPortal";
    public bool insideTrigger = false;

    Vector3 toSpawn;
    Transform withPlayer;
//private LongPressGesture longPressGesture;

    private void Awake()
    {
        Debug.LogWarning("[Deprycation Warning] class DoorPortal_Deprycated no more useful");

        //longPressGesture = transform.GetOrAddComponent<LongPressGesture>();
        //longPressGesture.TimeToPress = 0.3f;
        //longPressGesture.DistanceLimit = 0.1f;
#if !UNITY_EDITOR
             fingerID = 0; 
#endif
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        //longPressGesture.LongPressed += OnTapped;

        //Messenger<DoorPortal, Transform>.AddListener(E_DoorPortal, OnDoorPortalExec);
        //Messenger.AddListener(AppEvents.OP_PORTAL_TRANS_INIT, OnExecTrans);

        //Messenger<DoorPortal, Transform, bool>.AddListener(AppEvents.OP_PORTAL_TRANS_LAUNCH, OP_PORTAL_TRANS_LAUNCH);
    }

    private void OnDisable()
    {
        //longPressGesture.LongPressed -= OnTapped;

        //Messenger<DoorPortal, Transform>.RemoveListener(E_DoorPortal, OnDoorPortalExec);
        //Messenger.RemoveListener(AppEvents.OP_PORTAL_TRANS_INIT, OnExecTrans);

        //Messenger<DoorPortal, Transform, bool>.RemoveListener(AppEvents.OP_PORTAL_TRANS_LAUNCH, OP_PORTAL_TRANS_LAUNCH);
    }

    void OnTapped(object sender, System.EventArgs e)
    {
        if (!EventSystem.current.IsPointerOverGameObject(fingerID))
        {
            OnExecTrans();
        }

        //Debug.Log("Goota call brother sam");
        //if (keepItReady)
        //{
        //Messenger.Broadcast(AppEvents.OP_PORTAL_TRANS_INIT);
        //keepItReady = false;
        //}
    }

    void OnExecTrans()
    {
        string sideToFind = "DoorPortal_";
        if (side.ToLower() == "a")
        {
            sideToFind += "B";
        }
        else
        {
            sideToFind += "A";
        }

        if (transform.parent.Find(sideToFind) != null)
        {
            if (insideTrigger)
            {
                //transform.parent.Find(sideToFind).GetComponent<DoorPortal>().insideTrigger = true;
                Messenger<Vector3>.Broadcast(StaticFunctionCalls.MovePlayerThroughPortal_Deprycated, transform.parent.Find(sideToFind).position);
            }
            else
            {
                Debug.Log("Bro's at:" + transform.parent.Find(sideToFind).position);
                var roomName = "Switch Rooms";
                var splitStr = transform.parent.parent.name.Split(new char[] { '_' }, 6);
                if (splitStr.Length >= 6)
                {
                    roomName = splitStr[5].Replace('_', ' ').ToTitleCase();
                }

                //WidgetManager.Instance.Confirm(roomName, "Are you sure you want to switch rooms?", (bool ans) =>
                WidgetManager.Instance.ConfirmToast("Go to " + roomName, true, 3, () =>
                     {
                    //transform.parent.Find(sideToFind).GetComponent<DoorPortal>().insideTrigger = true;
                    Messenger<Vector3>.Broadcast(StaticFunctionCalls.MovePlayerThroughPortal_Deprycated, transform.parent.Find(sideToFind).position);
                     });
            }
        }
        else
        {
            Debug.LogWarning("[Door Portal] Find Bro's Location Failed, portals will go offline now");
        }

        //Debug.Log("Lolwa: " + withPlayer.name + " lol " + toSpawn);
        //if (withPlayer != null && toSpawn != Vector3.zero)
        //{
        //    withPlayer.position = toSpawn;
        //}

    }

    //void OP_PORTAL_TRANS_LAUNCH(DoorPortal tSrc, Transform player, bool value)
    //{
    //    if (!trigger || group != tSrc.group || side == tSrc.side)
    //    {
    //        //print("Rejecting form " + side);
    //        return;
    //    }

    //    if (value)
    //    {
    //        MakeLaumcherReady(player);
    //    }
    //    else
    //    {
    //        MakeLaumcherDefuse();
    //    }

    //}


    //void OnDoorPortalExec(DoorPortal tSrc, Transform player)
    //{
    //    if (!trigger || group != tSrc.group || side == tSrc.side)
    //    {
    //        //print("Rejecting form " + side);
    //        return;
    //    }

    //    tSrc.trigger = false;

    //    toSpawn = player.position;
    //    toSpawn.x = transform.position.x;
    //    toSpawn.z = transform.position.z;

    //    //player.position = toSpawn;

    //    withPlayer = player;

    //    tSrc.GetComponentInParent<DoorPortalLauncher>().keepItReady = true;
    //}


    void MakeLaumcherReady(Transform player)
    {
        //GetComponentInParent<DoorPortalLauncher>().keepItReady = true;
        transform.parent.Find("DoorPortal_A/DoorPortalSwitch").gameObject.SetActive(true);
        transform.parent.Find("DoorPortal_B/DoorPortalSwitch").gameObject.SetActive(true);

        //toSpawn = player.position;
        //toSpawn.x = transform.position.x;
        //toSpawn.z = transform.position.z;

        ////player.position = toSpawn;

        //withPlayer = player;
    }

    void MakeLaumcherDefuse()
    {
        //GetComponentInParent<DoorPortalLauncher>().keepItReady = false;

        transform.parent.Find("DoorPortal_A/DoorPortalSwitch").gameObject.SetActive(false);
        transform.parent.Find("DoorPortal_B/DoorPortalSwitch").gameObject.SetActive(false);

        //toSpawn = Vector3.zero;
        //withPlayer = null;
    }


    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            insideTrigger = true;
            //Messenger<DoorPortal, Transform>.Broadcast(E_DoorPortal, this, col.gameObject.transform);

            MakeLaumcherReady(col.gameObject.transform);

            //Messenger<DoorPortal, Transform, bool>.Broadcast(AppEvents.OP_PORTAL_TRANS_LAUNCH, this, col.gameObject.transform, true);
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            MakeLaumcherDefuse();
            //Messenger<DoorPortal, Transform, bool>.Broadcast(AppEvents.OP_PORTAL_TRANS_LAUNCH, this, col.gameObject.transform, false);
            insideTrigger = false;
            //StartCoroutine(Wait(3, () =>
            //{
            //    trigger = true;
            //}));
        }
    }


    IEnumerator Wait(float sec, System.Action Callback)
    {
        yield return new WaitForSeconds(sec);
        Callback();
    }
}
