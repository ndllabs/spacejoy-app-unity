﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FPSPlayer : MonoBehaviour
{
    [Header("Speed")]
    public IFPSPlayerSpeed playerSpeed;

    public bool isEnabled = true;

    Rigidbody rigidBody;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isEnabled)
            Movement();
    }


    void Movement()
    {
        float move, look/*, pan*/;

        if (Input.touchCount == 1) //Input from Touches
        {
            move = Input.touches[0].deltaPosition.y * playerSpeed.touchOffset;
            look = Input.touches[0].deltaPosition.x * playerSpeed.touchOffset;

            move = Mathf.Abs(move) >= playerSpeed.inputThreshold.y ? move : 0;
            look = Mathf.Abs(look) >= playerSpeed.inputThreshold.x ? look : 0;


            rigidBody.velocity = transform.forward * move;

            transform.Rotate(transform.up * look, Space.World);
        }

        if (Input.GetMouseButton(0))
        {
            move = Input.GetAxis("Mouse Y") * playerSpeed.movement * Time.deltaTime;
            look = Input.GetAxis("Mouse X") * playerSpeed.rotation * Time.deltaTime;


            rigidBody.velocity = transform.forward * move;

            transform.Rotate(transform.up * look, Space.World);
        }

        //if (Input.GetMouseButton(1))
        //{
        //    print("RC");
        //    move = look = 0;

        //    if (Input.touchCount == 2)
        //        pan = Input.touches[1].deltaPosition.x * playerSpeed.touchOffset;
        //    else
        //    {
        //        pan = Input.GetAxis("Mouse X") * playerSpeed.pan * Time.deltaTime;
        //    }

        //    rigidBody.velocity = transform.right * pan;


        //}

        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || Input.touchCount == 0)
        {
            rigidBody.velocity = Vector3.zero;
            transform.Rotate(Vector3.zero, Space.World);
        }

    }



    void SpawnActor(GameObject go)
    {
        Debug.LogWarning(nameof(SpawnActor) + " deprycated");

        //var pos = (Vector3)go.transform.GetCenter();
        //SpawnActorPosition(pos);
    }

    public void SpawnActorPosition(Vector3 pos, Vector3? rotation = null)
    {

        print("Spawn at: " + pos.ToString());
        //pos.y = transform.localPosition.y < 0 ? .5f : transform.localPosition.y;

        if (rotation != null)
            transform.rotation = Quaternion.Euler((Vector3)rotation);

        transform.position = pos;
        // transform.rotation = Quaternion.identity;

    }
}


[System.Serializable]
public class IFPSPlayerSpeed
{
    [Header("Speed")]
    public float movement;
    public float pan;
    public float rotation;

    [Header("Threshold")]
    public Vector2 inputThreshold;


    [Header("Offset")]
    public float touchOffset;

    public void Reset()
    {
        movement = pan = rotation = 0;
    }
}
