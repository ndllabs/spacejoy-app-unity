﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    new Camera camera;
    float aspectRatio;
    const float HardCodedX = -29.85f, HardCodedY = 124.7f;
    int FOV;
    // Start is called before the first frame update
    void Start()
    {
        transform.Translate(new Vector3(0, 0.1f, 0));
        camera = GetComponent<Camera>();
        aspectRatio = camera.aspect;   //(float) Screen.height / Screen.width;
        FOVManager();
    }


    /// <summary>
    /// Decides the FOV for the scene.
    /// IPad pro is 1.33, 16:9 is 1.77, notches can be >2;
    /// hardcoding values in equation to get 1.33 = 85 ; 2 = 65.
    /// </summary>
    void FOVManager()
    {

        FOV = (int)(aspectRatio * HardCodedX + HardCodedY);
        //Debug.Log("Aspect ratio is " + aspectRatio+", FOV is "+FOV);
        camera.fieldOfView = FOV;
    }
}
