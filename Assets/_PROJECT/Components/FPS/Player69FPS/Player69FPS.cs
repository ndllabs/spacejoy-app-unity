﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
using UnityEngine;
using _unity.Events;
using UnityEngine.EventSystems;
using System;
using DG;
using DG.Tweening;
using TouchMan;

using Homefuly;

public class Player69FPS : MonoBehaviour
{
    public bool isEnabled;
    public GameObject cameraObject;
    float? camObjectHeight;
    Vector3? camRotationOnGround;
    GameObject roofObject;
    //public bool roofSemaphore;
    //private int fingerID = -1;
    CameraMode currentCameraMode = CameraMode.FreeMove;
    Rigidbody rigidBody;
    TouchManCamera tmCamera;
    Player69FPSVerticalCam verticalCamera;
    //public Vector2 moveSpeed;
    //public Vector2 speedModiferKeyboard;
    //public Vector2 speedModiferMouse;
    //public Vector2 speedModiferTouch;
    //public Vector2 inputThreshhold;

    private Vector3 OldPos;
    //public Camera myCamera;
    Color clr;

    IFPSPlayer fpsPlayerMove;

    //GameObject currentSegment;
    //[Header("Spawning")]
    //bool _isSpawnRotationActive;
    //float speedDampingLookAt = 100;



    private void Awake()
    {
        //#if !UNITY_EDITOR
        //             fingerID = 0; 
        //#endif
    }
    // Start is called before the first frame update
    void Start()
    {
        //Fetch the AudioSource from the GameObject
        //m_MyAudioSource = GetComponent<AudioSource>();

        rigidBody = GetComponent<Rigidbody>();
        tmCamera = GetComponent<TouchManCamera>();
        verticalCamera = GetComponent<Player69FPSVerticalCam>();

        fpsPlayerMove = new IFPSPlayer();

        isEnabled = true;

        /*Messenger<IDebugFPSSpeed>.Broadcast(StaticFunctionCalls.DEBUG_FPS_VALUE_SET, new IDebugFPSSpeed
        {
            movementSpeed = speedModiferTouch.y,
            rotationSpeed = speedModiferTouch.x,
            thresholdX = inputThreshhold.x,
            thresholdY = inputThreshhold.y
        });
        */
        DOTween.Init();
        GetOldPosition();

    }

    private void GetOldPosition()
    {
        OldPos = transform.position;
    }

    #region camera mode manager
    public void SwitchCameraMode(CameraMode camMode)
    {
        //Debug.Log("I was called");
        ResetCameras();
        if (!(camMode == CameraMode.Disabled))
            currentCameraMode = camMode;
        CameraHeightManager(currentCameraMode);
        CameraAngleManager(currentCameraMode);
        switch (camMode)
        {
            case CameraMode.FreeMove:
                tmCamera.enabled = true;
                RoofManager(true);
                break;
            case CameraMode.TopView:
                verticalCamera.enabled = true;
                RoofManager(false);
                //verticalCamera.TopViewSetup();
                break;
            case CameraMode.Disabled:   //Disable all, do not change currentCameraMode
                break;
        }
    }
    void ResetCameras()
    {
        tmCamera.enabled = false;
        verticalCamera.enabled = false;
    }

    void CameraAngleManager(CameraMode camMode)
    {
        switch (camMode)
        {
            case CameraMode.FreeMove:
                if (camRotationOnGround == null) return;
                cameraObject.transform.localEulerAngles = (Vector3)camRotationOnGround;//+= new Vector3(0, height, 0);
                camRotationOnGround = null;
                break;
            case CameraMode.TopView:
                if(camRotationOnGround == null)camRotationOnGround = cameraObject.transform.localEulerAngles;
                cameraObject.transform.localEulerAngles = Vector3.zero;
                break;
        }
    }
    void CameraHeightManager(CameraMode camMode)
    {
        switch (camMode)
        {
            case CameraMode.FreeMove:
                //float height;
                if (camObjectHeight == null) return;
                //float.TryParse(camObjectHeight.ToString(), out height);
                cameraObject.transform.localPosition += new Vector3(0, (float) camObjectHeight, 0);
                camObjectHeight = null;
                break;
            case CameraMode.TopView:
                if (camObjectHeight == null) camObjectHeight = cameraObject.transform.localPosition.y;
                cameraObject.transform.localPosition = Vector3.zero;
                break;
        }
    }

    void RoofManager(bool isOn)
    {
        if (roofObject == null) roofObject = GameObject.FindGameObjectWithTag("Roof");
        if (roofObject == null) return; //if still not found
        roofObject.SetActive(isOn);
    }
    #endregion



    /*void FixedUpdate()
    {
        if (isEnabled && !EventSystem.current.IsPointerOverGameObject(fingerID))
        {
            Movement(false);
            // MultiMovement();
            ResetMovement();
        }
    }*/

    private void OnEnable()
    {
        //Messenger<GameObject>.AddListener("ACTOR_SPAWN", SpawnActor);
        //Messenger<IDebugFPSSpeed>.AddListener(StaticFunctionCalls.DEBUG_FPS_VALUE, ON_DEBUG_FPS_VALUE);
        Messenger<Transform>.AddListener(StaticFunctionCalls.PlayerFPSLookAt, ReachAWallToPaint);
        //Messenger<Transform>.AddListener(StaticFunctionCalls.PlayerFPSLookCenter, LookAtAndTranslate);

    }

    private void OnDisable()
    {
        //Messenger<GameObject>.RemoveListener("ACTOR_SPAWN", SpawnActor);
        //Messenger<IDebugFPSSpeed>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_VALUE, ON_DEBUG_FPS_VALUE);
        Messenger<Transform>.RemoveListener(StaticFunctionCalls.PlayerFPSLookAt, ReachAWallToPaint);
        //Messenger<Transform>.RemoveListener(StaticFunctionCalls.PlayerFPSLookCenter, LookAtAndTranslate);
    }


    /*private void OnMouseUp()
    {
        ResetMotion();
    }

    void OnTouchUp()
    {
        ResetMotion();
    }*/

    #region DEBUG SPEED
    /*void ON_DEBUG_FPS_VALUE(IDebugFPSSpeed val)
    {
        speedModiferTouch.x = val.rotationSpeed;
        speedModiferTouch.y = val.movementSpeed;
        inputThreshhold.x = val.thresholdX;
        inputThreshhold.y = val.thresholdY;
        Messenger<IDebugFPSSpeed>.Broadcast(StaticFunctionCalls.DEBUG_FPS_VALUE_SET, new IDebugFPSSpeed
        {
            movementSpeed = speedModiferTouch.y,
            rotationSpeed = speedModiferTouch.x,
            thresholdX = inputThreshhold.x,
            thresholdY = inputThreshhold.y
        });
    }*/


    /*void DEBUG_FPS_movementSpeed(float val)
    {
        speedModiferTouch.x = val;
    }

    void DEBUG_FPS_rotationSpeed(float val)
    {
        speedModiferTouch.y = val;
    }

    void DEBUG_FPS_thresholdX(float val)
    {
        inputThreshhold.x = val;
    }

    void DEBUG_FPS_thresholdY(float val)
    {
        inputThreshhold.y = val;
    }*/

    #endregion

    /*public void SetTouchGestureLayerMode()
    {

    }*/

    public void Spawn(GameObject room)
    {
        var combinedBounds = new Bounds();
        var renderers = GetComponentsInChildren<Renderer>();

        foreach (Renderer render in renderers)
        {
            combinedBounds.Encapsulate(render.bounds);
        }

        Vector3 pos = transform.position;
        pos.x = combinedBounds.center.x;
        pos.z = combinedBounds.center.z;
        transform.position = pos;

    }

    public void UpdateMovement(bool enable)
    {
        isEnabled = enable;
        //tmCamera.enabled = enable;
        CameraMode requestMode = enable ? currentCameraMode : CameraMode.Disabled;
        SwitchCameraMode(requestMode);
    }

    //Deprycated 
    /*void Movement(bool useKeyboard = false)
    {
        Debug.LogError("I am stupid and I shouldn't have been called");
        //m_MyAudioSource.Play();
        //m_MyAudioSource.SetScheduledEndTime(AudioSettings.dspTime + (1f - 2f));

        Vector2 moveValue = Vector2.zero;
        Vector2 panValue = Vector2.zero;

        //Use Keyboard Nav
        if (Input.touchCount == 1)
        {
            moveValue = new Vector2(Input.touches[0].deltaPosition.x, Input.touches[0].deltaPosition.y);

            //moveValue.x *= speedModiferTouch.x;
            //moveValue.y *= speedModiferTouch.y;



        }
        else if (Input.touchCount > 1)
        {
            panValue = new Vector2(Input.touches[1].deltaPosition.x, Input.touches[1].deltaPosition.y);

            //panValue.x *= speedModiferTouch.x;
            //panValue.y *= speedModiferTouch.y;
        }
        else if (Input.GetMouseButton(0))
        {
            moveValue = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            //moveValue.x *= speedModiferMouse.x;
            //moveValue.y *= speedModiferMouse.y;
        }
        else if (Input.GetMouseButton(1))
        {
            panValue = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            //panValue.x *= speedModiferMouse.x;
            //panValue.y *= speedModiferMouse.y;
        }
        else if (useKeyboard)
        {
            //moveValue = new Vector2(Input.GetAxis("Horizontal") * speedModiferKeyboard.x, Input.GetAxis("Vertical") * speedModiferKeyboard.y);

            if (Input.GetKeyDown(KeyCode.Q))
                panValue.x = -1;

            if (Input.GetKeyDown(KeyCode.E))
                panValue.x = 1;
        }
        //rigidBody.velocity = ((transform.forward * moveValue.y * moveSpeed.y) + (transform.right * panValue.x * moveSpeed.y)) * Time.deltaTime;

        //transform.Rotate(transform.up * moveValue.x * Time.deltaTime * moveSpeed.x, Space.World);
    }*/


    /*void ResetMovement()
    {
        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
            fpsPlayerMove.Reset();
    }*/



    /*void ResetMotion()
    {
        Debug.LogError("Who called this deprecated function?");
        fpsPlayerMove.Reset();
        rigidBody.velocity = Vector3.zero;
        transform.Rotate(Vector3.zero);
    }*/




    /*void SpawnActor(GameObject go)
    {
        var pos = (Vector3)go.transform.GetCenter();
        SpawnActorPosition(pos);
        Debug.LogError("SpawnActor called");
    }*/

    public void SpawnActorPosition(Vector3 pos, Vector3? rotation = null)
    {

        //print("Spawn at: " + pos.ToString());
        //pos.y = transform.localPosition.y < 0 ? .5f : transform.localPosition.y;

        if (rotation != null)
            transform.rotation = Quaternion.Euler((Vector3)rotation);

        //Debug.Log("Setting pos to:" + pos);

        transform.position = pos;
        // transform.rotation = Quaternion.identity;

    }

    public void SpawnActorTransform(ITransform obj)
    {
        //Debug.Log("Relocating player 5/5");
        transform.position = obj.position;
        transform.eulerAngles = new Vector3(0, obj.rotation.y, 0);
    }

    public void OnTriggerEnter(Collider coll)
    {
        if (Regex.IsMatch(coll.gameObject.name, "Column_", RegexOptions.IgnoreCase))
        {

            clr = coll.gameObject.transform.GetComponent<Renderer>().material.color;
            coll.gameObject.transform.GetComponent<Renderer>().material.color = Color.red;
            Handheld.Vibrate();
        }
    }

    public void OnTriggerExit(Collider coll)
    {
        if (Regex.IsMatch(coll.gameObject.name, "Column_", RegexOptions.IgnoreCase))
        {
            coll.gameObject.transform.GetComponent<Renderer>().material.color = clr;
        }

        if (coll.tag == "OutofBoundCheck")
        {
            if (verticalCamera.enabled) return; //SOS
            transform.position = OldPos;
            Debug.Log("Player Dieded");
        }
    }



    #region Spawn Management

    public void LookAt(Transform trans)
    {
        //print(trans.name);
        transform.DOLookAt((Vector3)trans.GetCenter(), .7f, AxisConstraint.Y);
    }


    public void LookAtAndTranslate(Transform target)
    {


        float distanceMultiplier = 1.8f;
        LookAt(target);

        Vector3 vec = (Vector3)target.GetCenter();
        vec.y = transform.position.y;
        Vector3 scale = target.GetRealScale();

        //print("scale: " + scale);
        //print("center: " + vec);

        if (scale.x > scale.z)
        {
            vec.z -= Mathf.Sign(vec.z) * distanceMultiplier;
        }
        else
        {
            vec.x -= Mathf.Sign(vec.x) * distanceMultiplier;
        }

        transform.position = vec;
    }

    public void ReachAWallToPaint(Transform target)
    {
        Vector3 vec = (Vector3)target.GetCenter();
        vec.y = transform.position.y;
        //vec.x = -1 * vec.x; //no more required :)
        Vector3 scale = target.GetRealScale();

        if (Mathf.Abs(scale.x) > Mathf.Abs(scale.z))
        {
            var weight = (Mathf.Abs(scale.x) > Mathf.Abs(scale.y)) ? Mathf.Abs(scale.x) / 2.55f : Mathf.Abs(scale.y) / 1.55f;
            if (transform.position.z > vec.z)
                vec.z = vec.z + Mathf.Abs(weight);
            else
                vec.z = vec.z - Mathf.Abs(weight);
        }
        else
        {
            var weight = (Mathf.Abs(scale.z) > Mathf.Abs(scale.y)) ? Mathf.Abs(scale.z) / 2.55f : Mathf.Abs(scale.y) / 1.55f;
            if (transform.position.x > vec.x)
                vec.x = vec.x + Mathf.Abs(weight);
            else
                vec.x = vec.x - Mathf.Abs(weight);
        }

        transform.DOMove((Vector3)vec, .7f);

        var center = (Vector3)target.GetCenter();
        //center.x = -1 * center.x; //no more required :)
        Vector3 rot = GetLookAtEuler(center, vec);
        rot.x = rot.z = 0f;
        transform.DORotate(rot, .7f);
    }

    public Vector3 GetLookAtEuler(Vector3 whereToLook, Vector3 currentPos)
    {
        return Quaternion.LookRotation((whereToLook - currentPos), Vector3.up).eulerAngles;


        //var axisOfRotation = Vector3.Cross(Vector3.Normalize(whereToLook), Vector3.Normalize(currentPos));
        //var angle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(whereToLook, currentPos));
        //Debug.Log("ou hvae to rotate " + angle + " Degrees");
        //return new Vector3(currentPos.x, currentPos.y - angle, currentPos.z);
    }

    public void SetActorTransform(ITransform trans)
    {
        transform.DOMove(trans.position, .4f);
        transform.DORotate(trans.rotation, .4f);
    }

    #endregion





    [System.Serializable]
    class IFPSPlayer
    {
        public float moveHorizontal;
        public float moveVertical;

        public float lookHorizontal;
        public float lookVertical;


        public void Reset()
        {
            moveHorizontal = moveVertical = lookHorizontal = lookVertical = 0;
        }
    }

}

public enum CameraMode
{
    FreeMove,
    TopView,
    Disabled
}