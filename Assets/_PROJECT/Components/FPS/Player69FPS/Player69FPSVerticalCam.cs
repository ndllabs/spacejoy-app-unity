﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchMan;
using Homefuly;

[RequireComponent(typeof(Rigidbody))]
public class Player69FPSVerticalCam : MonoBehaviour
{
    public new Camera camera;
    public Bounds roomBounds;
    public bool canMove = true;
    float camHeight;
    private Rigidbody rigidBody;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        //ResetCamera();
        TouchManEvents.OnPinch += PinchCam;
        //TouchManEvents.OnTwoFingerPan += OnTwoFingerPan;
        //TouchManEvents.OnTwoFingerStationary += OnTwoFingerStationary;
        TouchManEvents.OnEndTouch += OnEndTouch;
        TouchManEvents.OnBeginDrag += OnBeginDrag;
        TouchManEvents.OnDrag += OnDrag;
        roomBounds = AppComponent.Instance.db.currentRoom.transform.GetTotalRenderBoundsOf();
    }

    private void OnDisable()
    {
        TouchManEvents.OnPinch -= PinchCam;
        //TouchManEvents.OnTwoFingerPan -= OnTwoFingerPan;
        //TouchManEvents.OnTwoFingerStationary -= OnTwoFingerStationary;
        TouchManEvents.OnEndTouch -= OnEndTouch;
        TouchManEvents.OnBeginDrag -= OnBeginDrag;
        TouchManEvents.OnDrag -= OnDrag;
        //camera.transform.localPosition = new Vector3(0, camHeight, 0);
    }
    /*public void TopViewSetup()
    {
        camHeight = camera.transform.localPosition.y;
        camera.transform.localPosition = new Vector3(0, 0, 0);
    }*/
    /*void ResetCamera()
    {
        camera.transform.eulerAngles = new Vector3(90, 0, 0);   //set rotation to look down
    }*/

    private void PinchCam(float TouchDistanceDelta, Touch touch0, Touch touch1)
    {
        Debug.Log("[Vertical Cam] pinch feature not implemented yet");
        //if (!canMove) return;
        //MoveCamera_Deprycated((TouchDistanceDelta * (Ts._advanced.MultiplyerTwoFingerGestures / 6)) * Ts._speedModiferTouch, Vector2.zero);
    }

    private void OnBeginDrag(GameObject dragStartedOver, Vector2 touchPosition)
    {
        //Debug.Log("[Vertical Cam] You are now using vertical cam movement!");   //SOS delete this

        if (dragStartedOver && !dragStartedOver.GetComponent<Collider>().isTrigger &&
            dragStartedOver.layer.IsOneOf(TouchManLayers._SelectableObjectsLayerList))
            canMove = false;
        else
            canMove = true;
    }

    private void OnDrag(GameObject dragStartedOver, Vector2 touchPosition, Vector2 touchDeltaPosition)
    {
        if (!canMove) return;

        /* Move Camera forward and back, if one finger up down
         * */
        MoveCameraTopView(touchDeltaPosition * TouchManSettings._speedModiferTouch);//, Vector2.zero);

        /* Rotate Camera forward and back, if one finger right left
         * */
        //RotateCamera(new Vector2(0, touchDeltaPosition.x * Ts._speedModiferTouch.x));
    }

    private void OnEndTouch()
    {
        canMove = true;

        //Break Movement
        rigidBody.velocity = Vector3.zero;
    }

    #region Core
    Vector3 positionModifier;
    void MoveCameraTopView(Vector2 moveForwardAmount)//, Vector2 moveRightAmount)
    {
        if (!canMove) return;
        positionModifier=transform.position+ ((Vector3.forward * moveForwardAmount.y ) + (Vector3.right * moveForwardAmount.x )) * Time.deltaTime;
        var checkModifier = new Vector3(positionModifier.x,roomBounds.center.y, positionModifier.z);
        if (roomBounds.Contains(checkModifier))
            transform.position = positionModifier;
        //else
            //Debug.LogWarning("[Vertical Cam] Cant go out of bounds. Current update position be like "+positionModifier+"\nBounds be like"+roomBounds.extents);
        //rigidBody.velocity = ((Vector3.forward * moveForwardAmount.y * TouchManSettings._advanced.moveSpeed.y) + (Vector3.right * moveForwardAmount.x * TouchManSettings._advanced.moveSpeed.x)) * Time.deltaTime;
    }

    #endregion

}
