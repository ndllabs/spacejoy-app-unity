﻿using _unity.Events;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;



public class PostProcessor : MonoBehaviour
{
    //public PostProcessingProfile postProcessing;
    //public GameObject c;
    //public InputField radius;
    //public Slider intensitty;
    //public Dropdown dd,ddAA;
    //public Text intensity_value;
    //public Button hide;
    //public bool hidden = false;
    //public Toggle toggle;
    //List<string> options = new List<string> { "Lowest", "Low", "Medium", "High", "Ultra" };
    //List<string> optionsAA = new List<string> { "None", "FAA", "SMAA", "TAA" };

    //GameObject point, directional;
    public PostProcessProfile postProcess;
    //AmbientOcclusion am;
    PostProcessLayer ppl;


    private void OnEnable()
    {
        Messenger<int>.AddListener(StaticFunctionCalls.PostProcessSettings, ExternalSetter);


    }

    private void OnDisable()
    {
        Messenger<int>.RemoveListener(StaticFunctionCalls.PostProcessSettings, ExternalSetter);
    }

    private void Start()
    {

        //am = postProcess.GetSetting<AmbientOcclusion>();
        ppl = gameObject.GetComponent<PostProcessLayer>();
        ExternalSetter(QualitySettings.GetQualityLevel());  //Setting postProcess settings at runtime  

        //directional = GameObject.Find("Directional Light");
        //point = GameObject.Find("elemAmbientLight");

        //directional.SetActive(false);

        //intensitty.value = am.intensity.value;
        //radius.text = am.radius.value.ToString();
        //dd.ClearOptions();
        //dd.AddOptions(options);
        //ddAA.ClearOptions();
        //ddAA.AddOptions(optionsAA);

    }


    #region TestFunctions
    //public void ChooseLightSource()
    //{
    //    if (point == null)
    //    point = GameObject.Find("elemAmbientLight");
    //    switch (toggle.isOn)
    //    {
    //        case true:
    //           directional.SetActive(true);
    //            point.SetActive(false);
    //            toggle.transform.Find("Label").GetComponent<Text>().text = "Directional";
    //            break;

    //        case false:
    //            directional.SetActive(false);
    //            point.SetActive(true);
    //            toggle.transform.Find("Label").GetComponent<Text>().text = "Point";
    //            break;

    //    }
    //}

    //public void ChangeIntensity()
    //{
    //    am.intensity.value = intensitty.value;
    //    intensity_value.text = intensitty.value.ToString();
    //}

    //public void ChangeRadius()
    //{
    //    float.TryParse(radius.text,out am.radius.value);
    //}

    //public void ChangeDropDownValue()
    //{ 
    //switch(dd.value)
    //    {
    //        case 0:
    //            am.quality.value = AmbientOcclusionQuality.Lowest;
    //            break;

    //        case 1:
    //            am.quality.value = AmbientOcclusionQuality.Low;
    //            break;

    //        case 2:
    //            am.quality.value = AmbientOcclusionQuality.Medium;
    //            break;

    //        case 3:
    //            am.quality.value = AmbientOcclusionQuality.High;
    //            break;

    //        case 4:
    //            am.quality.value = AmbientOcclusionQuality.Ultra;
    //            break;


    //    }
    //}


    //public void ChangeAADropDownValue()
    //{
    //    switch (ddAA.value)
    //    {
    //        case 0:
    //            ppl.antialiasingMode = PostProcessLayer.Antialiasing.None;
    //            break;

    //        case 1:
    //            ppl.antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
    //            break;

    //        case 2:
    //            ppl.antialiasingMode = PostProcessLayer.Antialiasing.SubpixelMorphologicalAntialiasing;
    //            break;

    //        case 3:
    //            ppl.antialiasingMode = PostProcessLayer.Antialiasing.TemporalAntialiasing;
    //            break;



    //    }
    //}

    //public void hideUI()
    //{

    //    c.gameObject.SetActive(hidden);
    //    hidden = !hidden;
    //}


    #endregion
    public void ExternalSetter(int g)//GraphicSettingPresets g)
    {
        #region comments
        //if (point == null)
        //{
        //    point = GameObject.Find("elemAmbientLight");

        //}
        //switch (g)
        //{

        //    case GraphicSettingPresets.low:
        //        point.SetActive(true);
        //        am.radius.value = 0.0f;
        //        am.intensity.value = 0;
        //        am.quality.value = 0;
        //        ppl.antialiasingMode = PostProcessLayer.Antialiasing.None;
        //        break;

        //    case GraphicSettingPresets.medium:

        //        point.SetActive(true);
        //        am.radius.value = 0.0f;
        //        am.intensity.value = 0;
        //        am.quality.value = 0;
        //        ppl.antialiasingMode = PostProcessLayer.Antialiasing.None;
        //        break;

        //    case GraphicSettingPresets.high:
        //        point.SetActive(true);
        //        am.radius.value = 1.1f;
        //        am.intensity.value = 0.5f;
        //        am.quality.value = AmbientOcclusionQuality.Lowest;
        //        ppl.antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
        //        break;

        //}

        #endregion

        switch (g)
        {
            case 0://GraphicSettingPresets.low:
                //point.SetActive(true);
                //am.active = false;
                //am.radius.value = 0.0f;
                //am.intensity.value = 0;
                //am.quality.value = 0;
                ppl.antialiasingMode = PostProcessLayer.Antialiasing.None;
                break;

            case 2:// GraphicSettingPresets.medium:
                //point.SetActive(true);
                //am.active = false;
                //am.radius.value = 0.0f;
                //am.intensity.value = 0;
                //am.quality.value = 0;
                ppl.antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
                ppl.fastApproximateAntialiasing.fastMode = true;
                break;

            case 4:// GraphicSettingPresets.high:
                ////point.SetActive(true);
                //am.active = true;
                //am.radius.value = 0.18f;
                //am.intensity.value = 0.18f;
                //am.quality.value = AmbientOcclusionQuality.Lowest;
                ppl.antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
                ppl.fastApproximateAntialiasing.fastMode = true;
                break;

            default:
                Debug.LogWarning("Postprocessing encountered unexpected graphic level, setting postprocess settings to low");
                ExternalSetter(0);
                break;

        }
    }

}
