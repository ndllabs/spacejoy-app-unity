﻿/* Analytics 2.0, smarter and more efficient module that the later.
 * 
 * Low key on memory and computaional resources requirement
 * Stays fully offline when not in use
 * On user opts-in the data sent can be anonymous 
 * 
 * Note: both ios and android should have similar version system
 * 
 * prajwalshetty2018@gmail.com
 * */

using _unity;
using _unity.Events;
using System.Collections.Generic;
using UnityEngine;


namespace Homefuly.Analytics
{
    using Homefuly.Events;
    using System;
    using UnityEngine.Analytics;
    using APN = AnalyticsParamNames;
    using AEN = AnalyticsCustomEventNames;
    using SFC = StaticFunctionCalls;

    public class HomefulyAnalytics : Singleton<HomefulyAnalytics>
    {
        public string UUID { get; private set; }
        bool isInit = false;

        //bool beAnonymous = false;

        public bool IsAnalyticsOnline { get; private set; }
        #region Variables

        #region DataChunks
        Dictionary<string, object> DefaultParams = new Dictionary<string, object> {
            { APN.customer, "" }, { APN.appversion, "" }, { APN.region, "" } };

        Dictionary<string, object> CE_ActionFailed = new Dictionary<string, object> {
            { APN.actiontype, "" }, { APN.errorcode, -1 }};

        Dictionary<string, object> CE_SceneClosed = new Dictionary<string, object>();

        #endregion


        #endregion


        // Start is called before the first frame update
        void Init()
        {
            if (isInit) return;

            UUID = SystemInfo.deviceUniqueIdentifier;
            isInit = false;

            if (ShouldEnableAnalytics())
                EnableAnalytics();
        }

        void UpdateDefaultParameters()
        {

        }


        void EnableAnalytics()
        {
            #region UserBehaviour

            #region SceneClose

            #endregion

            #endregion


            #region Feature_Insights

            #region Swap
            Messenger<IHotspot>.AddListener(SFC.Action_HotspotSwapped, WrapperSwap_Hotspot);
            Messenger<IAssetEntry>.AddListener(SFC.Action_AssetSwapped, WrapperSwap_Asset);
            Messenger<IWallfinishHotspot>.AddListener(SFC.Action_WallfinishSwapped, WrapperSwap_WallfinishHotspot);
            #endregion

            #region ProductLoad
            Messenger<string>.AddListener(AppEvents.ASSET_DROP_COMPLETE, WrapperLoad_Asset);
            Messenger<IWallfinish, GameObject>.AddListener(SFC.DropWallfinishInScene, WrapperLoad_Wallfinish);
            #endregion

            #region Shop
            Messenger<IAsset, AppPage, string>.AddListener(SFC.ShopButtonClicked, WrapperShop_Legacy);
            HomefulyAnalyticsEvent.OnShopClicked += WrapperShop_AnalyticsEvent;
            #endregion

            #endregion


            #region Error_Reports
            HomefulyEvents.OnActionFailed += CustomEvent_ActionFailed;
            #endregion

            #region UI_Interactions
            Messenger<AppPage>.AddListener(AnalyticsCalls.ProductListOpened, WrapperUIClik_EstimatePanel); //Click for estimate
            Messenger<PanelModes>.AddListener(SFC.openHUDPanel, WrapperUIClik_OpenHUDPanel); ////Buttons for asset store, paint, wallpaper, estimate, swap, info
            Messenger.AddListener(AnalyticsCalls.ShareButtonClicked, WrapperUIClik_ShareButton); //Share button event



            #endregion
            ////0: How much time is spent in each design
            //Messenger.AddListener(AnalyticsCalls.TimeSpentInDesign, DesignTime);

            ////3. Product Selection in scene
            //Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

            ////5. Drag paint in scene from add paint or wallpaper
            //Messenger<IWallfinish, GameObject>.AddListener(StaticFunctionCalls.DropWallfinishInScene, OnWallfinishDroppedOnScene);

            ////7. Delete from localpanel         //hide functionalitynot found
            //Messenger.AddListener(StaticFunctionCalls.ButtonDelete_Deprycated, AssetDelete);

            ////Extra_2. Save Design completed
            //Messenger<string>.AddListener(AnalyticsCalls.SaveDesignCompleted, OnSaveCompleted);

            ////Extra_3. Open Center with Confirmation result.
            //Messenger<string, bool>.AddListener(AnalyticsCalls.SceneConfirmationResponse, SceneConfirmationResponse);

            ////Button_SaveDesign
            //Messenger<string>.AddListener(StaticFunctionCalls.OnSaveDesign, OnSaveButton);

            ////Time took to load design
            //Messenger<long>.AddListener(AnalyticsCalls.DesignLoadTime, DesignLoadTime);


            IsAnalyticsOnline = true;
        }

        private void WrapperUIClik_ShareButton()
        {
            throw new NotImplementedException();
        }

        private void WrapperUIClik_OpenHUDPanel(PanelModes obj)
        {
            throw new NotImplementedException();
        }

        private void WrapperUIClik_EstimatePanel(AppPage obj)
        {
            throw new NotImplementedException();
        }

        private void WrapperLoad_Wallfinish(IWallfinish arg1, GameObject arg2)
        {
            throw new NotImplementedException();
        }

        private void WrapperShop_Legacy(IAsset arg1, AppPage arg2, string arg3)
        {
            throw new NotImplementedException();
        }

        private void WrapperShop_AnalyticsEvent(IAsset asset, ShopButtonType buttonType)
        {
            throw new NotImplementedException();
        }

        private void WrapperLoad_Asset(string obj)
        {
            throw new NotImplementedException();
        }

        private void WrapperSwap_WallfinishHotspot(IWallfinishHotspot obj)
        {
            throw new NotImplementedException();
        }

        private void WrapperSwap_Hotspot(IHotspot obj)
        {
            throw new NotImplementedException();
        }

        private void WrapperSwap_Asset(IAssetEntry obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// returns true if <see cref="AnalyticsManager"/> should be recording
        /// </summary>
        /// <returns></returns>
        public bool ShouldEnableAnalytics()
        {
            var acc = AppComponent.Instance.account;

            if (Debug.isDebugBuild ||
                acc.internalTestUser == acc.CRED_USERNAME ||
                !isInit)
            {
                Debug.LogWarning("[" + nameof(HomefulyAnalytics) + "][" + nameof(ShouldEnableAnalytics) + "] Analytics Offline, Due To: isEditor | isDevelopmentBuild | TestUserLoggedIn | Analytics is Not Init");
                return false;
            }

            return true;
        }

        #region Custom Events

        private void CustomEvent_ActionFailed(ActionType actionType, int errCOde)
        {
            CE_ActionFailed[APN.actiontype] = actionType.ToString();
            CE_ActionFailed[APN.errorcode] = -1;

            AnalyticsEvent.Custom(AEN.ActionFailed, CE_ActionFailed);
        }
        #endregion
    }

    [SerializeField]
    public class AnalyticsParamNames
    {
        public const string customer = "customer";
        public const string region = "region";
        public const string appversion = "appversion";

        public const string actiontype = "actiontype";
        public const string errorcode = "errorcode";
    }

    [SerializeField]
    public class AnalyticsCustomEventNames
    {
        public const string ActionFailed = "ActionFailed";
    }

    public class HomefulyAnalyticsEvent
    {

        public delegate void DesignClosed(SceneType scene, float totalSessionTime);
        public static DesignClosed OnDesignClosed;

        public delegate void ShopClicked(IAsset asset, ShopButtonType buttonType);
        public static ShopClicked OnShopClicked;

        public delegate void Response_ChallengeComplete(ChallengeComplete_Behaviour buttonType);
        public static Response_ChallengeComplete OnChallengeComplete;

        public delegate void DesignOpened(SceneType sceneType);
        public static DesignOpened OnDesignOpened;

        public delegate void ExpertHelpPressed();
        public static ExpertHelpPressed OnExpertHelpPressed;

        public delegate void ExpertHelpImgUpload();
        public static ExpertHelpImgUpload OnExpertHelpImgUpload;

        public delegate void ExperHelpSubmitted();
        public static ExperHelpSubmitted OnExpertHelpSubmitted;
    }

    public enum ShopButtonType
    {
        estimate_panel,
        asset_options,
        asset_store,
        swap_panel,
        hotspot_swap,
        callback_webui,
        legacy,
    }


    public enum ChallengeComplete_Behaviour
    {
        CollectRewards_Clicked,
        CheckAnim_Shown,
        NextChallenge_Clicked,
        Home_Clicked,
    }
}
