﻿using _unity;
using _unity.Events;
using Homefuly;
using Homefuly.Analytics;
using Homefuly.Database;
using Homefuly.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using static Homefuly.Analytics.HomefulyAnalyticsEvent;
//using Firebase.Analytics;

//To Put in center script:
//center switch between project tabs
//on edit press - design which is selected id/name, and response of yes/no
//seperate product list clicked in center scene and design scene - provide both
public class AnalyticsManager : Singleton<AnalyticsManager>
{
    string UUID;
    //SceneType stype = SceneType.None;
    string currentScene = string.Empty;
    Dictionary<string, ICategory> categories = new Dictionary<string, ICategory>();

    public void Init()
    {
        
    }



    private void OnEnable()
    {

        UUID = SystemInfo.deviceUniqueIdentifier;

        //overall time spent in app     //is tracked in Core Events automatically

        //DAU                           //is tracked in Core Events automatically

        //0: How much time is spent in each design
        Messenger<SceneType>.AddListener(AnalyticsCalls.SceneClosed, OnSceneClosed);

        //1: How many swaps are happening
        Messenger<IAsset>.AddListener(AppEvents.OnSwapCurrentAsset, OnSwapCurrentAsset);

        //2. How many users are clicking shop button: which link is clicked?
        Messenger<IAsset, AppPage, string>.AddListener(StaticFunctionCalls.ShopButtonClicked, ShopButton);

        //3. Product Selection in scene
        Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

        //4. Drag product in scene from add product
        Messenger<IAsset, Vector3, GameObject>.AddListener(AppEvents.ASSET_DROPPED_INSIDE_ROOM, DropAssetInsideRoom);//asset, hitCenter.point, hitCenter.collider.gameObject);
        //Messenger<bool>.Broadcast(StaticFunctionCalls.OnDragAssetFromStore, true);

        //5. Drag paint in scene from add paint or wallpaper
        Messenger<IWallfinish, GameObject>.AddListener(StaticFunctionCalls.DropWallfinishInScene, OnWallfinishDroppedOnScene);

        //6. View details clicked for estimate
        Messenger<AppPage>.AddListener(AnalyticsCalls.ProductListOpened, ProductListOpened);

        //7. Delete from localpanel         //hide functionalitynot found
        Messenger.AddListener(StaticFunctionCalls.ButtonDelete_Deprycated, AssetDelete);


        //8. Unhide all
        //functionality not found

        //9. Screenshot
        //functionality not found

        //10. any views clicked (doesnt matter which view)
        //functionality not found

        //Extra_1. Save viewpoint
        Messenger.AddListener(StaticFunctionCalls.TriggerPositionSave, AddRenderPosition);

        //Extra_2. Save Design completed
        Messenger<string>.AddListener(AnalyticsCalls.SaveDesignCompleted, OnSaveCompleted);

        //Extra_3. Open Center with Confirmation result.
        Messenger<string, bool>.AddListener(AnalyticsCalls.SceneConfirmationResponse, SceneConfirmationResponse);

        //Extra_4. Design tab switched in project scene
        Messenger<string>.AddListener(AnalyticsCalls.CenterTabSwitch, CenterTabSwitch);

        //Button_SaveDesign
        Messenger<string>.AddListener(StaticFunctionCalls.OnSaveDesign, OnSaveButton);

        //Buttons for asset store, paint, wallpaper, estimate, swap, info
        Messenger<PanelModes>.AddListener(StaticFunctionCalls.openHUDPanel, HUDPanelButtons);

        //Time took to load design
        Messenger<long>.AddListener(AnalyticsCalls.DesignLoadTime, DesignLoadTime);

        //Tutorial events
        Messenger<string>.AddListener(AnalyticsCalls.TutorialChapterStarted, TutorialChapterStarted);
        Messenger<string>.AddListener(AnalyticsCalls.TutorialChapterComplete, TutorialChapterCompleted);
        Messenger<string>.AddListener(AnalyticsCalls.TutorialSkipped, TutorialSkipped);
        Messenger<string>.AddListener(AnalyticsCalls.TutorialCompleted, TutorialCompleted);

        //Share button event
        Messenger.AddListener(AnalyticsCalls.ShareButtonClicked, ShareButtonClicked);


        Messenger<string>.AddListener(StaticFunctionCalls.Action_DesignSubmited, OnDesignChallengeComplete);
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, OnSwapHotspot);
        Messenger<IWallfinishHotspot>.AddListener(StaticFunctionCalls.Action_WallfinishSwapped, OnSwapHotspotWallfinish);
        HomefulyEvents.OnActionFailed += CustomEvent_ActionFailed;
        HomefulyEvents.OnAuthUpdated += OnAuthUpdated;
        SceneManager.sceneLoaded += OnSceneChanged;
        // AppDatabase.OnDBReady += OnDBReady;
        HomefulyAnalyticsEvent.OnChallengeComplete += OnChallengeComplete;
        HomefulyAnalyticsEvent.OnDesignOpened += Event_DesignOpened;
        HomefulyAnalyticsEvent.OnShopClicked += Event_ShopClicked;
        HomefulyAnalyticsEvent.OnExpertHelpPressed += Event_OnExpertHelp_Pressed;
        HomefulyAnalyticsEvent.OnExpertHelpImgUpload += Event_OnExpertHelp_ImgUpload;
        HomefulyAnalyticsEvent.OnExpertHelpSubmitted += Event_OnExpertHelp_Submitted;
    }


    private void OnSceneChanged(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Designer")
            currentScene = AppComponent.Instance.CurrentSceneType.ToString();
        else
            currentScene = scene.name;
    }

    private void OnDBReady()
    {
        categories = AppComponent.Instance.db.categories;
    }

    private void OnDisable()
    {
        //DAL is tracked automatically

        //0: How much time is spent in each design
        Messenger<SceneType>.RemoveListener(AnalyticsCalls.SceneClosed, OnSceneClosed);

        //1: How many swaps are happening
        Messenger<IAsset>.RemoveListener(AppEvents.OnSwapCurrentAsset, OnSwapCurrentAsset);

        //2. How many users are clicking shop button: which link is clicked?
        Messenger<IAsset, AppPage, string>.RemoveListener(StaticFunctionCalls.ShopButtonClicked, ShopButton);

        //3. Product Selection in scene
        Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

        //4. Drag product in scene from add product
        Messenger<IAsset, Vector3, GameObject>.RemoveListener(AppEvents.ASSET_DROPPED_INSIDE_ROOM, DropAssetInsideRoom);//asset, hitCenter.point, hitCenter.collider.gameObject);
        //Messenger<bool>.Broadcast(StaticFunctionCalls.OnDragAssetFromStore, true);

        //5. Drag paint in scene from add paint or wallpaper
        Messenger<IWallfinish, GameObject>.RemoveListener(StaticFunctionCalls.DropWallfinishInScene, OnWallfinishDroppedOnScene);

        //6. View details clicked for estimate
        Messenger<AppPage>.RemoveListener(AnalyticsCalls.ProductListOpened, ProductListOpened);

        //7. Delete from localpanel         //hide functionalitynot found
        Messenger.RemoveListener(StaticFunctionCalls.ButtonDelete_Deprycated, AssetDelete);

        //8. Unhide all
        //functionality not found

        //9. Screenshot
        //functionality not found

        //10. any views clicked (doesnt matter which view)
        //functionality not found

        //Extra_1. Save viewpoint
        Messenger.RemoveListener(StaticFunctionCalls.TriggerPositionSave, AddRenderPosition);

        //Extra_2. Save Design
        Messenger<string>.RemoveListener(AnalyticsCalls.SaveDesignCompleted, OnSaveCompleted);

        //Extra_3. Open Center with Confirmation result.
        Messenger<string, bool>.RemoveListener(AnalyticsCalls.SceneConfirmationResponse, SceneConfirmationResponse);

        //Extra_4. Design tab switched in project scene
        Messenger<string>.RemoveListener(AnalyticsCalls.CenterTabSwitch, CenterTabSwitch);

        //Button_SaveDesign
        Messenger<string>.RemoveListener(StaticFunctionCalls.OnSaveDesign, OnSaveButton);

        //Buttons for asset store, paint, wallpaper, estimate, swap, info
        Messenger<PanelModes>.RemoveListener(StaticFunctionCalls.openHUDPanel, HUDPanelButtons);

        //Time took to load design
        Messenger<long>.RemoveListener(AnalyticsCalls.DesignLoadTime, DesignLoadTime);

        //Tutorial events
        Messenger<string>.RemoveListener(AnalyticsCalls.TutorialChapterStarted, TutorialChapterStarted);
        Messenger<string>.RemoveListener(AnalyticsCalls.TutorialChapterComplete, TutorialChapterCompleted);
        Messenger<string>.RemoveListener(AnalyticsCalls.TutorialSkipped, TutorialSkipped);
        Messenger<string>.RemoveListener(AnalyticsCalls.TutorialCompleted, TutorialCompleted);

        //Share button event
        Messenger.RemoveListener(AnalyticsCalls.ShareButtonClicked, ShareButtonClicked);

        Messenger<string>.RemoveListener(StaticFunctionCalls.Action_DesignSubmited, OnDesignChallengeComplete);
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, OnSwapHotspot);
        Messenger<IWallfinishHotspot>.RemoveListener(StaticFunctionCalls.Action_WallfinishSwapped, OnSwapHotspotWallfinish);
        HomefulyEvents.OnActionFailed -= CustomEvent_ActionFailed;
        HomefulyEvents.OnAuthUpdated -= OnAuthUpdated;
        SceneManager.sceneLoaded -= OnSceneChanged;
        // AppDatabase.OnDBReady -= OnDBReady;
        HomefulyAnalyticsEvent.OnChallengeComplete -= OnChallengeComplete;
        HomefulyAnalyticsEvent.OnDesignOpened -= Event_DesignOpened;
        HomefulyAnalyticsEvent.OnShopClicked -= Event_ShopClicked;
        HomefulyAnalyticsEvent.OnExpertHelpPressed -= Event_OnExpertHelp_Pressed;
        HomefulyAnalyticsEvent.OnExpertHelpImgUpload -= Event_OnExpertHelp_ImgUpload;
        HomefulyAnalyticsEvent.OnExpertHelpSubmitted -= Event_OnExpertHelp_Submitted;
    }

    #region OldEvents

    //Records button clicks for multiple panels
    private void HUDPanelButtons(PanelModes obj)
    {
        if (IsAnalyticsOffline()) return;

        string title = "";
        switch (obj)
        {
            case PanelModes.AssetStore:
                title = AnalyticsResources.Button_Clicked_Asset_Store;
                break;

            case PanelModes.Paint:
                title = AnalyticsResources.Button_Clicked_Paint_Store;
                break;

            case PanelModes.Wallpapers:
                title = AnalyticsResources.Button_Clicked_Wallpaper_Store;
                break;

            case PanelModes.ProductList://Estimate panel
                Messenger<AppPage>.Broadcast(AnalyticsCalls.ProductListOpened, AppPage.Designer);
                return;
            //break;

            case PanelModes.Swap:
                title = AnalyticsResources.Button_Clicked_Swap_Store;
                break;

            case PanelModes.Local://product info
                title = AnalyticsResources.Button_Clicked_Product_Info;
                break;
            default: return;
        }
        AnalyticsEvent.Custom(title, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version}
        });
    }

    private void CenterTabSwitch(string obj)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Project_List_Tab_Clicked, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Tab_Name", obj }
        });
    }

    private void SceneConfirmationResponse(string arg1, bool arg2)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Project_Selected, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "RoomVersion_Id", arg1},
            { "SceneOpenedConfirmation", arg2.ToString()}
        });
    }

    private void OnSaveButton(string buttonType = null)
    {
        if (IsAnalyticsOffline()) return;

        // Debug.Log(">>>>>>>>>>>>>> Save Button Clicked: " + buttonType);

        AnalyticsEvent.Custom(AnalyticsResources.Save_Design_Button_Clicked, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
            {"Button Clicked", buttonType}
        });
    }

    private void OnSaveCompleted(string buttonType = null)
    {
        if (IsAnalyticsOffline()) return;

        // Debug.Log(">>>>>>>>>>>>>> Save Completed: " + buttonType);

        AnalyticsEvent.Custom(AnalyticsCalls.SaveDesignCompleted, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
            { "Button Clicked", buttonType}
        });
    }

    private void AddRenderPosition()
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Viewpoint_saved, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()}
        });
    }

    private void AssetDelete()
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Asset_Deleted, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()}
        });
    }

    private void ProductListOpened(AppPage page)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Estimate_Panel_Opened, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Scene_Name", page.ToString()}
        });
    }

    private void OnWallfinishDroppedOnScene(IWallfinish arg1, GameObject arg2)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Wallfinish_Dropped_Inside_Room, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
            { "Wallfinish_Name", arg1.name},
            { "Wallfinish_Type", arg1.wftype}
        });
    }

    private void DropAssetInsideRoom(IAsset arg1, Vector3 arg2, GameObject arg3)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Asset_Dropped_Inside_Room, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
            { "Asset_Name", arg1.name}
        });
    }

    private void OnAssetSelected(IAssetEntry obj)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Asset_Selected, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
            { "Asset_Name", obj.asset.name}
        });
    }

    private void ShopButton(IAsset obj, AppPage page, string origin)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Shop_Clicked, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
            { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Scene", page.ToString()},
            { "Clicked_From",origin },
            { "Shop_Url", obj.designedBy.productUrl}
        });
    }

    private void OnSwapCurrentAsset(IAsset obj)
    {
        Debug.Log("Swapped Hotspots Cat: " + ((categories.ContainsKey(obj.subcategory)) ? categories[obj.subcategory].name : "none"));

        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Asset_Swaped, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
             { "subcategory", (categories.ContainsKey(obj.subcategory)) ? categories[obj.subcategory].name : "none" },
            { "AppVersion", Application.version},
            { "NewSwappedAsset", obj.name}
        });
    }


    private void DesignLoadTime(long miliseconds)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Design_Load_Time, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
            { "design_load_time", miliseconds/1000f } //Time.timeSinceLevelLoad }
        });
        Debug.Log("Design Load time: " + (miliseconds / 1000f) + "\nTime since level load: " + Time.timeSinceLevelLoad);
    }

    //private void DesignTime(SceneType scenetype)
    //{
    //    if (IsAnalyticsOffline()) return;
    //    AnalyticsEvent.Custom(AnalyticsResources.Design_Closed, new Dictionary<string, object>
    //    {
    //        { "customer", AppComponent.Instance.account.CRED_USERID },
    //         { "Unique ID", UUID},
    //        { "AppVersion", Application.version},
    //        { "scene_type", scenetype.ToString()},
    //        { "design_time", Time.timeSinceLevelLoad }
    //    });
    //    Debug.Log("Time Spent in Scene: " + Time.timeSinceLevelLoad);
    //}

    private void OnSceneClosed(SceneType scenetype)
    {
        Debug.Log("Design Close Analytics: \n" + (AnalyticsResources.OnCloseScene + "_" + scenetype.ToString()) + "\nTimeSpent: " + Time.timeSinceLevelLoad);
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.OnCloseScene + "_" + scenetype.ToString().ToLower(), new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "scene_type", scenetype.ToString()},
            { "design_time", Time.timeSinceLevelLoad }
        });

        // FirebaseAnalytics.LogEvent(AnalyticsResources.OnCloseScene + "_" + scenetype.ToString().ToLower(), new Parameter[] {
        //     new Parameter ("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter("Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version),
        //     new Parameter("scene_type", scenetype.ToString()),
        //     new Parameter("design_time", Time.timeSinceLevelLoad)
        // });

        Debug.Log("Time Spent in Scene: " + Time.timeSinceLevelLoad);
    }


    /// <summary>
    /// Called when tutorial goes into next level
    /// </summary>
    private void TutorialChapterStarted(string obj)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Tutorial_Started, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
             { "Chapter", obj}
        });
    }

    /// <summary>
    /// Called when the whole tutorial is done
    /// </summary>
    private void TutorialCompleted(string obj)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Tutorial_Completed, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
             { "Chapter", obj}
        });

    }

    /// <summary>
    /// Called when tutorial is skipped 
    /// </summary>
    private void TutorialSkipped(string obj)
    {
        //Debug.Log("Analytics : Tutorial Skipped at " + obj);
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Tutorial_Skipped, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
             { "Chapter", obj}
        });
    }


    /// <summary>
    /// Called when tutorial completes a level
    /// </summary>
    private void TutorialChapterCompleted(string obj)
    {
        Debug.Log("Analytics : Tutorial Chapter Completed " + obj);
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Tutorial_Chapter_Completed, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
             { "Chapter", obj}
        });
    }

    /// <summary>
    /// Share button click analytic
    /// </summary>
    private void ShareButtonClicked()
    {
        Debug.Log("Analytics : Share button clicked ");
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Share_Button_Clicked, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "IsCustomer", IsCustomer()},
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "Is_Demo_Design", IsDemoDesign()},
        });
    }

    #endregion

    #region New Calls
    private void CustomEvent_ActionFailed(ActionType actionType, int errCOde)
    {
        AnalyticsEvent.Custom("ActionFailed", new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "actiontype", actionType.ToString()},
            { "errorcode", ((ErrorTracking.ErrorCodes)errCOde).ToString()} 
        });
    }

    private void OnSwapHotspot(IHotspot obj)
    {
        Debug.Log("[Analytics] Swapped Hotspots Cat: " + ((!obj.isHotspot && categories.ContainsKey(obj.asset.subcategory)) ? categories[obj.asset.subcategory].name : "none"));

        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Hotspot_Swaped, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "NewSwappedAsset", obj.asset.name},
            { "subcategory", (!obj.isHotspot && categories.ContainsKey(obj.asset.subcategory))? categories[obj.asset.subcategory].name: "none" },
            { "HotspotType", "asset" }
        });
    }


    private void OnSwapHotspotWallfinish(IWallfinishHotspot obj)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.Hotspot_Swaped, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "NewSwappedAsset", "wallfinish"},
            { "subcategory", "paint" },
            {"HotspotType", "wallfinish" }
        });
    }

    private void OnDesignChallengeComplete(string challengeName)
    {
        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.ChallengeComplete, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "challenge_name", challengeName },
            {"current_coins", AppComponent.Instance.db.userSavedData.currentCoins },
            {"total_challenges", AppComponent.Instance.db.userSavedData.challengesCompleted.Count },
        });

        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter( "Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version),
        //     new Parameter("challenge_name", challengeName),
        //     new Parameter("current_coins", AppComponent.Instance.db.userSavedData.currentCoins ),
        //     new Parameter("total_challenges", AppComponent.Instance.db.userSavedData.challengesCompleted.Count )
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.ChallengeComplete, param);
    }

    private void OnAuthUpdated(AuthUpdateType userRes, int errCode)
    {
        Debug.Log("[Analytics] User Auth respone: " + userRes.ToString());

        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.AuthPanel_Response, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "response", userRes.ToString() },
            { "scene_type", currentScene }
        });

        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter("Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version),
        //     new Parameter("response", userRes.ToString()),
        //     new Parameter("scene_type", currentScene)
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.AuthPanel_Response, param);
    }

    private void OnChallengeComplete(ChallengeComplete_Behaviour CCB)
    {
        Debug.Log("[Analytics] Challenge Complete respone: " + CCB.ToString());

        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.ChallengeComplete_Response, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "response", CCB.ToString() },
        });

        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter ("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter ( "Unique_ID", UUID),
        //     new Parameter ("AppVersion", Application.version),
        //     new Parameter ("response", CCB.ToString())
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.ChallengeComplete_Response, param);
    }


    private void Event_DesignOpened(SceneType sceneType)
    {
        Debug.Log("[Analytics] OnScene Opened: " + sceneType.ToString());

        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.OnOpen_design, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
             { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "scene_type", sceneType.ToString() },
        });
        // Firebase event using array of parameters - key value pairs
        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter( "customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter( "Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version),
        //     new Parameter( "scene_type", sceneType.ToString())
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.OnOpen_design, param);
    }


    private void Event_ShopClicked(IAsset asset, ShopButtonType buttonType)
    {
        Debug.Log("[Analytics] OnShop Clicked at: " + buttonType.ToString()+ " asst: "+ asset.name);

        if (IsAnalyticsOffline()) return;
        AnalyticsEvent.Custom(AnalyticsResources.OnShop_Click, new Dictionary<string, object>
        {
            { "customer", AppComponent.Instance.account.CRED_USERID },
            { "Unique ID", UUID},
            { "AppVersion", Application.version},
            { "scene_type", currentScene},
            { "button_type", buttonType.ToString() },
            { "subcategory", (categories.ContainsKey(asset.subcategory))? categories[asset.subcategory].name: "null" },
            { "product_name", asset.name}
        });

        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter("Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version),
        //     new Parameter("scene_type", currentScene),
        //     new Parameter("button_type", buttonType.ToString() ),
        //     new Parameter( "subcategory", (categories.ContainsKey(asset.subcategory))? categories[asset.subcategory].name: "null" ),
        //     new Parameter("product_name", asset.name)
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.OnShop_Click, param);
    }

    private void Event_OnExpertHelp_Pressed()
    {
        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter("Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version)
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.OnExpertHelp_Started, param);
    }

    private void Event_OnExpertHelp_ImgUpload()
    {
        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter("Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version)
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.OnExpertHelp_ImageUploaded, param);
    }

    private void Event_OnExpertHelp_Submitted()
    {
        // Parameter[] param = new Parameter[]
        // {
        //     new Parameter("customer", AppComponent.Instance.account.CRED_USERID),
        //     new Parameter("Unique_ID", UUID),
        //     new Parameter("AppVersion", Application.version)
        // };
        // FirebaseAnalytics.LogEvent(AnalyticsResources.OnExpertHelp_Submitted, param);
    }

    #endregion
    /// <summary>
    /// returns 1 if customer, 0 if not customer
    /// </summary>
    /// <returns></returns>
    string IsCustomer()
    {
        return AppComponent.Instance.account.CRED_ISCUSTOMER.ToString();//.Equals("1") ? "1" : "0"; 
    }

    /// <summary>
    /// Returns true if demo design, false if not demo design
    /// </summary>
    /// <returns></returns>
    string IsDemoDesign()
    {
        return AppComponent.Instance.db.CurrentDesign.demo.ToString();// == true ? : ;
    }

    /// <summary>
    /// returns true if <see cref="AnalyticsManager"/> shouldn't be recording
    /// </summary>
    /// <returns></returns>
    public static bool IsAnalyticsOffline()
    {
#if UNITY_EDITOR
        //Debug.Log("Analytics Offline, Unity Edior..!");
        return true;
#else
        //returns true if analytics shouldn't be recording
        if (AppComponent.Instance.account.internalTestUser == AppComponent.Instance.account.CRED_USERNAME)
        {
            Debug.Log("Analytics Offline Due to internal user login");
            return true;
        }

        if (Debug.isDebugBuild)
        {
            Debug.Log("Analytics Offline Due to Debug Build");
            return true;
        }

        return false;
#endif
    }



}


/// <summary>
/// Warning: Read Only properties, please dont change any of the string variables here as 
/// it might directly effect the analytics data
/// </summary>
public class AnalyticsCalls
{
    public const string DesignLoadTime = "DesignLoadTime";
    public const string SceneClosed = "TimeSpentInDesign";
    public const string ProductListOpened = "ProductListOpened";
    public const string SceneConfirmationResponse = "SceneConfirmationResponse";
    public const string CenterTabSwitch = "CenterTabSwitch";
    public const string TutorialChapterStarted = "TutorialChapterStarted";
    public const string TutorialChapterComplete = "TutorialChapterComplete";
    public const string TutorialSkipped = "TutorialSkipped";
    public const string TutorialCompleted = "TutorialCompeleted";
    public const string ShareButtonClicked = "ShareButtonClicked";
    public const string SaveDesignCompleted = "Save_Design_Clicked";
}

/// <summary>
/// Warning: Read Only properties, please dont change any of the string variables here as 
/// it might directly effect the analytics data
/// </summary>
public static class AnalyticsResources
{
    public const string Button_Clicked_Asset_Store = "Button_Clicked_Asset_Store";
    public const string Button_Clicked_Paint_Store = "Button_Clicked_Paint_Store";
    public const string Button_Clicked_Wallpaper_Store = "Button_Clicked_Wallpaper_Store";
    public const string Button_Clicked_Swap_Store = "Button_Clicked_Swap_Store";
    public const string Button_Clicked_Product_Info = "Button_Clicked_Product_Info";
    public const string Project_List_Tab_Clicked = "Project_List_Tab_Clicked";
    public const string Project_Selected = "Project_Selected";
    public const string Save_Design_Button_Clicked = "Save_Design_Button_Clicked";
    public const string Viewpoint_saved = "Viewpoint_saved";
    public const string Asset_Deleted = "Asset_Deleted";
    public const string Estimate_Panel_Opened = "Estimate_Panel_Opened";
    public const string Wallfinish_Dropped_Inside_Room = "Wallfinish_Dropped_Inside_Room";
    public const string Asset_Dropped_Inside_Room = "Asset_Dropped_Inside_Room";
    public const string Asset_Selected = "Asset_Selected";
    public const string Shop_Clicked = "Shop_Clicked";
    public const string Asset_Swaped = "Asset_Swaped";
    public const string Design_Load_Time = "Design_Load_Time";
    public const string Design_Closed = "Design_Closed";
    public const string Tutorial_Started = "Tutorial_Started";
    public const string Tutorial_Completed = "Tutorial_Completed";
    public const string Tutorial_Skipped = "Tutorial_Skipped";
    public const string Tutorial_Chapter_Completed = "Tutorial_Chapter_Completed";
    public const string Share_Button_Clicked = "Share_Button_Clicked";


    public const string ActionFailed = "action_failed";
    public const string ChallengeComplete = "challenge_complete";
    public const string Hotspot_Swaped = "hotspot_swaped";
    public const string AuthPanel_Response = "authpanel_response";
    public const string ChallengeComplete_Response = "challengecomplete_response";
    public const string OnCloseScene = "onclose";
    public const string OnOpen_design = "onopen_design";
    public const string OnShop_Click = "onshop_click";

    public const string OnExpertHelp_Started = "onexperthelp_started";
    public const string OnExpertHelp_ImageUploaded = "onexperthelp_imageuploaded";
    public const string OnExpertHelp_Submitted = "onexperthelp_submitted";
}