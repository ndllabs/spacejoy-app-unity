﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ChallengeSystem;
using _unity;
using _unity.Events;
using System;
using Homefuly;

public class SwapNumberConstraint : IChallenge
{

    public enum SwapLimitType
    {
        Less,
        Greater,
        Range
    }

    private string challengeType = string.Empty;
    private string description = string.Empty;
    private bool isComplete = new bool();
    private string limitType;

    public string ChallengeType { get => challengeType;}

    public string Description { get => description; }

    public bool IsComplete { get => isComplete; }

    #region fields
    private int numberOfSwaps;
    #endregion

    public SwapNumberConstraint(IConstraint constraint)
    {
        numberOfSwaps = 0;
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, IncrementSwap);
    }

    void OnDisable()
    {
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, IncrementSwap);
    }

    public void AddListener()
    {
        return;
    }

    public void RemoveListener()
    {
        return;
    }

    void IncrementSwap(IHotspot hotspot)
    {
        ++numberOfSwaps;
    }
}
