﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity;
using _unity.Events;
using Homefuly;
using ChallengeSystem;
using System;
using SFC = StaticFunctionCalls;
using Homefuly.Events;

/*
 * Challenge Manager v1.0
 * Created by: Praveen Sundar
 * This script manages all the challenge related tasks 
 * When an action is made, it is received through the scene manager as an event and corresponding functions are executed
 * Evey action (from scene management) should be accounted for as a challenge constraint
 * Constraints for version 1.0
 *  Budget
 *  Number of moves/actions (to be updated later)
 *  Swap (number of swaps, assets to be swapped etc., also to be updated later)
 * 
 * In the basic form, only the budget constraint is applied
 */

public class ChallengeManager : Singleton<ChallengeManager>
{
    protected ChallengeManager() { }

    #region challenge properties

    List<IConstraint> challengeConstraints;
    List<IChallenge> challengesList;
    DateTime startTime;
    DateTime endTime;
    bool challengeStatus;

    GameObject GOChlngConstraints;
    #endregion


    #region eventManager

    #endregion
    //StaticFunctionCalls.Action_AssetMoved
    //StaticFunctionCalls.Action_AssetDelete

    /// <summary>
    /// this is a singleton but can be destroyed on load
    /// not derived it from singleton class which creates a game object that is added to dontdestroyonload
    /// Not necessary to make it thread safe since only the scene manager will have access to it.
    /// if we need to make it a monobehavior, this won't be necessary
    /// </summary>

    // public enum ConstraintType
    // {
    //     Budget,
    //     Swap,
    //     Time,
    //     Add
    // }

    //public static ChallengeManager _instance;
    //public static ChallengeManager Instance
    //{
    //    get
    //    {
    //        if (_instance != null)
    //            return _instance;
    //        _instance = new ChallengeManager();
    //        return _instance;
    //    }
    //}

    void OnEnable()
    {
        //challengeConstraints = new List<IConstraint>();
        //challengesList = new List<IChallenge>();
        //Debug.Log("List of challenge ilst " + challengesList.Count);
        //startTime = DateTime.Now;
        //endTime = DateTime.Now;



        Messenger.AddListener(SFC.ConstraintUpdate, OnConstraintUpdate);
    }

    void OnDisable()
    {
        //Messenger.RemoveListener(SFC.Request_DesignSave, RewardAndSaveCoin);
        Messenger.RemoveListener(SFC.ConstraintUpdate, OnConstraintUpdate);
    }

    /// <summary>
    /// Initializes the local IConstraint data structure to the one from the db
    /// </summary>
    public void Init()
    {
        challengeStatus = false;
        challengeConstraints = new List<IConstraint>();
        challengesList = new List<IChallenge>();
        startTime = DateTime.Now;
        endTime = DateTime.Now;

        ResetConstraints();

        challengeConstraints = AppComponent.Instance.db.designChallenge.constraints;
        foreach (IConstraint constraint in challengeConstraints)
        {
            if (Enum.TryParse(constraint.constraint.constraintType, out ConstraintType thisConstraintType))
            {
                IChallenge currentConstraint;
                switch (thisConstraintType)
                {
                    case ConstraintType.budget:
                        {
                            GameObject go = new GameObject(constraint.constraint.constraintType);
                            go.transform.GetOrAddComponent<BudgetConstraint>().Setup(constraint);
                            go.transform.SetParent(GOChlngConstraints.transform);
                            challengesList.Add(go.transform.GetComponent<BudgetConstraint>());
                        }
                        break;
                    case ConstraintType.time:
                        {
                            //currentConstraint = new TimeConstraint(constraint);
                        }
                        break;
                    case ConstraintType.swap:
                        {
                            currentConstraint = new SwapChallenge(constraint);
                            challengesList.Add(currentConstraint);
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Debug.Log("The constraint type doesn't match with the one in the database");
            }
        }
        //Messenger.AddListener(SFC.Request_DesignSave, RewardAndSaveCoin);

    }


    void ResetConstraints()
    {
        challengesList.Clear();
        if (GOChlngConstraints != null)
            GameObject.Destroy(GOChlngConstraints);
        SetupConstraints();
    }

    void SetupConstraints()
    {
        GOChlngConstraints = GameObject.Find("ChallengeConstraints");
        if (GOChlngConstraints == null)
        {
            GOChlngConstraints = new GameObject("ChallengeConstraints");
        }
    }

    /// <summary>
    /// rewards coins for completing the challenge and saves the coins to the user db
    /// </summary>
    public void RewardAndSaveCoin()
    {
        //AppComponent.Instance.db.UpdateCoins(coins);
        //AppComponent.Instance.db.userSavedData.currentCoins = coins.ToString();
        float rewardCoins;
        rewardCoins = AppComponent.Instance.db.userSavedData.currentCoins;
        var challenges = AppComponent.Instance.db.userSavedData.challengesCompleted;
        List<ChallengeReward> rewards = AppComponent.Instance.db.designChallenge.rewards;
        foreach (ChallengeReward reward in rewards)
        {
            Enum.TryParse(reward.rewardType, out RewardType currentRewardType);
            switch (currentRewardType)
            {
                case RewardType.coin:
                    {
                        // get the coins from the app db and add the reward
                        //rewardCoins = AppComponent.Instance.db.userSavedData.currentCoins;
                        rewardCoins += reward.rewardValue;
                    }
                    break;
                default:
                    break;
            }
        }

        if (!challenges.Contains(AppComponent.Instance.db.designChallenge._id))
            challenges.Add(AppComponent.Instance.db.designChallenge._id);

        // save coin to the user db
        // to update the coins and rewards
        AppComponent.Instance.db.UpdateUserChallenges(rewardCoins, challenges, true);
    }

    /// <summary>
    /// When a hotspot constraint is updated or completed, this function is triggered
    /// if there is a change in the previous challenge status, it broadcasts a message
    /// with the status value true or false
    /// </summary>
    private void OnConstraintUpdate()
    {
        bool isComplete = IsChallengeComplete();
        if (isComplete != challengeStatus)
        {
            Messenger<bool>.Broadcast(SFC.ChallengeStatusUpdate, isComplete);
            challengeStatus = isComplete;
        }
    }


    /// <summary>
    /// To be called from the scene manager to check if challenge constraints have been met
    /// Return value : Bool
    /// returns true of all the constraints in the IConstraint class is met
    /// </summary>
    public bool IsChallengeComplete()
    {
        endTime = DateTime.UtcNow;
        bool isComplete = true;
        foreach (IChallenge constraint in challengesList)
        {
            isComplete &= constraint.IsComplete;
        }
        return isComplete;
    }
}
