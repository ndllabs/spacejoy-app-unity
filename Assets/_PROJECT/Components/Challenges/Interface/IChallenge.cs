﻿/// <summary>
/// Interface for challenge
/// Multiple types of challenges can be inherited from this interface (can implement this interface)
/// </summary>
namespace ChallengeSystem
{
    public interface IChallenge
    {
        string ChallengeType { get; }
        string Description { get; }
        bool IsComplete { get; }
        void AddListener();
        void RemoveListener();
    }
}
