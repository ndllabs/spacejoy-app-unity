﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChallengeSystem;
using _unity;
using _unity.Events;
using SFC = StaticFunctionCalls;

public class BudgetConstraint : MonoBehaviour, IChallenge
{
    private string description = string.Empty;
    private string challengeType = string.Empty;
    private bool isComplete = new bool();
    private IConstraint constraint;

    private List<string> wallsEverHadPaintOn = new List<string>();

    public string ChallengeType { get => challengeType; }

    public string Description { get => description; }

    public bool IsComplete { get => isComplete; }


    private float minBudget;
    private float maxBudget;
    private float currentBudget;

    // property for currentBudget
    public float CurrentBudget { get => currentBudget; }

    public void AddListener()
    {
        return;
    }

    public void RemoveListener()
    {
        return;
    }

    public void Setup(IConstraint constraint)
    {
        minBudget = constraint.constraint.minBudget;
        maxBudget = constraint.constraint.maxBudget;
        currentBudget = 0.0f;
    }
    private void OnEnable()
    {

        Messenger<IHotspot>.AddListener(SFC.Action_AssetDeleted, OnAssetDelete);
        Messenger<IHotspot>.AddListener(SFC.Action_HotspotSwapped, OnAssetSwap);
        Messenger<IWallfinishHotspot>.AddListener(SFC.Action_WallfinishSwapped, OnWallFinishApplied);

    }

        private void OnDisable()
    {

        Messenger<IHotspot>.RemoveListener(SFC.Action_AssetDeleted, OnAssetDelete);
        Messenger<IHotspot>.RemoveListener(SFC.Action_HotspotSwapped, OnAssetSwap);
        Messenger<IWallfinishHotspot>.RemoveListener(SFC.Action_WallfinishSwapped, OnWallFinishApplied);
    }
   

    public void IsChallengeComplete()
    {
        bool prevStatus = isComplete;
        if (currentBudget >= minBudget && currentBudget <= maxBudget)
            isComplete = true;
        else
            isComplete = false;
        Messenger.Broadcast(SFC.ConstraintUpdate);
    }

    private void OnAssetDelete(IHotspot hotspot)
    {
        if (hotspot.isHotspot) return;

        Debug.Log("Current Budddget now: " + currentBudget);
        currentBudget -= hotspot.asset.price.customer;

        Debug.Log("Current Bussdget now: " + currentBudget);
        IsChallengeComplete();
    }

    private void OnAssetSwap(IHotspot hotspot)
    {
        if (hotspot.isHotspot) return;

        Debug.Log("Current Budddget now: " + currentBudget+" Asset price: "+ hotspot.asset.price.customer);
        currentBudget += hotspot.asset.price.customer;

        Debug.Log("Final Budget: " + currentBudget);

        IsChallengeComplete();
        Messenger<float>.Broadcast(SFC.Action_BudgetUpdated, currentBudget);
    }

    private void OnWallFinishApplied(IWallfinishHotspot fromWallFinish)
    {
        if (fromWallFinish.isHotspot) return;

        Debug.Log("Current Budget: " + currentBudget);

        if (!wallsEverHadPaintOn.Contains(fromWallFinish.wall))
        {
            currentBudget += 200f;
            wallsEverHadPaintOn.Add(fromWallFinish.wall);
            Debug.Log("Current Budget now: " + currentBudget);
        }

        IsChallengeComplete();

        Messenger<float>.Broadcast(SFC.Action_BudgetUpdated, currentBudget);
    }
}
