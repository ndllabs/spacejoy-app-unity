﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity;
using _unity.Events;
using ChallengeSystem;

public class SwapChallenge : IChallenge
{
    private string challengeType = string.Empty;
    private string description = string.Empty;
    private bool isComplete = new bool();
    private bool isSelected;
    private IConstraint constraint;
    private IHotspot assetHotSpot;

    #region fields for swap action

    #endregion


    public string ChallengeType
    {
        get
        {
            return challengeType;
        }
    }

    public string Description
    {
        get
        {
            return description;
        }
    }

    public bool IsComplete
    {
        get
        {
            return isComplete;
        }
    }

    public SwapChallenge(IConstraint challengeConstraint)
    {
        challengeType = "Swap";
        constraint = challengeConstraint;
        //description = desc;
        //assetHotSpot = hotspot;
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_AssetDeleted, OnAssetDelete);
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, OnAssetSwap);
    }

    ~SwapChallenge()
    {
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_AssetDeleted, OnAssetDelete);
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, OnAssetSwap);
    }
    

    public void AddListener()
    {
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, OnAssetSwap);
    }

    public void RemoveListener()
    {
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, OnAssetSwap);
    }

    void OnAssetDelete(IHotspot hotspot)
    {
        //if (constraint.id == hotspot.asset._id)
        //{
        //    isSelected = true;
        //}
    }

    void OnAssetSwap(IHotspot hotspot)
    {
        if (isSelected)
        {
            isComplete = true;
            isSelected = false;
        }

        // Debug.Log("[IONIC] Closing unity");
        // IonicComms.FinishActivity("Unity Closed");
    }
}
