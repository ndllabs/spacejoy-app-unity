﻿using System.Threading.Tasks;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

using Homefuly;
//using Google;
//using Firebase;
//using Firebase.Auth;
//using Facebook.Unity;
using System.Collections.Generic;
using Homefuly.Events;

/// <summary>
/// Handles the regular email password authentication
/// and the logins using google and facebook
/// Once either of the social logins are used, it syncs with the Homefuly server
/// to get the user id and the user information
/// </summary>
public class AuthManager : MonoBehaviour
{
    #region Fields

    AppComponent app;


    public TMP_InputField inpEmail;
    public TMP_InputField inpPass;
    public Button btnLogin,btnShowPass;
    public GameObject goLoading;

    public TMP_InputField inpNameSignup;
    public TMP_InputField inpEmailSignup;
    public TMP_InputField inpPassSignup;
    public Button btnSignup,btnShowPassSignup;
    public GameObject goLoadingSignup;

    [Header("Google Sign In")]
    public string webClientId;
    public string webSecretKey;
    public string googleAuthHost;
    public string googleAuthURL;
    public string redirect_uri;

    public Text infoText;
    public Text serverText;
    //private GoogleSignInConfiguration configuration;
    //private FirebaseAuth auth;
    #endregion

    private Action LoginCallBack = null;

    EventSystem eventSys;
    bool isPrevUserDemo;
    TouchScreenKeyboard keyboard;
    private static bool isLoggedIn = false;
    public static bool IsLoggedIn
    {
        get
        {
            return isLoggedIn;
        }
        set
        {
            isLoggedIn = value;
        }
    }

    public static Action Callback;

    void OnEnable()
    {
        //Screen.orientation = ScreenOrientation.Portrait;

    }

    void Awake()
    {
        //configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestIdToken = true };
        //Initialize facebook
        //if (!FB.IsInitialized)
        //    FB.Init(InitCallback, OnHideUnity);
        //else
        //    FB.ActivateApp();
    }

    #region Facebook setup

    //private void InitCallback()
    //{
    //    if (FB.IsInitialized)
    //    {
    //        // Signal an app activation App Event
    //        FB.ActivateApp();
    //        //AddToInformation("FB Init done");
    //        Debug.Log("FB Init done");
    //        // Continue with Facebook SDK
    //        // ...
    //    }
    //    else
    //    {
    //        //AddToInformation("Failed to Initialize the Facebook SDK");
    //        Debug.Log("Failed to Initialize the Facebook SDK");
    //    }
    //}

    //private void OnHideUnity(bool isGameShown)
    //{
    //    if (!isGameShown)
    //    {
    //        // Pause the game - we will need to hide
    //        Time.timeScale = 0;
    //    }
    //    else
    //    {
    //        // Resume the game - we're getting focus again
    //        Time.timeScale = 1;
    //    }
    //}
    #endregion

    //private void CheckFirebaseDependencies()
    //{
    //    //NotificationMain.Instance.Init();
    //    FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
    //    {
    //        if (task.IsCompleted)
    //        {
    //            if (task.Result == DependencyStatus.Available)
    //            {
    //                auth = FirebaseAuth.DefaultInstance;
    //                Debug.Log("Firebase dependencies checked and available");
    //            }
    //            else {
    //                Debug.Log("Could not resolve all Firebase dependencies: " + task.Result.ToString());
    //            }
    //        }
    //        else
    //        {
    //            Debug.Log("Dependency check was not completed. Error : " + task.Exception.Message);
    //        }
    //    });
    //}

    public void Init()
    {
        isPrevUserDemo = app.account.CRED_ISDEMO == string.Empty ? true : false;
        //clear the email and password fields
        ClearAuthFields();
    }

    //void AddToInformation(string input)
    //{
    //    infoText.text = input;
    //}

    void Start()
    {
        //await Task.Delay(TimeSpan.FromSeconds(2));
        WidgetManager.Instance.LoaderClose();
        eventSys = EventSystem.current;

        app = AppComponent.Instance;

        StateAuth(AuthPageState.idle);
        inpEmail.keyboardType = TouchScreenKeyboardType.URL;
        inpPass.keyboardType = TouchScreenKeyboardType.Default;

        inpEmailSignup.keyboardType = TouchScreenKeyboardType.URL;
        inpPassSignup.keyboardType = TouchScreenKeyboardType.Default;
        inpNameSignup.keyboardType = TouchScreenKeyboardType.Default;

        
        //AddToInformation(webClientId);
        //CheckFirebaseDependencies();

        

        // inpEmail.keyboardType = (TouchScreenKeyboardType)(-1);
        // inpPass.keyboardType = (TouchScreenKeyboardType)(-1);

        // inpEmail.onSelect.RemoveAllListeners();
        // inpEmail.onSelect.AddListener((Delegate)=>
        // {
        //     ForceKeyboardOpen();
        // }
        // );

        // inpPass.onSelect.RemoveAllListeners();
        // inpPass.onSelect.AddListener((Delegate) =>
        // {
        //     ForceKeyboardOpen();
        // }
        //);


        //Assuming this doesn't run on desktop
#if !UNITY_EDITOR
            inpEmail.onEndEdit.AddListener(delegate { SwitchWithTab(); });
            inpPass.onEndEdit.AddListener(delegate { SwitchWithTab(); });
            inpEmailSignup.onEndEdit.AddListener(delegate { SwitchWithTab(); });
            inpPassSignup.onEndEdit.AddListener(delegate { SwitchWithTab(); });
            inpNameSignup.onEndEdit.AddListener(delegate { SwitchWithTab(); });
#endif

    }

    #region tab functionality

    //void ForceKeyboardOpen()
    //{

    //    if(!TouchScreenKeyboard.visible)
    //    TouchScreenKeyboard.Open("", TouchScreenKeyboardType.URL);

    //}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) SwitchWithTab();
    }
    /*
    private void GoToNextField()
    {
        inpEmail.FindSelectableOnDown().Select();
    }
    */
    private void SwitchWithTab()
    {
        try
        {
            Selectable selectable = eventSys.currentSelectedGameObject.GetComponent<Selectable>();
            if (selectable.FindSelectableOnDown() == null)
            {
                // if(WidgetManager.Instance.SignInPanel.activeSelf)
                // {
                //     inpEmail.Select();
                // }
                // else
                // {
                //     inpEmailSignup.Select();
                // }
            }
            else
            {
                if (selectable.FindSelectableOnDown() == btnLogin || selectable.FindSelectableOnDown() == btnSignup)
                {
                    //DoLogin();//Disabling auto login
                }
                else
                    selectable.FindSelectableOnDown().Select();
            }
        }
        catch
        {
            inpEmail.Select();
        }

        //Debug.Log("Current object selected: " + eventSys.currentSelectedGameObject.name);
    }
    /*
    void LoginIfEnteredDetails()
    {
        if(!string.IsNullOrEmpty(inpEmail.text) & !string.IsNullOrEmpty(inpPass.text))
        {
            DoLogin();
        }
    }*/

    #endregion
    
    /// <summary>
    /// When the user presses forgot password "text", they're redirected to the 
    /// reset password in the web ui
    /// </summary>
    public void RedirectForgotPass()
    {
        string url = "https://www.homefuly.com/#/forgot-password-email-verification/gameapp";

        InAppBrowser.OpenURL(url, new InAppBrowser.DisplayOptions
        {
            displayURLAsPageTitle = false,
            hidesTopBar = true,
            androidBackButtonCustomBehaviour = false,
            hidesDefaultSpinner = false,
            pinchAndZoomEnabled = false,
        });
    }


    /// <summary>
    /// Redirection to the signup page
    /// </summary>
    public void RedirectSignUp()
    {
        // InAppBrowser.OpenURL("https://homefuly.com/#/login");
        WidgetManager.Instance.SignInPanel.SetActive(false);
        WidgetManager.Instance.SignUpPanel.SetActive(true);
    }

    /// <summary>
    /// Redirection to the signin page
    /// </summary>
    public void RedirectSignIn()
    {
        WidgetManager.Instance.SignInPanel.SetActive(true);
        WidgetManager.Instance.SignUpPanel.SetActive(false);
    }

    /// <summary>
    /// Relogin happens only during the start of the application when the jwt token expires
    /// and the login was through facebook or google.
    /// The callback will continue with opening the home page
    /// </summary>
    /// <param name="provider"></param>
    /// <param name="reLoginCallBack"></param>
    public void Relogin(string provider, Action reLoginCallBack=null)
    {
        LoginCallBack = reLoginCallBack;
        if (provider == AuthType.facebook.ToString())
        {
            //LoginViaFacebook();
        }
        else if (provider == AuthType.google.ToString())
        {
            //LoginViaGoogle();
        }
        else
            LoginCallBack();
    }

    //public void ResetCallBack()
    //{
    //    LoginCallBack = null;
    //    return;
    //}

    #region Facebook Login
    /// <summary>
    /// First function called when the facebook button is pressed
    /// 
    /// Disables all the buttons to avoid spamming
    /// Checks, the firebase dependencies and uses the facebook sdk function for sign in
    /// the AuthCallBack is called once the login returns to the app
    /// </summary>
    //public void LoginViaFacebook()
    //{
    //    //Do something
    //    WidgetManager.Instance.DisableAuthButtons();
    //    //show mini loader
    //    WidgetManager.Instance.AuthLoader("Logging in...");
    //    // WidgetManager.Instance.Loader("Logging in...", 15f);
    //    //CheckFirebaseDependencies();
    //    //FB.LogOut();
    //    //auth.SignOut();
    //    Debug.Log("[" + nameof(LoginViaFacebook) + "] " + "Checked firebase dependencies");
    //    List<String> perms = new List<String>() { "public_profile", "email" };
    //    FB.LogInWithReadPermissions(perms, AuthCallBack);
    //}

    ///// <summary>
    ///// Called after the action returns from signing in to facebook
    ///// </summary>
    ///// <param name="result"></param>
    //private void AuthCallBack(ILoginResult result)
    //{
    //    if (result == null || string.IsNullOrEmpty(result.Error))
    //    {
    //        //AddToInformation("FB Auth success");
    //        Debug.Log("FB Auth Success");
    //    }
    //    Debug.Log("Called Auth callback");
    //    //AddToInformation(result.AccessToken.TokenString);
    //    if (FB.IsLoggedIn)
    //    {
    //        var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
    //        foreach (string perm in aToken.Permissions)
    //        {
    //            Debug.Log("Permission is : " + perm);
    //            Debug.Log(result.AccessToken.UserId);
    //        }
    //        Debug.Log("FB auth token is : " + result.AccessToken.TokenString);
    //        Debug.Log("Fb accesstoken tostring is : " + result.AccessToken.ToString());
    //        //SignInWithFacebookFirebase(aToken.TokenString);
    //        SocialLoginWithHomefuly("facebook", aToken.TokenString, null);
    //    }
    //    else
    //    {
    //        //AddToInformation(result.Error + "\n" + result.RawResult);
    //        Debug.Log("User cancelled login");
    //        Debug.Log(result.Error);
    //        // WidgetManager.Instance.LoaderClose();
    //        HomefulyEvents.OnActionFailed?.Invoke(ActionType.Auth, (int)ErrorTracking.ErrorCodes.Failed_FacebookLogin);
    //        WidgetManager.Instance.EnableAuthButtons();
    //        WidgetManager.Instance.AuthLoaderClose();
    //    }
    //}

    //public void SignInWithFacebookFirebase(string accessToken)
    //{
    //    Firebase.Auth.Credential credential = Firebase.Auth.FacebookAuthProvider.GetCredential(accessToken);

    //    auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
    //    {
    //        //if (task.IsCanceled)
    //        //{
    //        //    Debug.LogError("SignInWithCredentialAsync was canceled.");
    //        //    return;
    //        //}
    //        //if (task.IsFaulted)
    //        //{
    //        //    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
    //        //    return;
    //        //}
    //        AggregateException ex = task.Exception;
    //        if (ex != null)
    //        {
    //            if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
    //                //AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
    //                Debug.Log("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
    //        }
    //        else
    //        {
    //            //AddToInformation("Sign In Successful. ");
    //            Debug.Log("Sign In Successful. ");
    //        }
    //        Firebase.Auth.FirebaseUser newUser = task.Result;
    //        //AddToInformation("Facebook login " + newUser.DisplayName);

    //        Debug.Log(newUser.DisplayName);
    //        Debug.LogFormat("User signed in successfully: {0} ({1})",
    //            newUser.DisplayName, newUser.UserId);
    //    });
        
    //}
    #endregion


    #region Google Login
    //public void LoginViaGoogle()
    //{
    //    //Do something
    //    CheckFirebaseDependencies();
    //    WidgetManager.Instance.AuthLoader("Logging in...");
    //    // Enable loader and disable other buttons
    //    //WidgetManager.Instance.DisableAuthButtons();
    //    // WidgetManager.Instance.Loader("Logging in...", 15f);

    //    //GoogleSignIn.DefaultInstance.SignOut();
    //    //auth.SignOut();
    //    GoogleSignIn.Configuration = configuration;
    //    GoogleSignIn.Configuration.UseGameSignIn = false;
    //    GoogleSignIn.Configuration.RequestEmail = true;
    //    GoogleSignIn.Configuration.RequestProfile = true;
    //    GoogleSignIn.Configuration.RequestIdToken = true;
    //    GoogleSignIn.Configuration.RequestAuthCode = true;

    //    GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    //}

    //internal async void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    //{
    //    Debug.Log("Inside firebase auth");
    //    if (task.IsFaulted)
    //    {
    //        Debug.Log("Task fault");
    //        using (IEnumerator<System.Exception> enumerator =
    //                task.Exception.InnerExceptions.GetEnumerator())
    //        {
    //            if (enumerator.MoveNext())
    //            {
    //                GoogleSignIn.SignInException error =
    //                        (GoogleSignIn.SignInException)enumerator.Current;
    //                Debug.LogError("Got Error: " + error.Status + " " + error.Message);
    //            }
    //            else
    //            {
    //                Debug.LogError("Got Unexpected Exception?!?" + task.Exception);
    //            }
    //        }
    //        // WidgetManager.Instance.LoaderClose();
    //        WidgetManager.Instance.EnableAuthButtons();
    //        WidgetManager.Instance.AuthLoaderClose();
    //    }
    //    else if (task.IsCanceled)
    //    {
    //        Debug.Log("Google login Cancelled");
    //        //AddToInformation("Google login Cancelled");
    //        // WidgetManager.Instance.LoaderClose();
    //        WidgetManager.Instance.EnableAuthButtons();
    //        WidgetManager.Instance.AuthLoaderClose();
    //    }
    //    else
    //    {
    //        if (task.Result.AuthCode == string.Empty)
    //            Debug.Log("Unable to get the login information, please try again");
    //        Debug.Log("====== Start of login result =====");
    //        Debug.Log(task.Result.AuthCode);
    //        Debug.Log(task.Result.UserId);
    //        Debug.Log(task.Result.Email);
    //        Debug.Log("============== END =============");
    //      Debug.Log("Welcome: " + task.Result.DisplayName + "!");
    //        // add user to firebase console
    //        await SignInWithGoogleFirebase(task.Result.IdToken);
    //        SocialLoginWithHomefuly("google", task.Result.IdToken, task.Result.AuthCode);

    //    }
        
    //}

    //public async Task SignInWithGoogleFirebase(string idToken)
    //{
    //    Firebase.Auth.Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(idToken, null);
    //    await auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
    //    {
    //        Debug.Log("Firebase just threw an unknown elol");
    //        //if (task.IsCanceled)
    //        //{
    //        //    Debug.LogError("SignInWithCredentialAsync was canceled.");
    //        //    return;
    //        //}
    //        //if (task.IsFaulted)
    //        //{
    //        //    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
    //        //    return;
    //        //}
    //        AggregateException ex = task.Exception;
    //        if (ex != null)
    //        {
    //            if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
    //                //AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
    //                Debug.Log("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
    //            else
    //                Debug.Log("Firebase just threw an unknown exception: " + ex.Message);
    //        }
    //        else
    //        {
    //            //AddToInformation("Sign In Successful. ");

    //            Firebase.Auth.FirebaseUser newUser = task.Result;
    //            Debug.LogFormat("User signed in successfully: {0} ({1})",
    //                newUser.DisplayName, newUser.UserId);
    //        }
    //    });
    //}
    #endregion

    public async void SocialLoginWithHomefuly(string provider, string token, string authCode)
    {
        //if (provider == "google")
        //{

        //    IGoogleAuth googleAuthData = new IGoogleAuth();
        //    googleAuthData.client_id = webClientId;
        //    googleAuthData.client_secret = webSecretKey;
        //    googleAuthData.code = authCode;
        //    googleAuthData.redirect_uri = redirect_uri;
        //    googleAuthData.grant_type = "authorization_code";
        //    googleAuthData.scope = "";

        //    WebCore web = app.server.web;
        //    string body = JsonUtility.ToJson(googleAuthData);

        //    Debug.Log("URL is " + googleAuthHost + googleAuthURL + body);
        //    var result = await web.Post_WithHost(googleAuthHost, googleAuthURL, body);
        //    Debug.Log("Result is " + result.data);
        //    string google_IDToken = JsonUtility.FromJson<IGoogleAuthResponse>(result.data).id_token;
        //    Debug.Log("Google token is " + google_IDToken);
        //    token = google_IDToken;
        //}
        bool res = await app.account.OAuthLogin(provider, token);
        if (res)
        {
            Debug.Log("Server sync success " + app.account.CRED_USERNAME + " " + app.account.CRED_USEREMAIL);
            IsLoggedIn = true;
            //AddToInformation("Server sync success " + app.account.CRED_USERNAME + " " + app.account.CRED_USEREMAIL);

            // if the sign in is called from jwt expiry
            // relogin happens without opening the auth panel because the user has already signed in using facebook/google
            // so the db update
            WidgetManager.Instance.AuthLoaderClose();
            WidgetManager.Instance.AuthLoader("Syncing...");
            if (LoginCallBack == null)
            {
                if (app.db != null)
                {
                    //app.db = new Homefuly.Database.AppDatabase();
                    //app.db.Init();
                    await app.db.OnUpdateAuth(isPrevUserDemo);
                }
                HomefulyEvents.OnAuthUpdated?.Invoke(provider == "google" ? AuthUpdateType.Google : provider == "facebook" ? AuthUpdateType.Facebook : AuthUpdateType.Error);
                WidgetManager.Instance.AuthPanelClose();
            }
        }
        else
        {
            Debug.Log("Login failed");
            StateAuth(AuthPageState.invalidCred);
            HomefulyEvents.OnActionFailed?.Invoke(ActionType.Auth, provider == "google" ? (int)ErrorTracking.ErrorCodes.Failed_GoogleLinkWithInternal : provider == "facebook" ? (int)ErrorTracking.ErrorCodes.Failed_FacebookLinkWithInternal : (int)ErrorTracking.ErrorCodes.Invalid_InputData);
            IsLoggedIn = false;
        }
        //WidgetManager.Instance.LoaderClose();
        Debug.Log("[" + nameof(SocialLoginWithHomefuly) + "]" + "after login with our server");
        WidgetManager.Instance.AuthLoaderClose();
        WidgetManager.Instance.EnableAuthButtons();
        
        // the callback facebook token expiry relogin has served its purpose
        // so clear the callback variable
        if (LoginCallBack != null)
        {
            Action tempCallBack = LoginCallBack;
            LoginCallBack = null;
            Debug.Log("[" + nameof(SocialLoginWithHomefuly) + "] " + tempCallBack);
            tempCallBack();
        }
    }



    bool t;
    public void ShowPassword()
    {
        if (!t)
        {
            inpPass.inputType = TMP_InputField.InputType.Standard;
            btnShowPass.gameObject.GetComponent<Image>().color = new Color32(116, 167, 192, 255);
        }
        else
        {
            inpPass.inputType = TMP_InputField.InputType.Password;
            btnShowPass.gameObject.GetComponent<Image>().color = new Color32(116, 167, 192, 128);
        }
        t = !t;
        inpPass.ForceLabelUpdate();
       
    }

    public async void DoLogin()
    {
        StateAuth(AuthPageState.inprogress);
        if (AppComponent.Instance.account.CRED_USEREMAIL != string.Empty)
            await AppComponent.Instance.account.Logout();
        WidgetManager.Instance.DisableAuthButtons();
        // WidgetManager.Instance.Loader("Logging in...", 15f);
        WidgetManager.Instance.AuthLoader("Logging in...");
        var login = await app.account.Login(inpEmail.text, inpPass.text);

        if (login.err)
        {
            //btnLogin.gameObject.SetActive(true);
            StateAuth(AuthPageState.invalidCred, "Login failed. Please Try again");
            Debug.Log("Login failed " + login.data);
            AuthManager.IsLoggedIn = false;
            //WidgetManager.Instance.Alert("Login failed","Incorrect username or password, please try again");
        }
        else
        {
            app.account.CURRENT_ORG = AppConfig.PilotDemoConfig.OrgId;
            app.account.CURRENT_RETAIL = AppConfig.PilotDemoConfig.ProjectId;

            StateAuth(AuthPageState.idle);
            Debug.Log(app.account.CRED_USEREMAIL);
            //app.Navigate(AppPage.Preload_Deprycated);
            // add animation to disable the auth widget

            if (app.account.CRED_ISDEMO != string.Empty)
            {
                AuthManager.IsLoggedIn = true;
                Debug.Log("is demo " + app.account.CRED_ISDEMO);
            }
            WidgetManager.Instance.AuthLoader("Syncing...");
            if (app.db != null)
            {
                await app.db.OnUpdateAuth(isPrevUserDemo);
            }
            HomefulyEvents.OnAuthUpdated?.Invoke(AuthUpdateType.InternalSignIn);
            //Callback();
            WidgetManager.Instance.AuthPanelClose();
        }
        // WidgetManager.Instance.LoaderClose();
        WidgetManager.Instance.EnableAuthButtons();
        WidgetManager.Instance.AuthLoaderClose();
        //WidgetManager.Instance.Prompt("Sign In", (string name) =>
        //{
        //    return loggedIn;
        //}, false);
    }


    public async  void DoSignUp()
    {
        StateAuth(AuthPageState.inprogress);
        if (AppComponent.Instance.account.CRED_USEREMAIL != string.Empty)
            await AppComponent.Instance.account.Logout();
        
        WidgetManager.Instance.DisableAuthButtons();
        WidgetManager.Instance.Loader("Creating your account...", 15f);
        var signup = await app.account.Signup(inpNameSignup.text, inpEmailSignup.text, inpPassSignup.text);

        if (signup.err)
        {
            StateAuth(AuthPageState.invalidCred, signup.data);
            Debug.Log("Signup failed " + signup.data);
            AuthManager.IsLoggedIn = false;
        }
        else
        {
            app.account.CURRENT_ORG = AppConfig.PilotDemoConfig.OrgId;
            app.account.CURRENT_RETAIL = AppConfig.PilotDemoConfig.ProjectId;

            StateAuth(AuthPageState.idle);
            Debug.Log(app.account.CRED_USEREMAIL);

            if (app.account.CRED_ISDEMO != string.Empty)
            {
                AuthManager.IsLoggedIn = true;
                Debug.Log("is demo " + app.account.CRED_ISDEMO);
            }
            if (app.db != null)
            {
                await app.db.OnUpdateAuth(isPrevUserDemo);
            }
            HomefulyEvents.OnAuthUpdated?.Invoke(AuthUpdateType.InternalSignUp);
            WidgetManager.Instance.AuthPanelClose();
        }
        WidgetManager.Instance.LoaderClose();
        WidgetManager.Instance.EnableAuthButtons();
    }

    /*public async Task DoAutoLogin()
    {
        var login = await app.account.Login(AppConfig.PilotDemoConfig.TEMP_EMAIL, AppConfig.PilotDemoConfig.TEMP_PWD);

        if (login.err)
        {
            StateAuth(AuthPageState.invalidCred);
        }
        else
        {
            app.account.CURRENT_ORG = AppConfig.PilotDemoConfig.OrgId;
            app.account.CURRENT_RETAIL = AppConfig.PilotDemoConfig.ProjectId;

            app.Navigate(AppPage.Preload);
        }
    }*/

    // Called when opening the auth panel, clears the previous entry from the fields.
    void ClearAuthFields()
    {
        inpEmail.text = "";
        inpPass.text = "";
        inpNameSignup.text = "";
        inpEmailSignup.text = "";
        inpPassSignup.text = "";
}

    void StateAuth(AuthPageState state, string data = "Connection error or Incorrect Credentials. Please try again.")
    {

        WidgetManager.Instance.LoaderMiniClose();
        switch (state)
        {
            case AuthPageState.idle:

                if(WidgetManager.Instance.SignInPanel.activeSelf)
                {
                    btnLogin.gameObject.SetActive(true);
                    goLoading.SetActive(false);
                }
                else if(WidgetManager.Instance.SignUpPanel.activeSelf)
                {
                    btnSignup.gameObject.SetActive(true);
                    goLoadingSignup.SetActive(false);
                }
                break;

            case AuthPageState.inprogress:

                if(WidgetManager.Instance.SignInPanel.activeSelf)
                {
                    btnLogin.gameObject.SetActive(false);
                    goLoading.SetActive(true);
                }
                else if(WidgetManager.Instance.SignUpPanel.activeSelf)
                {
                    btnSignup.gameObject.SetActive(false);
                    goLoadingSignup.SetActive(true);
                }
                break;

            case AuthPageState.invalidCred:
                
                if(WidgetManager.Instance.SignInPanel.activeSelf)
                {
                    WidgetManager.Instance.Alert("", data);
                    btnLogin.gameObject.SetActive(true);
                    goLoading.SetActive(false);
                }
                else if(WidgetManager.Instance.SignUpPanel.activeSelf)
                {
                    WidgetManager.Instance.Alert("", data);
                    btnSignup.gameObject.SetActive(true);
                    goLoadingSignup.SetActive(false);
                }
                break;
        }
    }

    enum AuthPageState
    {
        idle,
        inprogress,
        invalidCred
    }
}
