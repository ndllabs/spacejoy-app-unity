﻿//// <copyright file="SigninSampleScript.cs" company="Google Inc.">
//// Copyright (C) 2017 Google Inc. All Rights Reserved.
////
////  Licensed under the Apache License, Version 2.0 (the "License");
////  you may not use this file except in compliance with the License.
////  You may obtain a copy of the License at
////
////  http://www.apache.org/licenses/LICENSE-2.0
////
////  Unless required by applicable law or agreed to in writing, software
////  distributed under the License is distributed on an "AS IS" BASIS,
////  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
////  See the License for the specific language governing permissions and
////  limitations

//namespace SocialLogin {

//    using System;
//    using System.Collections.Generic;
//    using System.Threading.Tasks;
//    using Google;
//    using UnityEngine;
//    using UnityEngine.UI;
//    using Firebase.Auth;
//    using Firebase;
//    using Facebook.Unity;

//    public class SocialLogin : MonoBehaviour
//    {

//        public Text statusText;

//        public string webClientId = "<your client id here>";

//        private GoogleSignInConfiguration configuration;
//        private FirebaseAuth auth;
//        public Text infoText;
//        // Defer the configuration creation until Awake so the web Client ID
//        // Can be set via the property inspector in the Editor.
//        void Awake()
//        {
//            configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestIdToken = true };
//            CheckFirebaseDependencies();
//            //AddToInformation(webClientId);

//            // Initialize facebook
//            if (!FB.IsInitialized)
//                FB.Init(InitCallback, OnHideUnity);
//            else
//                FB.ActivateApp();
//        }

//        #region Facebook setup

//        private void InitCallback()
//        {
//            if (FB.IsInitialized)
//            {
//                // Signal an app activation App Event
//                FB.ActivateApp();
//                AddToInformation("FB Init done");
//                // Continue with Facebook SDK
//                // ...
//            }
//            else
//            {
//                AddToInformation("Failed to Initialize the Facebook SDK");
//                Debug.Log("Failed to Initialize the Facebook SDK");
//            }
//        }

//        private void OnHideUnity(bool isGameShown)
//        {
//            if (!isGameShown)
//            {
//                // Pause the game - we will need to hide
//                Time.timeScale = 0;
//            }
//            else
//            {
//                // Resume the game - we're getting focus again
//                Time.timeScale = 1;
//            }
//        }
//        #endregion

//        private void CheckFirebaseDependencies()
//        {
//            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
//            {
//                if (task.IsCompleted)
//                {
//                    if (task.Result == DependencyStatus.Available)
//                    {
//                        auth = FirebaseAuth.DefaultInstance;
//                        AddToInformation("Firebase dependencies checked and available");
//                    }
//                    else
//                        AddToInformation("Could not resolve all Firebase dependencies: " + task.Result.ToString());
//                }
//                else
//                {
//                    AddToInformation("Dependency check was not completed. Error : " + task.Exception.Message);
//                }
//            });
//        }

//        public void OnSignIn()
//        {
//            GoogleSignIn.Configuration = configuration;
//            GoogleSignIn.Configuration.UseGameSignIn = false;
//            GoogleSignIn.Configuration.RequestIdToken = true;
//            AddStatusText("Calling SignIn");

//            GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
//            //Task<GoogleSignInUser> signin = GoogleSignIn.DefaultInstance.SignIn();
//            //AddToInformation(signin.Result.Email);
//            //AddToInformation("Called Signin - Didn't seem to work");
//        }

//        #region FB signin
//        public void OnFacebookSignIn()
//        {
//            List<String> perms = new List<String>() { "public_profile", "email"};
//            FB.LogInWithReadPermissions(perms, AuthCallBack);
//        }

//        private void AuthCallBack(ILoginResult result)
//        {
//            if (result == null || string.IsNullOrEmpty(result.Error))
//            {
//                AddToInformation("FB Auth success");
//            }
//            AddToInformation("Called Auth callback");
//            //AddToInformation(result.AccessToken.TokenString);
//            if (FB.IsLoggedIn)
//            {
//                var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
//                foreach (string perm in aToken.Permissions)
//                {
//                    Debug.Log("Permission is : " + perm);
//                    AddToInformation(result.AccessToken.UserId);
//                }
//                SignInWithFacebookFirebase(aToken.TokenString);
//            }
//            else
//            {
//                AddToInformation(result.Error + "\n" + result.RawResult);
//                Debug.Log("User cancelled login");
//                Debug.Log(result.Error);
//            }
//        }

//        public void SignInWithFacebookFirebase(string accessToken)
//        {
//            Firebase.Auth.Credential credential = Firebase.Auth.FacebookAuthProvider.GetCredential(accessToken);
//            auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
//            {
//                //if (task.IsCanceled)
//                //{
//                //    Debug.LogError("SignInWithCredentialAsync was canceled.");
//                //    return;
//                //}
//                //if (task.IsFaulted)
//                //{
//                //    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
//                //    return;
//                //}
//                AggregateException ex = task.Exception;
//                if (ex != null)
//                {
//                    if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
//                        AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
//                }
//                else
//                {
//                    AddToInformation("Sign In Successful. ");
//                }
//                Firebase.Auth.FirebaseUser newUser = task.Result;
//                AddToInformation(newUser.DisplayName);
//                Debug.LogFormat("User signed in successfully: {0} ({1})",
//                    newUser.DisplayName, newUser.UserId);
//            });
//        }

//        public void OnFacebookSignOut()
//        {
//            FB.LogOut();
//            auth.SignOut();
//        }

//        #endregion


//        public void OnSignOut()
//        {
//            AddStatusText("Calling SignOut");
//            GoogleSignIn.DefaultInstance.SignOut();
//            auth.SignOut();
//        }

//        public void OnDisconnect()
//        {
//            AddStatusText("Calling Disconnect");
//            GoogleSignIn.DefaultInstance.Disconnect();
//        }

//        internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
//        {
//            AddStatusText("Inside Firebase Auth");
//            if (task.IsFaulted)
//            {
//                using (IEnumerator<System.Exception> enumerator =
//                        task.Exception.InnerExceptions.GetEnumerator())
//                {
//                    if (enumerator.MoveNext())
//                    {
//                        GoogleSignIn.SignInException error =
//                                (GoogleSignIn.SignInException)enumerator.Current;
//                        AddStatusText("Got Error: " + error.Status + " " + error.Message);
//                    }
//                    else
//                    {
//                        AddStatusText("Got Unexpected Exception?!?" + task.Exception);
//                    }
//                }
//            }
//            else if (task.IsCanceled)
//            {
//                AddStatusText("Canceled");
//            }
//            else
//            {
//                AddStatusText("Welcome: " + task.Result.DisplayName + "!");
//                AddToInformation("Welcome: " + task.Result.DisplayName + "!");
//                // add user to firebase console
//                SignInWithGoogleFirebase(task.Result.IdToken);
//            }
//        }




//        public void SignInWithGoogleFirebase(string idToken)
//        {
//            Firebase.Auth.Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(idToken, null);
//            auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
//            {
//                //if (task.IsCanceled)
//                //{
//                //    Debug.LogError("SignInWithCredentialAsync was canceled.");
//                //    return;
//                //}
//                //if (task.IsFaulted)
//                //{
//                //    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
//                //    return;
//                //}
//                AggregateException ex = task.Exception;
//                if (ex != null)
//                {
//                    if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
//                        AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
//                }
//                else
//                {
//                    AddToInformation("Sign In Successful. ");
//                }

//                Firebase.Auth.FirebaseUser newUser = task.Result;
//                AddToInformation(newUser.DisplayName);
//                Debug.LogFormat("User signed in successfully: {0} ({1})",
//                    newUser.DisplayName, newUser.UserId);
//            });
//        }
//        public void OnSignInSilently()
//        {
//            GoogleSignIn.Configuration = configuration;
//            GoogleSignIn.Configuration.UseGameSignIn = false;
//            GoogleSignIn.Configuration.RequestIdToken = true;
//            AddStatusText("Calling SignIn Silently");

//            GoogleSignIn.DefaultInstance.SignInSilently()
//                  .ContinueWith(OnAuthenticationFinished);
//        }


//        public void OnGamesSignIn()
//        {
//            GoogleSignIn.Configuration = configuration;
//            GoogleSignIn.Configuration.UseGameSignIn = true;
//            GoogleSignIn.Configuration.RequestIdToken = false;

//            AddStatusText("Calling Games SignIn");

//            GoogleSignIn.DefaultInstance.SignIn().ContinueWith(
//              OnAuthenticationFinished);
//        }

//        private List<string> messages = new List<string>();
//        void AddStatusText(string text)
//        {
//            if (messages.Count == 5)
//            {
//                messages.RemoveAt(0);
//            }
//            messages.Add(text);
//            string txt = "";
//            foreach (string s in messages)
//            {
//                txt += "\n" + s;
//            }
//            statusText.text = txt;
//        }

//        private void AddToInformation(string str)
//        {
//            infoText.text = str;
//        }
//    }
//}
