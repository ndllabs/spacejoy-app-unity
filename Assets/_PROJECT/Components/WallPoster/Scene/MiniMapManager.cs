﻿/*
REQUIREMENTS:

*/

using System;
using _unity.Events;
using UnityEngine;
using UnityEngine.UI;


public class MiniMapManager : MonoBehaviour
{
    public Camera minicam;

    public GameObject FloorPlan;
    public  GameObject player;
    
    private GameObject playerHead;
    private bool touched = false;
    private Vector3 SizeofRoom;
    private float xVal = 0, zVal = 0;
    private float camSize = 0;
    private  int layerMask;
    private Bounds b;
    public Button maxbtn;
    public Button Clicker;
    private Rect MiniRect, MaxRect;


    private void Start()
    {
        Init();
    }

    private void OnEnable()
    {
        Messenger<bool>.AddListener(StaticFunctionCalls.HideMiniMapandUI, HideMinimapandUIs);
        Messenger<Texture>.AddListener(StaticFunctionCalls.SetMinimapImage, SetFloorPlan);

    }

    private void OnDisable()
    {
        Messenger<bool>.RemoveListener(StaticFunctionCalls.HideMiniMapandUI, HideMinimapandUIs);
        Messenger<Texture>.RemoveListener(StaticFunctionCalls.SetMinimapImage, SetFloorPlan);
    }



    public void OnClickAction()
    {

        if (touched == false)
        {
            MaximiseMiniMap();
        }
        else
        {
            MinimiseMiniMap();
        }
       
    }

    private Bounds FindFloorBounds()
    {
       b = new Bounds(Vector3.zero, Vector3.zero);
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Floor"))
        {
            Renderer r;
            r = g.GetComponent<Renderer>();
            b.Encapsulate(r.bounds);
        }
     
        return b;
       
    }

    private void Init()
    {   
        GetAspectRatio();
        minicam.rect = MiniRect;
        layerMask = LayerMask.GetMask("MinimapFloorplan");
       
         player = GameObject.Find("Player69FPS");

        Clicker.gameObject.SetActive(false);
      
        minicam.orthographicSize = 5.0f;
        playerHead = player.transform.Find("Head").gameObject;
        playerHead.transform.localScale = new Vector3(0.5f, 0.1f, 0.5f);
        AssignMiniCamtoPlayer();
        maxbtn.transform.localScale = new Vector3(2f, 2f, 0);

       

    
        FindFloorBounds();
    }

    private void SetCameraSize()
    {
        xVal = FloorPlan.transform.GetComponent<MeshRenderer>().bounds.extents.x;
        zVal = FloorPlan.transform.GetComponent<MeshRenderer>().bounds.extents.z;
        if (xVal > zVal)
        {
            camSize = xVal - 3;// / 2;//12;//4;
        }
        else
        {
            camSize = zVal - 3;/// 2;
        }
    }

    public void SetFloorPlan(Texture img)
    {
       
        FloorPlan = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Bounds floorBounds = FindFloorBounds();
        FloorPlan.transform.localScale = floorBounds.size;


        FloorPlan.layer = LayerMask.NameToLayer("MinimapFloorplan");
        FloorPlan.transform.position = floorBounds.center;
       
        FloorPlan.transform.rotation = Quaternion.identity;
      
        FloorPlan.GetComponent<BoxCollider>().enabled = false;

        if (img != null)
        {
            FloorPlan.transform.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Standard"));
            FloorPlan.transform.GetComponent<MeshRenderer>().material.mainTexture = img;
        }


        SetCameraSize();

    }

    public void RayCasttoMap()
    {
        Ray ray = minicam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, ~layerMask))
        {
            //Debug.DrawLine(ray.origin, hit.point);
            //Debug.Log(hit.transform.gameObject.name);
            //Debug.Log(hit.point);
            TeleportPlayer(hit.point.x, hit.point.z);
        }
    }

    public void MaximiseMiniMap()
    {
        Clicker.gameObject.SetActive(true);
        minicam.transform.SetParent(null);
        minicam.transform.position = new Vector3(0, 5, 0);
        minicam.transform.rotation = Quaternion.Euler(90, 0, 180);
        playerHead.transform.localScale = new Vector3(0.5f, 0.1f, 0.5f);
        minicam.orthographicSize = camSize;
        touched = true;
        minicam.rect = MaxRect;//new Rect(0.4f, 0.4f, 1, 1);
        maxbtn.GetComponent<Image>().enabled = true;
      //maxbtn.GetComponent<RectTransform>().localPosition = new Vector3(400, 250, 0);  // DONOT DELETE VERY HARD TO DO AGAIN
        maxbtn.transform.localScale = new Vector3(1f, 1f, 0);
        Messenger.Broadcast(StaticFunctionCalls.DoDeselectAsset_Deprycated);
        Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, true);
    }


    public void MinimiseMiniMap()
    {
        Clicker.gameObject.SetActive(false);
        minicam.orthographicSize = 5.0f;
        AssignMiniCamtoPlayer();
        playerHead.transform.localScale = new Vector3(0.5f, 0.1f, 0.5f);
        touched = false;
        minicam.rect = MiniRect; //new Rect(0.83f, 0.8f, 0.27f, 0.31f);
        maxbtn.GetComponent<Image>().enabled = false;
        maxbtn.transform.localScale = new Vector3(2f, 2f, 0);
        Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, false);
    }

    public void TeleportPlayer(float floorx,float floorz)
    {
         b = FindFloorBounds();
        if (Mathf.Abs(floorx) < b.extents.x - 0.3f && Mathf.Abs(floorz) < b.extents.z - 0.3f)
        {
            Vector3 oldpos = player.transform.position;
            player.transform.position = new Vector3(floorx, b.center.y - 0.5f, floorz);  // Hardcoded because something else that is not at the same level of the floor is tagged as floor. 
            bool good = CheckArea();
            if (!good)
                player.transform.position = oldpos;
        }
        else
        {
            ToastIt();
        }
    }

    private bool CheckArea()
    {
        Collider[] hitColliders = Physics.OverlapSphere(player.transform.position, 0.5f);
        if (hitColliders.Length > 0)
        {
            ToastIt();
            return false;
        }
            return true;
    }




    private void AssignMiniCamtoPlayer()
    {
        minicam.transform.SetParent(null);
        minicam.transform.position = new Vector3(player.transform.position.x, 5, player.transform.position.z);
        minicam.transform.rotation = Quaternion.Euler(90, player.transform.eulerAngles.y, 0);
        minicam.transform.SetParent(player.transform);

    }

    public void HideMinimapandUIs(bool hide) 
    {
        if (hide)
        {

           // Messenger.Broadcast(StaticFunctionCalls.DeselectAsset);
            Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, true);

            Clicker.gameObject.SetActive(false); 
            minicam.gameObject.SetActive(false);
            maxbtn.gameObject.SetActive(false);
        }
        else
        {
            Clicker.gameObject.SetActive(true); 
            minicam.gameObject.SetActive(true);
            maxbtn.gameObject.SetActive(true);
            MinimiseMiniMap();
        }

    }

    private void ToastIt()
    {
        //WidgetManager.Instance.Toast("Cannot Teleport To Chosen Location");
    }

    private void GetAspectRatio()
    {
        float aspect = Camera.main.aspect;
        if(aspect>=1.77f)  // 16:9 or Greater
        {
            MaxRect = new Rect(0.4f, 0.4f, 1, 1);
            MiniRect = new Rect(0.9f, 0.8f, 1, 1);
        }
        else
        {

            MaxRect = new Rect(0.4f, 0.4f, 1, 1);
            MiniRect = new Rect(0.83f, 0.8f, 1, 1);
        }
    }
}
