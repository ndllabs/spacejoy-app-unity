﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity;
using _unity.Events;

using Homefuly;

public class SceneManagerWallPoster : MonoBehaviour
{
    public GameObject objRoom;
    GameObject go;
    public Player69FPS scrPlayer69FPS;
    public GameObject prefWallPoster;

    bool isDesignWallPosterMode;
    public GameObject pnlDesignWallPosterMode;
    bool restrictPosterMode;

    public GameObject wallPosterContainer;
    Transform currentWall;

    [Header("ASYNC OBJECTS")]
    public Texture[] posterList;

    bool canLongPress;

    // Start is called before the first frame update
    void Start()
    {
       
        scrPlayer69FPS.isEnabled = true;
        restrictPosterMode = false;
        DesignWallPosterMode(null);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        //Messenger<IDebugFPSSpeed>.AddListener(StaticFunctionCalls.DEBUG_FPS_VALUE_SET, onDEBUG_FPS_VALUE_SET);

        Messenger<GameObject>.AddListener(StaticFunctionCalls.onGameObjectTapped_Deprycated, onGameObjectTapped);

        Messenger<Transform>.AddListener(StaticFunctionCalls.WallposterDroppedOnWall, DesignWallPosterMode);
    }

    private void OnDisable()
    {
        //Messenger<IDebugFPSSpeed>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_VALUE_SET, onDEBUG_FPS_VALUE_SET);

        Messenger<GameObject>.RemoveListener(StaticFunctionCalls.onGameObjectTapped_Deprycated, onGameObjectTapped);

        Messenger<Transform>.RemoveListener(StaticFunctionCalls.WallposterDroppedOnWall, DesignWallPosterMode);
    }


    /*void onDEBUG_FPS_VALUE_SET(IDebugFPSSpeed debugVal)
    {

    }*/

    void onGameObjectTapped(GameObject go)
    {

    }


    void DesignWallPosterMode(Transform trans)
    {

        bool isOn = trans != null;
       
        isDesignWallPosterMode = isOn;

        scrPlayer69FPS.isEnabled = !isDesignWallPosterMode;
        pnlDesignWallPosterMode.SetActive(isDesignWallPosterMode);

        if (isDesignWallPosterMode)
        {
           
          
            if (!restrictPosterMode)
            {
                currentWall = trans;
                Messenger<Transform>.Broadcast(StaticFunctionCalls.PlayerFPSLookAt, trans);
                restrictPosterMode = true;
            }

        }
        else
        {
           
            restrictPosterMode = false;
            currentWall = null;
            // scrPlayer69FPS.ResetLook();
        }


    }


    public void exitWallPosterMode()
    {
        DesignWallPosterMode(null);
 
        Messenger<int>.Broadcast(StaticFunctionCalls.DeselectAllPosters, 1);
      
    }

    public void SelectWallPoster(int val)
    {
        //        print("Selected POster: " + val);
        Vector3 pos, scale, rot;

       go = Instantiate(prefWallPoster, wallPosterContainer.transform);

        float normalizedY = ((Vector3)currentWall.GetCenter()).y;
        pos = currentWall.GetAlignedTransformSurface();
        pos.y = normalizedY;

        scale = currentWall.GetRealScale();
        //scale.z = .1f;

        Vector3 lookPoint = go.transform.position - Camera.main.transform.position;
        lookPoint.y = Camera.main.transform.position.y;

        go.transform.LookAt(lookPoint);
        rot.x = rot.z = 0;
        rot.y = go.transform.root.eulerAngles.y;

        go.transform.position = pos;
        //go.transform.localScale = scale;
        go.transform.LookAt(scrPlayer69FPS.gameObject.transform, Vector3.up);
        rot = go.transform.rotation.eulerAngles;
        rot.x = rot.z = 0;
        go.transform.rotation = Quaternion.Euler(rot);



        Texture2D tex = posterList[val] as Texture2D;
        go.transform.GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
       // go.transform.GetComponent<PosterStateManager>().wall1 = currentWall.gameObject;
        Messenger<GameObject>.Broadcast(StaticFunctionCalls.GetWallPosterisOn, currentWall.gameObject);


    }

    public void OnStart()
    {
        ProcessWalls(objRoom.transform);
    }

    void ProcessWalls(Transform transRoom)
    {
        foreach (Transform t in transRoom)
        {
            if (!t.name.ToLower().ContainsAny("wall"))
                continue;

            t.tag = "Wall";
            t.gameObject.layer = 17;


            t.gameObject.AddComponent<WallHandler>();

            //GameObject go = Instantiate(prefWallPoster, t);
            //go.transform.position = (Vector3)t.GetCenter();
            //go.transform.localScale = t.GetRealScale();


            //print(t.name + " : " + t.GetCenter());
            //print(t.name + " = " + GetRealCoords(t).ToArrString());
            //print(t.name + " = " + t.GetRealScale());
        }
    }


    Vector2[] GetRealCoords(Transform t)
    {
        Mesh mesh = t.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices;
        Vector2[] uvs = new Vector2[vertices.Length];
        Bounds bounds = mesh.bounds;
        int i = 0;
        while (i < uvs.Length)
        {
            uvs[i] = new Vector2(vertices[i].x / bounds.size.x, vertices[i].z / bounds.size.x);
            i++;
        }
        mesh.uv = uvs;

        return mesh.uv;
    }


}
