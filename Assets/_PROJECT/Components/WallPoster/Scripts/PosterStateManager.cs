﻿using System.Collections;
using System.Collections.Generic;
using _unity.Events;
using UnityEngine;
//using TouchScript.Gestures.TransformGestures;
//using TouchScript.Behaviors;
//using System.Threading.Tasks;
//using TouchScript.Gestures;
using _unity;
using System;
using UnityEngine.UI;

using Homefuly;

public enum PosterTransformMode
{
    idle,
    horizontal_Translate,
    vertical_Translate,
    scale
}

//[RequireComponent(typeof(LongPressGesture))]
//[RequireComponent(typeof(TapGesture))]
[RequireComponent(typeof(BoxCollider))]
public class PosterStateManager : MonoBehaviour
{
    [HideInInspector]   
    public GameObject MyWall, Another_Wall;

    private GameObject MyBorder;
    int Counter = 0;
    MeshRenderer poster_renderer;
    //private LongPressGesture longPressGesture;
    //private TapGesture tapGesture;
    Renderer wall_renderer;
    Vector3 poster_position_x,poster_position_z,poster_scale_x,poster_scale_z;

    //TransformGesture transformGesture;
    //Transformer transformer;
    float aspectMultiplierx;
    private float aspectMultipliery;

    [HideInInspector]
    public PosterTransformMode posterTransformMode;
    public bool isSelected = false, isInit = false;

    //WallPosterEditState currentWallposterState = WallPosterEditState.Idle;

    //PosterTransformMode defaultTransformMode = PosterTransformMode.idle;

    private LineRenderer leftLR;
    private Material lineMaterial;
    private Gradient gradient = new Gradient();

    private void Awake()
    {
        SetupLineRendVars();

        Debug.LogWarning("[Deprycation Warning] class PosterStateManager_Deprycated no more useful");


        //longPressGesture = transform.GetOrAddComponent<LongPressGesture>();
        //longPressGesture.TimeToPress = 0.45f;
        //longPressGesture.DistanceLimit = 0.1f;

        //tapGesture = transform.GetOrAddComponent<TapGesture>();
    }

    public void Init()
    {
        if (!isInit)
        {
            poster_position_x = poster_position_z = this.gameObject.transform.position;
            poster_scale_z = poster_scale_x = this.gameObject.transform.localScale;

            //transformGesture = this.gameObject.GetComponent<TransformGesture>();
            //transformer = this.gameObject.GetComponent<Transformer>();
            poster_renderer = gameObject.GetComponent<MeshRenderer>();

            ChangeEditState(WallPosterEditState.Idle);

            Texture tex = poster_renderer.material.mainTexture;

            aspectMultiplierx = Math.Abs((float)tex.width / (float)tex.height);
            aspectMultipliery = Math.Abs((float)tex.height / (float)tex.width);

            isInit = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    private void Update()
    {
        if (isSelected)
            ClampToWall();
    }


    private void OnEnable()
    {
        //longPressGesture.LongPressed += OnLongPressed;
        //tapGesture.Tapped += OnTapped;
        Messenger<GameObject>.AddListener(StaticFunctionCalls.GetWallPosterisOn, AssignWall);

        Messenger.AddListener(StaticFunctionCalls.WallpaperResize_Deprycated, OnResizeWallposter);
        Messenger.AddListener(StaticFunctionCalls.WallpaperPosition_Deprycated, WallpaperPosition);
        Messenger.AddListener(StaticFunctionCalls.WallpaperDeselected, OnDeselectWallpaper);

        //Messenger<Transform, Texture>.AddListener(StaticFunctionCalls.WallposterSelected, WallposterSelected);
    }


    private void OnDisable()
    {
        //longPressGesture.LongPressed -= OnLongPressed;
        //tapGesture.Tapped -= OnTapped;
        Messenger<GameObject>.RemoveListener(StaticFunctionCalls.GetWallPosterisOn, AssignWall);

        Messenger.RemoveListener(StaticFunctionCalls.WallpaperResize_Deprycated, OnResizeWallposter);
        Messenger.RemoveListener(StaticFunctionCalls.WallpaperPosition_Deprycated, WallpaperPosition);
        Messenger.RemoveListener(StaticFunctionCalls.WallpaperDeselected, OnDeselectWallpaper);

        //Messenger<Transform, Texture>.RemoveListener(StaticFunctionCalls.WallposterSelected, WallposterSelected);
    }

    private void OnResizeWallposter() {
        ChangeEditState(WallPosterEditState.Scale);
    }
    private void WallpaperPosition() {
        ChangeEditState(WallPosterEditState.Move);
    }
    private void OnDeselectWallpaper() {
        isSelected = false;
        ChangeEditState(WallPosterEditState.Idle);
    }
    //private void WallposterSelected(Transform wall, Texture wpp) {
    //    if (wpp.name == gameObject.name) isSelected = true;
    //}
    private void OnLongPressed(object sender, EventArgs e) {
        if (HomefulyExtensions.PointIsOverUI()) return;
        poster_renderer.material.mainTexture.name = gameObject.name.Replace("Poster_", "");
        Messenger<Transform, Texture>.Broadcast(StaticFunctionCalls.WallposterLongPressed_Deprycated, transform.parent, gameObject.GetComponent<MeshRenderer>().material.mainTexture); // gameObject.GetComponent<SpriteRenderer>().sprite.texture);
    }
    private void OnTapped(object sender, EventArgs e){
        if (HomefulyExtensions.PointIsOverUI()) return;
        poster_renderer.material.mainTexture.name = gameObject.name.Replace("Poster_", "");
        Messenger<Transform, Texture>.Broadcast(StaticFunctionCalls.WallposterSelected, transform.parent, poster_renderer.material.mainTexture);
    }


    #region Assign the wall poster is on

    void AssignWall(GameObject w)
    {
        if (Counter < 1) {
            Counter++; MyWall = w;
        }
        else {
            Another_Wall = w;
            transform.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }



    #endregion


    public void ChangeEditState(WallPosterEditState state)
    {
        if (!isSelected) state = WallPosterEditState.Idle;
        
        switch (state)
        {
            case WallPosterEditState.Idle:
                MyBorder.SetActive(false);
                isSelected = false;
                //gameObject.GetComponent<TransformGesture>().Type = TransformGesture.TransformType.None;
                break;
            case WallPosterEditState.Selected:
                isSelected = true;
                MyBorder.SetActive(true);
                MyBorder.layer = 19;
                //gameObject.GetComponent<TransformGesture>().Type = TransformGesture.TransformType.Translation | TransformGesture.TransformType.Scaling;//TransformGesture.TransformType.None;
                break;
            case WallPosterEditState.Move:
                MyBorder.SetActive(true);
                MyBorder.layer = 19; //"WallEditLayer"
                //gameObject.GetComponent<TransformGesture>().Type = TransformGesture.TransformType.Translation | TransformGesture.TransformType.Scaling;//TransformGesture.TransformType.Translation; 
                break;
            case WallPosterEditState.Scale:
                MyBorder.SetActive(true);
                MyBorder.layer = 19; //"WallEditLayer"
                //gameObject.GetComponent<TransformGesture>().Type = TransformGesture.TransformType.Translation | TransformGesture.TransformType.Scaling;//TransformGesture.TransformType.Scaling;
                break;
        }
    }

 

    #region Clamping 
    
    //temp
    public void ClampToWall()
    {
        if (MyWall != null && isInit)
        {
            wall_renderer = MyWall.gameObject.GetComponent<Renderer>();

            if (Mathf.Abs(wall_renderer.bounds.size.x) > Mathf.Abs(wall_renderer.bounds.size.z))
            {
                if (poster_scale_x.x >= Math.Abs(wall_renderer.bounds.max.x - wall_renderer.bounds.min.x))
                {
                    poster_scale_x.x = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.x), 0, Math.Abs(wall_renderer.bounds.max.x - wall_renderer.bounds.min.x)); //Math.Abs(wall_renderer.bounds.max.x - wall_renderer.bounds.min.x);
                    poster_scale_x = new Vector3(poster_scale_x.x, poster_scale_x.x * aspectMultipliery, 1);
                }
                else if (poster_scale_x.y >= Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y))
                {
                    poster_scale_x.y = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.y), 1, Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y)); //Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y);
                    poster_scale_x = new Vector3(poster_scale_x.y * aspectMultiplierx, poster_scale_x.y, 1);
                }
                else
                {
                    poster_scale_x.x = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.x), 1, Math.Abs(wall_renderer.bounds.max.x - wall_renderer.bounds.min.x));
                    poster_scale_x.y = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.y), 1, Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y));
                    //poster_scale_x = new Vector3(poster_scale_x.y * aspectMultiplierx, poster_scale_x.y, 1);
                    if (aspectMultiplierx < aspectMultipliery) poster_scale_x = new Vector3(poster_scale_x.y * aspectMultiplierx, poster_scale_x.y, 1);
                    else poster_scale_x = new Vector3(poster_scale_x.x, poster_scale_x.x * aspectMultipliery, 1);
                }
                gameObject.transform.localScale = poster_scale_x;

                poster_position_x.x = Mathf.Clamp(poster_renderer.transform.position.x, wall_renderer.bounds.min.x + poster_renderer.bounds.size.x / 2, wall_renderer.bounds.max.x - poster_renderer.bounds.size.x / 2);
                poster_position_x.y = Mathf.Clamp(poster_renderer.transform.position.y, wall_renderer.bounds.min.y + poster_renderer.bounds.size.y / 2, wall_renderer.bounds.max.y - poster_renderer.bounds.size.y / 2);
                gameObject.transform.position = poster_position_x;
            }
            else
            {
                if (poster_scale_z.x >= Math.Abs(wall_renderer.bounds.max.z - wall_renderer.bounds.min.z)){
                    //Debug.Log("Scale x of current wall went out of bounds: " + poster_renderer.bounds.size.x + " calmping to" + Math.Abs(wall_renderer.bounds.max.z - wall_renderer.bounds.min.z));
                    poster_scale_z.x = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.x), 0, Math.Abs(wall_renderer.bounds.max.z - wall_renderer.bounds.min.z));  //Math.Abs(wall_renderer.bounds.max.z - wall_renderer.bounds.min.z);
                    poster_scale_z = new Vector3(poster_scale_z.x, poster_scale_z.x * aspectMultipliery, 1);
                }
                else if (poster_scale_z.y >= Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y)){
                    poster_scale_z.y = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.y), 0, Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y)); //Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y);
                    poster_scale_z = new Vector3(poster_scale_z.y * aspectMultiplierx, poster_scale_z.y, 1);
                }
                else{
                    poster_scale_z.x = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.z), 0, Math.Abs(wall_renderer.bounds.max.z - wall_renderer.bounds.min.z));
                    poster_scale_z.y = Mathf.Clamp(Math.Abs(poster_renderer.bounds.size.y), 0, Math.Abs(wall_renderer.bounds.max.y - wall_renderer.bounds.min.y));
                    if (aspectMultiplierx < aspectMultipliery) poster_scale_z = new Vector3(poster_scale_z.y * aspectMultiplierx, poster_scale_z.y, 1);
                    else poster_scale_z = new Vector3(poster_scale_z.x, poster_scale_z.x * aspectMultipliery, 1);
                }
                gameObject.transform.localScale = poster_scale_z;

                poster_position_z.z = Mathf.Clamp(poster_renderer.transform.position.z, wall_renderer.bounds.min.z + poster_renderer.bounds.size.z / 2, wall_renderer.bounds.max.z - poster_renderer.bounds.size.z / 2);
                poster_position_z.y = Mathf.Clamp(poster_renderer.transform.position.y, wall_renderer.bounds.min.y + poster_renderer.bounds.size.y / 2, wall_renderer.bounds.max.y - poster_renderer.bounds.size.y / 2);
                gameObject.transform.position = poster_position_z;
            }
        }
        WallposterSelectCue();
    }

    #endregion

    #region util

    void WallposterSelectCue()
    {
        UpdateRenderCuePos(leftLR, new List<Vector3> { new Vector3(0.5f, 0.5f, 0), new Vector3(-0.5f, 0.5f, 0), new Vector3(-0.5f, -0.5f, 0), new Vector3(0.5f, -0.5f, 0), new Vector3(0.5f, 0.5f, 0), new Vector3(0.5f, 0.5f, 0) });
    }

    void DrawLineRenderer(LineRenderer lr, List<Vector3> positions)
    {
        lr.startWidth = 0.03f;
        lr.endWidth = 0.03f;
        lr.material = lineMaterial;
        lr.positionCount = positions.Count;
        lr.SetPositions(positions.ToArray());
        lr.colorGradient = gradient;
    }

    void UpdateRenderCuePos(LineRenderer lr, List<Vector3> positions)
    {
        lr.positionCount = positions.Count;
        lr.SetPositions(positions.ToArray());
    }

    void SetupLineRendVars()
    {
        GameObject leftLine = new GameObject("leftLine");
        leftLine.transform.SetParent(transform, false);
        leftLR = leftLine.AddComponent<LineRenderer>();
        leftLR.useWorldSpace = false;
        leftLR.numCornerVertices = 10;
        MyBorder = leftLine;
        MyBorder.SetActive(false);

        GradientColorKey[] colorKey = new GradientColorKey[2];
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];
        lineMaterial = new Material(Shader.Find("Sprites/Default"));
        Color lineColor1 = HexToColor("#00F5FF"); //LBluesish //("#f05a46"); orrangish
        Color lineColor2 = HexToColor("#00F5FF");
        colorKey[0].color = lineColor1;
        colorKey[0].time = 0.0f;
        colorKey[1].color = lineColor2;
        colorKey[1].time = 1.0f;
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;
        gradient.SetKeys(colorKey, alphaKey);
        
        DrawLineRenderer(leftLR, new List<Vector3> { (Vector3.one / 2), new Vector3(-0.5f, 0.5f, 0), (Vector3.one / -2), new Vector3(0.5f, -0.5f, 0), (Vector3.one / 2) });
    }

    static Color HexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }
    #endregion

    public enum WallPosterEditState
    {
        Idle,
        Selected,
        Move,
        Scale
    }
    
    #region DumpYard

    public void OnGameObjectTapped(GameObject g)
    {
        throw new NotImplementedException();
        //if ( gameObject == g)
        //{
        //Debug.Log("Im the one who is tapped");
        //}
        //throw new NotImplementedException();
        // if (g.tag == "Poster" && isSelected==false &&MyWall==Another_Wall)
        // {
        //     SelectSelf();
        // }
        // else
        //     if(g.tag!="Poster" && isSelected==true)
        // {
        //     DeselectSelf(1);   
        // }
        ////else
        // if(g.tag=="Wall" && MyWall == Another_Wall)
        // {
        //     SelectSelf();  //But when a new poster is placed on the wall, This case would be called for the already existing wall. Making SelecSelf() function be called twice.
        // }
    }

    public void SelectSelf()
    {
        throw new NotImplementedException();
        //isSelected = true; //!isSelected;
        //transformer.enabled = true; //!transformer.enabled;
        //posterTransformMode = PosterTransformMode.horizontal_Translate;
        //Debug.Log(transform.name + " is now in " + posterTransformMode + " Mode");
        //Messenger<Transform>.Broadcast(StaticFunctionCalls.WallposterDroppedOnWall, MyWall.transform);
        //transform.gameObject.GetComponent<BoxCollider>().enabled = true;
        //this.gameObject.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;//!this.gameObject.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled;
        //this.gameObject.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;//!this.gameObject.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled;
    }

    public void DeselectSelf(int i)
    {
        throw new NotImplementedException();
        //isSelected = false;
        //transformer.enabled = false;
        //transform.gameObject.GetComponent<BoxCollider>().enabled = false;
        //posterTransformMode = PosterTransformMode.idle;
        //Debug.Log(transform.name + "is now in " + posterTransformMode + " Mode");
        //this.gameObject.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;//!this.gameObject.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled;
        //this.gameObject.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;//!this.gameObject.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled;

    }

    private void TransformCompleted(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void Transformed(object sender, EventArgs e)
    {
        throw new NotImplementedException();
        //if (MyWall != null)
        //{
        //    wall_renderer = MyWall.gameObject.GetComponent<Renderer>();
        //    poster_renderer.bounds.size.y, 0, Mathf.Abs(wall_renderer.bounds.min.y) + Mathf.Abs(wall_renderer.bounds.max.y)

        //    if()
        //    transformGesture.Cancel(true, true);
        //}
        //throw new NotImplementedException();
    }

    private void TransformStarted(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    /*
    //if (MyWall != null)
    //{
    //    wall_renderer = MyWall.gameObject.GetComponent<Renderer>();
    //    poster_renderer = gameObject.GetComponent<SpriteRenderer>();

    //    Debug.Log("scaleie: " +

    //    wall_renderer.bounds.min + " lol " + wall_renderer.bounds.max);
    //    if (wall_renderer.bounds.size.x > wall_renderer.bounds.size.z)
    //    {

    //        poster_position_x.x = Mathf.Clamp(poster_renderer.transform.position.x, wall_renderer.bounds.min.x + poster_renderer.bounds.size.x / 2, wall_renderer.bounds.max.x - poster_renderer.bounds.size.x / 2);
    //        poster_position_x.y = Mathf.Clamp(poster_renderer.transform.position.y, wall_renderer.bounds.min.y + poster_renderer.bounds.size.y / 2, wall_renderer.bounds.max.y - poster_renderer.bounds.size.y / 2);
    //        gameObject.transform.position = poster_position_x;

    //        poster_scale_x.x = Mathf.Clamp(poster_renderer.bounds.size.x, wall_renderer.bounds.min.x, wall_renderer.bounds.max.x);
    //        poster_scale_x.y = Mathf.Clamp(poster_renderer.bounds.size.y, wall_renderer.bounds.min.y, wall_renderer.bounds.max.y);
    //        gameObject.transform.localScale = poster_scale_x;


    //    }
    //    else
    //    {


    //        poster_position_z.z = Mathf.Clamp(poster_renderer.transform.position.z, wall_renderer.bounds.min.z + poster_renderer.bounds.size.z / 2, wall_renderer.bounds.max.z - poster_renderer.bounds.size.z / 2);
    //        poster_position_z.y = Mathf.Clamp(poster_renderer.transform.position.y, wall_renderer.bounds.min.y + poster_renderer.bounds.size.y / 2, wall_renderer.bounds.max.y - poster_renderer.bounds.size.y / 2);
    //        gameObject.transform.position = poster_position_z;



    //        poster_scale_z.z = Mathf.Clamp(poster_renderer.bounds.size.z, wall_renderer.bounds.min.z, wall_renderer.bounds.max.z);
    //        poster_scale_z.y = Mathf.Clamp(poster_renderer.bounds.size.y, wall_renderer.bounds.min.y, wall_renderer.bounds.max.y);
    //        gameObject.transform.localScale = poster_scale_z;


    //    }
    //}*/

    #endregion
}
