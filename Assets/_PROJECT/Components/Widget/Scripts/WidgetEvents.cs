﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WidgetEvents : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void BackdropStopped()
    {
        WidgetManager.Instance.CloseBackdropPost();

    }



    public void ToastAnimaionStopped()
    {
        WidgetManager.Instance.ToastClosePost();
    }


    public void AlertAnimStopped()
    {
        WidgetManager.Instance.AlertClosePost();
    }
}
