﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using _unity;
using TMPro;
using UnityEngine.EventSystems;
using System.Threading;
using System.Threading.Tasks;

using Homefuly;
using System.Collections.Generic;
using Homefuly.Events;

public class WidgetManager : MonoBehaviour
{
    protected WidgetManager() { }

    #region Singleton

    public static WidgetManager Instance;


    #endregion

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #region references

    [Header("Global")]
    public bool canUse = true;
    bool isInit;
    public GameObject pnlBackdrop;


    [Header("Alert")]
    public GameObject pnlAlert;
    public TextMeshProUGUI txtTitle_pnlAlert;
    public TextMeshProUGUI txtMsg_pnlAlert;
    public TextMeshProUGUI txtButtonText;
    public Button btnAffirm_pnlAlert;
    public Button btnClose_pnlAlert;


    [Header("Confirm")]
    public GameObject pnlCOnfirm;
    public TextMeshProUGUI txtTitle_pnlCOnfirm;
    public TextMeshProUGUI txtMsg_pnlCOnfirm;
    public Button btnAffirm_pnlCOnfirm;
    public Button btNegate_pnlCOnfirm;
    public Button btDismiss_pnlCOnfirm;

    [Header("ConfirmToast")]
    public GameObject pnlConfirmToast;
    public TextMeshProUGUI txtTitle_pnlConfirmToast;
    public Button btnAffirm_pnlConfirmToast;
    public Image imgHelper;
    CancellationTokenSource ConfirmToastToken;

     [Header("ConfirmSave")]
    public GameObject pnlConfirmSave;
    // public TextMeshProUGUI txtTitle_pnlConfirm;
    // public TextMeshProUGUI txtMsg_pnlConfirm;
    public Button btnAffirm_pnlConfirmSave;
    public Button btnNegate_pnlConfirmSave;
    public Button btnCancel_pnlConfirm;

    [Header("Prompt")]
    public GameObject pnlPrompt;
    //public GameObject elemPromptpnl;
    public TextMeshProUGUI txtTitle_pnlPrompt;
    //public Text txtBody_pnlPrompt;
    //public TextMeshProUGUI inputField;
    public TMP_InputField inputField;
    public Button btnAffirm_pnlPrompt;
    public Button btNegate_pnlPrompt;


    [Header("LoaderMini")]
    public GameObject pnlLoaderMini;
    public TextMeshProUGUI txtTitle_pnlLoaderMini;
    public Button btnAffirm_pnlLoaderMini;

    [Header("Loader Auth")]
    public GameObject pnlLoaderAuth;
    public TextMeshProUGUI txtTitle_pnlLoaderAuth;
    public Button btnAffirm_pnlLoaderAuth;


    [Header("Loader")]
    public GameObject pnlLoader;
    public TextMeshProUGUI txtTitle_pnlLoader;
    public Slider LoaderProgressSlider;
    //bool progressLoader = true;
    //public RawImage LoaderProgressThumbnail;
    //public Texture defaultTexture;


    [Header("Toast")]
    public GameObject pnlToast;
    public TextMeshProUGUI txtMsg_pnlToast;
    CancellationTokenSource ToastToken;

    //[Header("Tutorial")]
    //public GameObject pnlTutorial;
    //public GameObject pnlTutorialContainer;
    //public string[] tutorialAnim;
    ////public GameObject btnHelp;
    //public GameObject btnClose;
    //public GameObject tutorialButtonLeft;
    //public GameObject tutorialButtonRight;
    //int tutorialNumber;
    //int totalTutorials;

    [Header("AuthPanel")]
    public GameObject authPanel;
    public GameObject SignInPanel;
    public GameObject SignUpPanel;
    public GameObject debugPanel;
    public GameObject notNowText;
    public GameObject closeBox;
    public GameObject AuthDescription;
    public AuthManager authMan;
    public ScrollRect authScroll;
    public List<Button> authButtons;
    Action<bool> SignInCallback;
    #endregion

    // Use this for initialization
    void Start()
    {
        //tutorialNumber = 0;
        //totalTutorials = tutorialAnim.Length;

        //sliderOBJ.maxValue = maxtimeOBJ;
        //sliderSKP.maxValue = maxtimeSKP;
    }

    public void Init()
    {
        if (!isInit)
        {
            //_.l("Widget System Init ...");
            isInit = true;
        }
        // AuthPanelOpen();
    }

    #region Backdrop

    public void OnBackdropClick()
    {
        //Disable any active pnl
        if (pnlLoaderMini.activeSelf)
        {
            LoaderMiniClose();
        }

        if (pnlAlert.activeSelf)
        {
            AlertClose();

        }

        if (pnlCOnfirm.activeSelf)
        {
            ConfirmClose();
        }
    }

    public void ShowBackdrop()
    {
        if (!pnlBackdrop.activeSelf) pnlBackdrop.SetActive(true);
    }

    public void CloseBackdrop()
    {
        CloseBackdropPost();
        //pnlBackdrop.GetComponent<Animator>().SetTrigger("stop");
    }

    public void CloseBackdropPost()
    {
        pnlBackdrop.SetActive(false);
    }

    #endregion

    #region Alert

    public void Alert(string title, string msg, Action Callback = null, string CustomizedOK = "", bool SetCloseBtn = false)
    {
        ShowBackdrop();
        pnlAlert.SetActive(true);

        if(CustomizedOK =="")
        {
            txtButtonText.text = "OK";
        }
        else
        {
            txtButtonText.text = CustomizedOK;
        }
        btnClose_pnlAlert.gameObject.SetActive(SetCloseBtn);

        txtTitle_pnlAlert.text = title.Trim().ToTitleCase();
        Debug.Log("list in alert: " + msg);
        txtMsg_pnlAlert.text = msg;

        btnAffirm_pnlAlert.onClick.RemoveAllListeners();
        if (Callback != null)
        {
            btnAffirm_pnlAlert.onClick.AddListener(() =>
            {
                AlertClosePost();
                Callback();
            });
        }
        else
        {
            btnAffirm_pnlAlert.onClick.AddListener(() =>
            {
                AlertClosePost();
            });
        }
        btnAffirm_pnlAlert.Select();
    }

    public void AlertClose()
    {
        CloseBackdrop();
        pnlAlert.GetComponent<Animator>().SetTrigger("stop");
    }

    public void AlertClosePost()
    {
        //print("Closing ..");
        txtTitle_pnlAlert.text = "";
        txtMsg_pnlAlert.text = "";
        btnAffirm_pnlAlert.onClick.RemoveAllListeners();
        CloseBackdrop();
        pnlAlert.SetActive(false);
    }

    #endregion

    #region Confirm

    public void Confirm(string title, string msg, Action<string> Callback = null, bool btdismissActivate = true)
    {
        ShowBackdrop();
        pnlCOnfirm.SetActive(true);
        btDismiss_pnlCOnfirm.gameObject.SetActive(btdismissActivate);
        txtTitle_pnlCOnfirm.text = title.Trim().ToTitleCase();
        txtMsg_pnlCOnfirm.text = msg;

        btnAffirm_pnlCOnfirm.onClick.RemoveAllListeners();
        btNegate_pnlCOnfirm.onClick.RemoveAllListeners();
        btDismiss_pnlCOnfirm.onClick.RemoveAllListeners();
        
        if (Callback != null)
        {
            btnAffirm_pnlCOnfirm.onClick.AddListener(() =>
            {
                ConfirmClose();
                Callback("yes");
            });

            btNegate_pnlCOnfirm.onClick.AddListener(() =>
            {
                ConfirmClose();
                Callback("no");
            });

            btDismiss_pnlCOnfirm.onClick.AddListener(() =>
            {
                ConfirmClose();
                Callback("dismiss");
            });
        }

    }
    public void Confirm(string title, string msg, Texture2D imgTex, Action<string> Callback = null)
    {
        imgHelper.sprite = Sprite.Create(imgTex, new Rect(0, 0, imgTex.width, imgTex.height), new Vector2(.5f, .5f));
        Confirm(title, msg, Callback);
    }

    public void ConfirmClose()
    {
        CloseBackdrop();
        txtTitle_pnlCOnfirm.text = "";
        txtMsg_pnlCOnfirm.text = "";
        imgHelper.gameObject.SetActive(false);
        btnAffirm_pnlCOnfirm.onClick.RemoveAllListeners();
        btNegate_pnlCOnfirm.onClick.RemoveAllListeners();
        pnlCOnfirm.SetActive(false);
    }

        public void ConfirmSave(Action<string> Callback = null)
    {
        ShowBackdrop();
        pnlConfirmSave.SetActive(true);
        btnAffirm_pnlConfirmSave.onClick.RemoveAllListeners();
        btnNegate_pnlConfirmSave.onClick.RemoveAllListeners();
        btnCancel_pnlConfirm.onClick.RemoveAllListeners();
        
        if (Callback != null)
        {
            btnAffirm_pnlConfirmSave.onClick.AddListener(() =>
            {
                ConfirmSaveClose();
                Callback("yes");
            });

            btnNegate_pnlConfirmSave.onClick.AddListener(() =>
            {
                ConfirmSaveClose();
                Callback("no");
            });

            btnCancel_pnlConfirm.onClick.AddListener(() =>
            {
                ConfirmSaveClose();
                Callback("dismiss");
            });
        }
    }

    public void ConfirmSaveClose()
    {
        CloseBackdrop();
        btnAffirm_pnlConfirmSave.onClick.RemoveAllListeners();
        btnNegate_pnlConfirmSave.onClick.RemoveAllListeners();
        btnCancel_pnlConfirm.onClick.RemoveAllListeners();
        pnlConfirmSave.SetActive(false);
    }

    #endregion

    #region ConfirmToast
    public void ConfirmToast(string title, bool isHelperIcon, int timeout = 5, Action Callback = null)
    {
        if (isHelperIcon) imgHelper.gameObject.SetActive(true);
        ConfirmToast(title, timeout, Callback);
    }

    async public void ConfirmToast(string title, int timeout = 5, Action Callback = null)
    {
        if (pnlConfirmToast.activeSelf && ConfirmToastToken != null)
            ConfirmToastToken.Cancel();

        ConfirmToastToken = new CancellationTokenSource();

        txtTitle_pnlConfirmToast.text = title.Trim();

        btnAffirm_pnlConfirmToast.onClick.RemoveAllListeners();

        if (Callback != null)
        {
            btnAffirm_pnlConfirmToast.onClick.AddListener(() =>
            {
                Callback();
                ConfirmToastToken.Cancel();
                ConfirmToastClose();
            });

        }
        pnlConfirmToast.SetActive(true);
        try
        {
            await Task.Delay(timeout * 1000, ConfirmToastToken.Token);
            ConfirmToastClose();
        }
        catch (OperationCanceledException) { }
    }

    public void ConfirmToastClose()
    {
        txtTitle_pnlConfirmToast.text = "";
        btnAffirm_pnlConfirmToast.onClick.RemoveAllListeners();
        imgHelper.gameObject.SetActive(false);
        pnlConfirmToast.SetActive(false);
    }

    #endregion

    #region Prompt

    public void Prompt(string title, Action<string> Callback = null, bool setNegate = true)
    {

        ShowBackdrop();

        /*GameObject promptObject = Instantiate(elemPromptpnl) as GameObject;
        promptObject.transform.SetParent(transform.Find("Panel").transform ,false);
        promptObject.SetActive(true);
        promptObject.transform.Find("InputField/Placeholder").GetComponent<Text>().text = "";
        promptObject.transform.Find("btnOk").GetComponent<Button>().onClick.RemoveAllListeners();
        promptObject.transform.Find("btnCancel").GetComponent<Button>().onClick.RemoveAllListeners();
        //promptObject.transform.Find("txtTitle").GetComponent<Text>().text = title.Trim();
        promptObject.transform.Find("InputField").GetComponent<InputField>().Select();
        if (Callback != null)
        {
            promptObject.transform.Find("btnOk").GetComponent<Button>().onClick.AddListener(() =>
            {
                Callback(promptObject.transform.Find("InputField").GetComponent<InputField>().text);
                CloseBackdrop();
                Destroy(promptObject);
            });

            promptObject.transform.Find("btnCancel").GetComponent<Button>().onClick.AddListener(() =>
            {
                Callback(string.Empty);
                CloseBackdrop();
                Destroy(promptObject);
            });
        }*/



        pnlPrompt.SetActive(true);
        txtTitle_pnlPrompt.text = title.Trim().ToTitleCase();
        inputField.characterLimit = 30;
        // btNegate_pnlPrompt.gameObject.SetActive(setNegate);


        btnAffirm_pnlPrompt.onClick.RemoveAllListeners();
        btNegate_pnlPrompt.onClick.RemoveAllListeners();

        if (Callback != null)
        {
            btnAffirm_pnlPrompt.onClick.AddListener(() =>
            {
                Callback(inputField.text);
                PromptClose();
            });

            btNegate_pnlPrompt.onClick.AddListener(() =>
            {
                if (setNegate)
                {
                    Callback(string.Empty);
                }
                PromptClose();
            });
        }


    }

    public void PromptClose()
    {

        CloseBackdrop();
        txtTitle_pnlPrompt.text = "";
        inputField.text = "";
        //txtBody_pnlPrompt.text = string.Empty;
        //pnlPrompt.transform.Find("InputField/Placeholder").GetComponent<Text>().text = "";
        btnAffirm_pnlPrompt.onClick.RemoveAllListeners();
        btNegate_pnlPrompt.onClick.RemoveAllListeners();
        pnlPrompt.SetActive(false);
    }

    public void OnLoseFocus()
    {
        StartCoroutine(ActivateInputFieldWithoutSelection(inputField));
    }

    IEnumerator ActivateInputFieldWithoutSelection(TMP_InputField inputField)
    {
        inputField.ActivateInputField();
        // wait for the activation to occur in a lateupdate
        yield return new WaitForEndOfFrame();
        // make sure we're still the active ui
        if (EventSystem.current.currentSelectedGameObject == inputField.gameObject)
        {
            // To remove hilight we'll just show the caret at the end of the line
            inputField.MoveTextEnd(false);
        }
    }
    #endregion

    #region LoaderMini

    public void LoaderMini(string title, float timeoutCloseBtnShowAfter = 15f)
    {
        ShowBackdrop();
        if (!pnlLoaderMini.activeSelf) pnlLoaderMini.SetActive(true);

        btnAffirm_pnlLoaderMini.gameObject.SetActive(false);

        if (!string.IsNullOrEmpty(title))
            txtTitle_pnlLoaderMini.text = title.Trim().ToTitleCase();
        StartCoroutine(_exec(timeoutCloseBtnShowAfter, () =>
        {
            btnAffirm_pnlLoaderMini.onClick.RemoveAllListeners();
            btnAffirm_pnlLoaderMini.onClick.AddListener(() =>
            {
                LoaderMiniClose();
            });

            btnAffirm_pnlLoaderMini.gameObject.SetActive(true);
        }));
    }

    public void LoaderMiniClose()
    {
        CloseBackdrop();
        txtTitle_pnlLoaderMini.text = string.Empty;
        btnAffirm_pnlLoaderMini.gameObject.SetActive(false);
        pnlLoaderMini.SetActive(false);
    }

    #endregion

    #region Loader

    public void Loader(string title, float timeoutCloseBtnShowAfter = 15f)
    {
        //ShowBackdrop();
        if (!pnlLoader.activeSelf) pnlLoader.SetActive(true);

        if (!string.IsNullOrEmpty(title))
            txtTitle_pnlLoader.text = title.Trim().ToTitleCase();

        LoaderProgressSlider.value = 0f;
        //LoaderProgressThumbnail.texture = defaultTexture;
    }
    /*public IEnumerator LoaderProgressThumbnailManager(string textureUrl)
    {
        UnityWebRequest www1 = UnityWebRequest.Get(textureUrl);
        DownloadHandlerTexture tx1 = new DownloadHandlerTexture(true);
        www1.downloadHandler = tx1;
        yield return www1.SendWebRequest();
        if (www1.isNetworkError || www1.isHttpError)
        {
            Debug.LogError(www1.error);   //handle server errors here
            //Debug.Log("Using default image");
            LoaderProgressThumbnail.sprite = null;
        }
        else
        {
            Texture2D txVar = tx1.texture;
            LoaderProgressThumbnail.sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));
            Debug.LogError("Test");
        }
    }*/
    public void UpdateLoaderProgress()
    {
        LoaderProgressSlider.value += 0.05f;
    }

    public void UpdateLoaderProgress(float percent = 0.05f)
    {
        if (percent > 1) percent = 1f;
        LoaderProgressSlider.value = percent;
    }

    public void LoaderClose()
    {
        //CloseBackdrop();
        txtTitle_pnlLoader.text = string.Empty;
        LoaderProgressSlider.value = 0f;
        pnlLoader.SetActive(false);
    }

    #endregion

    #region Auth Loader

    public void AuthLoader(string title = "", float time = 15.0f)
    {
        ShowBackdrop();
        if (!pnlLoaderAuth.activeSelf) pnlLoaderAuth.SetActive(true);

        btnAffirm_pnlLoaderAuth.gameObject.SetActive(false);

        if (!string.IsNullOrEmpty(title))
            txtTitle_pnlLoaderAuth.text = title.Trim().ToTitleCase();
    }

    public void AuthLoaderClose()
    {
        CloseBackdrop();
        txtTitle_pnlLoaderAuth.text = string.Empty;
        btnAffirm_pnlLoaderAuth.gameObject.SetActive(false);
        pnlLoaderAuth.SetActive(false);
    }
    #endregion


    #region Toast 

    public async void Toast(string msg, float timeout = 3f)
    {
        if (pnlToast.activeSelf && ToastToken != null)
            ToastToken.Cancel();
        ToastToken = new CancellationTokenSource();
        //ShowBackdrop();
        pnlToast.SetActive(true);
        txtMsg_pnlToast.text = msg.Trim();
        try
        {
            await Task.Delay((int)timeout * 1000, ToastToken.Token);
            ToastClose();
        }
        catch (OperationCanceledException) { }

    }

    public void ToastClose()
    {
        //CloseBackdrop();
        StartCoroutine(_exec(0.2f, () => { ToastClosePost(); }));
        pnlToast.GetComponent<Animator>().SetTrigger("stopToast");
        txtMsg_pnlToast.text = "";
    }

    public void ToastClosePost()
    {
        ToastToken.Cancel();
        txtMsg_pnlToast.text = string.Empty;
        pnlToast.SetActive(false);
    }

    #endregion

    //#region tutorials
    //public void StartTutorial()
    //{
    //    tutorialNumber = 0;
    //    pnlTutorial.SetActive(true);
    //    StartTutorial(tutorialNumber);
    //    //btnHelp.SetActive(false);
    //}

    //public void PrevTutorial()
    //{
    //    tutorialNumber = Math.Abs(tutorialNumber - 1) % totalTutorials;
    //    StartTutorial(tutorialNumber);
    //}

    //public void NextTutorial()
    //{
    //    tutorialNumber = Math.Abs(tutorialNumber + 1) % totalTutorials;
    //    StartTutorial(tutorialNumber);
    //}

    //void StartTutorial(int tutorialno)
    //{
    //    ToggleTutorialButtons(tutorialno);
    //    String tutorialAnimName = "", tutorialTitle = "Default", tutorialDesc = "Default";
    //    //pnlTutorial.SetActive(true);
    //    //pnlTutorialContainer.GetComponent<Image>().sprite = tutorialSprites[tutorialNumber];
    //    switch (tutorialno)
    //    {
    //        //Setup tutorial animationName, title, description here
    //        case 0:
    //            tutorialAnimName = tutorialAnim[0];
    //            tutorialTitle = "Add Product";
    //            tutorialDesc = "In the product panel, hold and drag the thumbnail into the scene";
    //            break;
    //        case 1:
    //            tutorialAnimName = tutorialAnim[1];
    //            tutorialTitle = "Select Product";
    //            tutorialDesc = "Click on a product to select it, then select an option from left panel to operate";
    //            break;
    //        case 2:
    //            tutorialAnimName = tutorialAnim[2];
    //            tutorialTitle = "Move Around";
    //            tutorialDesc = "With one finger, drag sideways to look around and drag straight to move";
    //            break;
    //        default:
    //            Debug.LogError("animation not found");
    //            break;
    //    }
    //    pnlTutorialContainer.GetComponent<Animator>().Play(tutorialAnimName);
    //    pnlTutorial.GetComponentsInChildren<TextMeshProUGUI>()[0].text = tutorialTitle;
    //    pnlTutorial.GetComponentsInChildren<TextMeshProUGUI>()[1].text = tutorialDesc;
    //}

    //void ToggleTutorialButtons(int tutorialno)
    //{
    //    btnClose.GetComponent<Image>().color = new Color32(117, 169, 193, 255);
    //    tutorialButtonLeft.SetActive(true);
    //    tutorialButtonRight.SetActive(true);
    //    if (tutorialno == 0) tutorialButtonLeft.SetActive(false);
    //    if (tutorialno == totalTutorials - 1)
    //    {
    //        btnClose.GetComponent<Image>().color = new Color32(240, 90, 70, 255);
    //        tutorialButtonRight.SetActive(false);
    //    }
    //}
    //public void CloseTutorials()
    //{
    //    pnlTutorial.SetActive(false);
    //    //btnHelp.SetActive(true);
    //}
    //#endregion

    #region Util
    IEnumerator _exec(float t, Action Callback)
    {
        yield return new WaitForSeconds(t);
        Callback();
    }

    #endregion

    // Open and close auth panel
    public void AuthPanelOpen(Action<bool> returnFunc = null, bool isFirstScreen = false, bool isChallenge = false)
    {
        // debugPanel.SetActive(false);

        authPanel.SetActive(true);
        authMan.Init();
        // if(isFirstScreen) HomefulyExtensions.SetScreenOrientationToLandscape(false);

        // if (isFirstScreen)
        // {
        //     closeBox.SetActive(false);
        //     notNowText.SetActive(true);
        //     authScroll.enabled = false;
        // }
        // else
        // {
        //     closeBox.SetActive(true);
        //     notNowText.SetActive(false);
        //     authScroll.enabled = true;
        // }
        closeBox.SetActive(true);
        authScroll.enabled = true;
        AuthDescription.GetComponent<TextMeshProUGUI>().text = isChallenge ? "To collect rewards and save your design" : "To save your design";

        AuthManager.IsLoggedIn = false;
        SignInCallback = returnFunc;

    }

    public void OnAuthPanelCloseRequest()
    {
        AuthPanelClose();
        HomefulyEvents.OnAuthUpdated?.Invoke(AuthUpdateType.UserDenial);
    }

    public void AuthPanelClose()
    {
        EnableAuthButtons();
        authPanel.SetActive(false);
        Debug.Log("isLoggedIn: " + AuthManager.IsLoggedIn);

        if (SignInCallback != null)
            SignInCallback(AuthManager.IsLoggedIn);
    }

    public void EnableAuthButtons()
    {
        foreach (Button button in authButtons)
        {
            button.interactable = true;
        }
    }

    public void DisableAuthButtons()
    {
        foreach (Button button in authButtons)
        {
            button.interactable = false;
        }
    }
}
