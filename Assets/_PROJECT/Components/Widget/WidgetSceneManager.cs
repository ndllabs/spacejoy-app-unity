﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WidgetSceneManager : MonoBehaviour
{
    WidgetManager wid;
    float progress;
    // Start is called before the first frame update

    //int fingerID = -1;
    void Start()
    {
        wid = WidgetManager.Instance;
        wid.Init();
        progress = 0;


//#if !UNITY_EDITOR
//             fingerID = 0; 
//#endif

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Z))
        {
            wid.Alert("Title", "msg", () =>
            {
                print("Donw with confirmation!");
            });
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            wid.Prompt("Title", ans =>
            {
                print("ANs: " + ans);
            });
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            wid.Confirm("Title", "DO u agree?", ans =>
            {
                print("ANs: " + ans);
            });
        }
        if (Input.GetKeyUp(KeyCode.V))
        {
            wid.LoaderMini("Title", 3);
        }
        if (Input.GetKeyUp(KeyCode.B))
        {
            wid.Toast("Title", 2f);
        }
        if (Input.GetKeyUp(KeyCode.N))
        {
            wid.Loader("Title", 3);
            wid.UpdateLoaderProgress();
        }
        if (Input.GetKeyUp(KeyCode.M))
        {
            progress += 0.2f;
            wid.UpdateLoaderProgress(progress);
        }
        if (Input.GetKeyUp(KeyCode.T))
        {
            //wid.StartTutorial();
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            wid.ConfirmToast("Enter dour?", 3,() =>
            {
                print("Working macha");
            });
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            wid.ConfirmToast("Enter dour?",true, 2, () =>
            {
                print("Working macha");
            });
        }
    }
}
