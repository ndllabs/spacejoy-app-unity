# README

Coin Manager for the Game App project
Created By: Praveen Sundar

### CHANGELOG

- Added support for swap action
- Made it as Singleton

### Setup

- Get reference to singleton instance
- Use properties to fetch data

## Properties (Unity C# property)

- Instance => Returns the singletone instance if created or creates a new instance and returns it
- Coins => Returns the number of coins the user currently owns

## Configuration

- CoinManager listens to messages for each type of action and adjusts the number of coins
- On saving the design, the number of coins is copied to the userdata in AppComponent db.