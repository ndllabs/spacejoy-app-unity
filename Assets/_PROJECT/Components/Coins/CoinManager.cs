﻿using _unity.Events;
using UnityEngine;
using System.Collections.Generic;
using System;
using Homefuly;
using Homefuly.Events;

/*
 * Used in personal design scenes
 * Gets the coins info from the database when the scene starts
 * Coins are removed or added depending on the action taken
 */
public class CoinManager:MonoBehaviour
{
    // this object needs to be destroyed after the designer scene is complete
    // so, not extending it from the singleton class as it doesn't destroy the object
    // once the designer scene is closed, the events will be removed and the object will be destroyed

    // making it thread safe using object lock and checking if the object is about to be destroyed

    #region Fields
    float coins;
    public float Coins => coins;
    static float coinDivider = 10f;
    Dictionary<string, IAsset> IAsset_Indexed;
    List<IAsset> assetOwnedList;

    Dictionary<string, string> wallfinishs_Indexed;
    List<string> wallfinishesOwnedList;
    #endregion
    protected CoinManager() { }

    //private static bool m_ShuttingDown = false;
    private static CoinManager _instance;
    private static object _obj = new object();

    private AppComponent app;

    public static CoinManager Instance
    {
        get
        {
            print("Getting coin manager instance");
            //if (m_ShuttingDown)
            //{
            //    Debug.Log("App shutting down, not returning or creating coin manager singleton");
            //    return null;
            //}
            lock (_obj)
            {
                if (_instance == null)
                {
                    _instance = (CoinManager)FindObjectOfType(typeof(CoinManager));
                    print("Coin manager instance found in the scene");
                    if (_instance == null)
                    {
                        var CoinManagerObj = new GameObject("CoinManager");
                        _instance = CoinManagerObj.AddComponent<CoinManager>();
                        print("Coin Manager not found, creating a new instance");
                    }
                }
                return _instance;
                //if (_instance != null)
                //    return _instance;
                //_instance = new CoinManager();
                //return _instance;
            }
        }
    }

    private void OnApplicationQuit()
    {
        //m_ShuttingDown = true;
        _instance = null;
    }

    private void OnDestroy()
    {
        //m_ShuttingDown = true;
        _instance = null;
    }

    void OnEnable()
    {

        HomefulyEvents.OnAuthUpdated += OnAuthUpdated;
    }

    void OnDisable()
    {
        HomefulyEvents.OnAuthUpdated -= OnAuthUpdated;
        //Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.AssetPurchased, RemoveCoins);
        //Messenger<IWallfinishEntry>.RemoveListener(StaticFunctionCalls.AssetPurchased, RemoveCoinsWallFinish);
    }

    public void Init()
    {
        app = AppComponent.Instance;
        coins = app.db.userSavedData.currentCoins;

        // for testing, to be removed
        if (coins == 0.0f)
            coins = 500f;

        OnStart();
        //Debug.Log(AppComponent.Instance.db.userSavedData.currentCoins + " " + coins;
        //Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.AssetPurchased, RemoveCoins);
        //Messenger<IWallfinishEntry>.AddListener(StaticFunctionCalls.AssetPurchased, RemoveCoinsWallFinish);
    }

    private void OnStart()
    {
        //coins = 0f;
        IAsset_Indexed = new Dictionary<string, IAsset>(app.db.userSavedData.assetsOwned_Indexed);
        assetOwnedList = new List<IAsset>(app.db.userSavedData.assetsOwned);

        // wall finish
        wallfinishs_Indexed = new Dictionary<string, string>(app.db.userSavedData.wallfinishsOwned_Indexed);
        wallfinishesOwnedList = new List<string>(app.db.userSavedData.paintOwned);
    }

    private void OnAuthUpdated(AuthUpdateType authUpdateType, int errCode)
    {
        Debug.Log("updating The coins, Cuz of login");

        var appUser = AppComponent.Instance.db.userSavedData;
        coins = appUser.currentCoins;
        IAsset_Indexed = appUser.assetsOwned_Indexed;
        assetOwnedList = appUser.assetsOwned;

        // wall finish
        wallfinishs_Indexed = appUser.wallfinishsOwned_Indexed;
        wallfinishesOwnedList = appUser.paintOwned;

        Messenger<float>.Broadcast(StaticFunctionCalls.Action_CoinUpdated, appUser.currentCoins);

    }

    

    /// <summary>
    /// Called when an asset is "purchased" for use in "Design your own room" scenario
    /// </summary>
    /// <param name="assetEntry"></param>
    //void RemoveCoins(IAssetEntry assetEntry)

    //{

    //    coins -= assetEntry.asset.price.customer;
    //    //Messenger<float>.Broadcast(StaticFunctionCalls.Action_CoinUpdated, coins);
    //}

    //void RemoveCoinsWallFinish(IWallfinishEntry wallfinishEntry)
    //{
    //    coins -= 100f;
    //    Debug.Log("Wall finish purchase completed");
    //}

    /// <summary>
    /// Called once the user presses save design to update the coins to the db
    /// </summary>
    void SaveCoins()
    {
        return;
    }

    // for later if we want to attach a cost for every action
    //void SwapFromTo(IHotspot fromAsset, IHotspot toAsset)
    //{
    //    //coins += 10f; // replace with coins of fromAsset
    //    //coins -= 20f; // replace with coins of toAsset
    //}

    public bool IsPurchaseValid(IAssetEntry asset)
    {
        if (asset.asset.price.customer / coinDivider < coins)
            return true;
        return false;
    }

    public bool IsPurchaseValid(IWallfinishEntry wallfinish)
    {
        if (coins >= 100f)
            return true;
        return false;
    }

    /// <summary>
    /// Returns true if an asset is not previously owned and the user has enough coins to make a purchase
    /// The asset is added to the owned assets list and the coins is updated
    /// Returns false otherwise
    /// </summary>
    /// <param name="asset"></param>
    /// <returns></returns>
    public bool DoTransaction(IAsset asset)
    {
        if (IAsset_Indexed.ContainsKey(asset._id))
        {
            HomefulyEvents.OnActionFailed?.Invoke(ActionType.Transaction, (int)ErrorTracking.ErrorCodes.Already_ProductOwned);
            Debug.LogWarning("[CoinManager.cs][DoTransaction(assetEntry)]: Asset already owned.");
            return false;
        }
        else if (asset.price.customer > coins)
        {
            HomefulyEvents.OnActionFailed?.Invoke(ActionType.Transaction, (int)ErrorTracking.ErrorCodes.NotEnough_Coins);
            Debug.LogWarning("[CoinManager.cs][DoTransaction(assetEntry)]: Not enough coins to purchase the item.");
            return false;
        }
        IAsset_Indexed[asset._id] = asset;
        assetOwnedList.Add(asset);
        coins -= asset.price.customer;
        AppComponent.Instance.db.UpdateUserPurchase_Assets(coins, assetOwnedList, true);
        return true;
    }

    public bool DoTransaction(IWallfinish wallFinish)
    {
        if (wallfinishs_Indexed.ContainsKey(wallFinish._id))
        {
            HomefulyEvents.OnActionFailed?.Invoke(ActionType.Transaction, (int) ErrorTracking.ErrorCodes.Already_ProductOwned );
            Debug.LogWarning("[CoinManager.cs][DoTransaction(assetEntry)]: Asset already owned.");
            return false;
        }
        else if (wallFinish.price > coins)
        {
            HomefulyEvents.OnActionFailed?.Invoke(ActionType.Transaction, (int) ErrorTracking.ErrorCodes.NotEnough_Coins);
            Debug.LogWarning("[CoinManager.cs][DoTransaction(assetEntry)]: Not enough coins to purchase the item.");
            return false;
        }
        wallfinishs_Indexed[wallFinish._id] = wallFinish._id;
        wallfinishesOwnedList.Add(wallFinish._id);
        coins -= wallFinish.price;
        AppComponent.Instance.db.UpdateUserPurchase_Wallfinishs(coins, wallfinishesOwnedList, true);
        return true;
    }
}
