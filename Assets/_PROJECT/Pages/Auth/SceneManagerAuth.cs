﻿using System.Threading.Tasks;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

using Homefuly;

public class SceneManagerAuth : MonoBehaviour
{
    AppComponent app;

    public TMP_InputField inpEmail;
    public TMP_InputField inpPass;
    public Button btnLogin,btnShowPass;
    public GameObject goLoading;

    EventSystem eventSys;
    TouchScreenKeyboard keyboard;

    private void OnEnable()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    void Start()
    {
        WidgetManager.Instance.LoaderClose();
        eventSys = EventSystem.current;

        app = AppComponent.Instance;

        StateLogin(AuthPageLoginState.idle);
        inpEmail.keyboardType = TouchScreenKeyboardType.URL;
        inpPass.keyboardType = TouchScreenKeyboardType.Default;
        
        // inpEmail.keyboardType = (TouchScreenKeyboardType)(-1);
        // inpPass.keyboardType = (TouchScreenKeyboardType)(-1);

        // inpEmail.onSelect.RemoveAllListeners();
        // inpEmail.onSelect.AddListener((Delegate)=>
        // {
        //     ForceKeyboardOpen();
        // }
        // );

        // inpPass.onSelect.RemoveAllListeners();
        // inpPass.onSelect.AddListener((Delegate) =>
        // {
        //     ForceKeyboardOpen();
        // }
        //);


        //Assuming this doesn't run on desktop
#if !UNITY_EDITOR
            inpEmail.onEndEdit.AddListener(delegate { SwitchWithTab(); });
            inpPass.onEndEdit.AddListener(delegate { SwitchWithTab(); });
#endif

    }

    #region tab functionality

    //void ForceKeyboardOpen()
    //{

    //    if(!TouchScreenKeyboard.visible)
    //    TouchScreenKeyboard.Open("", TouchScreenKeyboardType.URL);

    //}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) SwitchWithTab();
    }
    /*
    private void GoToNextField()
    {
        inpEmail.FindSelectableOnDown().Select();
    }
    */
    private void SwitchWithTab()
    {
        try
        {
            Selectable selectable = eventSys.currentSelectedGameObject.GetComponent<Selectable>();
            if (selectable.FindSelectableOnDown() == null)
                inpEmail.Select();
            else
            {
                if (selectable.FindSelectableOnDown() == btnLogin)
                {
                    //DoLogin();//Disabling auto login
                }
                else
                    selectable.FindSelectableOnDown().Select();
            }
        }
        catch
        {
            inpEmail.Select();
        }

        //Debug.Log("Current object selected: " + eventSys.currentSelectedGameObject.name);
    }
    /*
    void LoginIfEnteredDetails()
    {
        if(!string.IsNullOrEmpty(inpEmail.text) & !string.IsNullOrEmpty(inpPass.text))
        {
            DoLogin();
        }
    }*/

    #endregion

    public void RedirectForgotPass()
    {
        InAppBrowser.OpenURL("https://www.homefuly.com/#/forgot-password-email-verification");
    }

    public void RedirectSignUP()
    {
        InAppBrowser.OpenURL("https://homefuly.com/#/login");
    }

    public void LoginViaFacebook()
    {
        //Do something
    }

    public void LoginViaGoogle()
    {
        //Do something
    }

    bool t;
    public void ShowPassword()
    {
        if (!t)
        {
            inpPass.inputType = TMP_InputField.InputType.Standard;
            btnShowPass.gameObject.GetComponent<Image>().color = new Color32(116, 167, 192, 255);
        }
        else
        {
            inpPass.inputType = TMP_InputField.InputType.Password;
            btnShowPass.gameObject.GetComponent<Image>().color = new Color32(116, 167, 192, 128);
        }
        t = !t;
        inpPass.ForceLabelUpdate();
       
    }

    public async void DoLogin()
    {

        StateLogin(AuthPageLoginState.inprogress);

        var login = await app.account.Login(inpEmail.text, inpPass.text);

        if (login.err)
        {
            //btnLogin.gameObject.SetActive(true);
            StateLogin(AuthPageLoginState.invalidCred);
        }
        else
        {
            app.account.CURRENT_ORG = AppConfig.PilotDemoConfig.OrgId;
            app.account.CURRENT_RETAIL = AppConfig.PilotDemoConfig.ProjectId;

            StateLogin(AuthPageLoginState.idle);

            PlayerPrefs.SetString("minimizeUnity", " ");
            app.Navigate(AppPage.AppComponent);

            // app.Navigate(AppPage.Home);
        }
    }


    /*public async Task DoAutoLogin()
    {
        var login = await app.account.Login(AppConfig.PilotDemoConfig.TEMP_EMAIL, AppConfig.PilotDemoConfig.TEMP_PWD);

        if (login.err)
        {
            StateLogin(AuthPageLoginState.invalidCred);
        }
        else
        {
            app.account.CURRENT_ORG = AppConfig.PilotDemoConfig.OrgId;
            app.account.CURRENT_RETAIL = AppConfig.PilotDemoConfig.ProjectId;

            app.Navigate(AppPage.Preload);
        }
    }*/

    void StateLogin(AuthPageLoginState state)
    {

        WidgetManager.Instance.LoaderMiniClose();
        switch (state)
        {
            case AuthPageLoginState.idle:

                btnLogin.gameObject.SetActive(true);
                goLoading.SetActive(false);
                break;

            case AuthPageLoginState.inprogress:
                btnLogin.gameObject.SetActive(false);
                goLoading.SetActive(true);
                break;

            case AuthPageLoginState.invalidCred:
                WidgetManager.Instance.Alert("", "Connection error or Username/Password is incorrect. Please try again.");
                btnLogin.gameObject.SetActive(true);
                goLoading.SetActive(false);
                break;

        }

    }

    enum AuthPageLoginState
    {
        idle,
        inprogress,
        invalidCred
    }
}
