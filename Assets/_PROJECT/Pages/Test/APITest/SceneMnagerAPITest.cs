﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity;
using UnityEngine.UI;

using Homefuly;

public class SceneMnagerAPITest : MonoBehaviour
{
    AppComponent app;
    // Start is called before the first frame update
    public Transform container;
    public GameObject prefElem;

    Texture t;

    async void Start()
    {
        app = AppComponent.Instance;

        var posterList = await app.server.artifact.GetAssetWallpaperList();
        var newPosterList = await app.server.artifact.GetAssetWallpaperListWithTex_Deprycated(posterList);
        if (newPosterList == null || newPosterList.Count < 1)
        {
            _.l("No Poster Found! ");
        }
        else
        {
            _.l(newPosterList[0].name);
            _.l("new tex: " + newPosterList[0].texture);
            await app.server.web.GetTextureAlt(newPosterList[0].textureUrl, (Texture tex) =>
             {
                 t = tex;
             });

            // LoadWallPosterUI(newPosterList);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void LoadWallPosterUI(List<IWallfinish> list)
    {
        container.DestroyChildren();

        foreach (IWallfinish wall in list)
        {
            GameObject go = Instantiate(prefElem);
            go.transform.SetParent(container);
            go.transform.GetComponent<RawImage>().texture = t;
        }
    }
}
