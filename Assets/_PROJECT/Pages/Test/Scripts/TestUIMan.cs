﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using _unity.Events;

public class TestUIMan : MonoBehaviour
{
    [Header("Walk")]
    public Toggle tglMove;
    public GameObject goControls;

    [Header("Assets")]
    public Toggle tglAddPoducts;
    public GameObject pnlAddProducts;

    public Toggle tglAddPoster;
    public GameObject pnlAddPosters;


    [Header("MiniMap")]
    public TextMeshProUGUI txtSegmentName;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.Escape))
        //UnityEngine.Application.Quit();

    }

    private void OnEnable()
    {
        Messenger<string>.AddListener("UI_ONUPDATE_SEGMENTNAME", OnSetSegmentName);
    }

    private void OnDisable()
    {
        Messenger<string>.RemoveListener("UI_ONUPDATE_SEGMENTNAME", OnSetSegmentName);
    }

    public void OnSetSegmentName(string txt)
    {
        txtSegmentName.text = txt;
    }
    public void OnChangeTglMove()
    {
        goControls.GetComponent<Canvas>().enabled = tglMove.isOn;
    }


    public void OnChangeTglProducts()
    {
        pnlAddProducts.SetActive(tglAddPoducts.isOn);

        if (tglAddPoducts.isOn)
            tglAddPoster.isOn = false;
    }

    public void OnChangeTglPosters()
    {
        pnlAddPosters.SetActive(tglAddPoster.isOn);

        if (tglAddPoster.isOn)
            tglAddPoducts.isOn = false;
    }


}
