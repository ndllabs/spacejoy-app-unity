﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
using System.Linq;

public class TestMouseControl : MonoBehaviour
{
    public Camera cam;  //Drag the camera object here

    Rigidbody rigidBody;

    public Vector2 moveSpeed;


    public Vector2 speedModiferKeyboard;
    public Vector2 speedModiferTouch;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Movement(false);
        }
        else
        {
            Movement(true);
        }

    }

    void Movement(bool useKeyboard = false)
    {
        Vector2 moveValue = Vector2.one;

        if (useKeyboard)
        {
            moveValue = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            moveValue.x *= speedModiferKeyboard.x;
            moveValue.y *= speedModiferKeyboard.y;
        }
        else
        {
            moveValue = Input.touchCount > 0
                ? new Vector2(Input.touches[0].deltaPosition.x, Input.touches[0].deltaPosition.y * speedModiferTouch.y)
                : new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            moveValue.x *= speedModiferTouch.x;
            moveValue.y *= speedModiferTouch.y;


            if (SystemInfo.deviceType == DeviceType.Handheld)
            {
                moveValue.x *= .1f;
                moveValue.y *= .1f;
            }
        }


        //transform.Translate(transform.forward * moveValue.y * Time.deltaTime * moveSpeed.y, Space.World);
        rigidBody.velocity = transform.forward * moveValue.y * Time.deltaTime * moveSpeed.y;

        transform.Rotate(transform.up * moveValue.x * Time.deltaTime * moveSpeed.x, Space.World);
    }


}
