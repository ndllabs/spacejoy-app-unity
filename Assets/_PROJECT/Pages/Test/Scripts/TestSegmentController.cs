﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using _unity.Events;

public class TestSegmentController : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        OnTap();
    }

    void OnTap()
    {
        print("Transport to " + gameObject.name);
        Messenger<GameObject>.Broadcast("ACTOR_SPAWN", gameObject);
    }
}
