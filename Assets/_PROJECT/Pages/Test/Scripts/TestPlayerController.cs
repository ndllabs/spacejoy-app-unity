﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class TestPlayerController : MonoBehaviour
{
    GameObject currentSegment;
    public Transform floor;
    Transform transRoomCenter;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 roomCenter = floor.GetComponent<Renderer>().bounds.center;
        roomCenter.y = 1;
        transRoomCenter = new GameObject("roomCenter").transform;
        transRoomCenter.transform.position = roomCenter;
    }

    // Update is called once per frame
    void Update()
    {

    }



    private void OnEnable()
    {
        Messenger<GameObject>.AddListener("ACTOR_SPAWN", SpawnActor);
    }

    private void OnDisable()
    {
        Messenger<GameObject>.RemoveListener("ACTOR_SPAWN", SpawnActor);
    }

    void SpawnActor(GameObject go)
    {
        var pos = go.transform.GetComponent<Renderer>().bounds.center;
        print("Spawn at: " + pos.ToString());
        pos.y = 1.39f;
        transform.position = pos;
        transform.LookAt(transRoomCenter);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Segment")
        {
            currentSegment = other.gameObject;
            Messenger<string>.Broadcast("UI_ONUPDATE_SEGMENTNAME", currentSegment.name);

        }
    }
}
