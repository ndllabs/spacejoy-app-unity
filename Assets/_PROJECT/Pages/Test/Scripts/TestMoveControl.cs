﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class TestMoveControl : MonoBehaviour
{
    public string def;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            print("Hmm");
        }

    }

    private void OnMouseDown()
    {
        print("MD");
        Vector2 vec = Vector2.zero;
        switch (def)
        {
            case "up":
                vec = new Vector2(0, 1);
                break;
            case "down":
                vec = new Vector2(0, -1);
                break;
            case "left":
                vec = new Vector2(-1, 0);
                break;
            case "right":
                vec = new Vector2(1, 0);
                break;
        }

        Messenger<Vector2>.Broadcast("INPUT_MOVE", vec);
    }

    private void OnMouseUp()
    {
        Messenger.Broadcast("INPUT_MOVE_RELEASE");
    }
}
