﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity;
using Selector;
using _unity.Events;

public class SceneManagerAssetTransDemo : MonoBehaviour
{
    public GameObject objToLoad;
    public GameObject selectedAsset;

    public GameObject assetHighlighter;

    WidgetManager wid;
    ArtifactManager artifactManager;
    // Start is called before the first frame update
    void Start()
    {
        wid = WidgetManager.Instance;
        wid.Init();

        artifactManager = new ArtifactManager(new WebApi("http://google.com", 30, 100));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        Messenger<GameObject>.AddListener(AppEvents.ASSET_SELECTED, OnAssetSelected);
        Messenger<GameObject>.AddListener(AppEvents.ASSET_DESELECTED, OnAssetDeselected);
    }

    private void OnDisable()
    {
        Messenger<GameObject>.RemoveListener(AppEvents.ASSET_SELECTED, OnAssetSelected);
        Messenger<GameObject>.RemoveListener(AppEvents.ASSET_DESELECTED, OnAssetDeselected);
    }



    void OnAssetSelected(GameObject go)
    {
        if (selectedAsset != null)
        {
            doIdle();
            Messenger<GameObject>.Broadcast(AppEvents.ASSET_DESELECTED, selectedAsset);
        }

        selectedAsset = go;
        selectedAsset.GetComponentInChildren<SelectorMain>().SelectAsset();
        wid.Toast(go.name + " Selected", .5f);
    }

    void OnAssetDeselected(GameObject go)
    {
        selectedAsset.GetComponentInChildren<SelectorMain>().CloseAssetSelection();
        wid.Toast(go.name + " Deselected", .5f);
    }


    #region UI

    public void doLoad()
    {
        GameObject go = Instantiate(objToLoad);

        //artifactManager.PostProcessAsset(go.transform, assetHighlighter, Vector3.zero, Vector3.zero);

        GameObject goSelector = Instantiate(assetHighlighter);
        goSelector.transform.SetParent(go.transform);
        goSelector.GetComponent<SelectorMain>().Init(SelectorMain.SelectorMode.Normal, string.Empty);


        go.transform.GetOrAddComponent<AssetTransformManager>();

    }


    public void doDelete()
    {
        AssetTransformManager atm = selectedAsset.GetComponent<AssetTransformManager>();
        atm.assetTransformMode = AssetTransformMode.idle;
        Destroy(selectedAsset);
    }

    public void doMove()
    {
        AssetTransformManager atm = selectedAsset.GetComponent<AssetTransformManager>();

        atm.assetTransformMode = AssetTransformMode.translate;
    }

    public void doMoveVertically()
    {
        AssetTransformManager atm = selectedAsset.GetComponent<AssetTransformManager>();

        atm.assetTransformMode = AssetTransformMode.translateVertical;

    }

    public void doRotate()
    {
        AssetTransformManager atm = selectedAsset.GetComponent<AssetTransformManager>();
        atm.assetTransformMode = AssetTransformMode.rotate;
    }

    public void doIdle()
    {
        AssetTransformManager atm = selectedAsset.GetComponent<AssetTransformManager>();
        atm.assetTransformMode = AssetTransformMode.idle;
    }

    #endregion


    #region Utils


    #endregion
}
