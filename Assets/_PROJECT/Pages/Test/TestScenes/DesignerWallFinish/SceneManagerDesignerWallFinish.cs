﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerDesignerWallFinish : MonoBehaviour
{

    public Transform srcGo;
    public Canvas canvas;
    public RectTransform pnl;

    Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.A))
        {
            DoTransform();
        }
    }

    void DoTransform()
    {
        pnl.localScale = srcGo.localScale;
    }
}
