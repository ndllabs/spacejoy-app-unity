﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

using Homefuly;
using UnityEngine.UI;

public class UIController_PanelSwap : MonoBehaviour
{

    [Header("prefabs")]
    //public GameObject elemStoreAsset;
    public GameObject elemSwappable;

    [Header("References")]
    public Transform PanelContainer;
    public GameObject elemLoader;
    public TextMeshProUGUI txtNoResult;
    [HideInInspector]
    //ICategory subcat;
    bool noResult;
    //List<elemSwappable> swappables = new List<elemSwappable>();
    

    public async void Setup(IAsset refAsset)//List<ICategory> subcategories)
    {
        //subcat = AppComponent.Instance.db.categories[subcategory];
        noResult = false;
        PanelContainer.DestroyChildren();
        elemLoader.SetActive(true);
        List<IAsset> data = await AppComponent.Instance.server.artifact.GetAssetsBySubcategory(refAsset.subcategory, 0, 40);

        elemLoader.SetActive(false);
        //if data is null
        if (data == null)
        {
            noResult = true;
        }
        else if (data.Count == 0)
        {
            noResult = true;
        }
        if (noResult)
        {
            txtNoResult.text = "No products found for swapping";
            Debug.LogError("No swappable assets found wtf");
            //elemLoader.SetActive(false);
            //g = Instantiate(elemStoreAsset) as GameObject;
            //t.SetParent(container.transform, false);
            //t.Find("txtViewMore").gameObject.SetActive(true);
            //t.Find("txtViewMore").GetComponent<TextMeshProUGUI>().text = "No Assets found";
            //t.Find("imgAsset").gameObject.SetActive(false);
            //t.localScale = Vector3.one;
            return;
        }
        data = data.OrderByDescending(x => x.vertical == refAsset.vertical).ToList();
        //Debug.Log(data.PrintList());
    //if data count > 0
        //int Datacount = result.Count;

        GameObject g; noResult = true;
       //PanelContainer.transform.parent.transform.parent.gameObject.GetComponent<ScrollRect>().normalizedPosition = Vector2.one;
        int CountForSwapItems = 23; //How many assets to be shown

        foreach (IAsset asset in data)
        {
            bool isShoppable = asset.category != AppConfig.PilotDemoConfig.NonShoppableCatId ? true : false;
            if (asset.currency == "INR" | !isShoppable | asset._id == refAsset._id | asset.price.customer==0) continue;   //dont show same asset in swap //Dont show INR products or nonshoppable products    //Dont show products with price = 0
            noResult = false;
            g = Instantiate(elemSwappable) as GameObject;
            g.transform.SetParent(PanelContainer);
            g.transform.localScale = Vector3.one;
            g.GetComponent<elemSwappable>().Setup(asset);
            if (--CountForSwapItems < 0) break;
        }
        if(noResult)
        {
            txtNoResult.text = "No products found for swapping";
        }
        else
        {
            txtNoResult.text = "";
        }
        //VIEW MORE BUTTON
        //{
        //    g = Instantiate(elemSwappable) as GameObject;
        //    Transform t = g.transform;
        //    t.SetParent(PanelContainer);
        //    t.localScale = Vector3.one;
        //    t.GetComponent<elemSwappable>().ViewMoreButton();
        //}
    }

}
/*
public static class Utils2
{
    public static string PrintList<T>(this List<T> list)
    {
        string res="";
        for(int i = 0; i<list.Count; i++)
        {
            res += list[i].ToString() + "\n";
        }
        return res;
    }
}*/


