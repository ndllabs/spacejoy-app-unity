﻿using _unity.Events;
using Homefuly.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using Homefuly;

public class UIController_PanelAssetStore : MonoBehaviour
{
    [Header("prefabs")]
    //public GameObject elemStoreAsset;
    public GameObject elemCatPanel;
    public GameObject elemCatMini;
    public GameObject inventoryPanel;

    [Header("References")]
    public Transform allCatPanelContainer;
    public Transform CategoryPanelContainer;
    public GameObject allCatPanel;
    public GameObject CategoryPanel;
    public Button btnNavbarBack;

    [HideInInspector]
    List<ICategory> subcategories = new List<ICategory>();
    List<ICategory> subcategoriesMini = new List<ICategory>();
    bool init;
    StorePanel CurrentStorePanel;

    public void Init(AppDatabase data)//IDictionary<string,ICategory> data)
    {
        int topCatCount = 0;
        int doubleScrollModifier = 0;//3;   //SOS to check how many categories have double scroll
        subcategories.Clear();
        subcategoriesMini.Clear();

        foreach (var res in data.GetCategoriesOfType(CategoryType.Category))
        {
            if (topCatCount < doubleScrollModifier)
            {
                int topSubcatCount = 0;
                foreach (string str in res.Value.children)
                {
                    if (topSubcatCount < 3)
                    {
                        subcategories.Add(data.categories[str]);
                        topSubcatCount++;
                    }
                    else
                    {
                        subcategoriesMini.Add(data.categories[str]);
                    }
                }
            }
            else
            {
                foreach (string str in res.Value.children)
                {
                    //SOS hardcoded to remove subcats of Customer products and props
                    if (str.Equals("5ba4a959f120bf1cd051519f")/*cust products*/|| str.Equals("5ba234a2e852e80dad11b656"/*props*/)) continue;

                    subcategoriesMini.Add(data.categories[str]);
                }
            }
            topCatCount++;
        }
    }

    void Init()
    {
        if (!init)
        {
            init = true;
            Setup();
        }
    }

    private void OnEnable()
    {
        Init();
        StoreStateManager(StorePanel.allCat);
        Messenger<ICategory>.AddListener(StaticFunctionCalls.AssetStoreCategory, OpenCategoryPage);
    }

    private void OnDisable()
    {
        Messenger<ICategory>.RemoveListener(StaticFunctionCalls.AssetStoreCategory, OpenCategoryPage);
    }

    void Setup()//List<ICategory> subcategories)
    {
        #region Verticals list
        ////Finding verticals saving in a file
        ////Regex regx = new Regex("^.{7}$", RegexOptions.IgnoreCase);
        ////var faultyPaints = from sas in listPaints
        //                   //where !regx.IsMatch(sas.code)
        //                   //select sas;
        //string s = "\n",s2,s3,s4;
        //var verticals = AppComponent.Instance.db.GetCategoriesOfType(CategoryType.Vertical);

        //foreach (KeyValuePair<string, ICategory> vertical in verticals)
        //{
        //    s = vertical.Value.name;
        //    using (System.IO.StreamWriter file =
        //      new System.IO.StreamWriter(@"C:\Users\kurokala\Desktop\Verticals.txt", true))
        //    {
        //        file.WriteLine(s);
        //    }
        //    s2 = AppComponent.Instance.db.categories[vertical.Value.parentCat]._id;
        //    using (System.IO.StreamWriter file =
        //      new System.IO.StreamWriter(@"C:\Users\kurokala\Desktop\SubCatID.txt", true))
        //    {
        //        file.WriteLine(s2);
        //    }
        //    s3 = AppComponent.Instance.db.categories[vertical.Value.parentCat].name;
        //    using (System.IO.StreamWriter file =
        //      new System.IO.StreamWriter(@"C:\Users\kurokala\Desktop\SubCats.txt", true))
        //    {
        //        file.WriteLine(s3);
        //    }
        //    s4 = vertical.Value._id;
        //    using (System.IO.StreamWriter file =
        //      new System.IO.StreamWriter(@"C:\Users\kurokala\Desktop\Vertical ID.txt", true))
        //    {
        //        file.WriteLine(s4);
        //    }
        //}
        #endregion

        allCatPanelContainer.DestroyChildren();
        CategoryPanelContainer.DestroyChildren();
        GameObject g;

        inventoryPanel.transform.SetParent(allCatPanelContainer);
        inventoryPanel.SetActive(false);
        //inventoryPanel.transform.localScale = Vector3.one;
        //g.GetComponent<Button>().onClick.AddListener(() => OpenInventory());
        //int count = 0;//temporary SOS: loading only first 6 cats
        foreach (ICategory cat in subcategories)
        {
            g = Instantiate(elemCatPanel) as GameObject;
            g.transform.SetParent(allCatPanelContainer);
            g.transform.localScale = Vector3.one;
            g.GetComponent<UIController_AllCatContainer>().Setup(cat);
            //count++;    //SOS
            //if (count > 6) break;   //SOS
        }
        foreach (ICategory cat in subcategoriesMini)
        {
            g = Instantiate(elemCatMini) as GameObject;
            Transform t = g.transform;
            t.SetParent(allCatPanelContainer);
            t.localScale = Vector3.one;
            g.GetComponent<PrefabCatMiniContainer>().Setup(cat);
        }
    }

    void OpenCategoryPage(ICategory category)
    {
        //In progress
        StoreStateManager(StorePanel.Category);
        CategoryPanel.GetComponent<UIController_CatContainer>().Setup(category._id, category.name);
    }

    public void OpenInventory()
    {
        StoreStateManager(StorePanel.Inventory);
        CategoryPanel.GetComponent<UIController_CatContainer>().SetupInventory();
    }

    void StoreStateManager(StorePanel storePanel, string metadata = null)
    {
        HideAllStorePanels();
        CurrentStorePanel = storePanel;
        btnNavbarBack.onClick.RemoveAllListeners();
        switch (storePanel)
        {
            case StorePanel.allCat:
                allCatPanel.SetActive(true);
                btnNavbarBack.onClick.AddListener(() => Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global));
                break;
            case StorePanel.Category:
                CategoryPanel.SetActive(true);
                Messenger<string>.Broadcast(ChallengeUICalls.UpdateNavbarText, "Add Products");
                btnNavbarBack.onClick.AddListener(() => StoreStateManager(StorePanel.allCat));
                break;

            case StorePanel.Inventory:
                CategoryPanel.SetActive(true);
                btnNavbarBack.onClick.AddListener(() => StoreStateManager(StorePanel.allCat));
                break;
        }

    }

    void HideAllStorePanels()
    {
        allCatPanel.SetActive(false);
        CategoryPanel.SetActive(false);
    }

    enum StorePanel
    {
        allCat,
        Category,
        Inventory
    }
    /*
    public void ProductSetup(List<IAssetStoreElement> result)
    {
        Debug.Log("Setup was called :D");
        ProductContainer.DestroyChildren();
        if (result == null)
        {
            GameObject g = Instantiate(elemStoreAsset) as GameObject;
            g.transform.SetParent(ProductContainer.transform, false);

            //StoreIds
            GameObject gid = new GameObject("aId");
            gid.transform.SetParent(g.transform);
            g.transform.Find("txtName").GetComponent<TextMeshProUGUI>().text = "No Assets found";

            return;
        }
        int Datacount = result.Count;
        for (int i = 0; i < Datacount; i++)
        {
            string assetId = result[i]._id;
            //SpawnCard
            GameObject g = Instantiate(elemStoreAsset) as GameObject;
            g.transform.SetParent(ProductContainer.transform, false);

            //StoreIds
            GameObject gid = new GameObject("aId"); gid.transform.SetParent(g.transform);
            GameObject gidV = new GameObject(assetId); gidV.transform.SetParent(gid.transform);

            /*  //SET PRICE BASED ON MODEL
            string price= result.data[i].price;
            float fPrice = 0;
            try
            {
                fPrice = float.Parse(price);
            }
            catch (Exception e)
            {
                fPrice = 0;
                Debug.Log("Price display error!");
            }
            if (fPrice != 0f)
                g.transform.Find("TmTxtPrice").GetComponent<TextMeshProUGUI>().text = currency == "USD" ? "$" + fPrice.ToMoney() : "INR " + fPrice.ToMoney();
            else
                g.transform.Find("TmTxtPrice").gameObject.SetActive(false);

            //* /
            g.transform.Find("txtName").GetComponent<TextMeshProUGUI>().text = result[i].name.ToTitleCase();
            g.transform.Find("txtExtra").GetComponent<TextMeshProUGUI>().text = result[i].description;
            //GetThumb
            //Server.Instance.GetAssetImageThumbnail(assetId, (bool err1, Texture t) =>
            //{
            //    if (g != null)
            //    {
            //        if (g.transform.Find("image/elemLoader") != null)
            //        {
            //            g.transform.Find("image/elemLoader").gameObject.SetActive(false);

            //            if (!err1)
            //            {
            //                g.transform.Find("image").GetComponent<RawImage>().texture = t;
            //            }
            //        }
            //    }
            //});

            g.transform.GetComponent<Button>().onClick.RemoveAllListeners();
            g.transform.Find("Button_Delete").GetComponent<Button>().onClick.RemoveAllListeners();
            g.transform.Find("Button_Delete").gameObject.SetActive(false);
        }
    }

    /*
    /// <summary>
    /// Spawns a single Regular asset Card Based On AssetModel, Run it inside a loopp
    /// </summary>
    /// <param name="a"></param>
    /// <param name="isStore"></param>
    void CardSpawner(MCAssetModel a, bool isStore = false)
    {
        //SetId
        string assetId = a.aId;
        //curCards.Add(a.aId);

        //SpawnCard
        GameObject g = Instantiate(elemStoreAsset) as GameObject;
        g.transform.SetParent(container.transform, false);

        //StoreIds
        GameObject gid = new GameObject("aId"); gid.transform.SetParent(g.transform);
        GameObject gidV = new GameObject(a.aId); gidV.transform.SetParent(gid.transform);

        string currency = a.currency;
        float fPrice = 0f;
        try
        {
            fPrice = float.Parse(a.price);
        }
        catch (Exception e)
        {
            fPrice = 0;
        }

        g.transform.Find("TmTxtName").GetComponent<TextMeshProUGUI>().text = a.name.ToTitleCase();
        if (fPrice != 0f)
            g.transform.Find("TmTxtPrice").GetComponent<TextMeshProUGUI>().text = currency == "USD" ? "$" + fPrice.ToMoney() : "INR " + fPrice.ToMoney();
        else
            g.transform.Find("TmTxtPrice").gameObject.SetActive(false);

        g.transform.Find("TextRetailer").GetComponent<TextMeshProUGUI>().text = a.provider.ToTitleCase();

        //GetThumb
        //Server.Instance.GetAssetImageThumbnail(assetId, (bool err1, Texture t) =>
        //{
        //    if (g != null)
        //    {
        //        if (g.transform.Find("image/elemLoader") != null)
        //        {
        //            g.transform.Find("image/elemLoader").gameObject.SetActive(false);

        //            if (!err1)
        //            {
        //                g.transform.Find("image").GetComponent<RawImage>().texture = t;
        //            }
        //        }
        //    }
        //});


        g.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        
        g.transform.Find("Button_Delete").GetComponent<Button>().onClick.RemoveAllListeners();
        g.transform.Find("Button_Delete").gameObject.SetActive(false);

        g.transform.Find("Button_SwapOption").GetComponent<Button>().onClick.RemoveAllListeners();
        g.transform.Find("Button_SwapOption").gameObject.SetActive(false);
    }*/

    //kunsScripts
    //public void BackRequestBroadcast()
    //{
    //    Messenger.Broadcast(StaticFunctionCalls.GoBack);
    //}
}
