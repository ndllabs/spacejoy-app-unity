﻿using Homefuly;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController_CatContainer : MonoBehaviour
{
    [Header("containers")]
    public Transform container;
    public GameObject elemStoreAsset;
    public TextMeshProUGUI title;
    public Image elemLoader;
    public ScrollRect scrollRect;

    bool noResult;
    string Title;
    string id;

    private void OnEnable()
    {
    }

    public async void Setup(string id, string title)
    {
        GameObject g; Transform t;
        this.title.text = Title = title;
        this.id = id;
        container.DestroyChildren();
        elemLoader.gameObject.SetActive(true);
        List<IAsset> result = await AppComponent.Instance.server.artifact.GetAssetsBySubcategory(this.id, 0, 50);
        scrollRect.normalizedPosition = new Vector2(0, 1); //to bring it to the top
        //if data is null
        if (result == null)
        {
            noResult = true;
        }
        else if (result.Count == 0)
        {
            noResult = true;
        }
        else
        {
            noResult = false;
        }
        if (noResult)
        {
            g = Instantiate(elemStoreAsset) as GameObject;
            t = g.transform;
            t.SetParent(container.transform, false);
            t.Find("txtViewMore").gameObject.SetActive(true);
            t.Find("txtViewMore").GetComponent<TextMeshProUGUI>().text = "No Assets found";
            t.Find("imgAsset").gameObject.SetActive(false);
            t.Find("btnShop").gameObject.SetActive(false);
            t.localScale = Vector3.one;
            elemLoader.gameObject.SetActive(false);
            return;
        }

        var allBoughtAssets = AppComponent.Instance.db.userSavedData.assetsOwned_Indexed;

        //if data count > 0
        int Datacount = result.Count;
        List<Task> assetImageLoadTasks = new List<Task>();
        for (int i = 0; i < Datacount; i++)
        {
            IAsset asset = result[i];
            if (asset.currency.Equals("INR") | asset.price.customer == 0) continue;   //Dont show asset if price is 0

            string assetId = asset._id;
            //SpawnCard
            g = Instantiate(elemStoreAsset) as GameObject;
            t = g.transform;
            t.SetParent(container.transform, false);

            //StoreIds
            GameObject gid = new GameObject("aId"); gid.transform.SetParent(g.transform);
            GameObject gidV = new GameObject(assetId); gidV.transform.SetParent(gid.transform);
            t.GetComponent<DragHandlerAsset>().asset = asset;

            //g.transform.Find("txtName").GetComponent<TextMeshProUGUI>().text = result[i].name.ToTitleCase();
            //g.transform.Find("txtExtra").GetComponent<TextMeshProUGUI>().text = result[i].description;

            assetImageLoadTasks.Add(DownloadTex(t, asset.textureUrl));
            t.Find("elemLoader").gameObject.SetActive(true);


            if (allBoughtAssets.ContainsKey(asset._id))
                t.Find("Coins/txtCoins").gameObject.GetComponent<TextMeshProUGUI>().text = "Owned";
            else
                t.Find("Coins/txtCoins").gameObject.GetComponent<TextMeshProUGUI>().text = asset.price.customer.ToMoney();

            t.Find("Coins/txtCoins").gameObject.GetComponent<TextMeshProUGUI>().text = "$" + asset.price.customer.ToMoney();
            //t.Find("Coins").gameObject.SetActive(!AppConfig.GameConfig.creativeMode);
            t.Find("Coins").gameObject.SetActive(true);

            t.GetComponent<Button>().onClick.RemoveAllListeners();
            //g.transform.Find("Button_Delete").GetComponent<Button>().onClick.RemoveAllListeners();
            //g.transform.Find("Button_Delete").gameObject.SetActive(false);
            t.localScale = Vector3.one;
            Button bb = t.Find("btnShop").gameObject.GetComponent<Button>();
            bb.onClick.RemoveAllListeners();
            bb.onClick.AddListener(() =>
            {
                HomefulyExtensions.OpenProductStorePage(asset, Homefuly.Analytics.ShopButtonType.asset_store);
            });

        }
        elemLoader.gameObject.SetActive(false);
        CancellationTokenSource cts = new CancellationTokenSource();

        try
        {
            await Task.WhenAll(assetImageLoadTasks);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }

    }

    async Task DownloadTex(Transform t, string url)
    {
        url = url.Replace(".jpg", "_t.jpg").Replace(".png", "_t.png");  //to get lowres thumbnail img
        var texture = t.Find("imgAsset").GetComponent<Image>();

        Texture2D txVar = (Texture2D)await AppComponent.Instance.server.web.GetTexture(url);
        if (txVar != null) texture.sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));
        t.Find("elemLoader").gameObject.SetActive(false);
        Destroy(t.Find("elemLoader").gameObject);
        t.Find("imgAsset").gameObject.SetActive(true);
    }

    public async void SetupInventory()
    {
        GameObject g; Transform t;
        this.title.text = Title = "Inventory";

        container.DestroyChildren();
        elemLoader.gameObject.SetActive(true);
        // List<IAsset> result = await AppComponent.Instance.server.artifact.GetAssetsBySubcategory(this.id, 0, 50);
        List<IAsset> result = AppComponent.Instance.db.userSavedData.assetsOwned;
        scrollRect.normalizedPosition = new Vector2(0, 1); //to bring it to the top
        //if data is null
        if (result == null)
        {
            noResult = true;
        }
        else if (result.Count == 0)
        {
            noResult = true;
        }
        else
        {
            // added by praveen
            // you forgot to check the corner case and reset the noResult variable
            noResult = false;
        }
        if (noResult)
        {
            // should have had a placeholder / just a text prefab for alerts like this
            // so that we don't have to waste cpu time finding children and their components

            g = Instantiate(elemStoreAsset) as GameObject;
            g.GetComponent<Image>().enabled = false;
            t = g.transform;
            t.SetParent(container.transform, false);
            t.Find("txtViewMore").gameObject.SetActive(true);
            t.Find("txtViewMore").GetComponent<TextMeshProUGUI>().text = "Items you purchase will appear here";
            t.Find("imgAsset").gameObject.SetActive(false);
            t.Find("Coins").gameObject.SetActive(false);
            t.Find("elemLoader").gameObject.SetActive(false);
            t.localScale = Vector3.one;
            elemLoader.gameObject.SetActive(false);
            return;
        }



        //if data count > 0
        int Datacount = result.Count;
        List<Task> assetImageLoadTasks1 = new List<Task>();
        for (int i = 0; i < Datacount; i++)
        {
            IAsset asset = result[i];
            asset = await AppComponent.Instance.server.artifact.GetAssetData(asset._id);
            string assetId = asset._id;
            //SpawnCard
            g = Instantiate(elemStoreAsset) as GameObject;
            t = g.transform;
            t.SetParent(container.transform, false);

            //StoreIds
            GameObject gid = new GameObject("aId");
            gid.transform.SetParent(g.transform);
            GameObject gidV = new GameObject(assetId);
            gidV.transform.SetParent(gid.transform);
            t.GetComponent<DragHandlerAsset>().asset = asset;

            //g.transform.Find("txtName").GetComponent<TextMeshProUGUI>().text = result[i].name.ToTitleCase();
            //g.transform.Find("txtExtra").GetComponent<TextMeshProUGUI>().text = result[i].description;

            assetImageLoadTasks1.Add(DownloadTexAndAssetDetails(t, "assets/" + assetId + "/" + assetId + ".jpg", asset));
            t.Find("elemLoader").gameObject.SetActive(true);
            t.Find("Coins/txtCoins").gameObject.GetComponent<TextMeshProUGUI>().text = "Owned";

            t.GetComponent<Button>().onClick.RemoveAllListeners();
            //g.transform.Find("Button_Delete").GetComponent<Button>().onClick.RemoveAllListeners();
            //g.transform.Find("Button_Delete").gameObject.SetActive(false);
            t.localScale = Vector3.one;
        }
        elemLoader.gameObject.SetActive(false);
        CancellationTokenSource cts = new CancellationTokenSource();

        try
        {
            await Task.WhenAll(assetImageLoadTasks1);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }

    }

    async Task DownloadTexAndAssetDetails(Transform t, string url, IAsset asset)
    {
        asset = await AppComponent.Instance.server.artifact.GetAssetData(asset._id);
        await DownloadTex(t, url);
    }
}
