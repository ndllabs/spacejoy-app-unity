﻿using _unity.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TouchMan;
using TMPro;

public class DragHandlerAsset : MonoBehaviour, IPointerDownHandler, /*IPointerUpHandler,*/ ITouchManUI
{
    float time;
    bool pressed, lifted;
    PointerEventData storeClickPositon;
    //readonly float HoldThreshold = 0.3f;

    #region Drag Now
    public IAsset asset;
    //public string id;
    //string assetName;
    public GameObject gDp;
    //private RectTransform gRt;
    private Transform canvas;

    private Vector2 pointerStartPos = Vector2.zero;
    private Vector2 pointerCurrentPos = Vector2.zero;
    //private float pointerRightMoveThreshold = 120f;

    //public bool isEnabled = false;
    public void InitializeAssetDrag()
    {
        gDp = Instantiate(Resources.Load("DragDropRes/DragDropAssetThumbnail", typeof(GameObject))) as GameObject;

        gDp.transform.SetParent(GameObject.Find("PanelFull").transform);
        gDp.GetComponent<Image>().sprite = transform.Find("imgAsset").GetComponent<Image>().sprite;
        gDp.GetComponent<Image>().color = new Color32(117, 169, 193, 255);

        gDp.transform.Find("elemLoader").gameObject.SetActive(false);

        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, true);
        //id = transform.Find("aId").GetChild(0).name;
        //gDp.GetComponent<DragDropAssetThumbnail>().assetName = transform.Find("txtName").GetComponent<TextMeshProUGUI>().text;
        gDp.GetComponent<DragDropAssetThumbnail>().asset = asset;

        storeClickPositon.pointerDrag = gDp;
        //gameObject.GetComponent<DragAndDropAssetV2>().OnBeginDrag(storeClickPositon);
        //ExecuteEvents.Execute(gameObject.GetComponent<DragAndDropAssetV2>().gDp, storeClickPositon, ExecuteEvents.initializePotentialDrag);
        ExecuteEvents.Execute(gDp, storeClickPositon, ExecuteEvents.beginDragHandler);
        //Debug.Log("Ready for Drag & Drop 2/5");
        ExecuteEvents.Execute(gDp, storeClickPositon, ExecuteEvents.dragHandler);
        //Destroy(this);
    }

    #endregion

    /*void Update()
    {
        //Temp Only, Use touchman Events Instead
        if (lifted)
        {
            if (Input.touchSupported && Input.touchCount > 0)
            {
                pointerCurrentPos = Input.touches[0].position;
                if (Input.touches[0].phase == TouchPhase.Ended)
                    lifted = false;
            }
            else
            {
                pointerCurrentPos = Input.mousePosition;
                if(Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
                    lifted = false;
            }
            

            MakeAssetReadyForDragAndDrop();
        }
    }*/


    
    public void OnPointerDown(PointerEventData eventData)
    {
        storeClickPositon = eventData;
    }


    //Temp Only, Use touchman Events Instead
   /* public void OnPointerUp(PointerEventData eventData)
    {
        //Debug.Log("OnPointerUp 6/5");
        pressed = false;
    }*/


    /*void MakeAssetReadyForDragAndDrop()
    {
        if ((Mathf.Abs(pointerCurrentPos.x - pointerStartPos.x) > pointerRightMoveThreshold && pointerCurrentPos.x / Screen.width < 0.6f)||
         (Time.time - time > HoldThreshold && pressed))
        {
            pressed = false; lifted = false;
            InitializeAssetDrag();
        }
    }*/

    public void OnUIDraggedTowardsWorld()
    {
        InitializeAssetDrag();
    }

    public void OnUISwiped()
    {
        //throw new System.NotImplementedException();
    }
}
