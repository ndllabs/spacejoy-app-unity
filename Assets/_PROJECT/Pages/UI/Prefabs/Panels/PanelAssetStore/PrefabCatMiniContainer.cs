﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using _unity.Events;
using Homefuly;

public class PrefabCatMiniContainer : MonoBehaviour
{

    [Header("containers")]
    public TextMeshProUGUI title;
    public Image img;
    public GameObject elemLoader;
    public ICategory category;  //data for this allCatContainer

    bool noResult;

    public async void Setup(ICategory category)
    {

        this.category = category;
        //GameObject g; Transform t;
        title.text = category.name;
        //this.id = category.id;
        elemLoader.SetActive(true);
        img.enabled = false;
        List<IAsset> result = await AppComponent.Instance.server.artifact.GetAssetsBySubcategory(category._id, 0, 1,"USD");
        //if data is null
        if (result == null)
        {
            noResult = true;
        }
        else if (result.Count == 0)
        {
            noResult = true;
        }
        if (noResult)
        {
            elemLoader.SetActive(false);
            Destroy(gameObject);
            //g = Instantiate(elemStoreAsset) as GameObject;
            //t.SetParent(container.transform, false);
            //t.Find("txtViewMore").gameObject.SetActive(true);
            //t.Find("txtViewMore").GetComponent<TextMeshProUGUI>().text = "No Assets found";
            //t.Find("imgAsset").gameObject.SetActive(false);
            //t.localScale = Vector3.one;
            return;
        }

        //if data count > 0
        gameObject.GetComponent<Button>().onClick.AddListener(() => Messenger<ICategory>.Broadcast(StaticFunctionCalls.AssetStoreCategory, category));
        //int Datacount = result.Count;
        //List<Task> assetImageLoadTasks = new List<Task>();
        foreach (IAsset asset in result)
        {

            if (asset.textureUrl == string.Empty | asset.textureUrl == null)
                    asset.textureUrl = "/assets/" + asset._id + "/" + asset._id + ".jpg";
            //assetImageLoadTasks.Add(DownloadTex(t, asset.textureUrl));
            DownloadTex(asset.textureUrl);

        }//g = Instantiate(elemStoreAsset) as GameObject;
        //t = g.transform;
        //t.SetParent(container.transform, false);
        //Destroy(t.GetComponent<DragHandlerAsset>());
        //t.GetComponent<Button>().onClick.RemoveAllListeners();
        //t.GetComponent<Button>().onClick.AddListener(() => Messenger<ICategory>.Broadcast(StaticFunctionCalls.AssetStoreCategory, category));
        //t.Find("txtViewMore").gameObject.SetActive(true);
        //img.enabled = true;
        //t.Find("imgAsset").gameObject.SetActive(false);
        //t.localScale = Vector3.one;

        //elemLoader.SetActive(false);
        //await Task.WhenAll(assetImageLoadTasks);

    }

    async void DownloadTex(string url)
    {
        url = url.Replace(".jpg", "_t.jpg").Replace(".png", "_t.png");  //to get lowres thumbnail img

        Texture2D txVar = (Texture2D)await AppComponent.Instance.server.web.GetTexture(url);
        if (txVar != null)
        {
            img.sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));
        }
        else
        {
            txVar = img.sprite.texture;
            }
        elemLoader.SetActive(false);
        img.enabled = true;
    }
}
