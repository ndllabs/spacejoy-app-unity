﻿using _unity.Events;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using Homefuly;

public class UIController_AllCatContainer : MonoBehaviour
{
    [Header("containers")]
    public Transform container;
    public GameObject elemStoreAsset;
  
    public Button catExpander;
    public TextMeshProUGUI title;
    
    public ICategory category;  //data for this allCatContainer

    public GameObject elemLoader;
    bool noResult;

    public async void Setup(ICategory category)
    {

        this.category = category;
        GameObject g; Transform t;
        this.title.text = category.name;
        //this.id = category.id;
        container.DestroyChildren();
      
        elemLoader.SetActive(true);
        List<IAsset> result = await AppComponent.Instance.server.artifact.GetAssetsBySubcategory(category._id, 0, 10);

        //if data is null
        if (result == null)
        {
           noResult = true;
        }
        else if(result.Count == 0)
        {
            noResult = true;
        }
        if (noResult)
        {
            elemLoader.SetActive(false);
            Destroy(gameObject);
            //g = Instantiate(elemStoreAsset) as GameObject;
            //t.SetParent(container.transform, false);
            //t.Find("txtViewMore").gameObject.SetActive(true);
            //t.Find("txtViewMore").GetComponent<TextMeshProUGUI>().text = "No Assets found";
            //t.Find("imgAsset").gameObject.SetActive(false);
            //t.localScale = Vector3.one;
            return;
        }

        //if data count > 0
        catExpander.onClick.RemoveAllListeners();
        catExpander.onClick.AddListener(() => Messenger<ICategory>.Broadcast(StaticFunctionCalls.AssetStoreCategory, category));
        //int Datacount = result.Count;
        List<Task> assetImageLoadTasks = new List<Task>();
        int count = 0;
        foreach (IAsset asset in result)
        {
            if (asset.currency.Equals("INR") | asset.price.customer == 0) continue;
            else count++;
            if (count > 4) break;
            //string assetId = result[i]._id;
            //SpawnCard
            g = Instantiate(elemStoreAsset) as GameObject;
            t = g.transform;
            t.SetParent(container.transform, false);

            //StoreIds
            GameObject gid = new GameObject("aId"); gid.transform.SetParent(t);
            GameObject gidV = new GameObject(asset._id); gidV.transform.SetParent(gid.transform);
            t.GetComponent<DragHandlerAsset>().asset = asset;
            //t.Find("txtName").GetComponent<TextMeshProUGUI>().text = result[i].name.ToTitleCase();
            //t.Find("txtExtra").GetComponent<TextMeshProUGUI>().text = result[i].description;
            assetImageLoadTasks.Add(DownloadTex(t, asset.textureUrl));
            t.Find("elemLoader").gameObject.SetActive(true);
            t.GetComponent<LayoutElement>().preferredWidth = t.Find("imgAsset").GetComponent<Image>().sprite.textureRect.width;//Testing fitting
            t.localScale = Vector3.one;

            //t.GetComponent<Button>().onClick.RemoveAllListeners();
            //t.Find("Button_Delete").GetComponent<Button>().onClick.RemoveAllListeners();
            //t.Find("Button_Delete").gameObject.SetActive(false);
        }

        //VIEW MORE BUTTON
        {
            g = Instantiate(elemStoreAsset) as GameObject;
            t = g.transform;
            t.SetParent(container.transform, false);
            Destroy(t.GetComponent<DragHandlerAsset>());
            t.GetComponent<Button>().onClick.RemoveAllListeners();
            t.GetComponent<Button>().onClick.AddListener(() => Messenger<ICategory>.Broadcast(StaticFunctionCalls.AssetStoreCategory, category));
            t.Find("txtViewMore").gameObject.SetActive(true);
            t.GetComponent<Image>().enabled = false;
            t.Find("imgAsset").gameObject.SetActive(false);
            t.localScale = Vector3.one;
        }
        elemLoader.SetActive(false);
        await Task.WhenAll(assetImageLoadTasks);

    }

    async Task DownloadTex(Transform transform, string url)
    {
        url = url.Replace(".jpg", "_t.jpg").Replace(".png", "_t.png");  //to get lowres thumbnail img

        Image texture = transform.Find("imgAsset").GetComponent<Image>();
        Texture2D txVar = (Texture2D)await AppComponent.Instance.server.web.GetTexture(url);
        LayoutElement layout = transform.GetComponent<LayoutElement>();
        if (txVar != null)
        {
            layout.preferredWidth = transform.GetComponent<RectTransform>().rect.height * ((float)txVar.width / (float)txVar.height);//Testing fitting
            texture.sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));
        }
        else
        {
            txVar = texture.sprite.texture;
            layout.preferredWidth = transform.GetComponent<RectTransform>().rect.height * ((float)txVar.width / (float)txVar.height);//Testing fitting
        }
        transform.Find("elemLoader").gameObject.SetActive(false);
        Destroy(transform.Find("elemLoader").gameObject);
        transform.Find("imgAsset").gameObject.SetActive(true);
    }
}
