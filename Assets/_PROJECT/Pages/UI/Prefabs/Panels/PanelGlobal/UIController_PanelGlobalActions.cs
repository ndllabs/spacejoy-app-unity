﻿using _unity.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIController_PanelGlobalActions : MonoBehaviour
{
    public void Setup()
    {
    }

    public void CardSave_Save()
    {
        Messenger<string>.Broadcast(StaticFunctionCalls.OnSaveDesign, "Menu Button");
    }

    public void CardCatalog_OpenPnlAssetStore()
    {
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.AssetStore);
    }

    public void CardCatalog_OpenPnlPaintStore()
    {
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Paint);
    }
    
    public void CardCatalog_OpenPnlWallpaperStore()
    {
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Wallpapers);
    }
    public void CardCatalog_OpenSettingsTab()
    {
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Settings);
    }
    public void Section_OpenProductList()
    {
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.ProductList);
    }
    public void Section_ViewViewpoints()
    {
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Viewpoints);
    }

}
