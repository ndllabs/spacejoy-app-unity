﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Android;
using TouchMan;
using Homefuly;

public class UIController_PanelSettings : MonoBehaviour
{
    #region refs
    public Color highlightColor;

    //public UIController_HUD HUDPanel;
    public TMP_InputField movementSpeedX, movementSpeedY, VelocityMultiplier, MovementVelocityDelay;

    public TMP_InputField camFOV, camHeight, camAngle;

    public Button lowButton, mediumButton, highButton;

    public TextMeshProUGUI versionText;

    public GameObject SceneManager;
    public new Camera camera;
    TouchSettingsAdvanced TouchSettings;// = TouchManSettings._advanced;

    AccountConfigService AccountConfigService;

    public Button btnSignOut;
    #endregion



    int currentGraphicPreset;
    //IDebugFPSSpeed fpsSpeed;


    private void OnEnable()
    {
        AccountConfigService = new AccountConfigService();
        TouchSettings = SceneManager.GetComponent<TouchManMain>().settings.advanced; //TouchManSettings._advanced;//

        SetupMovementData();
        SetupCameraData();
        //HUDPanel.TransmitMovementSettingsData();
        Debug.Log("Current graphic level is : " + QualitySettings.GetQualityLevel());
        SetHighlighted(QualitySettings.GetQualityLevel());//currentGraphicPreset);
        versionText.text = "Version: " + Application.version;

        if (AppComponent.Instance.account.CRED_ISDEMO == string.Empty)
        {
            btnSignOut.interactable = false;
        }
        else
        {
            btnSignOut.interactable = true;
        }
    }

    public void OnSignOut()
    {
        // AppComponent.Instance.SignOut();
        WidgetManager.Instance.Confirm("Confirm", "Are you sure you want to sign out and exit design?", (string ans)=>{
            if (ans == "Yes")
            {
                Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, AppComponent.Instance.db.sceneType);
                AppComponent.Instance.account.CURRENT_ROOM_ID = string.Empty;
                AppComponent.Instance.account.CRED_USERID = AccountConfigService.demoUserID;
                AppComponent.Instance.account.CRED_ISDEMO = string.Empty;
                AppComponent.Instance.Navigate(AppPage.AppComponent);
            }
        }, false);
    }

    /*public void SetHighlighted(GraphicSettingPresets preset)
    {
        currentGraphicPreset = preset;
        switch (currentGraphicPreset)
        {
            case GraphicSettingPresets.low:
                SetHighlighted(lowButton);
                break;

            case GraphicSettingPresets.medium:
                SetHighlighted(mediumButton);
                break;

            case GraphicSettingPresets.high:
                SetHighlighted(highButton);
                break;
        }
    }*/

    //Set Graphics Button Current
    public void SetHighlighted(int level)
    {
        //currentGraphicPreset = preset;
        switch (level)//currentGraphicPreset)
        {
            case 0://GraphicSettingPresets.low:
                SetHighlighted(lowButton);
                break;

            case 2:// GraphicSettingPresets.medium:
                SetHighlighted(mediumButton);
                break;

            case 4:// GraphicSettingPresets.high:
                SetHighlighted(highButton);
                break;

            default:
                Debug.LogWarning("Unexpected quality level set, cannot update settings UI");
                break;
        }
    }

    private void SetHighlighted(Button button)
    {
        ResetGraphicButtonHighlights();
        button.gameObject.GetComponent<Image>().color = highlightColor;
        button.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
    }

    private void ResetGraphicButtonHighlights()
    {
        lowButton.gameObject.GetComponent<Image>().color = Color.white;
        lowButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = Color.gray;
        mediumButton.gameObject.GetComponent<Image>().color = Color.white;
        mediumButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = Color.gray;
        highButton.gameObject.GetComponent<Image>().color = Color.white;
        highButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = Color.gray;
    }

    /*public void ON_DEBUG_FPS_VALUE_SET(IDebugFPSSpeed val)
    {
        movementSpeed.text = val.movementSpeed.ToString();
        rotationSpeed.text = val.rotationSpeed.ToString();
        thresholdx.text = val.thresholdX.ToString();
        thresholdy.text = val.thresholdY.ToString();
    }

    public void onClickSubmit()
    {
        Messenger<IDebugFPSSpeed>.Broadcast(StaticFunctionCalls.DEBUG_FPS_VALUE, ResultObj);
    }*/


    float sanitize(string val)
    {
        float.TryParse(val, out float res);
        return res;
    }

    public void SendFeedback()
    {
        //#if PLATFORM_ANDROID
        //        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        //        {
        //            WidgetManager.Instance.Confirm("Storage Permission Required", "A basic screenshot will be sent with feedback. Storage permission is required to save screenshots.", (bool val) =>
        //              {
        //                  if (val)
        //                  {
        //                      Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        //                      if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)) return;
        //                      Debug.Log("Feedback popup click");
        //                      GameObject.Find("Feedback").GetComponent<EasyFeedback.FeedbackForm>().FeedbackKeyHit();
        //                  }
        //              }
        //            );
        //        }
        //        else
        //        {
        //            Debug.Log("Feedback popup click");
        //            GameObject.Find("Feedback").GetComponent<EasyFeedback.FeedbackForm>().FeedbackKeyHit();
        //        }
        //#else
        Debug.Log("Feedback popup click");
        GameObject.Find("Feedback").GetComponent<EasyFeedback.FeedbackForm>().FeedbackKeyHit();
        //#endif
    }
    /*
    public IDebugFPSSpeed ResultObj
    {
        get
        {
            return new IDebugFPSSpeed
            {
                movementSpeed = sanitize(movementSpeed.text),
                rotationSpeed = sanitize(rotationSpeed.text),
                thresholdX = sanitize(thresholdx.text),
                thresholdY = sanitize(thresholdy.text)
            };
        }
        set
        {
            Setup(value);
        }
    }

    public void Setup(IDebugFPSSpeed value)
    {
        movementSpeed.text = value.movementSpeed.ToString();
        rotationSpeed.text = value.rotationSpeed.ToString();
        thresholdx.text = value.thresholdX.ToString();
        thresholdy.text = value.thresholdY.ToString();
    }
    */
    public void UpdateMovementData()
    {
        TouchSettings.moveSpeed = new Vector2(sanitize(movementSpeedX.text), sanitize(movementSpeedY.text));
        TouchSettings.VelocityMultiplier = sanitize(VelocityMultiplier.text);
        TouchSettings.MovementVelocityDelay = sanitize(MovementVelocityDelay.text);
    }

    void SetupMovementData()
    {
        movementSpeedX.text = TouchSettings.moveSpeed.x.ToString();
        movementSpeedY.text = TouchSettings.moveSpeed.y.ToString();
        VelocityMultiplier.text = TouchSettings.VelocityMultiplier.ToString();
        MovementVelocityDelay.text = TouchSettings.MovementVelocityDelay.ToString();
    }

    public void UpdateCameraData()
    {
        Vector3 tempPosition = camera.gameObject.transform.position;
        tempPosition.y = sanitize(camHeight.text);
        camera.gameObject.transform.position = tempPosition;
        camera.fieldOfView = sanitize(camFOV.text);

        Vector3 tempRotation = new Vector3(sanitize(camAngle.text),0);
        camera.transform.localEulerAngles = tempRotation;
    }

    void SetupCameraData()
    {
        camHeight.text = camera.gameObject.transform.position.y.ToString("0.00");
        camFOV.text = camera.fieldOfView.ToString();
        camAngle.text = camera.transform.localEulerAngles.x.ToString("0.00");
    }

    #region graphicSettings broadcasts


    //public void GraphicsVeryLow()
    //{
    //    QualitySettings.SetQualityLevel(0, true);
    //}

    public void GraphicsLow() // Verylow is 0, ultra is 5
    {
        //Messenger<GraphicSettingPresets>.Broadcast(StaticFunctionCalls.GraphicSettings, GraphicSettingPresets.low);
        Messenger<int>.Broadcast(StaticFunctionCalls.PostProcessSettings, 0); //GraphicSettingPresets.low);
        SetHighlighted(0);//GraphicSettingPresets.low);
        QualitySettings.SetQualityLevel(0, true);
        AccountConfigService.GRAPHICS_LEVEL = 0;
    }
    public void GraphicsMedium()
    {
        //Messenger<GraphicSettingPresets>.Broadcast(StaticFunctionCalls.GraphicSettings, GraphicSettingPresets.medium);
        Messenger<int>.Broadcast(StaticFunctionCalls.PostProcessSettings, 2);// GraphicSettingPresets.medium);
        SetHighlighted(2);// GraphicSettingPresets.medium);
        QualitySettings.SetQualityLevel(2, true);
        AccountConfigService.GRAPHICS_LEVEL = 2;
    }
    public void GraphicsHigh()
    {
        //Messenger<GraphicSettingPresets>.Broadcast(StaticFunctionCalls.GraphicSettings, GraphicSettingPresets.high);
        Messenger<int>.Broadcast(StaticFunctionCalls.PostProcessSettings, 4);// GraphicSettingPresets.high);
        SetHighlighted(4);// GraphicSettingPresets.high);
        AccountConfigService.GRAPHICS_LEVEL = 4;
        Debug.Log(">>>>>>>>setting graphics to 4 or there is a player pref so defaulting to 4");
        QualitySettings.SetQualityLevel(4, true);
    }


    //public void GraphicsVeryHigh()
    //{
    //    QualitySettings.SetQualityLevel(4, true);
    //}

    //public void GraphicsUltra()
    //{
    //    QualitySettings.SetQualityLevel(5, true);
    //}
    #endregion
}
/*
public enum GraphicSettingPresets
{
    medium,
    low,
    high
}
*/
