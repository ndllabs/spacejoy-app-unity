﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
using UnityEngine.UI;



public class SliderManagerRotation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnEnable()
    {
        Messenger<int>.AddListener(StaticFunctionCalls.TransmitRotationValue, EditRotationSlider);
    }
    void OnDisable()
    {
        Messenger<int>.RemoveListener(StaticFunctionCalls.TransmitRotationValue, EditRotationSlider);
    }


    // Update is called once per frame
    void EditRotationSlider(int value)
    {
        if (value > 360 | value < 0) value = 0;
        GetComponent<Slider>().value = 360-value;
    }
}
