﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using _unity.Events;

public class btnGroupTemplateScript : MonoBehaviour
{
    public Image btnImg;
    public Image btnImgBg;
    public bool isSelected;
    public  Color color;
    
    private void OnEnable()
    {
        Deselect();//SetActiveWhite();
        //Messenger.AddListener(StaticFunctionCalls.LocalWorldSwithces, SetActiveWhite);
    }
    private void OnDisable()
    {
        //Messenger.RemoveListener(StaticFunctionCalls.LocalWorldSwithces, SetActiveWhite);
    }
    //public void OnSelected()
    //{
    //    //if(isSelected)
    //    //{
    //    //    SetActiveWhite();
    //    //}
    //    //else
    //    //{
    //        SetActiveRed();
    //    //}
    //}
    public void SetSelected()
    {
        //Debug.LogWarning("[" + nameof(btnGroupTemplateScript_Deprycated) + "]["+nameof(SetSelected) + "] Deprycation warning, SetSelected_Deprycated is deprycated, use a different approach to update the highlights");


        //Messenger.Broadcast(StaticFunctionCalls.LocalWorldSwithces);
        isSelected = true;
        btnImgBg.color = Color.white;
        btnImg.color = color;

        //print("Focus gained by "+btnImg.name);
    }
    public void Deselect()
    {
        //Debug.LogWarning("[" + nameof(btnGroupTemplateScript_Deprycated) + "][" + nameof(Deselect_Deprycated) + "] Deprycation warning, SetSelected_Deprycated is deprycated, use a different approach to update the highlights");


        isSelected = false;


        btnImgBg.color = color; //=> 
        btnImg.color = Color.white;
        //print("Focus lost by " + btnImg.name);
    }

}