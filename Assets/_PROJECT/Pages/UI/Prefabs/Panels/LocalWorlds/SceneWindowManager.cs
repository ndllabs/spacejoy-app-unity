﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
using UnityEngine.UI;

using Homefuly;

//Usage: Attach script to SceneManager and put panels into Refs
public class SceneWindowManager : MonoBehaviour
{
    #region Refs

    [Header("LocalWorld")] //Asset Panel
    public GameObject pnlLocalWorld;
    //public GameObject pnlHorizontal;
    //public GameObject pnlVertical;
    public btnGroupTemplateScript info, shop, swap, horizontal, vertical, rotate, delete;
    public Slider rotationSlider;

    //[Header("LocalWorld+")] //Advance Asset Panel
    //public GameObject pnlLocalWorldPlus;
    //public btnGroupTemplateScript distance, delete;

    [Header("LocalWallpaperWorld")] //Wallfinish Panel
    public GameObject pnlLocalWallpaperWorld;
    public btnGroupTemplateScript resize, position, deselectWall, deleteWall;

    [HideInInspector]
    AppComponent app;
    Panels currentActivePanel;

    bool isReadOnly;


    #endregion

    // Start is called before the first frame update
    void Start()
    {
        app= AppComponent.Instance;
        /*if (db == null) Debug.LogError("Fuckall db null start");

        if (db != AppComponent.Instance.db)
        {
            Debug.LogError("Fuckall db not db start");
            db = AppComponent.Instance.db;
        }*/
        //LoadPanel(Panels.LocalWorld);
        //Messenger.Broadcast(StaticFunctionCalls.LocalWorldSwithces);
        //Messenger.Broadcast("SetLocalActiveDefault");
    }
    private void OnEnable()
    {
        UnHighlightButtons();
        rotationSlider.gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        rotationSlider.gameObject.SetActive(false);
    }
    #region UI Service



    /*public void ButtonAdd()
    {
        //Function for button goes here
        //Debug.Log("WTF");
        LoadPanel(Panels.LocalWorldPlus);
    }*/

    public void ButtonRotate()
    {
        if (rotate.isSelected)
        {
            ButtonHorizontal();
            return;
        }
        UnHighlightButtons();
        rotate.SetSelected();
        Messenger.Broadcast(StaticFunctionCalls.ButtonRotate);
        rotationSlider.gameObject.SetActive(true);

    }

    public void ButtonHorizontal()
    {
        UnHighlightButtons();
        horizontal.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonHorizontal);

        rotationSlider.gameObject.SetActive(false);
    }

    public void ButtonVertical()
    {
        if (vertical.isSelected)
        {
            ButtonHorizontal();
            return;
        }
        UnHighlightButtons();
        vertical.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonVertical);
        rotationSlider.gameObject.SetActive(false);
    }

    public void Deselect()
    {
        UnHighlightButtons();
        delete.SetSelected();
        Messenger.Broadcast(StaticFunctionCalls.DoDeselectAsset_Deprycated);
    }

    /*public void ButtonClose()
    {
        //Function for button goes here
        LoadPanel(Panels.LocalWorld);
    }*/

    public void ButtonDelete()
    {
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonDelete_Deprycated);
    }

    public void ButtonDuplicate()
    {
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonDuplicate);
    }

    public void ButtonSwap()
    {
        UnHighlightButtons();
        swap.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonSwap);
    }

    public void ButtonInfo()
    {
        UnHighlightButtons();
        info.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonInfo);
    }

    public void ButtonShop()
    {
        UnHighlightButtons();
        shop.SetSelected();
        Messenger.Broadcast(StaticFunctionCalls.ShowProductInfo);
    }

    public void ButtonDistance()
    {
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonDistance);
        LoadPanel(Panels.LocalWorld, isReadOnly);
        ButtonHorizontal();
    }

    public void ButtonDimension()
    {
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.ButtonDimension);
    }

    public void ButtonResize()
    {
        UnHighlightButtons();
        resize.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.WallpaperResize_Deprycated);
    }

    public void ButtonPosition()
    {
        UnHighlightButtons();
        position.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.WallpaperPosition_Deprycated);
    }

    public void ButtonDeselectWallposter()
    {
        UnHighlightButtons();
        deselectWall.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.WallpaperDeselected);
    }

    public void ButtonDeleteWallpaper()
    {
        UnHighlightButtons();
        deleteWall.SetSelected();
        //Function for button goes here
        Messenger.Broadcast(StaticFunctionCalls.WallpaperDeleted);
    }

    #endregion

    #region Panel Management

    public void LoadPanel(Panels panel, bool isReadOnly)
    {
        currentActivePanel = panel;
        ResetPanels();
        this.isReadOnly = isReadOnly;
        switch (panel)
        {
            case Panels.LocalWorld:
                pnlLocalWorld.SetActive(true);
                if (app == null)
                {
                    Debug.LogWarning("[SceneWindowManager] appComponent reference is null again :///");
                    app = AppComponent.Instance;   //SOS
                }
                if (!app.db.IsCurrentObjectValid<IAssetEntry>()) return;
                MountType mt = (app.db.currentObject as IAssetEntry).asset.mountType.ToEnum<MountType>();
                bool Vertical = mt == MountType.floor | mt == MountType.wall | mt == MountType.ceiling? false : true;
                bool Rotate = mt == MountType.wall ? false : true;

                InfoOrShop();


                if (isReadOnly)
                {
                    swap.gameObject.SetActive(false);
                    horizontal.gameObject.SetActive(false);
                    vertical.gameObject.SetActive(false);
                    rotate.gameObject.SetActive(false);
                    delete.gameObject.SetActive(false);
                }
                else
                {

                    vertical.gameObject.SetActive(Vertical);
                    rotate.gameObject.SetActive(Rotate);
                }
                break;
            case Panels.LocalWallpaperWorld:
                pnlLocalWallpaperWorld.SetActive(true);
                break;
        }
    }

    void InfoOrShop()
    {
        if (!(app.db.currentObject is IAssetEntry))
        {
            Debug.LogError("Current Asset Cast to IAssetEntry Failed");
            return;
        }

        var thisAsset = (app.db.currentObject as IAssetEntry).asset;
        bool result = string.IsNullOrEmpty(thisAsset.designedBy.productUrl) | thisAsset.designedBy.productUrl.ToLower().Contains("homefuly") ? false : true;
        
            shop.gameObject.SetActive(result);
        info.gameObject.SetActive(!result);
    }
    void ResetPanels()
    {
        pnlLocalWorld.SetActive(false);
        //pnlLocalWorldPlus.SetActive(false);
        pnlLocalWallpaperWorld.SetActive(false);
        //rotationSlider.gameObject.SetActive(false);
    }

    void UnHighlightButtons()
    {
        switch (currentActivePanel)
        {
            case Panels.LocalWorld:
                vertical.Deselect();
                horizontal.Deselect();
                rotate.Deselect();
                swap.Deselect();
                info.Deselect();
                shop.Deselect();
                //deselect.Deselect();
                break;
            //case Panels.LocalWorldPlus:
            //break;
            case Panels.LocalWallpaperWorld:
                resize.Deselect();
                position.Deselect();
                deleteWall.Deselect();
                deselectWall.Deselect();
                break;
        }
    }


    #endregion

    public enum Panels
    {
        LocalWorld,
        //LocalWorldPlus,
        LocalWallpaperWorld
    }

}
