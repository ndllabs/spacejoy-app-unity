﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using _unity.Events;
using UnityEngine.Networking;

using Homefuly;

public class UIController_PanelWallpapers : MonoBehaviour
{
    public Transform container;
    public GameObject elemWallpaper;
    public GameObject elemLoader;
    bool init;

    private void OnEnable()
    {
        if (!init) Init();
    }

    public void Init()
    {
        init = true;
        Setup();
    }

    async void Setup()
    {
        container.DestroyChildren();
        elemLoader.SetActive(true);
        
        //replace with db fetch
        var result = await AppComponent.Instance.server.artifact.GetAssetWallpaperList("5bec052737cbc32dfa683a15");
        //result = await AppComponent.Instance.server.artifact.GetAssetWallpaperListWithTex_Deprycated(result);  //this server call is a waste SOS
        
        //NoData
        if (result == null) return;
        
        //Setting up textures
        foreach(var data in result)
        {
            data.texture = await AppComponent.Instance.server.artifact.GetWallpaperTexture(data._id);
        }

        GameObject g; Transform t;
        foreach (var data in result)
        {
            g = Instantiate(elemWallpaper) as GameObject;
            t = g.transform;
            Color color = Color.white;
            t.Find("Image").GetComponent<Image>().color = color;
            t.Find("txtName").GetComponent<TextMeshProUGUI>().text = data.name;
            //IEnumerator beta = DownloadImgTexture(t.Find("Image").GetComponent<Image>(), data.textureUrl);
            //StartCoroutine(beta);
            Texture2D txVar = (Texture2D)data.texture;
            t.Find("Image").GetComponent<Image>().sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));

            t.GetComponent<DragHandlerWallpaper>().wallfinish = data;

            GameObject gid = new GameObject("pId"); gid.transform.SetParent(g.transform);
            GameObject gidV = new GameObject(data._id); gidV.transform.SetParent(gid.transform);
            /*
            t.GetComponent<Button>().onClick.RemoveAllListeners();
            t.GetComponent<Button>().onClick.AddListener(() =>
            {
                Messenger<string>.Broadcast(StaticFunctionCalls.OnDragWallposter, data._id);
            });*/

            t.SetParent(container);
            t.localScale = Vector3.one;
        }
        elemLoader.SetActive(false);
    }
}