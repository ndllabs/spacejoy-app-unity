﻿using _unity.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TouchMan;

public class DragHandlerWallpaper : MonoBehaviour, IPointerDownHandler, ITouchManUI
{
    float time;
    bool pressed;
    PointerEventData storeClickPositon;
    //readonly float HoldThreshold = 0.3f;

    #region Drag NOW

    //public string id;
    //string assetName;
    public GameObject gDp;

    public IWallfinish wallfinish;
    //public bool isEnabled = false;


    private bool lifted;
    private Vector2 pointerCurrentPos;
    private Vector2 pointerStartPos;
    //private float pointerRightMoveThreshold = 120f;

    public void InitializePosterDrag()
    {
        gDp = Instantiate(Resources.Load("DragDropRes/DragDropWallfinishThumbnail", typeof(GameObject))) as GameObject;

        gDp.transform.SetParent(GameObject.Find("PanelFull").transform);
        gDp.GetComponent<Image>().sprite = transform.Find("Image").GetComponent<Image>().sprite;
        gDp.GetComponent<Image>().color = new Color32(117, 169, 193, 255);

        gDp.transform.Find("elemLoader").gameObject.SetActive(false);

        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, true);
        //id = transform.Find("pId").GetChild(0).name;
        gDp.GetComponent<DragDropWallfinishThumbnail>().wallfinish = wallfinish;//assetName = transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
        //gDp.GetComponent<DragDropPosterThumbnail>().id = id;

        storeClickPositon.pointerDrag = gDp;
        //gameObject.GetComponent<DragAndDropAssetV2>().OnBeginDrag(storeClickPositon);
        //ExecuteEvents.Execute(gameObject.GetComponent<DragAndDropAssetV2>().gDp, storeClickPositon, ExecuteEvents.initializePotentialDrag);
        ExecuteEvents.Execute(gDp, storeClickPositon, ExecuteEvents.beginDragHandler);
        //Debug.Log("Ready for Drag & Drop 2/5");
        ExecuteEvents.Execute(gDp, storeClickPositon, ExecuteEvents.dragHandler);
        //Destroy(this);

    }

    #endregion

    /*void Update()
    {
        //Temp Only, Use touchman Events Instead
        if (lifted)
        {
            if (Input.touchSupported && Input.touchCount > 0)
            {
                pointerCurrentPos = Input.touches[0].position;
                if (Input.touches[0].phase == TouchPhase.Ended)
                    lifted = false;
            }
            else
            {
                pointerCurrentPos = Input.mousePosition;
                if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
                    lifted = false;
            }


            MakeAssetReadyForDragAndDrop();
        }
    }*/

    //Temp Only, Use touchman Events Instead
    public void OnPointerDown(PointerEventData eventData)
    {
        /*if (Input.touchSupported && Input.touchCount == 1)
            pointerStartPos = Input.touches[0].position;
        else pointerStartPos = Input.mousePosition;

        pointerCurrentPos = eventData.position;

        time = Time.time;
        pressed = lifted = true;*/
        storeClickPositon = eventData;
    }

    public void OnUIDraggedTowardsWorld()
    {
        InitializePosterDrag();
    }

    public void OnUISwiped()
    {
        //throw new System.NotImplementedException();
    }



    /*//Temp Only, Use touchman Events Instead
    public void OnPointerUp(PointerEventData eventData)
    {
        //Debug.Log("OnPointerUp 6/5");
        pressed = false;
    }

    void MakeAssetReadyForDragAndDrop()
    {
        if ((Mathf.Abs(pointerCurrentPos.x - pointerStartPos.x) > pointerRightMoveThreshold && pointerCurrentPos.x / Screen.width < 0.6f) ||
         (Time.time - time > HoldThreshold && pressed))
        {
            pressed = false; lifted = false;
            InitializePosterDrag();
        }
    }*/
}
