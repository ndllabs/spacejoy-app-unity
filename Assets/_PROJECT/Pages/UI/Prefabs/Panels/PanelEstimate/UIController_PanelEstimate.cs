﻿using _unity.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using Homefuly;

public class UIController_PanelEstimate : MonoBehaviour
{
    [Header("References")]
    public Transform container;
    public GameObject elemDetails;
    public GameObject elemLoader;
    public TextMeshProUGUI txtTotal;
    [HideInInspector]
    bool init;
    Dictionary<string, IAssetEntry> sceneAssetData;
    float EstimateTotal;

    public void Init(Dictionary<string, IAssetEntry> sceneAssetData)
    {
        this.sceneAssetData = sceneAssetData;
        init = false;
    }

    void Init()
    {
        if (!init)
        {
            init = true;
            try
            {
                EstimatesSetup();
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    private void OnEnable()
    {
        Init();
    }

    async void EstimatesSetup()
    {
        elemLoader.SetActive(true);
        //sceneAssetData = db.sceneAssets;
        container.DestroyChildren();
        EstimateTotal = 0;
        if (sceneAssetData.Count == 0) //if no data found
        {
            GameObject g = Instantiate(elemDetails) as GameObject;
            Transform t = g.transform;
            t.SetParent(container.transform, false);

            t.Find("txtName").GetComponent<TextMeshProUGUI>().text = "No Assets found";
            t.Find("txtBrand").GetComponent<TextMeshProUGUI>().text = "";
            t.Find("txtPrice").gameObject.SetActive(false);
            //t.Find("imgProduct").GetComponent<Image>().sprite = null;
            t.Find("btnAddToCart").gameObject.SetActive(false);
            t.Find("btnDelete").gameObject.SetActive(false);
            t.localScale = Vector3.one;
            txtTotal.text = "";
            elemLoader.SetActive(false);
            return;
        }
        //if data found
        List<Task> assetImageLoadTasks = new List<Task>();
        foreach (IAssetEntry res in sceneAssetData.Values)
        {
            bool isShoppable = res.asset.category != AppConfig.PilotDemoConfig.NonShoppableCatId ? true : false;
            if (res.asset.currency == "INR" | !isShoppable | res.asset.price.customer == 0) continue;// Destroy(gameObject);   //Dont show INR products or nonshoppable products   //Dont show products with price = 0
            GameObject g = Instantiate(elemDetails) as GameObject;
            Transform t = g.transform;
            t.SetParent(container.transform, false);



            t.Find("txtName").GetComponent<TextMeshProUGUI>().text = res.asset.name.ToTitleCase();
            //IEnumerator beta = DownloadImgTexture(t.Find("imgProduct").GetComponent<Image>(), res.asset.textureUrl);
            //StartCoroutine(beta);
            t.Find("txtPrice").GetComponent<TextMeshProUGUI>().text = res.asset.currency == "USD" ? "$" + res.asset.price.customer : "INR " + res.asset.price.customer;
            if (res.asset.textureUrl == null) res.asset.textureUrl = "/assets/" + res.asset._id + "/" + res.asset._id + ".jpg";
            assetImageLoadTasks.Add(DownloadTex(t, res.asset.textureUrl));
            t.Find("imgAsset").Find("elemLoader").gameObject.SetActive(true);
            //Button b = t.GetComponent<Button>();
            //b.gameObject.SetActive(true);
            //b.onClick.RemoveAllListeners();
            //b.onClick.AddListener(() => Messenger<GameObject>.Broadcast(AppEvents.ASSET_TAPPED, res.objRef));
            //if company name is missing
            if (res.asset.designedBy.name == string.Empty)
            {
                //Debug.Log("Company name missing, checking DB");
                if (!AppComponent.Instance.db.retailers.ContainsKey(res.asset.designedBy.organizationInternalLink))
                {
                    Debug.LogError("[Estimate] could not fetch the retailer from db, asset name is " + res.asset.name + " \norganizationInternalLink is " + res.asset.designedBy.organizationInternalLink);
                }
                else
                    res.asset.designedBy.name = AppComponent.Instance.db.retailers[res.asset.designedBy.organizationInternalLink].name;
            }
            t.Find("txtBrand").GetComponent<TextMeshProUGUI>().text = res.asset.designedBy.name.ToTitleCase();
            t.Find("btnAddToCart").GetComponent<Button>().onClick.AddListener(() =>
            {
                HomefulyExtensions.OpenProductStorePage(res.asset, Homefuly.Analytics.ShopButtonType.estimate_panel);

                //string url = !string.IsNullOrEmpty(res.asset.designedBy.productUrl)
                //    ? res.asset.designedBy.productUrl
                //    : "https://www.google.co.in/search?q=" + res.asset.name;
                ////System.Diagnostics.Process.Start(url);
                //url = System.Uri.EscapeUriString(url);

                //InAppBrowser.OpenURL(url);
                //Messenger<IAsset, AppPage, string>.Broadcast(StaticFunctionCalls.ShopButtonClicked, res.asset, AppPage.Designer, "Estimate_Panel");

            });
            t.Find("btnDelete").GetComponent<Button>().onClick.AddListener(() => Messenger.Broadcast(StaticFunctionCalls.DeleteAsset));
            t.localScale = Vector3.one;
            //t.Find("txtCount").GetComponent<TextMeshProUGUI>().text = "x"+res.Value;

            //non shoppable comparing
            //bool isShoppable = res.asset.category != AppConfig.PilotDemoConfig.NonShoppableCatId ? true : false;

            t.Find("btnAddToCart").gameObject.SetActive(isShoppable);
            t.Find("txtPrice").gameObject.SetActive(isShoppable);


            if (isShoppable && (res.inuse) && (res.asset.currency == "USD"))    //Add to total if conditions are met
                EstimateTotal += res.asset.price.customer;


        }
        elemLoader.SetActive(false);
        await Task.WhenAll(assetImageLoadTasks);
        txtTotal.text = "Total : $ " + Math.Round(EstimateTotal, 2);
    }

    async Task DownloadTex(Transform transform, string url)
    {
        url = url.Replace(".jpg", "_t.jpg").Replace(".png", "_t.png");  //to get lowres thumbnail img

        var trans = transform.Find("imgAsset");
        Image texture = trans.GetComponent<Image>();
        Texture2D txVar = (Texture2D)await AppComponent.Instance.server.web.GetTexture(url);
        if (txVar != null)
        {
            texture.sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));
        }
        trans.GetComponent<Image>().color = Color.white;
        trans.Find("elemLoader").gameObject.SetActive(false);
        Destroy(trans.Find("elemLoader").gameObject);
    }
}
