﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
using TMPro;
using UnityEngine.UI;

using Homefuly;

public class UIController_PanelViewPoints : MonoBehaviour
{

    public Transform container;
    public GameObject ViewPointElem;

    public void OnSavePlayerPosition()
    {
        Messenger.Broadcast(StaticFunctionCalls.TriggerPositionSave);
    }
    
    public void CreateRenderPositionBeacon(int positionData)
    {
        container.DestroyChildren();
        for (int i = 0; i < positionData; i++)
        {
            GameObject newPositionPanel = Instantiate(ViewPointElem) as GameObject;
            Transform t = newPositionPanel.transform;
            t.SetParent(container.transform, false);
            TextMeshProUGUI name = t.Find("name").GetComponent<TextMeshProUGUI>();
            name.text = "View " + (i + 1);
            //GetComponentInChildren<TextMeshProUGUI>().text = 
            //Debug.Log("3/5 Creating button of index "+positionData);
            t.Find("btnViewThePoint").GetComponent<Button>().onClick.RemoveAllListeners();
            t.Find("btnViewThePoint").GetComponent<Button>().onClick.AddListener(() =>
            {
                //int x = int.Parse(newPositionButton.GetComponentInChildren<TextMeshProUGUI>().text);
                //Debug.Log(" 4/5 Button view Clicked, it holds this data: "+(int.Parse(name.text)-1));
                Messenger<int>.Broadcast(StaticFunctionCalls.RelocatePlayer, (int.Parse(name.text.Substring(5)) - 1));
            });
            t.Find("btnDeletePoint").GetComponent<Button>().onClick.RemoveAllListeners();
            t.Find("btnDeletePoint").GetComponent<Button>().onClick.AddListener(() =>
            {
                //int x = int.Parse(newPositionButton.GetComponentInChildren<TextMeshProUGUI>().text);
                //Debug.Log(" 4/5 Button delete Clicked, it holds this data: "+ (int.Parse(name.text)-1));
                Messenger<int>.Broadcast(StaticFunctionCalls.DeleteViewpoint, (int.Parse(name.text.Substring(5)) - 1));
                //Destroy(newPositionPanel);
            });

        }
        //newPositionPanel.GetComponent<Button>().onClick.RemoveAllListeners();
        //newPositionPanel.GetComponent<Button>().onClick.AddListener(() =>
        //{
        //int x = int.Parse(newPositionButton.GetComponentInChildren<TextMeshProUGUI>().text);
        //Debug.Log(" 4/5 Button Clicked, it holds this data: "+positionData);
        //    Messenger<int>.Broadcast(StaticFunctionCalls.RelocatePlayer, positionData);
        //});
    }
}
