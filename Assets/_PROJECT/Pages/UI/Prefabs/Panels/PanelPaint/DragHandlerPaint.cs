﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using _unity.Events;
using TouchMan;

public class DragHandlerPaint : MonoBehaviour, IPointerDownHandler, ITouchManUI
{
    #region Dont Drag Yet Res
    float time;
    bool pressed;
    PointerEventData storeClickPositon;
    //readonly float HoldThreshold = 0.3f;
    #endregion

    #region Drag NOW Res

    public IWallfinish wallfinish;
    Color color;
    //string id;
    //string colorName;
    public GameObject gDp;
    private bool lifted;
    private Vector2 pointerCurrentPos;
    private Vector2 pointerStartPos;
    //private float pointerRightMoveThreshold = 120f;

    public void Start()
    {
        color = Color.white;
        ColorUtility.TryParseHtmlString(wallfinish.code, out color);//transform.Find("Image").GetComponent<Image>().color;
        //id = wallfinish._id;//transform.Find("pId").GetChild(0).name;
        //colorName = wallfinish.name;//transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
    }

    public void InititalizeDragAndDrop()
    {
        gDp = Instantiate(Resources.Load("DragDropRes/DragDropWallfinishThumbnail", typeof(GameObject))) as GameObject;
        gDp.transform.SetParent(GameObject.Find("PanelFull").transform);
        gDp.GetComponent<Image>().color = color;
        //gDp.transform.Find("elemLoader").gameObject.SetActive(false);

        Messenger<bool>.Broadcast(StaticFunctionCalls.AssetDragFromStoreEvents, true);
        DragDropWallfinishThumbnail dragDropPaintThumbnail = gDp.GetComponent<DragDropWallfinishThumbnail>();
        dragDropPaintThumbnail.wallfinish = wallfinish;
        //dragDropPaintThumbnail.color = color;
        //paintDragDropThumbnailScript.assetName = colorName;
        //paintDragDropThumbnailScript.id = id;

        storeClickPositon.pointerDrag = gDp;
        //ExecuteEvents.Execute(gameObject.GetComponent<DragAndDropAssetV2>().gDp, storeClickPositon, ExecuteEvents.initializePotentialDrag);
        ExecuteEvents.Execute(gDp, storeClickPositon, ExecuteEvents.beginDragHandler);
        //Debug.Log("Ready for Drag & Drop 2/5");
        ExecuteEvents.Execute(gDp, storeClickPositon, ExecuteEvents.dragHandler);
    }

    #endregion

    /*void Update()
    {
        //Temp Only, Use touchman Events Instead
        if (lifted)
        {
            if (Input.touchSupported && Input.touchCount > 0)
            {
                pointerCurrentPos = Input.touches[0].position;
                if (Input.touches[0].phase == TouchPhase.Ended)
                    lifted = false;
            }
            else
            {
                pointerCurrentPos = Input.mousePosition;
                if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
                    lifted = false;
            }


            MakeAssetReadyForDragAndDrop();
        }
    }*/

    //Temp Only, Use touchman Events Instead
    public void OnPointerDown(PointerEventData eventData)
    {
        /*if (Input.touchSupported && Input.touchCount == 1)
            pointerStartPos = Input.touches[0].position;
        else pointerStartPos = Input.mousePosition;

        pointerCurrentPos = eventData.position;

        time = Time.time;
        pressed = lifted = true;*/
        storeClickPositon = eventData;
    }

    public void OnUIDraggedTowardsWorld()
    {
        InititalizeDragAndDrop();
    }

    public void OnUISwiped()
    {
        //throw new System.NotImplementedException();
    }


    /*public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
    }

    void MakeAssetReadyForDragAndDrop()
    {
        if ((Mathf.Abs(pointerCurrentPos.x - pointerStartPos.x) > pointerRightMoveThreshold && pointerCurrentPos.x / Screen.width < 0.6f) ||
         (Time.time - time > HoldThreshold && pressed))
        {
            pressed = false; lifted = false;
            InititalizeDragAndDrop();
        }
    }*/
}
