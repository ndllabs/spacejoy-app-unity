﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using _unity.Events;
using _unity;
using System;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Homefuly;

public class UIController_PanelPaint : MonoBehaviour
{
    #region Refs and variables

    [Header("Prefabs")]
    public GameObject elemPaint;
    public GameObject elemPaintGroup;
    public GameObject elemPaintRetailer;
    public GameObject popupPaintBuy;

    [Header("Sub Panels")]
    public GameObject PanelPaints;
    public GameObject PanelPaintGroups;
    public GameObject PanelPaintRetailers;

    [Header("Content holders")]
    public Transform containerPaint;
    public Transform containerPaintGroup;
    public Transform containerPaintRetailer;

    [Header("References")]
    public Button btnNavBarBack;
    public GameObject elemLoader;
    AppComponent app;
    bool init;

    [Header("Data variables")]
    Dictionary<string, IRetailer> paintRetailerlist;
    List<IWallfinishGroup> paintGrouplist;
    List<IWallfinish> paintsList;

    #endregion

    public void Init(Dictionary<string, IRetailer> data)
    {
        paintRetailerlist = data;
        init = false;
    }

    void Init()
    {
        if (!init)
        {
            paintGrouplist = new List<IWallfinishGroup>();
            paintsList = new List<IWallfinish>();
            init = true;
            app = AppComponent.Instance;
            SetupPaintRetailers();
            LoadUIState(UI_Paint_State.retailer);
        }
    }

    private void OnEnable()
    {
        Init();
        //LoadUIState(UI_Paint_State.retailer);
        //Debug.LogError("Set paint Retailers called");
    }

    private void OnDisable()
    {
        if (popupPaintBuy.activeSelf)
            Messenger.Broadcast(ChallengeUICalls.ClosePaintBuyPopup);
    }

    public void LoadUIState(UI_Paint_State state, string metaData = null)
    {
        ResetStates();
        btnNavBarBack.onClick.RemoveAllListeners();
        switch (state)
        {
            case UI_Paint_State.retailer:
                PanelPaintRetailers.SetActive(true);
                btnNavBarBack.onClick.AddListener(() => Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global));
                break;
            case UI_Paint_State.paintgroup:
                PanelPaintGroups.SetActive(true);
                if (metaData != null) SetupPaintGroups(metaData);
                btnNavBarBack.onClick.AddListener(() => LoadUIState(UI_Paint_State.retailer, null));
                break;
            case UI_Paint_State.paint:
                PanelPaints.SetActive(true);
                SetupPaints(metaData);
                btnNavBarBack.onClick.AddListener(() => LoadUIState(UI_Paint_State.paintgroup, null));
                break;
        }
    }

    private void ResetStates()
    {
        if(popupPaintBuy.activeSelf)
        Messenger.Broadcast(ChallengeUICalls.ClosePaintBuyPopup);
        PanelPaintGroups.SetActive(false);
        PanelPaintRetailers.SetActive(false);
        PanelPaints.SetActive(false);
    }


    private void SetupPaintRetailers()
    {
        //paintRetailerlist = app.db.GetRetailersOfType(RetailerType.paint);
        GameObject g; Transform t;
        containerPaintRetailer.DestroyChildren();
        foreach (IRetailer retailer in paintRetailerlist.Values)
        {
            //if (retailer.country != "US") continue; //Replace with prajwal db code fix in parameter itself SOS
            if (retailer.name == "Dulux" | retailer.name == "Restoration Hardware" | retailer.name == "Homefuly Wallpapers") continue;   //These retailers have 0 paints in database so skipping for now
            g = Instantiate(elemPaintRetailer) as GameObject;
            t = g.transform;
            Texture2D tex = Resources.Load<Texture2D>("TextureRetailers/" + retailer.name);
            if (tex != null)
                t.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));
            else Debug.LogError("This one problem child: " + retailer.name);
            t.GetComponent<Button>().onClick.AddListener(() =>
            {
                LoadUIState(UI_Paint_State.paintgroup, retailer._id);
            });
            t.SetParent(containerPaintRetailer);
            t.localScale = Vector3.one;
        }
    }



    private async void SetupPaintGroups(string id)
    {
        elemLoader.SetActive(true);
        GameObject g; Transform t;
        containerPaintGroup.DestroyChildren();

        await FetchPaintGroups(id);  //not optimal to call each paint group once

        List<Task> PaintLoadTasks = new List<Task>();
        foreach (IWallfinishGroup paintGroup in paintGrouplist)
        {
            g = Instantiate(elemPaintGroup) as GameObject;
            t = g.transform;
            t.GetComponentInChildren<TextMeshProUGUI>().text = paintGroup._id.Contains("-") ? paintGroup._id.Substring(paintGroup._id.IndexOf("-") + 1).ToTitleCase() : paintGroup._id.ToTitleCase();

            #region new paintGroup hintColorImages
            PaintLoadTasks.Add(PaintMiniLoader(t, paintGroup._id));
            #endregion

            t.GetComponent<Button>().onClick.AddListener(() =>
            {
                LoadUIState(UI_Paint_State.paint, paintGroup._id);
            });
            t.SetParent(containerPaintGroup);
            t.localScale = Vector3.one;
        }

        await Task.WhenAll(PaintLoadTasks);
        elemLoader.SetActive(false);
    }

    async Task PaintMiniLoader(Transform t, string id)
    {
        //Skipping faulty paints
        Regex regx = new Regex("^.{7}$", RegexOptions.IgnoreCase);

        List<IWallfinish> groupPaintsList = await app.server.artifact.GetPaintsOfGroup(id, 0, 10);
        int i = 1;  //range 1 to 5
        foreach (IWallfinish paint in groupPaintsList)
        {
            if (!regx.IsMatch(paint.code)) continue;  //Faulty paints skipping
            Color color = Color.white;
            ColorUtility.TryParseHtmlString(paint.code, out color);
            t.Find("imgclr" + i).GetComponent<Image>().color = color;
            i++;
            if (i > 5) break;
        }
    }

    private async void SetupPaints(string id)
    {
        elemLoader.SetActive(true);
        var BoughtPaints = app.db.userSavedData.paintOwned;
        GameObject g; Transform t;
        containerPaint.DestroyChildren();
        PanelPaints.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1); //to bring it to the top

        await FetchPaints(id, 40);   //Sample data here

        #region Faulty Paints

        //Skipping faulty paints
        Regex regx = new Regex("^.{7}$", RegexOptions.IgnoreCase);

        #endregion

        foreach (IWallfinish paint in paintsList)
        {
            if (!regx.IsMatch(paint.code)) continue;  //Faulty paints skipping
            g = Instantiate(elemPaint) as GameObject;
            t = g.transform;

            paint.price = AppConfig.GameConfig.DefaultWallfinishPrice;
           
          
            Button b = t.Find("Button").GetComponent<Button>();
            if (!AppConfig.GameConfig.creativeMode && !BoughtPaints.Contains(paint._id))
            {
                t.GetComponentInChildren<TextMeshProUGUI>().text = paint.price.ToString();
                b.onClick.RemoveAllListeners();
                b.onClick.AddListener(() =>
                {
                    popupPaintBuy.SetActive(true);
                    Messenger<IWallfinish, Button>.Broadcast(ChallengeUICalls.OpenPaintBuyPopup, paint, b); //Show Paint buy popup
                }
                );
            }
            else
            {
                DragHandlerPaint dhp = g.AddComponent<DragHandlerPaint>();
                dhp.wallfinish = paint;
                Destroy(b.gameObject);
                t.Find("imgCoin").gameObject.SetActive(!AppConfig.GameConfig.creativeMode);
                t.Find("Text").gameObject.SetActive(!AppConfig.GameConfig.creativeMode);
            }
            Color color = Color.white;
            ColorUtility.TryParseHtmlString(paint.code, out color);
            t.Find("Image").GetComponent<Image>().color = color;

          
           

            //Texture2D tex = (Texture2D)paint.texture;
            //if (tex != null)
            //    t.Find("Image").GetComponent<Image>().overrideSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));

            GameObject gid = new GameObject("pId"); gid.transform.SetParent(g.transform);
            GameObject gidV = new GameObject(paint._id); gidV.transform.SetParent(gid.transform);

            t.GetComponent<Button>().onClick.RemoveAllListeners();
            t.GetComponent<Button>().onClick.AddListener(() =>
            {
                Debug.LogWarning("Lol Bro, Clicking Paints? We Dont Do that Here");
            });

            t.SetParent(containerPaint);
            t.localScale = Vector3.one;
        }
        elemLoader.SetActive(false);
    }


           

    #region data fetching

    async Task FetchPaintGroups(string metaData)
    {
        paintGrouplist.Clear();
        paintGrouplist = await app.server.artifact.GetPaintGroups(metaData);
        if (paintGrouplist == null)
        {
            Debug.LogError("Paint Groups Fetch Failed for id " + metaData);
        }
    }


    async Task FetchPaints(string metadata, int limit = 100)
    {
        //paginatorComponent.DestroyChildren();

        //switch (currentPaintState)
        //{
        //    case PaintState.paintsFromGroup:
        //        await Server.Instance.I_GetCountPaintsOfGroup(metadata, retVal => resCount = retVal["data"]);
        //        if (resCount > 0) await Server.Instance.I_GetAllPaintsOfGroup(metadata, retval => paintData = retval, 0, itemsOnPaintPage);
        //        break;
        //    case PaintState.paintsFromRetailer:
        //        await Server.Instance.I_GetCountPaintsOfRetailer(metadata, retVal => resCount = retVal["data"]);
        //        if (resCount > 0) await Server.Instance.I_GetAllPaintsOfRetailer(metadata, retval => paintData = retval, 0, itemsOnPaintPage);
        //        break;
        //    case PaintState.paintsFromRetailerSearch:
        //        await Server.Instance.I_GetCountPaintsOfRetailerSearch(metadata, searchKey, retVal => resCount = retVal["data"]);
        //        if (resCount > 0) await Server.Instance.I_GetAllPaintsOfRetailerSearch(metadata, searchKey, retval => paintData = retval, 0, itemsOnPaintPage);
        //        break;
        //}
        //if (resCount > itemsOnPaintPage)
        //{
        //    paginatorComponent.AddPageButtons((resCount - 1) / itemsOnPaintPage + 1, metadata); //add pagination buttons
        //}

        paintsList.Clear();
        paintsList = await app.server.artifact.GetPaintsOfGroup(metadata, 0, limit);
        if (paintsList == null)
        {
            Debug.Log("No paints found");
        }
    }

    #endregion

}

public enum UI_Paint_State
{
    retailer,
    paintgroup,
    paint
}
