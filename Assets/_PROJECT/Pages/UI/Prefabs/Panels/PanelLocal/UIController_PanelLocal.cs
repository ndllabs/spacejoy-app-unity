﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using _unity;
using _unity.Events;
using System.Collections.Generic;
using System.Threading.Tasks;

using Homefuly;

public class UIController_PanelLocal : MonoBehaviour
{
    //public Button swapButton;
    [Header("References")]
    public Texture2D defaultSprite;
    public Button shopButton;
    public GameObject elemLoader;

    public Image assetImage;
    public TextMeshProUGUI assetName;
    public TextMeshProUGUI assetCompany;
    public TextMeshProUGUI assetDimension;
    public TextMeshProUGUI assetPrice;

    IAsset thisAsset;
    bool isShoppable;
    //[Header("[ Rotation ]")]
    //public Slider sldRotation;

    //bool showDistance = false;

    //private void OnDisable()
    //{
    //    Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global);
    //}

    #region Card Details

    public async void Setup(IAsset asset)
    {
        ImageLoading(true);
        thisAsset = asset;
        assetName.text = asset.name.ToTitleCase();
        //if company name is missing
        if (asset.designedBy.name == string.Empty)
        {
            //Debug.Log("Company name missing, checking DB");
            asset.designedBy.name = AppComponent.Instance.db.retailers[asset.designedBy.organizationInternalLink].name;
        }
        assetCompany.text = asset.designedBy.name;

        //Sanitizing the dimensions to display inches
        assetDimension.text = SanitizeFeetToInches(asset.dimension.depth) + "\" d  X  " + SanitizeFeetToInches(asset.dimension.width) + "\" w  X  " + SanitizeFeetToInches(asset.dimension.height) + "\" h "; //SOS

        Debug.Log("Comparing: " + asset.category + ", with " + AppConfig.PilotDemoConfig.NonShoppableCatId);
        isShoppable = asset.category != AppConfig.PilotDemoConfig.NonShoppableCatId ? true : false;

        shopButton.gameObject.SetActive(isShoppable);
        assetCompany.gameObject.SetActive(isShoppable);
        assetPrice.gameObject.SetActive(isShoppable);

        assetPrice.text = asset.currency == "USD" ? "$" + asset.price.customer : "INR " + asset.price.customer;

        //sldRotation.onValueChanged.RemoveAllListeners();
        //sldRotation.GetComponent<Slider>().value = ; //Update Rot Here
        //sldRotation.onValueChanged.AddListener((float val) => { OnRotation(val); });

        //showDistance = false;
        if (asset.texture == null)
        {
            if (asset.textureUrl == null)
                asset.textureUrl = "/assets/" + asset._id + "/" + asset._id + ".jpg";
            //Debug.Log(asset.textureUrl);
            await DownloadTex(asset.textureUrl);//AppComponent.Instance.server.artifact.GetAssetImage(asset._id); //not optimal SOS
        }
        else
        {
            assetImage.sprite = Sprite.Create((Texture2D)asset.texture, new Rect(0, 0, asset.texture.width, asset.texture.height), new Vector2(.5f, .5f));// Sprite.Create(defaultSprite, new Rect(0, 0, defaultSprite.width, defaultSprite.height), new Vector2(.5f, .5f));
        }
        ImageLoading(false);
    }
    #endregion

    public float SanitizeFeetToInches(float val)
    {
        return (float)Math.Round((double)val * 12, 1);
    }

    async Task DownloadTex(string url)
    {
        //url = url.Replace(".jpg", "_c.jpg").Replace(".png", "_c.png");  //to get lowres thumbnail img

        //Image texture = transform.Find("imgAsset").GetComponent<Image>();
        Texture2D txVar = (Texture2D)await AppComponent.Instance.server.web.GetTexture(url);
        //LayoutElement layout = transform.GetComponent<LayoutElement>();
        if (txVar != null)
        {
            assetImage.sprite = Sprite.Create(txVar, new Rect(0, 0, txVar.width, txVar.height), new Vector2(.5f, .5f));
        }
        else
        {
            Debug.LogWarning("Couldn't Fetch image despite URL");
            assetImage.sprite = Sprite.Create(defaultSprite, new Rect(0, 0, defaultSprite.width, defaultSprite.height), new Vector2(.5f, .5f));
        }
    }

    void ImageLoading(bool value)
    {
        assetImage.color = value ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);// SetActive(!value);
        elemLoader.SetActive(value);
    }

    public void Shop()
    {
        HomefulyExtensions.OpenProductStorePage(thisAsset, Homefuly.Analytics.ShopButtonType.legacy);

        //string url = "";

        //if (!string.IsNullOrEmpty(thisAsset.designedBy.productUrl))
        //{
        //    url = thisAsset.designedBy.productUrl;
        //}
        //else
        //{
        //    url = "https://www.google.co.in/search?q=" + thisAsset.name;
        //}

        ////_.l("OPenning : " + url);
        ////System.Diagnostics.Process.Start(url);

        //url = System.Uri.EscapeUriString(url);
        //InAppBrowser.OpenURL(url);
        //Messenger<IAsset, AppPage, string>.Broadcast(StaticFunctionCalls.ShopButtonClicked, thisAsset, AppPage.Designer, "Info_Panel");

    }

    //#region Card Action
    //public void Delete()
    //{
    //    Messenger.Broadcast(StaticFunctionCalls.OnDeleteAssetFromScene);
    //}

    //public void Hide()
    //{
    //    Messenger.Broadcast(StaticFunctionCalls.OnHideAsset);
    //}
    //#endregion


    //#region Card Views
    //public void Distance()
    //{
    //    Messenger<bool>.Broadcast(StaticFunctionCalls.OnShowAssetRelatedDistances, !showDistance);
    //    showDistance = !showDistance;
    //}

    //public void Dimension()
    //{

    //}
    //#endregion

    //#region Card Rotation
    //public void EnableRotation(bool val = true)
    //{
    //    sldRotation.GetComponent<Slider>().enabled = val;
    //}

    //public void OnRotation()
    //{
    //    // sldRotation.GetComponent<Slider>().value;
    //    //Messenger<float>.Broadcast(StaticFunctionCalls.OnSetAssetYRotation, -sldRotation.GetComponent<Slider>().value);
    //}

    //public void OnRotation(float val)
    //{
    //    //snapper
    //    if (val % 90f < 18)
    //    {
    //        sldRotation.GetComponent<Slider>().value = 90f * (float)Math.Floor(val / 90f);
    //        val = 90f * (float)Math.Floor(val / 90f);
    //    }
    //    //{ }

    //    //rot.y = rot.y % 90f > 45 ? 90f * (float)(Math.Ceiling(rot.y / 90f)) : 90f * (float)Math.Floor(rot.y / 90f);
    //    Messenger<float>.Broadcast(StaticFunctionCalls.OnSetAssetYRotation, val);
    //}

    //#endregion

}
