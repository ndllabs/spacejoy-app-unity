﻿using System.Collections;
using UnityEngine;
using System.Globalization;
using UnityEngine.UI;
using _unity.Events;
using TMPro;
using System;
using System.Collections.Generic;

using Homefuly.Database;
using Homefuly;
using Homefuly.Events;
using System.Threading.Tasks;

public class UIController_HUD : MonoBehaviour
{
    #region Setup refs

    [Header("References")]
    AppDatabase db;
    AppComponent app;
    bool isReadOnly;
    //Dictionary<string, List<IWallfinishGroup>> paintGroupList = new Dictionary<string, List<IWallfinishGroup>>();

    //public Transform PaintContainer;
    //public GameObject PaintElem;

    //public Transform PaintGroupContainer;
    //public GameObject PaintGroupElem;

    //public Transform PaintRetailerContainer;
    //public GameObject PaintRetailerElem;

    //public GameObject elemStoreAsset;
    //public Transform ProductContainer;

    //public Transform WallpaperContainer;
    //public GameObject WallpaperElem;


    //public Transform ProductListContainer;
    //public GameObject elemProductListElement;


    [Header("Panels and stuff")]
    public UIController_PanelGlobalActions panelGlobalActions;
    public UIController_PanelPaint panelPaint;
    public UIController_PanelLoading panelLoading;
    public UIController_PanelAssetStore panelAssetStore;
    public UIController_PanelWallpapers panelWallpapers;
    public UIController_PanelEstimate panelEstimate;
    public UIController_PanelSettings panelSettings;
    public UIController_PanelViewPoints panelViewpoints;
    AccountConfigService accountConfigService;
    public GameObject panelMenu;
    public GameObject panelInfoMenu;
    public GameObject panelLocalWorldParent;
    public GameObject panelLocalWorld;
    public GameObject panelLocalWorldPlus;
    public UIController_PanelLocal panelLocal;
    public UIController_PanelSwap panelSwap;
    public Button buttonBack;
    public GameObject additionalOptions;
    public GameObject popupGotoChallenge;

    public GameObject pnlPurchases;
    [Header("Texts")]
    public TextMeshProUGUI title;
    public TextMeshProUGUI infoMenuTitle;

    [Header("World Buttons")]
    public GameObject worldButtonContainer;
    public Button maximiseButton;
    public GameObject btnHelp;
    public GameObject btnEstimate;
    public GameObject btnSave;
    public Toggle toggleCamView;
    public Image toggleCamIcon;
    public GameObject btnBudget;
    public GameObject btnClose;

    [Header("Outside World Button")]
    public GameObject OutsideEstimateButton;

    [Header("Animator")]
    public Animator ExpansionAnimation;

    [HideInInspector]
    private PanelModes currentPanel;
    //private bool isStorePanelOpen;
    //public bool isPanelMenuOpen;
    int HideMainUICount_Minimap = 0;
    int HideMainUICount_Asset = 0;
    int HideMainUICount_Wallposter = 0;
    private bool wasSwapping = false;


    IAssetEntry iAssetEntryObject;

    //IDebugFPSSpeed movementSettingsData;
    #endregion

    private void OnEnable()
    {
        #region New Routes
        Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);
        Messenger.AddListener(StaticFunctionCalls.OnAssetDeselected, OnAssetDeselected);
        Messenger<bool>.AddListener(StaticFunctionCalls.OnAssetDeleted, OnAssetDeleted);
        #endregion

        //Messenger.AddListener(StaticFunctionCalls.MaximiseMenuPanel, MaximisePressed);  //SOS shouldn't be here

        Messenger<PanelModes>.AddListener(StaticFunctionCalls.openHUDPanel, OpenHUDPanel);

        Messenger.AddListener(StaticFunctionCalls.ButtonInfo, ShowCurrentAssetInfo);
        Messenger.AddListener(StaticFunctionCalls.ButtonSwap, SetSwapStore);
        Messenger<IAsset>.AddListener(StaticFunctionCalls.ShowProductInfo, ShowProductInfo);
        //Messenger<string>.AddListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

        //Messenger.AddListener(StaticFunctionCalls.OnAssetDeselected, OnAssetDeselected);

        //Messenger<List<IAsset>>.AddListener(StaticFunctionCalls.GetProductData, ProductSetup);

        //Messenger<List<IPaintRetailer_Deprycated>>.AddListener(StaticFunctionCalls.GetPaintRetailers, PaintRetailerSetup);
        //Messenger<List<IPaintStoreElement_Deprycated>>.AddListener(StaticFunctionCalls.GetPaintData, PaintSetup);
        //Messenger<Dictionary<string,List<IWallfinishGroup>>>.AddListener(StaticFunctionCalls.GetPaintGroups, SetupPaintDictionary);

        //Messenger<List<IWallfinish>>.AddListener(StaticFunctionCalls.GetWallpaperData, WallpaperSetup);

        //Messenger<List<IAssetEntry>>.AddListener(StaticFunctionCalls.GetProductListData, ProductListSetup);

        Messenger<bool>.AddListener(StaticFunctionCalls.ToggleLocalWorld_Deprycated, ToggleLocalWorld_Deprycated);

        Messenger<bool>.AddListener(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, ToggleLocalWallpaperWorld);
        //Settings
        //Messenger<IDebugFPSSpeed>.AddListener(StaticFunctionCalls.DEBUG_FPS_VALUE_SET, ON_DEBUG_FPS_VALUE_SET);
        //Messenger<GraphicSettingPresets>.AddListener(StaticFunctionCalls.GraphicSettings, SetCurrentGraphicsSettings);

        //Messenger<Db>.AddListener(StaticFunctionCalls.OnDatabaseUpdate_Deprycated, OnDatabaseUpdate);

        AppDatabase.OnDBUpdate += OnDatabaseUpdate;

        Messenger<int>.AddListener(StaticFunctionCalls.TransferRenderPositionIndex, CreateRenderPositionBeacon);
        Messenger.AddListener(StaticFunctionCalls.MinimizeMenuPanel, MinimisePanel);
        Messenger<bool>.AddListener(StaticFunctionCalls.HideUICauseMinimap, MinimapToggleMainUI);

        Messenger.AddListener(StaticFunctionCalls.OpenMenuPanel, ActivatePanelMenu);
        Messenger<float>.AddListener(StaticFunctionCalls.Action_CoinUpdated, UpdateCoinValue);
        Messenger<bool>.AddListener(ChallengeUICalls.ShowPurchaseButtons, ShowPurchaseButtons);
        Messenger<string>.AddListener(ChallengeUICalls.UpdateNavbarText, UpdateNavbarText);

        // Messenger.AddListener()

        Homefuly.Events.HomefulyEvents.OnActionFailed += HomefulyEvents_OnActionFailed;
    }





    private void OnDisable()
    {
        #region New Routes
        Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);
        Messenger.RemoveListener(StaticFunctionCalls.OnAssetDeselected, OnAssetDeselected);
        Messenger<bool>.RemoveListener(StaticFunctionCalls.OnAssetDeleted, OnAssetDeleted);
        #endregion
        //Messenger.RemoveListener(StaticFunctionCalls.MaximiseMenuPanel, MaximisePressed);  //SOS shouldn't be here

        Messenger<PanelModes>.RemoveListener(StaticFunctionCalls.openHUDPanel, OpenHUDPanel);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonSwap, SetSwapStore);

        Messenger.RemoveListener(StaticFunctionCalls.ButtonInfo, ShowCurrentAssetInfo);
        Messenger<IAsset>.RemoveListener(StaticFunctionCalls.ShowProductInfo, ShowProductInfo);
        //Messenger<string>.RemoveListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

        //Messenger.RemoveListener(StaticFunctionCalls.OnAssetDeselected, OnAssetDeselected);

        //Messenger<List<IAsset>>.RemoveListener(StaticFunctionCalls.GetProductData, ProductSetup);

        //Messenger<List<IPaintRetailer_Deprycated>>.RemoveListener(StaticFunctionCalls.GetPaintRetailers, PaintRetailerSetup);
        //Messenger<Dictionary<string, List<IWallfinishGroup>>>.RemoveListener(StaticFunctionCalls.GetPaintGroups, SetupPaintDictionary);
        //Messenger<List<IPaintStoreElement_Deprycated>>.RemoveListener(StaticFunctionCalls.GetPaintData, PaintSetup);

        //Messenger<Db>.RemoveListener(StaticFunctionCalls.OnDatabaseUpdate_Deprycated, OnDatabaseUpdate);
        AppDatabase.OnDBUpdate -= OnDatabaseUpdate;

        //Messenger<List<IWallfinish>>.RemoveListener(StaticFunctionCalls.GetWallpaperData, WallpaperSetup);

        //Messenger<List<IAssetEntry>>.RemoveListener(StaticFunctionCalls.GetProductListData, ProductListSetup);

        Messenger<bool>.RemoveListener(StaticFunctionCalls.ToggleLocalWorld_Deprycated, ToggleLocalWorld_Deprycated);

        Messenger<bool>.RemoveListener(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, ToggleLocalWallpaperWorld);

        //settings
        //Messenger<IDebugFPSSpeed>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_VALUE_SET, ON_DEBUG_FPS_VALUE_SET);
        //Messenger<GraphicSettingPresets>.RemoveListener(StaticFunctionCalls.GraphicSettings, SetCurrentGraphicsSettings);

        Messenger<int>.RemoveListener(StaticFunctionCalls.TransferRenderPositionIndex, CreateRenderPositionBeacon);
        Messenger.RemoveListener(StaticFunctionCalls.MinimizeMenuPanel, MinimisePanel);
        Messenger<bool>.RemoveListener(StaticFunctionCalls.HideUICauseMinimap, MinimapToggleMainUI);
        Messenger.RemoveListener(StaticFunctionCalls.OpenMenuPanel, ActivatePanelMenu);
        Messenger<float>.RemoveListener(StaticFunctionCalls.Action_CoinUpdated, UpdateCoinValue);
        Messenger<bool>.RemoveListener(ChallengeUICalls.ShowPurchaseButtons, ShowPurchaseButtons);


        Messenger<string>.RemoveListener(ChallengeUICalls.UpdateNavbarText, UpdateNavbarText);

        Homefuly.Events.HomefulyEvents.OnActionFailed -= HomefulyEvents_OnActionFailed;
    }


    void HomefulyEvents_OnActionFailed(ActionType actionType, int errCode)
    {
        ErrorTracking.ErrorCodes eCode = (ErrorTracking.ErrorCodes)errCode;
        if (actionType == ActionType.Transaction)
        {
            switch (eCode)
            {
                case ErrorTracking.ErrorCodes.User_Denial:
                    break;
                case ErrorTracking.ErrorCodes.Invalid_SceneType:
                    break;
                case ErrorTracking.ErrorCodes.Invalid_ID:
                    break;
                case ErrorTracking.ErrorCodes.NotEnough_Coins:
                    Instantiate(popupGotoChallenge, gameObject.transform);
                    break;
            }
        }

    }

    private void MaximisePressed()
    {
        Debug.LogWarning("[Tutorials] Workaround to stop buttons from breaking because tutorialManager is inactive. Please fix");
    }

    #region New Routes
    private void OnAssetDeselected()
    {
        wasSwapping = false;
    }
    private void OnAssetSelected(IAssetEntry obj)
    {
        iAssetEntryObject = obj;
        if (wasSwapping)
            OpenHUDPanel(PanelModes.Swap);
        //SetSwapStore();
    }
    private void OnAssetDeleted(bool wasSwapping)
    {
        this.wasSwapping = wasSwapping;
    }
    #endregion

    #region Cam View
    public void CamViewButton()
    {
        Debug.Log("Cam view button reached");
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleCamView, toggleCamView.isOn);
        Messenger.Broadcast(ChallengeUICalls.ViewButtonClicked);
        if (HomefulyEvents.OnViewButtonPressed != null)
            HomefulyEvents.OnViewButtonPressed();
        toggleCamView.gameObject.GetComponent<Image>().color = toggleCamView.isOn ? new Color32(255, 255, 255, 255) : new Color32(76, 63, 124, 255);// new Color32(255, 255, 255, 255);   //switch to our color
        toggleCamView.gameObject.GetComponent<RectTransform>().rotation = toggleCamView.isOn ? Quaternion.Euler(new Vector3(0, 0, -90)) : Quaternion.Euler(0, 0, 0);
        toggleCamIcon.color = !toggleCamView.isOn ? new Color32(255, 255, 255, 255) : new Color32(76, 63, 124, 255);// new Color32(255, 255, 255, 255);   //switch to our color
    }
    #endregion

    #region activatePanel and animations

    private void Start()        //listener to go back for navbar back button
    {
        app = AppComponent.Instance;
        buttonBack.onClick.AddListener(() =>
        {
            OpenHUDPanel(PanelModes.Global);
        });

        if (app.account.CRED_ISDEMO != string.Empty) btnSave.SetActive(false);
        btnBudget.transform.parent.gameObject.SetActive(!AppConfig.GameConfig.creativeMode);

    }

    public void UIApprovesThePurchase()
    {
        Messenger<IAssetEntry>.Broadcast(ChallengeUICalls.UIApprovesPurchase, iAssetEntryObject);
        ShowPurchaseButtons(false);
    }

    public void UIRejectsThePurchase()
    {
        Messenger.Broadcast(ChallengeUICalls.UIRejectsPurchase);
        ShowPurchaseButtons(false);
    }

    public void ShowPurchaseButtons(bool b)
    {
        pnlPurchases.SetActive(b);
    }

    void UpdateCoinValue(float c)
    {
        btnBudget.gameObject.GetComponent<TextMeshProUGUI>().text = ((int)c).ToString();
    }

    void CheckSceneType()
    {
        if (app.db.sceneType == SceneType.ReadOnlyDesign)
            HideWorldButtons(true);

    }

    public void ActivatePanelMenu()
    {
        //worldButtonContainer.SetActive(false);
        //btnHelp.SetActive(false);
        //btnEstimate.SetActive(false);
        panelMenu.SetActive(true);
        //toggleCamView.gameObject.SetActive(false);
        //StartCoroutine("PlayMaximiseAnim");
        PlayMaximiseAnim();
        //maximiseButton.gameObject.SetActive(false);
        //Messenger.Broadcast(StaticFunctionCalls.MaximiseMenuPanel);
    }

    public void MinimisePanel()
    {
        if (HomefulyEvents.OnMinimise != null)
            HomefulyEvents.OnMinimise();
        if (!isReadOnly)
        {
            worldButtonContainer.SetActive(true);
            toggleCamView.gameObject.SetActive(true);
        }
        //btnHelp.SetActive(true);
        //btnEstimate.SetActive(true);
        //maximiseButton.gameObject.SetActive(true);
        StartCoroutine("PlayMinimiseAnim");
    }

    IEnumerator PlayMinimiseAnim()
    {
        if (panelMenu.activeSelf)
        {
            Animator m_Animator = panelMenu.GetComponent<Animator>();
            m_Animator.Play("Minimise");
            yield return new WaitForSeconds(0.3f);
            panelMenu.SetActive(false);
        }
    }

    void PlayMaximiseAnim()
    {
        Animator m_Animator = panelMenu.GetComponent<Animator>();
        m_Animator.Play("Maximise");
        //yield return new WaitForSeconds(0.3f);
        //maximiseButton.gameObject.SetActive(false);
    }
    #endregion
    public void MinimiseInfoPanel()
    {
        panelInfoMenu.SetActive(false);
    }
    #region localPanel Toggling
    void ToggleLocalWorld_Deprycated(bool state)
    {
        if (state)
        {
            Debug.LogWarning("[Deprycation Warning] ToggleLocalWorld_Deprycated, Use OnAssetSelected instead");
            HideMainUICount_Asset = 1;
            panelLocalWorldParent.SetActive(true);
            panelLocalWorldParent.GetComponent<SceneWindowManager>().LoadPanel(SceneWindowManager.Panels.LocalWorld, app.db.sceneType == SceneType.UserRoom ? false : true);
            ToggleMainUI(state, app.db.sceneType == SceneType.UserRoom ? false : true);
        }
        else
        {
            Debug.LogWarning("[Deprycation Warning] ToggleLocalWorld_Deprycated, Use OnAssetDeselected instead");
            HideMainUICount_Asset = 0;
            panelLocalWorldParent.SetActive(false);
            ToggleMainUI(state);
        }
    }
    void ToggleLocalWallpaperWorld(bool state)
    {
        if (state)
        {
            HideMainUICount_Wallposter = 1;
            panelLocalWorldParent.SetActive(true);
            panelLocalWorldParent.GetComponent<SceneWindowManager>().LoadPanel(SceneWindowManager.Panels.LocalWallpaperWorld, false);
        }
        else
        {
            HideMainUICount_Wallposter = 0;
            panelLocalWorldParent.SetActive(false);
        }
        ToggleMainUI(state);
    }

    private void MinimapToggleMainUI(bool state)
    {
        if (isReadOnly)
            return;
        if (state)
        {
            HideMainUICount_Minimap = 1;
        }
        else
        {
            HideMainUICount_Minimap = 0;
        }

        ToggleMainUI(state);
    }
    //More control on UI buttons
    void ToggleMainUI(bool state, bool enableMainUI = false)
    {

        int Count = HideMainUICount_Asset + HideMainUICount_Minimap + HideMainUICount_Wallposter;

        if (Count > 0)
        {
            MinimisePanel();
            panelInfoMenu.SetActive(false);
            worldButtonContainer.SetActive(false);
        }
        else
        {
            panelInfoMenu.SetActive(false);
            worldButtonContainer.SetActive(true);
        }

        if (enableMainUI)
            worldButtonContainer.SetActive(true);
    }

    void HideWorldButtons(bool b) // Hide the World buttons except estimate cause the scene is read only
    {
        //worldButtonContainer.SetActive(!b);
        OutsideEstimateButton.SetActive(b);
        foreach (Transform t in btnClose.transform.parent.transform.parent)
        {
            t.gameObject.SetActive(false);
        }

        foreach (Transform t in btnClose.transform.parent)
        {
            t.gameObject.SetActive(false);
        }
        btnClose.transform.parent.gameObject.SetActive(b);
        btnClose.SetActive(b);


    }


    public void BtnSave()
    {
        Debug.Log("Save Clicked...");
        Messenger<string>.Broadcast(StaticFunctionCalls.OnSaveDesign, "On Screen Button");
        //Messenger.Broadcast(ChallengeUICalls.SaveButtonClicked);
    }

    public void BtnEstimate()
    {
        ActivatePanelMenu();
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.ProductList);
    }
    #region tutorial
    public void ShowTutorial()
    {
        //WidgetManager.Instance.StartTutorial();
    }
    #endregion
    #endregion

    #region PanelHUD manager

    public void OpenHUDPanel(PanelModes panel)
    {
        currentPanel = panel;
        HideAllPanels();
        switch (currentPanel)
        {
            case PanelModes.Loading:
                panelLoading.gameObject.SetActive(true);
                panelLoading.pnlLoadingSlider.gameObject.SetActive(false);
                break;
            case PanelModes.Global:
                buttonBack.gameObject.SetActive(false);
                //panelGlobalActions.gameObject.SetActive(true);
                Debug.Log("Overriding global panle open, and redicrecting it to minimse panel");
                MinimisePanel();
                break;
            //case PanelModes.Account:
            //pnlAccount.SetActive(true);
            //Debug.LogWarning("Account Panel Not Setup Yet");
            //break;
            case PanelModes.AssetStore:
                panelAssetStore.gameObject.SetActive(true);
                //isStorePanelOpen = true;
                break;
            case PanelModes.Local:
                panelLocal.gameObject.SetActive(true);
                panelInfoMenu.SetActive(true);
                break;
            case PanelModes.Swap:
                panelSwap.gameObject.SetActive(true);
                panelInfoMenu.SetActive(true);

                break;
            case PanelModes.Paint:
                panelPaint.gameObject.SetActive(true);
                break;
            case PanelModes.Wallpapers:
                panelWallpapers.gameObject.SetActive(true);
                break;
            case PanelModes.Settings:
                panelSettings.gameObject.SetActive(true);
                break;
            case PanelModes.Viewpoints:
                panelViewpoints.gameObject.SetActive(true);
                break;
            case PanelModes.ProductList:
                panelEstimate.gameObject.SetActive(true);
                break;
        }
        NavbarTextUpdate(currentPanel);
    }

    void NavbarTextUpdate(PanelModes panel)
    {
        switch (panel)
        {
            case PanelModes.Loading:
                title.text = "Loading";
                break;
            case PanelModes.Global:
                title.text = "Make Changes";
                break;
            //case PanelModes.Account:
            //break;
            case PanelModes.AssetStore:
                title.text = "Add Products";
                break;
            case PanelModes.Local:
                infoMenuTitle.text = "Product Info";
                break;
            case PanelModes.Swap:
                infoMenuTitle.text = "Swap";
                break;
            case PanelModes.Paint:
                title.text = "Add Paint";
                break;
            case PanelModes.Wallpapers:
                title.text = "Add Wallpaper";
                break;
            case PanelModes.Settings:
                title.text = "Settings";
                break;
            case PanelModes.Viewpoints:
                title.text = "Views";
                break;
            case PanelModes.ProductList:
                title.text = "Estimate";
                break;
        }
    }

    public void HideAllPanels()
    {
        //if (currentPanel == PanelModes.Local | currentPanel == PanelModes.Swap)  panelGlobalActions.gameObject.SetActive(false);
        panelLocal.gameObject.SetActive(false);
        panelSwap.gameObject.SetActive(false);
        panelPaint.gameObject.SetActive(false);
        panelLoading.gameObject.SetActive(false);
        panelAssetStore.gameObject.SetActive(false);
        panelWallpapers.gameObject.SetActive(false);
        //panelLocalWorldParent.SetActive(false);
        panelSettings.gameObject.SetActive(false);
        panelEstimate.gameObject.SetActive(false);
        panelViewpoints.gameObject.SetActive(false);
        buttonBack.gameObject.SetActive(true);
        panelInfoMenu.SetActive(false);
    }

    /*public void GoBack()
    {
        OpenHUDPanel(PanelModes.Global);
    }*/

    #endregion

    #region other Panel setups

    private void OnDatabaseUpdate(Db obj)
    {
        //temp
        db = AppComponent.Instance.db;

        //Debug.Log("Fukallery: " + obj.ToString());
        //if (!init) Init();
        if (obj == Db.Whole)
        {
            InitPaintStore();
            InitAssetStore();
            panelWallpapers.Init();//SOS will work on this when wallpaper store is ready lol no you wont.
            if (app.db.sceneType == Homefuly.SceneType.UserRoom)
            {
                btnBudget.transform.parent.gameObject.SetActive(!AppConfig.GameConfig.creativeMode);
                btnBudget.GetComponent<TextMeshProUGUI>().text = ((int)db.userSavedData.currentCoins).ToString();

            }

        }
        else if (obj == Db.Objects)
        {
            InitEstimate();
        }


        if (db.sceneType == SceneType.ReadOnlyDesign)
        {
            HideWorldButtons(true);
            isReadOnly = true;
        }
        //Debug.Log("CRED_ROLE: " + AppComponent.Instance.account.CRED_ROLE + " Demo hey kya: " + AppComponent.Instance.account.CRED_ISDEMO);
    }



    #region product info
    private void ShowCurrentAssetInfo()
    {
        if (app.db.currentObject is IAssetEntry)
            Messenger<IAsset>.Broadcast(StaticFunctionCalls.ShowProductInfo, (app.db.currentObject as IAssetEntry).asset);
        else Debug.LogError("Current Asset Cast to IAssetEntry Failed");
    }

    private void ShowProductInfo(IAsset thisAsset)
    {
        bool result = string.IsNullOrEmpty(thisAsset.designedBy.productUrl) | thisAsset.designedBy.productUrl.ToLower().Contains("homefuly") ? false : true;

        if (result)
        {
            HomefulyExtensions.OpenProductStorePage(thisAsset, Homefuly.Analytics.ShopButtonType.asset_options);

            //url = thisAsset.designedBy.productUrl;

            //_.l("OPenning : " + url);
            //System.Diagnostics.Process.Start(url);
            //await Task.Delay(100);
            //#if !UNITY_EDITOR
            //            //HomefulyExtensions.SetScreenOrientationToLandscape(false);
            //#endif

            //            url = System.Uri.EscapeUriString(url);
            //            InAppBrowser.OpenURL(url);
            //            Messenger<IAsset, AppPage, string>.Broadcast(StaticFunctionCalls.ShopButtonClicked, thisAsset, AppPage.Designer, "Asset_Buttons");
        }
        else
        {
            //url = "https://www.google.co.in/search?q=" + thisAsset.name;
            OpenHUDPanel(PanelModes.Local);
            panelLocal.Setup(thisAsset);
        }


    }
    #endregion

    #region swap
    private void SetSwapStore()
    {
        OpenHUDPanel(PanelModes.Swap);
        if (app.db.currentObject is IAssetEntry)
            panelSwap.Setup((app.db.currentObject as IAssetEntry).asset);
        else Debug.LogError("Current Asset Cast to IAssetEntry Failed");
    }
    #endregion

    #region Paint Store
    void InitPaintStore()
    {
        var data = db.GetRetailersOfType(RetailerType.paint);
        panelPaint.Init(data);
    }
    #endregion

    #region assetStore
    void InitAssetStore()
    {
        var data = db;//.GetCategoriesOfType(CategoryType.Subcategory);
        panelAssetStore.Init(db);
    }
    #endregion

    #region estimate
    void InitEstimate()
    {
        panelEstimate.Init(db.sceneAssets);
    }
    #endregion

    #region settings
    /*void ON_DEBUG_FPS_VALUE_SET(IDebugFPSSpeed value)
    {
        //movementSettingsData = value;
        panelSettings.ON_DEBUG_FPS_VALUE_SET(value);// TransmitMovementSettingsData();
    }*/
    /*
    void SetCurrentGraphicsSettings(GraphicSettingPresets graphicPreset)
    {
        panelSettings.SetHighlighted(graphicPreset);
    }*/
    #endregion

    #region viewpoints stuff

    void CreateRenderPositionBeacon(int obj)
    {
        panelViewpoints.CreateRenderPositionBeacon(obj);
    }

    #endregion

    #endregion



    #region New Event Calls By Prajwal (Please organise and rename this region Accordingly)
    /// <summary>
    /// When the Add product button is Clicked
    /// </summary>
    public void OnClick_AddProductButton()
    {
        if (HomefulyEvents.OnAddProductPressed != null)
            HomefulyEvents.OnAddProductPressed();
        if (!panelMenu.activeSelf)
            ActivatePanelMenu();

        OpenHUDPanel(PanelModes.AssetStore);
    }

    /// <summary>
    /// When the Add wallfinish button is Clicked
    /// </summary>
    public void OnClick_AddWallfinishButton()
    {
        if (!panelMenu.activeSelf)
            ActivatePanelMenu();

        OpenHUDPanel(PanelModes.Paint);
    }


    bool optionOpen;
    /// <summary>
    /// When the Add wallfinish button is Clicked
    /// </summary>
    public void OnClick_ExpandButton()
    {
        Messenger.Broadcast(ChallengeUICalls.ExpandButtonClicked);
        if (HomefulyEvents.OnExpandButtonPressed != null)
            HomefulyEvents.OnExpandButtonPressed();

        //additionalOptions.SetActive(!additionalOptions.activeSelf);
        if (optionOpen)
        {

            ExpansionAnimation.Play("OptionsExpansionAnimation");
            StartCoroutine(waiter1(false));
            optionOpen = false;
        }
        else
        {
            StartCoroutine(waiter(true));
            ExpansionAnimation.Play("OptionsExpansionAnimationReverse");
            optionOpen = true;
        }
    }


    private void UpdateNavbarText(string obj)
    {
        title.text = obj;
    }

    #endregion




    IEnumerator waiter(bool b)
    {
        additionalOptions.SetActive(b);
        yield return new WaitForSeconds(0.5f);

    }

    IEnumerator waiter1(bool b)
    {

        yield return new WaitForSeconds(0.5f);
        additionalOptions.SetActive(b);

    }
}

public enum PanelModes
{
    Loading,
    Global,
    Local,
    Swap,
    Paint,
    Wallpapers,
    //Account,
    AssetStore,
    Settings,
    Viewpoints,
    ProductList
}

public static class ExtendedUtilities
{
    public static Color HexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }
}