﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using System;

public class ReplaceFonts : MonoBehaviour
{

    private TextMeshProUGUI[] GetText;
    public TMP_FontAsset myFont;

    // Use this for initialization
    void Start()
    {
        Init();
       

    }

    private void Init()
    {
       
        GetText = Resources.FindObjectsOfTypeAll<TextMeshProUGUI>();

        foreach (TextMeshProUGUI go in GetText)
        {

            go.font = myFont;
        }
        
    }
}