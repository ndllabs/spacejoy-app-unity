﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using _unity.Events;
using System;


using Homefuly;

public class DebugTextScript : MonoBehaviour
{
    private void OnEnable()
    {
        Messenger<bool>.AddListener(StaticFunctionCalls.AssetDragFromStoreEvents, OnDragAsset);
        //Messenger<string>.AddListener(StaticFunctionCalls.OnDragWallposter, OnDragWallposter);

        Messenger<string>.AddListener(StaticFunctionCalls.OnUpdateFooterText, OnFooterUpdate);

        Messenger.AddListener(StaticFunctionCalls.OnSaveDesign, OnSaveDesign);

        Messenger.AddListener(StaticFunctionCalls.DeleteAsset, OnDeleteAssetFromScene);

        Messenger.AddListener(StaticFunctionCalls.OnHideAsset, OnHideAsset);

        Messenger<bool>.AddListener(StaticFunctionCalls.OnShowAssetRelatedDistances, OnShowAssetRelatedDistances);

        Messenger<float>.AddListener(StaticFunctionCalls.OnSetAssetYRotation, OnSetAssetYRotation);
        //LOCALWORLD
        Messenger.AddListener(StaticFunctionCalls.ButtonRotate, ButtonRotate);
        Messenger.AddListener(StaticFunctionCalls.ButtonHorizontal, ButtonHorizontal);
        Messenger.AddListener(StaticFunctionCalls.ButtonVertical, ButtonVertical);
        Messenger.AddListener(StaticFunctionCalls.ButtonDelete_Deprycated, ButtonDelete);
        Messenger.AddListener(StaticFunctionCalls.ButtonDuplicate, ButtonDuplicate);
        Messenger.AddListener(StaticFunctionCalls.ButtonDistance, ButtonDistance);
        Messenger.AddListener(StaticFunctionCalls.ButtonDimension, ButtonDimension);
        Messenger<bool>.AddListener(StaticFunctionCalls.ToggleLocalWorld_Deprycated, ToggleLocalWorld);

        Messenger.AddListener(StaticFunctionCalls.WallpaperPosition_Deprycated, WallpaperPosition);
        Messenger.AddListener(StaticFunctionCalls.WallpaperResize_Deprycated, WallpaperResize);
        Messenger<bool>.AddListener(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, ToggleLocalWallpaperWorld);


        //Messenger<Db>.AddListener(StaticFunctionCalls.OnDatabaseUpdate_Deprycated, OnDatabaseUpdate);
        //AppDatabase.OnDBUpdate -= OnDatabaseUpdate;

        //draganddroppaint
        //Messenger<GameObject, string, Texture2D>.Broadcast("OnApplyTextureToWall", g, id, (Texture2D)transform.Find("ptex").GetComponent<RawImage>().texture);
        //Messenger<GameObject, string, string>.Broadcast("OnApplyPaintToWall", g, id, "#" + ColorUtility.ToHtmlStringRGB(transform.Find("Image").GetComponent<Image>().color));
        //draganddropassetv2
        //Messenger<string, Vector3, GameObject>.Broadcast("OnAssetDroppedInsideRoom", id, hitCenter.point, hitCenter.collider.gameObject);

        Messenger<string>.AddListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

        Messenger.AddListener(StaticFunctionCalls.OnAssetDeselected, OnAssetDeselected);

        Messenger<GameObject>.AddListener(StaticFunctionCalls.onGameObjectTapped_Deprycated, onGameObjectTapped);

        Messenger.AddListener(StaticFunctionCalls.MaximisePanel, MaximisePanel);
        //Messenger<GraphicSettingPresets>.AddListener(StaticFunctionCalls.GraphicSettings, GraphicSettings);

        //#region dummy 
        //Messenger.AddListener(StaticFunctionCalls.MaximiseMenuPanel, dummyForMaximiseCall);
        //#endregion

        //Messenger<string>.AddListener(StaticFunctionCalls.OnDragWallposter, WallpaperButtonClicked);

        //Messenger<float>.AddListener(StaticFunctionCalls.DEBUG_FPS_movementSpeed, (float a) => { Debug.Log("New movement speed is " + a); });
        //Messenger<float>.AddListener(StaticFunctionCalls.DEBUG_FPS_rotationSpeed, (float a) => { Debug.Log("New rotation speed is " + a); });
        //Messenger<float>.AddListener(StaticFunctionCalls.DEBUG_FPS_thresholdX, (float a) => { Debug.Log("New threshold X is " + a); });
        //Messenger<float>.AddListener(StaticFunctionCalls.DEBUG_FPS_thresholdY, (float a) => { Debug.Log("New threshold Y is " + a); });
    }

    private void OnDatabaseUpdate(Db obj)
    {
        //throw new NotImplementedException();
    }

    private void OnDisable()
    {

        Messenger<bool>.RemoveListener(StaticFunctionCalls.AssetDragFromStoreEvents, OnDragAsset);
        //Messenger<string>.RemoveListener(StaticFunctionCalls.OnDragWallposter, OnDragWallposter);

        //Messenger<float>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_movementSpeed, (float a) => { Debug.Log("New movement speed is " + a); });
        //Messenger<float>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_rotationSpeed, (float a) => { Debug.Log("New rotation speed is " + a); });
        //Messenger<float>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_thresholdX, (float a) => { Debug.Log("New threshold X is " + a); });
        //Messenger<float>.RemoveListener(StaticFunctionCalls.DEBUG_FPS_thresholdY, (float a) => { Debug.Log("New threshold Y is " + a); });


        Messenger<string>.RemoveListener(StaticFunctionCalls.OnUpdateFooterText, OnFooterUpdate);

        Messenger.RemoveListener(StaticFunctionCalls.OnSaveDesign, OnSaveDesign);

        Messenger.RemoveListener(StaticFunctionCalls.DeleteAsset, OnDeleteAssetFromScene);

        Messenger.RemoveListener(StaticFunctionCalls.OnHideAsset, OnHideAsset);

        Messenger<bool>.RemoveListener(StaticFunctionCalls.OnShowAssetRelatedDistances, OnShowAssetRelatedDistances);

        Messenger<float>.RemoveListener(StaticFunctionCalls.OnSetAssetYRotation, OnSetAssetYRotation);
        //LOCALWORLD
        Messenger.RemoveListener(StaticFunctionCalls.ButtonRotate, ButtonRotate);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonHorizontal, ButtonHorizontal);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonVertical, ButtonVertical);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonDelete_Deprycated, ButtonDelete);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonDuplicate, ButtonDuplicate);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonDistance, ButtonDistance);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonDimension, ButtonDimension);
        Messenger<bool>.RemoveListener(StaticFunctionCalls.ToggleLocalWorld_Deprycated, ToggleLocalWorld);

        //Messenger<Db>.RemoveListener(StaticFunctionCalls.OnDatabaseUpdate_Deprycated, OnDatabaseUpdate);
        //AppDatabase.OnDBUpdate -= OnDatabaseUpdate;

        Messenger.RemoveListener(StaticFunctionCalls.WallpaperPosition_Deprycated, WallpaperPosition);
        Messenger.RemoveListener(StaticFunctionCalls.WallpaperResize_Deprycated, WallpaperResize);
        Messenger<bool>.RemoveListener(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, ToggleLocalWallpaperWorld);
        //draganddroppaint
        //Messenger<GameObject, string, Texture2D>.Broadcast("OnApplyTextureToWall", g, id, (Texture2D)transform.Find("ptex").GetComponent<RawImage>().texture);
        //Messenger<GameObject, string, string>.Broadcast("OnApplyPaintToWall", g, id, "#" + ColorUtility.ToHtmlStringRGB(transform.Find("Image").GetComponent<Image>().color));
        //draganddropassetv2
        //Messenger<string, Vector3, GameObject>.Broadcast("OnAssetDroppedInsideRoom", id, hitCenter.point, hitCenter.collider.gameObject);

        Messenger<string>.RemoveListener(StaticFunctionCalls.OnAssetSelected, OnAssetSelected);

        Messenger.RemoveListener(StaticFunctionCalls.OnAssetDeselected, OnAssetDeselected);

        Messenger<GameObject>.RemoveListener(StaticFunctionCalls.onGameObjectTapped_Deprycated, onGameObjectTapped);

        Messenger.RemoveListener(StaticFunctionCalls.MaximisePanel, MaximisePanel);

        //Messenger<GraphicSettingPresets>.RemoveListener(StaticFunctionCalls.GraphicSettings, GraphicSettings);

        //#region dummy 
        //Messenger.RemoveListener(StaticFunctionCalls.MaximiseMenuPanel, dummyForMaximiseCall);
        //#endregion

        //Messenger<string>.RemoveListener(StaticFunctionCalls.OnDragWallposter, WallpaperButtonClicked);

    }

    private void dummyForMaximiseCall()
    {
        throw new NotImplementedException();
    }

    //private void OnDragWallposter(string obj)
    //{
    //    UpdateFakeText(StaticFunctionCalls.OnDragWallposter);
    //}

    private void OnDragAsset(bool obj)
    {
        UpdateFakeText(StaticFunctionCalls.AssetDragFromStoreEvents);
    }

    private void WallpaperPosition()
    {
        UpdateFakeText(StaticFunctionCalls.WallpaperPosition_Deprycated);
    }
    private void WallpaperResize()
    {
        UpdateFakeText(StaticFunctionCalls.WallpaperResize_Deprycated);
    }
    

    void ButtonRotate()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonRotate);
    }
    void ButtonHorizontal()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonHorizontal);
    }
    void ButtonVertical()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonVertical);
    }
    void ButtonDelete()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonDelete_Deprycated);
    }
    void ButtonDuplicate()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonDuplicate);
    }
    void ButtonDistance()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonDistance);
    }
    void ButtonDimension()
    {
        UpdateFakeText(StaticFunctionCalls.ButtonDimension);
    }

    void OnFooterUpdate(string txt)
    {
        UpdateFakeText("OnFooterUpdate");
    }

    void OnSaveDesign()
    {
        UpdateFakeText(StaticFunctionCalls.OnSaveDesign);
    }

    void OnDeleteAssetFromScene()
    {
        UpdateFakeText(StaticFunctionCalls.DeleteAsset);
    }

    void OnHideAsset()
    {
        UpdateFakeText(StaticFunctionCalls.OnHideAsset);
    }

    void OnShowAssetRelatedDistances(bool dummy)
    {
        UpdateFakeText(StaticFunctionCalls.OnShowAssetRelatedDistances);
    }

    void OnSetAssetYRotation(float dummy)
    {
        UpdateFakeText(StaticFunctionCalls.OnSetAssetYRotation);
    }


    void OnAssetSelected(string txt)
    {
        UpdateFakeText("OnAssetSelected");
    }

    void OnAssetDeselected()
    {
        UpdateFakeText("OnAssetDeselected");
    }

    void onGameObjectTapped(GameObject obj)
    {
        UpdateFakeText("onGameObjectTapped");
    }
    void ToggleLocalWorld(bool x)
    {
        UpdateFakeText(StaticFunctionCalls.ToggleLocalWorld_Deprycated);
    }
    void ToggleLocalWallpaperWorld(bool x)
    {
        UpdateFakeText(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated);
    }
    void MaximisePanel()
    {
        UpdateFakeText(StaticFunctionCalls.MaximisePanel);
    }
    //void GraphicSettings(GraphicSettingPresets preset)
    //{
    //    UpdateFakeText("Gfx preset : "+preset);
    //}
    void UpdateFakeText(string txt)
    {
        gameObject.GetComponent<TextMeshProUGUI>().text += "\n" + txt;
    }
}
