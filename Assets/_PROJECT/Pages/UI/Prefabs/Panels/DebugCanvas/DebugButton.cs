﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class DebugButton : MonoBehaviour
{
    public bool activate;
    public bool activateWall;
    //public GameObject panelRef;
    private void Start()
    {
        activate = true;
        activateWall = true;
    }

    void ToggleMode(int i)
    {
        switch(i)
        {
            case 1:
                activate = true;
                activateWall = true;
                break;
            case 2:
                activate = false;
                activateWall = true;
                break;
            case 3:
                activate = true;
                activateWall = false;
                break;
        }
    }
    public void ClickFunction()
    {
        Debug.Log("Activate before broadcast: " + activate);
        Debug.Log("ActivateWall before broadcast: " + activateWall);
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, activate);
        if (activate)
        {
            //panelRef.SetActive(true);
            ToggleMode(2);
        }
        else
        {
            //panelRef.SetActive(false);
            ToggleMode(1);
        }
        Debug.Log("Activate: " + activate);
        Debug.Log("ActivateWall: " + activateWall);
    }

    public void ClickWallFunction()
    {
        Debug.Log("Activate before broadcast: " + activate);
        Debug.Log("ActivateWall before broadcast: " + activateWall);
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, activateWall);
        if (activateWall)
        {
            //panelRef.SetActive(true);
            ToggleMode(3);
        }
        else
        {
            //panelRef.SetActive(false);
            ToggleMode(1);
        }
        Debug.Log("Activate: " + activate);
        Debug.Log("ActivateWall: " + activateWall);
    }

}
