﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController_PanelLoading : MonoBehaviour
{
    int colorNumber = 0;
    List<Color32> color = new List<Color32>
    {
        new Color32(117, 169, 193, 255),
        new Color32(119, 98, 123, 255)
        //new Color32(240, 90, 70, 255)
    };
    public Slider pnlLoadingSlider;
    public TextMeshProUGUI quoteText;
    public TextMeshProUGUI titleText;

    List<string> quotes = new List<string>();

    string quoteTookTime = "\"Quality is more important even if it takes more time\"";
    string quoteTookLotOfTime = "\"Be patient, good things take time\"";
    string quoteTookTooMuchTime = "\"We need some love and little more time\"";

    int currentTime = 0;
    int quoteUpdateTime = 5;


    //bool doyourthingie = false;

    void Start()
    {
        //Feedback Polling
        quotes.Add("\"A house is made of bricks and beams. A home is made of hopes and dreams\"");
        quotes.Add("\"Home is not a place…it’s a feeling\"");
        quotes.Add("\"Home is where love resides, memories are created, friends always belong, and laughter never ends\"");
        quotes.Add("\"Home is where our story begins…\"");
        quotes.Add("\"Home is where one starts from.\" –T.S. Eliot\"");
        quotes.Add("\"Life takes you unexpected places, love brings you home\"");
        quotes.Add("\"home is where the heart is\"");
        quotes.Add("\"Where we love is home - home that our feet may leave, but not our hearts\" - Oliver Wendell Holmes, Sr.");
        quotes.Add("\"Architecture is not an inspirational business. It's a rational procedure to do sensible and hopefully beautiful things.\"- Harry Seidler");
        quotes.Add("\"Coming home is one of the most beautiful things\" -Andre Rieu");
        quotes.Add("\"Have nothing in your house that you do not know to be useful or believe to be beautiful\" -William Morris");
        quotes.Add("\"Blessed are they who see beautiful things in humble places\" - Camille Pissarro");
        quotes.Add("\"Every detail counts: A functional object can become a beautiful moment\" - Sarah Bartholomew");
        quotes.Add("\"Every human is an artist. The dream of your life is to make beautiful art\" - Don Miguel Ruiz");
        quotes.Add("\"A good home must be made, not bought\" - Joyce Maynard");
        quotes.Add("\"If you go anywhere, even paradise, you will miss your home\" - Malala");
        quotes.Add("\"Home won't built in a day\" - Jane Sherwood Ace");
        quotes.Add("\"Some people look for a beautiful place, others make a place beautiful\" - Hazrat Inayat Khan");
        quotes.Add("\"Whenever you are creating beauty around you, you are restoring your own soul\" - Alice Walker");
        quotes.Add("\"Home is an invention on which no one has yet improved\" - Ann Douglas");
    }

    void OnUpdateQuote()
    {
        string quote = string.Empty;

        if (currentTime == quoteUpdateTime * 3)
            quote = quoteTookTime;
        else if (currentTime == quoteUpdateTime * 6)
            quote = quoteTookLotOfTime;
        else if (currentTime == quoteUpdateTime * 9)
            quote = quoteTookTooMuchTime;
        else
        {
            //Debug.Log("Setting Random Quote");
            int randInt = UnityEngine.Random.Range(0, quotes.Count - 1);
            quote = quotes[randInt];
        }

        quoteText.text = quote;
        //Debug.LogError(color[colorNumber].ToString());
        quoteText.color = color[colorNumber];
        colorNumber = (colorNumber + 1) % 2;
        currentTime += quoteUpdateTime;
    }

    private void OnEnable()
    {
        currentTime = 0;
        InvokeRepeating("OnUpdateQuote", 0f, quoteUpdateTime);
        //doyourthingie = true;
    }

    private void OnDisable()
    {
        CancelInvoke("OnUpdateQuote");
        //doyourthingie = false;
    }
}
