﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class SceneManagerTestUI : MonoBehaviour
{
    bool flagLocalOpen;
    //WidgetManager wid;
    public static List<IAsset> result = new List<IAsset>();

    public static List<IWallfinish> dummyResultwall = new List<IWallfinish>();
    public static List<IDebugPaints> dummyResult = new List<IDebugPaints>();
    // Start is called before the first frame update
    void Start()
    {
        //DUMMY DATA
        result.Add(new IAsset
        {
            _id = "test",
            name = "test",
            mountType = "test",
            description = "desc test",
            roomType = "test",
            glossy = false
        });
        //MORE DUMMY DATA
        dummyResult.Add(new IDebugPaints
        {
            _id = "testid",
            name = "Dulux",
            //code = "#FFFF12"
        });
        dummyResult.Add(new IDebugPaints
        {
            _id = "testid2",
            name = "Benjamin Moore",
            //code = "#FFFF12"
        });
        dummyResultwall.Add(new IWallfinish
        {
            _id = "testid",
            name = "khbkhb",
        });

    }


    //private void Init()
    //{
    //StartCoroutine(wait(2, () => {
    //Messenger<List<IPaintStoreElement>>.Broadcast(StaticFunctionCalls.GetWallpaperData, dummyResult);
    //}));
    //}

    public void OnClickSetupUI()
    {
        Messenger<List<IWallfinish>>.Broadcast(StaticFunctionCalls.GetWallpaperData, dummyResultwall);
        Messenger<List<IAsset>>.Broadcast(StaticFunctionCalls.GetProductData, result);
        Messenger<List<IDebugPaints>>.Broadcast(StaticFunctionCalls.GetPaintData, dummyResult);

        //Messenger<GraphicSettingPresets>.Broadcast(StaticFunctionCalls.GraphicSettings, GraphicSettingPresets.high);
        //StartCoroutine(wait(3,()=> {
        //wid.LoaderClose();
        //}));
    }

    public void OnClickASsetSelected()
    {
        flagLocalOpen = !flagLocalOpen;

        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, flagLocalOpen);
    }


    IEnumerator wait(float sec, System.Action Callback)
    {
        yield return new WaitForSeconds(sec);
        Callback();
    }
}
