﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class SpawnCues : MonoBehaviour
{
    GameObject spawnanim_clone;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable() => Messenger<GameObject>.AddListener(StaticFunctionCalls.AssetDragFromStoreEvents, DragAndDropped);

    private void OnDisable() => Messenger<GameObject>.RemoveListener(StaticFunctionCalls.AssetDragFromStoreEvents, DragAndDropped);

    private void DragAndDropped(GameObject over)
    {
        
        spawnanim_clone = Instantiate(Resources.Load<GameObject>("SpawnAnim"), over.transform.position, Quaternion.identity);

        Renderer rend = over.GetComponent<Renderer>();
        spawnanim_clone.transform.GetChild(0).localPosition = new Vector3(0, 0.3f, 0);
        spawnanim_clone.transform.GetChild(1).localScale = rend.bounds.size.x > rend.bounds.size.z
            ? new Vector3(rend.bounds.size.x * 1.1f, spawnanim_clone.transform.GetChild(1).localScale.y, rend.bounds.size.x * 1.1f)
            : new Vector3(rend.bounds.size.z * 1.1f, spawnanim_clone.transform.GetChild(1).localScale.y, rend.bounds.size.z * 1.1f);

    }
}
