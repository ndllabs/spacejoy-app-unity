﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using _unity.Events;
using System;
using Homefuly;

public class UIManagerDesignerScene : MonoBehaviour
{
    bool loaded;
    WidgetManager widget;
    public TextMeshProUGUI txtSegmentName;
    public ToolTip101 toolTipManager;

    // Start is called before the first frame update
    private void Awake()
    {
        widget = WidgetManager.Instance;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            NavigateCenter();
        }
    }

    private void OnEnable()
    {
        Messenger<long>.AddListener(AnalyticsCalls.DesignLoadTime, SetLoaded);   //to know when design is loaded
        Messenger<string>.AddListener("UI_ONUPDATE_SEGMENTNAME", OnSetSegmentName);
    }

    private void SetLoaded(long data)
    {
        loaded = true;
    }

    private void OnDisable()
    {
        Messenger<long>.RemoveListener(AnalyticsCalls.DesignLoadTime, SetLoaded);   //to know when design is loaded
        Messenger<string>.RemoveListener("UI_ONUPDATE_SEGMENTNAME", OnSetSegmentName);
    }

    void OnSetSegmentName(string txt)
    {
        txtSegmentName.text = txt;
    }

    public void NavigateCenter()
    {
        toolTipManager.UpdateToolTipPause(true);
        if (!loaded)
        {
            widget.Alert("Loading", "Please wait for the design to load or restart app");
            return;
        }
        Debug.Log(">>>>>>>>>>>>>> Yo");

        ExitScene();
    }

    public void ExitScene()
    {
        if(AppComponent.Instance.CurrentSceneType == SceneType.ReadOnlyDesign || !AppComponent.Instance.db.isDesignDirty)
        {
            widget.Confirm("Exit", "Are you sure you want to exit design?", (ans) =>
            {
                if (ans == "yes")
                {
                    Messenger<string>.Broadcast(StaticFunctionCalls.OnSaveDesign, "Exit Button");
                }
                else if (ans == "no" || ans == "dismiss")
                {
                    toolTipManager.UpdateToolTipPause(false);
                }
            });
        }
        else
        {
            widget.ConfirmSave((ans) =>
            {
                if (ans == "yes")
                {
                    Messenger<string>.Broadcast(StaticFunctionCalls.OnSaveDesign, "Exit Button");
                }
                else if(ans == "no")
                {
                     Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, AppComponent.Instance.db.sceneType);
                    AppComponent.Instance.account.CURRENT_CHALLENGE_ID = string.Empty;
                    AppComponent.Instance.account.CURRENT_ROOM_ID = string.Empty;
                    
                    PlayerPrefs.SetString("minimizeUnity", " ");
                    AppComponent.Instance.Navigate(AppPage.AppComponent);
                    // AppComponent.Instance.Navigate(AppPage.Home);
                }
                else if (ans == "dismiss")
                {
                    toolTipManager.UpdateToolTipPause(false);
                }
            });
        }
    }
}
