﻿/*
Script to use Native share, hides ui elements, Takes a screenshot, shares link to app.
*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using _unity.Events;

public class ShareIt : MonoBehaviour
{
    public GameObject canvasHUD, canvas;

#if UNITY_IOS
    string linkIOS = "Get the Design Home Makeover app from the Appstore now at : https://itunes.apple.com/us/app/design-home-makeover/id1448690338?mt=8";
#elif UNITY_ANDROID
    string linkAndroid = "Get the Design Home Makeover app from the Playstore now at : https://play.google.com/store/apps/details?id=com.homefuly.idsuite.retail";
#endif
    string link;

    public void Start()
    {
#if UNITY_IOS
        link = linkIOS;
#elif UNITY_ANDROID
        link = linkAndroid;
#endif

    }
    public void ClickMe()
    {
        Messenger.Broadcast(AnalyticsCalls.ShareButtonClicked);
        StartCoroutine(TakeSSAndShare());
        canvas.gameObject.SetActive(false);
        canvasHUD.gameObject.SetActive(false);

    }

    private IEnumerator TakeSSAndShare()
    {
        yield return new WaitForEndOfFrame();

      
        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        string filePath = Path.Combine(Application.temporaryCachePath, "sharedimg.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());
        canvas.gameObject.SetActive(true);
        canvasHUD.gameObject.SetActive(true);

        // To avoid memory leaks
        Destroy(ss);

        new NativeShare().AddFile(filePath).SetSubject("Design Home Makeover").SetText(link).Share();

    }
}
