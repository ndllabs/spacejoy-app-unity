﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homefuly;
using _unity.Events;

public class AnimationManager : MonoBehaviour
{

    public GameObject stars;
   

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, ExternalCelebrationCall);
        Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.Action_AssetSwapped, ExternalCelebrationCall);
        Messenger<IWallfinishHotspot>.AddListener(StaticFunctionCalls.Action_WallfinishSwapped, ExternalCelebrationCall);
    }

    private void OnDisable()
    {
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, ExternalCelebrationCall);
        Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.Action_AssetSwapped, ExternalCelebrationCall);
        Messenger<IWallfinishHotspot>.RemoveListener(StaticFunctionCalls.Action_WallfinishSwapped, ExternalCelebrationCall);
    }


   
    public void AnimateAndDestroy(IAssetEntry game, bool b)
    {
        StartCoroutine(Shrinker());

        IEnumerator Shrinker()
        {
            Transform t = game.objRef.GetComponent<Transform>();
            t.gameObject.GetComponent<Rigidbody>().useGravity = false;
            float x = t.localScale.x;
            // for (float i = x; i >= 0; i -= 0.25f)
            // {
            //     t.localScale = new Vector3(i, i, i);
            //     yield return new WaitForSeconds(0.1f);
            // }

            // smooth shrink transition
            Vector3 p = t.position;
            float i = x;
            while (i > 0)
            {
                for (float j = 0; j < 5; j++)
                {
                    i -= 0.01f * j;
                    t.localScale = new Vector3(i, i, i);
                    t.position = new Vector3(p.x, p.y + (x-i)/2.0f, p.z);
                    yield return new WaitForSeconds(0.025f);
                }
                i -= 0.25f;
            }
            // SceneManagerDesigner.DeleteAssetAfterAnimation(game, b);
            Destroy(t.gameObject);
        }
    }

    public void ExternalCelebrationCall(IHotspot hotspot)
    {
        CelebratePurchase(hotspot.objRef);
    }

    public void ExternalCelebrationCall(IAssetEntry hotspot)
    {
        CelebratePurchase(hotspot.objRef);
    }

    public void ExternalCelebrationCall(IWallfinishHotspot hotspot)
    {
        GameObject star = Instantiate(stars);
        star.transform.position = AppComponent.Instance.db.currentRoom.transform.Find(hotspot.wall).gameObject.GetComponent<BoxCollider>().center;
        StartCoroutine(Destroyer());
        IEnumerator Destroyer()
        {
            yield return new WaitForSeconds(2.9f);
            Destroy(star);
        }

    }

    public void CelebratePurchase(GameObject g)
    {
     GameObject star = Instantiate(stars, g.transform);
        StartCoroutine(Destroyer());
        IEnumerator Destroyer()
        {
            yield return new WaitForSeconds(2.9f);
            Destroy(star);
        }
    }


}
