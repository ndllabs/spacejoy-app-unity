﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using _unity.Events;
using TouchMan;
using System;
using System.Threading.Tasks;
using Homefuly;

public class TutorialManager : MonoBehaviour
{
    // For analytics 
    string Chapter;

    public TutLevels currentLevel = TutLevels.level0;

    public GameObject TutorialManagerObject;


    public Button btnMain, btnTutorial, btnNext, btnSkip;
    public Image overlay;

    public TextMeshProUGUI text;
    public GameObject textBubble;

    public GameObject arrows;
    public GameObject elipses;
    public GameObject nina;

    public RectTransform ninaRect;
    public Animator popinAnim, arrowAnim, ElipseAnim;

    public Sprite ninaThumbsUp, ninaPoint, ninaFlat, ninaIDK, ninaHi;  // Nina Flat should be on the right side of the screen. oh man. you're right.
    public Sprite dialogueBox1, dialogueBox2; // Box1 has right side tall, Box2 has left side tall


    public GameObject GradientMaskRectangle;
    public GameObject Masker;
    public GameObject MaskFrame;
    public GameObject MaskFrameCircular;
    public GameObject TutorialCanvas;
    //Buttons To Highlight
    public Button swapbtn;
    public Button shopbtn;
    public Button menubtn;
    public Button addProductbtn;
    public Button designEstimatebtn;

    //Panels to Highlight
    public GameObject swappanel;
    public GameObject assetStorepanel;

    Vector2 beginningPos, endPos;
    Vector2 defaultMin = new Vector2(0, 0);
    Vector2 defaultMax = new Vector2(0.4f, 0.7f);

    bool[] swiped = new bool[4];
    int WantPressed;


    void Start()
    {
        Chapter = "Level 0";
        WantPressed = 0;
        swiped[0] = swiped[1] = swiped[2] = swiped[3] = false;
    }

    void DesignLoaded(long obj) // is called when the design has completed loading and checks if it is the first time
    {
        if(AppComponent.Instance.db.sceneType == SceneType.UserRoom)
        {
            TutorialCanvas.SetActive(false);
            TutorialManagerObject.SetActive(false);
            return;
        }

        Debug.Log("Analytics Manager called Design Loaded");
        if (!PlayerPrefs.HasKey("First_time"))
            StartTutorial();
        else
        {
            TutorialCanvas.SetActive(false);
            TutorialManagerObject.SetActive(false);
        }
    }

    public void StartTutorial() // Starts the tutorial
    {
        // If not first time load, disable tutorials.
        // Read from player preferenes about status of tutorial
            btnSkip.gameObject.SetActive(true);
            //Debug.Log("No first time key either");
            HideNinaAndText(false);
            Messenger<bool>.Broadcast(StaticFunctionCalls.TutorialRunning, true);
            PlayerPrefs.SetString("First_time", "true");
            PlayerPrefs.Save();
            currentLevel = TutLevels.level1;
            TutorialStateManager(currentLevel);
       
    }

    private void OnEnable()
    {
        TouchManEvents.OnDrag+= TouchManEvents_OnDrag;
        TouchManEvents.OnBeginDrag+= TouchManEvents_OnBeginDrag;
        Messenger.AddListener(StaticFunctionCalls.ButtonInfo, DesiredButtonPressed );
        Messenger.AddListener(StaticFunctionCalls.ButtonSwap, DesiredButtonPressed);
        Messenger.AddListener(StaticFunctionCalls.MaximiseMenuPanel, MaximisePressed);
        Messenger.AddListener(StaticFunctionCalls.MaximiseMenuPanel, MaximisePressed3);
        Messenger<PanelModes>.AddListener(StaticFunctionCalls.openHUDPanel, AddProductOpened);
        Messenger<PanelModes>.AddListener(StaticFunctionCalls.openHUDPanel, AddProductOpened3);
        Messenger<IAsset, Vector3, GameObject>.AddListener(AppEvents.ASSET_DROPPED_INSIDE_ROOM, AssetDroppedInsideRoom);
        Messenger<IAsset>.AddListener(AppEvents.OnSwapCurrentAsset, AssetDroppedInsideRoom1);
        Messenger<IAssetEntry>.AddListener(StaticFunctionCalls.OnAssetSelected, AssetSelected);
        Messenger<long>.AddListener(AnalyticsCalls.DesignLoadTime, DesignLoaded);
        Messenger.AddListener(StaticFunctionCalls.OpenTutorial, StartTutorial);
    }



    private void OnDisable()
    {
        TouchManEvents.OnDrag -= TouchManEvents_OnDrag;
        TouchManEvents.OnBeginDrag -= TouchManEvents_OnBeginDrag;
        Messenger.RemoveListener(StaticFunctionCalls.ButtonInfo,DesiredButtonPressed);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonSwap, DesiredButtonPressed);
        Messenger.RemoveListener(StaticFunctionCalls.MaximiseMenuPanel, MaximisePressed);
        Messenger.RemoveListener(StaticFunctionCalls.MaximiseMenuPanel, MaximisePressed3);
        Messenger<IAsset, Vector3, GameObject>.RemoveListener(AppEvents.ASSET_DROPPED_INSIDE_ROOM, AssetDroppedInsideRoom);
        Messenger<IAsset>.RemoveListener(AppEvents.OnSwapCurrentAsset, AssetDroppedInsideRoom1);
        Messenger<PanelModes>.RemoveListener(StaticFunctionCalls.openHUDPanel, AddProductOpened);
        Messenger<PanelModes>.RemoveListener(StaticFunctionCalls.openHUDPanel, AddProductOpened3);
        Messenger<IAssetEntry>.RemoveListener(StaticFunctionCalls.OnAssetSelected, AssetSelected);
        Messenger<long>.RemoveListener(AnalyticsCalls.DesignLoadTime, DesignLoaded);
        Messenger.RemoveListener(StaticFunctionCalls.OpenTutorial, StartTutorial);
    }

   
    void TouchManEvents_OnBeginDrag(GameObject dragStartedOver, Vector2 touchPosition)
    {
        beginningPos = touchPosition;
    }


    void TouchManEvents_OnDrag(GameObject dragStartedOver, Vector2 touchPosition, Vector2 touchDeltaPosition)
    {
        if (currentLevel == TutLevels.complete || currentLevel == TutLevels.level0)
            return;
        endPos = touchPosition;
        Vector2 temp;
        temp.x =(endPos.x - beginningPos.x); 
        temp.y = (endPos.y - beginningPos.y);

        if (Mathf.Abs(temp.x) > Mathf.Abs(temp.y))
            {
            // Horizontal swipe
            if (temp.x > 400)
            {
#if UNITY_IOS
                swiped[1] = true;
#else
                swiped[0] = true;
#endif
            }
            else
            if (temp.x < -400)
            {
#if UNITY_IOS
                swiped[0] = true;
#else
                swiped[1] = true;
#endif
            }
        }
        else
            {
            // Vertical Swipe
            if (temp.y > 400)
            {
#if UNITY_IOS
                swiped[3] = true;
#else
                swiped[2] = true;
#endif
            }
            else
            if (temp.y < -400)
            {
#if UNITY_IOS
                swiped[2] = true;
#else
                swiped[3] = true;
#endif
            }
        }
           
    }


    void HideUIButtons()
    {
        //Debug.Log("Hiding UI buttons");
        Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, true);
    }

    void HideNinaAndText(bool hide)
    {
        nina.gameObject.SetActive(!hide);
        text.gameObject.SetActive(!hide);
        textBubble.gameObject.SetActive(!hide);
        btnNext.gameObject.SetActive(!hide);
        overlay.gameObject.SetActive(!hide);
    }

    void resetScene()
    {
        // Unhide the main buttons
        Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, false);
    }


    GameObject Masker1, Masker2, Masker3, Masker4, MaskFrameInstance; //GradientMaskInstance;
    void MaskIt(GameObject btn, MaskMode maskNeeded, bool special=false) // Used to mask ui elements, takes the ui element gameobject and mask mode as parameters
    {
        Debug.Log("Masking");

        RectTransform bt = btn.GetComponent<Image>().rectTransform;
        RectTransform rt;
        Vector3[] corners = new Vector3[4];
        int screenWidth, screenHeight;
        screenWidth = Screen.width;
        screenHeight = Screen.height;

        Debug.Log("Screen Width: " + screenWidth + " Screen Height :" + screenHeight);
        bt.GetWorldCorners(corners);
        foreach (Vector3 corner in corners)
            Debug.Log(corner);

       //GradientMaskInstance = Instantiate(GradientMaskRectangle,btn.transform);

        //Left Side
        if (!special)
        {
            Masker1 = Instantiate(Masker);
            rt = Masker1.GetComponent<Image>().rectTransform;
            Masker1.transform.SetParent(TutorialCanvas.transform);
            Masker1.transform.SetAsFirstSibling();

            rt.anchorMin = new Vector2(0, corners[0].y / screenHeight);
            rt.anchorMax = new Vector2(corners[0].x / screenWidth, corners[1].y / screenHeight);
            rt.offsetMax = new Vector2(0, 0);
            rt.offsetMin = new Vector2(0, 0);
            rt.localScale = new Vector3(1, 1, 1);
        }

        //Right Side
        Masker2 = Instantiate(Masker);
        rt = Masker2.GetComponent<Image>().rectTransform;
        Masker2.transform.SetParent(TutorialCanvas.transform);
        Masker2.transform.SetAsFirstSibling();

        rt.anchorMin = new Vector2(corners[2].x / screenWidth, corners[3].y / screenHeight);
        rt.anchorMax = new Vector2(1, corners[2].y / screenHeight);
        rt.offsetMax = new Vector2(0, 0);
        rt.offsetMin = new Vector2(0, 0);
        rt.localScale = new Vector3(1, 1, 1);


        //Top
        Masker3 = Instantiate(Masker);
        rt = Masker3.GetComponent<Image>().rectTransform;
        Masker3.transform.SetParent(TutorialCanvas.transform);
        Masker3.transform.SetAsFirstSibling();

        rt.anchorMin = new Vector2(0, corners[1].y / screenHeight);
        rt.anchorMax = new Vector2(1, 1);
        rt.offsetMax = Vector2.zero;
        rt.offsetMin = Vector2.zero;
        rt.localScale = new Vector3(1, 1, 1);

        //Bottom
        Masker4 = Instantiate(Masker);
        rt = Masker4.GetComponent<Image>().rectTransform;
        Masker4.transform.SetParent(TutorialCanvas.transform);
        Masker4.transform.SetAsFirstSibling();

        rt.anchorMin = new Vector2(0, 0);
        rt.anchorMax = new Vector2(1, corners[0].y / screenHeight);
        rt.offsetMax = Vector2.zero;
        rt.offsetMin = Vector2.zero;
        rt.localScale = new Vector3(1, 1, 1);

        if (maskNeeded == MaskMode.cylinder)
            MaskFrameInstance = Instantiate(MaskFrame, btn.transform);
        else
             if (maskNeeded == MaskMode.circle)
            MaskFrameInstance = Instantiate(MaskFrameCircular, btn.transform);
        else
            return;


    }

    void UnMaskIt() // Destroy ui masking elements 
    {
        //Debug.Log("Unmasking");
        Destroy(Masker1);
        Destroy(Masker2);
        Destroy(Masker3);
        Destroy(Masker4);
        Destroy(MaskFrameInstance);
        //Destroy(GradientMaskInstance);
    }


    void TutorialStateManager(TutLevels level)
    {

        switch(level)
        {
            case TutLevels.level0:  
                // Do stuff
                resetScene();
                break;

            case TutLevels.level1:
                level1();
               //resetScene();
                break;

            case TutLevels.level2:
                level2();
               // resetScene();
                break;

            case TutLevels.level3:
                resetScene();
                break;

            case TutLevels.level4:
                resetScene();
                break;

            case TutLevels.level5:
                resetScene();
                break;

            case TutLevels.complete:
                resetScene();
                break;
        }

    }



   
    void level1()
    {
        HideUIButtons();
        btnNext.onClick.RemoveAllListeners();
        btnNext.onClick.AddListener(level1Screens);
        currentLevel = TutLevels.level1;
        //TutorialStateManager(currentLevel);
    }

    void level2()
    {
        HideUIButtons();
        btnNext.onClick.RemoveAllListeners();
        btnNext.onClick.AddListener(level2Screens);
        currentLevel = TutLevels.level2;
        //TutorialStateManager(currentLevel);
    }

   
    void level3()
    {
        HideUIButtons();
        btnNext.onClick.RemoveAllListeners();
        btnNext.onClick.AddListener(level2Screens);
        currentLevel = TutLevels.level3;
        //TutorialStateManager(TutLevels.level4);
    }

    void level4()
    {
        TutorialStateManager(TutLevels.level5);
    }

    void level5()
    {
        TutorialStateManager(TutLevels.complete);
    }

    void levelComplete()
    {
        // Done. Reward user or something 
    }
   

     int stage =0 ; // static int's default value is 0
    async void level1Screens()
    {
        if (currentLevel != TutLevels.level1)
            return;

        Chapter = "Level 1";
    
        string[] s = {
            "Let's learn all the amazing things we can do here. ",
            "First let's learn how to move around in the room.",
            "Just follow the arrows & drag your finger.",
            "Just follow the arrows & drag your finger.",
            "and finally swipe to the left to turn the camera to the left",
            "Great Job! Now you know how to move around!",
            "Awesome! Now tap a product that you like.",
            "Alright, first go to any product you like in the room and touch it.",
            "Go Ahead! I'll wait...",
            "Good choice! Click on the shop button on your left",
            "Click on the \"Shop\" Button",
            "This button will take you to the product listing page.",
            "Next, click on the Swap button.",
            "This is the swap panel and it shows you all the alternates of the product you've just selected",
            "Tap on a product to Swap!",
            "Excellent choice! You're doing amazing!"
            };

        overlay.gameObject.SetActive(true);
        arrowAnim.gameObject.SetActive(false);
        stage++;
        switch(stage)
        {
            case 1:
                text.text = s[0];
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                Messenger<string>.Broadcast(AnalyticsCalls.TutorialChapterStarted, Chapter);
                break;
            case 2:
                text.text = s[1];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                break;
            case 3:
                text.text = s[2];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                break;
            case 4:
                StartCoroutine(waiter(ninaIDK, 2));
                arrows.gameObject.SetActive(true);
                arrows.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                arrowAnim.gameObject.SetActive(true);
#if UNITY_IOS
                arrowAnim.Play("swipedown");
#else
                arrowAnim.Play("swipeup");
#endif

                text.text = s[2];
                break;
            case 5:
                text.text = s[3];
                StartCoroutine(waiter(ninaPoint,3));
                arrowAnim.gameObject.SetActive(true);
#if UNITY_IOS
                arrowAnim.Play("swipeup");
#else
                arrowAnim.Play("swipedown");
#endif
                break;
            case 6:
                text.text = s[4];
                StartCoroutine(waiter(ninaIDK,0));
                arrowAnim.gameObject.SetActive(true);
#if UNITY_IOS
                arrowAnim.Play("swipeleft");
#else
                arrowAnim.Play("swiperight");
#endif
                break;
            case 7:
                text.text = s[5];
                StartCoroutine(waiter(ninaThumbsUp,1));
                arrowAnim.gameObject.SetActive(true);
#if UNITY_IOS
                arrowAnim.Play("swiperight");
#else
                arrowAnim.Play("swipeleft");
#endif
                break;
            case 8:
                HideNinaAndText(false);
                popinAnim.Play("pop-in");
                arrows.GetComponent<Image>().color = new Color32(255, 255, 255, 0);
                text.text = s[6];
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                break;

            case 9:
                level1Screens();
                break;

            case 10:
                text.text = s[8];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                break;

            case 11:
                Messenger<bool>.Broadcast(StaticFunctionCalls.TutorialRunning, false);
                StartCoroutine(waitForInput(true));
                text.text = s[9];
                nina.gameObject.GetComponent<Image>().sprite = ninaThumbsUp;
                ninaRect.anchorMin = new Vector2(0.7f, 0); // The right side
                ninaRect.anchorMax = new Vector2(1.1f, 0.7f);
                ninaRect.offsetMax = Vector2.zero;
                ninaRect.offsetMin = Vector2.zero;
                textBubble.gameObject.GetComponent<Image>().sprite = dialogueBox2;
                Debug.Log("Bubble image changed, waiting for input");
                break;

            case 12:
                level1Screens();
                break;

            case 13: // Wait for input, Shop button
                StartCoroutine(waitForInput(true));
                MaskIt(shopbtn.gameObject,MaskMode.circle);
                break;

            case 14:
                text.text = s[11];
                UnMaskIt();
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                overlay.gameObject.SetActive(true);
                ninaRect.anchorMin = defaultMin;
                ninaRect.anchorMax = defaultMax;
                textBubble.GetComponent<Image>().sprite = dialogueBox1;
                level1Screens();
                Debug.Log("case 13");
                break;

            case 15:
                text.text = s[12];
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                overlay.gameObject.SetActive(true);
                Debug.Log("case 14");
                break;

            case 16: 
                arrows.gameObject.SetActive(true);
                arrowAnim.Play("pointswap");
                StartCoroutine(waitForInput(true));
                MaskIt(swapbtn.gameObject,MaskMode.circle);
                Debug.Log("case 15");
                break;

            case 17: // Wait for input, swap.
                UnMaskIt();
                arrows.gameObject.SetActive(false);
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                text.text = s[14];
                overlay.gameObject.SetActive(true);
                await Assets();
                Debug.Log("case 16, Checking if swappable only now");
                break;

            case 18:
                level1Screens();
                break;

            case 19:
                MaskIt(swappanel,MaskMode.none);
                StartCoroutine(specialWaitForInput());
                overlay.gameObject.SetActive(false);
                nina.gameObject.GetComponent<Image>().sprite = ninaThumbsUp;
                text.text = s[15];
                Debug.Log("case 18");
                break;

            case 20:
                HideNinaAndText(true);
                btnNext.gameObject.SetActive(true);
                overlay.gameObject.SetActive(true);
                UnMaskIt();
                break;

            case 21:
                currentLevel = TutLevels.level2;
                HideNinaAndText(false);
                Messenger<bool>.Broadcast(StaticFunctionCalls.TutorialRunning, true);
                UnMaskIt();
                btnNext.onClick.RemoveAllListeners();
                btnNext.onClick.AddListener(level2Screens);
                Debug.Log("case 19");
                Messenger<string>.Broadcast(AnalyticsCalls.TutorialChapterComplete, Chapter);
                level2Screens();
                break;

           
        }

       
    }

   
    int stage2 =0;
    void level2Screens()
    {
        if (currentLevel != TutLevels.level2)
            return;
      
        Chapter = "Level 2";

        string[] s = {
        "Alright! Now why don't you move the product to a new place.",
        "To move the product, just touch and hold the product and move your finger to where you want to move the product to!",     
        "Go ahead and try it!",
        "That looks good there! Now lets add a product into the room",
        "I think this place needs a little something more, don't you?",
        "Press the Menu button",
        "This is the menu panel, you can find everything you'll need here",
        "Lets add a new product to the room",
        "Click on \"Add Product\"",
        "This is the products panel, here you can find all the products available to add into the room.",
        "Click on Add Product",
        "Now find a product you like and drag it onto the floor of the room",
        "Awesome! You are doing Great!",
        "Awesome Job! You can add more products this way!"
        };

        stage2++;
     
        switch (stage2)
        {
            case 1:
                overlay.gameObject.SetActive(true);
                Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated,false);
                Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global);
                Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
                Messenger<string>.Broadcast(AnalyticsCalls.TutorialChapterStarted, Chapter);
                HideUIButtons();
                text.text = s[0];
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                break;

            case 2:
                text.text = s[1];
                nina.gameObject.GetComponent<Image>().sprite = ninaIDK;
                break;

            case 3:
                text.text = s[2];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                Debug.Log("Case 3");
                break;

            case 4: // Waiting for movement 
                Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, false);
                Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global);
                Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
                Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, true);
                Debug.Log("Case 4");
                break;

            case 5:
                nina.gameObject.SetActive(false);
                text.gameObject.SetActive(false);
                textBubble.gameObject.SetActive(false);
                btnNext.gameObject.SetActive(true);
                overlay.gameObject.SetActive(false);
                Debug.Log("Case 5");
                break;

            case 6:
                text.text = s[3];
                nina.gameObject.SetActive(true);
                text.gameObject.SetActive(true);
                textBubble.gameObject.SetActive(true);
                overlay.gameObject.SetActive(true);
                nina.gameObject.GetComponent<Image>().sprite = ninaThumbsUp;
                Debug.Log("Case 6");
                level2Screens();
                break;

            case 7:
                Messenger<bool>.Broadcast(StaticFunctionCalls.HideUICauseMinimap, true);
                Messenger.Broadcast(StaticFunctionCalls.DoDeselectAsset_Deprycated);
                overlay.gameObject.SetActive(true);
                Debug.Log("Case 7");
                break;

            case 8:
                Debug.Log("case 8");
                overlay.gameObject.SetActive(false);
                level2Screens();
                break;
            case 9:
                resetScene();
                HideNinaAndText(true);
                overlay.gameObject.SetActive(false);
                MaskIt(menubtn.gameObject, MaskMode.circle);
                Debug.Log("case 9");
                break;

            case 10:
                UnMaskIt();
                Debug.Log("case 10");
                level2Screens();
                break;

            case 11:
                Debug.Log("case 11");
                level2Screens();
                break;

            case 12:
                Debug.Log("case 12");
                text.text = s[10];
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                HideNinaAndText(false);
                break;

            case 13:
                HideNinaAndText(true);
                MaskIt(addProductbtn.gameObject, MaskMode.circle);
                Debug.Log("case 13");
                break;

            case 14:
                UnMaskIt();
                text.text = s[11];
                HideNinaAndText(false);
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                break;

            case 15:
               
                MaskIt(swappanel.gameObject,MaskMode.none,true);
                HideNinaAndText(true);
                break;

            case 16:
                UnMaskIt();
                level2Screens();
                break;

            case 17:
                text.text = s[12];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                HideNinaAndText(false);
                Debug.Log("case 17");
                break;

            case 18:  //Wait for input
                level2Screens();
                break;

            case 19:
                UnMaskIt();
                text.text = s[13];
                nina.gameObject.GetComponent<Image>().sprite = ninaThumbsUp;
                Debug.Log("case 19");
                Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global);
                Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
                currentLevel = TutLevels.level3;
                btnNext.onClick.RemoveAllListeners();
                btnNext.onClick.AddListener(level3Screens);
                Messenger<string>.Broadcast(AnalyticsCalls.TutorialChapterComplete, Chapter);
                level3Screens();
                break;

        }
    }

     int stage3 =0;
       void level3Screens()
         {
        if (currentLevel != TutLevels.level3)
            return;
        Chapter = "Level 3";
       
        string[] s = {
            "Just one more last step to go. Click on the $ button.",
            "Click on Menu",
            "Click on \"Design Estimate\"",
            "This is the Estimate Panel and here you can see the prices of all the products in the room",
            "Have a look and see, you can also shop the products you like here",
            "This is the Estimate Panel and here you can see the prices of all the products in the room!",
            "And That'll be the end of the tutorial!",
            "And that's it! Happy Designing!",
            "Just press the question mark on the bottom right of the screen if you need me. Bye!"
         };
        stage3++;
        switch(stage3)
        {
            case 1:
                text.text = s[0];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                resetScene();
                Debug.Log("case 1");
                Messenger<string>.Broadcast(AnalyticsCalls.TutorialChapterStarted, Chapter);
                break;

            case 2:
                level3Screens();
                break;
            case 3:
                StartCoroutine(waitForInput(true));
                MaskIt(designEstimatebtn.gameObject, MaskMode.circle);
                Debug.Log("case 3");
                break;

            case 4:
                UnMaskIt();
                level3Screens();
                break;

            case 5:
                level3Screens();
                break;

            case 6:
                UnMaskIt();
                text.text = s[3];
                nina.gameObject.GetComponent<Image>().sprite = ninaFlat;
                Debug.Log("case 6");
                break;

            case 7:
                level3Screens();
                break;

            case 8:
                level3Screens();
                break;

            case 9:
                level3Screens();
                break;

            case 10:
                text.text = s[7];
                nina.gameObject.GetComponent<Image>().sprite = ninaPoint;
                HideNinaAndText(true);
                btnNext.gameObject.SetActive(false);
                StartCoroutine(waitForSeconds(3));
                Debug.Log("case 10");
                break;

            case 11:
                level3Screens();
                break;

            case 12:
                Debug.Log("case 12");
                Messenger<string>.Broadcast(AnalyticsCalls.TutorialChapterComplete, Chapter);
                levelCompleteScreens();
                break;

        }
    }


    public void levelCompleteScreens()
    {
        Chapter = "Complete";
        Messenger<string>.Broadcast(AnalyticsCalls.TutorialCompleted, Chapter);
        currentLevel = TutLevels.level0;
        nina.gameObject.GetComponent<Image>().sprite = ninaHi;
        btnNext.onClick.RemoveAllListeners();
        UnMaskIt();
        Messenger<bool>.Broadcast(StaticFunctionCalls.TutorialRunning, false);
        stage = stage2 = stage3 = 0;
        btnSkip.gameObject.SetActive(false);
        text.text = "Hello and Welcome to Homefuly!";
        arrowAnim.StopPlayback();
        arrows.SetActive(false);
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, false);
        Messenger.Broadcast(StaticFunctionCalls.DoDeselectAsset_Deprycated);
        Messenger.Broadcast(StaticFunctionCalls.OpenMenuPanel);
        HideNinaAndText(true);
        WantPressed = 0;
        TutorialCanvas.gameObject.SetActive(false);
        TutorialManagerObject.gameObject.SetActive(false);

    }

    public void SkipTutorials()
    {
        currentLevel = TutLevels.level0;
        nina.gameObject.GetComponent<Image>().sprite = ninaHi;
        btnNext.onClick.RemoveAllListeners();

        UnMaskIt();

        Messenger<bool>.Broadcast(StaticFunctionCalls.TutorialRunning, false);
        Messenger<string>.Broadcast(AnalyticsCalls.TutorialSkipped, Chapter);
        Chapter = "Level 0";
        stage = stage2 = stage3 = 0;
        btnSkip.gameObject.SetActive(false);
        text.text = "Hello and Welcome to Homefuly!";
        resetScene();
        arrowAnim.StopPlayback();
        arrows.SetActive(false);
        Messenger<PanelModes>.Broadcast(StaticFunctionCalls.openHUDPanel, PanelModes.Global);
        Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, false);
        Messenger.Broadcast(StaticFunctionCalls.DoDeselectAsset_Deprycated);
        HideNinaAndText(true);
        ninaRect.anchorMin = defaultMin;
        ninaRect.anchorMax = defaultMax;
        WantPressed = 0;
        TutorialCanvas.gameObject.SetActive(false);
        TutorialManagerObject.gameObject.SetActive(false);
    }

    async Task Assets()
    {
        if (!AppComponent.Instance.db.IsCurrentObjectValid<IAssetEntry>()) return;

        List<IAsset> data = await AppComponent.Instance.server.artifact.GetAssetsBySubcategory((AppComponent.Instance.db.currentObject as IAssetEntry).asset.subcategory, 0, 24);
        Debug.Log("Selected asset is : " + (AppComponent.Instance.db.currentObject as IAssetEntry).asset.name);
        Debug.Log("Inside Assets function");
        Debug.Log("Number of swappable:" + data.Count);
      
         
        bool noResult = true;
        foreach (IAsset asset in data)
        {
            bool isShoppable = asset.category != AppConfig.PilotDemoConfig.NonShoppableCatId ? true : false;
            if (asset.currency == "INR" | !isShoppable | asset._id == (AppComponent.Instance.db.currentObject as IAssetEntry).asset._id | asset.price.customer == 0) continue;   //dont show same asset in swap //Dont show INR products or nonshoppable products    //Dont show products with price = 0
            noResult = false;
         
        }

        if (noResult)
        {
            stage+=3;
            level1Screens();
            Debug.Log("Nothing to swap to, skipping swap");
        }

    }

    IEnumerator waiter(Sprite s,int index) // Waits for input and changes nina's sprite to the next required one, used for detecting swipe
    {
        btnNext.gameObject.SetActive(false);
        textBubble.gameObject.SetActive(false);
        popinAnim.Play("popout");
        swiped[0] = swiped[1] = swiped[2] = swiped[3] = false;
        overlay.gameObject.SetActive(false);
        while (!swiped[index])
        {
            yield return null;
        }
        level1Screens();
        yield return new WaitForSeconds(1);

    }

    IEnumerator specialWaitForInput() // Just to detect swaps
    {
        HideNinaAndText(true);
        WantPressed = 0;
        while (WantPressed < 1)
            yield return null;

        yield return new WaitForSeconds(1);
    }

    IEnumerator waitForInput(bool hide) // Waiting for input and hides nina if true
    {
        if (hide)
        {
            HideNinaAndText(true);
        }
        WantPressed = 0;
        while (WantPressed < 1)
            yield return null;
        HideNinaAndText(false);
        yield return new WaitForSeconds(1);

    }

    IEnumerator waitForSeconds(int seconds) // waits for given seconds and proceeds to next indefineitly 
    {
        btnNext.gameObject.SetActive(false);
        HideNinaAndText(true);
        yield return new WaitForSeconds(seconds);
        HideNinaAndText(true);
        level3Screens();

    }

    void AssetSelected(IAssetEntry obj)
    {
        WantPressed++;
    }

    void AddProductOpened(PanelModes obj)
    {
        WantPressed++;
        level2Screens();
    }

    void AddProductOpened3(PanelModes obj)
    {
        WantPressed++;
        level3Screens();
    }

    void AssetDroppedInsideRoom(IAsset arg1, Vector3 arg2, GameObject arg3)
    {
        WantPressed++;
        level2Screens();
    }

    void AssetDroppedInsideRoom1(IAsset arg3)
    {
        WantPressed++;
        level1Screens();
    }

    void DesiredButtonPressed()
    {
        WantPressed++;
        level1Screens();
    }

    void MaximisePressed()
    {
        WantPressed++;
        level2Screens();
    }

    void MaximisePressed3()
    {
        WantPressed++;
        level3Screens();
    }
   

    public enum TutLevels
    {
        level0,
        level1,
        level2,
        level3,
        level4,
        level5,
        complete
    }

    public enum MaskMode
    {
        cylinder,
        circle,
        none
    }
}
