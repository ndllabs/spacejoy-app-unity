﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ColliderBreaker : MonoBehaviour
{
    public List<BoxCollider> boxes = new List<BoxCollider>();
    Vector3[] vertices;
   
    Mesh mesh;
    // Start is called before the first frame update
    void Start()
    {
        mesh = gameObject.GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;

        PreProcessVertices();
        //ProcessAsset();
        //EncapsulateBoxColliders();


    }

    private void PreProcessVertices()
    {
        var v = vertices.Distinct();
        Debug.Log(v.Count() +" " + vertices.Count());
    }

    public void ProcessAsset()
    {



        for (int i = 0; i < vertices.Length; i++)
        {


            BoxCollider bx = gameObject.AddComponent<BoxCollider>();

            bx.center = vertices[i];
            bx.size = new Vector3(0.25f, 0.25f, 0.25f);
            boxes.Add(bx);


        }





    }

    public void EncapsulateBoxColliders()
    {
       
    }
}
