﻿
using _unity;
using _unity.Events;
using DG.Tweening;
using Homefuly;
using Homefuly.Analytics;
using Homefuly.Events;
using Selector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using TouchMan;
using UnityEngine;

public class SceneManagerDesigner : MonoBehaviour
{
    #region Vars

    System.Diagnostics.Stopwatch timer; //, timerAutoSave;
    CancellationTokenSource CancelToken;

    private void OnDestroy()
    {
        if (CancelToken != null) CancelToken.Cancel();
    }

    public bool isTutorial;
    public GameObject TutorialCanvas;
    public GameObject TutorialManager;
    public GameObject preCam;
    public GameObject roomParent;

    [Header("Component Modules")]
    public GameObject ComFPS;
    public GameObject ComMiniCam;

    [Header("Lights")]
    public Light lightAmbient;
    public GameObject ComLightContainer;
    public GameObject reflectionProbe;
    public GameObject lightHalo;
    public Material matLight;
    public float haloOffset = .3f;


    [Header("Navigation")]
    //public GameObject elemDoorPortal;
    public Material segmentMaterial;
    public IFloorPlanProcessor floorplanData;


    [Header("FPS")]
    public Player69FPS scrPlayerFPS;
    public FPSPlayer scrPlayerFpsAlt;

    [Header("Assets")]
    public Transform assetsContainer;
    public GameObject prefSelector;
    // public GameObject prefPurchasable;

    [Header("HUD")]
    public TextMeshProUGUI txtInfo;
    public GameObject saveSpinner;
    public GameObject btnSave;

    [Header("Wallposter")]
    public GameObject prefWallPoster;

    [Header("Minimap")]
    public Texture MinimapImage;

    [Header("Tool tip")]
    public ToolTip101 tooltipManager;

    AppComponent app;
    ArtifactManager artifactManager;
    WidgetManager widget;

    GameObject roomGo;
    Bounds roomBounds;
    GameObject selectedWall;
    GameObject selectedWallPoster;
    DesignSceneState currentState;

    SceneType sceneType = SceneType.None;

    string CURRENT_ROOM, CURRENT_ROOM_NAME, CURRENT_VERSION_ID;

    IRoomDesign roomDesign;
    Vector3 fpsPos, fpsRot;

    ITransform lastPosInNormalView;
    ITransform lastPosInTopView;

    bool isCamVertical, isAssetLoading = false;

    List<ITransform> viewPointsList;
    IRoomDesign designDataCache;

    public GameObject degrees;
    bool isStoreOpen, isAssetSelected;
    Vector3 currentAssetPos, currentAssetRot;

    public int originalCulMask;
    public int totalUnboughtAssets = 0;
    public int maxAllowedUnboughtAssetsAllowed = 2;
    private ITransform prevTransform;

    int loadProgressCount, loadProgressTotal;
    static InAppBrowserBridge bridge;

    // private string publicProjectId = "5cb5b29a4c6813333fef7610"; // public project
    //private string loginPageUrl = AppConfig.ServerConfig.inAppBrowserHost + "/#/unity-login/true";
    private bool alreadySaved; // to check if design is saved in current session
    private string alreadySavedVersionId = null, alreadySavedName = null;
    private string buttonType = string.Empty;
    private float saveStart = 20f;
    private float saveFreq = 20f;
    private bool autosaveDone = false; // for prevent autosave analytics call spamming

    public AnimationManager animationMan;

    #endregion

    #region Broadcasts
    /// <summary>
    /// Broadcasts when the Asset Swap action is "COMPLETE"
    /// </summary>
    /// <param name="hotpot">hotspot Class reference</param>
    private void Broadcast_ActionAssetSwapped(IAssetEntry hotpot)
    {
        Messenger<IAssetEntry>.Broadcast(StaticFunctionCalls.Action_AssetSwapped, hotpot);
    }

    private void Broadcast_ActionAssetDropped(string hotpot)
    {
        //Messenger<string>.Broadcast(AppEvents.ASSET_DROP_COMPLETE, hotpot);
        Messenger.Broadcast(AppEvents.ASSET_DROP_COMPLETE);
    }

    private void Broadcast_OnActionFailed(ActionType actiontype, int errCode)
    {
        HomefulyEvents.OnActionFailed?.Invoke(actiontype, errCode);
    }
    #endregion

    #region Unity Functions

    private void Awake()
    {

        HomefulyExtensions.SetScreenOrientation(true);

        // Debug.Log("token: " + app.account.CRED_JWTTOKEN);


        //Screen.autorotateToPortrait = false;
        //Screen.autorotateToLandscapeRight = false;
        //Screen.autorotateToPortraitUpsideDown = false;
        //Screen.autorotateToLandscapeLeft = true;

        //Screen.orientation = ScreenOrientation.AutoRotation;

        widget = WidgetManager.Instance;
        //originalTouchGestureLayerMask = ComFPS.GetComponentInChildren<StandardLayer>().LayerMask;

        bridge = FindObjectOfType<InAppBrowserBridge>();
        //Debug.Log("[Awake SceneManagerDesigner] Bridge is null");

        alreadySaved = false;
    }

    async void Start()
    {
        widget.Loader("Loading, Please Wait...", 50);
        await ChangeSceneState(DesignSceneState.Preload);

        widget.LoaderClose();
    }


    private void Update()
    {
    }

    // To be tested
    void OnApplicationFocus(bool hasFocus)
    {
        if(hasFocus)
        {
            #if !UNITY_EDITOR
                Canvas.ForceUpdateCanvases();
                Debug.Log("force redraw on lost focus!");
            #endif
        }
    }

    void OnApplicationPause(bool pause)
    {
        if(!pause)
        {
            #if !UNITY_EDITOR
                Canvas.ForceUpdateCanvases();
                Debug.Log("force redraw on pause!");
            #endif
        }
    }

    private void OnEnable()
    {
        Messenger<bool>.AddListener(StaticFunctionCalls.ToggleCamView, ToggleTopView);
        Messenger<string>.AddListener(StaticFunctionCalls.OnSaveDesign, OnSaveManual);

        //Messenger.AddListener(StaticFunctionCalls.MinimisePanel, OnMinimisePanel);
        //Messenger.AddListener(StaticFunctionCalls.MaximisePanel, OnMaximisePanel);

        Messenger<bool>.AddListener(StaticFunctionCalls.AssetDragFromStoreEvents, OnDragAssetFromStore);

        Messenger<IAsset, Vector3, GameObject>.AddListener(AppEvents.ASSET_DROPPED_INSIDE_ROOM, OnAssetDroppedInsideRoom_Wrapper);
        Messenger<IAsset>.AddListener(AppEvents.OnSwapCurrentAsset, OnSwapCurrentAsset);

        Messenger.AddListener(StaticFunctionCalls.ButtonDelete_Deprycated, ButtonDelete_Deprycated);
        Messenger.AddListener(StaticFunctionCalls.DeleteAsset, DeleteAsset_Wrapper);
        Messenger.AddListener(StaticFunctionCalls.ButtonDistance, ButtonDistance);

        Messenger.AddListener(StaticFunctionCalls.ButtonRotate, OnUIBtnAssetRotate);
        Messenger.AddListener(StaticFunctionCalls.ButtonHorizontal, OnUIBtnAssetMoveHorizontal);
        Messenger.AddListener(StaticFunctionCalls.ButtonVertical, OnUIBtnAssetMoveVertical);

        Messenger.AddListener(StaticFunctionCalls.DoDeselectAsset_Deprycated, DoDeselectAsset);

        TouchManEvents.OnTap += OnScreenTap;
        SelectorMain.OnClickPurchase += OnUpdateTransaction;
        HomefulyEvents.OnAuthUpdated += OnAuthUpdated;

        Messenger<IWallfinish>.AddListener(StaticFunctionCalls.OnUpdateTransaction, OnUpdateTransaction_Wallfinish);

        Messenger.AddListener(StaticFunctionCalls.DoDeselectAsset_Deprycated, DoDeselectAsset);

        //Messenger<GameObject>.AddListener(AppEvents.ASSET_TAPPED, ON_ASSET_TAPPED);
        //Messenger<GameObject>.AddListener(StaticFunctionCalls.onGameObjectTapped, OnGameobjectTapped);

        Messenger.AddListener(StaticFunctionCalls.TriggerPositionSave, AddRenderPosition);

        Messenger<int>.AddListener(StaticFunctionCalls.RelocatePlayer, SpawnActorTransform);
        Messenger<int>.AddListener(StaticFunctionCalls.DeleteViewpoint, DeleteViewpoint);

        //Messenger<Vector3>.AddListener(StaticFunctionCalls.MovePlayerThroughPortal, MovePlayerThroughPortal);

        #region WallposterEvents
        //Messenger<Transform, Texture>.AddListener(StaticFunctionCalls.WallposterDroppedOnWall, WallposterDroppedOnWall);
        Messenger<Transform, Texture>.AddListener(StaticFunctionCalls.WallposterSelected, WallposterSelected);
        Messenger<Transform, Texture>.AddListener(StaticFunctionCalls.WallposterLongPressed_Deprycated, WallposterLongPressed);
        Messenger.AddListener(StaticFunctionCalls.WallpaperDeselected, WallpaperDeselect);
        Messenger.AddListener(StaticFunctionCalls.WallpaperDeleted, DeleteSelectedWallpaper);
        //Messenger.AddListener(StaticFunctionCalls.DeselectAsset, DeselectAsset);
        #endregion


        #region Wallfinish
        Messenger<IWallfinish, GameObject>.AddListener(StaticFunctionCalls.DropWallfinishInScene, OnWallfinishDroppedOnScene);
        #endregion

        #region Tutorial
        Messenger<bool>.AddListener(StaticFunctionCalls.TutorialRunning, TutorialChecker);
        #endregion

        //#region dummy 
        //Messenger.AddListener(StaticFunctionCalls.MaximiseMenuPanel, Dummy);
        //#endregion

        bridge.onJSCallback.RemoveAllListeners();
        bridge.onJSCallback.AddListener(OnIABMessage);
        bridge.onBrowserClosed.AddListener(OnBrowserClosed);
    }

    private void OnAuthUpdated(AuthUpdateType authUpdateType, int errCode)
    {
        if (authUpdateType == AuthUpdateType.Error || authUpdateType == AuthUpdateType.UserDenial)
            return;

        //Debug.Log("Login Updated, refetching user data");
        //await app.db.OnUpdateAuth();
    }

    private void OnBrowserClosed()
    {
#if !UNITY_EDITOR
        if (Screen.orientation == ScreenOrientation.Portrait)
            HomefulyExtensions.SetScreenOrientation(true);
#endif
    }

    void OnIABMessage(string jsMessage)
    {
        Debug.Log(">>>>[ jsMsg ]: " + jsMessage);
        IABResponse resp = new IABResponse(jsMessage);
        Debug.Log(">>>> resp");

        Debug.Log(">>>> After Return");
        if (resp.Err)
        {
            widget.Toast("Oops! Somethign went wrong. Please restart the app and try again.");
            InAppBrowser.CloseBrowser();
            return;
        }

        if (resp.Context == "loginDone")
        {
            // var iabData = resp.GetData<IABMUserDetails>();
            // app.account.CRED_JWTTOKEN = iabData.token;
            // app.account.CRED_USERID = iabData.id;
            // app.account.CRED_USEREMAIL = iabData.name;
            // app.account.CRED_ROLE = iabData.role;
            // app.account.CRED_ISDEMO = "Not Demo";
            // //AppComponent.Instance.Navigate(AppPage.DesignScene);
            // // Screen.orientation = ScreenOrientation.Landscape;
            // InAppBrowser.CloseBrowser();
            // getUserInputs();
        }
        else
        {
            InAppBrowser.CloseBrowser();
        }
    }

    private void OnApplicationQuit()
    {
        //Analytics: time spent in design
        Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, sceneType);
        Debug.Log("Printing application quit");
        
        OnSaveManual("Force Exit");
    }

    private void OnDisable()
    {

        Messenger<bool>.RemoveListener(StaticFunctionCalls.ToggleCamView, ToggleTopView);

        Messenger<string>.RemoveListener(StaticFunctionCalls.OnSaveDesign, OnSaveManual);

        //Messenger.RemoveListener(StaticFunctionCalls.MinimisePanel, OnMinimisePanel);
        //Messenger.RemoveListener(StaticFunctionCalls.MaximisePanel, OnMaximisePanel);

        Messenger<bool>.RemoveListener(StaticFunctionCalls.AssetDragFromStoreEvents, OnDragAssetFromStore);

        Messenger<IAsset>.RemoveListener(AppEvents.OnSwapCurrentAsset, OnSwapCurrentAsset);
        Messenger<IAsset, Vector3, GameObject>.RemoveListener(AppEvents.ASSET_DROPPED_INSIDE_ROOM, OnAssetDroppedInsideRoom_Wrapper);

        Messenger.RemoveListener(StaticFunctionCalls.ButtonDelete_Deprycated, ButtonDelete_Deprycated);
        Messenger.RemoveListener(StaticFunctionCalls.DeleteAsset, DeleteAsset_Wrapper);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonDistance, ButtonDistance);

        Messenger.RemoveListener(StaticFunctionCalls.ButtonRotate, OnUIBtnAssetRotate);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonHorizontal, OnUIBtnAssetMoveHorizontal);
        Messenger.RemoveListener(StaticFunctionCalls.ButtonVertical, OnUIBtnAssetMoveVertical);

        Messenger.RemoveListener(StaticFunctionCalls.DoDeselectAsset_Deprycated, DoDeselectAsset);

        TouchManEvents.OnTap -= OnScreenTap;
        SelectorMain.OnClickPurchase -= OnUpdateTransaction;
        HomefulyEvents.OnAuthUpdated -= OnAuthUpdated;

        Messenger<IWallfinish>.RemoveListener(StaticFunctionCalls.OnUpdateTransaction, OnUpdateTransaction_Wallfinish);


        //Messenger<GameObject>.RemoveListener(AppEvents.ASSET_TAPPED, ON_ASSET_TAPPED);
        //Messenger<GameObject>.RemoveListener(StaticFunctionCalls.onGameObjectTapped, OnGameobjectTapped);

        Messenger.RemoveListener(StaticFunctionCalls.TriggerPositionSave, AddRenderPosition);

        Messenger<int>.RemoveListener(StaticFunctionCalls.RelocatePlayer, SpawnActorTransform);
        Messenger<int>.RemoveListener(StaticFunctionCalls.DeleteViewpoint, DeleteViewpoint);

        // Messenger<Vector3>.RemoveListener(StaticFunctionCalls.MovePlayerThroughPortal, MovePlayerThroughPortal);

#region WallposterEvents
        //Messenger<Transform, Texture>.RemoveListener(StaticFunctionCalls.WallposterDroppedOnWall, WallposterDroppedOnWall);
        Messenger<Transform, Texture>.RemoveListener(StaticFunctionCalls.WallposterSelected, WallposterSelected);
        Messenger<Transform, Texture>.RemoveListener(StaticFunctionCalls.WallposterLongPressed_Deprycated, WallposterLongPressed);
        Messenger.RemoveListener(StaticFunctionCalls.WallpaperDeselected, WallpaperDeselect);
        Messenger.RemoveListener(StaticFunctionCalls.WallpaperDeleted, DeleteSelectedWallpaper);
        //Messenger.RemoveListener(StaticFunctionCalls.DeselectAsset, DeselectAsset);
#endregion

#region Wallfinish
        Messenger<IWallfinish, GameObject>.RemoveListener(StaticFunctionCalls.DropWallfinishInScene, OnWallfinishDroppedOnScene);
#endregion

#region Tutorial
        Messenger<bool>.RemoveListener(StaticFunctionCalls.TutorialRunning, TutorialChecker);
#endregion

        //#region dummy 
        //Messenger.RemoveListener(StaticFunctionCalls.MaximiseMenuPanel, Dummy);
        //#endregion

        bridge.onJSCallback.RemoveListener(OnIABMessage);
        bridge.onBrowserClosed.RemoveListener(OnBrowserClosed);
    }


#endregion

    private void MovePlayerThroughPortal(Vector3 pos)
    {
        if (scrPlayerFPS == null) { Debug.LogWarning("Skipping Move Becuase Player Doesnt Exist"); return; }
        var playerPos = scrPlayerFPS.transform.position;
        playerPos.x = pos.x;
        playerPos.z = pos.z;
        scrPlayerFPS.SpawnActorPosition(playerPos);
    }


#region Wallfinish

    private void OnWallfinishDroppedOnScene(IWallfinish wallfinish, GameObject droppedOn)
    {
        if (droppedOn.tag != "Wall") return;

        var queryRes = app.db.sceneWallfinishs.FirstOrDefault(x => x.Value.wall == droppedOn.name && x.Value.wallfinishData.category == wallfinish.category);
        if (queryRes.Value != null)
        {
            if (queryRes.Value.objRef)
                Destroy(queryRes.Value.objRef);
            app.db.DeleteSceneWallfinishEntry(queryRes.Key);
        }

        if (wallfinish.wftype.ToEnum<wfType>() == wfType.wallpaper)
        {
            WallposterDroppedOnWall(droppedOn.transform, wallfinish);
        }
        else if (wallfinish.wftype.ToEnum<wfType>() == wfType.paint && ColorUtility.TryParseHtmlString(wallfinish.code, out Color paintColor))
        {
            //droppedOn.transform.DestroyChildren(); Donot Delete Children, We need 'em

            Material wallPaint = new Material(Shader.Find("Standard"))
            {
                color = paintColor//new Color(1, 1, 1)
            };
            wallPaint.SetFloat("_Glossiness", 0f);
            //wallPaint.SetTexture("_MainTex", tex);
            //Material[] mats = Wall.GetComponent<Renderer>().materials;
            /*for (int i = 0; i < mats.Length; i++)
            {
                mats[i] = wallPaint;
            }*/
            foreach (Material mat in droppedOn.GetComponent<Renderer>().materials)
                Destroy(mat);
            droppedOn.GetComponent<Renderer>().material = wallPaint;
            //Material[] mat = droppedOn.transform.GetComponent<MeshRenderer>().materials;
            /*foreach (Material material in mat)
            {
                material.color = paintColor;
            }*/
            GameObject paint;
            Transform paintTrans = droppedOn.transform.Find("paint");
            if (paintTrans != null) Destroy(paintTrans.gameObject);
            paint = new GameObject("paint");
            paint.transform.SetParent(droppedOn.transform);
            GameObject paintID = new GameObject(wallfinish._id);
            paintID.transform.SetParent(paint.transform);

            app.db.AddNewWallfinishEntry(new IWallfinishEntry()
            {
                wall = droppedOn.name,
                wallfinish = wallfinish._id,
                entryId = HomefulyExtensions.randomIdGenerator().ToString(),
                wallfinishData = wallfinish,
                adjust = new IWallfinishAdjust(),
                objRef = null
            });

        }
        else if (wallfinish.category.ToEnum<WallfinishCategory>() == WallfinishCategory.texture)
        {
            Debug.Log("given is a texture");
        }
        else
        {
            Debug.Log("givedsdn is a " + wallfinish.category + " i.e" + wallfinish.category.ToEnum<WallfinishCategory>().ToString() + " hexxie: " + wallfinish.code);
        }
    }


    async Task OnLoadApplyWallfinish(IWallfinishEntry wallfinishData)
    {
        var t = roomGo.transform.Find(wallfinishData.wall);
        if (t != null && t.GetComponent<MeshRenderer>())
        {
            var dataRes = await app.server.artifact.GetWallPaintCode(new IPaintStoreElement_Deprycated(wallfinishData.wallfinish)); //new IPaintStoreElement(paintData.wallfinish));//paintStoreList[paintData.wallfinish]);
            if (dataRes == null) return;

            if (dataRes.category.ToEnum<WallfinishCategory>() == WallfinishCategory.wallpaper)
            {
                Debug.Log("Recieved Wallposter req, " + wallfinishData.wallfinishData.name + " id: " + wallfinishData.wallfinishData._id);

                var res = await app.server.artifact.GetWallpaperTexture(wallfinishData.wallfinish);
                if (res == null) return;


                res.name = wallfinishData.wallfinish + "_" + HomefulyExtensions.randomIdGenerator().ToString();

                app.db.AddNewWallfinishEntry(new IWallfinishEntry()
                {
                    wall = wallfinishData.wall,
                    wallfinish = wallfinishData.wallfinish,
                    entryId = HomefulyExtensions.randomIdGenerator().ToString(),
                    wallfinishData = dataRes,
                    adjust = wallfinishData.adjust,
                    objRef = DuringLoadSpawnWallpaper(roomGo.transform.Find(wallfinishData.wall), (Texture2D)res, new ITransform()
                    {
                        position = wallfinishData.adjust.offset,
                        scale = wallfinishData.adjust.scale
                    })
                });

            }
            else
            {
                app.db.AddNewWallfinishEntry(new IWallfinishEntry()
                {
                    wall = wallfinishData.wall,
                    wallfinish = wallfinishData.wallfinish,
                    entryId = HomefulyExtensions.randomIdGenerator().ToString(),
                    wallfinishData = dataRes,
                    adjust = wallfinishData.adjust,
                    objRef = null
                });


                dataRes.name = wallfinishData.wallfinish;
                t.DestroyChildren();

                GameObject paint = new GameObject("paint");
                paint.transform.SetParent(t);
                GameObject paintID = new GameObject(dataRes.name);
                paintID.transform.SetParent(paint.transform);
                ColorUtility.TryParseHtmlString(dataRes.code, out Color color);
                Material[] mat = t.GetComponent<MeshRenderer>().materials;

                foreach (Material ma in mat)
                {
                    ma.color = color;
                    ma.mainTexture = null;
                }
                t.GetComponent<MeshRenderer>().materials = mat;
            }
        }
        else Debug.LogWarning("[Paint Applier Loader] Unable to find wall named " + wallfinishData.wall + " skipping 1 paint data");
    }


#endregion

    //Move This Region To A Seperate Class later-on
#region WallPaper

    private async void WallposterLongPressed(Transform wall, Texture wallpaper)
    {
        if (selectedWall == null || selectedWall != wall.gameObject) await ToggleWallPosterEditMode(wall, true);
        DeselectAllPosterOfWall(selectedWall);

        foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
        {
            if (c.gameObject.name.Contains(wallpaper.name))
            {
                c.isSelected = true;
                c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Selected);
                break;
            }
        }

        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, true);
    }


    private void WallposterSelected(Transform wall, Texture wallpaper)
    {
        if (selectedWall == null || selectedWall != wall.gameObject) return;
        DeselectAllPosterOfWall(selectedWall);

        foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
        {
            if (c.gameObject.name.Contains(wallpaper.name))
            {
                c.isSelected = true;
                c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Selected);
                break;
            }
        }

        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, true);
    }
    private async void WallpaperDeselect()
    {
        await ToggleWallPosterEditMode(null, false);
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, false);
    }
    private void DeleteSelectedWallpaper()
    {
        if (selectedWall != null)
        {
            foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
            {
                if (c.isSelected == true)
                {
                    WallpaperDeselect(); Destroy(c.gameObject);
                    app.db.DeleteSceneWallfinishEntry(c.gameObject); return;
                }
            }
        }
        Debug.LogWarning("Nothing to delete");
    }

#region Internal
    private async Task ToggleWallPosterEditMode(Transform wall, bool enable)
    {
        var rBody = ComFPS.GetComponent<Rigidbody>();
        if (enable)
        {
            if (wall == null) { Debug.LogWarning("Given Wall Is Null"); return; }

            if (app.db.IsCurrentObjectValid<IAssetEntry>())
            {
                DoDeselectAsset();
            }

            int wallEditCullMask = (1 << LayerMask.NameToLayer("WallEditMode"));
            if (selectedWall != null)
            {
                if (selectedWall == wall.gameObject)
                {
                    Debug.Log("wall already selected, Skipping");
                    Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
                    return;
                }
                Debug.Log("not Currently Sleected wall,  s wall: " + selectedWall.name + " lol wall" + wall.gameObject.name);
                await ChangeSceneState(DesignSceneState.Navigate);
            }
            selectedWall = wall.gameObject;
            selectedWall.layer = 19; //"WallEditMode"
            foreach (var child in selectedWall.GetComponentsInChildren<Transform>())
            {
                if (child.tag == "Poster")
                    child.gameObject.layer = 19; /*"WallEditMode"*/
            }

            prevTransform = new ITransform(ComFPS.transform);
            rBody.collisionDetectionMode = CollisionDetectionMode.Discrete;
            rBody.isKinematic = true;
            ComFPS.GetComponentInChildren<Camera>().cullingMask = wallEditCullMask;
            //ComFPS.GetComponentInChildren<StandardLayer>().LayerMask = wallEditCullMask;

            await ChangeSceneState(DesignSceneState.WallEdit);
            Messenger<Transform>.Broadcast(StaticFunctionCalls.PlayerFPSLookAt, wall);
            Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
            //Messenger<bool>.Broadcast(StaticFunctionCalls.HideMiniMapandUI, true);
        }
        else
        {
            if (selectedWall == null) { Debug.LogWarning("What should i deselect?"); return; }

            foreach (var child in selectedWall.GetComponentsInChildren<Transform>())
            {
                if (child.tag == "Poster") { child.gameObject.layer = 20; /*"Wallpaper"*/ }
                else { child.gameObject.layer = 0; /*"Default"*/ }
            }
            selectedWall.layer = 17; //"Wall"
            foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
            {
                c.isSelected = false;
                c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Idle);
            }

            rBody.isKinematic = false;
            rBody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            //ComFPS.GetComponentInChildren<StandardLayer>().LayerMask = originalTouchGestureLayerMask;
            ComFPS.GetComponent<Player69FPS>().SetActorTransform(prevTransform);
            ComFPS.GetComponentInChildren<Camera>().cullingMask = originalCulMask;

            Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, false);
            //Messenger<bool>.Broadcast(StaticFunctionCalls.HideMiniMapandUI, false);
            selectedWall = null;
            await ChangeSceneState(DesignSceneState.Navigate);
        }
    }
    private async void WallposterDroppedOnWall(Transform wall, IWallfinish wallposter)
    {
        if (selectedWall != null && selectedWall != wall) await ToggleWallPosterEditMode(null, false);

        await ToggleWallPosterEditMode(wall, true);

        DeselectAllPosterOfWall(selectedWall);

        var go = SpawnWallPoster(wall, (Texture2D)wallposter.texture);
        foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
        {
            if (go == c.gameObject)
            {
                c.isSelected = true;
                c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Selected);
                break;
            }
        }

        app.db.AddNewWallfinishEntry(new IWallfinishEntry()
        {
            wall = wall.name,
            wallfinish = wallposter._id,
            entryId = HomefulyExtensions.randomIdGenerator().ToString(),
            wallfinishData = wallposter,
            adjust = new IWallfinishAdjust(),
            objRef = go
        });

        return;
    }

    public GameObject SpawnWallPoster(Transform wall, Texture2D wallposter)
    {
        Vector3 pos, scale;

        GameObject go = Instantiate(prefWallPoster, wall.transform);
        go.layer = 19; //"WallEditLayer"

        go.tag = "Poster";
        go.name = "Poster_" + wallposter.name;
        float normalizedY = ((Vector3)wall.GetCenter()).y;
        pos = wall.GetPosAlignedTransformSurface(scrPlayerFPS.transform.position);
        pos.y = normalizedY;

        scale = wall.GetRealScale();
        go.transform.eulerAngles = wall.GetPosAlignedRotationEuler(scrPlayerFPS.transform.position);   //LookAt(lookPoint);
        go.transform.position = pos;
        go.transform.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Standard"));
        go.transform.GetComponent<MeshRenderer>().material.mainTexture = wallposter;

        Messenger<GameObject>.Broadcast(StaticFunctionCalls.GetWallPosterisOn, wall.gameObject);
        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWallpaperWorld_Deprycated, true);

        go.transform.localScale = new Vector3(Math.Abs((float)wallposter.width / (float)wallposter.height) * 2, 2, 1);
        go.GetComponent<PosterStateManager>().isSelected = true;
        go.GetComponent<PosterStateManager>().Init();
        go.GetComponent<PosterStateManager>().ClampToWall();

        return go;
    }
    public GameObject DuringLoadSpawnWallpaper(Transform wall, Texture2D wallposter, ITransform trans)
    {
        if (!wall)
        { Debug.LogError("[DuringLoadSpawnWallpaper]Wall not Found..!"); return null; }

        GameObject go = Instantiate(prefWallPoster, wall.transform);
        go.layer = 20; //"Wallpaper"
        go.tag = "Poster";
        go.name = "Poster_" + wallposter.name;

        go.transform.eulerAngles = wall.GetPosAlignedRotationEuler(trans.position);

        go.transform.position = trans.position;
        go.transform.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Standard"));
        go.transform.GetComponent<MeshRenderer>().material.mainTexture = wallposter;

        go.transform.localScale = trans.scale;
        Messenger<GameObject>.Broadcast(StaticFunctionCalls.GetWallPosterisOn, wall.gameObject);
        go.GetComponent<PosterStateManager>().Init();
        go.GetComponent<PosterStateManager>().ClampToWall();

        return go;
    }

    async Task LoadAndApplyWallpaper_Deprycated(IWallfinishEntry paintData)
    {
        if (roomGo.transform.Find(paintData.wall) != null)
        {
            var wallfinishData = await app.server.artifact.GetWallfinishData(paintData.wallfinish);
            if (wallfinishData == null) return;

            var res = await app.server.artifact.GetWallpaperTexture_Deprycated(wallfinishData);
            if (res == null) return;

            res.texture.name = paintData.wallfinish + "_" + HomefulyExtensions.randomIdGenerator().ToString();

            DuringLoadSpawnWallpaper(roomGo.transform.Find(paintData.wall), (Texture2D)res.texture, new ITransform()
            {
                position = paintData.adjust.offset,
                scale = paintData.adjust.scale
            });
        }
        else Debug.LogWarning("[Wallposter Loader] Unable to find wall named " + paintData.wall + " skipping 1 paint data");
    }

    void DeselectAllPosterOfWall(GameObject wall)
    {
        foreach (var c in wall.GetComponentsInChildren<PosterStateManager>())
        {
            c.isSelected = false;
            c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Idle);
        }
    }
#endregion
#endregion

#region Viewpoints

    private void DeleteViewpoint(int obj)
    {
        //throw new NotImplementedException();
        //Debug.Log("it works, now reassign numbers to buttons "+ viewPointsList.Count+"\n"+viewPointsList[obj].position);
        viewPointsList.RemoveAt(obj);
        Messenger<int>.Broadcast(StaticFunctionCalls.TransferRenderPositionIndex, viewPointsList.Count);
    }

    private void SpawnActorTransform(int index)
    {
        ComFPS.GetComponent<Player69FPS>().SpawnActorTransform(viewPointsList[index]);
    }

    /*
     foreach (ITransform renderPosition in roomDesign.viewPoints)
   {
       ITransformIndexed item = new ITransformIndexed
       {
           i = roomDesign.viewPoints.IndexOf(renderPosition),
           itransform = renderPosition
       };
       Messenger<ITransformIndexed>.Broadcast(StaticFunctionCalls.TransferRenderPositionIndex, item);
       Debug.Log("Sending data of index " + item.i);
   }*/



    void RefreshViewPoints(List<ITransform> data)
    {
        viewPointsList.Clear();
        viewPointsList = data;

        Messenger<int>.Broadcast(StaticFunctionCalls.TransferRenderPositionIndex, viewPointsList.Count);
        //foreach (ITransform elem in viewPointsList)
        //{
        //Debug.Log("Metadata Added Data list at index " + viewPointsList.IndexOf(elem));
        //    Messenger<int>.Broadcast(StaticFunctionCalls.TransferRenderPositionIndex, viewPointsList.IndexOf(elem));
        //cannot teleport to choosen location

        //Debug.Log("SOS - adding "+data.Count+" viewpoints");
        if (data.Count == 0) Debug.LogWarning("No previously saved viewpoints found");
    }
    private void AddRenderPosition()
    {
        //Debug.Log("Adding render position to database 1/5");
        if (viewPointsList == null)
        {
            viewPointsList = new List<ITransform>();
            Debug.Log("Making new list");
        }
        ITransform item = new ITransform()
        {
            position = ComFPS.transform.position,
            rotation = ComFPS.transform.eulerAngles,
            scale = ComFPS.transform.localScale
        };
        //Check if number of viewpoints is greater than limit
        if (viewPointsList.Count > 9)
        {
            widget.Alert("Out of viewpoint slots", "Cant save more than 10 viewpoints, please contact team homefuly");
            //Debug.LogError("Cant save more than 10 viewpoints, please contact team homefuly");
            return;
        }
        //check for duplicate viewpoints
        foreach (ITransform i in viewPointsList)
        {
            if (i.position == item.position)
                if (i.rotation == item.rotation)
                {
                    widget.Alert("Cannot Add View Point", "This viewpoint already exists as View " + (viewPointsList.IndexOf(i) + 1));
                    return;
                }
        }
        viewPointsList.Add(item);
        //Debug.Log("2/5 Added " + item.position + " to Data list at index "+ viewPointsList.IndexOf(item));

        Messenger<int>.Broadcast(StaticFunctionCalls.TransferRenderPositionIndex, viewPointsList.Count);
    }

#endregion

    void Init()
    {
        app = AppComponent.Instance;
        app.CheckInit();


        artifactManager = new ArtifactManager(app.server);
        widget = WidgetManager.Instance;


        CURRENT_ROOM = app.account.CURRENT_ROOM_ID;
        CURRENT_ROOM_NAME = app.account.CURRENT_ROOM_NAME;
        CURRENT_VERSION_ID = app.account.CURRENT_VERSION_ID;

        //floorplanData = new IFloorPlanProcessor { lightHalo = lightHalo, matLight = matLight, doorPortal = elemDoorPortal, segmentMaterial = segmentMaterial };


        preCam.SetActive(true);
        preCam.transform.GetComponent<Camera>().cullingMask = ~LayerMask.GetMask("MinimapFloorplan");

    }



    void UpdateNavigation()
    {
        scrPlayerFPS.UpdateMovement(!isStoreOpen && /*!isAssetSelected &&*/ (currentState != DesignSceneState.WallEdit));
    }

#region Listners

    private void ButtonDelete_Deprycated()
    {
        if (app.db.IsCurrentObjectValid<IAssetEntry>() && (!app.db.legallyOwnedObjects.ContainsKey((app.db.currentObject as IAssetEntry).asset._id) && !app.db.userSavedData.assetsOwned_Indexed.ContainsKey((app.db.currentObject as IAssetEntry).asset._id)))
        {
            totalUnboughtAssets--;
        }
        Debug.LogWarning("[Deprycation Warning] ButtonDelete_Deprycated, Use DeleteAsset_Wrapper() instead");
        DeleteAsset(false);
    }

    private void DeleteAsset_Wrapper()
    {
        if (app.db.IsCurrentObjectValid<IAssetEntry>() && (!app.db.legallyOwnedObjects.ContainsKey((app.db.currentObject as IAssetEntry).asset._id) && !app.db.userSavedData.assetsOwned_Indexed.ContainsKey((app.db.currentObject as IAssetEntry).asset._id)))
        {
            totalUnboughtAssets--;
        }
        DeleteAsset(false);
    }

    void DeleteAsset(bool isBeingSwapped = false)
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }
        var asset = app.db.currentObject as IAssetEntry;
        DoDeselectAsset();
        animationMan.AnimateAndDestroy(asset, isBeingSwapped);

        //this code may be heavy on script
        app.db.DeleteSceneAssetEntry(asset.entryId);

        Messenger<bool>.Broadcast(StaticFunctionCalls.OnAssetDeleted, isBeingSwapped);

    }

    public void DeleteAssetAfterAnimation(IAssetEntry asset, bool isBeingSwapped)
    {
        DoDeselectAsset();
        //this code may be heavy on script
        app.db.DeleteSceneAssetEntry(asset.entryId);

        Messenger<bool>.Broadcast(StaticFunctionCalls.OnAssetDeleted, isBeingSwapped);
    }

    void ButtonDistance()
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }

        var av = (app.db.currentObject as IAssetEntry).objRef.GetComponent<AssetVisuals_Deprycated>();
        av.drawCues = !av.drawCues;
        Debug.Log("Setting drawcue to" + av.drawCues);
    }

    private void OnUpdateTransaction_Wallfinish(IWallfinish wallfinish)
    {
        if (CoinManager.Instance.DoTransaction(wallfinish))
        {
            Messenger<float>.Broadcast(StaticFunctionCalls.Action_CoinUpdated, app.db.userSavedData.currentCoins);
        }
        else
        {
            string errMsg = "[" + nameof(SceneManagerDesigner) + "][" + nameof(OnUpdateTransaction) + "] Sorry Transaction Cannot Happen Due to: Transcation Failed | User Denial | Invalid SceneType | Invalid UniquID";
            Debug.LogWarning(errMsg);

        }
    }

    /// <summary>
    /// Checks the Transactions Validity and if valid requests <see cref="CoinManager"/> to do the transaction
    /// and updates UI accordinly
    /// </summary>
    /// <param name="acceptPurchase"></param>
    /// <param name="uniqueID"></param>
    private void OnUpdateTransaction(bool acceptPurchase, string uniqueID)
    {
        if (AppConfig.GameConfig.creativeMode)
        {
            Debug.LogWarning("[" + nameof(SceneManagerDesigner) + "][" + nameof(OnUpdateTransaction) + "] Creative mode enabled, You no longer need to buy the assets");
            return;
        }

        if (acceptPurchase && app.db.sceneAssets.ContainsKey(uniqueID) && app.db.sceneType == SceneType.UserRoom)
        {
            if (CoinManager.Instance.DoTransaction(app.db.sceneAssets[uniqueID].asset))
            {
                totalUnboughtAssets--;
                app.db.sceneAssets[uniqueID].objRef.GetComponentInChildren<SelectorMain>().UpdatePurchasableMode(false);
                Messenger<float>.Broadcast(StaticFunctionCalls.Action_CoinUpdated, app.db.userSavedData.currentCoins);
                animationMan.CelebratePurchase(app.db.sceneAssets[uniqueID].objRef);
                app.db.isDesignDirty = true;
            }
            else
            {
                OnUpdateTransaction(false, string.Empty);
            }

        }
        else
        {
            if (!app.db.sceneAssets.ContainsKey(uniqueID))
                Broadcast_OnActionFailed(ActionType.Transaction, (int)ErrorTracking.ErrorCodes.Invalid_ID);
            else
                app.db.currentObject = app.db.sceneAssets[uniqueID];

            totalUnboughtAssets--;
            DeleteAsset();

            string errMsg = "[" + nameof(SceneManagerDesigner) + "][" + nameof(OnUpdateTransaction) + "] Sorry Transaction Cannot Happen Due to: Transcation Failed | User Denial | Invalid SceneType | Invalid UniquID";
            Debug.LogWarning(errMsg);


        }
    }

    private async void OnScreenTap(GameObject tappedObject)
    {
        if (isTutorial)
        {
            Debug.Log("Tutorial is Running cannot select asset");
            return;
        }
        if (tappedObject.layer == LayerMask.NameToLayer("Selector") && app.db.sceneType == SceneType.UserRoom)
        {
            if (tappedObject.GetComponent<Purchasable>())
                tappedObject.GetComponent<Purchasable>().UpdatePurchase();
        }
        else if (app.db.IsCurrentObjectValid<IAssetEntry>() && (tappedObject == null || (app.db.currentObject as IAssetEntry).objRef == tappedObject))
        {
            DeselectAsset();
            await ChangeSceneState(DesignSceneState.AssetDeselected);
        }
        else if (tappedObject == null)
            return;
        else if (tappedObject.tag == "Asset" && !selectedWall)
        {

            if (app.db.IsCurrentObjectValid<IAssetEntry>())
                DeselectAsset();

            app.db.currentObject = app.db.sceneAssets[tappedObject.name];
            SelectAsset();
            await ChangeSceneState(DesignSceneState.AssetSelected);
            return;
        }
        else if (tappedObject.tag == "Poster")
        {
            if (selectedWall != null && selectedWall == tappedObject.transform.parent)
            {
                GameObject selectedChild = null;
                foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
                {
                    if (c.gameObject.name == tappedObject.name && c.isSelected == false)
                    {
                        selectedChild = c.gameObject;
                    }
                }
                if (selectedChild != null)
                {
                    Debug.Log("Selected Wallpaper " + selectedChild.name);
                    foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
                    {
                        if (c.gameObject == selectedChild)
                            c.GetComponent<PosterStateManager>().isSelected = true;
                        else
                        {
                            c.GetComponent<PosterStateManager>().isSelected = false;
                            c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Idle);
                        }
                    }
                }
            }
        }
        else if (app.db.IsCurrentObjectValid<IAssetEntry>()) //WHen tapped on anything else other than poster and asset
        {
            DeselectAsset();
            await ChangeSceneState(DesignSceneState.AssetDeselected);
        }
    }

    void DeselectAsset()
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) return;

        BeIdleMove();
        //EnableVerticalCam(false);

        var go = (app.db.currentObject as IAssetEntry).objRef;

        if (go.transform.position != currentAssetPos || go.transform.rotation.eulerAngles != currentAssetRot)
            app.db.isDesignDirty = true;

        go.transform.GetComponentInChildren<SelectorMain>().CloseAssetSelection();
        go.GetComponent<Collider>().isTrigger = true;
        go.GetComponent<AssetTransformManager>().UpdateIsAssetSelected(false);

        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, false);

        var av = go.GetComponent<AssetVisuals_Deprycated>();


        go.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Discrete;
        go.GetComponent<Rigidbody>().isKinematic = true;
        if (av.drawCues)
        {
            av.drawCues = false;
            av.DrawDirectionCue();
        }
        av.enabled = false;

        app.db.currentObject = null;

        Messenger.Broadcast(StaticFunctionCalls.OnAssetDeselected);
    }


    void SelectAsset()
    {


        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }

        Messenger<bool>.Broadcast(StaticFunctionCalls.ToggleLocalWorld_Deprycated, true);
        var go = (app.db.currentObject as IAssetEntry).objRef;

        go.GetComponent<Collider>().isTrigger = false;
        go.transform.GetComponentInChildren<SelectorMain>().SelectAsset();
        go.GetComponent<AssetVisuals_Deprycated>().enabled = true;
        go.GetComponent<AssetTransformManager>().UpdateIsAssetSelected(true);

        go.GetComponent<Rigidbody>().isKinematic = false;
        go.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        currentAssetPos = go.transform.position;
        currentAssetRot = go.transform.rotation.eulerAngles;

        Messenger<IAssetEntry>.Broadcast(StaticFunctionCalls.OnAssetSelected, app.db.currentObject as IAssetEntry);

        //OnUIBtnAssetMoveHorizontal();
        //widget.Toast("Selected " + go.GetComponent<AssetTransformManager>().entry.asset.name, 1);

    }

    async void OnDragAssetFromStore(bool isBeingDragged)
    {
        await ChangeSceneState(isBeingDragged ? DesignSceneState.StoreOpen : DesignSceneState.StoreClose);
    }

    async void DoDeselectAsset()
    {
        DeselectAsset();
        await ChangeSceneState(DesignSceneState.AssetDeselected);
    }


#region UI

    async void OnSwapCurrentAsset(IAsset newAsset)
    {
        var aOwned = app.db.userSavedData.assetsOwned_Indexed;
        if (!AppConfig.GameConfig.creativeMode && !aOwned.ContainsKey(newAsset._id) && app.db.sceneType == SceneType.UserRoom && totalUnboughtAssets >= maxAllowedUnboughtAssetsAllowed)
        {
            widget.Alert("", "You can only load " + maxAllowedUnboughtAssetsAllowed + " products at once, please buy or remove one of them to continue");
            Broadcast_OnActionFailed(ActionType.Swap, (int)ErrorTracking.ErrorCodes.TooMany_UnboughtAssetsInScene);
            return;
        }

        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) return;

        var tar = (app.db.currentObject as IAssetEntry).objRef.transform.position;
        tar.y = scrPlayerFPS.transform.position.y;

        if (!isCamVertical)
            scrPlayerFPS.transform.DOLookAt(tar, 0.5f, AxisConstraint.Y);

        IAssetEntry prevasset = /*HomefulyExtensions.DeepCopy<IAssetEntry>(*/app.db.currentObject as IAssetEntry/*)*/;
        prevasset.transform = new ITransform(prevasset.objRef.transform);


        if (!AppConfig.GameConfig.creativeMode && app.db.sceneType == SceneType.UserRoom && !aOwned.ContainsKey(prevasset.asset._id)) //ie. is asset unbought?
            totalUnboughtAssets--;

        DeleteAsset(true);

        var entryId = await OnAssetLoadInsideRoom(newAsset, prevasset.transform.position, null, prevasset.transform.rotation);

        if (!string.IsNullOrEmpty(entryId) && app.db.sceneAssets.ContainsKey(entryId))
        {
            app.db.currentObject = app.db.sceneAssets[entryId];
            SelectAsset();
            Broadcast_ActionAssetSwapped(app.db.sceneAssets[entryId]);

            await ChangeSceneState(DesignSceneState.AssetSelected);
        }
        else
            Debug.LogError("Failed to Fetch just spawned asset data");
    }

    async void OnAssetDroppedInsideRoom_Wrapper(IAsset assetData, Vector3 pos, GameObject go)
    {
        var res = await OnAssetLoadInsideRoom(assetData, pos, go);
        if (res != string.Empty)
            Broadcast_ActionAssetDropped(res);

    }

    async Task<string> OnAssetLoadInsideRoom(IAsset assetData, Vector3 pos, GameObject go, Vector3? rot = null)
    {
        var aOwned = app.db.userSavedData.assetsOwned_Indexed;

        if (!AppConfig.GameConfig.creativeMode && !aOwned.ContainsKey(assetData._id) && app.db.sceneType == SceneType.UserRoom && totalUnboughtAssets >= maxAllowedUnboughtAssetsAllowed)
        {
            widget.Alert("", "You can only load " + maxAllowedUnboughtAssetsAllowed + " products at once, please buy or remove one of them to continue");
            Broadcast_OnActionFailed(ActionType.ProductLoad, (int)ErrorTracking.ErrorCodes.TooMany_UnboughtAssetsInScene);
            return string.Empty;
        }

        widget.LoaderMini("Loading " + assetData.name + "..", 3000);
        isAssetLoading = true;

        var rotation = Vector3.zero;
        if (rot != null)
            rotation = (Vector3)rot;

        var artRes = await LoadAssetAsync(assetData, assetsContainer, pos, rotation);

        if (artRes.err || artRes.gameObject == null)
        {
            widget.Alert("Error Loading asset!", artRes.data);

            widget.LoaderMiniClose();
            Broadcast_OnActionFailed(ActionType.ProductLoad, (int)ErrorTracking.ErrorCodes.Failed_GameobjectLoad);
            return string.Empty;

        }

        var sMode = SelectorMain.SelectorMode.Normal;
        if (!AppConfig.GameConfig.creativeMode && app.db.sceneType == SceneType.UserRoom && !(aOwned.ContainsKey(assetData._id)))
        {
            sMode = SelectorMain.SelectorMode.Purchase;
            totalUnboughtAssets++;
        }

        /* Warning: Unsafe type cast
         * */
        AttachSelector(artRes.gameObject.transform, sMode, artRes.gameObject.name, (int)assetData.price.customer);

        app.db.AddSceneAssetEntry(new IAssetEntry
        {
            entryId = artRes.gameObject.name,
            inuse = true,
            objRef = artRes.gameObject,
            transform = new ITransform(artRes.gameObject.transform),
            asset = assetData
        });

        isAssetLoading = false;
        // isDesignDirty = true;

        widget.LoaderMiniClose();

        app.db.currentObject = app.db.sceneAssets[artRes.gameObject.name];
        SelectAsset();
        await ChangeSceneState(DesignSceneState.AssetSelected);

        return artRes.gameObject.name;
    }


    void BeIdleMove()
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }

        //EnableVerticalCam(false);
        AssetTransformManager atm = (app.db.currentObject as IAssetEntry).objRef.GetComponent<AssetTransformManager>();
        atm.SwitchAssetTransformMode(AssetTransformMode.idle);
    }

    private void OnUIBtnAssetMoveVertical()
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }

        //EnableVerticalCam(false);
        AssetTransformManager atm = (app.db.currentObject as IAssetEntry).objRef.GetComponent<AssetTransformManager>();
        atm.SwitchAssetTransformMode(AssetTransformMode.translateVertical);

    }

    private void OnUIBtnAssetMoveHorizontal()
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }

        //EnableVerticalCam(true);
        AssetTransformManager atm = (app.db.currentObject as IAssetEntry).objRef.GetComponent<AssetTransformManager>();
        atm.SwitchAssetTransformMode(AssetTransformMode.translate);
    }

    private void OnUIBtnAssetRotate()
    {
        if (!app.db.IsCurrentObjectValid<IAssetEntry>()) { Debug.LogWarning("Invalid CurrentSelectedAsset"); return; }

        //EnableVerticalCam(false);
        AssetTransformManager atm = (app.db.currentObject as IAssetEntry).objRef.GetComponent<AssetTransformManager>();
        atm.SwitchAssetTransformMode(AssetTransformMode.rotate);
    }

#endregion

#endregion


#region Core

    /// <summary>
    /// Downloads and loads asset given the assetid
    /// </summary>
    /// <param name="assetId"></param>
    /// <param name="parent"></param>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <param name="original"></param>
    /// <returns></returns>
    async Task<IArtifactLoadAsyncOperationResult> LoadAssetAsync(IAsset asset, Transform parent, Vector3 position, Vector3 rotation, GameObject original = null)
    {
        var artRes = new IArtifactLoadAsyncOperationResult();

        if (asset == null) return artRes;

        artRes = await artifactManager.DownloadAsset(asset, parent, original);

        if (!artRes.err && artRes.gameObject != null)
        {
            // skip post processing if duplicated from existing asset
            if (original != null)
            {
                Destroy(artRes.gameObject.transform.Find("Selector(Clone)").gameObject);
            }
            //Setting updated At for asset cache SOS
            string str = "ASSET_UPDATE_CACHE_" + asset._id;
            PlayerPrefs.SetString(str, asset.updatedAt);
            //Debug.Log("Setting " + asset.updatedAt);    //SOSOS
            artifactManager.PostProcessAsset(artRes.gameObject.transform, position, rotation);

            var atm = artRes.gameObject.GetComponent<AssetTransformManager>();
            if (atm.entry == null)
                atm.entry = new IAssetEntry() { asset = new IAsset() };

            atm.entry.entryId = artRes.gameObject.name;
            
            atm.entry.asset = asset;
            atm.Init(app.db.sceneType == SceneType.UserRoom ? true : false);

        }

        return artRes;
    }


    /*public void Update()
    {
        if (isCamVertical && app.db.currentAsset != null)   //This is moving the camera based on asset, deprecated
        {
            var pos = (app.db.currentAsset as IAssetEntry).objRef.transform.position;
            pos.y = ComFPS.transform.position.y;
            pos.z += 0.85f;

            ComFPS.transform.position = pos;
        }
        //FpsControl();
    }*/
    void ToggleTopView(bool val)
    {
        int fpsCul = (1 << LayerMask.NameToLayer("TransparentFX") | LayerMask.NameToLayer("Ignore Raycast") | LayerMask.NameToLayer("UI") | LayerMask.NameToLayer("PostProcessing") | LayerMask.NameToLayer("Roof") | LayerMask.NameToLayer("Floor") | LayerMask.NameToLayer("RoofAddons") | LayerMask.NameToLayer("Asset") | LayerMask.NameToLayer("Default"));
        int verCul = (1 << LayerMask.NameToLayer("Floor") | LayerMask.NameToLayer("TransparentFX") | LayerMask.NameToLayer("Ignore Raycast") |/* LayerMask.NameToLayer("Roof") |*/ LayerMask.NameToLayer("UI") | LayerMask.NameToLayer("Asset"));

        Vector3 oldpos, oldRot;// = ComFPS.transform.position;

        if (val && !isCamVertical)
        {
            ComFPS.GetComponent<Rigidbody>().useGravity = false;
            ComFPS.GetComponent<Rigidbody>().isKinematic = false;
            fpsPos = ComFPS.transform.position;
            fpsRot = ComFPS.transform.rotation.eulerAngles;
            oldRot = fpsRot;
            Vector3 nPos = ComFPS.transform.position;//(app.db.currentAsset as IAssetEntry).objRef.transform.position;  //ComFPS.transform.position;//Vector3.zero;
            Vector3 nRot = Vector3.zero;

            //lastPosInNormalView = new ITransform(ComFPS.transform);
            nPos.x = roomBounds.center.x;
            nPos.z = roomBounds.center.z;

            nPos.y += 4;
            //nPos.z += 0.85f;
            nRot.x = 90;

            ComFPS.transform.position = nPos;
            ComFPS.transform.eulerAngles = nRot;
            ComFPS.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
            ComFPS.GetComponentInChildren<Camera>().cullingMask = originalCulMask & ~(1 << LayerMask.NameToLayer("Roof")) & ~(1 << LayerMask.NameToLayer("RoofAddons"));
            ComFPS.GetComponent<Player69FPS>().SwitchCameraMode(CameraMode.TopView);
            isCamVertical = true;
        }
        else if (!val && isCamVertical)
        {
            oldpos = fpsPos;
            //fpsRot.y = ComFPS.transform.eulerAngles.y;
            ComFPS.transform.eulerAngles = fpsRot;
            ComFPS.GetComponentInChildren<Camera>().cullingMask = originalCulMask;

            fpsPos = ComFPS.transform.position; //(app.db.currentAsset as IAssetEntry).objRef.transform.position;
            
            //Vector3 newpos;
            //newpos.z = oldpos.z;// * 0.3f;
            //newpos.x = oldpos.x;//* 0.3f;
            //newpos.y = oldpos.y;
            ComFPS.transform.position = oldpos;
            //ComFPS.transform.eulerAngles = oldRot;
            //ComFPS.transform.LookAt((app.db.currentAsset as IAssetEntry).objRef.transform);

            ComFPS.GetComponent<Rigidbody>().useGravity = true;
            ComFPS.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
            //Vector3 resetRot = ComFPS.transform.eulerAngles;
            //resetRot.x = 0;
            //resetRot.z = 0;
            //ComFPS.transform.eulerAngles = resetRot;
            ComFPS.GetComponent<Player69FPS>().SwitchCameraMode(CameraMode.FreeMove);

            isCamVertical = false;
        }
    }
#endregion

#region FSM Methods


    /// <summary>
    /// Initialises all the vars and gos
    /// </summary>
    /// <returns></returns>
    async Task FSM_Preload()
    {
        timer = new System.Diagnostics.Stopwatch();
        timer.Start();
        /* Check if the AppComponent is ready and initiated
         * */
        app = AppComponent.Instance;
        app.CheckInit();

        transform.GetOrAddComponent<SpawnCueManager>();

        /* Define all variables
         * */
        artifactManager = new ArtifactManager(app.server);
        CURRENT_ROOM = app.account.CURRENT_ROOM_ID;
        CURRENT_ROOM_NAME = app.account.CURRENT_ROOM_NAME;
        CURRENT_VERSION_ID = app.account.CURRENT_VERSION_ID;
        viewPointsList = new List<ITransform>();
        designDataCache = new IRoomDesign();

        /* Set Gameobjects Active State
         * */
        preCam.SetActive(true);
        ComFPS.SetActive(false);
        ComMiniCam.SetActive(false);

        if (!await app.db.RefreshDb(app.CurrentSceneType))
        {

            WidgetManager.Instance.Alert("Design Load Failed", "Unable to load the design at the moment, please try again after some time", () =>
            {
                WidgetManager.Instance.LoaderClose();
                app.Navigate(AppPage.AppComponent);
            });
        }

        HomefulyAnalyticsEvent.OnDesignOpened?.Invoke(app.CurrentSceneType);

        sceneType = app.db.sceneType;

        CancelToken = new CancellationTokenSource();
        CoinManager.Instance.Init();

        // Debug.LogWarning("<<<<<<<<<<< Current Scene Type: " + app.CurrentSceneType);

        await ChangeSceneState(DesignSceneState.DownloadMeta);


    }

    /// <summary>
    /// Fetches designs and loads asset and wallpaper data from store
    /// </summary>
    /// <returns></returns>
    async Task FSM_DownloadMeta()
    {
        widget.Loader("Loading, Please Wait...");

        /* Fetch All Designs In The Room
         * */
        var designList = await app.server.artifact.GetRoomDesignListAsync(CURRENT_ROOM);
        if (designList == null || designList.Count < 1)
        {
            Debug.LogError("No Designs Found! " + CURRENT_ROOM);
            widget.Alert("Design Error", "No Designs Found for this center.", () =>
            {
                app.Navigate(AppPage.AppComponent);
                return;
            });
        }
        string designId = designList[0]._id;
        designDataCache = designList[0];

        /* Fetch Design's JSON
         * */
        roomDesign = IRoomDesign.CreateCopy(app.db.CurrentDesign);

        loadProgressTotal = 10 + roomDesign.assets.Count;
        //widget.LoaderClose();
        widget.Loader("Loading, Please Wait...", 20);
        widget.LoaderProgressSlider.maxValue = loadProgressTotal;
        UpdateLoaderProgressValue(3);

        /* Setup required vars 
         * */
        RefreshViewPoints(roomDesign.viewPoints);
        UpdateLoaderProgressValue(4);
        await ChangeSceneState(DesignSceneState.DownloadArtifactRoom);
    }


    /// <summary>
    /// Downloads and Loads room
    /// </summary>
    /// <returns></returns>
    async Task FSM_DownloadArtifactRoom()
    {
        UpdateLoaderProgressValue(5, "Loading, Please Wait...");

        /* Downloads the room file
         * */
        var roomRes = await artifactManager.DownloadFloorplan(CURRENT_ROOM, roomParent.transform);

        if (roomRes.err || roomRes.gameObject == null)
        {
            await ChangeSceneState(DesignSceneState.DownloadRoomErr);
            return;
        }

        /* Load the room file
         * */
        roomGo = roomRes.gameObject;

        artifactManager.PostProcessFloorplan(roomGo.transform, floorplanData);
        app.db.SetCurrentRoom(roomGo);


        roomRes = null;

        if (!roomGo.GetComponent<BoxCollider>())
            roomGo.AddComponent<BoxCollider>();



       roomBounds = roomGo.transform.GetTotalRenderBounds();
        var center = roomBounds.center;
        lightAmbient.transform.position = new Vector3(center.x, roomBounds.max.y - 0.45f, center.z);



        RenderSettings.ambientLight = new Color32(0xBE, 0xBE, 0xBE, 0xBE); //ACACAC //BEBEBE //CCCCCCFF

        reflectionProbe.transform.position = roomBounds.center;
        reflectionProbe.GetComponent<ReflectionProbe>().size = roomBounds.size;
        CreateTrigger();
        ComFPS.SetActive(true);
        scrPlayerFPS.SpawnActorPosition(center/* roomDesign.camera.position*/, Vector3.zero /*roomDesign.camera.rotation*/);
        roomGo.GetComponent<BoxCollider>().enabled = false;

        if (roomDesign != null)
        {
            if (app.db.sceneType == SceneType.UserRoom)
            {
                //WIP: What if pos is out side the room, for eg in a L shaped room
                var pos = new Vector3(roomBounds.max.x - 0.4f, roomBounds.min.y + 0.2f, roomBounds.max.z - 0.4f);

                scrPlayerFPS.SpawnActorPosition(pos, new Vector3(0, -135, 0));
            }
            else if (roomDesign.viewpoint.Count > 0)
                scrPlayerFPS.SpawnActorPosition(roomDesign.viewpoint[0].transform.position, new Vector3(0, roomDesign.viewpoint[0].transform.rotation.y, 0));
            else
                Debug.LogWarning("[Camera Setup] No ViewPoint Exist");

            List<Task> paintLoadTasks = new List<Task>();
            foreach (var paintData in roomDesign.paint)
            {
                paintLoadTasks.Add(OnLoadApplyWallfinish(paintData));
            }
            await Task.WhenAll(paintLoadTasks);
        }
        else Debug.LogWarning("[Paint Loader] DesignData Not Available");

        UpdateLoaderProgressValue(6);
        GC.Collect();
        await Resources.UnloadUnusedAssets();
        await ChangeSceneState(DesignSceneState.DownloadArtifactAsset);
    }


    void AttachSelector(Transform trans, SelectorMain.SelectorMode mode, string uniqueID, int price)
    {
        var goSelector = Instantiate(prefSelector);
        // var purchasable = Instantiate(prefPurchasable);
        goSelector.transform.SetParent(trans);
        // purchasable.transform.SetParent(goSelector.transform);
        
        // goSelector.GetComponentInChildren<Canvas>().worldCamera = ComFPS.GetComponentInChildren<Camera>().transform.GetChild(0).GetComponent<Camera>();
        // purchasable.GetComponent<Canvas>().worldCamera = ComFPS.GetComponentInChildren<Camera>().transform.GetChild(0).GetComponent<Camera>();

        goSelector.GetComponent<SelectorMain>().Init(mode, uniqueID, price);
    }

    private void CreateTrigger() // Function that creates a trigger around the room, encapsulating it
    {

        Bounds a = roomGo.transform.GetTotalRenderBounds();
        a.Expand(new Vector3(1, 7, 1));
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = a.center;
        cube.transform.localScale = a.size;
        cube.transform.GetComponent<Renderer>().enabled = false;
        cube.GetComponent<BoxCollider>().isTrigger = true;
        cube.name = "OutofBoundChecker";
        cube.tag = "OutofBoundCheck";
        cube.gameObject.AddComponent<OutofBoundChecker>();

    }

    List<IAssetEntry> sceneassets; //= new List<IAssetEntry>();
    async Task LoadTimeAssetLoader(IAssetEntry entry)
    {
        if (!entry.inuse) return;
        IArtifactLoadAsyncOperationResult task;

        /* Downloads and loads asset given the assetid
         * */
        task = await LoadAssetAsync(entry.asset, assetsContainer, entry.transform.position, entry.transform.rotation);

        if (task.gameObject == null) return;

        sceneassets.Add(new IAssetEntry
        {
            entryId = task.gameObject.name,
            inuse = true,
            objRef = task.gameObject,
            transform = new ITransform(task.gameObject.transform),
            asset = entry.asset
        });

        AttachSelector(task.gameObject.transform, SelectorMain.SelectorMode.Normal, task.gameObject.name, (int)entry.asset.price.customer);

        task = null;
    }

    /// <summary>
    /// Downloads and loads all required assets to scene, parallely in batches
    /// </summary>
    /// <returns></returns>
    async Task FSM_DownloadArtifactAsset()
    {
        UpdateLoaderProgressValue(7, "Loading Products..");
        List<IArtifactLoadAsyncOperationResult> taskList = new List<IArtifactLoadAsyncOperationResult>();
        int loadcount = 7;

        /* 1.  Check instance list
         * 2. if in instance, duplicate,
         * 3. if not, load instance and then update instance list
         * */

        sceneassets = new List<IAssetEntry>();
        var sortedAssets = roomDesign.assets.OrderByDescending(x => x.asset.mountType).ToList();
        //var sortedAsset1 = roomDesign.assets.Where(x => x.asset.mountType != MountType.attached.ToString()).OrderByDescending(x=>x.asset.mountType).ToList();
        //var sortedAsset2 = roomDesign.assets.Where(x => x.asset.mountType == MountType.attached.ToString()).ToList();

        //Backup code to load sequentially
        int i = 0;
        foreach (var entry in sortedAssets)
        {
            await LoadTimeAssetLoader(entry);
            UpdateLoaderProgressValue(i + loadcount, "Loading product " + i + " / " + sortedAssets.Count());
            i++;
        }

        /*
        List<Task> assetTaskList = new List<Task>();
        foreach (var entry in sortedAsset1)
        {
            assetTaskList.Add(LoadTimeAssetLoader(entry));
        }
        List<List<Task>> taskDelegation = assetTaskList.SplitList(3);

        int loadtotal = roomDesign.assets.Count;

        int count;
        for (count = 0; count < taskDelegation.Count; count++)
        {
            assetTaskList = taskDelegation[count];
            UpdateLoaderProgressValue(count*3 + loadcount, "Loading product " + count*3 + " / " + loadtotal);
            Debug.Log("Now Loading " + count);
            await Task.WhenAll(assetTaskList);
        }
        count = count * 3;
        foreach (var entry in sortedAsset2) //Sequentially load attached assets
        {
            count++;
            await LoadTimeAssetLoader(entry);
            UpdateLoaderProgressValue(count+loadcount, "Loading product " + count + " / " + loadtotal);
        }*/

        app.db.AddSceneAssetEntry(sceneassets);

        await Resources.UnloadUnusedAssets();
        app.db.isDesignDirty = false;

        await ChangeSceneState(DesignSceneState.LoadFPS);
    }

    async Task FSM_LoadFPS()
    {
        UpdateLoaderProgressValue(loadProgressTotal - 1, "Finishing up, Please Wait...");

        originalCulMask = ComFPS.GetComponentInChildren<Camera>().cullingMask;
        scrPlayerFPS.UpdateMovement(false);
        scrPlayerFPS.Spawn(roomGo);
        prevTransform = new ITransform(ComFPS.transform);


        ComLightContainer.SetActive(true);
        lightAmbient.gameObject.SetActive(true);
        Light Amlight = lightAmbient.GetComponent<Light>();
        Amlight.intensity = 0.71f; //Amlight.intensity * 0.8f;

        widget.LoaderClose();
        CancelToken = null;
        timer.Stop();

        Messenger<long>.Broadcast(AnalyticsCalls.DesignLoadTime, timer.ElapsedMilliseconds);

        HomefulyEvents.OnSceneReady?.Invoke(AppPage.Designer.ToString(), app.db.sceneType);

        /*
        Fix the preCam to a viewpoint or camera transform from the current design. 
        Set by designer and used to take screenshots
         */ 
        preCam.SetActive(false);
        if(roomDesign.viewpoint.Count > 0)
        {
            preCam.transform.position = roomDesign.viewpoint[0].transform.position;
            preCam.transform.rotation = Quaternion.Euler(roomDesign.viewpoint[0].transform.rotation);
        }
        else if(roomDesign.viewPoints.Count > 0)
        {
            preCam.transform.position = roomDesign.viewPoints[0].position;
            preCam.transform.rotation = Quaternion.Euler(roomDesign.viewPoints[0].rotation);
        }

        /*
        Start Autosave for logged in user and non overwritable designs
         */
        // if (app.db.sceneType == SceneType.UserRoom && !app.db.CurrentDesign.demo && app.account.CRED_ISDEMO != string.Empty && app.db.CurrentDesign.project.Length > 0)
        if (app.CurrentSceneType != SceneType.ReadOnlyDesign && app.account.CRED_ISDEMO != string.Empty && app.db.CurrentDesign.customer.Length > 0)
        {
            UploadVersionCoverImage();
            InvokeRepeating(nameof(AutoSave), saveStart, saveFreq);
        }

        await ChangeSceneState(DesignSceneState.Navigate);
    }

    void FSM_Navigate()
    {

        if (isAssetLoading)
            return;
        UpdateNavigation();
    }

    void FSM_StoreOpen()
    {
        Debug.LogWarning("[Deprycation Warning] FSM_StoreOpen_Deprycated, Use FSM_AssetSelected instead");

        isStoreOpen = true;
        UpdateNavigation();
    }

    async Task FSM_StoreClose()
    {
        isStoreOpen = false;
        await ChangeSceneState(DesignSceneState.Navigate);
    }

    void FSM_AssetManOpen()
    {
        //isAssetSelected = true;
        UpdateNavigation();
    }

    async Task FSM_AssetManClose()
    {
        //isAssetSelected = false;
        UpdateNavigation();
        await ChangeSceneState(DesignSceneState.Navigate);
    }

#region Save_functionality


    /// <summary>
    /// Autosave routine
    /// </summary>
    /// <returns></returns>
    private async void AutoSave()
    {
        Debug.LogWarning("Autosave only if design is dirty");

        if (!app.db.isDesignDirty || app.db.CurrentDesign.customer.Length < 1) return;

        Debug.LogWarning("Autosaving...");
        saveSpinner.SetActive(true);

        var temp = await app.db.SyncUserRoomWithServer(new ITransform{
                position = ComFPS.transform.position,
                rotation = ComFPS.transform.rotation.eulerAngles,
                scale = ComFPS.transform.localScale
            }, false);

        roomDesign = IRoomDesign.CreateCopy(app.db.CurrentDesign);
        UploadVersionCoverImage();
        
        if(!autosaveDone)
        {
            Messenger<string>.Broadcast(AnalyticsCalls.SaveDesignCompleted, "AutoSave");
            autosaveDone = true;
        }
    }

     /// <summary>
    /// Save updated design to server
    /// </summary>
    /// <param name="buttonEvent"></param>
    /// <returns></returns>
    public void OnSaveManual(string buttonEvent = null)
    {
        Debug.Log("Manual Saving...");

        /*
        Skip save for readonly scene
         */

        if (app.CurrentSceneType == SceneType.ReadOnlyDesign)
        {
            if (buttonEvent == "Exit Button" || buttonEvent == "Force Exit")
            {
                Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, app.db.sceneType);
                AppComponent.Instance.account.CURRENT_ROOM_ID = string.Empty;
                AppComponent.Instance.account.CURRENT_CHALLENGE_ID = string.Empty;

                PlayerPrefs.SetString("minimizeUnity", " ");
                app.Navigate(AppPage.AppComponent);
                // AppComponent.Instance.Navigate(AppPage.Home);
            }
            return;
        }

        /*
        Check authentication then proceed to save
         */ 
        if (app.account.CRED_ISDEMO == string.Empty)
        {
            tooltipManager.UpdateToolTipPause(true);
            HomefulyExtensions.SetScreenOrientation(false);
            widget.AuthPanelOpen((bool success) =>
            {
                tooltipManager.UpdateToolTipPause(false);
                SaveOnLoggedIn(success, buttonEvent);
                if(success)
                {
                    InvokeRepeating(nameof(AutoSave), 30f, saveFreq);
                    if(btnSave.activeSelf) btnSave.SetActive(false);
                }
            });
        }
        else
        {
            SaveOnLoggedIn(true, buttonEvent);
        }
    }

    // Do save for logged in user
    public async void SaveOnLoggedIn(bool success, string buttonEvent)
    {
        if (success && app.db.isDesignDirty)
        {
            widget.LoaderMini("Saving... ");

            if(app.db.CurrentDesign.project.Length < 1)
                app.db.CurrentDesign.project = AppConfig.PilotDemoConfig.ProjectId;

            /*
            Check if the design is overwritable
            If not then set createNewVersion flag
             */
            bool createNewVersion = false;
             if (app.db.CurrentDesign.customer.Length < 1)
            {
                createNewVersion = true;
                app.db.CurrentDesign.customer = app.account.CRED_USERID;
                app.db.CurrentDesign.demo = false;
            }

            // set to false if save succeded
            createNewVersion = await app.db.SyncUserRoomWithServer(new ITransform{
                position = ComFPS.transform.position,
                rotation = ComFPS.transform.rotation.eulerAngles,
                scale = ComFPS.transform.localScale
            }, createNewVersion);

            // checks if saved or not
            if(!createNewVersion)
            {
                roomDesign = IRoomDesign.CreateCopy(app.db.CurrentDesign);
                UploadVersionCoverImage();
                Messenger<string>.Broadcast(AnalyticsCalls.SaveDesignCompleted, buttonEvent);
            }

            await Task.Delay(3000);
            widget.LoaderMiniClose();
        }
        // else if (!success && buttonEvent == "Exit Button")
        // {
        //     // delete design if user exited without signing in
        //     var res = app.server.artifact.DeleteRoomVersion(AppConfig.PilotDemoConfig.ProjectId, app.account.CURRENT_ROOM_ID, app.account.CURRENT_VERSION_ID);
        // }

        if (buttonEvent == "Exit Button" || buttonEvent == "Force Exit")
        {
            Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, app.db.sceneType);
            AppComponent.Instance.account.CURRENT_CHALLENGE_ID = string.Empty;
            AppComponent.Instance.account.CURRENT_ROOM_ID = string.Empty;

            PlayerPrefs.SetString("minimizeUnity", " ");
            app.Navigate(AppPage.AppComponent);
            // AppComponent.Instance.Navigate(AppPage.Home);
        }
        else
        {
            HomefulyExtensions.SetScreenOrientation(true);
        }
        return;
    }

    // Design Screenshots
    private async void UploadVersionCoverImage()
    {
        // roomDesign = IRoomDesign.CreateCopy(app.db.CurrentDesign);
        string imgName = roomDesign._id + "_cover";
        string imgPath = Path.Combine(Application.persistentDataPath + "/screenshots/", imgName + ".png");
        await CaptureScreen(imgName);
        var res = await app.server.artifact.UploadCoverImage(roomDesign, imgPath);
        if (res == null) Debug.Log("Failed to save screenshot");
        
        File.Delete(imgPath);
        if(saveSpinner.activeSelf) saveSpinner.SetActive(false);

    }

    private async Task CaptureScreen(string nameie)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/screenshots"))
            Directory.CreateDirectory(Application.persistentDataPath + "/screenshots");

        Camera cam;
        if(roomDesign.viewpoint.Count > 0 || roomDesign.viewPoints.Count > 0)
        {
            preCam.SetActive(true);
            cam = preCam.GetComponentInChildren<Camera>();
        }
        else
        {
            cam = ComFPS.GetComponentInChildren<Camera>();
        }

        Rect oldCamRect = cam.rect;
        cam.rect = new Rect(0, 0, 1, 1);

        // fixed renderTexture size, invariant to screen orientation
        int w = 1280;
        int h = 720;
        await Task.Delay(250);

        RenderTexture rt = new RenderTexture(w, h, 24, RenderTextureFormat.ARGB32);
        Texture2D screenShot = new Texture2D(w, h, TextureFormat.RGB24, false);

        RenderTexture.active = rt;
        cam.targetTexture = rt;
        cam.Render();

        screenShot.ReadPixels(new Rect(0, 0, w, h), 0, 0);
        screenShot.Apply();

        cam.targetTexture = null; 
        RenderTexture.active = null; 
        Destroy(rt);

        byte[] bytes = screenShot.EncodeToPNG();
        string filename = ScreenShotName(nameie);
        File.WriteAllBytes(filename, bytes);

        cam.rect = oldCamRect;
        if(preCam.activeSelf) preCam.SetActive(false);
    }

    private static string ScreenShotName(string name)
    {
        if (!string.IsNullOrEmpty(name))
            return string.Format("{0}/screenshots/{1}.png",
                                Application.persistentDataPath,
                                name);
        else
            return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
                            Application.persistentDataPath,
                            Screen.width, Screen.height,
                            System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

#region Old School Methods

    public class UserInputs
    {
        public bool overWrite;
        public string name;
    }

    private void getUserInputs()
    {
        UserInputs ui = new UserInputs();
        ui.overWrite = false;
        ui.name = "new design";

        if (!app.db.CurrentDesign.demo)
        {
            widget.Confirm("Confirm", "Overwrite existing design?", (string ans) =>
            {
                if (ans == "yes")
                {
                    ui.overWrite = true;
                    SaveScene(ui);
                }
                if (ans == "no")
                {
                    widget.Prompt("Save As", (string name) =>
                    {
                        ui.name = name;
                        // ui.overWrite = false;
                        SaveScene(ui);
                    }, false);
                }
            }, true);
        }
        else
        {
            widget.Prompt("Save As", (string name) =>
            {
                ui.name = name;
                SaveScene(ui);
            }, false);
        }
    }

    async void SaveScene(UserInputs ui)
    {
        widget.LoaderMini("Saving.. ", 4);
        await ChangeSceneState(DesignSceneState.Save, ui);
        UploadVersionCoverImage(); // Screenshot process will run in background
        widget.LoaderMiniClose();

        widget.Alert("Design Saved!!", "You can browse your designs in \'My Designs\' section", () =>
        {
            widget.LoaderClose();
            Messenger<string>.Broadcast(AnalyticsCalls.SaveDesignCompleted, buttonType);

            if (buttonType == "Exit Button")
            {
                Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, app.db.sceneType);
                AppComponent.Instance.account.CURRENT_ROOM_ID = string.Empty;
                AppComponent.Instance.Navigate(AppPage.AppComponent);
            }
            else
            {
                Messenger.Broadcast(StaticFunctionCalls.MinimizeMenuPanel);
            }
        },
        "", false);
        return;
    }

    async Task FSM_Save(UserInputs ui)
    {
        roomDesign.camera.position = ComFPS.transform.position;
        roomDesign.camera.rotation = ComFPS.transform.rotation.eulerAngles;

        app.db.UpdateAssetTransformData();
        roomDesign.assets = app.db.sceneAssets.Values.ToList(); //RefreshAssetEntryList_Deprycated();
        roomDesign.viewPoints = viewPointsList;

        app.db.UpdateWallfinishAdjustData();
        roomDesign.paint = app.db.sceneWallfinishs.Values.ToList(); //PrepareWallposterList_Deprycated();

        Debug.Log("roomDesign : " + JsonUtility.ToJson(roomDesign));
        roomDesign.demo = false;
        roomDesign.project = AppConfig.ProjectConfig.PROJ_USER_SAVED_OLD;
        roomDesign.customer = AppComponent.Instance.account.CRED_USERID;
        AppConfig.PilotDemoConfig.ProjectId = roomDesign.project;

        // save demo design
        if (app.db.CurrentDesign.demo)
        {
            if (alreadySaved && ui.name == alreadySavedName)
            {
                // update the previously saved version
                // Debug.Log("Demo Updating");
                roomDesign._id = alreadySavedVersionId;
                roomDesign.name = ui.name;
                var res = await app.server.artifact.UpdateRoomDesignAsync(roomDesign);
            }
            else
            {
                // create a new version
                // Debug.Log("Demo Creating New");
                roomDesign._id = "delete";
                roomDesign.name = ui.name;
                var res = await app.server.artifact.CreateNewRoomVersion(roomDesign);
                roomDesign._id = res._id;
                alreadySaved = true;
                alreadySavedVersionId = res._id;
                alreadySavedName = res.name;
            }
        }
        else
        {
            if (ui.overWrite)
            {
                // Debug.Log("Existing Updating");
                var res = await app.server.artifact.UpdateRoomDesignAsync(roomDesign);
            }
            else
            {
                // Debug.Log("Existing Creating New");
                roomDesign._id = "delete";
                roomDesign.name = ui.name;
                var res = await app.server.artifact.CreateNewRoomVersion(roomDesign);
                roomDesign._id = res._id;
            }
        }

        app.db.SetCurrentDesignData(roomDesign);
        await ChangeSceneState(DesignSceneState.Navigate);
    }

#endregion

    void FSM_Clean()
    {
        //assetStoreList_Deprycated.Clear();
        //assetEntryList_Deprycated.Clear();
    }


    private void FSM_WallEditMode()
    {
        UpdateNavigation();
    }


    private Task FSM_DownloadAssetErr()
    {
        throw new NotImplementedException();
    }

    void FSM_DownloadRoomErr()
    {
        widget.LoaderMiniClose();
        widget.Alert("Room Error!", "There was a problem downloading the center room!");
        print("Room Loading Err : ");
    }


#endregion

#region FSM

    public async Task ChangeSceneState(DesignSceneState state, UserInputs ui = null)
    {
        try
        {

            //_.l("State Change to " + state.ToString());
            currentState = state;

            switch (state)
            {
                case DesignSceneState.Preload:
                    await FSM_Preload();
                    break;

                case DesignSceneState.DownloadMeta:
                    await FSM_DownloadMeta();
                    break;

                case DesignSceneState.DownloadArtifactRoom:
                    await FSM_DownloadArtifactRoom();
                    break;

                case DesignSceneState.DownloadArtifactAsset:
                    await FSM_DownloadArtifactAsset();
                    break;

                case DesignSceneState.LoadFPS:
                    await FSM_LoadFPS();
                    break;

                case DesignSceneState.Navigate:
                    FSM_Navigate();
                    break;

                case DesignSceneState.StoreOpen:
                    FSM_StoreOpen();
                    break;

                case DesignSceneState.StoreClose:
                    await FSM_StoreClose();
                    break;

                case DesignSceneState.AssetSelected:
                    FSM_AssetManOpen();
                    break;

                case DesignSceneState.AssetDeselected:
                    await FSM_AssetManClose();
                    break;

                case DesignSceneState.Save:
                    await FSM_Save(ui);

                    break;

                // case DesignSceneState.SaveAs:

                //     await FSM_SaveAs();
                //     break;


                case DesignSceneState.Clean:
                    FSM_Clean();
                    break;
                case DesignSceneState.DownloadRoomErr:
                    FSM_DownloadRoomErr();
                    break;
                case DesignSceneState.DownloadAssetErr:
                    await FSM_DownloadAssetErr();
                    break;
                case DesignSceneState.WallEdit:
                    FSM_WallEditMode();
                    break;
            }
        }
        catch (OperationCanceledException e)
        {
            Debug.Log(e.Message);
        }

    }



    public enum DesignSceneState
    {
        Preload,
        DownloadMeta,
        DownloadArtifactRoom,
        DownloadRoomErr,
        DownloadArtifactAsset,
        DownloadAssetErr,
        LoadFPS,
        Navigate,
        StoreOpen,
        StoreClose,
        AssetSelected,
        AssetDeselected,
        Save,
        SaveAs,
        Clean,
        WallEdit
    }

#endregion

    private void TutorialChecker(bool obj)
    {
        if (obj)
            isTutorial = true;
        else
            isTutorial = false;

        //Debug.Log("Tutorial is : " + isTutorial);
    }

#region minitasks

    void UpdateLoaderProgressValue(int val, string title = null)
    {
        if (!string.IsNullOrEmpty(title))
        {
            // _.l("Loader Title: " + title);
            widget.Loader(title);
        }

        widget.LoaderProgressSlider.value = (int)val;
    }

#endregion

#region Deprycated

    /*void UpdateInfo_Deprycated(string txt = null)
    {
        //txtInfo.transform.parent.gameObject.SetActive(txt == null || string.IsNullOrEmpty(txt));

        //txtInfo.text = txt;   //Disabling txtInfo for now
    }*/

    /*void UpdateNavigation_Deprycated()
    {
        scrPlayerFPS.UpdateMovement(!isStoreOpen && !isAssetSelected && (currentState != DesignSceneState.WallEdit));


        //scrPlayerFPS.isEnabled = (!isStoreOpen && !isAssetSelected && (currentState != DesignSceneState.WallEdit));
        string msg = "[Navigation] : Turned Off! Close Asset Store to enable Navigation." + isAssetSelected;


        if (isStoreOpen)
        {
            msg = "[Navigation] : Turned Off! Close Asset Store to enable Navigation." + isStoreOpen;
        }

        if (isAssetSelected)
        {
            msg = ("[Navigation] : Turned Off! Deselect Asset to enable Navigation.");
        }

        if (isAssetSelected)
        {
            msg = ("[Navigation] : Turned Off! Editing Wallposter");
        }

        if (!isStoreOpen && !isAssetSelected)
        {
            msg = ("[Navigation] : Turned On.");
        }

        UpdateInfo_Deprycated(msg);
    }*/


    /*public bool sizeset = false;


    List<float> fps = new List<float>();
    float totTime = 0f;
    public GameObject canvas_WorldSpace;

    /// <summary>
    /// Run This Function Every Frame, And It Eventually it will Start updating the avgFps Var Value.
    /// </summary>
    protected void FpsControl()
    {
        fps.Add(1 / Time.deltaTime);
        totTime += Time.deltaTime;

        if (totTime >= 1) //1 here represents 1 seconds, change it to desired value;
        {
            totTime = 0f; float sum = 0f;
            for (int i = 0; i < fps.Count; i++) { sum += fps[i]; }
            float avgFps = (float)Math.Ceiling(sum / fps.Count);
            //By Here Average Value Is ready to Use.

            fps.Clear();
            if (avgFps < 20) { /*May Be Give A Warning Lol*//* }
        }
    }*/

    /*public void OpenTutorials()
    {
        TutorialCanvas.gameObject.SetActive(true);
        TutorialManager.gameObject.SetActive(true);
        Messenger.Broadcast(StaticFunctionCalls.OpenTutorial);
    }*/

    /*private List<IWallfinishEntry> PrepareWallposterList_Deprycated()
    {
        Debug.LogWarning("[Deprycation Warning] PrepareWallposterList_Deprycated, Use app.db.scenewallfinishes instead");


        // Debug.LogError("Prepare Wallposter List Function called");
        List<IWallfinishEntry> wallposterList = new List<IWallfinishEntry>();
        foreach (Transform child in roomGo.transform)
        {
            foreach (Transform grandChild in child)
            {
                if (grandChild.gameObject.name.Contains("paint"))
                {
                    var wallposterEntry = new IWallfinishEntry();
                    if (child.gameObject.name.Contains("wall_"))
                    {
                        wallposterEntry.wall = child.name;
                        wallposterEntry.wallfinish = grandChild.GetChild(0).name;
                    }
                    var trans = grandChild.transform;
                    wallposterEntry.adjust = new IWallfinishAdjust();
                    wallposterEntry.adjust.scale = new Vector3(trans.localScale.x, trans.localScale.y, trans.localScale.z);
                    wallposterEntry.adjust.offset = new Vector3(trans.position.x, trans.position.y, trans.position.z);
                    wallposterList.Add(wallposterEntry);

                }
            }
        }

        return wallposterList;
    }*/



    //async void OnMinimisePanel()
    //{
    //    await ChangeSceneState(DesignSceneState.StoreClose);
    //}

    //async void OnMaximisePanel()
    //{
    //    await ChangeSceneState(DesignSceneState.StoreOpen);
    //}


    //Deprycated
    /*void OnGameobjectTapped_Deprycated(GameObject go)
    {
        Debug.LogWarning("[Deprycated Warning] OnGameobjectTapped_Deprycated, use OnScreenTap() instead");

        if (go.tag == "Poster")
        {
            if (selectedWall != null && selectedWall == go.transform.parent)
            {
                GameObject selectedChild = null;
                foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
                {
                    if (c.gameObject.name == go.name && c.isSelected == false)
                    {
                        selectedChild = c.gameObject;
                    }
                }
                if (selectedChild != null)
                {
                    Debug.Log("Selected Wallpaper " + selectedChild.name);
                    foreach (var c in selectedWall.GetComponentsInChildren<PosterStateManager>())
                    {
                        if (c.gameObject == selectedChild)
                            c.GetComponent<PosterStateManager>().isSelected = true;
                        else
                        {
                            c.GetComponent<PosterStateManager>().isSelected = false;
                            c.GetComponent<PosterStateManager>().ChangeEditState(PosterStateManager.WallPosterEditState.Idle);
                        }
                    }
                }
            }
        }
        //if (!isCamVertical)
        //{
        //    if (go.tag == "Floor" || go.name.ToLower().Contains("column") || go.tag == "Wall" || (go.tag == "Roof"))
        //    {
        //        //Debug.Log("Current Asset Tag: " + go.tag + " Curent name: " + go.name);
        //        if (selectedAsset_Deprycated != null)
        //        {
        //            //if (selectedAsset.GetComponent<AssetTransformManager>().assetTransformMode == AssetTransformMode.idle)
        //            //    DoDeselectAsset();
        //        }
        //        //print("X RAY HITTTT");
        //        //Messenger.Broadcast(StaticFunctionCalls.DoDeselectAsset);
        //    }
        //    else if (go.tag == "Asset")
        //    {
        //        //assetEntryList.
        //        //ON_ASSET_TAPPED(go);
        //    }
        //}
    }*/


    //Deprycated
    //private List<IWallposterEntry> PrepareWallposterList_Deprycated()
    //{
    //    List<IWallposterEntry> wallposterList = new List<IWallposterEntry>();
    //    foreach (Transform child in roomGo.transform)
    //    {
    //        foreach (Transform grandChild in child)
    //        {
    //            if (grandChild.gameObject.name.Contains("Poster_"))
    //            {
    //                var trans = grandChild.transform;
    //                var wallposterEntry = new IWallposterEntry()
    //                {
    //                    wall = child.name,
    //                    wallfinish = grandChild.gameObject.name.Split('_')[1],
    //                    adjust = new IWallposterAdjust()
    //                    {
    //                        scale = new Vector3(trans.localScale.x, trans.localScale.y, trans.localScale.z),
    //                        offset = new Vector3(trans.position.x, trans.position.y, trans.position.z)
    //                    }
    //                };
    //                wallposterList.Add(wallposterEntry);


    //                //wallposterEntry.wall = child.name;
    //                //wallposterEntry.wallfinish = grandChild.gameObject.name.Split('_')[1];
    //                //wallposterEntry.adjust = new IWallposterAdjust();

    //                //wallposterEntry.adjust.scale = new Vector3(trans.localScale.x, trans.localScale.y, trans.localScale.z);
    //                //wallposterEntry.adjust.offset = new Vector3(trans.position.x, trans.position.y, trans.position.z);
    //            }
    //        }
    //    }

    //    return wallposterList;
    //}
    //Deprycated
    /*async void ON_ASSET_TAPPED(GameObject tappedGo)
    {
        Debug.LogWarning("[Deprycation Warning] OnAssetTapped_Deprycated, use OnScreenTap() instead");
        if (!app.db.IsCurrentObjectValid<IAssetEntry>())
        {
            if (!app.db.sceneAssets.ContainsKey(tappedGo.name))
            {
                Debug.LogError("Selected Asset Doesnt Exist"); return;
            }

            app.db.currentObject = app.db.sceneAssets[tappedGo.name];
            SelectAsset();

            await ChangeSceneState(DesignSceneState.AssetSelected);

            return;
        }


        if ((app.db.currentObject as IAssetEntry).entryId != tappedGo.GetComponent<AssetTransformManager>().entry.entryId)
        {
            if (!app.db.sceneAssets.ContainsKey(tappedGo.name))
            {
                Debug.LogError("Selected Asset Doesnt Exist"); return;
            }
            DeselectAsset();

            app.db.currentObject = app.db.sceneAssets[tappedGo.name];
            SelectAsset();

            await ChangeSceneState(DesignSceneState.AssetSelected);

            return;
        }



        //if (selectedAsset_Deprycated.GetComponent<AssetTransformManager>().assetTransformMode == AssetTransformMode.idle)
        //{
        //    print("idle mode  de-ASsign");
        //    //DeselectAsset();
        //    //  await ChangeSceneState(DesignSceneState.AssetManClose);
        //    return;
        //}

        return;
    }*/


    //void EnableVerticalCam_Deprecated(bool val = true)
    //{

    //    Debug.LogWarning("[EnableVerticalCam_Deprecated] this function uses old asset selection logic, may be unstable");
    //    //val = false;    //TEST
    //    int fpsCul = (1 << LayerMask.NameToLayer("TransparentFX") | LayerMask.NameToLayer("Ignore Raycast") | LayerMask.NameToLayer("UI") | LayerMask.NameToLayer("PostProcessing") | LayerMask.NameToLayer("Roof") | LayerMask.NameToLayer("Floor") | LayerMask.NameToLayer("RoofAddons") | LayerMask.NameToLayer("Asset") | LayerMask.NameToLayer("Default"));
    //    int verCul = (1 << LayerMask.NameToLayer("Floor") | LayerMask.NameToLayer("TransparentFX") | LayerMask.NameToLayer("Ignore Raycast") |/* LayerMask.NameToLayer("Roof") |*/ LayerMask.NameToLayer("UI") | LayerMask.NameToLayer("Asset"));

    //    Vector3 oldpos;// = ComFPS.transform.position;

    //    if (val && !isCamVertical)
    //    {
    //        ComFPS.GetComponent<Rigidbody>().useGravity = false;
    //        ComFPS.GetComponent<Rigidbody>().isKinematic = false;
    //        fpsPos = ComFPS.transform.position;
    //        fpsRot = ComFPS.transform.rotation.eulerAngles;

    //        Vector3 nPos = (app.db.currentAsset as IAssetEntry).objRef.transform.position;  //ComFPS.transform.position;//Vector3.zero;
    //        Vector3 nRot = Vector3.zero;

    //        nPos.y += 4;
    //        //nPos.z += 0.85f;
    //        nRot.x = 90;
    //        nRot.y = 180;

    //        ComFPS.transform.position = nPos;
    //        ComFPS.transform.rotation = Quaternion.Euler(nRot);
    //        ComFPS.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
    //        //Debug.LogError("Curr Pos is: " + nPos + " thetransi is: " + ComFPS.transform.position + " ysaya is: " + scrPlayerFPS.transform.position + " Assetwa : " + (app.db.currentAsset as IAssetEntry).objRef.transform.position + " asset namewa:" + (app.db.currentAsset as IAssetEntry).objRef.name);
    //        ComFPS.GetComponentInChildren<Camera>().cullingMask = originalCulMask & ~(1 << LayerMask.NameToLayer("Roof")) & ~(1 << LayerMask.NameToLayer("RoofAddons"));

    //        isCamVertical = true;
    //    }
    //    else if (!val && isCamVertical)
    //    {
    //        oldpos = fpsPos;
    //        isCamVertical = false;
    //        fpsRot.y = ComFPS.transform.eulerAngles.y;
    //        ComFPS.transform.eulerAngles = fpsRot;
    //        ComFPS.GetComponentInChildren<Camera>().cullingMask = originalCulMask;

    //        fpsPos = (app.db.currentAsset as IAssetEntry).objRef.transform.position;
    //        Vector3 newpos;
    //        newpos.z = oldpos.z * 0.3f;
    //        newpos.x = oldpos.x * 0.3f;
    //        newpos.y = oldpos.y;
    //        ComFPS.transform.position = newpos;

    //        ComFPS.transform.LookAt((app.db.currentAsset as IAssetEntry).objRef.transform);


    //        ComFPS.GetComponent<Rigidbody>().useGravity = true;
    //        ComFPS.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
    //        Vector3 resetRot = ComFPS.transform.eulerAngles;
    //        resetRot.x = 0;
    //        resetRot.z = 0;
    //        ComFPS.transform.eulerAngles = resetRot;
    //    }


    //}

    /*#region Storage_Deprycated

    //List<IAssetEntry> RefreshAssetEntryList_Deprycated()
    //{
    //    assetEntryList_Deprycated.Clear();
    //    foreach (Transform t in assetsContainer)
    //    {
    //        if (t.GetComponent<AssetTransformManager>() != null)
    //        {
    //            IAssetEntry ase = t.GetComponent<AssetTransformManager>().entry;
    //            /* SOS Debug.LogError("" + t.GetComponent<AssetTransformManager>().entry.asset.textureUrl); */
    //            ase.transform = new ITransform
    //            {
    //                position = t.position,
    //                rotation = t.rotation.eulerAngles,
    //                scale = t.localScale
    //            };

    //            ase.objRef = t.gameObject;
    //            assetEntryList_Deprycated.Add(ase.entryId, ase);
    //        }
    //    }
    //    Messenger<List<IAssetEntry>>.Broadcast(StaticFunctionCalls.GetProductListData, new List<IAssetEntry>(assetEntryList_Deprycated.Values));
    //    return new List<IAssetEntry>(assetEntryList_Deprycated.Values);
    //}


    //List<string> GetAssetEntryListFormAssetId(string id)
    //{
    //    List<string> list = new List<string>();
    //    foreach (KeyValuePair<string, IAssetEntry> ase in assetEntryList_Deprycated)
    //    {
    //        if (ase.Value.asset._id == id)
    //        {
    //            list.Add(ase.Key);
    //            //break;
    //        }
    //    }

    //    return list;
    //}
    //=~ LayerMask.GetMask("Enemy"); <inheritdoc />
#endregion*/

#endregion

    void Dummy()
    { }



}


public static class Utils
{

    /// <summary>
    /// SPlit List to Smaller lists
    /// </summary>
    /// <param name="locations"></param>
    /// <param name="nSize"></param>
    /// <returns></returns>\
    public static List<List<T>> SplitList<T>(this List<T> items, int sliceSize = 30)
    {
        List<List<T>> list = new List<List<T>>();
        for (int i = 0; i < items.Count; i += sliceSize)
            list.Add(items.GetRange(i, Math.Min(sliceSize, items.Count - i)));
        return list;
    }
}
