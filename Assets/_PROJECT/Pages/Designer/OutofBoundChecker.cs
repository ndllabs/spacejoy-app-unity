﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;

public class OutofBoundChecker : MonoBehaviour
{
   
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Asset")
        {
            Debug.LogError("Asset Deleted for it left the building(High Treason).");
            //Messenger.Broadcast(StaticFunctionCalls.DeleteAsset);
        }

    }
}
