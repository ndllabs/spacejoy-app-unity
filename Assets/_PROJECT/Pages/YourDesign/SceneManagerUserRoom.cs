﻿/* Holds Control flow for scene DesignChallenge
 * 
 * prajwalshetty2018@gmail.com
 * */

using _unity.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using SFC = StaticFunctionCalls;

using Homefuly;
using Homefuly.Database;
using Homefuly.Events;

public class SceneManagerUserRoom : MonoBehaviour
{
    #region Vars
    [Header("Prefabs")]
    public GameObject elemHotspot;
    public GameObject elemWallPoster;
    public GameObject elemSelector;

    public Transform roomParent;
    public Transform assetsParent;

    private AppComponent app;
    private ArtifactManager artifactManager;
    public IFloorPlanProcessor floorplanData;
    System.Diagnostics.Stopwatch timer;
    private IDesignChallenge designChallenge;
    private GameObject roomGo;
    #endregion

    #region Broadcasts

    /// <summary>
    /// Broadcasts when the Asset Swap action is "COMPLETE"
    /// </summary>
    /// <param name="hotpot">hotspot Class reference</param>
    private void Broadcast_ActionAssetSwapped(IHotspot hotpot)
    {
        Messenger<IHotspot>.Broadcast(SFC.Action_HotspotSwapped, hotpot);
    }

    /// <summary>
    /// Broadcasts when the Asset Delete action is "COMPLETE"
    /// </summary>
    /// <param name="hotpot">hotspot Class reference</param>
    private void Broadcast_ActionAssetDeleted(IHotspot hotpot)
    {
        Messenger<IHotspot>.Broadcast(SFC.Action_AssetDeleted, hotpot);
    }

    /// <summary>
    /// Broadcasts when the Asset Selected action is "COMPLETE"
    /// </summary>
    /// <param name="isSelected">is Asset Selected? or is it Deselected?</param>
    private void Broadcast_ActionAssetSelected(bool isSelected)
    {
        Messenger<bool>.Broadcast(SFC.ActionAssetSelected, isSelected);
    }

    ///// <summary>
    ///// Broadcasts when the Design Submission action is "COMPLETE"
    ///// </summary>
    //private void Broadcast_DesignSubmitted()
    //{
    //    Messenger.Broadcast(SFC.Action_DesignSubmited);
    //}

    /// <summary>
    /// Broadcasts when the Wallfinish Apply action is "COMPLETE"
    /// </summary>
    /// <param name="wallspot">Wallfinish hotspot Class reference</param>
    private void Broadcast_ActionWallfinishApplied(IWallfinishHotspot wallspot)
    {
        Messenger<IWallfinishHotspot>.Broadcast(SFC.Action_WallfinishSwapped, wallspot);
    }

    /// <summary>
    /// Broadcasts when the Scene Load action is "COMPLETE"
    /// </summary>
    private void Broadcast_ActionSceneLoaded()
    {
        HomefulyEvents.OnSceneReady?.Invoke(string.Empty, SceneType.DesignChallenge);
    }

    private void Broadcast_OnActionFailed(ActionType actiontype)
    {
        //HomefulyEvents.OnActionFailed?.Invoke(actiontype);
    }
    #endregion


    #region Listners
    private void ClassListners()
    {
        /* IHotspot => Hotspot To Be Swapped
         * string: => Asset ID to be Swapped With
         * */
        Messenger<IHotspot, string>.AddListener(SFC.Request_HotspotSwap, OnAssetSwapRequest);

        /* IHotspot => Hotspot To Be Deleted
         * Note: Will only delete the asset in the hotspot, not the hostpot itself
         * */
        Messenger<IHotspot>.AddListener(SFC.Request_AssetDelete, OnAssetDeleteRequest);

        /* bool => Request asset Select(true) or asset deselect(false)
         * */
        Messenger<bool>.AddListener(SFC.Request_AssetSelect, OnAssetSelectRequest);

        //* 
        // * */
        //Messenger.AddListener(SFC.Request_DesignSubmit, OnDesignSubmitRequest);

        /* IWallfinishHotspot => Wallfinish Hotspot To Be Swapped
         * string: => Wallfinish ID to be Swapped With
         * */
        Messenger<IWallfinishHotspot, string>.AddListener(SFC.Request_WallfinishApply, OnWallfinishSwapRequest);
    }


    #endregion


    #region Unity Events
    private void OnEnable()
    {
        // AppDatabase.OnDBReady += OnDatabaseReady;

        ClassListners();
    }
    private void OnDisable()
    {
        // AppDatabase.OnDBReady -= OnDatabaseReady;

        Messenger<IHotspot, string>.RemoveListener(SFC.Request_HotspotSwap, OnAssetSwapRequest);
        Messenger<IHotspot>.RemoveListener(SFC.Request_AssetDelete, OnAssetDeleteRequest);
        Messenger<bool>.RemoveListener(SFC.Request_AssetSelect, OnAssetSelectRequest);
        Messenger<IWallfinishHotspot, string>.RemoveListener(SFC.Request_WallfinishApply, OnWallfinishSwapRequest);

    }
    #endregion


    #region Core

    #region Design Load

    /// <summary>
    /// Once the DB is ready, Begin Scene Load
    /// </summary>
    private void OnDatabaseReady()
    {
        Debug.Log("Initiating scene load");
        ChangeSceneState(DesignChallengeState.Load);
    }


    async Task FSM_Load()
    {
        timer = new System.Diagnostics.Stopwatch();
        timer.Start();

        app = AppComponent.Instance;

        /* Define all variables
         * */
        artifactManager = new ArtifactManager(app.server);
        designChallenge = app.db.designChallenge;

        var roomid = app.account.CURRENT_ROOM_ID;
        var versionid = app.account.CURRENT_VERSION_ID;
        var challengeid = app.account.CURRENT_CHALLENGE_ID;

        RenderSettings.ambientLight = new Color32(0xBE, 0xBE, 0xBE, 0xBE);

        /* Download and Load Room
         * */
        var roomRes = await artifactManager.DownloadFloorplan(roomid, roomParent.transform);

        if (roomRes.err || roomRes.gameObject == null)
        {
            Debug.LogError("[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(FSM_Load) + "] Scene Load Abrorted, Reason: Room Load Error");
            WidgetManager.Instance.Alert("Load Failed", "Sorry, Unable to open this design please try again later");
            return;
        }
        roomGo = roomRes.gameObject;

        artifactManager.PostProcessFloorplan(roomGo.transform, floorplanData);
        app.db.SetCurrentRoom(roomGo);

        /* TO DO:
         * Init Camera, Set Position,
         * Init Light and Update GFX settings
         * */

        /* Load All Asset/Hotspot And Apply All Paints 
         * */
        await Task.WhenAll(LoadAllAssets(designChallenge.baseAssets), LoadAllWallfinishs(designChallenge.basePaint));

        ChangeSceneState(DesignChallengeState.Play);
    }


    void FSM_StartChallenge()
    {

    }

    #endregion

    #region CoreModules

    #region Wallfinish

    async Task LoadAllWallfinishs(List<IWallfinishHotspot> wallfinishs)
    {
        List<Task> paintLoadTasks = new List<Task>();
        foreach (var paintData in wallfinishs)
        {
            paintLoadTasks.Add(OnLoadApplyWallfinish(paintData.ToEntry()));
        }
        await Task.WhenAll(paintLoadTasks);
    }

    async Task OnLoadApplyWallfinish(IWallfinishEntry wallfinishData)
    {
        var t = roomGo.transform.Find(wallfinishData.wall);
        if (t != null && t.GetComponent<MeshRenderer>())
        {
            var dataRes = await app.server.artifact.GetWallPaintCode(new IPaintStoreElement_Deprycated(wallfinishData.wallfinish)); //new IPaintStoreElement(paintData.wallfinish));//paintStoreList[paintData.wallfinish]);
            if (dataRes == null) return;

            if (dataRes.category.ToEnum<WallfinishCategory>() == WallfinishCategory.wallpaper)
            {
                Debug.Log("Recieved Wallposter req, " + wallfinishData.wallfinishData.name + " id: " + wallfinishData.wallfinishData._id);

                var res = await app.server.artifact.GetWallpaperTexture(wallfinishData.wallfinish);
                if (res == null) return;


                res.name = wallfinishData.wallfinish + "_" + HomefulyExtensions.randomIdGenerator().ToString();

                app.db.AddNewWallfinishEntry(new IWallfinishEntry()
                {
                    wall = wallfinishData.wall,
                    wallfinish = wallfinishData.wallfinish,
                    entryId = HomefulyExtensions.randomIdGenerator().ToString(),
                    wallfinishData = dataRes,
                    adjust = wallfinishData.adjust,
                    objRef = DuringLoadSpawnWallpaper(roomGo.transform.Find(wallfinishData.wall), (Texture2D)res, new ITransform()
                    {
                        position = wallfinishData.adjust.offset,
                        scale = wallfinishData.adjust.scale
                    })
                });

            }
            else
            {
                app.db.AddNewWallfinishEntry(new IWallfinishEntry()
                {
                    wall = wallfinishData.wall,
                    wallfinish = wallfinishData.wallfinish,
                    entryId = HomefulyExtensions.randomIdGenerator().ToString(),
                    wallfinishData = dataRes,
                    adjust = wallfinishData.adjust,
                    objRef = null
                });


                dataRes.name = wallfinishData.wallfinish;
                t.DestroyChildren();

                GameObject paint = new GameObject("paint");
                paint.transform.SetParent(t);
                GameObject paintID = new GameObject(dataRes.name);
                paintID.transform.SetParent(paint.transform);
                ColorUtility.TryParseHtmlString(dataRes.code, out Color color);
                Material[] mat = t.GetComponent<MeshRenderer>().materials;

                foreach (Material ma in mat)
                {
                    ma.color = color;
                }
                t.GetComponent<MeshRenderer>().materials = mat;
            }
        }
        else Debug.LogWarning("[Paint Applier Loader] Unable to find wall named " + wallfinishData.wall + " skipping 1 paint data");
    }
    GameObject DuringLoadSpawnWallpaper(Transform wall, Texture2D wallposter, ITransform trans)
    {
        if (!wall)
        { Debug.LogError("[DuringLoadSpawnWallpaper]Wall not Found..!"); return null; }

        GameObject go = Instantiate(elemWallPoster, wall.transform);
        go.layer = 20; //"Wallpaper"
        go.tag = "Poster";
        go.name = "Poster_" + wallposter.name;

        go.transform.eulerAngles = wall.GetPosAlignedRotationEuler(trans.position);

        go.transform.position = trans.position;
        go.transform.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Standard"));
        go.transform.GetComponent<MeshRenderer>().material.mainTexture = wallposter;

        go.transform.localScale = trans.scale;
        Messenger<GameObject>.Broadcast(StaticFunctionCalls.GetWallPosterisOn, wall.gameObject);
        go.GetComponent<PosterStateManager>().Init();
        go.GetComponent<PosterStateManager>().ClampToWall();

        return go;
    }
    #endregion

    #region IAsset

    async Task LoadAllAssets(List<IHotspot> baseAssets)
    {
        var sortedHotspots = baseAssets.OrderByDescending(x => x.asset.mountType).ToList();

        foreach (var hot in sortedHotspots)
        {
            await LoadTimeAssetLoader(hot);
        }
    }

    //temp old deprycated fn
    async Task LoadTimeAssetLoader(IHotspot entry)
    {
        if (entry.isHotspot) return;
        IArtifactLoadAsyncOperationResult task;

        /* Downloads and loads asset given the assetid
         * */
        task = await LoadAssetAsync(entry.asset, assetsParent, entry.transform.position, entry.transform.rotation);

        if (task.gameObject == null) return;


        /*sceneassets.Add(new IAssetEntry
        {
            entryId = task.gameObject.name,
            inuse = true,
            objRef = task.gameObject,
            transform = new ITransform(task.gameObject.transform),
            asset = entry.asset
        });*/

        task = null;
    }

    //temp old deprycated fn
    async Task<IArtifactLoadAsyncOperationResult> LoadAssetAsync(IAsset asset, Transform parent, Vector3 position, Vector3 rotation, GameObject original = null)
    {
        var artRes = new IArtifactLoadAsyncOperationResult();

        if (asset == null) return artRes;

        artRes = await artifactManager.DownloadAsset(asset, parent, original);

        if (!artRes.err && artRes.gameObject != null)
        {
            // skip post processing if duplicated from existing asset
            if (original != null)
            {
                Destroy(artRes.gameObject.transform.Find("Selector(Clone)").gameObject);
            }

            //Setting updated At for asset cache SOS
            string str = "ASSET_UPDATE_CACHE_" + asset._id;
            PlayerPrefs.SetString(str, asset.updatedAt);

            artifactManager.PostProcessAsset(artRes.gameObject.transform, position, rotation);

            if (artRes.gameObject.GetComponent<AssetTransformManager>().entry == null)
                artRes.gameObject.GetComponent<AssetTransformManager>().entry = new IAssetEntry() { asset = new IAsset() };

            artRes.gameObject.GetComponent<AssetTransformManager>().entry.entryId = artRes.gameObject.name;

            artRes.gameObject.GetComponent<AssetTransformManager>().entry.asset = asset;
            artRes.gameObject.GetComponent<AssetTransformManager>().Init(true);
        }

        return artRes;
    }
    #endregion

    #endregion

    #region FSM

    private async void ChangeSceneState(DesignChallengeState state)
    {

        try
        {
            switch (state)
            {
                case DesignChallengeState.Load:
                    Debug.Log("rammed, Loading design..");
                    await FSM_Load();
                    break;
                case DesignChallengeState.Play:
                    FSM_StartChallenge();
                    break;
                case DesignChallengeState.End:
                    break;
            }
        }

        catch (OperationCanceledException e)
        {
            Debug.LogError("[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(ChangeSceneState) + "] Rammed, Critical Load Failure, ECODE 010" + e.Message);
        }

    }

    #endregion

    #region ListnerActions
    private void OnWallfinishSwapRequest(IWallfinishHotspot arg1, string arg2)
    {
        throw new NotImplementedException();
    }

    private void OnAssetSelectRequest(bool obj)
    {
        throw new NotImplementedException();
    }

    private void OnDesignSubmitRequest()
    {
        throw new NotImplementedException();
    }

    private void OnAssetDeleteRequest(IHotspot obj)
    {
        throw new NotImplementedException();
    }

    private void OnAssetSwapRequest(IHotspot arg1, string arg2)
    {
        throw new NotImplementedException();
    }
    #endregion

    #endregion
}


