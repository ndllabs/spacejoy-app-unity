﻿using _unity.Events;
using Homefuly;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
public class SceneManagerCenter : MonoBehaviour
{
    AppComponent app;
    WidgetManager widget;
    //const string demoPageUrl = AppConfig.ServerConfig.inAppBrowserHost + "/#/unity-demo-design";
    static string demoPageUrl = AppConfig.ServerConfig.webUIHost + "/#/unity-demo-design";

    public Transform listContainer, pnlDemoList, listProducts;
    public GameObject prefListElem;
    public GameObject prefProductElem;
    public GameObject pnlCenterLists;
    public GameObject prefDemoListElem;
    public GameObject prefabComingSoon;
    public GameObject pnlMoreOptions;
    public GameObject pnlViewProducts;
    public GameObject pnlDemoUser;
    public GameObject pnlUserWaiting;
    public GameObject pnlUserReady;
    public GameObject pnlSurveryPending;
    public GameObject pnlProjectPending;
    public GameObject pnlPaymentPending;


    public GameObject pnlDemoListContainer;
    public Button btnSignout;
    public Button btnEdit, btnCancel;
    public Button btnDemo, btnUser;
    public Button btnCloseProducts;
    public Button btnMore_Edit;
    public Button btnMore_ProductList;
    public Button btnFillReq;
    public Button btnPayment;
    public Button btnGetStarted;



    public Button btnDemo_signup;
    public Button btnDemo_signin;

    CancellationTokenSource cts, pcts;
    public Image demoImg, myprojectsImg;
    public Sprite placeHolder;
    public Sprite tabletBGImage;

    public ScrollRect productsScrollRect;

    float aspectRatio;

    bool ProductPanelOpen;
    bool MorePanelOpen;

    public bool skipIfSingle = false;

    string ProjectID, RoomName;

    UserState currentState = UserState.demo;

    static InAppBrowserBridge bridge;
    //private bool isHomepage = false;

    private void Awake()
    {
        HomefulyExtensions.SetScreenOrientation(false);
        demoPageUrl = AppConfig.ServerConfig.webUIHost + "/#/unity-demo-design";

        //bundlename = Application.identifier;
        //if (Screen.safeArea.y > 0)
        //{
        //    GameObject gg = transform.gameObject;
        //    gg.transform.Find("pnlNavBar").GetComponent<RectTransform>().anchorMax = new Vector2(1.0f, 0.96f);
        //    gg.transform.Find("pnlNavBar").GetComponent<RectTransform>().anchorMin = new Vector2(0, 0.83f);

        //    GameObject ss = gg.transform.Find("scrViews").gameObject;
        //    ss.GetComponent<RectTransform>().anchorMax = new Vector2(1, 0.83f);
        //}
        //if (bridge == null)
        //{
        bridge = FindObjectOfType<InAppBrowserBridge>();
        //Debug.Log("[Awake SceneManagerCenter] Bridge is null");
        //}


    }


    // Start is called before the first frame update
    async void Start()
    {
        Init();

#if UNITY_EDITOR 
        await Setup();
#else
        OpenDemoWeb();
#endif
    }

    void Init()
    {
        //Screen.orientation = ScreenOrientation.Portrait;

        //print("In Center");
        app = AppComponent.Instance;
        widget = WidgetManager.Instance;
        //widget.btnHelp.SetActive(false);

    }

    private void OnEnable()
    {
        bridge.onJSCallback.RemoveAllListeners();
        bridge.onJSCallback.AddListener(OnIABMessage);
        // bridge.onAndroidBackButtonPressed.AddListener(OnBackButtonPress);
    }

    // void OnBackButtonPress()
    // {
    //     Debug.Log("OnBackButtonPress: " + isHomepage);
    //     if(isHomepage)
    //     {
    //         InAppBrowser.CloseBrowser();
    //         widget.Confirm("Confirm", "Are you sure you want to exit?", (string ans) =>
    //         {
    //             if(ans == "Yes")
    //             {
    //                 Application.Quit();
    //             }
    //             else
    //             {
    //                 OpenDemoWeb();
    //             }
    //         }, false);
    //     }
    // }

    void OnIABMessage(string jsMessage)
    {

        Debug.Log(">>>>[ Inside ONAIBMessage Function]" );
        Debug.Log(">>>>[ jsMsg ]: " + jsMessage);
        IABResponse resp = new IABResponse(jsMessage);
        Debug.Log(">>>> resp");
        //InAppBrowser.CloseBrowser();
        //Debug.Log("Context: " + resp.Context);
        //Debug.Log("Err: " + resp.Err);
        //if (PlayerPrefs.HasKey("First_time"))
        //return;

        Debug.Log(">>>> After Return");
        if (resp.Err)
        {
            widget.Toast("Oops! Somethign went wrong. Please restart the app and try again.");
            InAppBrowser.CloseBrowser();
            return;
        }

        if (resp.Context == "editDesign")
        {
            // Debug.Log(">>>> editDesign");
            var iabData = resp.GetData<IABMDemoDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_ROOM_NAME = iabData.rid;// center.name;
            app.account.CURRENT_VERSION_ID = iabData.vid;

            Messenger<string, bool>.Broadcast(AnalyticsCalls.SceneConfirmationResponse, app.account.CURRENT_VERSION_ID, true);

            app.CurrentSceneType = SceneType.OldSchool;
            AppComponent.Instance.Navigate(AppPage.Designer);
            InAppBrowser.CloseBrowser();
        }
        else if (resp.Context == "login")
        {
            // Debug.Log(">>>> login");
            var iabData = resp.GetData<IABMUserDetails>();
            app.account.CRED_JWTTOKEN = iabData.token;
            app.account.CRED_USERID = iabData.id;
            app.account.CRED_USEREMAIL = iabData.name;
            app.account.CRED_ROLE = iabData.role;
            app.account.CRED_ISDEMO = "Not Demo";
        }
        else if (resp.Context == "logout")
        {
            // Debug.Log(">>>> logout");
            var iabData = resp.GetData<IABMUserDetails>();
            app.account.CRED_USERID = AccountConfigService.demoUserID;
            app.account.CRED_ISDEMO = string.Empty;
        }
        else if (resp.Context == "challenge")
        {
            // Debug.Log(">>>> design challenge");
            var iabData = resp.GetData<IABChallengeClass>();
            app.account.CURRENT_CHALLENGE_ID = iabData.challenge;
            Debug.Log(">>>> challenge ID: " + iabData.challenge);
            InAppBrowser.CloseBrowser();
            AppComponent.Instance.Navigate(AppPage.DesignChallenge);
        }
        else
        {
            // Debug.Log(">>>>Closing browser");
            // widget.Toast("Dev: Invalid context (required: editDesign, got: " + resp.Context);
            InAppBrowser.CloseBrowser();
        }
        // InAppBrowser.CloseBrowser();

    }



    void OpenDemoWeb()
    {
        StartCoroutine(Waitforasec());
        // await app.account.Connect();
        // InAppBrowser.OpenURL(demoPageUrl, new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true });
    }

    IEnumerator Waitforasec() // 1/10th of a sec
    {
        yield return new WaitForSeconds(0.1f);
        InAppBrowser.OpenURL(
            demoPageUrl + "/user/" + app.account.CRED_USERID + "/token/" + app.account.CRED_JWTTOKEN.Substring(4),
            new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true }
        );
        //InAppBrowser.OpenURL(
        //  "https://homefuly-web2.firebaseapp.com/game-homescreen/user/" + app.account.CRED_USERID + "/token/" + app.account.CRED_JWTTOKEN.Substring(4),
        //         new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true }
        //     );

    }

    async Task Setup()
    {
        transform.Find("pnlNavBar").gameObject.SetActive(true);
        transform.Find("scrViews").gameObject.SetActive(true);


        btnCloseProducts.onClick.RemoveAllListeners();
        btnCloseProducts.onClick.AddListener(CloseProductsList);
        btnUser.onClick.RemoveAllListeners();
        btnUser.onClick.AddListener(() =>
        {
            ToggleProjects(true);
            pnlMoreOptions.SetActive(false);
            MorePanelOpen = false;
            CloseProductsList();
        });
        btnDemo.onClick.RemoveAllListeners();
        btnDemo.onClick.AddListener(() =>
        {
            ToggleProjects(false);
            pnlMoreOptions.SetActive(false);
            MorePanelOpen = false;
            CloseProductsList();
        });



        if (!string.IsNullOrEmpty(app.account.CURRENT_ROOM_ID))
        {
            //Debug.Log("yo");
            //AppComponent.Instance.Navigate(AppPage.DesignScene);
            //return;
        }


        btnEdit.onClick.RemoveAllListeners();
        btnEdit.onClick.AddListener(() =>
        {
            Debug.Log("yo");
            app.CurrentSceneType = SceneType.OldSchool;
            AppComponent.Instance.Navigate(AppPage.Designer);
        });
        btnCancel.onClick.RemoveAllListeners();
        btnCancel.onClick.AddListener(() =>
        {
            pnlMoreOptions.SetActive(false);
            MorePanelOpen = false;
        });

        btnMore_Edit.onClick.RemoveAllListeners();
        btnMore_Edit.onClick.AddListener(() =>
        {
            widget.LoaderClose();
            Messenger<string, bool>.Broadcast(AnalyticsCalls.SceneConfirmationResponse, app.account.CURRENT_VERSION_ID, true);
            widget.Loader("Loading..");
            widget.UpdateLoaderProgress(0.01f);

            app.CurrentSceneType = SceneType.OldSchool;
            AppComponent.Instance.Navigate(AppPage.Designer);
        });

        btnMore_ProductList.onClick.RemoveAllListeners();
        btnMore_ProductList.onClick.AddListener(async () =>
        {
            pnlMoreOptions.SetActive(false);
            await OpenProductListAsync();
            MorePanelOpen = false;
        });
        btnSignout.onClick.RemoveAllListeners();
        btnSignout.onClick.AddListener(SignOut);

        btnDemo_signin.onClick.RemoveAllListeners();
        btnDemo_signin.onClick.AddListener(GotoAuthScene);

        btnDemo_signup.onClick.RemoveAllListeners();
        btnDemo_signup.onClick.AddListener(GotoSignUp);

        btnFillReq.onClick.RemoveAllListeners();
        btnFillReq.onClick.AddListener(GotoSignUp);

        await SetupList();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.T))
        {
            // app.Navigate(AppPage.Test);
        }
    }

    public void OnDisable()
    {
        bridge.onJSCallback.RemoveAllListeners();
        // bridge.onAndroidBackButtonPressed.RemoveAllListeners();
        if (cts != null)
            cts.Cancel();
    }

    public void SignOut()
    {
        AppComponent.Instance.SignOut();

    }

    async Task SetupList()
    {

        widget.Loader("Loading Projects..");
        widget.UpdateLoaderProgress(0.2f);

        foreach (Transform t in listContainer)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in pnlDemoList)
        {
            Destroy(t.gameObject);
        }
        listContainer.DestroyChildren();
        pnlDemoList.DestroyChildren();
        var projects = await app.server.artifact.GetMyProjects();

        RoleManager(projects);
        //AppConfig.PilotDemoConfig.ProjectId = projects[0]._id;

        // Debug.Log("Loading " + projects[0]._id);
        widget.UpdateLoaderProgress(0.5f);

        /*if (app.account.CRED_ISDEMO == string.Empty && currentState == UserState.demo)
        {
            Debug.Log("Falad e 3");
        }
        else*/
        if (currentState == UserState.ready)
        {

            var centers = await app.server.artifact.GetProjectRoomsAndVersions();

            print("Room versions Count: " + centers.Count);

            if (skipIfSingle && centers.Count == 1)
            {
                app.CurrentSceneType = SceneType.OldSchool;
                app.account.CURRENT_ROOM_ID = centers[0].room._id;
                app.account.CURRENT_ROOM_NAME = centers[0].room.name;
                app.account.CURRENT_VERSION_ID = centers[0].versions[0]._id;
                Debug.Log("Defaulted, selecting: " + centers[0].versions[0]._id);
                AppComponent.Instance.Navigate(AppPage.Designer);
                return;
            }
            List<Task> versionImageLoadTasks = new List<Task>();
            foreach (var roomAndVersions in centers)
            {
                foreach (var versionData in roomAndVersions.versions)
                {
                    Transform go = Instantiate(prefListElem, listContainer).transform;
                    go.localScale = Vector3.one;
                    go.position = Vector3.zero;
                    go.rotation = Quaternion.identity;

                    string s = roomAndVersions.room.name + ", " + versionData.name;
                    s.ToTitleCase();
                    go.Find("pnlName").Find("Text").GetComponent<TextMeshProUGUI>().text = s;
                    go.Find("pnlName").transform.Find("Text").GetComponent<TextMeshProUGUI>().text = s;
                    if (go.Find("pnlImage").Find("Image").GetComponent<Image>() != null)
                        versionImageLoadTasks.Add(DownloadTex(go.Find("pnlImage").Find("Image").GetComponent<Image>(), roomAndVersions.room._id, versionData._id));

                    go.GetComponent<Button>().onClick.RemoveAllListeners();
                    go.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        app.account.CURRENT_ROOM_ID = roomAndVersions.room._id;// center._id;
                        app.account.CURRENT_ROOM_NAME = roomAndVersions.room.name;// center.name;
                        app.account.CURRENT_VERSION_ID = versionData._id;
                        Debug.Log("Clicked, selecting: " + versionData._id);
                        SceneExitConfirmation();
                    });

                    go.Find("pnlButtons").Find("btnEdit").GetComponent<Button>().onClick.RemoveAllListeners();
                    go.Find("pnlButtons").Find("btnEdit").GetComponent<Button>().onClick.AddListener(() =>
                    {
                        app.account.CURRENT_ROOM_ID = roomAndVersions.room._id;// center._id;
                        app.account.CURRENT_ROOM_NAME = roomAndVersions.room.name;// center.name;
                        app.account.CURRENT_VERSION_ID = versionData._id;
                        Debug.Log("Clicked, selecting: " + versionData._id);
                        SceneExitConfirmation();
                    });

                    go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.RemoveAllListeners();
                    go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.AddListener(() =>
                    {
                        app.account.CURRENT_ROOM_ID = roomAndVersions.room._id;// center._id;
                        app.account.CURRENT_ROOM_NAME = roomAndVersions.room.name;// center.name;
                        app.account.CURRENT_VERSION_ID = versionData._id;
                        OpenMorePanel();
                    });


                    go.Find("pnlButtons").Find("btnProductList").GetComponent<Button>().onClick.RemoveAllListeners();
                    go.Find("pnlButtons").Find("btnProductList").GetComponent<Button>().onClick.AddListener(async () =>
                    {
                        app.account.CURRENT_ROOM_ID = roomAndVersions.room._id;// center._id;
                        app.account.CURRENT_ROOM_NAME = roomAndVersions.room.name;// center.name;
                        app.account.CURRENT_VERSION_ID = versionData._id;

                        await OpenProductListAsync();
                    });



                }
            }

            await Task.WhenAll(versionImageLoadTasks);
        }
        //temporary show all designs to internal team
        else
        {
            var centers = await app.server.artifact.GetAllCustomerDesigns();


            Debug.Log("Current ip address: " + HomefulyExtensions.GetLocalIPAddress().ToString());
            //print("idle Count: " + JsonConvert.SerializeObject(centers));
            List<Task> versionImageLoadTasks = new List<Task>();

            pnlUserReady.SetActive(true);
            pnlUserWaiting.SetActive(false);
            pnlDemoUser.SetActive(false);

            foreach (var designs in centers)
            {
                Transform go = Instantiate(prefListElem, listContainer).transform;
                go.localScale = Vector3.one;
                go.position = Vector3.zero;
                go.rotation = Quaternion.identity;

                string s = designs.name; // .room.name + ", " + versionData.name;
                s.ToTitleCase();
                go.Find("pnlName").Find("Text").GetComponent<TextMeshProUGUI>().text = s;
                go.Find("pnlName").transform.Find("Text").GetComponent<TextMeshProUGUI>().text = s;
                if (go.Find("pnlImage").Find("Image").GetComponent<Image>() != null)
                    versionImageLoadTasks.Add(DownloadProductTexture("roomversions/" + designs.room + "/" + designs._id + "/" + designs._id + "_c.jpg", go.Find("pnlImage").Find("Image").GetComponent<Image>()));


                go.GetComponent<Button>().onClick.RemoveAllListeners();
                go.GetComponent<Button>().onClick.AddListener(() =>
                {
                    app.CurrentSceneType = SceneType.OldSchool;
                    app.account.CURRENT_ROOM_ID = designs.room;// center._id;
                    app.account.CURRENT_ROOM_NAME = designs.name;// center.name;
                    app.account.CURRENT_VERSION_ID = designs._id;
                    Debug.Log("Clicked, selecting: " + designs._id);
                    SceneExitConfirmation();
                });

                go.Find("pnlButtons").Find("btnEdit").GetComponent<Button>().onClick.RemoveAllListeners();
                go.Find("pnlButtons").Find("btnEdit").GetComponent<Button>().onClick.AddListener(() =>
                {
                    app.CurrentSceneType = SceneType.OldSchool;
                    app.account.CURRENT_ROOM_ID = designs.room;// center._id;
                    app.account.CURRENT_ROOM_NAME = designs.name;// center.name;
                    app.account.CURRENT_VERSION_ID = designs._id;
                    Debug.Log("Clicked, selecting: " + designs._id);
                    SceneExitConfirmation();
                });

                go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.RemoveAllListeners();
                go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.AddListener(() =>
                {
                    app.CurrentSceneType = SceneType.OldSchool;
                    app.account.CURRENT_ROOM_ID = designs.room;// center._id;
                    app.account.CURRENT_ROOM_NAME = designs.name;// center.name;
                    app.account.CURRENT_VERSION_ID = designs._id;
                    OpenMorePanel();
                });


                go.Find("pnlButtons").Find("btnProductList").GetComponent<Button>().onClick.RemoveAllListeners();
                go.Find("pnlButtons").Find("btnProductList").GetComponent<Button>().onClick.AddListener(async () =>
                {
                    app.CurrentSceneType = SceneType.OldSchool;
                    app.account.CURRENT_ROOM_ID = designs.room;// center._id;
                    app.account.CURRENT_ROOM_NAME = designs.name;// center.name;
                    app.account.CURRENT_VERSION_ID = designs._id;

                    await OpenProductListAsync();
                });


            }
            await Task.WhenAll(versionImageLoadTasks);
        }
        widget.UpdateLoaderProgress(0.7f);
        widget.LoaderClose();

        //DEMO DESIGNS
        var democenters = await app.server.artifact.GetDemoDesigns();






        List<Task> versionImageLoadTasks1 = new List<Task>();
        foreach (var roomAndVersions in democenters)
        {


            Transform go = Instantiate(prefListElem, pnlDemoList).transform;
            go.localScale = Vector3.one;
            go.position = Vector3.zero;
            go.rotation = Quaternion.identity;

            string s = roomAndVersions.name; //roomAndVersions.room.name; //+ ", " + roomAndVersions.name;


            s.ToTitleCase();
            go.Find("pnlName").Find("Text").GetComponent<TextMeshProUGUI>().text = s;

            go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.RemoveAllListeners();
            go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.AddListener(() =>
            {
                SetRoomValues(roomAndVersions.room._id, roomAndVersions.room.name, roomAndVersions._id);
                AppConfig.PilotDemoConfig.ProjectId = roomAndVersions.room.project;
                OpenMorePanel();
            });

            go.Find("pnlButtons").Find("btnEdit").GetComponent<Button>().onClick.RemoveAllListeners();
            go.Find("pnlButtons").Find("btnEdit").GetComponent<Button>().onClick.AddListener(() =>
            {
                SetRoomValues(roomAndVersions.room._id, roomAndVersions.room.name, roomAndVersions._id);
                AppConfig.PilotDemoConfig.ProjectId = roomAndVersions.room.project;
                SceneExitConfirmation();
                Debug.Log("Clicked, selecting: " + roomAndVersions._id);

            });

            go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.RemoveAllListeners();
            go.Find("pnlName").Find("btnMore").GetComponent<Button>().onClick.AddListener(() =>
            {
                SetRoomValues(roomAndVersions.room._id, roomAndVersions.room.name, roomAndVersions._id);
                AppConfig.PilotDemoConfig.ProjectId = roomAndVersions.room.project;
                OpenMorePanel();
            });


            go.Find("pnlButtons").Find("btnProductList").GetComponent<Button>().onClick.RemoveAllListeners();
            go.Find("pnlButtons").Find("btnProductList").GetComponent<Button>().onClick.AddListener(async () =>
            {

                SetRoomValues(roomAndVersions.room._id, roomAndVersions.room.name, roomAndVersions._id);
                ProjectID = roomAndVersions.room.project;
                AppConfig.PilotDemoConfig.ProjectId = roomAndVersions.room.project;
                RoomName = roomAndVersions.name.ToTitleCase();
                await OpenProductListAsync();

            });

            go.GetComponent<Button>().onClick.RemoveAllListeners();
            go.GetComponent<Button>().onClick.AddListener(() =>
            {
                SetRoomValues(roomAndVersions.room._id, roomAndVersions.room.name, roomAndVersions._id);
                AppConfig.PilotDemoConfig.ProjectId = roomAndVersions.room.project;
                SceneExitConfirmation();
                Debug.Log("Clicked, selecting: " + roomAndVersions._id);

            });

            if (go.Find("pnlImage").Find("Image").GetComponent<Image>() != null)
                versionImageLoadTasks1.Add(DownloadTextures(go.Find("pnlImage").Find("Image").GetComponent<Image>(), roomAndVersions.room._id, roomAndVersions._id, roomAndVersions.room.project));


        }
        await Task.WhenAll(versionImageLoadTasks1);

    }

    async Task DownloadTextures(Image texture, string roomid, string versionid, string projectid)
    {
        cts = new CancellationTokenSource();
        try
        {

            GameObject loader = texture.gameObject.transform.parent.Find("elemLoader").gameObject;
            loader.SetActive(true);
            List<IDesignimage> im = await app.server.artifact.GetDesignRenders(projectid, roomid, versionid);

            if (im == null)
            {
                Debug.Log("null hey ye");
                return;
            }

            foreach (IDesignimage imm in im)
            {
                //Texture tex = await app.server.web.GetTexture(imm.files[1].url.Replace("http://api.homefuly.com/", "").Replace("https://homefuly.com:3443/", ""));
                Texture2D tex = (Texture2D)await app.server.web.GetTexture(imm.files[1].url.Replace("https://api.homefuly.com/", "").Replace("https://homefuly.com:3443/", ""));
                //Debug.Log("This is what lol:" + imm.files[1].url.Replace("http://api.homefuly.com/", ""));

                if (tex != null && texture != null)
                {
                    texture.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));
                }

                else
                {
                    texture.sprite = placeHolder;
                    Debug.Log("nullrr hey ye");
                }
                break;
            }
            loader.SetActive(false);
        }
        catch (Exception) { }

        cts = null;

    }

    async Task DownloadTex(Image texture, string roomid, string versionid)
    {
        cts = new CancellationTokenSource();
        try
        {
            GameObject loader = texture.gameObject.transform.parent.Find("elemLoader").gameObject;
            Texture2D tex = (Texture2D)await app.server.artifact.GetVersionCoverImage(roomid, versionid);

            if (tex != null && texture != null)
            {
                //go.transform.Find("btnDesign/elemLoader").gameObject.SetActive(false);
                //texture.texture = tex;
                texture.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));

            }
            loader.SetActive(false);
        }
        catch (OperationCanceledException) { }
        cts = null;

    }

    async Task DownloadProductTexture(string texture, Image img)
    {
        cts = new CancellationTokenSource();
        Transform elemLoader = img.transform.Find("elemLoader");
        if (elemLoader)
            elemLoader.gameObject.SetActive(true);
        try
        {
            Texture2D tex = (Texture2D)await app.server.web.GetTexture(texture);
            if (tex == null)
            {
                Debug.Log("nibba its null");
                img.sprite = placeHolder;
            }
            if (tex != null)// && texture != null)
            {

                img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));

            }
        }
        catch (OperationCanceledException) { }
        if (elemLoader)
            elemLoader.gameObject.SetActive(false);
        cts = null;

    }

    private void OpenMorePanel()
    {
        if (MorePanelOpen)
        {
            MorePanelOpen = false;
            pnlMoreOptions.SetActive(false);
            return;
        }
        pnlMoreOptions.SetActive(true);
        MorePanelOpen = true;

    }

    private async Task OpenProductListAsync()
    {
        pnlViewProducts.gameObject.transform.Find("pnlImage").Find("pnlSpacer").Find("elemLoader").gameObject.SetActive(true);
        Messenger<AppPage>.Broadcast(AnalyticsCalls.ProductListOpened, AppPage.Home);
        if (ProductPanelOpen == true)
        {
            pnlViewProducts.SetActive(false);
            ProductPanelOpen = false;
            return;
        }
        pnlViewProducts.SetActive(true);
        ProductPanelOpen = true;
        pnlViewProducts.gameObject.transform.Find("pnlImage").Find("Title").GetComponent<TextMeshProUGUI>().text = RoomName;

        foreach (Transform t in listProducts)
        {
            Destroy(t.gameObject);
        }


        List<IAssetEntry> result = await GetProductsList();

        result = result.GroupBy(x => x.asset.name).Select(x => x.First()).ToList();

        productsScrollRect.normalizedPosition = new Vector2(0, 0);


        if (result.Count < 1)
        {
            Transform go = Instantiate(prefProductElem, listProducts).transform;
            go.localScale = Vector3.one;
            go.position = Vector3.zero;
            go.rotation = Quaternion.identity;
            go.Find("Image").Find("elemLoader").gameObject.SetActive(false);
            go.Find("Panel").Find("txtProductName").GetComponent<TextMeshProUGUI>().text = "No Products in Room.";
            Destroy(go.Find("Panel").Find("txtProductPrice").gameObject);
            Destroy(go.Find("btnShop").gameObject);
        }


        List<Task> ProductImageDownloadTask = new List<Task>();
        foreach (var asset in result)
        {
            if (!asset.inuse || !asset.invoice || asset.asset.price.customer <= 0f || asset.asset.currency != "USD" || asset.asset.category == AppConfig.PilotDemoConfig.NonShoppableCatId)
            {
                //Debug.Log("Asset skipped: " + asset.asset.name);
                continue;
            }

            Transform go = Instantiate(prefProductElem, listProducts).transform;
            go.localScale = Vector3.one;
            go.position = Vector3.zero;
            go.rotation = Quaternion.identity;

            string s = asset.asset.name.ToTitleCase();
            s.ToTitleCase();

            go.Find("Panel").Find("txtProductName").GetComponent<TextMeshProUGUI>().text = s;

            string url = asset.asset.designedBy.productUrl;
            go.Find("btnShop").GetComponent<Button>().onClick.RemoveAllListeners();
            go.Find("btnShop").GetComponent<Button>().onClick.AddListener(() =>
            {
                //Messenger<IAsset, AppPage>.Broadcast(StaticFunctionCalls.ShopButtonClicked, asset.asset, AppPage.Home);
                ////Application.OpenURL(url);
                //InAppBrowser.OpenURL(url);
                //Debug.Log(url);

                HomefulyExtensions.OpenProductStorePage(asset.asset, Homefuly.Analytics.ShopButtonType.legacy);
            });


            s = asset.asset.price.customer.ToString();
            if (asset.asset.currency == "USD")
                go.Find("Panel").Find("txtProductPrice").GetComponent<TextMeshProUGUI>().text = "$ " + s;
            else
            {
                try
                {
                    Destroy(go);
                }
                catch (Exception)
                { }//go.Find("Panel").Find("txtProductPrice").GetComponent<TextMeshProUGUI>().text = "INR. " + s;
                continue;
            }

            ProductImageDownloadTask.Add(DownloadProductTexture("/assets/" + asset.asset._id + "/" + asset.asset._id + ".jpg", go.Find("Image").GetComponent<Image>()));

        }

        Debug.Log("Asset Images to download : " + ProductImageDownloadTask.Count);
        pnlViewProducts.gameObject.transform.Find("pnlImage").Find("Title").GetComponent<TextMeshProUGUI>().text = RoomName;
        Image img = pnlViewProducts.gameObject.transform.Find("pnlImage").Find("pnlSpacer").Find("Image").GetComponent<Image>();


        await DT(img, app.account.CURRENT_ROOM_ID, app.account.CURRENT_VERSION_ID);
        async Task DT(Image texture, string roomid, string versionid)
        {
            pcts = new CancellationTokenSource();
            try
            {
                List<IDesignimage> im = await app.server.artifact.GetDesignRenders(ProjectID, roomid, versionid);
                if (im == null)
                {
                    texture.sprite = placeHolder;
                    return;
                }

                foreach (IDesignimage imm in im)
                {


                    Texture2D tex = (Texture2D)await app.server.web.GetTexture(imm.files[1].url.Replace("https://api.homefuly.com/", "").Replace("https://homefuly.com:3443/", ""));

                    if (tex != null)
                    {
                        if (pnlViewProducts.gameObject.activeInHierarchy)
                            texture.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));

                    }

                    else
                    {
                        Debug.Log("nullrr hey ye");
                    }
                    break;

                }
                pnlViewProducts.gameObject.transform.Find("pnlImage").Find("pnlSpacer").Find("elemLoader").gameObject.SetActive(false);
            }
            catch (OperationCanceledException) { }
            pcts = null;

        }

        pnlViewProducts.gameObject.transform.Find("pnlImage").Find("pnlSpacer").Find("Image").GetComponent<Image>().sprite = img.sprite;

        await Task.WhenAll(ProductImageDownloadTask);

    }


    async Task<List<IAssetEntry>> GetProductsList()
    {

        IRoomDesign result = await AppComponent.Instance.server.artifact.GetRoomDesignAsync(app.account.CURRENT_ROOM_ID, app.account.CURRENT_VERSION_ID);
        Debug.Log(result.assets);
        return result.assets;

    }

    private void CloseProductsList()
    {
        if (pcts != null)
            pcts.Cancel();
        pnlViewProducts.SetActive(false);
        pnlViewProducts.gameObject.transform.Find("pnlImage").Find("pnlSpacer").Find("Image").GetComponent<Image>().sprite = null;
        ProductPanelOpen = false;
    }

    private void ToggleProjects(bool b)
    {

        Color almostBlack = new Color(57 / 255, 57 / 255, 57 / 255);
        btnUser.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = almostBlack;
        btnDemo.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = almostBlack;

        //Color(116, 167, 192);
        if (b) // myprojects
        {
            //Messenger<string>.Broadcast(AnalyticsCalls.CenterTabSwitch, "My_Projects");
            btnUser.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = Color.white;
            myprojectsImg.gameObject.SetActive(true);
            demoImg.gameObject.SetActive(false);
            pnlDemoListContainer.gameObject.SetActive(false);
            pnlCenterLists.gameObject.SetActive(true);
            app.openedTab = "myprojects";
        }
        else  // demo 
        {
            //Messenger<string>.Broadcast(AnalyticsCalls.CenterTabSwitch, "Demo_Designs");
            btnDemo.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = Color.white;
            demoImg.gameObject.SetActive(true);
            myprojectsImg.gameObject.SetActive(false);
            pnlDemoListContainer.gameObject.SetActive(true);
            pnlCenterLists.gameObject.SetActive(false);
            app.openedTab = "demo";
        }

        //Debug.Log("Opened tab: " + app.openedTab);
    }

    private void SetRoomValues(string roomid, string roomname, string versionid)
    {
        app.account.CURRENT_ROOM_ID = roomid;// center._id;
        app.account.CURRENT_ROOM_NAME = roomname;// center.name;
        app.account.CURRENT_VERSION_ID = versionid;
    }

    private void SceneExitConfirmation()
    {
        //widget.Confirm("Edit mode", "Are you sure you want to enter edit mode?", (bool ans) =>
        //{
        //    Messenger<string, bool>.Broadcast(AnalyticsCalls.SceneConfirmationResponse, app.account.CURRENT_VERSION_ID,ans);
        //    if (ans)
        //    {
        //        widget.LoaderClose();
        //        AppComponent.Instance.Navigate(AppPage.DesignScene);
        //    }
        //});
        //bool k = false;
        widget.Alert("D I Y", "Design it Yourself?", () =>
        {
            widget.LoaderClose();
            //Messenger<string, bool>.Broadcast(AnalyticsCalls.SceneConfirmationResponse, app.account.CURRENT_VERSION_ID, true);
            widget.Loader("Loading..");
            widget.UpdateLoaderProgress(0.01f);

            app.CurrentSceneType = SceneType.OldSchool;
            AppComponent.Instance.Navigate(AppPage.Designer);
            //k = true;
        },
        "Start", true);

        //Debug.Log("K is :" + k);
    }


    private void GotoAuthScene()
    {
        AppComponent.Instance.SignOut(false);
        //AppComponent.Instance.Navigate(AppPage.Auth);
    }


    private void GotoSignUp()
    {

        InAppBrowser.OpenURL("https://homefuly.com/#/home");
    }



    public void RoleManager(List<IProjects> projects)
    {

        aspectRatio = (float)Screen.height / Screen.width;
        //Debug.Log("Aspect Ratio is  : " + aspectRatio);


        pnlDemoList.gameObject.SetActive(true);
        pnlDemoUser.SetActive(false);
        pnlUserReady.SetActive(false);
        pnlUserWaiting.SetActive(false);
        pnlSurveryPending.SetActive(false);
        pnlPaymentPending.SetActive(false);
        pnlProjectPending.SetActive(false);


        int f1 = 1, f2 = 1;

        foreach (var p in projects)
        {

            if (p.quiz.Length <= 0)
            {
                //Debug.Log("Quiz status not there");
                currentState = UserState.pending;
                f1 = 0;
                break;
            }

            if (!p.paymentDone)
            {
                currentState = UserState.paymentPending;
                f1 = 0;
                break;
            }

            foreach (var q in p.quiz)
            {
                //Debug.Log("Quiz status is: " + q.status);
                if (q.status != "completed")
                {
                    //Debug.Log("Quiz not completed");
                    currentState = UserState.pending;
                    f1 = 0;
                    break;
                }
            }

            if (currentState == UserState.demo)
            {
                foreach (var q in p.phase)
                {
                    //Debug.Log("Design phases are : " + q.status);
                    if (q.status != "completed")
                    {
                        //Debug.Log("Design phases not completed");
                        currentState = UserState.waiting;
                        f2 = 0;
                        break;
                    }
                }
            }


        }

        if (f1 == 1 && f2 == 1)
        {
            currentState = UserState.ready;
            //Debug.Log("Design is ready to view");

        }

        if (app.account.CRED_ISDEMO == string.Empty)
        {
            currentState = UserState.demo;
        }

        if (projects.Count <= 0)
        {
            currentState = UserState.projectPending;
        }
        else
        {
            AppConfig.PilotDemoConfig.ProjectId = projects[0]._id;
        }




        switch (currentState)
        {

            case UserState.demo: //if Demo


                {
                    btnSignout.onClick.RemoveAllListeners();
                    btnSignout.onClick.AddListener(() =>
                    {
                        AppComponent.Instance.SignOut(false);//.Navigate(AppPage.Auth);
                    });
                    btnSignout.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Sign in";
                    pnlDemoUser.SetActive(true);
                    btnDemo_signin.onClick.RemoveAllListeners();
                    btnDemo_signin.onClick.AddListener(GotoAuthScene);

                    btnDemo_signup.onClick.RemoveAllListeners();
                    btnDemo_signup.onClick.AddListener(GotoSignUp);
                    currentState = UserState.demo;
                    if (app.openedTab == string.Empty)
                        ToggleProjects(false);
                    if (aspectRatio < 1.77f)
                    {
                        pnlDemoUser.GetComponent<Image>().sprite = tabletBGImage;
                        pnlDemoUser.GetComponent<AspectRatioFitter>().aspectRatio = 1.33f;
                    }

                }
                break;

            case UserState.waiting: //if Waiting
                {
                    btnSignout.onClick.RemoveAllListeners();
                    btnSignout.onClick.AddListener(() => AppComponent.Instance.SignOut());
                    btnSignout.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Sign out";
                    pnlUserWaiting.SetActive(true);
                    currentState = UserState.waiting;
                    if (app.openedTab == string.Empty)
                        ToggleProjects(true);
                    if (aspectRatio < 1.77f)
                    {
                        pnlUserWaiting.GetComponent<Image>().sprite = tabletBGImage;
                        pnlUserWaiting.GetComponent<AspectRatioFitter>().aspectRatio = 1.33f;
                    }

                }
                break;

            case UserState.pending://if Survey Pending / Predesign
                {
                    btnSignout.onClick.RemoveAllListeners();
                    btnSignout.onClick.AddListener(() => AppComponent.Instance.SignOut());
                    btnSignout.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Sign out";
                    pnlSurveryPending.SetActive(true);
                    currentState = UserState.pending;
                    if (app.openedTab == string.Empty)
                        ToggleProjects(true);
                    if (aspectRatio < 1.77f)
                    {
                        pnlSurveryPending.GetComponent<Image>().sprite = tabletBGImage;
                        pnlSurveryPending.GetComponent<AspectRatioFitter>().aspectRatio = 1.33f;
                    }

                }
                break;


            case UserState.ready://if Ready
                {
                    btnSignout.onClick.RemoveAllListeners();
                    btnSignout.onClick.AddListener(() => AppComponent.Instance.SignOut());
                    btnSignout.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Sign out";
                    if (app.openedTab == string.Empty)
                        pnlUserReady.SetActive(true);
                    currentState = UserState.ready;


                }

                break;

            case UserState.projectPending: //if project has not been started 


                {
                    btnSignout.onClick.RemoveAllListeners();
                    btnSignout.onClick.AddListener(() =>
                    {
                        AppComponent.Instance.SignOut(true);//.Navigate(AppPage.Auth);
                    });
                    btnSignout.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Sign out";
                    pnlProjectPending.SetActive(true);
                    btnGetStarted.onClick.RemoveAllListeners();
                    btnGetStarted.onClick.AddListener(GotoSignUp);


                    currentState = UserState.projectPending;
                    if (app.openedTab == string.Empty)
                        ToggleProjects(true);
                    if (aspectRatio < 1.77f)
                    {
                        pnlProjectPending.GetComponent<Image>().sprite = tabletBGImage;
                        pnlProjectPending.GetComponent<AspectRatioFitter>().aspectRatio = 1.33f;
                    }

                }
                break;

            case UserState.paymentPending: //if project has not been started 


                {
                    btnSignout.onClick.RemoveAllListeners();
                    btnSignout.onClick.AddListener(() =>
                    {
                        AppComponent.Instance.SignOut(true);//.Navigate(AppPage.Auth);
                    });
                    btnSignout.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "Sign out";
                    pnlPaymentPending.SetActive(true);
                    btnPayment.onClick.RemoveAllListeners();
                    btnPayment.onClick.AddListener(GotoSignUp);


                    currentState = UserState.paymentPending;
                    if (app.openedTab == string.Empty)
                        ToggleProjects(true);
                    if (aspectRatio < 1.77f)
                    {
                        pnlPaymentPending.GetComponent<Image>().sprite = tabletBGImage;
                        pnlPaymentPending.GetComponent<AspectRatioFitter>().aspectRatio = 1.33f;
                    }

                }
                break;
        }

        //Debug.Log("Current state is:" + currentState);

        if (app.openedTab != string.Empty)
        {
            if (app.openedTab == "demo")
                ToggleProjects(false);
            else
                if (app.openedTab == "myprojects")
                ToggleProjects(true);
        }

        //Debug.Log("Tab to be opened: " + app.openedTab);
    }

    public enum UserState
    {
        demo,
        waiting,
        pending,
        ready,
        projectPending,
        paymentPending
    }


}


[System.Serializable]
public class IABResponse<T>
{
    public bool err;
    public string context;
    public T data;
}

[System.Serializable]
public class IABMDemoDesign
{
    public string pid;
    public string rid;
    public string vid;
}

[System.Serializable]
public class IABChallengeClass
{
    public string challenge;
 
}

[System.Serializable]
public class IABMUserDetails
{
    public string token;
    public string id;
    public string name;
    public string email;
    public string role;
}

[System.Serializable]
public class IABMIsHomepage
{
    public bool isHome;
}