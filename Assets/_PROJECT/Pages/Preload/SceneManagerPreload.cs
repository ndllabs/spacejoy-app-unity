﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Homefuly;

public class SceneManagerPreload : MonoBehaviour
{
    // Start is called before the first frame update
    AppComponent app;
    async void Start()
    {
        WidgetManager.Instance.Loader("Loading..");
        app = AppComponent.Instance;
        print("In Preload");

        var resD = await app.account.Connect();  
        
        if (!resD.err)
        {
            //print("Wmail: " + app.account.CRED_USERNAME + ", token : " + app.account.CRED_JWTTOKEN);
            app.Navigate(AppPage.Home);
        }
        else
        {
            //WidgetManager.Instance.LoaderClose();
            //WidgetManager.Instance.Alert("Loading Error", resD.data,()=>app.Navigate(AppPage.Preload));
            print("Invalid Creds");
            app.account.CRED_PASSWORD = string.Empty;
            WidgetManager.Instance.AuthPanelOpen();
        }

    }


    private void OnEnable()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

  
    // Update is called once per frame
    void Update()
    {

    }
}
