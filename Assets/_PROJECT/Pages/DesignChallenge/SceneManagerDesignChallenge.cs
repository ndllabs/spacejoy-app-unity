﻿/* Holds Control flow for scene DesignChallenge
 * 
 * prajwalshetty2018@gmail.com
 * */

using _unity.Events;
using Homefuly;
using Homefuly.Analytics;
using Homefuly.Events;
using Selector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TMPro;
using TouchMan;
using UnityEngine;
using SFC = StaticFunctionCalls;

public class SceneManagerDesignChallenge : MonoBehaviour
{
    #region Vars
    [Header("Prefabs")]
    public GameObject elemHotspot;
    public GameObject elemWallPoster;
    public GameObject elemSelector;

    [Header("Rendering")]
    public Light lightAmbient;
    public GameObject reflectionProbe;


    [Header("")]
    public Transform roomParent;
    public Transform assetsParent;

    public Camera mainCam;

    private AppComponent app;
    private ArtifactManager artifactManager;
    public IFloorPlanProcessor floorplanData;
    System.Diagnostics.Stopwatch timer;
    private IDesignChallenge currentDesignChallenge;
    private GameObject roomGo;
    #endregion

    #region Broadcasts

    /// <summary>
    /// Broadcasts when the Asset Swap action is "COMPLETE"
    /// </summary>
    /// <param name="hotpot">hotspot Class reference</param>
    private void Broadcast_ActionHotspotSwapped(IHotspot hotpot)
    {
        Messenger<IHotspot>.Broadcast(SFC.Action_HotspotSwapped, hotpot);
    }

    /// <summary>
    /// Broadcasts when the Asset Swap action is "COMPLETE"
    /// </summary>
    /// <param name="hotpot">hotspot Class reference</param>
    private void Broadcast_ActionWallfinishSwapped(IWallfinishHotspot hotpot)
    {
        Messenger<IWallfinishHotspot>.Broadcast(SFC.Action_WallfinishSwapped, hotpot);
    }

    /// <summary>
    /// Broadcasts when the Asset Delete action is "COMPLETE"
    /// </summary>
    /// <param name="hotpot">hotspot Class reference</param>
    private void Broadcast_ActionAssetDeleted(IHotspot hotpot)
    {
        Messenger<IHotspot>.Broadcast(SFC.Action_AssetDeleted, hotpot);
    }

    /// <summary>
    /// Broadcasts when the Asset Selected action is "COMPLETE"
    /// </summary>
    /// <param name="isSelected">is Asset Selected? or is it Deselected?</param>
    private void Broadcast_ActionAssetSelected(IHotspot hotpot, bool isSelected)
    {
        Messenger<IHotspot, bool>.Broadcast(SFC.ActionAssetSelected, hotpot, isSelected);
    }

    /// <summary>
    /// Broadcasts when the Asset Selected action is "COMPLETE"
    /// </summary>
    /// <param name="isSelected">is Asset Selected? or is it Deselected?</param>
    private void Broadcast_ActionWallfinishSelected(IWallfinishHotspot hotpot, bool isSelected)
    {
        Messenger<IWallfinishHotspot, bool>.Broadcast(SFC.Action_WallfinishSelected, hotpot, isSelected);
    }

    /// <summary>
    /// Broadcasts when the Design Submission action is "COMPLETE"
    /// </summary>
    private void Broadcast_ChallengeCompleted(string challengeName)
    {
        Messenger<string>.Broadcast(SFC.Action_DesignSubmited, app.db.designChallenge.name);
    }

    /// <summary>
    /// Broadcasts when the Wallfinish Apply action is "COMPLETE"
    /// </summary>
    /// <param name="wallspot">Wallfinish hotspot Class reference</param>
    private void Broadcast_ActionWallfinishApplied(IWallfinishHotspot wallspot)
    {
        Messenger<IWallfinishHotspot>.Broadcast(SFC.Action_WallfinishSwapped, wallspot);
    }

    /// <summary>
    /// Broadcasts when the Scene Load action is "COMPLETE"
    /// </summary>
    private void Broadcast_ActionSceneLoaded()
    {
        HomefulyEvents.OnSceneReady?.Invoke(string.Empty, SceneType.DesignChallenge);
    }

    private void Broadcast_OnActionFailed(ActionType actiontype, int errCode)
    {
        HomefulyEvents.OnActionFailed?.Invoke(actiontype, errCode);
    }
    #endregion


    #region Listners
    private void ClassListners()
    {
        /* IHotspot => Hotspot To Be Swapped
         * string: => Asset ID to be Swapped With
         * */
        Messenger<IHotspot, string>.AddListener(SFC.Request_HotspotSwap, OnAssetSwapRequest);

        /* IHotspot => Hotspot To Be Deleted
         * Note: Will only delete the asset in the hotspot, not the hostpot itself
         * */
        Messenger<IHotspot>.AddListener(SFC.Request_AssetDelete, OnAssetDeleteRequest);

        /* IHotspot => Hotspot To Be Selected/Deselcted
         * bool => Request asset Select(true) or asset deselect(false)
         * */
        Messenger<IHotspot, bool>.AddListener(SFC.Request_AssetSelect, OnAssetSelectRequest);

        /* IWallfinishHotspot => Hotspot To Be Selected/Deselcted
         * bool => Request asset Select(true) or asset deselect(false)
         * */
        Messenger<IWallfinishHotspot, bool>.AddListener(SFC.Request_WallfinishSelect, OnWallfinishSelect);

        /* 
         * */
        Messenger.AddListener(SFC.Request_DesignSubmit, OnDesignSubmitRequest);

        /* IWallfinishHotspot => Wallfinish Hotspot To Be Swapped
         * string: => Wallfinish ID to be Swapped With
         * */
        Messenger<IWallfinishHotspot, IWallfinish>.AddListener(SFC.Request_WallfinishApply, OnWallfinishSwapRequest);
    }

    private void OnWallfinishSelect(IWallfinishHotspot arg1, bool arg2)
    {
        if (arg2)
            SelectHotspot(arg1);
        else
            DeselectHotspot(arg1);
    }


    #endregion


    #region Unity Events

    private void Awake()
    {
        WidgetManager.Instance.Loader("Loading, Please Wait...", 50);
        HomefulyExtensions.SetScreenOrientation(true);

        OnStart();

    }

    private void OnEnable()
    {
        TouchManEvents.OnTap += OnTap;

        ClassListners();
    }

    private void Update()
    {
        // Close scene after back button
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("On back button pressed in android");
            // This message specifics needs to be discussed
            WidgetManager.Instance.Confirm("Exit Challenge", "Are you sure you want to leave the challenge?", (res) =>
            {
                if (res == "yes")
                {
                    PlayerPrefs.SetString("minimizeUnity", " ");
                    app.Navigate(AppPage.AppComponent);
                    // app.Navigate(AppPage.Home);
                }
                return;
            });
        }
    }


    private void OnDisable()
    {
        TouchManEvents.OnTap -= OnTap;

        Messenger<IHotspot, string>.RemoveListener(SFC.Request_HotspotSwap, OnAssetSwapRequest);
        Messenger<IHotspot>.RemoveListener(SFC.Request_AssetDelete, OnAssetDeleteRequest);
        Messenger<IHotspot, bool>.RemoveListener(SFC.Request_AssetSelect, OnAssetSelectRequest);
        Messenger.RemoveListener(SFC.Request_DesignSubmit, OnDesignSubmitRequest);
        Messenger<IWallfinishHotspot, IWallfinish>.RemoveListener(SFC.Request_WallfinishApply, OnWallfinishSwapRequest);
        Messenger<IWallfinishHotspot, bool>.RemoveListener(SFC.Request_WallfinishSelect, OnWallfinishSelect);

    }
    #endregion


    #region Core

    #region Tap/Select
    private void OnTap(GameObject tappedObject)
    {

        if (tappedObject == null)
        {
            if (app.db.IsCurrentObjectValid<IHotspot>())
                DeselectHotspot(app.db.currentObject as ISceneObject);
        }
        else if (tappedObject.tag.ToLower() == "hotspot")
        {
            var indexed_sceneAssets = app.db.designChallenge.baseAssets_Indexed;
            if (indexed_sceneAssets.ContainsKey(tappedObject.name))
            {
                if (app.db.IsCurrentObjectValid<ISceneObject>())
                    DeselectHotspot(app.db.currentObject);

                app.db.currentObject = indexed_sceneAssets[tappedObject.name];
                SelectHotspot(indexed_sceneAssets[tappedObject.name]);
            }
            else if (app.db.designChallenge.basePaints_Indexed.ContainsKey(tappedObject.name))
            {
                if (app.db.IsCurrentObjectValid<ISceneObject>())
                    DeselectHotspot(app.db.currentObject);

                app.db.currentObject = app.db.designChallenge.basePaints_Indexed[tappedObject.name];
                SelectHotspot(app.db.currentObject);
            }
            else
            {
                var errMsg = "[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(OnTap) + "] Unable to find the inputed hotspot id in current available hotspots";
                Debug.LogWarning(errMsg);
                Broadcast_OnActionFailed(ActionType.Select, (int)ErrorTracking.ErrorCodes.Invalid_ID); return;
            }
        }
        else if (tappedObject.tag == "Wall")
        {
            if (app.db.IsCurrentObjectValid<ISceneObject>())
                DeselectHotspot(app.db.currentObject as ISceneObject);

            foreach (var paint in app.db.designChallenge.basePaint)
            {
                if (paint.wall == tappedObject.name && !paint.isHotspot)
                    SelectHotspot(paint);
            }

        }
        else
        {
            if (app.db.IsCurrentObjectValid<ISceneObject>())
                DeselectHotspot(app.db.currentObject as ISceneObject);
        }


    }

    private void SelectHotspot(ISceneObject hotspot)
    {
        // IonicComms.FinishActivity("Unity Hidden");

        app.db.currentObject = hotspot;

        if (!app.db.IsCurrentObjectValid<ISceneObject>())
        {
            Debug.LogWarning("Asset Select failed, Reason: Object not valid");
            return;
        }

        var go = app.db.currentObject.objRef;

        if ((hotspot is IHotspot && (hotspot as IHotspot).isHotspot) ||
            (hotspot is IWallfinishHotspot && (hotspot as IWallfinishHotspot).isHotspot))
            go.transform.GetChild(0).GetComponent<Collider>().isTrigger = false;
        else
            go.transform.GetComponent<Collider>().isTrigger = false;

        if (go.transform.GetComponentInChildren<SelectorMain>())
            go.transform.GetComponentInChildren<SelectorMain>().SelectAsset();

        if (app.db.currentObject is IHotspot)
            Broadcast_ActionAssetSelected(hotspot as IHotspot, true);
        else if (app.db.currentObject is IWallfinishHotspot)
            Broadcast_ActionWallfinishSelected(hotspot as IWallfinishHotspot, true);
    }

    private void DeselectHotspot(ISceneObject hotspot)
    {
        if (!app.db.IsCurrentObjectValid()) return;

        var go = app.db.currentObject.objRef;


        if (hotspot is IHotspot && (hotspot as IHotspot).isHotspot == true ||
            hotspot is IWallfinishHotspot && (hotspot as IWallfinishHotspot).isHotspot == true)
        {
            go.transform.GetChild(0).GetComponent<Collider>().isTrigger = true;
        }

        if (go.GetComponent<Collider>())
        {
            go.GetComponent<Collider>().isTrigger = true;
        }

        if (go.transform.GetComponentInChildren<SelectorMain>())
            go.transform.GetComponentInChildren<SelectorMain>().CloseAssetSelection();

        if (go.transform.GetComponent<AssetTransformManager>())
            go.GetComponent<AssetTransformManager>().UpdateIsAssetSelected(false);

        if (app.db.currentObject is IHotspot)
            Broadcast_ActionAssetSelected(hotspot as IHotspot, false);
        else if (app.db.currentObject is IWallfinishHotspot)
            Broadcast_ActionWallfinishSelected(hotspot as IWallfinishHotspot, false);


        app.db.currentObject = null;
    }

    #endregion

    #region Design Load

    /// <summary>
    /// Once the DB is ready, Begin Scene Load
    /// </summary>
    private async void OnStart()
    {
        WidgetManager.Instance.Loader("Loading, Please Wait...", 50);
        await ChangeSceneState(DesignChallengeState.Load);
        WidgetManager.Instance.LoaderClose();
        // IonicComms.FinishActivity("Unity Hidden");
    }


    async Task FSM_Load()
    {
        timer = new System.Diagnostics.Stopwatch();
        timer.Start();


        app = AppComponent.Instance;

        if (!await app.db.RefreshDb(SceneType.DesignChallenge))
        {
            WidgetManager.Instance.Alert("Challenge Load Failed", "Unable to load the challenge at the moment, please try again after some time", () =>
            {
                WidgetManager.Instance.LoaderClose();
                app.Navigate(AppPage.AppComponent);
            });
        }

        HomefulyAnalyticsEvent.OnDesignOpened?.Invoke(SceneType.DesignChallenge);

        /* Define all variables
         * */
        artifactManager = new ArtifactManager(app.server);
        currentDesignChallenge = app.db.designChallenge;
        // app.account.CURRENT_CHALLENGE_ID = "";

        ChallengeManager.Instance.Init();


        var roomid = app.db.designChallenge.room;
        var versionid = app.account.CURRENT_VERSION_ID;
        var challengeid = app.account.CURRENT_CHALLENGE_ID;

        /* TO DO:
         * Init Camera, Set Position,
         * Init Light and Update GFX settings
         * */
        mainCam = Camera.main;
        ITransform.ToTransform(app.db.designChallenge.camera, mainCam.transform);

        /* Load All Asset/Hotspot And Apply All Paints 
         * */
        await Task.WhenAll(LoadAllAssets(currentDesignChallenge.baseAssets), LoadRoomToScene(app.db.designChallenge.room));

        await LoadAllWallfinishs(currentDesignChallenge.basePaint);

        SetupLight(app.db.currentRoom.transform);

        await ChangeSceneState(DesignChallengeState.Play);

        HomefulyEvents.OnSceneReady?.Invoke(AppPage.Designer.ToString(), app.db.sceneType);
    }

    void SetupLight(Transform transRoom)
    {
        Bounds b = roomGo.transform.GetTotalRenderBounds();
        var center = b.center;
        lightAmbient.gameObject.SetActive(true);
        lightAmbient.transform.position = new Vector3(center.x, b.max.y - 0.45f, center.z);
        lightAmbient.GetComponent<Light>().intensity = 0.79f;

        RenderSettings.ambientLight = new Color32(0xBE, 0xBE, 0xBE, 0xBE);

        reflectionProbe.transform.position = b.center;
        reflectionProbe.GetComponent<ReflectionProbe>().size = b.size;
    }

    async Task LoadRoomToScene(string roomid)
    {
        /* Download and Load Room
        * */
        var roomRes = await artifactManager.DownloadFloorplan(roomid, roomParent.transform);

        if (roomRes.err || roomRes.gameObject == null)
        {
            Debug.LogError("[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(FSM_Load) + "] Scene Load Abrorted, Reason: Room Load Error");
            WidgetManager.Instance.Alert("Load Failed", "Sorry, Unable to open this design please try again later");
            return;
        }
        roomGo = roomRes.gameObject;

        artifactManager.PostProcessFloorplan(roomGo.transform, floorplanData);

        foreach (Transform trans in roomGo.transform)
        {
            if (trans.gameObject.tag == "Wall")
            {
                if (!trans.gameObject.GetComponent<Collider>())
                {
                    trans.gameObject.tag = "Untagged";
                    continue;
                }

                AttachSelector(trans, SelectorMain.SelectorMode.Normal);
                trans.GetComponentInChildren<SelectorMain>().safezonePercent = 0f;
                trans.GetComponentInChildren<SelectorMain>().MakeLineRenderersSeethrough();
            }


        }

        app.db.SetCurrentRoom(roomGo);

        var cam = Camera.main;
        var ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, 50))
        {
#if UNITY_EDITOR
            Debug.DrawRay(ray.origin,
                                  ray.direction.normalized * 50f,
                                  Color.red, 20f);
#endif
            // hide the wall in front of camera and all the windows and doors attached to it
            GameObject wallObject = raycastHit.collider.gameObject;
            Transform roomObject = wallObject.transform.parent;
            foreach(Transform childObj in roomObject)
            {
                if (childObj.name.Contains(wallObject.name + "_"))
                    childObj.gameObject.SetActive(false);
            }

            // old code hiding just the wall
            raycastHit.collider.gameObject.SetActive(false);
            cam.fieldOfView = 30;
        }
    }

    private void OnApplicationQuit()
    {
        //Analytics: time spent in design
        Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, SceneType.DesignChallenge);
        Debug.Log("Printing application quit");

    }

    void FSM_StartChallenge()
    {

    }

    #endregion

    #region CoreModules

    #region Wallfinish

    async Task LoadAllWallfinishs(List<IWallfinishHotspot> wallfinishs)
    {
        List<Task> paintLoadTasks = new List<Task>();
        foreach (var paintData in wallfinishs)
        {
            paintLoadTasks.Add(OnLoadApplyWallfinish(paintData));
        }

        await Task.WhenAll(paintLoadTasks);
    }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
    async Task OnLoadApplyWallfinish(IWallfinishHotspot wallfinishData)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
    {
        if (string.IsNullOrEmpty(wallfinishData.wall)) return;

        Debug.Log("Loading wall finish :" + wallfinishData.wall);

        var t = roomGo.transform.Find(wallfinishData.wall);
        if (t != null && t.GetComponent<MeshRenderer>())
        {
            GameObject go = Instantiate(elemHotspot);
            go.tag = "Hotspot";

            go.name = wallfinishData._id;

            go.transform.GetChild(0).tag = "Hotspot";
            go.transform.GetChild(0).name = wallfinishData._id;

            go.GetComponentInChildren<TextMeshPro>().text = "Paint";

            if (wallfinishData.hotspotTransform.position == Vector3.zero)
                go.transform.position = t.GetComponent<MeshRenderer>().bounds.center;
            else
            {
                wallfinishData.hotspotTransform.position.y = wallfinishData.hotspotTransform.position.y + 0.3f;
                go.transform.position = wallfinishData.hotspotTransform.position;
            }

            //Set Default Paint to White
            Material[] mat = t.GetComponent<MeshRenderer>().materials;
            foreach (Material ma in mat)
            {
                ma.mainTexture = null;
                ma.color = Color.white;
            }
            t.GetComponent<MeshRenderer>().materials = mat;


            go.transform.SetParent(assetsParent);
            go.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            app.db.designChallenge.basePaints_Indexed[wallfinishData._id].objRef = go;
        }
        else
            Debug.LogWarning("[OnLoadApplyWallfinish] Wall not found..!! Name: " + wallfinishData.wall);


    }
    GameObject DuringLoadSpawnWallpaper(Transform wall, Texture2D wallposter, ITransform trans)
    {
        if (!wall)
        { Debug.LogError("[DuringLoadSpawnWallpaper]Wall not Found..!"); return null; }

        GameObject go = Instantiate(elemWallPoster, wall.transform);
        go.layer = 20; //"Wallpaper"
        go.tag = "Poster";
        go.name = "Poster_" + wallposter.name;

        go.transform.eulerAngles = wall.GetPosAlignedRotationEuler(trans.position);

        go.transform.position = trans.position;
        go.transform.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Standard"));
        go.transform.GetComponent<MeshRenderer>().material.mainTexture = wallposter;

        go.transform.localScale = trans.scale;
        Messenger<GameObject>.Broadcast(StaticFunctionCalls.GetWallPosterisOn, wall.gameObject);
        go.GetComponent<PosterStateManager>().Init();
        go.GetComponent<PosterStateManager>().ClampToWall();

        return go;
    }
    #endregion

    #region IAsset

    async Task LoadAllAssets(List<IHotspot> baseAssets)
    {
        //var sortedHotspots = baseAssets.OrderByDescending(x => x.asset.mountType).ToList();

        foreach (var hot in baseAssets)
        {
            if (!hot.isHotspot)
                await LoadTimeAssetLoader(hot);
            else
                LoadTimeHotspotLoader(hot);
        }
    }

    void LoadTimeHotspotLoader(IHotspot hotspot)
    {
        GameObject go = Instantiate(elemHotspot);


        go.tag = "Hotspot";
        go.name = hotspot._id;

        go.transform.GetChild(0).tag = "Hotspot";
        go.transform.GetChild(0).name = hotspot._id;

        //temp Lans Mess: remove asap // => The Hotspot Prefab should have a Init Fn
        go.GetComponentInChildren<TextMeshPro>().text = app.db.categories.ContainsKey(hotspot.swapAssets[0].asset.subcategory) ? app.db.categories[hotspot.swapAssets[0].asset.subcategory].name : "Furniture";

        hotspot.transform.position.y = hotspot.transform.position.y + 0.3f;
        var pos = hotspot.transform.position;
        pos.y = hotspot.transform.position.y + (hotspot.swapAssets[0].asset.dimension.height * 0.3048f / 2);

        go.transform.position = pos;
        go.transform.SetParent(assetsParent);
        go.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        app.db.designChallenge.baseAssets_Indexed[hotspot._id].objRef = go;
    }


    async Task LoadTimeAssetLoader(IHotspot entry)
    {
        if (entry.isHotspot) return;
        IArtifactLoadAsyncOperationResult task;

        /* Downloads and loads asset given the assetid
         * */
        task = await LoadAssetAsync(entry.asset, assetsParent, entry.transform.position, entry.transform.rotation);

        if (task.gameObject == null) return;

        task.gameObject.tag = "Hotspot";
        task.gameObject.name = entry._id;
        app.db.designChallenge.baseAssets_Indexed[entry._id].objRef = task.gameObject;

        task = null;
    }


    async Task<IArtifactLoadAsyncOperationResult> LoadAssetAsync(IAsset asset, Transform parent, Vector3 position, Vector3 rotation, GameObject original = null)
    {
        var artRes = new IArtifactLoadAsyncOperationResult();

        if (asset == null) return artRes;

        artRes = await artifactManager.DownloadAsset(asset, parent, original);

        if (!artRes.err && artRes.gameObject != null)
        {
            // skip post processing if duplicated from existing asset
            if (original != null)
            {
                Destroy(artRes.gameObject.transform.Find("Selector(Clone)").gameObject);
            }

            //Setting updated At for asset cache SOS
            string str = "ASSET_UPDATE_CACHE_" + asset._id;
            PlayerPrefs.SetString(str, asset.updatedAt);

            artifactManager.PostProcessAsset(artRes.gameObject.transform, position, rotation);

            var atm = artRes.gameObject.GetComponent<AssetTransformManager>();

            if (atm.entry == null)
                atm.entry = new IAssetEntry() { asset = new IAsset() };

            atm.entry.entryId = artRes.gameObject.name;
            atm.entry.asset = asset;
            atm.Init(false);

            AttachSelector(artRes.gameObject.transform, SelectorMain.SelectorMode.Normal);
        }

        return artRes;
    }

    void AttachSelector(Transform trans, SelectorMain.SelectorMode mode)
    {
        var goSelector = Instantiate(elemSelector);
        goSelector.transform.SetParent(trans);

        goSelector.GetComponent<SelectorMain>().Init(mode, trans.name);
    }


    #endregion

    #endregion

    #region FSM

    private async Task ChangeSceneState(DesignChallengeState state)
    {
        try
        {
            switch (state)
            {
                case DesignChallengeState.Load:
                    Debug.Log("Rammed, Loading design..");
                    await FSM_Load();
                    break;
                case DesignChallengeState.Play:
                    FSM_StartChallenge();
                    break;
                case DesignChallengeState.End:
                    break;
            }
        }

        catch (OperationCanceledException e)
        {
            Debug.LogError("[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(ChangeSceneState) + "] Rammed, Critical Load Failure, ECODE 010" + e.Message);
        }

    }

    #endregion

    #region ListnerActions
    private void OnWallfinishSwapRequest(IWallfinishHotspot wallfinishData, IWallfinish dataRes)
    {
        var t = roomGo.transform.Find(wallfinishData.wall);
        if (t != null && t.GetComponent<MeshRenderer>())
        {
            if (!ColorUtility.TryParseHtmlString(dataRes.code, out Color color))
            {
                var errMsg = "[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(OnWallfinishSwapRequest) + "] Wallfinish Swap Failed. Due to Invalid Color Code, Unable To Parse. Code: " + dataRes.code;

                Debug.LogWarning(errMsg);
                Broadcast_OnActionFailed(ActionType.SwapWallfinish, (int)ErrorTracking.ErrorCodes.Invalid_InputData);
                return;
            }

            if (wallfinishData.isHotspot)
                Destroy(wallfinishData.objRef);

            Material[] mat = t.GetComponent<MeshRenderer>().materials;

            foreach (Material ma in mat)
            {
                ma.mainTexture = null;
                ma.color = color;
            }
            t.GetComponent<MeshRenderer>().materials = mat;

            var newHotspot = new IWallfinishHotspot
            {
                _id = wallfinishData._id,
                isHotspot = false,
                isMandatory = wallfinishData.isMandatory,
                swapWallFinish = wallfinishData.swapWallFinish,
                objRef = t.gameObject,
                transform = new ITransform(t.gameObject.transform),
                wall = wallfinishData.wall,
                wallfinish = dataRes
            };

            app.db.currentObject = newHotspot;
            SelectHotspot(app.db.currentObject);

            app.db.UpdateWallfinishHotspotEntry(newHotspot);

            Broadcast_ActionWallfinishSwapped(newHotspot);

            // IonicComms.FinishActivity("Unity Hidden");
        }
        else
        {
            var errMsg = "[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(OnWallfinishSwapRequest) + "] Wallfinish Swap Failed. Unable to find wall named " + wallfinishData.wall + " skipping wallfinish swap";
            Debug.LogWarning(errMsg); Broadcast_OnActionFailed(ActionType.SwapWallfinish, (int)ErrorTracking.ErrorCodes.Failed_GameobjectFind);
            return;
        }
    }



    private void OnAssetSelectRequest(IHotspot hotpot, bool toSelect)
    {
        if (toSelect)
            SelectHotspot(hotpot);
        else
            DeselectHotspot(hotpot);
    }

    private void OnDesignSubmitRequest()
    {
        if (!Debug.isDebugBuild && app.db.userSavedData.challengesCompleted.Contains(app.db.designChallenge._id))
        {
            HomefulyExtensions.SetScreenOrientation(true);
            Broadcast_OnActionFailed(ActionType.ChallengeSubmit, (int)ErrorTracking.ErrorCodes.Already_ChallengeCompleted);
            WidgetManager.Instance.Alert("Design Challenge", "You have already completed this design challenge before, please try some other design challenge");
            return;
        }

        if (ChallengeManager.Instance.IsChallengeComplete() && app.account.CRED_ISDEMO != string.Empty)
        {
            Debug.Log("Challenge completed sam " + app.db.designChallenge.name);

            ChallengeManager.Instance.RewardAndSaveCoin();
            OnSaveDesign();

            Broadcast_ChallengeCompleted(app.db.designChallenge.name);
        }
        else
            Debug.LogWarning("[" + nameof(SceneManagerDesignChallenge) + "][" + nameof(OnDesignSubmitRequest) + "] Challenge submission failed, Possible reasons: Challenge Submit Rejected by Challenge Manager | demo user logged in");
    }

    # region SAVE

    private async void OnSaveDesign()
    {
        var roomDesign = await app.db.SyncUserChallengeRoomWithServer(new ITransform
        {
            position = mainCam.transform.position,
            rotation = mainCam.transform.rotation.eulerAngles,
            scale = mainCam.transform.localScale
        });
        UploadVersionCoverImage(roomDesign);
    }

    private async void UploadVersionCoverImage(IRoomDesign roomDesign)
    {
        string imgName = roomDesign._id + "_cover";
        string imgPath = Path.Combine(Application.persistentDataPath + "/screenshots/", imgName + ".png");
        await CaptureScreen(imgName);
        var res = await app.server.artifact.UploadCoverImage(roomDesign, imgPath);
        if (res == null)
        {
            Debug.Log("Failed to save screenshot");
        }
        File.Delete(imgPath);
    }

    private async Task CaptureScreen(string nameie)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/screenshots"))
            Directory.CreateDirectory(Application.persistentDataPath + "/screenshots");

        var cam = mainCam;

        Rect oldCamRect = cam.rect;
        cam.rect = new Rect(0, 0, 1, 1);

        // Set fixed screen width and height, invariant to orientation
        int w = 1280;
        int h = 720;
        await Task.Delay(250);

        RenderTexture rt = new RenderTexture(w, h, 24, RenderTextureFormat.ARGB32);
        Texture2D screenShot = new Texture2D(w, h, TextureFormat.RGB24, false);

        RenderTexture.active = rt;
        cam.targetTexture = rt;
        cam.Render();

        screenShot.ReadPixels(new Rect(0, 0, w, h), 0, 0);
        screenShot.Apply();

        cam.targetTexture = null; 
        RenderTexture.active = null; 
        Destroy(rt);

        byte[] bytes = screenShot.EncodeToPNG();
        string filename = ScreenShotName(nameie);
        File.WriteAllBytes(filename, bytes);

        cam.rect = oldCamRect;
    }

    private static string ScreenShotName(string name)
    {
        if (!string.IsNullOrEmpty(name))
            return string.Format("{0}/screenshots/{1}.png",
                                Application.persistentDataPath,
                                name);
        else
            return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
                            Application.persistentDataPath,
                            Screen.width, Screen.height,
                            System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    # endregion


    private void OnAssetDeleteRequest(IHotspot hotpot)
    {
        throw new NotImplementedException();
    }

    private async void OnAssetSwapRequest(IHotspot HotspotTobeSwapped, string newAssetID)
    {
        if (!app.db.IsCurrentObjectValid<IHotspot>()) return;

        var tar = (app.db.currentObject as IHotspot).objRef.transform.position;

        var newAsset = await app.server.artifact.GetAssetData(newAssetID);

        IHotspot prevasset = (app.db.currentObject as IHotspot);//.Clone();

        DeleteAsset(prevasset);

        if (!prevasset.isHotspot)
            prevasset.transform = new ITransform(prevasset.objRef.transform);

        var res = await OnAssetLoadInsideRoom(newAsset, prevasset.transform.position, null, prevasset.transform.rotation);

        if (res.gameObject == null)
        {
            Broadcast_OnActionFailed(ActionType.Swap, (int)ErrorTracking.ErrorCodes.Failed_GameobjectLoad);
        }

        res.gameObject.tag = "Hotspot";
        res.gameObject.name = HotspotTobeSwapped._id;

        var newHotspot = new IHotspot
        {
            _id = HotspotTobeSwapped._id,
            isHotspot = false,
            isMandatory = HotspotTobeSwapped.isMandatory,
            swapAssets = HotspotTobeSwapped.swapAssets,
            objRef = res.gameObject,
            transform = new ITransform(res.gameObject.transform),
            asset = newAsset
        };


        app.db.UpdateHotspotEntry(newHotspot);

        var indexed_hotspots = app.db.designChallenge.baseAssets_Indexed;

        if (res != null && indexed_hotspots.ContainsKey(newHotspot._id))
        {
            app.db.currentObject = indexed_hotspots[newHotspot._id];
            SelectHotspot(indexed_hotspots[newHotspot._id]);
        }
        else
            Debug.LogError("Failed to Fetch just spawned asset data");

        Broadcast_ActionHotspotSwapped(newHotspot);

        // IonicComms.FinishActivity("Unity Hidden");
    }



    void DeleteAsset(IHotspot hotspot, bool isBeingSwapped = false)
    {
        if (app.db.IsCurrentObjectValid<IHotspot>() && (app.db.currentObject as IHotspot) == hotspot)
            DeselectHotspot(hotspot);

        Destroy(hotspot.objRef);
        app.db.DeleteAssetHotspot(hotspot);
        Broadcast_ActionAssetDeleted(hotspot);
    }

    async Task<IArtifactLoadAsyncOperationResult> OnAssetLoadInsideRoom(IAsset assetData, Vector3 pos, GameObject go, Vector3? rot = null)
    {
        var rotation = Vector3.zero;
        if (rot != null)
            rotation = (Vector3)rot;

        var artRes = await LoadAssetAsync(assetData, assetsParent, pos, rotation);




        if (artRes.err || artRes.gameObject == null)
        {
            Debug.Log("AssetLoadFailed, Unexpected | Error Code 0x306782");
            return null;
        }


        return artRes;
    }
    #endregion

    #endregion
}

