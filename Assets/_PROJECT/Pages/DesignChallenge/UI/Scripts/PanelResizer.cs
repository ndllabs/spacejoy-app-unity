﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelResizer : MonoBehaviour
{

    float aspectRatio;
    public RectTransform rt;
    float xMin;

    // Start is called before the first frame update
    void Start()
    {
        aspectRatio = (float)Screen.width / Screen.height;

        if (aspectRatio <= 1.33f)
            xMin = 0.65f;
        else
        if(aspectRatio>1.33f && aspectRatio <1.77f)
        {
            xMin = 0.70f;
        }
        else
            if (aspectRatio >= 1.77f)
            xMin = 0.75f;


        rt.anchorMin = new Vector2(xMin, 0);
        rt.anchorMax = new Vector2(1, 1);
        rt.offsetMax = Vector2.zero;
        rt.offsetMin = Vector2.zero;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
