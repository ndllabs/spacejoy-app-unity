﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity.Events;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using Homefuly;

public class UIController_PaintBuyPopup : MonoBehaviour
{

    public Image img;
    public TextMeshProUGUI text;

    IWallfinish wall;

    Button buyButton;
    DragHandlerPaint dhp;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        Messenger<IWallfinish,Button>.AddListener(ChallengeUICalls.OpenPaintBuyPopup, Init);
        Messenger.AddListener(ChallengeUICalls.ClosePaintBuyPopup, DontBuyThePaint);
        Messenger<float>.AddListener(StaticFunctionCalls.Action_CoinUpdated, HandleAction);
    }

    private void OnDisable()
    {
        Messenger<IWallfinish,Button>.RemoveListener(ChallengeUICalls.OpenPaintBuyPopup, Init);
        Messenger.RemoveListener(ChallengeUICalls.ClosePaintBuyPopup, DontBuyThePaint);
        Messenger<float>.RemoveListener(StaticFunctionCalls.Action_CoinUpdated, HandleAction);
    }
   
    public void Init(IWallfinish wallfinish, Button b)
    {
        buyButton = b;
        wall = wallfinish;
        text.text =  wallfinish.price.ToString() + "?"; 

        ColorUtility.TryParseHtmlString(wallfinish.code, out Color c);
        img.color = c;

    }

    public void BuyThePaint()
    {
        Messenger<IWallfinish>.Broadcast(StaticFunctionCalls.OnUpdateTransaction, wall); //Send Paint buy Request
    }

  public  void DontBuyThePaint()
    {
        gameObject.SetActive(false);
    }

    public void HandleAction(float obj)
    {
        if (obj <= 0)
            return;
        dhp = buyButton.transform.parent.gameObject.AddComponent<DragHandlerPaint>();
        dhp.wallfinish = wall;
        Destroy(buyButton.gameObject);
        gameObject.SetActive(false);
    }


   
}
