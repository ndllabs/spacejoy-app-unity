﻿using System.Collections;
using Homefuly;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIController_InfoPanel : MonoBehaviour
{
    public TextMeshProUGUI title, desc;
    public GameObject pnlBudgetContainer;
    public GameObject infoButton;
    public TextMeshProUGUI min;
    public RawImage image;
    private bool inited;

    AppComponent app;

    public async void Init(IDesignChallenge designChallenge)
    {
        app = AppComponent.Instance;
        if (inited)
            return;

        //Debug.Log("Challenge description " + designChallenge.description);
        inited = true;
        for (int i = pnlBudgetContainer.transform.childCount - 1; i >= 0; --i)
        {
            Destroy(pnlBudgetContainer.transform.GetChild(i).gameObject);
        }
        pnlBudgetContainer.transform.DetachChildren();
        TextMeshProUGUI tempMin;
        foreach (IConstraint constraint in designChallenge.constraints)
        {
             tempMin = Instantiate(min, pnlBudgetContainer.transform);
            switch (constraint.constraint.constraintType)
            {
                case "budget":
                      tempMin.text = "• Design the room within a budget of $" + constraint.constraint.minBudget.ToString() + " and $" + constraint.constraint.maxBudget.ToString();
                    break;
                case "add":
                      tempMin.text = "• Design the room such that you add a minimum of " + constraint.constraint.minBudget.ToString() + " number of furniture and a maximum of " + constraint.constraint.maxBudget.ToString();
                    break;
                case "swap":
                     tempMin.text = "• Design such that you atleast swap " + constraint.constraint.minBudget.ToString() + " number of furniture and a maximum of " + constraint.constraint.maxBudget.ToString();
                    break;      
                case "time":
                    tempMin.text = "• Design the room in " + constraint.constraint.minBudget.ToString()+" time";
                    inited = false;
                    break;
            }
        }
        desc.text = designChallenge.description;
        title.text = designChallenge.name;
        image.texture = await app.server.artifact.GetChallengeCharacter(designChallenge._id);
        Debug.Log(">>>>>>>>>> Image Assigned");

    }


   

    public void CloseInfoPanel()
    {
        gameObject.GetComponent<Animator>().Play("infoPanelClose");
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.3f);
        gameObject.SetActive(false);
        infoButton.SetActive(true);
        yield return null;

    }

}
