﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using _unity.Events;
using Homefuly.Database;
using Homefuly;
using TouchMan;
using System;
using UnityEngine.UI;
using Homefuly.Events;
using System.Threading.Tasks;

public class UIController_DesignChallenge : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject gameObjectBudget; // Budget of room
    public GameObject gameObjectDaysLeft; //Days left till challenge end
    public GameObject gameObjectReward; // Number of coins you'll get
    public GameObject btnInfo;
    public UIController_DesignSwap pnlSwap;
    public UIController_InfoPanel pnlInfo;
    public GameObject pnlStatusBar;
    public GameObject pnlMainButtons;
    public UIController_ChallengeCompleted pnlChallengeComplete;
    public UIController_PaintSwap pnlPaintSwap;
    public GameObject DollarSigns;
    public Canvas particleCanvas;

    [Header("Buttons")]
    public Button btnFinish;

    [Header("Tooltip Manager")]
    public ToolTip_DesignChallenge toolTipManager;

    private EasyFeedback.FeedbackForm feedbackForm;

    TextMeshProUGUI txtBudget;
    TextMeshProUGUI txtRange;
    TextMeshProUGUI textReward;


    ColorBlock normalBtnCol;
    ColorBlock disabledBtnCol;


    AppComponent app;
    WidgetManager widgetManager;

    bool isChallengeSubmitable = false;


    #region Setup
    void Start()
    {
        app = AppComponent.Instance;
        widgetManager = WidgetManager.Instance;
        InitStatusBar();
        btnFinish.gameObject.SetActive(false);

        ColorBlock col = new ColorBlock();

        col.normalColor = btnFinish.colors.normalColor;
        col.pressedColor = btnFinish.colors.pressedColor;
        col.disabledColor = btnFinish.colors.disabledColor;
        disabledBtnCol = col;

        col.normalColor = btnFinish.colors.disabledColor;
        normalBtnCol = col;
        feedbackForm = GameObject.Find("Feedback").GetComponent<EasyFeedback.FeedbackForm>();
    }


    private void OnEnable()
    {
        TouchManEvents.OnTap += TouchManEvents_OnTap;
        Messenger<IHotspot, bool>.AddListener(StaticFunctionCalls.ActionAssetSelected, PopulateSwapPanel);
        Messenger<IWallfinishHotspot, bool>.AddListener(StaticFunctionCalls.Action_WallfinishSelected, PopulatePaintSwapPanel);
        Messenger<float>.AddListener(StaticFunctionCalls.Action_BudgetUpdated, UpdateCoinValue);
        Messenger<bool>.AddListener(StaticFunctionCalls.ChallengeStatusUpdate, EnableFinishButton);
        Messenger<IWallfinishHotspot>.AddListener(StaticFunctionCalls.Action_WallfinishSwapped, Action_WallfinishSwapped);
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, Action_HotspotSwapped);
        Messenger<string>.AddListener(StaticFunctionCalls.Action_DesignSubmited, OnChallengeComplete);
        AppDatabase.OnDBUpdate += AppDatabase_OnDBUpdate;
        HomefulyEvents.OnActionFailed += OnActionFailed;
    }


    private void OnDisable()
    {
        TouchManEvents.OnTap -= TouchManEvents_OnTap;
        HomefulyEvents.OnActionFailed -= OnActionFailed;

        AppDatabase.OnDBUpdate -= AppDatabase_OnDBUpdate;
        Messenger<IHotspot, bool>.RemoveListener(StaticFunctionCalls.ActionAssetSelected, PopulateSwapPanel);
        Messenger<IWallfinishHotspot, bool>.RemoveListener(StaticFunctionCalls.Action_WallfinishSelected, PopulatePaintSwapPanel);
        Messenger<float>.RemoveListener(StaticFunctionCalls.Action_BudgetUpdated, UpdateCoinValue);
        Messenger<bool>.RemoveListener(StaticFunctionCalls.ChallengeStatusUpdate, EnableFinishButton);
        Messenger<IWallfinishHotspot>.RemoveListener(StaticFunctionCalls.Action_WallfinishSwapped, Action_WallfinishSwapped);
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, Action_HotspotSwapped);
        Messenger<string>.RemoveListener(StaticFunctionCalls.Action_DesignSubmited, OnChallengeComplete);
    }
    #endregion

    #region UIControls
    private void InitStatusBar()
    {
        txtBudget = gameObjectBudget.GetComponent<TextMeshProUGUI>();
        txtBudget.text = "0";
        txtRange = gameObjectReward.GetComponent<TextMeshProUGUI>();
        txtRange.text = "";
    }

    private void UpdateCoinValue(float obj)
    {
        obj = Mathf.Round(obj);
        float newVal = 0, oldVal = 0;
        float.TryParse(obj.ToString(), out newVal);
        float.TryParse(txtBudget.text, out oldVal);
        DollarSigns.SetActive(true);
        if (oldVal < newVal)
        {
            DollarSigns.gameObject.transform.GetChild(2).gameObject.SetActive(true);
            DollarSigns.gameObject.transform.GetChild(3).gameObject.SetActive(false);
        }
        else
        if (oldVal > newVal)
        {
            DollarSigns.gameObject.transform.GetChild(2).gameObject.SetActive(false);
            DollarSigns.gameObject.transform.GetChild(3).gameObject.SetActive(true);
        }
        else
             if (oldVal == newVal)
        {
            DollarSigns.SetActive(false);
            return;
        }
        StartCoroutine(DestroyDollar());
        StartCoroutine(CountToValue(newVal, oldVal, 3));
        //txtBudget.text = obj.ToString();
    }


    public void SetRangeValue()
    {
        foreach (IConstraint constraint in app.db.designChallenge.constraints)
        {

            switch (constraint.constraint.constraintType)
            {
                case "budget":
                    txtRange.text = "Design within:\n $" + constraint.constraint.minBudget.ToString() + " - $" + constraint.constraint.maxBudget.ToString();
                    break;
            }
        }
        AppDatabase.OnDBUpdate -= AppDatabase_OnDBUpdate;
    }

    /// <summary>
    /// Enables the finish button when budget is met
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnableFinishButton(bool b)
    {
        btnFinish.gameObject.SetActive(true);

        isChallengeSubmitable = b;
        btnFinish.interactable = true;

        Color col = new Color();

        if (isChallengeSubmitable)
            ColorUtility.TryParseHtmlString("47692", out col);
        else
            ColorUtility.TryParseHtmlString("555555", out col);

        btnFinish.transform.Find("Image").GetComponent<Image>().color = col;

        //temp, Replace app db ref, and avoid using loop
        foreach (var ass in app.db.designChallenge.baseAssets)
        {
            if (ass.isHotspot)
            {
                btnFinish.gameObject.SetActive(false);
            }
        }
        foreach (var pant in app.db.designChallenge.basePaint)
        {
            if (pant.isHotspot)
            {
                btnFinish.gameObject.SetActive(false);
            }
        }

    }
    #endregion

    #region Database Listener
    void AppDatabase_OnDBUpdate(Db updateType)
    {
        if (updateType == Db.Whole)
            SetRangeValue();
    }
    #endregion

    #region TouchManEventListener
    private void TouchManEvents_OnTap(GameObject tappedObject)
    {


        if (tappedObject != null && tappedObject.tag == "Hotspot")// && indexedBP[tappedObject.name].isHotspot)
        {
            Messenger<GameObject>.Broadcast(ChallengeUICalls.HotspotTapped, tappedObject);
        }
        else
        if (tappedObject != null && tappedObject.tag == "Hotspot")
        {
            Messenger.Broadcast(ChallengeUICalls.UnselectHotspot);
        }

    }
    #endregion

    #region Child UIControls
    private void PopulateSwapPanel(IHotspot arg1, bool arg2)
    {
        if (!arg2)
        {
            if (pnlSwap.gameObject.activeSelf)
                Messenger.Broadcast(ChallengeUICalls.CloseSwapPanel);

        }
        else
        {
            pnlSwap.gameObject.SetActive(true);
            pnlSwap.PopulateSwapPanel(arg1, arg2);
        }
    }

    private void PopulatePaintSwapPanel(IWallfinishHotspot arg1, bool arg2)
    {
        if (!arg2)
        {
            if (pnlPaintSwap.gameObject.activeSelf)
                Messenger.Broadcast(ChallengeUICalls.CloseSwapPanel);
        }
        else
        {
            pnlPaintSwap.gameObject.SetActive(true);
            pnlPaintSwap.PopulateSwapPaintPanel(arg1, arg2);
        }
    }


    public void ShowInfoPanel()
    {
        pnlInfo.gameObject.SetActive(true);
        btnInfo.SetActive(false);
        pnlInfo.Init(app.db.designChallenge);
    }
    #endregion

    #region GameFlow
    public void ExitScene()
    {

        toolTipManager.ToolTipPauseStatus(true);

        // Debug.Log("[IONIC] Hiding unity");
        // IonicComms.FinishActivity("Unity Hidden");
        WidgetManager.Instance.Confirm("Exit Challenge", "Are you sure you want to leave the challenge?", res =>
        {
            toolTipManager.ToolTipPauseStatus(false);
            if (res == "yes")
            {
                Messenger<SceneType>.Broadcast(AnalyticsCalls.SceneClosed, app.db.sceneType);
                // app.Navigate(AppPage.Home);

                // go back to appcomponent scene before minimizing the unity app
                HomefulyExtensions.AskAppFeedback(() =>
                {
                    PlayerPrefs.SetString("minimizeUnity", " ");
                    app.Navigate(AppPage.AppComponent);
                });
            }

        });
    }

    /// <summary>
    /// When User Clicks submit design, Request a Challenge Complete
    /// </summary>
    public void SaveDesign()
    {

        if (!isChallengeSubmitable)
        {
            widgetManager.Alert("", "Design the room inside the challenge's budget range to complete the challenge", () =>
            {
                //nothing happens here
            });
        }

        /* When user Clicks Submit button
         * if User currently not logged in, show the collect rewards panel.
         * else ask the scene manager to reward the user.
         * 
         * once the user logs in then fire the collect rewards,
         * 
         * */

        if (app.account.CRED_ISDEMO == string.Empty)
        {
            if (ChallengeManager.Instance.IsChallengeComplete())
            {
                pnlChallengeComplete.gameObject.SetActive(true);

                // Note: instead of depending on the db here for rewards, get the rewards in the this.init()
                // Warning: Not Safe Type Cast
                particleCanvas.gameObject.SetActive(true);
                pnlChallengeComplete.ShowCollectRewards((int)app.db.designChallenge.rewards[0].rewardValue);
            }
            else
            {
                widgetManager.Alert("", "Design the room inside the challenge's budget range to complete the challenge", () =>
                {
                    //nothing happens here
                });
            }
        }
        else
        {

            Messenger.Broadcast(StaticFunctionCalls.Request_DesignSubmit);
        }


    }

    /// <summary>
    /// When the challnge is Litrelly complete
    /// </summary>
    /// <param name="challengename"></param>
    public void OnChallengeComplete(string challengename)
    {
        pnlChallengeComplete.gameObject.SetActive(true);
        // Note: instead of depending on the db here for rewards, get the rewards in the this.init()
        // Warning: Not Safe Type Cast
        pnlChallengeComplete.ShowChallengeComplete((int)app.db.designChallenge.rewards[0].rewardValue);
        particleCanvas.gameObject.SetActive(true);
        pnlStatusBar.SetActive(false);
        pnlMainButtons.SetActive(false);

    }
    #endregion

    #region IEnumerators
    IEnumerator DestroyDollar()
    {
        yield return new WaitForSeconds(0.5f);
        DollarSigns.SetActive(false);
        yield return null;
    }

    IEnumerator CountToValue(float newVal, float oldVal, float time)
    {
        float lerp = newVal / time;
        if (newVal > oldVal)
        {
            oldVal = 0;
            for (float i = 0; i <= newVal; i += lerp / 5)
            {
                yield return new WaitForSeconds(0.03f);
                txtBudget.text = ((int)(oldVal + i)).ToString();
            }
        }
        else
        {
            for (float i = 0; i <= newVal; i += lerp / 5)
            {
                yield return new WaitForSeconds(0.03f);
                txtBudget.text = ((int)(oldVal - i)).ToString();
            }
        }
        if (Math.Abs(oldVal - newVal) <= 0)
            yield return null;
        txtBudget.text = newVal.ToString();
        yield return null;
    }
    #endregion

    #region Actions
    private void Action_HotspotSwapped(IHotspot obj)
    {
        EnableFinishButton(ChallengeManager.Instance.IsChallengeComplete());
    }

    private void Action_WallfinishSwapped(IWallfinishHotspot obj)
    {
        EnableFinishButton(ChallengeManager.Instance.IsChallengeComplete());
    }

    private void OnActionFailed(ActionType actionType, int errCode)
    {
        if (actionType == ActionType.ChallengeSubmit)
        {
            pnlChallengeComplete.gameObject.SetActive(false);
            widgetManager.LoaderClose();
        }
    }
    #endregion

}


#region ChallengeUICalls
public static class ChallengeUICalls
{
    public static string UIApprovesPurchase = "UIApprovesPurchase";
    public static string UIRejectsPurchase = "UIRejectsPurchase";
    public static string ShowPurchaseButtons = "UIShowPurchaseButtons";
    public static string HotspotTapped = "HotspotTapped";
    public static string UpdateNavbarText = "UpdateNavbarText";
    public static string UnselectHotspot = "UnselectHotspot";
    public static string CloseSwapPanel = "CloseSwapPanel";
    public static string OpenPaintBuyPopup = "OpenPaintBuyPopup";
    public static string ClosePaintBuyPopup = "ClosePaintBuyPopup";
    public static string ShowToolTip = "ShowToolTip";
    public static string DestroyToolTip = "DestroyToolTip";

    public static string ExpandButtonClicked = "ExpandButtonClicked";
    public static string SaveButtonClicked = "SaveButtonClicked";
    public static string ViewButtonClicked = "ViewButtonClicked";
}
#endregion
