﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using _unity.Events;
using TMPro;

public class ChallengeHotSpotManager : MonoBehaviour
{
     GameObject selectedHotspot;

    private void OnEnable()
    {
        Messenger<GameObject>.AddListener(ChallengeUICalls.HotspotTapped, SelectHotspot);
        Messenger.AddListener(ChallengeUICalls.UnselectHotspot, UnselectHotspot);
    }

    private void OnDisable()
    {
        Messenger<GameObject>.RemoveListener(ChallengeUICalls.HotspotTapped, SelectHotspot);
        Messenger.RemoveListener(ChallengeUICalls.UnselectHotspot, UnselectHotspot);
    }

    /// <summary>
    /// Selects the hotspot, changes color, 
    /// plays-stops animation
    /// hottie is the newly selected hotspot
    /// </summary>
    /// <param name="hottie">Hottie.</param>
    public void SelectHotspot(GameObject hottie)
    {
        if (selectedHotspot != null && selectedHotspot!=hottie)
        {  
            UnselectHotspot();
            if (selectedHotspot.GetComponent<Animator>() == null)
                return;
        }
        if (hottie == null || hottie.GetComponent<Animator>() == null)
            return;
        selectedHotspot = hottie;
        selectedHotspot.transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.white; //text
        selectedHotspot.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = Color.white; //icon
        selectedHotspot.transform.GetChild(2).GetComponent<MeshRenderer>().material.color = new Color(76/ 255f, 63/ 255f, 124/ 255f); //bg
        selectedHotspot.GetComponent<Animator>().Play("SelectedHotspotAnimation");
        Debug.Log(">>> Hotspot selected");
    }

    void UnselectHotspot()
    {
       
        if (selectedHotspot == null || selectedHotspot.GetComponent<Animator>() == null)
            return;
        selectedHotspot.transform.GetChild(2).GetComponent<MeshRenderer>().material.color = Color.white;
        selectedHotspot.GetComponent<Animator>().Play("New State");
        selectedHotspot.transform.GetChild(0).GetComponent<TextMeshPro>().color = new Color(28 / 255f, 23 / 255f, 46 / 255f);
        selectedHotspot.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = new Color(76 / 255f, 63 / 255f, 124 / 255f);
        Debug.Log(">>> Hotspot unselected");
    }


}
