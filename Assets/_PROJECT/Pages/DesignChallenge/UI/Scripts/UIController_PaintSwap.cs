﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Homefuly;
using _unity.Events;
using System.Threading.Tasks;

public class UIController_PaintSwap : MonoBehaviour
{

    public GameObject pnlSwapPaint;
    public GameObject popupPaintBuy;
    public GameObject Content;
    public GameObject  elemPaint;
    public GameObject elemLoader;
     Button btnBuy;
   
    AppComponent app;
    IWallfinishHotspot temp_hs;


    private void OnEnable()
    {
        Messenger.AddListener(ChallengeUICalls.CloseSwapPanel, ClosePaintPanel);
    }

    private void OnDisable()
    {
        Messenger.RemoveListener(ChallengeUICalls.CloseSwapPanel, ClosePaintPanel);
    }

    public async void PopulateSwapPaintPanel(IWallfinishHotspot hotpot, bool obj)
    {
        bool IsChallenge = false;
        app = AppComponent.Instance;
        if (app.db.sceneType == SceneType.DesignChallenge)
        {
            IsChallenge = true;
        }

        if (!obj)
        {
            return;
        }

        pnlSwapPaint.GetComponent<ScrollRect>().normalizedPosition = Vector2.one;
        temp_hs = hotpot;
        pnlSwapPaint.SetActive(true);
        //GameObject g;

        List<Task> PaintDownloadTask = new List<Task>();
        for (int i = Content.transform.childCount - 1; i >= 0; --i)
        {
            Destroy(Content.transform.GetChild(i).gameObject);
        }
        Content.transform.DetachChildren();

        foreach (var ass in app.db.designChallenge.basePaints_Indexed[hotpot._id].swapWallFinish)
        {
            PaintDownloadTask.Add(TaskSetPainElem(hotpot, IsChallenge, ass.wallFinish));

        }
        Task t = Task.WhenAll(PaintDownloadTask);
        await t;
        if (t.IsCompleted)
        {
            elemLoader.SetActive(false);
        }
    }

    private async Task<GameObject> TaskSetPainElem(IWallfinishHotspot hotpot, bool IsChallenge, string id)
    {
        GameObject g = Instantiate(elemPaint, Content.transform);

        var wallfinish = await app.server.artifact.GetWallfinishData(id);
        wallfinish.price = AppConfig.GameConfig.DefaultWallfinishPrice;

        if (g != null)
        {
            if (ColorUtility.TryParseHtmlString(wallfinish.code, out Color c))
                g.transform.Find("imgProduct").GetComponent<Image>().color = c;

            g.transform.Find("txtPrice").GetComponent<TextMeshProUGUI>().text = "$" + wallfinish.price.ToString();

            Button b = g.transform.gameObject.AddComponent<Button>();

            b.onClick.RemoveAllListeners();
            b.onClick.AddListener(() =>
            {
                Messenger<IWallfinishHotspot, IWallfinish>.Broadcast(StaticFunctionCalls.Request_WallfinishApply, hotpot, wallfinish); // Send Paint apply Request
            });


            btnBuy = g.transform.Find("btnBuyPaint").GetComponent<Button>();
            if (!IsChallenge)
            {
                btnBuy.onClick.RemoveAllListeners();
                btnBuy.onClick.AddListener(() =>
                {
                    //  popupPaintBuy.SetActive(true);
                    Messenger<IWallfinish>.Broadcast(ChallengeUICalls.OpenPaintBuyPopup, wallfinish);// Send Paint buy Request
                });

            }
            else
            {
                btnBuy.gameObject.SetActive(false);
            }

        }
            return g;
        
    }

    public void ClosePaintPanel()
    {
        Messenger.Broadcast(ChallengeUICalls.UnselectHotspot);
        pnlSwapPaint.SetActive(false);
        Messenger<IWallfinishHotspot, bool>.Broadcast(StaticFunctionCalls.Request_WallfinishSelect, temp_hs, false);
        
    }

}
