﻿using _unity.Events;
using Homefuly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController_DesignSwap : MonoBehaviour
{
    AppComponent app;
    WidgetManager widget;
    public GameObject elemProduct;
    public GameObject swapPanelContent;
    public GameObject swapPanel;
    public GameObject elemLoader;
    IHotspot temp_hs;

    static InAppBrowserBridge bridge;

    // Start is called before the first frame update
    void Awake()
    {
        app = AppComponent.Instance;
        widget = WidgetManager.Instance;
        bridge = FindObjectOfType<InAppBrowserBridge>();
    }



    private void OnEnable()
    {

        Messenger.AddListener(ChallengeUICalls.CloseSwapPanel, CloseSwapPanel);
        Messenger<IHotspot, string>.AddListener(StaticFunctionCalls.Request_HotspotSwap, ShowLoader);
        Messenger<IHotspot>.AddListener(StaticFunctionCalls.Action_HotspotSwapped, HideLoader);
        bridge.onBrowserClosed.AddListener(OnBrowserClosed);
    }

    private void OnBrowserClosed()
    {
        HomefulyExtensions.SetScreenOrientation(true);
    }

    private void OnDisable()
    {
        Messenger.RemoveListener(ChallengeUICalls.CloseSwapPanel, CloseSwapPanel);
        Messenger<IHotspot, string>.RemoveListener(StaticFunctionCalls.Request_HotspotSwap, ShowLoader);
        Messenger<IHotspot>.RemoveListener(StaticFunctionCalls.Action_HotspotSwapped, HideLoader);

        bridge.onBrowserClosed.RemoveListener(OnBrowserClosed);
    }


    /// <summary>
    /// Populates the swap panel.
    /// Takes the slected hotspot/asset as parameter, 
    /// boolean obj for describing selection status
    /// </summary>
    /// <param name="hotpot">Hotpot.</param>
    /// <param name="obj">If set to <c>true</c> object.</param>
    public async void PopulateSwapPanel(IHotspot hotpot, bool obj)
    {

        //Debug.Log("Populate Hotspot called");
        if (!obj)
        {
            return;
        }
        elemLoader.SetActive(true);
        temp_hs = hotpot;
        swapPanel.SetActive(true);
        GameObject g;
        List<Task> ProductImageDownloadTask = new List<Task>();

        for (int i = swapPanelContent.transform.childCount - 1; i >= 0; --i)
        {
            UnityEngine.Object.Destroy(swapPanelContent.transform.GetChild(i).gameObject);
        }
        swapPanelContent.transform.DetachChildren();
        swapPanel.GetComponent<ScrollRect>().normalizedPosition = Vector2.one;

        foreach (var ass in app.db.designChallenge.baseAssets_Indexed[hotpot._id].swapAssets)
        {
            g = Instantiate(elemProduct, swapPanelContent.transform);
            Button b = g.transform.gameObject.AddComponent<Button>();
            b.onClick.RemoveAllListeners();
            b.onClick.AddListener(() =>
            {
                Messenger<IHotspot, string>.Broadcast(StaticFunctionCalls.Request_HotspotSwap, hotpot, ass.asset._id);
            });
            g.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "$" + ass.asset.price.customer.ToString();  //Asset Price
            g.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = AppComponent.Instance.db.retailers[ass.asset.designedBy.organizationInternalLink].name; //Asset name
            g.transform.GetChild(3).GetComponent<Button>().onClick.RemoveAllListeners();
            g.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() =>
            {

                HomefulyExtensions.OpenProductStorePage(ass.asset, Homefuly.Analytics.ShopButtonType.hotspot_swap);

                // HomefulyExtensions.SetScreenOrientation(false);
                // string url = !string.IsNullOrEmpty(ass.asset.designedBy.productUrl)
                //                       ? ass.asset.designedBy.productUrl
                //                       : "https://www.google.co.in/search?q=" + ass.asset.name;
                // url = Uri.EscapeUriString(url);
                // IonicComms.FinishActivity("?p=t&context=productPage&data=" + url + "&err=false");

                //string url = System.Uri.EscapeUriString(ass.asset.designedBy.productUrl); //Add shoppable url
                ////HomefulyExtensions.SetScreenOrientationToLandscape(false);
                ////url = System.Uri.EscapeUriString(url);
                //InAppBrowser.OpenURL(url);
                //Debug.Log(url);
            });

            ProductImageDownloadTask.Add(DownloadProductTexture("/assets/" + ass.asset._id + "/" + ass.asset._id + ".jpg", g.transform.GetChild(0).GetComponent<Image>()));

        }
        Task t = Task.WhenAll(ProductImageDownloadTask);
        await t;
        if (t.IsCompleted)
        {
            elemLoader.SetActive(false);
        }
    }

    /// <summary>
    /// Downloads the product texture.
    /// After the swap panel is populated
    /// the images are later downloaded
    /// </summary>
    /// <returns>The product texture.</returns>
    /// <param name="texture">Texture.</param>
    /// <param name="img">Image.</param>
    async Task DownloadProductTexture(string texture, Image img)
    {
        Texture2D tex = (Texture2D)await app.server.web.GetTexture(texture);
        if (tex == null)
        {
            Debug.Log("nibba its null");
        }
        if (tex != null)
        {
            if (img != null)
                img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f));
        }

    }

    public void CloseSwapPanel()
    {
        Messenger.Broadcast(ChallengeUICalls.UnselectHotspot);
        swapPanel.SetActive(false);
        if (temp_hs != null)
            Messenger<IHotspot, bool>.Broadcast(StaticFunctionCalls.Request_AssetSelect, temp_hs, false);
    }



    public void ShowLoader(IHotspot h, string s)
    {
        widget.LoaderMini("Loading..");
    }

    public void HideLoader(IHotspot h)
    {
        widget.LoaderMiniClose();
    }

}
