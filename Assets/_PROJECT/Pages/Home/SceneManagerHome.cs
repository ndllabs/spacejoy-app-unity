﻿using Homefuly;
// using Homefuly.WebView;
using Homefuly.Analytics;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using Homefuly.Events;

public class SceneManagerHome : MonoBehaviour
{
    static InAppBrowserBridge bridge;
    AppComponent app;
    WidgetManager widget;
    public Canvas noNetCanvas;
    // UniWebView webView;

    # region Internal
    private bool isHomepage = false;
    private bool isLoggingOut = false;

    // bool isFromShop = false;
    // bool isClosingForShop = false;
    string currentUrl = string.Empty;
    # endregion

    private async void Start()
    {
        Debug.Log("Here 2");

        await HomefulyExtensions.SetScreenOrientationAndWait(false);
        // WebViewService.Instance.SetWebView();

#if UNITY_EDITOR
        //app.Navigate(AppPage.AppComponent);
#else

        // Test
        // InAppBrowser.OpenURL("https://web.homefuly.com");

         OpenWebBrowser();

        //StartCoroutine(checkInternetConnection((isConnected) => {
        //    if (isConnected)
        //        OpenWebBrowser();
        //    else
        //    {
        //        InAppBrowser.CloseBrowser();
        //        HideCanvas(false);
        //    }
        //}));
      
#endif

    }

    private void Awake()
    {
        app = AppComponent.Instance;
        widget = WidgetManager.Instance;
        bridge = FindObjectOfType<InAppBrowserBridge>();
        HomefulyExtensions.SetScreenOrientation(false);



    }

    //IEnumerator checkInternetConnection(Action<bool> action)
    //{
    //    WWW www = new WWW("http://google.com");
    //    yield return www;
    //    if (www.error != null)
    //    {
    //        action(false);
    //    }
    //    else
    //    {
    //        action(true);
    //    }
    //}

    private void OnEnable()
    {
        //bridge.onJSCallback.RemoveAllListeners();
        //bridge.onJSCallback.AddListener(OnIABMessage);
        //bridge.onAndroidBackButtonPressed.AddListener(OnBackButtonPressed);
        //bridge.onBrowserClosed.AddListener(OnBrowserClosed);
        //bridge.onBrowserStartedLoading.AddListener(OnBrowserStartedLoadingUrl);
        //bridge.onBrowserFinishedLoading.AddListener(OnBrowserFinishedLoadingUrl);
        //bridge.onBrowserFinishedLoadingWithError.AddListener(OnBrowserFinishedLoadingWithError);

        // if (webView)
        // {
        //     webView.OnMessageReceived += OnWebViewCallback;
        //     webView.OnPageStarted += OnWebViewStarted;
        //     webView.OnPageFinished += OnWebViewFinished;
        //     webView.OnShouldClose += OnWebViewClosed;
        // }
    }

    private void OnDisable()
    {
        //bridge.onJSCallback.RemoveAllListeners();
        //bridge.onAndroidBackButtonPressed.RemoveListener(OnBackButtonPressed);
        //bridge.onBrowserClosed.RemoveListener(OnBrowserClosed);
        //bridge.onBrowserStartedLoading.RemoveListener(OnBrowserStartedLoadingUrl);
        //bridge.onBrowserFinishedLoading.RemoveListener(OnBrowserFinishedLoadingUrl);
        //bridge.onBrowserFinishedLoadingWithError.RemoveListener(OnBrowserFinishedLoadingWithError);

        // webView.OnMessageReceived -= OnWebViewCallback;
        // webView.OnPageStarted -= OnWebViewStarted;
        // webView.OnPageFinished -= OnWebViewFinished;
        // webView.OnShouldClose -= OnWebViewClosed;
    }


    //TODO: based on open mode, open defferent URLS,
    async Task OpenWebBrowser(string externalUrl = null)
    {
        await Task.Delay(100);

        string url;
        //bool hideNavBar;

        if (externalUrl != null) // for external websites
        {
            url = System.Uri.EscapeUriString(externalUrl);

            Debug.Log("Extern: " + externalUrl + "\n parsed: " + url);

            // displayoptions
            //hideNavBar = false;
        }
        else
        {
            Debug.Log("opening web UI 2");
            /*
            NEW URL
             */
            url = AppConfig.ServerConfig.webUIHost + "/iab/login/" + System.Uri.EscapeUriString(app.account.CRED_JWTTOKEN) + "/" + AppConfig.GameConfig.nextChallenge.ToString().ToLower();
            AppConfig.GameConfig.nextChallenge = false;
            //hideNavBar = true;

            //InAppBrowser.ClearCache();
        }


        Debug.Log("Web UI URL: " + url);

        //InAppBrowser.OpenURL(url, new InAppBrowser.DisplayOptions
        //{
        //    displayURLAsPageTitle = false,
        //    pageTitle = "Product Page",
        //    hidesTopBar = hideNavBar,
        //    androidBackButtonCustomBehaviour = true,
        //    hidesDefaultSpinner = true,
        //    pinchAndZoomEnabled = false,
        //    hidesHistoryButtons = true,
        //    shouldStickToPortrait = true
        //});

        // WebViewService.Instance.EnableTopBar(false);

        //webView.SetImmersiveModeEnabled(false);
        // webView = WebViewService.Instance.OpenURL(url);

        // webView.OnMessageReceived += OnWebViewCallback;
        // webView.OnPageStarted += OnWebViewStarted;
        // webView.OnPageFinished += OnWebViewFinished;
        // webView.OnShouldClose += OnWebViewClosed;
    }

    // Handles authentication after logout from webui
    private async void OnLogoutAuth()
    {
        await app.account.Logout();
        isLoggingOut = true;
        // WebViewService.Instance.close();
        InAppBrowser.CloseBrowser();

        WidgetManager.Instance.AuthPanelOpen(async (bool success) =>
        {
            isLoggingOut = false;
            if (!success)
            {
                var resD = await app.account.Connect();
            }
            app.db.ResetDb();
            await OpenWebBrowser();
        });
    }

    public async void CreateUserOwnDesign()
    {
        // get current design
        widget.LoaderMini("Creating your room... ", 4);
        var templateDesign = await app.server.artifact.GetRoomDesignAsync(app.account.CURRENT_ROOM_ID, app.account.CURRENT_VERSION_ID);
        templateDesign.project = AppConfig.PilotDemoConfig.ProjectId;
        templateDesign.customer = app.account.CRED_USERID;

        Debug.Log(templateDesign.project + ", " + templateDesign.customer);

        var userCreatedDesign = await app.server.artifact.CreateNewRoomVersion(templateDesign);
        app.account.CURRENT_VERSION_ID = userCreatedDesign._id;
        app.account.CURRENT_ROOM_ID = userCreatedDesign.room;

        widget.LoaderMiniClose();
        app.Navigate(AppPage.Designer);
    }



    #region Response From InAppBrowser

    /*
    *  Closes the application when webui is in homepage state else goes back to the homepage
    * This need to be tested in build
     */

    void OnBrowserStartedLoadingUrl(string url)
    {
        currentUrl = url;
        // isHomepage = false;
        // if(url == AppConfig.ServerConfig.webUIHost + "/iab/login/" + app.account.CRED_JWTTOKEN + "/false") isHomepage = true;

        if (url.Contains(AppConfig.ServerConfig.webUIHost + "/iab/login") || url.Contains(AppConfig.ServerConfig.webUIHost + "/iab/home")) // workaround
        {
            isHomepage = true;
        }
        else
        {
            isHomepage = false;
        }

        Debug.Log(">>>> isHomepage: " + isHomepage);
        Debug.Log(">>>> URL started loading: " + url);
    }

    void OnBrowserFinishedLoadingUrl(string url)
    {
        // currentUrl = url;
        // isHomepage = false;
        // // if(url == AppConfig.ServerConfig.webUIHost + "/iab/login/" + app.account.CRED_JWTTOKEN + "/false") isHomepage = true;

        // if(url == AppConfig.ServerConfig.webUIHost + "/iab/home" || url .Contains(AppConfig.ServerConfig.webUIHost + "/iab/login")) // workaround
        // {
        //     isHomepage = true;
        // }

        if (url.Contains(AppConfig.ServerConfig.webUIHost + "/iab/home"))
        {
            isHomepage = true;
        }

        Debug.Log(">>>> isHomepage: " + isHomepage);
        Debug.Log(">>>> URL finished loading: " + url);
    }

    private void OnBrowserFinishedLoadingWithError(string url, string err)
    {
        Debug.Log("URL Error while loading: " + url + " 2nd var: " + err);
    }


    void OnBackButtonPressed()
    {
        if (isHomepage)
        {
            Application.Quit();
        }
        // else if(!currentUrl.Contains(AppConfig.ServerConfig.webUIHost))
        // {
        //     // InAppBrowser.GoBack();
        //     // InAppBrowser.ExecuteJS("window.history.back();");

        //     await OpenWebBrowser();
        //     isHomepage = true;
        // }
        else
        {
            //InAppBrowser.GoBack();
            // InAppBrowser.ExecuteJS("window.history.back();");
        }
    }

    private void OnBrowserClosed()
    {
        // if (isClosingForShop){ isClosingForShop = false; return; }

        if (!currentUrl.Contains(AppConfig.ServerConfig.webUIHost))
        {
            //InAppBrowser.GoBack();
            // InAppBrowser.ExecuteJS("window.history.back();");
            // OpenWebBrowser();
            // isFromShop = false;
        }
        else if (!isLoggingOut)
        {
            Debug.Log(">>>>>>>> browser closed. Changing to landscape");
            //HomefulyExtensions.SetScreenOrientationToLandscape(true);
        }
    }

    async void OnIABMessage(string jsMessage)
    {
        Debug.Log(">>>>[ Inside ONAIBMessage Function]");
        Debug.Log(">>>>[ jsMsg ]: " + jsMessage);
        IABResponse resp = new IABResponse(jsMessage);
        Debug.Log(">>>>[ jsMsgConverted ]: " + JsonUtility.ToJson(resp));
        Debug.Log(">>>>[ jsMsgConverted ]: " + resp.data);

        if (resp.Err)
        {
            widget.Toast("Oops! Somethign went wrong. Please restart the app and try again.");
            InAppBrowser.CloseBrowser();
            return;
        }

        if (resp.Context == "demoDesign")
        {
            var iabData = resp.GetData<IABMDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_ROOM_NAME = iabData.rid;// center.name;
            app.account.CURRENT_VERSION_ID = iabData.vid;

            // Messenger<string, bool>.Broadcast(AnalyticsCalls.SceneConfirmationResponse, app.account.CURRENT_VERSION_ID, true);

            // app.CurrentSceneType = SceneType.ReadOnlyDesign;
            app.CurrentSceneType = SceneType.UserRoom;
            InAppBrowser.CloseBrowser();

            await Task.Delay(100);
            app.Navigate(AppPage.Designer);
        }
        else if (resp.Context == "designChallenge")
        {
            var iabData = resp.GetData<IABMChallenge>();
            app.account.CURRENT_CHALLENGE_ID = iabData._id;
            app.CurrentSceneType = SceneType.DesignChallenge;
            AppConfig.PilotDemoConfig.ProjectId = AppConfig.ProjectConfig.PROJ_CHALLENGE;

            InAppBrowser.CloseBrowser();
            Debug.LogError("navigationnC");
            await Task.Delay(100);

            app.Navigate(AppPage.DesignChallenge);
        }
        else if (resp.Context == "ownVersionDesignEdit")
        {
            Debug.Log(">>>>>>>>>> ownVersionDesignEdit");
            var iabData = resp.GetData<IABMDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_ROOM_NAME = iabData.rid;
            app.account.CURRENT_VERSION_ID = iabData.vid;

            Debug.Log(iabData.pid + ", " + iabData.rid + ", " + iabData.vid);

            app.CurrentSceneType = SceneType.UserRoom;
            // if (iabData.pid == AppConfig.ProjectConfig.PROJ_CHALLENGE)
            // {
            //     app.CurrentSceneType = SceneType.ReadOnlyDesign;
            // }
            // else
            // {
            //     app.CurrentSceneType = SceneType.UserRoom;
            // }

            // Debug.LogWarning(">>>>>>>>Current Scene Type: " + app.CurrentSceneType);

            InAppBrowser.CloseBrowser();
            await Task.Delay(100);
            app.Navigate(AppPage.Designer);
        }
        else if (resp.Context == "ownVersionDesignCreate")
        {
            var iabData = resp.GetData<IABMDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_VERSION_ID = iabData.vid;
            app.CurrentSceneType = SceneType.UserRoom;

            InAppBrowser.CloseBrowser();
            await Task.Delay(100);
            // CreateUserOwnDesign();
            app.Navigate(AppPage.Designer);
        }
        else if (resp.Context == "shop")
        {
            Debug.Log("shot context data: " + resp.data.Replace("\"", "").Trim());

            await OpenWebBrowser(resp.data.Replace("\"", "").Trim());


            /* Analytics need more than url currenlty,
             * Missing values:
             * asset.name, asset.cat
             * 
             * skipping.....
             * */
            //HomefulyAnalyticsEvent.OnShopClicked?.Invoke(asset, shopButtonType);

        }
        else if (resp.Context == "logout")
        {
            Debug.Log("Logout Auth");
            OnLogoutAuth();
        }
        else
        {
        }
    }

    // private void OnBrowserClosed()
    // {
    //     if (isClosingForShop) { isClosingForShop = false; return; }

    //     if (isFromShop)
    //     {
    //         OpenWebBrowser();
    //         isFromShop = false;
    //     }
    //     else
    //     {
    //         HomefulyExtensions.SetScreenOrientationToLandscape(true);
    //     }
    // }

    //public void CheckInternet()
    //{
    //    app.Init((bool fail) =>
    //    {
    //        if(!fail)
    //        {
    //            app.Navigate(AppPage.Home);
    //        }
    //    }
    //    );
    //}


    public void HideCanvas(bool hide)
    {
        if (hide)
        {
            noNetCanvas.gameObject.SetActive(false);
        }
        else
        {
            noNetCanvas.gameObject.SetActive(true);
        }
    }


    #endregion


    #region ===[ UniWebView ]===


    bool OnWebViewClosed(UniWebView vw)
    {
        print("On web view close called");
        string url = AppConfig.ServerConfig.webUIHost + "/iab/login/" + System.Uri.EscapeUriString(app.account.CRED_JWTTOKEN) + "/" + AppConfig.GameConfig.nextChallenge.ToString().ToLower();
        // webView = WebViewService.Instance.OpenURL(url);
        return false;
    }

    void OnWebViewStarted(UniWebView vw, string url)
    {
        OnBrowserStartedLoadingUrl(url);
    }

    void OnWebViewFinished(UniWebView vw, int status, string url)
    {
        OnBrowserFinishedLoadingUrl(url);
    }


    async void OnWebViewCallback(UniWebView view, UniWebViewMessage msg)
    {
        Debug.Log(">>>>[ Inside ONAIBMessage Function]");
        Debug.Log(">>>>[ jsMsg ]: " + msg.RawMessage);
        IABResponse resp = new IABResponse(msg.RawMessage);
        Debug.Log(">>>>[ jsMsgConverted ]: " + JsonUtility.ToJson(resp));
        Debug.Log(">>>>[ jsMsgConverted ]: " + resp.data);

        if (resp.Err)
        {
            widget.Toast("Oops! Somethign went wrong. Please restart the app and try again.");
            //InAppBrowser.CloseBrowser();
            return;
        }

        if (resp.Context == "demoDesign")
        {
            var iabData = resp.GetData<IABMDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_ROOM_NAME = iabData.rid;// center.name;
            app.account.CURRENT_VERSION_ID = iabData.vid;

            // Messenger<string, bool>.Broadcast(AnalyticsCalls.SceneConfirmationResponse, app.account.CURRENT_VERSION_ID, true);

            // app.CurrentSceneType = SceneType.ReadOnlyDesign;
            app.CurrentSceneType = SceneType.UserRoom;
            //InAppBrowser.CloseBrowser();
            // WebViewService.Instance.close();



            await Task.Delay(100);
            app.Navigate(AppPage.Designer);
        }
        else if (resp.Context == "designChallenge")
        {
            var iabData = resp.GetData<IABMChallenge>();
            app.account.CURRENT_CHALLENGE_ID = iabData._id;
            app.CurrentSceneType = SceneType.DesignChallenge;
            AppConfig.PilotDemoConfig.ProjectId = AppConfig.ProjectConfig.PROJ_CHALLENGE;

            // WebViewService.Instance.close();
            //InAppBrowser.CloseBrowser();
            Debug.LogError("navigationnC");
            await Task.Delay(100);

            app.Navigate(AppPage.DesignChallenge);
        }
        else if (resp.Context == "ownVersionDesignEdit")
        {
            Debug.Log(">>>>>>>>>> ownVersionDesignEdit");
            var iabData = resp.GetData<IABMDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_ROOM_NAME = iabData.rid;
            app.account.CURRENT_VERSION_ID = iabData.vid;

            Debug.Log(iabData.pid + ", " + iabData.rid + ", " + iabData.vid);

            app.CurrentSceneType = SceneType.UserRoom;
            // if (iabData.pid == AppConfig.ProjectConfig.PROJ_CHALLENGE)
            // {
            //     app.CurrentSceneType = SceneType.ReadOnlyDesign;
            // }
            // else
            // {
            //     app.CurrentSceneType = SceneType.UserRoom;
            // }

            // Debug.LogWarning(">>>>>>>>Current Scene Type: " + app.CurrentSceneType);

            //InAppBrowser.CloseBrowser();
            // WebViewService.Instance.close();
            await Task.Delay(100);
            app.Navigate(AppPage.Designer);
        }
        else if (resp.Context == "ownVersionDesignCreate")
        {
            var iabData = resp.GetData<IABMDesign>();
            AppConfig.PilotDemoConfig.ProjectId = iabData.pid;
            app.account.CURRENT_ROOM_ID = iabData.rid;
            app.account.CURRENT_VERSION_ID = iabData.vid;
            app.CurrentSceneType = SceneType.UserRoom;

            //InAppBrowser.CloseBrowser();
            WidgetManager.Instance.LoaderMini("Loading...");
            // WebViewService.Instance.close();
            await Task.Delay(100);
            WidgetManager.Instance.LoaderMiniClose();
            // CreateUserOwnDesign();
            app.Navigate(AppPage.Designer);
        }
        else if (resp.Context == "shop")
        {
            Debug.Log("shot context data: " + resp.data.Replace("\"", "").Trim());

            await OpenWebBrowser(resp.data.Replace("\"", "").Trim());


            /* Analytics need more than url currenlty,
             * Missing values:
             * asset.name, asset.cat
             * 
             * skipping.....
             * */
            //HomefulyAnalyticsEvent.OnShopClicked?.Invoke(asset, shopButtonType);

        }
        else if (resp.Context == "logout")
        {
            Debug.Log("Logout Auth");
            OnLogoutAuth();
        }
        // else if (resp.Context == "checkCameraAccess")
        // {
        //     string context = "cameraAccessStatus";
        //     bool status = true;

        //     #if UNITY_ANDROID
        //         status = Permission.HasUserAuthorizedPermission(Permission.Camera);
        //     #endif
            
        //     WebViewService.Instance.SendMessageToWeb(context, status.ToString());
        //     // webView.EvaluateJavaScript("responseFromUnity("+ context + ", "+ status +")");
        // }
        // else if (resp.Context == "getCameraAccess")
        // {
        //     string context = "cameraAccessGranted";
        //     bool status = true;
            
        //     WebViewService.Instance.hideWeb();
        //     #if UNITY_ANDROID
        //         status = await PermissionManager();
        //     #endif

        //     WebViewService.Instance.showWeb();
        //     WebViewService.Instance.SendMessageToWeb(context, status.ToString());
        //     // webView.EvaluateJavaScript("responseFromUnity("+ context + ", "+ status +")");
        // }
        else if (resp.Context == "appLoginHome")
        {
            print("got applogin callback");
            var iabData = resp.GetData<IABAppLogin>();
            app.account.UpdateLoginInfo(iabData);
            //var appAccount = app.account;
            //appAccount.CRED_JWTTOKEN = iabData.token;
            //appAccount.CRED_USERID = iabData.userid;
            //appAccount.CRED_USEREMAIL = iabData.useremail;
            //appAccount.CRED_USERNAME = iabData.name;
            //appAccount.CRED_PASSWORD = iabData.password;
            //appAccount.CRED_ROLE = iabData.role;
            //appAccount.CRED_AUTHPROVIDER = iabData.authProvider;
            //appAccount.CRED_ISCUSTOMER = iabData.role == "customer" ? 1 : 0;
            //appAccount.CRED_ISDEMO = "not demo";
            await app.db.OnUpdateAuth(true);
            iabData.authProvider = iabData.authProvider.ToLower();
            HomefulyEvents.OnAuthUpdated?.Invoke(iabData.authProvider == "google" ? AuthUpdateType.Google : (iabData.authProvider == "facebook" ? AuthUpdateType.Facebook : (iabData.authProvider == "homefuly" ? AuthUpdateType.InternalSignIn : AuthUpdateType.Error)));
            // WebViewService.Instance.EnableImmersive();
            string url = AppConfig.ServerConfig.webUIHost + "/iab/login/" + System.Uri.EscapeUriString(app.account.CRED_JWTTOKEN) + "/" + AppConfig.GameConfig.nextChallenge.ToString().ToLower();
            // webView = WebViewService.Instance.OpenURL(url);
        }
        else if (resp.Context == "appLoginExpert")
        {
            print("got applogin callback from expert help");
            var iabData = resp.GetData<IABAppLogin>();
            app.account.UpdateLoginInfo(iabData);
            //var appAccount = app.account;
            //appAccount.CRED_JWTTOKEN = iabData.token;
            //appAccount.CRED_USERID = iabData.userid;
            //appAccount.CRED_USEREMAIL = iabData.useremail;
            //appAccount.CRED_USERNAME = iabData.name;
            //appAccount.CRED_PASSWORD = iabData.password;
            //appAccount.CRED_ROLE = iabData.role;
            //appAccount.CRED_AUTHPROVIDER = iabData.authProvider;
            //appAccount.CRED_ISCUSTOMER = iabData.role == "customer" ? 1 : 0;
            //appAccount.CRED_ISDEMO = "not demo";
            await app.db.OnUpdateAuth(true);
            iabData.authProvider = iabData.authProvider.ToLower();
            HomefulyEvents.OnAuthUpdated?.Invoke(iabData.authProvider == "google" ? AuthUpdateType.Google : (iabData.authProvider == "facebook" ? AuthUpdateType.Facebook : (iabData.authProvider == "homefuly" ? AuthUpdateType.InternalSignIn : AuthUpdateType.Error)));
            // WebViewService.Instance.EnableImmersive();
        }
        else if (resp.Context == "expertHelpPressed")
        {
            HomefulyAnalyticsEvent.OnExpertHelpPressed?.Invoke();
        }
        else if (resp.Context == "expertImagesUploaded")
        {
            HomefulyAnalyticsEvent.OnExpertHelpImgUpload?.Invoke();
            Debug.Log("Expert image upload called");
        }
        else if (resp.Context == "expertHelpSubmitted")
        {
            HomefulyAnalyticsEvent.OnExpertHelpSubmitted?.Invoke();
        }
        else
        {
        }
    }

    // public IEnumerator<bool> PermissionManager()
    // {
    //         if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
    //         {
    //             Permission.RequestUserPermission(Permission.Camera);
    //         } 
    //         yield return Permission.HasUserAuthorizedPermission(Permission.Camera);
    // }

    private IEnumerator SkipAFrame()
    {
        yield return null;
    }


    #endregion

    [System.Serializable]
    class IABResponse<T>
    {
        public bool err = new bool();
        public string context = string.Empty;
        public T data = default(T);
    }

    [System.Serializable]
    class IABMDesign
    {
        public string pid = string.Empty;
        public string rid = string.Empty;
        public string vid = string.Empty;
    }

    [System.Serializable]
    class IABMChallenge
    {
        public string _id = string.Empty;

    }

    [System.Serializable]
    class IABMShopLink
    {
        public string productUrl = string.Empty;
    }

    #region Dumpyard

    //[System.Serializable]
    //class IABMUserDetails
    //{
    //    public string token;
    //    public string id;
    //    public string name;
    //    public string email;
    //    public string role;
    //}

    //[System.Serializable]
    //class IABMIsHomepage
    //{
    //    public bool isHome;
    //}

    //StartCoroutine(Waitforasec());
    // await app.account.Connect();
    // InAppBrowser.OpenURL(demoPageUrl, new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true });

    /*IEnumerator Waitforasec() // 1/10th of a sec
    {
        yield return new WaitForSeconds(0.1f);
        // InAppBrowser.OpenURL(
        //     demoPageUrl + "/user/" + app.account.CRED_USERID + "/token/" + app.account.CRED_JWTTOKEN.Substring(4),
        //     new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true }
        // );

        // Ionic Web ui
        //InAppBrowser.OpenURL(
        //  "https://homefuly-web2.firebaseapp.com/game-homescreen/user/" + app.account.CRED_USERID + "/token/" + app.account.CRED_JWTTOKEN.Substring(4),
        //         new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true }
        //     );
        
        Debug.Log(AppConfig.ServerConfig.inAppBrowserHost + "/iab?token=" + app.account.CRED_JWTTOKEN);

        // Angular based Game App WebUI
        InAppBrowser.OpenURL(
         AppConfig.ServerConfig.inAppBrowserHost + "/iab?token=" + app.account.CRED_JWTTOKEN,
                new InAppBrowser.DisplayOptions { displayURLAsPageTitle = false, pageTitle = "Demo Designs", hidesTopBar = true, androidBackButtonCustomBehaviour = true, hidesDefaultSpinner = true }
            );


    }*/
    #endregion
}

[System.Serializable]
public class IABAppLogin
{
    public string token;
    public string userid;
    public string name;
    public string useremail;
    public string password;
    public string authProvider;
    public string role;
}
