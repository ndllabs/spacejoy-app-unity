﻿namespace Homefuly.Events
{
    public static class HomefulyEvents
    {
        /// <summary>
        /// Fires Event When The New Unity Scene Is Loaded And The Scene Manager has Completed Load
        /// </summary>
        /// <param name="sceneName">Name of the Scene</param>
        /// <param name="sceneType">Enum, Type of the Scene Thats currently Loaded</param>
        public delegate void SceneReady(string sceneName, SceneType sceneType);
        public static SceneReady OnSceneReady;

        /// <summary>
        /// When Any action Fails..
        /// </summary>
        /// <param name="actionType">What Failed</param>
        /// <param name="errCode">Why did it fail</param>
        public delegate void ActionFailed(ActionType actionType, int errCode);
        public static ActionFailed OnActionFailed;

        /// <summary>
        /// Fired When A User Logs in
        /// </summary>
        /// <param name="authUpdateType"></param>
        /// <param name="errCode"></param>
        public delegate void AuthUpdated(AuthUpdateType authUpdateType, int errCode = 0);
        public static AuthUpdated OnAuthUpdated;

        public delegate void ExpandButton();
        public static ExpandButton OnExpandButtonPressed;

        public delegate void ViewButton();
        public static ViewButton OnViewButtonPressed;

        public delegate void AddProductButton();
        public static AddProductButton OnAddProductPressed;

        public delegate void MinimiseButton();
        public static MinimiseButton OnMinimise;
    }

    public enum AuthUpdateType
    {
        Facebook,
        Google,
        InternalSignIn,
        InternalSignUp,
        Error,
        UserDenial
    }
}
