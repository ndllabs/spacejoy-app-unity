﻿


using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Homefuly.Analytics;
using _unity.Events;
using System.Threading.Tasks;
using System.Collections;
// using Homefuly.WebView;


#if UNITY_EDITOR
using UnityEditor;
using System.Reflection;
#endif


namespace Homefuly
{

    public static class HomefulyExtensions
    {
        static int fingerID = -1;

        static List<int> generatedIds = new List<int>();



        static HomefulyExtensions()
        {

        }

        public static void GotoHome()
        {
#if UNITY_ANDROID
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
#endif
        }
        /// <summary>
        /// Checks whether the string has any of the input params
        /// </summary>
        /// <param name="haystack"></param>
        /// <param name="needles"></param>
        /// <returns></returns>
        public static bool ContainsAny(this string haystack, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (haystack.Contains(needle))
                    return true;
            }

            return false;
        }

        public static Vector3? GetCenter(this Transform trans, Vector3? offset = null)
        {
            if (trans.GetComponent<MeshFilter>() == null)
            {
                Debug.LogError("No MEsh Filter Found, Wtf");
                return null;
            }

            var pos = trans.GetComponent<MeshFilter>().mesh.bounds.center;
            if (offset != null)
            {
                Debug.Log("Adding a Offset of: " + (Vector3)offset);
                pos += (Vector3)offset;
            }

            return pos;
        }

        public static string ToArrString(this Vector2[] vecArr)
        {
            string str = "";

            foreach (var vec in vecArr)
            {
                str += vec.ToString() + ", ";
            }

            return str;
        }

        public static Vector3 GetRealScale(this Transform trans)
        {
            Vector3 vec = trans.GetComponent<MeshFilter>().mesh.bounds.size;
            vec.x *= trans.localScale.x;
            vec.y *= trans.localScale.y;
            vec.z *= trans.localScale.z;
            return vec;
        }
        public static Vector3 GetRealScaleBoxCollider(this Transform trans)
        {
            Vector3 vec = trans.GetComponent<BoxCollider>().bounds.extents;
            //vec.x *= trans.localScale.x;
            //vec.y *= trans.localScale.y;
            //vec.z *= trans.localScale.z;
            return vec;
        }

        public static Bounds GetTotalRenderBounds(this Transform trans)
        {
            bool hasBounds = false;
            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

            foreach (Transform t in trans)
            {
                if (t.GetComponent<MeshRenderer>() != null)
                {

                    Renderer childRenderer = t.GetComponent<Renderer>();
                    if (childRenderer != null)
                    {
                        if (hasBounds)
                        {
                            bounds.Encapsulate(childRenderer.bounds);
                        }
                        else
                        {
                            bounds = childRenderer.bounds;
                            hasBounds = true;
                        }
                    }
                }
            }


            return bounds;
            //Debug.Log(" collider.size : " + collider.size);

        }

        /// <summary>
        /// Geneates and return a 5 digit Id Number 
        /// </summary>
        /// <returns></returns>
        public static int randomIdGenerator()
        {
            int eid = UnityEngine.Random.Range(10000, 99999);

            if (generatedIds.Contains(eid)) return randomIdGenerator();

            generatedIds.Add(eid);
            return eid;
        }

        public static Vector3 GetAlignedTransform(this Transform target, float distanceMultiplier = 1.8f)
        {
            Vector3 vec = (Vector3)target.GetCenter();
            Vector3 scale = target.GetRealScale();


            vec.y = target.position.y;

            if (scale.x > scale.z)
            {
                vec.z -= Mathf.Sign(vec.z) * distanceMultiplier;
            }
            else
            {
                vec.x -= Mathf.Sign(vec.x) * distanceMultiplier;
            }

            return vec;
        }

        public static Vector3 GetAlignedTransformSurface(this Transform target, float offset = .004f)
        {
            Vector3 vec = (Vector3)target.GetCenter();
            Vector3 scale = target.GetRealScale();


            vec.y = target.position.y;

            if (Mathf.Abs(scale.x) > Mathf.Abs(scale.z))
            {
                vec.z -= Mathf.Sign(vec.z) * ((scale.z * .5f) + offset);
            }
            else
            {
                vec.x -= Mathf.Sign(vec.x) * ((scale.x * .5f) + offset);
            }
            return vec;
        }

        public static Vector3 GetPosAlignedTransformSurface(this Transform target, Vector3 relativePlayerPos, float offset = .004f)
        {
            Vector3 vec = (Vector3)target.GetCenter();
            //vec.x = -1 * vec.x; //no more required
            Vector3 scale = target.GetRealScale();

            vec.y = target.position.y;

            if (Mathf.Abs(scale.x) > Mathf.Abs(scale.z))
            {
                if (relativePlayerPos.z > vec.z)
                {
                    vec.z += (scale.z * .5f) + offset;
                }
                else
                {
                    vec.z -= (scale.z * .5f) + offset;
                }
            }
            else
            {
                if (relativePlayerPos.x > vec.x)
                {
                    vec.x += (scale.x * .5f) + offset;
                }
                else
                {
                    vec.x -= (scale.x * .5f) + offset;
                }
            }
            return vec;
        }

        public static Vector3 GetAlignedRotationEuler(this Transform target)
        {
            Vector3 scale = target.GetRealScale();

            if (Mathf.Abs(scale.x) > Mathf.Abs(scale.z))
                return new Vector3(0, 0, 0);
            else
                return new Vector3(0, 90, 0);
        }

        public static Vector3 GetPosAlignedRotationEuler(this Transform target, Vector3 relativePlayerPos)
        {
            Vector3 scale = target.GetRealScale();
            Vector3 vec = (Vector3)target.GetCenter();
            //vec.x = -1 * vec.x; no more required

            if (Mathf.Abs(scale.x) > Mathf.Abs(scale.z))
            {
                if (relativePlayerPos.z > vec.z)
                    return new Vector3(0, -180, 0); //+z
                else
                    return new Vector3(0, 0, 0); //-z
            }
            else
            {
                if (relativePlayerPos.x > vec.x)
                    return new Vector3(0, -90, 0); //+x
                else
                    return new Vector3(0, 90, 0); //-x
            }
        }


        public static bool ClickedOnUI()
        {
            if (Input.touchCount > 0)
            {
                fingerID = Input.GetTouch(0).fingerId;
            }

            return EventSystem.current.IsPointerOverGameObject(fingerID);
        }


        private static List<RaycastResult> tempRaycastResults = new List<RaycastResult>();

        public static bool PointIsOverUI()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current)
            {
                position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
            };

            if (Input.touchCount > 0)
            {
                eventDataCurrentPosition.position = Input.GetTouch(0).position;
            }

            tempRaycastResults.Clear();

            EventSystem.current.RaycastAll(eventDataCurrentPosition, tempRaycastResults);

            return tempRaycastResults.Count > 0;
        }


        /// <summary>
        /// Converts string to  money rep formated string eg: 1,000,000
        /// </summary>
        /// <param name="money"></param>
        /// <returns></returns>
        public static string ToMoney(this float money)
        {
            //To Add Separator After every three Chars from right
            char[] charArray = Mathf.Round(money).ToString().ToCharArray();
            Array.Reverse(charArray);
            string i = new string(charArray);
            i = Regex.Replace(i, ".{3}", "$0,");
            charArray = i.ToCharArray();
            Array.Reverse(charArray);
            i = new string(charArray);
            if (i.ElementAt(0) == ',')
                i = i.Remove(0, 1);

            return i;
        }

        /// <summary>
        /// Converts String Title Case
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToTitleCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;

            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary>
        /// Rebuilds the Layout immediately
        /// </summary>
        /// <param name="transform"></param>
        public static void RefreshRectSize(this Transform transform)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform.GetComponent<RectTransform>());
        }

        /// <summary>
        /// Destorys all Children objects attached to this transform
        /// </summary>
        /// <param name="transform"></param>
        public static void DestroyChildren(this Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; --i)
            {
                UnityEngine.Object.Destroy(transform.GetChild(i).gameObject);
            }
            transform.DetachChildren();
        }


        /// <summary>
        /// Converts a given string to specified enum type, if possibel, else returns the default enum value
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TEnum ToEnum<TEnum>(this string value) where TEnum : struct
        {
            TEnum parsed;
            if (Enum.TryParse<TEnum>(value, out parsed))
                return parsed;

            var found = typeof(TEnum).GetMembers()
                .Select(x => new
                {
                    Member = x,
                    Attribute = x.GetCustomAttributes(typeof(EnumMemberAttribute), false).OfType<EnumMemberAttribute>().FirstOrDefault()
                })
                .FirstOrDefault(x => x.Attribute?.Value == value);
            if (found != null && Enum.IsDefined(typeof(TEnum), found.Member.Name))
                return (TEnum)Enum.Parse(typeof(TEnum), found.Member.Name);

            Debug.LogWarning("[Extensions] [ToEnum] Unable To Parse string to Enum, Choosing the dafault, which may lead to unwanted results. Str: " + value + ", enum: " + typeof(TEnum) + ", Hence Choosing the Default: " + default(TEnum).ToString());
            return default(TEnum);
        }

        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }

        public static T DeepCopy<T>(T other)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, other);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }

        public static IPAddress GetLocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        public static IPAddress GetDefaultGateway()
        {
            IPAddress result = null;
            var cards = NetworkInterface.GetAllNetworkInterfaces().ToList();
            if (cards.Any())
            {
                foreach (var card in cards)
                {
                    var props = card.GetIPProperties();
                    if (props == null)
                        continue;

                    var gateways = props.GatewayAddresses;
                    if (!gateways.Any())
                        continue;

                    var gateway =
                        gateways.FirstOrDefault(g => g.Address.AddressFamily.ToString() == "InterNetwork");
                    if (gateway == null)
                        continue;

                    result = gateway.Address;
                    break;
                };
            }

            return result;
        }

        /*public static IPAddress CheckIP()
        {
            myExtIPWWW = WWW("http://checkip.dyndns.org");
            if (myExtIPWWW == null) return;
            yield myExtIPWWW;
            myExtIP = myExtIPWWW.data;
            myExtIP = myExtIP.Substring(myExtIP.IndexOf(":") + 1);
            myExtIP = myExtIP.Substring(0, myExtIP.IndexOf("<"));
            // print(myExtIP);
        }*/

        public static IWallfinishEntry ToEntry(this IWallfinishHotspot wf)
        {
            return new IWallfinishEntry
            {
                wallfinishData = wf.wallfinish,
                entryId = randomIdGenerator().ToString(),
                wall = wf.wall,
            };
        }

        /// <summary>
        /// SPlit List to Smaller lists
        /// </summary>
        /// <param name="locations"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>\
        public static List<List<T>> SplitList<T>(this List<T> items, int sliceSize = 30)
        {
            List<List<T>> list = new List<List<T>>();
            for (int i = 0; i < items.Count; i += sliceSize)
                list.Add(items.GetRange(i, Math.Min(sliceSize, items.Count - i)));
            return list;
        }

        ///// <summary>
        ///// Deep Cloning, Extension method
        ///// Note: Based on Serialization, hence requires serialized class
        ///// </summary>
        ///// <typeparam name="T">Any Serialized calss</typeparam>
        ///// <param name="source">The serialized class that has to be deep cloned</param>
        ///// <returns></returns>
        //public static T Clone<T>(this T source)
        //{
        //    DataContractSerializer serializer = new DataContractSerializer(typeof(T));
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        serializer.WriteObject(ms, source);
        //        ms.Seek(0, SeekOrigin.Begin);
        //        return (T)serializer.ReadObject(ms);
        //    }
        //}

        //public static async void OpenProductStorePage(string url)
        //{
        //    Messenger<IAsset, AppPage>.Broadcast(StaticFunctionCalls.ShopButtonClicked, asset.asset, AppPage.Home);
        //    //Application.OpenURL(url);
        //    InAppBrowser.OpenURL(url);
        //    Debug.Log(url);
        //}

        public static async void OpenProductStorePage(IAsset asset, ShopButtonType shopButtonType)
        {
            // WidgetManager.Instance.LoaderMini("Loading Website");
            // await SetScreenOrientationAndWait(false);
            // await Task.Delay(100);

            // WidgetManager.Instance.LoaderMiniClose();
            // HomefulyAnalyticsEvent.OnShopClicked?.Invoke(asset, shopButtonType);

            // WebViewService.Instance.SetWebView();
            // WebViewService.Instance.EnableTopBar(true);

            // string url = !string.IsNullOrEmpty(asset.designedBy.productUrl)
            //                           ? asset.designedBy.productUrl
            //                           : "https://www.google.co.in/search?q=" + asset.name;
            // url = Uri.EscapeUriString(url);

            // var wv = WebViewService.Instance.OpenURL(url);

            // wv.OnShouldClose += Wv_OnShouldClose;

            Debug.Log("[Unity] Opening product store page");

            SetScreenOrientation(false);
            string url = !string.IsNullOrEmpty(asset.designedBy.productUrl)
                                      ? asset.designedBy.productUrl
                                      : "https://www.google.co.in/search?q=" + asset.name;
            url = Uri.EscapeUriString(url);
            IonicComms.FinishActivity("productPage|" + url);
        }

        // private static bool Wv_OnShouldClose(UniWebView webView)
        // {
        //     webView.OnShouldClose -= Wv_OnShouldClose;
        //     // added by praveen:
        //     // the webview object wasn't destroyed but the webview component was destroyed when closing webview
        //     // this was the issue with shop button not working
        //     WebViewService.Instance.ClearWebViewObject();
        //     Debug.Log("Wv_Onshould close triggered");
        //     SetScreenOrientation(true);
        //     return true;
        // }

        public static void SetScreenOrientation(bool toLandspace)
        {

#if UNITY_EDITOR
            SetEditorPlaySize(toLandspace ? /*16*/4 : 3);
#else
            //Screen.autorotateToPortrait = setToLandspace ? false : true;
            //Screen.autorotateToPortraitUpsideDown = false;
            //Screen.autorotateToLandscapeRight = Screen.autorotateToLandscapeLeft = setToLandspace ? true : false;
            
            Screen.orientation = toLandspace ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;

            //Screen.fullScreen = isLandscape;
#endif
        }

        public static IEnumerator SetScreenOrientationAndWait(bool toLandspace)
        {
#if UNITY_EDITOR
            SetEditorPlaySize(toLandspace ? /*16*/4 : 3);
            yield return null;
#else
            bool completed = Screen.orientation == (toLandspace ? ScreenOrientation.Landscape : ScreenOrientation.Portrait);
            Screen.orientation = toLandspace ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;

            int currentframe = 0;
            while (!completed)
            {
                currentframe++;
                completed = (Screen.orientation == ScreenOrientation.Portrait);
                yield return null;
            }

            Debug.Log("\n\n==Total Frame orientation took: " + currentframe + "==\n\n");
#endif
        }

#if UNITY_EDITOR
        public static void SetEditorPlaySize(int index)
        {
            var gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
            var gvWnd = EditorWindow.GetWindow(gvWndType);
            var SizeSelectionCallback = gvWndType.GetMethod("SizeSelectionCallback",
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            SizeSelectionCallback.Invoke(gvWnd, new object[] { index, null });
        }
#endif

        public static void AskAppFeedback(Action callback = null)
        {
            var app = AppComponent.Instance;
            if (app.account.CRED_ISDEMO != string.Empty)
            {
                if (app.db.userSavedData.challengesCompleted.Count > 5 && (!PlayerPrefs.HasKey("APPFEEDBACK_REQUEST") || PlayerPrefs.GetString("APPFEEDBACK_REQUEST") == string.Empty))
                {
                    var feedback = GameObject.Find("Feedback");
                    feedback.GetComponent<EasyFeedback.FeedbackForm>().Show(callback);
                    PlayerPrefs.SetString("APPFEEDBACK_REQUEST", "true");
                }
                else
                {
                    callback?.Invoke();
                }
            }
            else
            {
                callback?.Invoke();
            }
        }
    }
}