﻿namespace Homefuly
{

    public enum CategoryType
    {
        Root,
        Category,
        Subcategory,
        Vertical
    }

    public enum SceneType
    {
        None,
        OldSchool,
        DesignChallenge,
        UserRoom,
        ReadOnlyDesign,
        Enterprise
    }

    /// <summary>
    /// Donot Alter the Case of the Enums, Serilazers depend on it
    /// </summary>
    public enum RetailerType
    {
        none,
        furniture,
        paint
    }

    public enum RoomType
    {
        general,
        bedroom,
        livingroom
    }


    public enum DesignChallengeState
    {
        none,
        Load,
        Play,
        End
    }

    public enum ActionType
    {
        Auth,
        Swap,
        Delete,
        Select,
        Sceneload,
        ProductLoad,
        SwapWallfinish,
        Transaction,
        ChallengeSubmit,
        Other
    }

    /// <summary>
    /// wallfinsh type
    /// </summary>
    public enum wfType
    {
        none,
        paint,
        wallpaper
    }

    public enum WallfinishCategory
    {
        none,
        stencil,
        shade,
        wallpaper,
        texture
    }

    /// <summary>
    /// Represents What Part of Database Is being Updated Currently
    /// </summary>
    public enum Db
    {
        Whole,
        SaveGame,
        Design,
        Objects,
        Wallfinish,
        Image,
        Room,
        Cache
    }

    public enum Country
    {
        None,
        India,
        US
    }

    public enum MountType
    {
        floor,
        wall,
        attached,
        free,
        ceiling
    }

    public enum Tags
    {
        Wall,
        Asset,
        Poster,
        Floor,
        Roof
    }

    public enum AuthType
    {
        facebook,
        google,
        homefuly
    }
}
