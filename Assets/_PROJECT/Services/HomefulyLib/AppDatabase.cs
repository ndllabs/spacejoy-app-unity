﻿/* Contains and Controls Game Data
 * 
 * prajwalshetty2018@gmail.com
 * */

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Homefuly.Database
{
    public class AppDatabase
    {
        public delegate void DatabaseReady();
        public static DatabaseReady OnDBReady;

        public delegate void DatabaseUpdate(Db updateType);
        public static DatabaseUpdate OnDBUpdate;

        #region ReadOnly

        #region SceneIndependent
        public ISaveGame userSavedData { get { return _userSavedData;  } private set { } } //{ get { return _userSavedData.Clone(); }} //{ get { return ISaveGame.CreateCopy(userSavedData); } private set { } }
        public Dictionary<string, ICategory> categories { get; private set; }
        public Dictionary<string, IRetailer> retailers { get; private set; }
        public Dictionary<string, ITheme> themes { get; private set; }

        #endregion

        #region SceneDependent
        public SceneType sceneType { get; private set; } = SceneType.None;
        public IRoomDesign CurrentDesign { get { return _currentDesign; } private set { } }//{ get { return _currentDesign.Clone(); } } //{ get { return IRoomDesign.CreateCopy(_currentDesign); }}
        public IDesignChallenge designChallenge { get; private set; }
        public RoomType currentRoomType { get; private set; }
        public GameObject currentRoom { get; private set; }
        public bool isDesignDirty;
        public Dictionary<string, IAsset> legallyOwnedObjects { get; private set; }

        #endregion

        public Dictionary<string, IAssetEntry> sceneAssets { get; private set; }
        public Dictionary<string, IWallfinishEntry> sceneWallfinishs { get; private set; }
        #endregion

        #region ReadWrite
        /// <summary>
        /// Current Selected asset that is being modified in the design.
        /// </summary>
        public ISceneObject currentObject;
        /// <summary>
        /// Current wall that is being modified/or being applied paint to, in the design.
        /// </summary>
        public IWallfinishEntry currentWall;
        #endregion

        #region Cache
        public Dictionary<string, IAsset> cache_StoreAsset { get; private set; } = new Dictionary<string, IAsset>();
        public Dictionary<string, IWallfinish> cache_StorePaint { get; private set; } = new Dictionary<string, IWallfinish>();
        public Dictionary<string, IMeshCache> cache_MeshData { get; private set; }
        #endregion

        #region Internal
        private AppComponent app;
        private const string root = "root";
        private IRoomDesign _currentDesign;
        private ISaveGame _userSavedData;
        private bool isInit = false;
        #endregion

        #region Constructor n Init
        /// <summary>
        /// Warning: AppDatabase requires Auth and a Internet connection to Init
        /// </summary>
        public AppDatabase()
        {

            //SceneManager.sceneLoaded += OnSceneChanged;

            //Debug.LogError("listining yey ,, igationnC");
            ////Init();
        }

        ~AppDatabase()
        {
            //SceneManager.sceneLoaded -= OnSceneChanged;
            //Debug.LogError("say no morea");
        }

        //private async void OnSceneChanged(Scene loadedScene, LoadSceneMode loadMode)
        //{
        //    switch (loadedScene.name)
        //    {
        //        //case nameof(AppPage.YourDesign_Deprycated):
        //        //    await RefreshDb(SceneType.YourRoomDesign_Deprycated);
        //        //    break;
        //        case nameof(AppPage.DesignChallenge):
        //            await RefreshDb(SceneType.DesignChallenge);
        //            break;
        //        default:
        //            //Debug.Log("Loaded: " + loadedScene.name);
        //            break;
        //    }

        //    OnDBReady?.Invoke();
        //}

        public async void Init()
        {
            isInit = false;
            isDesignDirty = false;

            app = AppComponent.Instance;
            await Task.WhenAll(GetCategories(), GetUserSaveGame(), GetRetailers()/*, UpdateThemes()*/);

            isInit = true;
            OnDBReady?.Invoke();

            Debug.Log("[" + nameof(AppDatabase) + "][" + nameof(Init) + "] Database Ready, Time: " + System.DateTime.Now);
        }

        private bool IsDbReady()
        {
            if (!isInit)
                Debug.LogWarning("[" + nameof(AppDatabase) + "] [" + nameof(IsDbReady) + "] Database not ready yet");

            return isInit;
        }

        #endregion

        /// <summary>
        /// Refreshes the Whole Database with the server data, (Cats, Retailers, CurrentDesign)
        /// <param name="sType"></param>
        /// <param name="keepCache">Caches Some Data for reuse and decreases future Loadtime</param>
        /// <returns></returns>
        public async Task<bool> RefreshDb(SceneType sType = SceneType.OldSchool, bool keepCache = true)
        {
            if (!IsDbReady()) return false;

            sceneAssets = new Dictionary<string, IAssetEntry>();
            sceneWallfinishs = new Dictionary<string, IWallfinishEntry>();
            currentObject = null; currentWall = null;
            designChallenge = null; _currentDesign = null;
            sceneType = sType;


            if (string.IsNullOrEmpty(app.account.CURRENT_ROOM_NAME))
                app.account.CURRENT_ROOM_NAME = "";

            var rName = app.account.CURRENT_ROOM_NAME.ToLower().Replace(" ", "");
            currentRoomType = (rName.Contains(RoomType.livingroom.ToString())) ? RoomType.livingroom :
                              (rName.Contains(RoomType.bedroom.ToString())) ? RoomType.bedroom :
                               RoomType.general;

            switch (sceneType)
            {
                case SceneType.None:
                    Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(RefreshDb) + "] Exiting Without Data Fetch becuase scene type is not specified");
                    break;
                case SceneType.OldSchool:
                    if (!await GetCurrentDesign()) return false;
                    break;
                case SceneType.DesignChallenge:
                    if (!await GetCurrentDesignChallenge()) return false;
                    break;
                case SceneType.UserRoom:
                    if (!await GetCurrentDesign()) return false;
                    break;
                case SceneType.ReadOnlyDesign:
                    if (!await GetCurrentDesign()) return false;
                    break;
                case SceneType.Enterprise:
                    break;
            }

            if (!keepCache)
            {
                cache_StoreAsset = new Dictionary<string, IAsset>();
                cache_StorePaint = new Dictionary<string, IWallfinish>();
                cache_MeshData = null;
            }

            OnDBUpdate?.Invoke(Db.Whole);

            return true;
        }

        /// <summary>
        /// Completly Resets The db and re initiates it with new user and catalogue data,
        /// Warning: Use Only when a new login/logout event occurs
        /// </summary>
        public void ResetDb()
        {
            isInit = false;
            Init();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isPrevUserDemo">i.e the user who was logged in, before the current user, is it a demo account? if yes, transfer the coins  to the user</param>
        /// <returns></returns>
        public async Task OnUpdateAuth(bool isPrevUserDemo)
        {
            //legallyOwnedObjects = new Dictionary<string, IAsset>();

            var prevUserCoins = 0f;
            var prevOwnedAssets = new List<IAsset>();
            var prevOwnedPaints = new List<string>();

            if (isPrevUserDemo)
            {
                Debug.Log("Previously demo waas logged n, saving demo's coins " + userSavedData.currentCoins);
                prevUserCoins = userSavedData.currentCoins;
                prevOwnedAssets = userSavedData.assetsOwned;
                prevOwnedPaints = userSavedData.paintOwned;
            }

            _userSavedData = await app.server.artifact.GetUserSavedGame(app.account.CRED_USERID);

            if (_userSavedData == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetUserSaveGame) + "] No Internet Connection | Server Offline | No Authorization"); return; }

            if (_userSavedData.currentCoins == 0)
            {
                Debug.Log("user has zero coins transfaerring " + prevUserCoins + " to user");
                _userSavedData.currentCoins = prevUserCoins;
            }

            var aowned = userSavedData.assetsOwned_Indexed;

            foreach (var pAOwn in prevOwnedAssets)
            {
                if (!aowned.ContainsKey(pAOwn._id))
                    userSavedData.assetsOwned.Add(pAOwn);
            }
            foreach (var pAOwn in prevOwnedPaints)
            {
                if (!userSavedData.paintOwned.Contains(pAOwn))
                    userSavedData.paintOwned.Add(pAOwn);
            }


            if (isPrevUserDemo) await SyncUserSaveGameToServer();

            Debug.Log("Done updating usr coins");

            //legallyOwnedObjects = _currentDesign.assets
            //    .GroupBy(p => p.asset._id)
            //    .ToDictionary(p => p.First().asset._id, p => p.First().asset);
        }

        #region Update
        /// <summary>
        /// Updates the Current User Save Game to the Specified one
        /// Note: Server Syncing Will happen in background, the fucntion returns immediately
        /// TO DO: Push to Server will also invoke a local save game to file as well incase user is offline
        /// </summary>
        /// <param name="saveGame"></param>
        /// <param name="syncWithServer"> should the Save game be pushed and synced with the server</param>
        public async void UpdateUserSaveGame(ISaveGame saveGame, bool syncWithServer = false)
        {
            _userSavedData = ISaveGame.CreateCopy(saveGame);
            OnDBUpdate?.Invoke(Db.SaveGame);

            if (syncWithServer) await SyncUserSaveGameToServer();
        }

        /// <summary>
        /// Uploads User Saved Room to The Server
        /// </summary>
        /// <param name="camTransform"> transform of the main camera at the time of saving</param>
        public async Task<bool> SyncUserRoomWithServer(ITransform camTransform, bool createNewVersion = false)
        {
            Debug.Log("Syncing with server");
            var roomDesign = IRoomDesign.CreateCopy(CurrentDesign);
            
            roomDesign.camera = camTransform;
            UpdateAssetTransformData();

            if (!AppConfig.GameConfig.creativeMode)
            {
                //Dont Include unbought assets during server sync
                var indAData = userSavedData.assetsOwned_Indexed;
                roomDesign.assets = sceneAssets.Values.Where(x => legallyOwnedObjects.ContainsKey(x.asset._id) || indAData.ContainsKey(x.asset._id))
                                    .ToList();
            }
            else
            {
                roomDesign.assets = sceneAssets.Values.ToList();
            }

            UpdateWallfinishAdjustData();
            roomDesign.paint = sceneWallfinishs.Values.ToList();

            // Debug.Log("createNewVersion: " + createNewVersion);
            // Debug.Log("customer:  " + roomDesign.customer);
            if(createNewVersion)
            {
                var res = await app.server.artifact.CreateNewRoomVersion(roomDesign);
                Debug.Log("RES:" + res);
                if(res == null)
                {
                    roomDesign.customer = null;
                }
                else
                {
                    roomDesign = res;
                    createNewVersion = false;
                }
            }
            else
            {
                var res = await app.server.artifact.UpdateRoomDesignAsync(roomDesign);
                roomDesign = res==null ? roomDesign : res;
            }
            SetCurrentDesignData(roomDesign);
            isDesignDirty = false;
            Debug.Log("Syncing done");
            return createNewVersion;
        }

        /// <summary>
        /// Uploads User Saved Room to The Server
        /// </summary>
        /// <param name="camTransform"> transform of thSetCurrentDesignDatae main camera at the time of saving</param>
        public async Task<IRoomDesign> SyncUserChallengeRoomWithServer(ITransform camTransform)
        {
            var roomDesign = IRoomDesign.CreateFromChallenge(designChallenge);
            
            roomDesign.camera = camTransform;
            roomDesign.assets = IAssetEntry.CreateFromIHotspotList(designChallenge.baseAssets);
            roomDesign.paint = IWallfinishEntry.CreateFromIWallfinishHotspotList(designChallenge.basePaint);

            var res = await app.server.artifact.AddUserChallengeRoomversion(AppConfig.ProjectConfig.PROJ_CHALLENGE, roomDesign);
            if(res == null) Debug.Log("Error adding challenge roomversion");
            return res;
        }

        /// <summary>
        /// Updates the Current User Saved Coins to the Specified one
        /// Note: Server Syncing Will happen in background, the fucntion returns immediately
        /// </summary>
        /// <param name="currentCoins"> Replace the curreent saved coins, with the new coins specified</param>
        /// <param name="syncWithServer"> should the Save game be pushed and synced with the server</param>
        public async void UpdateUserCoins(float currentCoins, bool syncWithServer = false)
        {
            userSavedData.currentCoins = currentCoins;
            OnDBUpdate?.Invoke(Db.SaveGame);

            if (syncWithServer) await SyncUserSaveGameToServer();
        }

        public async void UpdateUserChallenges(float currentCoins, List<string> challengesCompleted, bool syncWithServer = false)
        {
            userSavedData.currentCoins = currentCoins;
            userSavedData.challengesCompleted = challengesCompleted;
            OnDBUpdate?.Invoke(Db.SaveGame);

            if (syncWithServer) await SyncUserSaveGameToServer();
        }

        /// <summary>
        /// Updates the Current User Saved Coins to the Specified one
        /// Note: Server Syncing Will happen in background, the fucntion returns immediately
        /// </summary>
        /// <param name="currentCoins"> Curent user Coins, Warning: Will replace the DB Value</param>
        /// <param name="ownedAsset">Curent user owned Assets, Warning: Will replace the DB Value</param>
        /// <param name="syncWithServer"></param>
        public async void UpdateUserPurchase_Assets(float currentCoins,List<IAsset> ownedAsset, bool syncWithServer = false)
        {
            userSavedData.currentCoins = currentCoins;
            userSavedData.assetsOwned = ownedAsset;
            OnDBUpdate?.Invoke(Db.SaveGame);

            if (syncWithServer) await SyncUserSaveGameToServer();
        }

        /// <summary>
        /// Updates the Current User Saved Coins to the Specified one
        /// Note: Server Syncing Will happen in background, the fucntion returns immediately
        /// </summary>
        /// <param name="currentCoins"> Curent user Coins, Warning: Will replace the DB Value</param>
        /// <param name="ownedPaint">Curent user owned Assets, Warning: Will replace the DB Value</param>
        /// <param name="syncWithServer"></param>
        public async void UpdateUserPurchase_Wallfinishs(float currentCoins, List<string> ownedPaint, bool syncWithServer = false)
        {
            userSavedData.currentCoins = currentCoins;
            userSavedData.paintOwned = ownedPaint;
            OnDBUpdate?.Invoke(Db.SaveGame);

            if (syncWithServer) await SyncUserSaveGameToServer();
        }


        /// <summary>
        /// Syncs the Itransform class of the entry with go's tranform
        /// </summary>
        public void UpdateAssetTransformData()
        {
            foreach (var asset in sceneAssets)
            {
                if (asset.Value.objRef)
                    asset.Value.transform = new ITransform(asset.Value.objRef.transform);
            }
        }

        /// <summary>
        /// Syncs the IAdjust class of the entry with go's tranform
        /// </summary>
        public void UpdateWallfinishAdjustData()
        {
            foreach (var wallfinish in sceneWallfinishs)
            {
                if (wallfinish.Value.objRef == null) continue;

                var trans = wallfinish.Value.objRef.transform;
                wallfinish.Value.adjust = new IWallfinishAdjust()
                {
                    scale = new Vector3(trans.localScale.x, trans.localScale.y, trans.localScale.z),
                    offset = new Vector3(trans.position.x, trans.position.y, trans.position.z)
                };
            }
        }

        #region delete
        /// <summary>
        /// Remove one of the asset entr from the sceneassets list
        /// </summary>
        /// <param name="entryId"></param>
        /// <returns></returns>
        public bool DeleteSceneAssetEntry(string entryId)
        {
            if (sceneAssets.ContainsKey(entryId))
            {
                sceneAssets.Remove(entryId);
                isDesignDirty = true;
                OnDBUpdate?.Invoke(Db.Objects);
                return true;
            }

            return false;
        }
        public bool DeleteAssetHotspot(IHotspot hotspot)
        {
            if (designChallenge.baseAssets_Indexed.ContainsKey(hotspot._id))
            {
                designChallenge.baseAssets.RemoveAll(x => x._id == hotspot._id);
                return true;
            }

            Debug.LogError("Unexpected, Delete Hotspot Failes for id: " + hotspot._id);
            return false;
        }
        public bool DeleteSceneWallfinishEntry(GameObject wallfinishObjRef)
        {
            IWallfinishEntry tobeRemoved = null;
            foreach (var wallfinish in sceneWallfinishs)
            {
                if (wallfinish.Value.objRef == wallfinishObjRef)
                {
                    Debug.Log("Deleting entry " + wallfinish.Key);
                    tobeRemoved = wallfinish.Value;
                }
            }

            if (tobeRemoved != null)
                return DeleteSceneWallfinishEntry(tobeRemoved.entryId);
            else
                return false;
        }
        public bool DeleteSceneWallfinishEntry(string key)
        {
            if (sceneWallfinishs.ContainsKey(key))
            {
                sceneWallfinishs.Remove(key);
                isDesignDirty = true;
                OnDBUpdate?.Invoke(Db.Wallfinish);
                return true;
            }
            return false;
        }
        #endregion

        #region Add
        /// <summary>
        /// Adds one of the asset entr from the sceneassets list
        /// </summary>
        /// <param name="entryId"></param>
        /// <returns></returns>
        public bool AddSceneAssetEntry(IAssetEntry asset)
        {
            if (!sceneAssets.ContainsKey(asset.entryId))
            {
                sceneAssets.Add(asset.entryId, asset);
                isDesignDirty = true;
                OnDBUpdate?.Invoke(Db.Objects);
                return true;
            }

            return false;
        }
        public bool AddHotspotEntry(IHotspot hotspot)
        {
            designChallenge.baseAssets.Add(hotspot);
            OnDBUpdate?.Invoke(Db.Objects);
            return true;
        }
        public bool UpdateHotspotEntry(IHotspot hotspot)
        {
            int index = designChallenge.baseAssets.FindIndex(a => a._id == hotspot._id);

            if (index >= 0)
                designChallenge.baseAssets[index] = hotspot;
            else
                designChallenge.baseAssets.Add(hotspot);

            OnDBUpdate?.Invoke(Db.Objects);
            return true;
        }
        public bool UpdateWallfinishHotspotEntry(IWallfinishHotspot hotspot)
        {
            int index = designChallenge.basePaint.FindIndex(a => a._id == hotspot._id);

            if (index >= 0)
                designChallenge.basePaint[index] = hotspot;
            else
                designChallenge.basePaint.Add(hotspot);

            OnDBUpdate?.Invoke(Db.Wallfinish);
            return true;
        }
        /// <summary>
        /// Batch add one of the asset entr from the sceneassets list
        /// </summary>
        /// <param name="entryId"></param>
        /// <returns></returns>
        public bool AddSceneAssetEntry(List<IAssetEntry> assets)
        {
            foreach (var entry in assets)
            {
                if (!sceneAssets.ContainsKey(entry.entryId))
                {
                    sceneAssets.Add(entry.entryId, entry);
                }
            }
            OnDBUpdate?.Invoke(Db.Objects);

            return true;
        }
        /// <summary>
        /// Adds a new wallfinish entry
        /// </summary>
        /// <param name="wallfinishEntry"></param>
        public void AddNewWallfinishEntry(IWallfinishEntry wallfinishEntry)
        {
            if (sceneWallfinishs.ContainsKey(wallfinishEntry.entryId)) return;

            sceneWallfinishs.Add(wallfinishEntry.entryId, wallfinishEntry);
            isDesignDirty = true;
            OnDBUpdate?.Invoke(Db.Wallfinish);
        }

        /// <summary>
        /// Adds a new Store Asset data to cache, For a quicker fetch next time
        /// </summary>
        /// <param name="assetData">asset to be added</param>
        public void AddAssetCacheEntry(IAsset assetData)
        {
            if (cache_StoreAsset.ContainsKey(assetData._id)) return;

            cache_StoreAsset.Add(assetData._id, assetData);
            //Messenger<Db>.Broadcast(Up, Db.Cache);
        }
        #endregion


        /// <summary>
        /// Updates the current design data in the db to the one inputed
        /// </summary>
        /// <param name="design"></param>
        public void SetCurrentDesignData(IRoomDesign design)
        {
            _currentDesign = design;//.Clone(); // IRoomDesign.CreateCopy(design);
            OnDBUpdate?.Invoke(Db.Design);
        }

        /// <summary>
        /// Sets the current room gameobject variable
        /// </summary>
        /// <param name="room"></param>
        public void SetCurrentRoom(GameObject room)
        {
            currentRoom = room;
            OnDBUpdate?.Invoke(Db.Room);
        }


        #endregion

        #region Get

        /// <summary>
        /// Gets You all the Categories of type
        /// </summary>
        /// <param name="categoryType">Cat vs SubCat vs Vertical</param>
        /// <returns></returns>
        public Dictionary<string, ICategory> GetCategoriesOfType(CategoryType categoryType)
        {
            return categories.Where(x => x.Value.type == categoryType).ToDictionary(p => p.Key, p => p.Value);
        }

        /// <summary>
        /// Gets You all the Retailers of type
        /// </summary>
        /// <param name="retailerType">furniture retailers vs paint retailers</param>
        /// <returns></returns>
        public Dictionary<string, IRetailer> GetRetailersOfType(RetailerType retailerType, Country country = Country.US)
        {
            return retailers.Where(x => x.Value.service.ToEnum<RetailerType>() == retailerType && x.Value.country.ToEnum<Country>() == country)
                                   .ToDictionary(p => p.Key, p => p.Value);
        }

        /// <summary>
        /// returns all the asset instaces in the scene of the inputed assetid
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public Dictionary<string, IAssetEntry> GetAllInstancesOfAsset(string assetId)
        {
            return sceneAssets.Where(x => x.Value.asset._id == assetId).ToDictionary(p => p.Key, p => p.Value);
        }

        /// <summary>
        /// returns all the unique assets in the scene based on the aid
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, IAsset> GetAllUniqueAssets()
        {
            return sceneAssets.Values.ToList().DistinctBy(x => x.asset._id).ToDictionary(p => p.asset._id, p => p.asset);
        }
        #endregion

        #region Check
        /// <summary>
        /// Checks if any Object is currently Selected and is Valid, Requires a type Sepcifier
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentObjectValid<T>()
        {
            if (currentObject == null || !(currentObject is T))
            {
                Debug.LogWarning("[" + nameof(AppDatabase) + "][" + nameof(IsCurrentObjectValid) + "] Type of CurrentAsset Does not match to " + typeof(T) + ", Ignoring call");
                return false;
            }

            if (typeof(T) == typeof(ISceneObject) && (currentObject as ISceneObject).objRef == null)
                return false;

            return true;
        }

        /// <summary>
        /// Checks if any Object is currently Selected and is Valid, Doesn't a type Sepcifier
        /// Note: Use the one with the type specifier for better stability
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentObjectValid()
        {
            if(currentObject == null)
            {
                return false;
            }

            if (currentObject is ISceneObject && (currentObject as ISceneObject).objRef == null)
                return false;

            return true;
        }

        #endregion

        #region Core

        #region OldSchool
        /// <summary>
        /// get All Cats/SubCats/Verts and store it in flat dictionary with references
        /// </summary>
        /// <returns></returns>
        async Task<bool> GetCategories()
        {
            var catBuffer = await app.server.artifact.GetAllCategories();

            if (catBuffer == null ) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetCategories) + "] No Internet Connection | Server Offline | No Authorization"); return false; }

            categories = new Dictionary<string, ICategory>();
            categories.Add(root, new ICategory()
            {
                _id = root,
                parentCat = string.Empty,
                children = new List<string>()
            });

            foreach (var cat in catBuffer)
            {
                categories.Add(cat._id, new ICategory(cat, root, CategoryType.Category));
                categories[root].children.Add(cat._id);

                foreach (var subcat in cat.subcategories)
                {
                    categories.Add(subcat._id, new ICategory(subcat, cat._id, CategoryType.Subcategory));
                    categories[cat._id].children.Add(subcat._id);

                    foreach (var vert in subcat.verticals)
                    {
                        categories.Add(vert._id, new ICategory(vert, subcat._id, CategoryType.Vertical));
                        categories[subcat._id].children.Add(vert._id);
                    }
                }
            }

            return true;
        }

        async Task GetUserSaveGame()
        {

            _userSavedData = await app.server.artifact.GetUserSavedGame(app.account.CRED_USERID);

            if (_userSavedData == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetUserSaveGame) + "] No Internet Connection | Server Offline | No Authorization"); return; }

            /* Temp:
             * if demo user Set Values to thier default
             * */
            if (AppComponent.Instance.account.CRED_ISDEMO == string.Empty)
            {
                Debug.LogWarning("[" + nameof(AppDatabase) + "][" + nameof(GetUserSaveGame) + "] Setting values to thier default");
                _userSavedData.assetsOwned = new List<IAsset>();
                _userSavedData.paintOwned = new List<string>();
                _userSavedData.challengesCompleted = new List<string>();
                //_userSavedData.currentCoins = AppConfig.GameConfig.DefaultCoins;
                _userSavedData.ownRooms = new List<string>();
                return;
            }
        }


        async Task<bool> GetCurrentDesign()
        {
            _currentDesign = await app.server.artifact.GetRoomDesignAsync(app.account.CURRENT_ROOM_ID, app.account.CURRENT_VERSION_ID);

            if (_currentDesign == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetCurrentDesign) + "] No Internet Connection | Server Offline | No Authorization"); return false; }
            legallyOwnedObjects = _currentDesign.assets
                .GroupBy(p => p.asset._id)
                .ToDictionary(p => p.First().asset._id, p => p.First().asset);

            return true;
        }

        async Task<bool> GetRetailers()
        {
            retailers = (await app.server.artifact.GetAllRetailers()).ToDictionary(x => x._id, x => x);

            if (retailers == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetRetailers) + "] No Internet Connection | Server Offline | No Authorization"); return false; }
            return true;
        }

        /*async Task UpdateThemes()
        {
            themes = new Dictionary<string, ITheme>();
        }*/
        #endregion

        #region DesignChallenge
        /// <summary>
        ///  Fetch Design Challenge
        /// </summary>
        /// <returns></returns>
        async Task<bool> GetCurrentDesignChallenge()
        {
            designChallenge = await app.server.artifact.GetDesignChallenge(app.account.CURRENT_CHALLENGE_ID);


            if (designChallenge == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetCurrentDesignChallenge) + "] No Internet Connection | Server Offline | No Authorization"); return false; }

            return true;
        }

        /// <summary>
        ///  Fetch Design Challenges All
        /// </summary>
        /// <returns></returns>
        // public async Task GetAllDesignChallenges()
        // {
        //     if(!isChallengesSync)
        //     {
        //         Debug.Log("fetching challenges");
        //         designChallengeList = await app.server.artifact.GetAllDesignChallenges();
        //     }
            

        //     if (designChallengeList == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetAllDesignChallenges) + "] No Internet Connection | Server Offline | No Authorization"); return; }
        //     isChallengesSync = true;
        // }

        #endregion

        #region YourRoom
        /// <summary>
        ///  Fetch User Own Room
        /// </summary>
        /// <returns></returns>
        async Task GetUserOwnRoomDesign()
        {
            _currentDesign = await app.server.artifact.GetUserOwnRoomversion(AppConfig.PilotDemoConfig.OrgId, AppConfig.ProjectConfig.PROJ_USER_ROOM, app.account.CURRENT_VERSION_ID);

            if (_currentDesign == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetUserOwnRoomDesign) + "] No Internet Connection | Server Offline | No Authorization"); return; }
        }

        async Task SyncUserSaveGameToServer()
        {
            /* Temp:
             * if demo user Skip Server Syncing
             * */
            if (AppComponent.Instance.account.CRED_ISDEMO == string.Empty)
            {
                Debug.LogWarning("[" + nameof(AppDatabase) + "][" + nameof(SyncUserSaveGameToServer) + "] Server Save Game Syncing Failed. Reason: No Valid Auth | Not a Valid User | Demo/public user account"); return;
            }

            if(!await app.server.artifact.SetUserSavedGame(app.account.CRED_USERID, userSavedData))
                Debug.LogWarning("[" + nameof(AppDatabase) + "][" + nameof(SyncUserSaveGameToServer) + "] Server Save Game Syncing Failed. Reason: Server Rejected Data"); return;

        }

        #region Design Challenge User Version
        /// <summary>
        ///  Fetch Design Challenge User Roomversion
        /// </summary>
        /// <returns></returns>
        async Task GetChallengeUserVersion()
        {
            _currentDesign = await app.server.artifact.GetUserChallengeRoomversion(AppConfig.ProjectConfig.PROJ_USER_ROOM, app.account.CURRENT_CHALLENGE_ID);

            if (_currentDesign == null) { Debug.LogError("[" + nameof(AppDatabase) + "][" + nameof(GetChallengeUserVersion) + "] No Internet Connection | Server Offline | No Authorization"); return; }
        }
        #endregion

        #endregion

        #endregion
    }

}


