﻿using System.Collections.Generic;

namespace Homefuly
{
    public static class ErrorTracking
    {
        //To Do: Replace the dictionary with a compile time constant KeyValuePair
        //public static Dictionary<int, string> ErrorCodesDict = new Dictionary<int, string>{
        //    { 1301, "User Denial" },
        //    { 1302, "Invalid SceneType" },
        //    { 1303, "Invalid ID" },
        //    { 1304, "Not Enough Coins" },
        //    { 1305, "Connection Interrupted" },
        //    { 1306, "No Autherization" },
        //    { 1307, "Product Already Owned" } };


        /// <summary>
        /// ErrorCodes, TO DO Append Desciption..
        /// </summary>
        public enum ErrorCodes: int
        {
            /// <summary>
            /// Default
            /// </summary>
            NoError = 0,
            /// <summary>
            /// If User Denies/Cancels the Operation
            /// </summary>
            User_Denial = 1201,
            User_NoAutherization = 1202,
            Invalid_SceneType = 1302,
            Invalid_ID = 1303,
            Invalid_InputData = 1304,
            NotEnough_Coins = 1404,
            Connection_Interrupted = 1405,
            Failed_GameobjectLoad = 1501,
            Failed_GameobjectFind = 1502,
            Failed_FacebookLogin = 1503,
            Failed_GoogleLogin = 1504,
            Failed_FacebookLinkWithInternal = 1505,
            Failed_GoogleLinkWithInternal = 1506,
            Failed_InternalLogin = 1507,
            TooMany_UnboughtAssetsInScene = 1601,
            Already_ProductOwned = 1701,
            Already_ChallengeCompleted = 1702
        }
    }
}