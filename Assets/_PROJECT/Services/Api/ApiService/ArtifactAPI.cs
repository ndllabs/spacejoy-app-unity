﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;

namespace ApiService
{
    public class ArtifactAPI
    {
        private WebCore web;

        public ArtifactAPI(WebCore web)
        {
            this.web = web;
        }


        #region Database Update Only, Donot Modify
        /// <summary>
        /// Get All Categories, Subcategories and verticals From the server
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<asset_category>> GetAllCategories(int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/asset_categorys?skip=" + skip + "&limit=" + limit);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<asset_categorysList>>(res.data).data.asset_categorys;
        }

        /// <summary>
        /// Get All Retailers From the server, Includes both paint and asset retailers
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<IRetailer>> GetAllRetailers(int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/retailers?skip=" + skip + "&limit=" + limit);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRetailerList>>(res.data).data.retailers;
        }

        # region Design Challenge Endpoints

        /// <summary>
        /// Search Design Challenges given the keyword
        /// </summary>
        /// <param name="search keyword = field:value"></param>
        /// <returns></returns>
        public async Task<IDesignChallengeList> SearchDesignChallenges(string keyword, int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/challenges/search?keyword=" + keyword + "&skip=" + skip + "&limit=" + limit);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignChallengeList>>(res.data).data;
        }

        /// <summary>
        /// Get All Design Challenges
        /// </summary>
        /// <returns></returns>
        public async Task<IDesignChallengeList> GetAllDesignChallenges(int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/challenges?skip=" + skip + "&limit=" + limit);
            // Debug.Log("http res: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignChallengeList>>(res.data).data;
        }


        /// <summary>
        /// Create new Design Challenge
        /// </summary>
        /// <returns></returns>
        public async Task<IDesignChallenge> CreateDesignChallenge(IDesignChallenge data)
        {
            string body = "{\"challenge\":" + JsonUtility.ToJson(data) + "}";
            var res = await web.Post("/api/challenge", body);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignChallenge>>(res.data).data;
        }

        /// <summary>
        /// Edit Design Challenge
        /// </summary>
        /// <param name="search keyword = field:value"></param>
        /// <returns></returns>
        public async Task<IDesignChallenge> EditDesignChallenge(IDesignChallenge data)
        {
            string body = "{\"challenge\":" + JsonUtility.ToJson(data) + "}";
            var res = await web.Put("/api/challenge/" + data._id, body);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignChallenge>>(res.data).data;
        }

        /// <summary>
        /// Get Design Challenge, given the ID
        /// </summary>
        /// <param name="challengeId"></param>
        /// <returns></returns>
        public async Task<IDesignChallenge> GetDesignChallenge(string challengeId)
        {
            var res = await web.Get("/api/challenge/" + challengeId);

            if (Debug.isDebugBuild)
            {
                Debug.Log("/api/challenge/" + challengeId);
                Debug.Log("Response: " + res.data);
            }
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignChallenge>>(res.data).data;
        }

        /// <summary>
        /// Get Design Challenge, given the ID
        /// </summary>
        /// <param name="challengeId"></param>
        /// <returns></returns>
        public async Task<string> DeleteDesignChallenge(string challengeId)
        {
            var res = await web.Delete("/api/challenge/" + challengeId);
            Debug.Log("Response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<string>>(res.data).data;
        }

        # endregion

        # region Challenge Roomversion Endpoints

        /// <summary>
        /// Post Design Challenge Roomversion of the User
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<IRoomDesign> AddUserChallengeRoomversion(string projectId, IRoomDesign data)
        {
            data._id = "delete";
            string body = "{\"roomversion\":" + JsonUtility.ToJson(data) + "}";
            
            // remove roomversion Id
            string removeString = "\"_id\":\"delete\",";
            int index = body.IndexOf(removeString);
            body = (index < 0) ? body : body.Remove(index, removeString.Length);
            Debug.Log(body);
            
            var res = await web.Post("/api/project/" + projectId + "/challengeversion", body);
            Debug.Log("Response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        /// <summary>
        /// Edit Design Challenge Roomversion of the User
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="challengeId"></param>
        /// <returns></returns>
        public async Task<IRoomDesign> EditUserChallengeRoomversion(string projectId, string challengeId, IRoomDesign data)
        {
            string body = "{\"roomversion\":" + JsonUtility.ToJson(data) + "}";
            var res = await web.Put("/api/project/" + projectId + "/challengeversion/" + challengeId);
            Debug.Log("Response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        /// <summary>
        /// Get Design Challenge Roomversion of the User
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="challengeId"></param>
        /// <returns></returns>
        public async Task<IRoomDesign> GetUserChallengeRoomversion(string projectId, string challengeId)
        {
            var res = await web.Get("/api/project/" + projectId + "/challengeversion/" + challengeId);
            Debug.Log("Response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        /// <summary>
        /// Get All Design Challenge Roomversions for the challenge ID
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="challengeId"></param>
        /// <returns></returns>
        public async Task<List<IRoomDesign>> GetChallengeRoomversions(string projectId, string challengeId)
        {
            var res = await web.Get("/api/project/" + projectId + "/challengeversions/" + challengeId);
            Debug.Log("Response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesignList>>(res.data).data.roomversions;
        }

        /// <summary>
        /// Delete Design Challenge Roomversion of the User
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="challengeId"></param>
        /// <returns></returns>
        public async Task<string> DeleteUserChallengeRoomversion(string projectId, string challengeId)
        {
            var res = await web.Delete("/api/project/" + projectId + "/challengeversion/" + challengeId);
            Debug.Log("Response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<string>>(res.data).data;
        }

        # endregion

        # region User Own Room Endpoints

        /*
        Note: User Own Room POST/PUT/DELETE/GET operations will follow same old endpoints for roomversion 
         */

        /// <summary>
        /// Get User's rooms, given user ID, Project ID
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<IRoomDesign> GetUserOwnRoomversion(string orgId, string projectId, string versionId)
        {
            var res = await web.Get("/api/organization/"+ orgId + "/project/" + projectId + "/mydesign/" + versionId);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        # endregion


        # region User Endpoints

        /// <summary>
        /// Get User Game Data
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ISaveGame> GetUserSavedGame(string userId)
        {
            var res = await web.Get("/api/usergamedata/" + userId);
            Debug.Log(res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<ISaveGame>>(res.data).data;
        }

        /// <summary>
       /// Edit User Game Data
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<bool> SetUserSavedGame(string userId, ISaveGame saveGame)
        {
            string body = JsonUtility.ToJson(saveGame);
            body = "{\"user\":" + body + "}";

            Debug.Log(body);

            var res = await web.Put("/api/user/" + userId, body);


            Debug.Log("res got, when trying ot save the game data "+ res.data);
            return res.err ? false : true;
        }

        /// <summary>
        /// Edit User Game Data
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IRoomDesign> SetUserRoomSave(string userId, IRoomDesign saveGame)
        {
            var res = await web.Put("/api/user/" + userId);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
            // return new IRoomDesign();
        }

        #endregion

        #region New Routes
        public async Task<IAsset> GetAssetData(string assetId)
        {
            var res = await web.Get("/api/asset/" + assetId);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IAsset>>(res.data).data;
        }


        public async Task<IWallfinish> GetWallfinishData(string wallfinishId)
        {
            var res = await web.Get("/api/wallfinish/" + wallfinishId);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IWallfinish>>(res.data).data;
        }

        public async Task<Texture> GetChallengeCharacter(string challengeId)
        {
            Debug.Log(challengeId);
            var res = await web.GetTexture("/challenges/" + challengeId+ "/" +challengeId + "_c.jpg");
            return res;

        }

        public async Task<List<IDemoDesign>> GetDemoDesigns(int skip = 0, int limit = 100)
        {
            var res = await web.Get("api/demoversions?skip=" + skip + "&limit=" + limit);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDemoDesignsRes>>(res.data).data.roomversions;
        }

        public async Task<List<IDesignimage>> GetDesignRenders(string projectID, string roomID, string versionID)
        {
            var res = await web.Get("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + projectID + "/room/" + roomID + "/version/" + versionID + "/demorenders");
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignimagesRes>>(res.data).data.designimages;
        }

        public async Task<List<IDesignimage>> UploadCoverImage(IRoomDesign bodyData, string ImagePath)
        {
            var res = await web.PostImage("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + bodyData.room + "/version/" + bodyData._id + "/image", ImagePath);
            Debug.Log(res.err);
            Debug.Log(res.data);
            Debug.Log(res.status);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IDesignimagesRes>>(res.data).data.designimages;
        }

        #endregion

        #region TempCalls
        
        public async Task<List<INewCusto>> GetAllCustomerDesigns(int skip = 0, int limit = 100)//(string projectId, int skip = 0, int limit = 100, Action<bool, JSONNode> Callback = null)
        {
            //AppComponent.Instance.account.CRED_JWTTOKEN = "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWY5NjE0M2ZhZjM4YTJmZjZhMDlmOGIiLCJuYW1lIjoiUHJhandhbCBTaGV0dHkiLCJlbWFpbCI6InByYWp3YWxzaGV0dHkyMDE4QGdtYWlsLmNvbSIsInJvbGUiOiJNZW1iZXIiLCJpYXQiOjE1NTYwMDI5MzUsImV4cCI6MTU1NjM2MjkzNX0.7GCJYAmir2TRk9RvMS_5vFFQ4pwkwu5NEjm2yoWL9Vk";
            var res = await web.Get("api/organization/5b068f62714382439d32fa04/project/5cb5b29a4c6813333fef7610/alluserversions?limit=all");
            Debug.Log("res: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IAllCusto>>(res.data).data.roomversions;//.versions;
        }


        [Serializable]
        public class IAllCusto
        {
            public List<INewCusto> roomversions;
        }

        [Serializable]
        public class INewCusto
        {
            public string name;
            public string _id;
            public string room;
        }


        #endregion

        public async Task<List<IRoomVersions>> GetProjectRoomsAndVersions(int skip = 0, int limit = 100)//(string projectId, int skip = 0, int limit = 100, Action<bool, JSONNode> Callback = null)
        {
            var res = await web.Get("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/rooms/versions?skip=" + skip + "&limit=" + limit);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomVersionsRes>>(res.data).data.projectrooms;//.versions;
        }

        public async Task<List<ICenter>> GetCenterList(int limit = 100)
        {
            var res = await web.Get("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/rooms?limit=" + limit);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<ICenterRes>>(res.data).data.projectrooms;
        }

        public async Task<List<IProjects>> GetMyProjects(int skip = 0, int limit = 1000)//(string projectId, int skip = 0, int limit = 100, Action<bool, JSONNode> Callback = null)
        {
            string url = "/api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/myprojects?skip=" + skip + "&limit=" + limit;
            var res = await web.Get(url);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IProjectsRes>>(res.data).data.projects;//.versions;
        }

        public async Task<List<ISegment>> GetSegmentList(string centerId)
        {
            var res = await web.Get("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + centerId + "/versions");
            return res.err ? null : JsonUtility.FromJson<ApiResponse<ISegmentRes>>(res.data).data.roomversions;
        }

        public async Task<IArtifactFile> DownloadArtifact(string artifactId, ArtifactType ftype)
        {
            IArtifactFile art = new IArtifactFile { _id = artifactId, artifactType = ftype };
            return await DownloadArtifact(art);
        }

        public async Task<IArtifactFile> DownloadArtifact(IArtifactFile artFile)
        {
            string url = artFile.artifactType == ArtifactType.room
                       ? "org/" + AppConfig.PilotDemoConfig.OrgId + "/rooms/" + artFile._id + "/" + artFile._id + ".zip"
                       : "assets/" + artFile._id + "/" + artFile._id + ".zip";

            artFile.downloadLocation = await web.GetFile(url);
            return artFile;
        }

        public async Task<bool> DownloadArtifactBatch(string[] artifactIdList)
        {
            await Task.Delay(2);
            return true;
        }

        #region wallpaper
        public async Task<List<IWallfinish>> GetAssetWallpaperList(string retailerID = AppConfig.PilotDemoConfig.RetailerWallpapers,int skip = 0, int limit  = 1000)
        {
            string url = "api/wallfinishs/search?keyword=designedBy.organizationInternalLink:" +retailerID + ",wftype:wallpaper&skip="+skip+"&limit="+limit;
            var res = await web.Get(url);
            if (res.err)
                return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IWallfinish>>>(res.data).data;

            data.ForEach((IWallfinish elem) =>
            {
                elem.textureUrl = web.GetApiUrl("/img/wallfinishimage/" + elem._id + "/" + elem._id + ".jpg");
            });

            return data;
        }


        public async Task<List<IWallfinish>> GetAssetWallpaperListWithTex_Deprycated(List<IWallfinish> list)
        {
            Debug.LogWarning("[Deprycation Warning] GetAssetWallpaperListWithTex_Deprycated use GetWallpaperTexture(wallfinishID) instead");
            
            List<Task<IWallfinish>> hitUrlList = new List<Task<IWallfinish>>();
            list.ForEach((IWallfinish elem) =>
            {
                hitUrlList.Add(GetWallpaperTexture_Deprycated(elem));
            });

            IWallfinish[] dataList = await Task.WhenAll(hitUrlList);

            return new List<IWallfinish>(dataList);

        }
        
        public async Task<IWallfinish> GetWallpaperTexture_Deprycated(IWallfinish element)
        {
            Debug.LogWarning("[Deprycation Warning] GetWallpaperTexture_Deprycated use GetWallpaperTexture(wallfinishID) instead");

            element.texture = await web.GetTexture(element.imgurl);
            return element;
        }

        public async Task<Texture> GetWallpaperTexture(string wallfinishID)
        {
            return await web.GetTexture("img/wallfinishimage/" + wallfinishID + "/" + wallfinishID + ".jpg");

        }

#endregion

        //Get Paint to apply to wall
        public async Task<IWallfinish> GetWallPaintCode(IPaintStoreElement_Deprycated element)
        {

            var res = await web.Get("api/wallfinish/" + element._id);
            var data = JsonUtility.FromJson<ApiResponse<IWallfinish>>(res.data).data;
            return data;
        }


        ////TEMP
        //public void /*Task<List<ITransform>>*/ GetViewPointsList()
        //{
        //    Debug.Log("Under Development");

        //}

        public async Task<Texture> GetAssetImage(string assetId)
        {
            string url = "org/" + AppConfig.PilotDemoConfig.OrgId + "/rooms/" + assetId + "/" + assetId + ".jpg";

            Texture res = await web.GetTexture(url);
            return res;
        }
        
        public async Task<Texture> GetVersionCoverImage(string roomId, string versionId)
        {
            string url = "/roomversions/" + roomId + "/" + versionId + "/" + versionId + "_c.jpg";

            Texture res = await web.GetTexture(url);
            return res;
        }

        public async Task<Texture> GetMinimapImage(string center)
        {
            string url = "org/" + AppConfig.PilotDemoConfig.OrgId + "/rooms/" + center + "/" + center + ".jpg";

            Texture res = await web.GetTexture(url);
            return res;
        }

        public async Task<List<IAsset>> GetAssetStoreList()
        {
            string url = "api/assets/search?keyword=category:" + AppConfig.PilotDemoConfig.AssetCategory + ",subcategory:" + AppConfig.PilotDemoConfig.AssetSubCategory + ",vertical:" + AppConfig.PilotDemoConfig.AssetVertical + "&limit=3000";
            var res = await web.Get(url);
            if (res.err)
                return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IAsset>>>(res.data).data;

            data.ForEach(elem =>
            {
                elem.textureUrl = web.GetApiUrl("/assets/" + elem._id + "/" + elem._id + ".jpg");
                //elem.texture = await web.GetTexture(elem.textureUrl);

            });

            return data;
        }



        #region Temprorary pls replace me
        //SOS UNOPTIMIZED TEMP FUNCTION TO GET ASSETS
        public async Task<List<IAsset>> GetAssetsBySubcategory(string subCat, int skip = 0, int limit = 100, string key= "")
        {
            string url;
            if (key==string.Empty)
            url = "api/assets/search?keyword=subcategory:" + subCat + "&skip=" + skip + "&limit=" + limit;

            else
            url = "api/assets/search?keyword=subcategory:" + subCat + ",currency:"+key+"&skip=" + skip + "&limit=" + limit;
                var res = await web.Get(url);
            if (res.err)
                return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IAsset>>>(res.data).data;

            //data.ForEach(async elem =>
            //{
            //    elem.textureUrl = "/assets/" + elem._id + "/" + elem._id + ".jpg";

            //    elem.texture = await web.GetTexture(elem.textureUrl);
            //});
            foreach (var elem in data)
            {
                elem.textureUrl = "/assets/" + elem._id + "/" + elem._id + ".jpg";

                //elem.texture = await web.GetTexture(elem.textureUrl);
            }

            return data;
        }
        #endregion


        public async Task<List<IRoomDesign>> GetRoomDesignListAsync(string centerId)
        {
            // Debug.Log("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + centerId + "/versions");
            var res = await web.Get("api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + centerId + "/versions");
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesignList>>(res.data).data.roomversions;
        }

        public async Task<IRoomDesign> GetRoomDesignAsync(string centerId, string designId)
        {
            string url = "api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + centerId + "/version/" + designId;
            Debug.Log("Getting Design : " + url);
            var res = await web.Get(url);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        public async Task<IRoomDesign> UpdateRoomDesignAsync(IRoomDesign bodyData)
        {
            IRoomDesignPost data = new IRoomDesignPost() { roomversion = bodyData };

            string url;
            url = "api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + bodyData.room + "/version/" + bodyData._id;
            Debug.Log  ("hitting :"+url);
            Debug.Log(JsonUtility.ToJson(data));
            var res = await web.Put(url, JsonUtility.ToJson(data));
            Debug.Log("response: " + res.data);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        public async Task<IRoomDesign> CreateNewRoomVersion(IRoomDesign bodyData)
        {
            bodyData.name = "Untitled";
            bodyData._id = "delete";
            IRoomDesignPost data = new IRoomDesignPost() { roomversion = bodyData };
            string url;
            url = "api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + AppConfig.PilotDemoConfig.ProjectId + "/room/" + bodyData.room + "/version";
            string body = JsonUtility.ToJson(data);
            
            // here we delete the id field from roomversion json
            string removeString = "\"_id\":\"delete\",";
            Debug.Log("removeString: " + removeString);
            int index = body.IndexOf(removeString); 
            body = (index < 0) ? body : body.Remove(index, removeString.Length);
            
            Debug.Log  ("hitting :"+url);
            Debug.Log("Body: "+body);
            var res = await web.Post(url , body);
            return res.err ? null : JsonUtility.FromJson<ApiResponse<IRoomDesign>>(res.data).data;
        }

        /// <summary>
        /// Delete Roomversion
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="roomId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public async Task<string> DeleteRoomVersion(string projectId, string roomId, string versionId)
        {
            string url = "api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/project/" + projectId + "/room/" + roomId + "/version/" + versionId;
            var res = await web.Delete(url);
            return res.err ? null:JsonUtility.FromJson<ApiResponse<string>>(res.data).data;
        }

        #region Paints

        #region Paint Retailers
        //Paint Retailers can be fetched by AppDatabase's GetRetailersOfType() function
        #endregion

        #region PaintGroups

        public async Task<List<IWallfinishGroup>> GetPaintGroups(string id)
        {
            var res = await web.Get("api/wallfinishs/groups/" + id);
            if (res.err) return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IWallfinishGroup>>>(res.data).data;
            return data;
        }

        #endregion

        #region Paint from PaintGroup
        
        public async Task<List<IWallfinish>> GetPaintsOfGroup(string groupId, int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/wallfinishs/search?skip=" + skip + "&limit=" + limit + "&keyword=group:" + groupId);
            if (res.err) return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IWallfinish>>>(res.data).data;
            return data;
        }

        public async Task<int> GetPaintsOfGroupCount(string groupId)
        {
            var res = await web.Get("/api/wallfinishs/search?count=true&keyword=group:" + groupId);
            if (res.err) return 0;
            var data = JsonUtility.FromJson<ApiResponse<int>>(res.data).data;
            return data;
        }

        #endregion

        #region Paint from PaintRetailer

        public async Task<List<IWallfinish>> GetPaintsOfRetailer(string retailerId, int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/wallfinishs/search?skip=" + skip + "&limit=" + limit + "&keyword=designedBy.organizationInternalLink:" + retailerId);
            if (res.err) return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IWallfinish>>>(res.data).data;
            return data;
        }

        public async Task<int> GetPaintsOfRetailerCount(string retailerId)
        {
            var res = await web.Get("/api/wallfinishs/search?count=true&keyword=designedBy.organizationInternalLink:" + retailerId);
            if (res.err) return 0;
            var data = JsonUtility.FromJson<ApiResponse<int>>(res.data).data;
            return data;
        }

        #region Paint Retailer Search

        public async Task<List<IWallfinish>> GetPaintsOfRetailer(string retailerId, string searchKey, int skip = 0, int limit = 1000)
        {
            var res = await web.Get("/api/wallfinishs/search?skip=" + skip + "&limit=" + limit + "&keyword=designedBy.organizationInternalLink:" + retailerId + ",name:" + searchKey);
            if (res.err) return null;
            var data = JsonUtility.FromJson<ApiResponse<List<IWallfinish>>>(res.data).data;
            return data;
        }

        public async Task<int> GetPaintsOfRetailerCount(string retailerId, string searchKey)
        {
            var res = await web.Get("/api/wallfinishs/search?count=true&keyword=designedBy.organizationInternalLink:" + retailerId + ",name:" + searchKey);
            if (res.err) return 0;
            var data = JsonUtility.FromJson<ApiResponse<int>>(res.data).data;
            return data;
        }

        #endregion

        #endregion

        #endregion
        
        #endregion
        
    }
}