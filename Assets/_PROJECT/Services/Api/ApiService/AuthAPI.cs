﻿using System;
using UnityEngine;
using System.Threading.Tasks;

namespace ApiService
{
    public class Auth
    {
        private WebCore web;
        const string ENDPOINT = "api/auth/";

        public Auth(WebCore web)
        {
            this.web = web;
        }


        public async Task<ApiStatus> CheckServer()
        {
            var res = await web.Get("");
            return res.err ? null : JsonUtility.FromJson<ApiStatus>(res.data);
        }

        public async Task<bool> CheckAuth()
        {
            var res = await web.Get(ENDPOINT + "check");
            return !res.err;
        }


        public async Task<IUserAuthResponse> Login(IUserAuth auth)
        {
            var res = await web.Post(ENDPOINT + "login", JsonUtility.ToJson(auth));
            return res.err ? null : JsonUtility.FromJson<IUserAuthResponse>(res.data);
        }

        public async Task<IUserAuthResponse> TokenLogin(IUserTokenAuth auth)
        {
            var res = await web.Post(ENDPOINT + "connectoauth", JsonUtility.ToJson(auth));
            return res.err ? null : JsonUtility.FromJson<IUserAuthResponse>(res.data);
        }

        public async Task<IUserAuthResponse> Logout()
        {
            var res = await web.Post(ENDPOINT + "logout", "{}");
            return res.err ? null : JsonUtility.FromJson<IUserAuthResponse>(res.data);
        }

        public async Task<IUserAuthResponse> Signup(IUserRegister auth)
        {
            var res = await web.Post(ENDPOINT + "registeruser", JsonUtility.ToJson(auth));
            return res.err ? null : JsonUtility.FromJson<IUserAuthResponse>(res.data);
        }

        public async Task<string> AddCustomer(ISubscriber user)
        {
            var body = "{\"subscriber\": " + JsonUtility.ToJson(user) + "}";
            Debug.Log("BODY: " + body);
            string url = "api/organization/" + AppConfig.PilotDemoConfig.OrgId + "/subscriber/customers/createcustomer";
            var res = await web.Post(url, body);
            return res.err ? null : res.data;
        }
    }
}
