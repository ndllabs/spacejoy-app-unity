﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ApiItem
{

    /*
    Web web;
    const string ENDPOINT = "api/item";


    public void Init(Web web)
    {
        this.web = web;
    }

    public async Task<List<Item>> GetList(ListOptions options = null)
    {
        ListOptions ops = options ?? new ListOptions();
        string url = ENDPOINT + "s?skip=" + ops.skip + "&limit=" + ops.limit;

        var res = await web.Get(url);
        return res.err ? null : res.data["data"]["data"].AsArray.DeserializeList<Item>();
    }

    public async Task<Item> Get(string id)
    {
        var res = await web.Get(ENDPOINT + "/" + id);
        return res.err ? null : res.data["data"].Deserialize<Item>();
    }

    public async Task<Item> Create(Item data)
    {
        var res = await web.Post(ENDPOINT, data.Serialize());
        return res.err ? null : res.data["data"].Deserialize<Item>();
    }

    public async Task<Item> Edit(Item data)
    {
        Debug.Log("ED: " + data.Serialize());
        var res = await web.Put(ENDPOINT + "/" + data._id, data.Serialize());
        return res.err ? null : data;
    }


    public async Task<bool> Delete(Item data)
    {
        return await Delete(data._id);
    }

    public async Task<bool> Delete(string id)
    {
        var res = await web.Delete(ENDPOINT + "/" + id);
        Debug.Log("res: " + res.data);
        return !res.err;
    }

    public async Task<List<Item>> Search(ListOptions options = null)
    {
        ListOptions ops = options ?? new ListOptions();
        string url = ENDPOINT + "s?skip=" + ops.skip + "&limit=" + ops.limit;

        var res = await web.Get(ENDPOINT + "s/search?keyword=");
        return res.err ? null : res.data["data"]["data"].AsArray.DeserializeList<Item>();
    }


    public async Task<ItemFileUpload> UploadFile(Item data, string filePath)
    {
        return await UploadFile(data._id, filePath);
    }

    public async Task<ItemFileUpload> UploadFile(string id, string filePath)
    {
        var res = await web.PostFile(ENDPOINT + "/" + id + "/upload", filePath, "file");
        return res.err ? null : res.data["data"].Deserialize<ItemFileUpload>();
    }



*/
}
