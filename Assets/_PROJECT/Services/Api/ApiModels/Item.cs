﻿using System;
using SimpleJSON;
using UnityEngine;

[Serializable]
public class Item
{
    public string _id;

    public string name;
    public float price;
    public string category;

    public string createdAt;
    public string updatedAt;

    public Item()
    {
    }

    public Item(JSONNode data)
    {
        this._id = data["_id"];

        this.name = data["name"];
        this.price = data["price"];
        this.category = data["category"];

        this.createdAt = data["createdAt"];
        this.updatedAt = data["updatedAt"];
    }

    public string ToJSON()
    {
        //return JsonUtility.ToJson(this);
        JSONNode data = JSON.Parse("{}");

        data["name"] = this.name;
        data["price"] = this.price;
        data["category"] = this.category;

        return data.ToString();
    }

}

[Serializable]
public class ItemFileUpload
{
    public string file;

    public ItemFileUpload(JSONNode data)
    {
        this.file = data["file"];
    }

}

