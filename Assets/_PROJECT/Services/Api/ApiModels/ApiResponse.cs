﻿using System;
using System.Collections.Generic;

[Serializable]
public class ApiResponse<T>
{
    public string status;
    public T data;
    public string message;
}

[Serializable]
public class ApiListResponse<T>
{
    public List<T> data;
    public int count;
}

[Serializable]
public class ApiStatus
{
    public bool err;
    public string data;
    public string version;
}