﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class HttpRes
{

    public bool err;
    public string data;
    public byte[] blobData;
    public string msg;
    public int status;


    public HttpRes(bool err, string data, byte[] blobData = null, string msg = "", int status = 200)
    {
        this.err = err;
        this.data = FixJsonData(data);
        this.blobData = blobData;
        this.msg = msg;
        this.status = status;
    }

    public string FixJsonData(string data)
    {
        return string.IsNullOrEmpty(data) ? "{}" : data[0] != '{' ? "{\"data\":\"" + data + "\"}" : data;
    }

}

public class ListOptions
{
    public int skip = 0;
    public int limit = 100;
    public int sort = 0; //-1 desc, 1 asc, 0 neutral
}
