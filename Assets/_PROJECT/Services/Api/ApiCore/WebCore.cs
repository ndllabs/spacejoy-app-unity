﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using _unity;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

public class WebCore
{
    #region Fields

    public string Host
    {
        get; private set;
    }

    public string jwtToken;

    public int basicTimeout = 1;
    public int extendedTimeout;

    #endregion


    public WebCore(string ApiHost, int basicTimeout = 60, int extendedTimeout = 1800)
    {

        if (string.IsNullOrEmpty(ApiHost))
        {
            _.l("Server Core Initialization FAILED! Error: Host Empty.");
            throw new Exception("Server Needs an API Host. Blank Given!");
        }

        this.Host = ApiHost;
        jwtToken = String.Empty;

        this.basicTimeout = basicTimeout;
        this.extendedTimeout = extendedTimeout;

    }


    #region ===[ CORE ]===

    #region Basic

    public async Task<HttpRes> Get(string endpoint)
    {
        string hitUrl = this.GetApiUrl(endpoint);

        using (UnityWebRequest www = UnityWebRequest.Get(hitUrl))
        {
            if (!string.IsNullOrEmpty(jwtToken))
            {
                www.SetRequestHeader("Authorization", jwtToken);
            }
            www.timeout = basicTimeout;

            //Debug.Log("hitUrl: " + hitUrl);
            //Debug.Log("jwtToken: " + jwtToken);

            await www.SendWebRequest();


            return www.isNetworkError || www.isHttpError || www.responseCode >= 400
                ? new HttpRes(true, www.error)
                : new HttpRes(false, www.downloadHandler.text);
        }
    }

    public async Task<HttpRes> Post(string endpoint, string body = null)
    {
        string hitUrl = this.GetApiUrl(endpoint);

        using (UnityWebRequest www = new UnityWebRequest(hitUrl, "POST"))
        {
            byte[] uData = Encoding.UTF8.GetBytes(body);

            UploadHandlerRaw uH = new UploadHandlerRaw(uData);
            www.uploadHandler = uH;
            www.SetRequestHeader("Content-Type", "application/json");

            DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
            www.downloadHandler = dH;

            if (!string.IsNullOrEmpty(jwtToken))
            {
                www.SetRequestHeader("Authorization", jwtToken);
            }

            www.timeout = extendedTimeout;
            await www.SendWebRequest();

            return www.isNetworkError || www.isHttpError || www.responseCode >= 400
                ? new HttpRes(true, www.error)
                : new HttpRes(false, www.downloadHandler?.text);
        }

    }

    public async Task<HttpRes> Post_WithHost(string host, string endpoint, string body = null, string token=null)
    {
        string hitUrl = host + endpoint;

        using (UnityWebRequest www = new UnityWebRequest(hitUrl, "POST"))
        {
            byte[] uData = Encoding.UTF8.GetBytes(body);

            UploadHandlerRaw uH = new UploadHandlerRaw(uData);
            www.uploadHandler = uH;
            www.SetRequestHeader("Content-Type", "application/json");

            DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
            www.downloadHandler = dH;

            if (!string.IsNullOrEmpty(token))
            {
                www.SetRequestHeader("Authorization", token);
            }

            www.timeout = extendedTimeout;
            await www.SendWebRequest();

            return www.isNetworkError || www.isHttpError || www.responseCode >= 400
                ? new HttpRes(true, www.error)
                : new HttpRes(false, www.downloadHandler?.text);
        }

    }

    public async Task<HttpRes> Put(string endpoint, string body = null)
    {
        string hitUrl = this.GetApiUrl(endpoint);

        using (UnityWebRequest www = new UnityWebRequest(hitUrl, "PUT"))
        {
            byte[] uData = Encoding.UTF8.GetBytes(body);

            UploadHandlerRaw uH = new UploadHandlerRaw(uData);
            www.uploadHandler = uH;
            www.SetRequestHeader("Content-Type", "application/json");

            DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
            www.downloadHandler = dH;


            www.SetRequestHeader("X-HTTP-Method-Override", "PUT");
            if (!string.IsNullOrEmpty(jwtToken))
            {
                www.SetRequestHeader("Authorization", jwtToken);
            }

            www.timeout = extendedTimeout;
            await www.SendWebRequest();

            
            return www.isNetworkError || www.isHttpError || www.responseCode >= 400
                ? new HttpRes(true, www.error)
                : new HttpRes(false, www.downloadHandler?.text);
        }
    }


    public async Task<HttpRes> Delete(string endpoint)
    {
        string hitUrl = this.GetApiUrl(endpoint);

        using (UnityWebRequest www = UnityWebRequest.Delete(hitUrl))
        {
            if (!string.IsNullOrEmpty(jwtToken))
            {
                www.SetRequestHeader("Authorization", jwtToken);
            }

            www.timeout = extendedTimeout;
            await www.SendWebRequest();

            return www.isNetworkError || www.isHttpError || www.responseCode >= 400
                ? new HttpRes(true, www.error)
                : new HttpRes(false, www.downloadHandler?.text);
        }
    }

    #endregion


    #region Download File
    public async Task<Texture> GetTexture(string endpoint)
    {
        string hitUrl = this.GetApiUrl(endpoint);
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(hitUrl);
        if (!string.IsNullOrEmpty(jwtToken))
        {
            www.SetRequestHeader("Authorization", jwtToken);
        }

        www.timeout = extendedTimeout;
        await www.SendWebRequest();

        //Debug.Log(((DownloadHandlerTexture)www.downloadHandler).texture);

        return (www.isNetworkError || www.isHttpError) ? null : DownloadHandlerTexture.GetContent(www);
    }


    public IEnumerator GetTextureAlt(string endpoint, Action<Texture> Callback)
    {
        string hitUrl = this.GetApiUrl(endpoint);

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(hitUrl);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Callback(null);
        }
        else
        {
            Callback(((DownloadHandlerTexture)www.downloadHandler).texture);
        }
    }


    public async Task<string> GetFile(string endpoint)
    {
        string hitUrl = this.GetApiUrl(endpoint);
        //Debug.Log("hitUrl : " + hitUrl);
        UnityWebRequest www = UnityWebRequest.Get(hitUrl);
        if (!string.IsNullOrEmpty(jwtToken))
        {
            www.SetRequestHeader("Authorization", jwtToken);
        }

        www.timeout = extendedTimeout;

        await www.SendWebRequest();

        string fName = Path.Combine(AppConfig.StorageConfig.TEMP_PATH, Path.GetFileName(endpoint));

        File.WriteAllBytes(fName, www.downloadHandler.data);



        return (www.isNetworkError || www.isHttpError) ? null : fName;
    }


    #endregion


    #region Upload File

    public async Task<HttpRes> PostFile(string endpoint, string filePath, string field = "file")
    {
        string hitUrl = this.GetApiUrl(endpoint);

        WWWForm wwwFormData = new WWWForm();

        Debug.Log("Reading File....................");
        Debug.Log(filePath);
        
        wwwFormData.AddBinaryData(field, File.ReadAllBytes(filePath));

        using (UnityWebRequest www = UnityWebRequest.Post(hitUrl, wwwFormData))
        {
            if (!string.IsNullOrEmpty(jwtToken))
            {
                www.SetRequestHeader("Authorization", jwtToken);
            }
            www.timeout = extendedTimeout;

            await www.SendWebRequest();


            return www.isNetworkError || www.isHttpError || www.responseCode >= 400
                ? new HttpRes(true, www.error, null, "", (int) www.responseCode)
                : new HttpRes(false, www.downloadHandler.text, null, "", (int) www.responseCode);
        }
    }


    public async Task<HttpRes> PostImage(string endpoint, string imagePath, string field = "image")
    {
        return await PostFile(endpoint, imagePath, field);
    }

    #endregion


    #endregion


    #region ===[ Utils ]===

    public string GetApiUrl(string data)
    {
        //Debug.Log("Hitting: " + this.Host + NormalizeEndpointRoute(data));
        return this.Host + NormalizeEndpointRoute(data);
    }

    public string NormalizeEndpointRoute(string data)
    {
        if (string.IsNullOrEmpty(data)) return "/";

        string str = data[0] != '/' ? "/" + data : data;
        str = str.Trim();

        return str;
    }


    #endregion



}
