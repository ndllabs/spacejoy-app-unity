﻿using UnityEngine;
using System.Collections;

public class WebApi
{
    public WebCore web;

    public ApiService.Auth auth;
    public ApiService.ArtifactAPI artifact;


    public WebApi(string ApiHost, int basicTimeout, int extendedTimeout)
    {
        web = new WebCore(ApiHost, basicTimeout, extendedTimeout);

        auth = new ApiService.Auth(web);
        artifact = new ApiService.ArtifactAPI(web);
    }

    public string JWT
    {
        get
        {
            return web.jwtToken;
        }
        set
        {
            web.jwtToken = value;
        }
    }
}
