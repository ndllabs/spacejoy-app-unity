﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
//using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.UI;

public class AssetVisuals_Deprycated : MonoBehaviour
{

    public bool drawCues = false;

    RaycastHit hitforward, hitback, hitleft, hitright;
    private Vector3 actualSize;
    private Vector3 actualCenter;
    private float radius;
    Material lineMaterial;
    Vector3[] linePositions = new Vector3[2];
    Gradient gradient = new Gradient();
    GradientColorKey[] colorKey = new GradientColorKey[2];
    private Material lineMaterial2;
    Color lineColor1 = new Color32();
    Color lineColor2 = new Color32();
    GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];

    private float distAsset, startWidth = 0.02f, endWidth = 0.03f;
    private GameObject canvasWorldSpace, showLines, showDimension;
    private LineRenderer leftLR, rightLR, frontLR, backLR, topLR, bottomLR;
    //private TransformGesture tg;
    private GameObject rightObject, frontObject, leftObject, backObject, topObject/*, bottomObject*/;
    private TextMeshProUGUI arText, afText, alText, abaText;


    // Start is called before the first frame update
    void Start()
    {
        //mains
        showLines = new GameObject("showLines"); showDimension = new GameObject("showDimension");


        //lines
        GameObject leftLine = new GameObject("leftLine"); GameObject rightLine = new GameObject("rightLine");
        GameObject frontLine = new GameObject("frontLine"); GameObject backLine = new GameObject("backLine");
        GameObject topLine = new GameObject("topLine"); GameObject bottomLine = new GameObject("bottomLine");

        canvasWorldSpace = GameObject.Find("Canvas_WorldSpace");

        //dimen
        //widthLine = new GameObject("widthLine"); heightLine = new GameObject("heightLine"); depthLine = new GameObject("depthLine");

        //setParents
        showLines.transform.parent = gameObject.transform; showDimension.transform.parent = gameObject.transform;
        leftLine.transform.parent = showLines.transform; rightLine.transform.parent = showLines.transform;
        frontLine.transform.parent = showLines.transform; backLine.transform.parent = showLines.transform;
        topLine.transform.parent = showLines.transform; bottomLine.transform.parent = showLines.transform;
        //widthLine.transform.parent = showDimension.transform; heightLine.transform.parent = showDimension.transform; depthLine.transform.parent = showDimension.transform;

        //lineRenderers
        leftLR = leftLine.AddComponent<LineRenderer>(); rightLR = rightLine.AddComponent<LineRenderer>(); frontLR = frontLine.AddComponent<LineRenderer>();
        backLR = backLine.AddComponent<LineRenderer>(); topLR = topLine.AddComponent<LineRenderer>(); bottomLR = bottomLine.AddComponent<LineRenderer>();
        //heighLR = heightLine.AddComponent<LineRenderer>(); widthLR = widthLine.AddComponent<LineRenderer>(); depthLR = depthLine.AddComponent<LineRenderer>();

        //setactive
        showLines.SetActive(false); showDimension.SetActive(false);

        //etc
        //if (tg == null) { tg = GetComponent<TransformGesture>(); }
        InitGradient();

        //ui that shows the dimensions
        //assetHeight = sm.assetHeight; assetWidth = sm.assetWidth; assetDepth = sm.assetDepth;
        //ahText = assetHeight.transform.Find("Text").GetComponent<TextMeshProUGUI>(); awText = assetWidth.transform.Find("Text").GetComponent<TextMeshProUGUI>();
        //adText = assetDepth.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        UpdateExternalObjectReferences();

        arText = rightObject.transform.Find("Text").GetComponent<TextMeshProUGUI>(); alText = leftObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();
        afText = frontObject.transform.Find("Text").GetComponent<TextMeshProUGUI>(); abaText = backObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        //InvokeRepeating("DrawDirectionCue", 0.1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        DrawDirectionCue();
    }

    //temp
    void UpdateExternalObjectReferences()
    {
        //ui that shows the assset to wall distance
        if (canvasWorldSpace.transform.Find("rightObject") == null)
        {
            rightObject = Instantiate(Resources.Load("DistanceDisplayObject1")) as GameObject;
            rightObject.transform.SetParent(canvasWorldSpace.transform);
            rightObject.name = "rightObject";
            rightObject.transform.localScale = Vector3.one * 0.7f;

            //ResetImagesZeeValue(rightObject);

            foreach (Transform t in rightObject.transform) { ResetImagesZeeValue(t.gameObject); }
        }
        else { rightObject = canvasWorldSpace.transform.Find("rightObject").gameObject; }

        if (canvasWorldSpace.transform.Find("backObject") == null)
        {
            backObject = Instantiate(Resources.Load("DistanceDisplayObject1")) as GameObject;
            backObject.transform.SetParent(canvasWorldSpace.transform);
            backObject.name = "backObject";
            backObject.transform.localScale = Vector3.one * 0.7f;

            //ResetImagesZeeValue(backObject);

            foreach (Transform t in backObject.transform) { ResetImagesZeeValue(t.gameObject); }
        }
        else { backObject = canvasWorldSpace.transform.Find("backObject").gameObject; }

        if (canvasWorldSpace.transform.Find("frontObject") == null)
        {
            frontObject = Instantiate(Resources.Load("DistanceDisplayObject1")) as GameObject;
            frontObject.transform.SetParent(canvasWorldSpace.transform);
            frontObject.name = "frontObject";
            frontObject.transform.localScale = Vector3.one * 0.7f;

            //ResetImagesZeeValue(frontObject);

            foreach (Transform t in frontObject.transform) { ResetImagesZeeValue(t.gameObject); }
        }
        else { frontObject = canvasWorldSpace.transform.Find("frontObject").gameObject; }

        if (canvasWorldSpace.transform.Find("leftObject") == null)
        {
            leftObject = Instantiate(Resources.Load("DistanceDisplayObject1")) as GameObject;
            leftObject.transform.SetParent(canvasWorldSpace.transform);
            leftObject.name = "leftObject";
            leftObject.transform.localScale = Vector3.one * 0.7f;

            //ResetImagesZeeValue(leftObject);

            foreach (Transform t in leftObject.transform) { ResetImagesZeeValue(t.gameObject); }
        }
        else { leftObject = canvasWorldSpace.transform.Find("leftObject").gameObject; }

        if (canvasWorldSpace.transform.Find("topObject") == null)
        {
            topObject = Instantiate(Resources.Load("DistanceDisplayObject1")) as GameObject;
            topObject.transform.SetParent(canvasWorldSpace.transform);
            topObject.name = "topObject";
            topObject.transform.localScale = Vector3.one * 0.7f;

            //ResetImagesZeeValue(topObject);

            foreach (Transform t in topObject.transform) { ResetImagesZeeValue(t.gameObject); }
        }
        else { topObject = canvasWorldSpace.transform.Find("topObject").gameObject; }
    }

    public void ResetImagesZeeValue(GameObject go)
    {
        if (go && go.GetComponent<Image>())
        {
            Image image = go.GetComponent<Image>();
            Material existingGlobalMat = image.materialForRendering;
            Material updatedMaterial = new Material(existingGlobalMat);
            updatedMaterial.SetInt("unity_GUIZTestMode", (int)UnityEngine.Rendering.CompareFunction.Always);
            image.material = updatedMaterial;
        }
        else if (go && go.GetComponent<TextMeshProUGUI>())
        {
            TextMeshProUGUI image = go.GetComponent<TextMeshProUGUI>();
            Material existingGlobalMat = image.materialForRendering;
            Material updatedMaterial = new Material(existingGlobalMat);
            updatedMaterial.SetInt("unity_GUIZTestMode", (int)UnityEngine.Rendering.CompareFunction.Always);
            image.material = updatedMaterial;
        }
    }

    public void UpdateAssetData()
    {
        //vars
        Collider[] renderes = GetComponentsInChildren<Collider>();
        Bounds combinedBounds = new Bounds();

        //bound
        if (renderes.Length > 0)
        {
            combinedBounds = renderes[0].bounds;
            for (int i = 1; i < renderes.Length; i++) { combinedBounds.Encapsulate(renderes[i].bounds); }
        }

        //assetvars
        actualSize = combinedBounds.size; actualCenter = combinedBounds.center;
        if (actualSize.x > actualSize.z) { radius = actualSize.x; }
        else { radius = actualSize.z; }

    }

    public void UpdateUiLookAtCamera()
    {
        //if (drawDimension)
        //{
        //    //drawDimen
        //    assetHeight.transform.rotation = Camera.main.transform.rotation;
        //    assetWidth.transform.rotation = Camera.main.transform.rotation;
        //    assetDepth.transform.rotation = Camera.main.transform.rotation;
        //}

        if (drawCues)
        {
            //drawcues
            leftObject.transform.rotation = Camera.main.transform.rotation;
            rightObject.transform.rotation = Camera.main.transform.rotation;
            frontObject.transform.rotation = Camera.main.transform.rotation;
            backObject.transform.rotation = Camera.main.transform.rotation;
        }

    }

    public void DrawDirectionCue()
    {
        if (drawCues)
        {
            Vector3 origin = /*new Vector3(0, 0, 0);//*/transform.position;
            Physics.Raycast(origin, Vector3.forward, out hitforward, 60);
            Physics.Raycast(origin, Vector3.back, out hitback, 60);
            Physics.Raycast(origin, Vector3.left, out hitleft, 60);
            Physics.Raycast(origin, Vector3.right, out hitright, 60);

            //debug
            Vector3 endPos = origin + (Vector3.forward * hitforward.distance);
            Debug.DrawLine(origin, endPos, Color.green, 3f);

            UpdateAssetData();
            float textOffset = 0.4f;


            if (!showLines.activeSelf)
                showLines.SetActive(true);

            // Set positions
            var pos = transform.position;
            //hitforward.point;



            //Left Line
            distAsset = pos.x - (actualSize.x * 0.5f);
            linePositions[0] = new Vector3(distAsset, (pos.y + (actualSize.y * 0.5f)), pos.z);
            linePositions[1] = new Vector3(hitleft.point.x, hitleft.point.y + (actualSize.y * 0.5f), hitleft.point.z);//new Vector3(distWall, (pos.y + (actualSize.y * 0.5f)), pos.z);
            onDrawLine(lineMaterial, linePositions, gradient, startWidth, endWidth, leftLR);
            leftObject.SetActive(true);
            leftObject.transform.position = new Vector3((distAsset - textOffset), ((pos.y + (actualSize.y * 0.5f)) + 0.15f /*+ 0.3f*/), pos.z);
            alText.text = (Mathf.Abs(Mathf.Round((distAsset - hitleft.point.x) * 3.28084f * 100.0f) / 100.0f)).ToString() + " ft";



            //Right Line
            distAsset = pos.x + (actualSize.x * 0.5f);
            linePositions[0] = new Vector3(distAsset, (pos.y + (actualSize.y * 0.5f)), pos.z);
            linePositions[1] = new Vector3(hitright.point.x, hitright.point.y + (actualSize.y * 0.5f), hitright.point.z);// new Vector3(distWall, (pos.y + (actualSize.y * 0.5f)), pos.z);
            onDrawLine(lineMaterial, linePositions, gradient, startWidth, endWidth, rightLR);
            rightObject.SetActive(true);
            rightObject.transform.position = new Vector3((distAsset + textOffset), ((pos.y + (actualSize.y * 0.5f)) + 0.15f /*+ 0.75f*/), pos.z);
            arText.text = (Mathf.Abs(Mathf.Round((distAsset - hitright.point.x) * 3.28084f * 100.0f) / 100.0f)).ToString() + " ft";



            //Back Line
            distAsset = pos.z + (actualSize.z * 0.5f);
            linePositions[0] = new Vector3(pos.x, (pos.y + (actualSize.y * 0.5f)), distAsset);
            linePositions[1] = new Vector3(hitforward.point.x, hitforward.point.y + (actualSize.y * 0.5f), hitforward.point.z);//new Vector3(pos.x, (pos.y + (actualSize.y * 0.5f)), distWall);
            onDrawLine(lineMaterial, linePositions, gradient, startWidth, endWidth, backLR);
            backObject.SetActive(true);
            backObject.transform.position = new Vector3(pos.x, ((pos.y + (actualSize.y * 0.5f)) + 0.15f /*+ 0.75f*/), (distAsset + textOffset));
            abaText.text = Math.Round(Mathf.Abs(Mathf.Round((distAsset - hitforward.point.z) * 3.28084f * 100.0f) / 100.0f), 1).ToString() + " ft";




            //Front Line
            distAsset = pos.z - (actualSize.z * 0.5f);
            linePositions[0] = new Vector3(pos.x, (pos.y + (actualSize.y * 0.5f)), distAsset);
            linePositions[1] = new Vector3(hitback.point.x, hitback.point.y + (actualSize.y * 0.5f), hitback.point.z);//new Vector3(pos.x, (pos.y + (actualSize.y * 0.5f)), distWall);
            onDrawLine(lineMaterial, linePositions, gradient, startWidth, endWidth, frontLR);
            frontObject.SetActive(true);
            frontObject.transform.position = new Vector3(pos.x, ((pos.y + (actualSize.y * 0.5f)) + 0.15f /*+ 0.3f*/), (distAsset - textOffset));
            afText.text = Math.Round(Mathf.Abs(Mathf.Round((distAsset - hitback.point.z) * 3.28084f * 100.0f) / 100.0f)).ToString() + " ft";


            //Top Line
            topObject.SetActive(false);
            topLR.gameObject.SetActive(false);

            //////Bottom Line
            //if (bottomObject != null)
            //    bottomObject.SetActive(false);

            if (bottomLR.gameObject != null)
                bottomLR.gameObject.SetActive(false);

            UpdateUiLookAtCamera();
        }
        else
        {
            //if (showLines && leftObject && rightObject && frontObject && backObject && topObject)
            //{
            if (showLines.activeSelf)
                showLines.SetActive(false);
            if (leftObject.activeSelf)
            {
                leftObject.SetActive(false);
                rightObject.SetActive(false);
                frontObject.SetActive(false);
                backObject.SetActive(false);
                topObject.SetActive(false);

            }

            //if (bottomObject != null)
            //    bottomObject.SetActive(false);

            //}
        }

    }


    void onDrawLine(Material Material, Vector3[] Positions, Gradient Gradient, float StartWidth, float EndWidth, LineRenderer lr)
    {
        lr.material = Material;
        lr.positionCount = Positions.Length;
        lr.startWidth = StartWidth;
        lr.endWidth = EndWidth;
        lr.SetPositions(Positions);
        lr.colorGradient = Gradient;
    }

    void InitGradient()
    {
        lineMaterial = new Material(Shader.Find("Sprites/Default"));
        lineColor1 = HexToColor("#1CB5E0");
        lineColor2 = HexToColor("#000046");
        colorKey[0].color = lineColor1;
        colorKey[0].time = 0.0f;
        colorKey[1].color = lineColor2;
        colorKey[1].time = 1.0f;
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;
        gradient.SetKeys(colorKey, alphaKey);

        InitGradient2();
    }


    void InitGradient2() //for Dimension Line
    {
        //    lineMaterial2 = new Material(Shader.Find("Sprites/Default"));
        //    lineColor1 = HexToColor("#A90000");
        //    lineColor2 = HexToColor("#FF0000");
        //    colorKey[0].color = lineColor1;
        //    colorKey[0].time = 0.0f;
        //    colorKey[1].color = lineColor2;
        //    colorKey[1].time = 1.0f;
        //    alphaKey[0].alpha = 1.0f;
        //    alphaKey[0].time = 0.0f;
        //    alphaKey[1].alpha = 1.0f;
        //    alphaKey[1].time = 1.0f;
        //    gradient2.SetKeys(colorKey, alphaKey);
    }



    public static Color HexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }
}
