﻿using UnityEngine;
using _unity.Events;
using _unity;
using TouchMan;

using Homefuly;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AssetVisuals_Deprycated))]
[RequireComponent(typeof(TouchManSkin))]
public class AssetTransformManager : MonoBehaviour
{
    private AssetVisuals_Deprycated visuals;
    private TouchManSkin touchmanskin;
    public AssetTransformMode assetTransformMode;
    bool isSelected = false;
    Rigidbody rigidBody;

    public IAssetEntry entry;
    public bool isExperimental = true;

    bool canMove;

    bool canObjectEverMove = true;

    public void Init(bool canObjectEverMove)
    {
        if (touchmanskin)
        {
            switch (entry.asset.mountType.ToEnum<MountType>())
            {
                case MountType.floor:
                    touchmanskin.Init(MoveMode.GroundAttached, entry.asset.clampVal);
                    break;
                case MountType.ceiling:

                    //Disable Shadows For Ceiling asset
                    foreach (Transform trans in transform)
                    {
                        if (trans.GetComponent<MeshRenderer>())
                            trans.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    }

                    touchmanskin.Init(MoveMode.CeilingAttached, entry.asset.clampVal);
                    break;
                case MountType.wall:
                    touchmanskin.Init(MoveMode.WallAttached, entry.asset.clampVal);
                    break;
                case MountType.attached:
                    touchmanskin.Init(MoveMode.AssetAttached, entry.asset.clampVal);
                    break;
                case MountType.free:
                    touchmanskin.Init(MoveMode.Free, entry.asset.clampVal);
                    break;
            }

            this.canObjectEverMove = canObjectEverMove;
            if (!canObjectEverMove)
            {
                touchmanskin.enabled = false;
            }
        }
        else
            Debug.LogError("No Carreir Exist");
    }

    private void Awake()
    {
        rigidBody = transform.GetOrAddComponent<Rigidbody>();
        visuals = transform.GetOrAddComponent<AssetVisuals_Deprycated>();
        touchmanskin = transform.GetOrAddComponent<TouchManSkin>();

        touchmanskin.enabled = false;
        visuals.enabled = false;

        assetTransformMode = AssetTransformMode.idle;
        entry = new IAssetEntry();
        Setup();
    }

    private void FixedUpdate()
    {
        if (HomefulyExtensions.PointIsOverUI())
        {
            // GUI Action
            return;
        }
        else
        {
            DoMoveOnDrag();
        }

    }

    void OnEnable()
    {
        touchmanskin.OnMovementUpdate += OnMovementUpdate;
        TouchManEvents.OnBeginDrag += BeginDrag;
        TouchManEvents.OnEndDrag += EndDrag;
        //TouchManEvents.OnOneFingerStationary += OneFingerStationary;
    }

    private void OnMovementUpdate(ObjectMovementUpdate movementType)
    {
        //throw new NotImplementedException();
    }

    void OnDisable()
    {
        touchmanskin.OnMovementUpdate -= OnMovementUpdate;
        TouchManEvents.OnBeginDrag -= BeginDrag;
        TouchManEvents.OnEndDrag -= EndDrag;
        //TouchManEvents.OnOneFingerStationary -= OneFingerStationary;
    }

    #region unity Collision
    /*private void OnCollisionEnter(Collision collision){ }
    private void OnCollisionExit(Collision collision){ }*/
    #endregion


    void Setup()
    {
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;//| RigidbodyConstraints.FreezePositionY;
        rigidBody.interpolation = RigidbodyInterpolation.Interpolate;
        rigidBody.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rigidBody.useGravity = false;

        visuals.drawCues = false;
    }


    public void UpdateIsAssetSelected(bool isSelected)
    {
        this.isSelected = isSelected;

        if (!canObjectEverMove) return;

        if (isExperimental)
            touchmanskin.enabled = isSelected;
        else
            touchmanskin.enabled = false;
    }

    public void SwitchAssetTransformMode(AssetTransformMode value)
    {
        rigidBody.velocity = Vector3.zero;
        assetTransformMode = value;
        if (value == AssetTransformMode.rotate | value == AssetTransformMode.translateVertical)
        {
            transform.GetComponent<TouchManSkin>().enabled = false;
        }

        else if (value == AssetTransformMode.translate)
        {
            if (isExperimental)
            {
                transform.GetComponent<TouchManSkin>().enabled = true;
                //touchmanskin.UpdateClampValueByLearning();
            }
        }
        //SOS disabling visual cues for now
        //transform.GetComponentInChildren<Selector>().EnableQuadChilds(value);
    }

    void BeginDrag(GameObject dragStartedOver, Vector2 touchPosition)
    {
        if (!isSelected) return;
        //Debug.Log(dragStartedOver.name+"\n"+gameObject.name);
        //edit this SOS
        if (dragStartedOver != gameObject)
        //!dragStartedOver.GetComponent<Collider>().isTrigger && dragStartedOver.layer.IsOneOf(TouchManLayers._SelectableObjectsLayerList))
        canMove = false;
        else
            canMove = true;
    }

    void DoMoveOnDrag()
    {
        float mouseSpeed = 4;
        float touchSpeed = .25f;
        float touchSpeedRotate = .6f;

        float rot = 0;
        float posY = 0;
        Vector3 posXZ = Vector3.zero;

        switch (assetTransformMode)
        {
            case AssetTransformMode.rotate:
                if (!canMove) return;
                if (Input.GetMouseButton(0))
                    rot = Input.GetAxis("Mouse X") * mouseSpeed;
                if (Input.touchCount > 0)
                    rot = Input.GetTouch(0).deltaPosition.x * touchSpeedRotate;

                transform.Rotate(Vector3.up * -rot);
                //ClampRotation();
                RotationalCue(transform.eulerAngles.y);
                rigidBody.velocity = Vector3.zero;
                break;

            case AssetTransformMode.translateVertical:
                if (!canMove) return;
                if (Input.GetMouseButton(0))
                    posY = Input.GetAxis("Mouse Y") * mouseSpeed;

                if (Input.touchCount > 0)
                    posY = Input.GetTouch(0).deltaPosition.y * touchSpeed;

                transform.GetComponent<Rigidbody>().velocity = transform.up * posY;
                break;

            case AssetTransformMode.translate:

                if (!isExperimental)
                {
                    if (Input.GetMouseButton(0))
                        posXZ = new Vector3(-Input.GetAxis("Mouse X"), 0, -Input.GetAxis("Mouse Y")) * mouseSpeed * Time.deltaTime * 100;

                    if (Input.touchCount > 0)
                        posXZ = new Vector3(-Input.GetTouch(0).deltaPosition.x, 0, -Input.GetTouch(0).deltaPosition.y) * touchSpeed;


                    transform.GetComponent<Rigidbody>().velocity = posXZ;
                }
                break;
        }
    }

    void EndDrag()
    {
        if (assetTransformMode == AssetTransformMode.rotate)
            ClampRotation();
        transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    void OneFingerStationary()  //Not using right now
    {
        if (assetTransformMode == AssetTransformMode.rotate)
            ClampRotation();
    }

    void ClampRotation()
    {
        Vector3 rotation_of_object = transform.rotation.eulerAngles;

        float rotVal = rotation_of_object.y;
        if (rotVal % 45 < 7 || rotVal % 45 > 38)
        {
            rotVal = Mathf.Round(rotVal / 45) * 45f;
        }
        rotation_of_object.y = rotVal;
        transform.rotation = Quaternion.Euler(rotation_of_object);
        RotationalCue(rotVal);

    }

    void RotationalCue(float rotVal)
    {
        Messenger<int>.Broadcast(StaticFunctionCalls.TransmitRotationValue, (int)rotVal);
    }
}

public enum AssetTransformMode
{
    idle,
    translate,
    translateVertical,
    rotate,
    scale
}