﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _unity;
using System.Threading.Tasks;
using System.IO;
using System;
using System.Linq;
using TMPro;
using Loader3DC;
using System.Text.RegularExpressions;

using Homefuly;

public class ArtifactManager
{

    #region Primitive
    WebApi server;


    public ArtifactManager(WebApi server)
    {
        this.server = server;

    }

    //GameObject prefSelector;

    #endregion



    #region Core

    IArtifactFile CheckExists_Deprecated(IArtifactFile artFile)
    {
        Debug.LogWarning("this CheckExists is Deprecated, use new CheckExists function in ArtifactManager");
        string fPath = artFile.artifactType == ArtifactType.room
            ? AppConfig.StorageConfig.ROOMS_PATH
            : AppConfig.StorageConfig.ASSETS_PATH;

        artFile.location = fPath + "/" + artFile._id;

        return Directory.Exists(artFile.location) ? artFile : null;
    }

    int[] _ZIP_PROGRESS = new int[1];
    IArtifactFileResponse Prepare(IArtifactFile artFile)
    {
        IArtifactFileResponse response = new IArtifactFileResponse { err = true, data = "Error Resolving File Locally.", artifact = artFile };

        response.artifact.location = artFile.artifactType == ArtifactType.room
          ? AppConfig.StorageConfig.ROOMS_PATH
          : AppConfig.StorageConfig.ASSETS_PATH;

        response.artifact.location += "/" + artFile._id;

        Directory.CreateDirectory(response.artifact.location);

        if (!File.Exists(artFile.downloadLocation))
        {
            Debug.LogError("Couldnt Find the zip in Temp Directory While Unzipping: " + artFile.downloadLocation);
            response.data = "Zip file not Found : " + artFile.downloadLocation;
            return response;
        }

        try
        {
            lzip.decompress_File(artFile.downloadLocation, response.artifact.location, _ZIP_PROGRESS);
            //ZipUtil.Unzip(artFile.downloadLocation, response.artifact.location);
            //Debug.Log("Unzipped to: " + exportLocation);
        }
        catch (Exception e)
        {
            Debug.LogError("Catched Exception while unzipping, : " + e.Message);
            response.data = "Exception while unzipping : " + e.Message;
            return response;
        }

        try
        {
            File.Delete(artFile.downloadLocation);
            response.data = "Artifact prepared succefully.";
        }
        catch (Exception e)
        {
            Debug.LogWarning("Catched Exception while deleting File, Ignoring it Anyway : " + e.Message);
            response.data = "Exception while deleting zip. Ignoring anyway: " + e.Message;
        }

        response.err = false;
        return response;
    }

    async Task<GameObject> Load(IArtifactFileResponse artifactFileRes, Vector3? position = null, Vector3? rotation = null)
    {
        Vector3 v = position == null ? new Vector3(-.55f, .05f, 0) : (Vector3)position;
        Vector3 v1 = rotation == null ? new Vector3(0, 0, 0) : (Vector3)rotation;

        return await Loader3DCMain.Load3DContentFrom(new DirectoryInfo(artifactFileRes.artifact.location));

        /*string objLocation = GetObjFilePath(artifactFileRes.artifact.location);
        Loader3DCMain.Load3DContentFrom(new DirectoryInfo(artifactFileRes.artifact.location));
        return await ObjLoader.AsyncLoadOBJFile(objLocation, new Vector3(0f, 0.01f, 0));*/
    }

    GameObject LoadDuplicate(GameObject original, IArtifactFileResponse artifactFileRes, Vector3? position = null, Vector3? rotation = null)
    {
        Vector3 v = position == null ? new Vector3(-.55f, .05f, 0) : (Vector3)position;
        Vector3 v1 = rotation == null ? new Vector3(0, 0, 0) : (Vector3)rotation;

        return UnityEngine.Object.Instantiate(original);
    }

    string GetObjFilePath(string possibleObjFolderPath)
    {
        List<string> _excludedDirectories = new List<string>() { "Admin", "__MACOSX" };
        var filteredDirs = Directory.GetDirectories(possibleObjFolderPath).Where(d => !(_excludedDirectories.Any(p => new DirectoryInfo(d).Name.Equals(p))));

        //Try To Get it From The Default Path
        foreach (string dir in filteredDirs)
        {
            string[] files = Directory.GetFiles(dir, "*.obj", SearchOption.TopDirectoryOnly);
            if (files.Length > 0)
            {
                return files[0];
            }
        }

        //Debug.LogWarning("[OBJ Finder] First Level File Fetch Failed, Looking For Other Options");

        string[] toplevelFiles = Directory.GetFiles(possibleObjFolderPath, "*.obj", SearchOption.TopDirectoryOnly);
        if (toplevelFiles.Length > 0)
        {
            // Debug.Log("[OBJ Finder] Level 2 Fecth Success, Fecthing obj from directory" + toplevelFiles[0]);
            return toplevelFiles[0];
        }

        foreach (string dir in filteredDirs)
        {
            string[] files = Directory.GetFiles(dir, "*.obj", SearchOption.AllDirectories);
            if (files.Length > 0)
            {
                Debug.Log("[OBJ Finder] Deep Fetch Success, Fecthing obj from directory" + files[0]);
                return files[0];
            }
        }

        Debug.LogError("[OBJ Finder] All Attempt of File Fetch Failed, OBJ FILE NOT FOUND IN THE DIRECTORY");

        return string.Empty;
    }

    #region Asset Update checker
    //SOS
    public IArtifactFile CheckExists2(IArtifactFile artFile, string fileInServerUpdatedAt="")
    {

        string fPath = artFile.artifactType == ArtifactType.room
            ? AppConfig.StorageConfig.ROOMS_PATH
            : AppConfig.StorageConfig.ASSETS_PATH;

        artFile.location = fPath + "/" + artFile._id;

        if (Directory.Exists(artFile.location))
        {
            if (string.IsNullOrEmpty(GetObjFilePath(artFile.location)))
            {
                Debug.LogWarning("[Artifact] Asset Folder Exists But It Is Empty. Requires Redownloading for "+artFile._id);
                //CleanFileData(assetId);
                return null;
            }

            if (string.IsNullOrEmpty(fileInServerUpdatedAt)&& artFile.artifactType==ArtifactType.asset)
            {
                Debug.LogWarning("[Artifact] This asset doesnt have updatedAt: " + artFile._id);
                //Debug.Log("Tis work");
                //if (ReturnAssetDataFromDb(artFile._id)!=null)// Account.Instance.VersionData.localStore.ContainsKey(assetId))
                //{
                //    fileInServerUpdatedAt = ReturnAssetDataFromDb(artFile._id).updatedAt;// Account.Instance.VersionData.localStore[assetId].updatedAt;
                    //Debug.Log("Asset " + artFile._id + "'s File has this updated at:" + ReturnAssetDataFromDb(artFile._id).updatedAt + ", Local Version: " + GetAssetCacheMeta(artFile._id));
                //}
                //else
                //{
                    //Debug.Log("ReturnAssetDataFromDB failed");
                //}
            }

            if (!string.IsNullOrEmpty(fileInServerUpdatedAt))
            {
                if (GetAssetCacheMeta(artFile._id)!= fileInServerUpdatedAt)
                {
                    Debug.LogWarning("[Artifact] Asset " + artFile._id + "'s File is outdated, Redownloading. Server Version Of Asset:" + fileInServerUpdatedAt + ", Local Version: " + GetAssetCacheMeta(artFile._id));
                    CleanFileData(artFile._id);
                    return null;
                }
                else
                {
                    //Debug.Log("All fine and dandy in Asset " + artFile._id + "'s File Outdated, Redownloading. Server Version Of Asset:" + fileInServerUpdatedAt + ", Local Version: " + GetAssetCacheMeta(artFile._id));
                    return artFile;
                }
            }
            else
            {
                return artFile;
            }
        }
        else
        {
            Debug.Log("[Artifact] Requires Downloading asset from server: "+artFile._id);
            return null;
        }
    }

    /*public IAsset ReturnAssetDataFromDb(string id)
    {
        var data = AppComponent.Instance.db.GetAllUniqueAssets();
        if (data.ContainsKey(id)) return data[id];
        else
            return null;
    }*/

    public bool CleanFileData(string Id)//, FileType ft = FileType.asset)
    {
        string fileLocation = string.Empty;
        //if (ft == FileType.asset) { fileLocation = ServerCore.Instance.assetsPath + "/" + Id; }
        //else { fileLocation = ServerCore.Instance.roomsPath + "/" + Id; }
        fileLocation = AppConfig.StorageConfig.ASSETS_PATH+ "/" + Id;
        
        try
        {
            RemoveAssetCacheMeta(Id);
            Directory.Delete(fileLocation, true);
            return true;
        }
        catch (Exception e) { Debug.LogWarning("Catched Exception While Cleaning the Failed To Load File Data from the Loc: " + fileLocation + ", Catched: " + e.Message); return false; }
    }

    public void RemoveAssetCacheMeta(string id)
    {
        string str = "ASSET_UPDATE_CACHE_" + id;
        if (PlayerPrefs.HasKey(str))
        {
            PlayerPrefs.DeleteKey(str);
        }
        else Debug.LogWarning("Invalid Key To Delete In Player Prefs: " + str);

    }

    public string GetAssetCacheMeta(string id)
    {
        string str = "ASSET_UPDATE_CACHE_" + id;
        if (!PlayerPrefs.HasKey(str))
        {
            PlayerPrefs.SetString(str, "");
        }

        return PlayerPrefs.GetString(str);
    }
    #endregion

    async Task<IArtifactLoadAsyncOperationResult> DownloadArtifact(string artId, ArtifactType artType, Transform parent, GameObject original = null, string AssetUpdatedAt = null)
    {
        IArtifactFileResponse response = new IArtifactFileResponse
        {
            artifact = new IArtifactFile
            {
                _id = artId,
                artifactType = artType
            },
            err = true,
            data = "Unknown Error"
        };

        IArtifactFile isArtExists = this.CheckExists2(response.artifact, AssetUpdatedAt);

        if (isArtExists == null)
        {
            response.artifact = await server.artifact.DownloadArtifact(response.artifact);

            if (response.artifact == null)
            {

                //widget.Alert("404, File not found!", "File not found on Server");
                return new IArtifactLoadAsyncOperationResult { err = true, data = "404, File not found", gameObject = null };
            }

            response = this.Prepare(response.artifact);
        }
        else
        {
            response.err = false;
            response.data = "Asset Found!";
            response.artifact = isArtExists;
        }



        GameObject rmGo;
        if (original != null)
        {
            Debug.Log("Duplicate Found!: ");
            rmGo = this.LoadDuplicate(original, response);
            original = null;
        }
        else
        {
            rmGo = await this.Load(response);
        }

        rmGo.transform.SetParent(parent);


        if (rmGo == null)
        {
            //widget.Alert("Unable to Load Room", "There was an error loading the rooms");
            return new IArtifactLoadAsyncOperationResult { err = true, data = "Unable to Load Room", gameObject = null };
        }

        return new IArtifactLoadAsyncOperationResult { err = false, data = "Room Loaded", gameObject = rmGo };
    }

    void PostProcessArtifact(Transform transArtifact, ArtifactType artType, IFloorPlanProcessor floorPlanData = null, Vector3? positionOffset = null, Vector3? rotation = null)
    {
        if (artType == ArtifactType.room && floorPlanData == null)
        {
            throw new Exception("IFloorPlanProcessor is required,= to process Floorplan.");
        }


        if (artType == ArtifactType.room)
        {

            foreach (Transform t in transArtifact)
            {
                string name = t.name.ToLower();


                if (name.Contains("floor"))
                {
                    ProcessSorting(t, transArtifact, "Floor");
                    ProcessFloor(t);
                    t.GetOrAddComponent<BoxCollider>(); //ProcessColliders(t);
                }
                else if (name.Contains("segment"))
                {
                    ProcessSorting(t, transArtifact, "Segment");
                    ProcessSegment(t, floorPlanData.segmentMaterial_Deprycated);
                    ProcessColliders(t, true);
                }
                else if (name.Contains("ignoreme"))
                {
                    t.gameObject.layer = LayerMask.NameToLayer("RoofAddons");
                }
                else if (name.Contains("ceiling") || name.Contains("roof") || name.Contains("box"))
                {
                    ProcessSorting(t, transArtifact, "Roof");
                    ProcessRoof(t);
                    t.GetOrAddComponent<BoxCollider>(); //ProcessColliders(t);
                }
                else if (name.Contains("light"))
                {
                    ProcessSorting(t, transArtifact, "Lights");
                    ProcessLights(t, floorPlanData.lightHalo, floorPlanData.matLight, floorPlanData.haloOffset);
                }
                else if (name.Contains("window") || name.Contains("door"))
                {
                    if (t.GetComponent<Renderer>())
                    {
                        Material[] floorMats = t.GetComponent<Renderer>().materials;//apply smoothness to floor
                        foreach (Material m in floorMats)
                        {
                            if (m.GetFloat("_Mode") == 3)
                            {
                                m.shader = Shader.Find("Mobile/Particles/Additive");
                                m.SetTexture("_MainTex", Resources.Load("invisible_tex") as Texture);
                                m.SetFloat("_Mode", 1);
                                Color temp = new Color(1f, 1f, 1f, 0f);
                                m.SetColor("_Color", temp);

                            }
                        }
                    }
                    foreach (Transform childt in t)
                    {
                        Material[] floorMats = childt.GetComponent<Renderer>().materials;//apply smoothness to floor
                        foreach (Material m in floorMats)
                        {
                            m.SetFloat("_Glossiness", 0.7f);
                            m.SetColor("_SpecColor", new Color(0.07450981f, 0.07450981f, 0.07450981f));   //new Color(0x13, 0x13, 0x13, 0xFF));
                        }
                    }
                }
                else if (name.Contains("wall") && !name.Contains("door"))
                {
                    ProcessSorting(t, transArtifact, "Walls");
                    ProcessWall(t);
                    ProcessWallColliders(t);
                }
                //Not using portals so placement is fine
                else if (name.Contains("door"))
                {
                    ProcessSorting(t, transArtifact, "Doors");
                    ProcessDoor(t, floorPlanData.doorPortal_Deprycated);
                    //ProcessColliders(t);
                }
                else if (name.Contains("pillar"))
                {
                    ProcessSorting(t, transArtifact, "Pillars");
                    //ProcessColliders(t);
                }
                else if (name.Contains("fence"))
                {
                    //ProcessSorting(t, transArtifact, "Pillars");
                    //ProcessColliders(t);
                }
                else
                {
                    ProcessSorting(t, transArtifact, "Misc");
                    //ProcessColliders(t);
                }

            }

        }

        if (artType == ArtifactType.asset)
        {

            transArtifact.tag = "Asset";
            transArtifact.gameObject.layer = 14;


            //Debug.LogError("Says Process Asset Movement");

            ProcessAssetMovement(transArtifact);


            foreach (Transform t in transArtifact)
            {
                ProcessCollidersAsset(transArtifact);
            }

            //AttachSelector(transArtifact);

        }

        if (positionOffset != null)
        {
            transArtifact.position = (Vector3)positionOffset;

            if (transArtifact.GetComponent<BoxCollider>())
            {
                if (transArtifact.GetComponent<BoxCollider>().size.y < 0.01f)
                {
                    Debug.LogWarning("[Artifact Loader] Very Thin asset detected, lifitng it up and increasing its size by 0.01f, Asset name: " + transArtifact.gameObject.name);
                    transArtifact.GetComponent<BoxCollider>().size = new Vector3(transArtifact.GetComponent<BoxCollider>().size.x, transArtifact.GetComponent<BoxCollider>().size.y + 0.01f, transArtifact.GetComponent<BoxCollider>().size.z);
                    transArtifact.position = new Vector3(transArtifact.position.x, transArtifact.position.y + 0.01f, transArtifact.position.z);
                }
            }
        }

        if (rotation != null)
        {
            transArtifact.rotation = Quaternion.Euler((Vector3)rotation);
        }

    }


    #endregion


    #region Room Manipulation

    void PaintWallWithTexture(GameObject Wall, string wallfinishId, Texture2D tex)
    {

        if (Wall.transform.Find("pId") != null)
        {
            GameObject gid = new GameObject("pId");
            gid.transform.SetParent(Wall.transform);

            GameObject gidV = new GameObject("pIdV");
            gidV.transform.SetParent(gid.transform);
        }

        Wall.transform.Find("pId").GetChild(0).name = wallfinishId;

        Material wallPaint = new Material(Shader.Find("Standard"))
        {
            color = new Color(1, 1, 1)
        };

        wallPaint.SetTexture("_MainTex", tex);
        Material[] mats = Wall.GetComponent<Renderer>().materials;
        for (int i = 0; i < mats.Length; i++)
        {
            mats[i] = wallPaint;
        }
        Wall.GetComponent<Renderer>().materials = mats;
    }

    #endregion


    #region Post Process Room

    void ProcessSorting(Transform trans, Transform parent, string parentName)
    {
        // trans.SetParent(parent.Find(parentName));
    }

    void ProcessColliders(Transform trans, bool isTrigger = false)
    {

        if (isTrigger)
        {
            trans.GetOrAddComponent<BoxCollider>().isTrigger = true;
        }
        else
        {
            if (trans.GetComponent<MeshRenderer>() != null)
                trans.GetOrAddComponent<MeshCollider>().isTrigger = false;
        }

    }


    void ProcessWallColliders(Transform trans)
    {
        var rgx = new Regex(@"^(?i)(wa*l+|wal+)(\s|_|\.|\+|-)*[a-zA-Z1-9]*(,|$)");
        Match matchWall = rgx.Match(trans.name);
        var rgx_skecthup = new Regex(@"^(?i)mesh[0-9]*(\s|_|\.|\+|-)*[a-z0-9]*(\s|_|\.|\+|-)*(wa*l+|wal+).*");
        Match matchWall_skecthup = rgx_skecthup.Match(trans.name);
        if (matchWall.Success || matchWall_skecthup.Success)
        {
            trans.GetOrAddComponent<BoxCollider>();
        }
        else
        {
            //if (trans.GetComponent<MeshRenderer>() != null)
            //trans.GetOrAddComponent<MeshCollider>().isTrigger = false;
        }

    }

    void ProcessLights(Transform trans, GameObject lightHalo, Material matLight, float haloOffset)
    {

        trans.gameObject.tag = "Light";
        trans.gameObject.layer = 13;

        /*
        var mat = trans.GetComponent<Renderer>();
        if (mat == null) return;

        mat.material = matLight;
        var pos = trans.GetCenter(new Vector3(0, haloOffset, 0));
        if (pos == null) return;

        GameObject go = UnityEngine.Object.Instantiate(lightHalo, trans);
        go.transform.position = (Vector3)pos;

        */
    }

    void ProcessRoof(Transform trans)
    {
        trans.gameObject.layer = 9;
        trans.gameObject.tag = "Roof";
    }

    void ProcessFloor(Transform trans)
    {
        if (trans.GetComponent<MeshFilter>() == null)
            return;
        trans.gameObject.tag = "Floor";
        trans.gameObject.layer = 10;
    }

    void ProcessSegment(Transform trans, Material matSegment)
    {
        trans.gameObject.tag = "Segment";
        if (trans.GetComponent<MeshRenderer>() == null)
            return;

        trans.GetComponent<MeshRenderer>().material = matSegment;
        trans.GetOrAddComponent<FloorSegmentTracker>();
        trans.gameObject.layer = 12;
    }

    void ProcessDoor(Transform trans, GameObject elemPortal)
    {
        if (!trans.name.Contains("_a_")) return;

        trans.gameObject.tag = "Door";
        //Debug.Log("trans pos: " + trans.position);
        Vector3 pos = (Vector3)trans.GetCenter();
        Vector3 rot = trans.GetAlignedRotationEuler();
        pos.x = Mathf.Sign(trans.localScale.x) * pos.x;
        //pos = pos * MULTIPLYWITHSCALEHERE
        //Debug.Log("ret pos: " + (Vector3)pos);

        if (pos == null) return;

        GameObject go = UnityEngine.Object.Instantiate(elemPortal, trans);

        foreach (DoorPortal_Deprycated dp in go.transform.GetComponentsInChildren<DoorPortal_Deprycated>())
        {
            dp.group = trans.name;
        }
        //Debug.Log("Adding a pos of: " + (Vector3)pos);
        go.transform.position = pos;
        go.transform.eulerAngles = rot;
        go.SetActive(true);

        //parse Door Path From Door Name
        var splitStr = trans.name.Split(new char[] { '_' }, 6);
        if (splitStr.Length >= 6)
        {
            splitStr[5] = splitStr[5].Replace('_', ' ').ToTitleCase();
            go.transform.Find("DoorPortal_A/imgDoorName_A/txtDoorName").gameObject.GetComponent<TextMeshPro>().text = splitStr[5];
            go.transform.Find("DoorPortal_B/imgDoorName_B/txtDoorName").gameObject.GetComponent<TextMeshPro>().text = splitStr[5];
        }
        else
        {
            //Debug.LogWarning("no hoDoor nith noname nound nring nata:" + trans.name);
            go.transform.Find("DoorPortal_A/imgDoorName_A").gameObject.SetActive(false);
            go.transform.Find("DoorPortal_B/imgDoorName_B").gameObject.SetActive(false);
        }

        trans.GetOrAddComponent<DoorPortalLauncher_Deprycated>();
    }

    void ProcessWall(Transform trans)
    {
        trans.gameObject.tag = "Wall";
        trans.gameObject.layer = 17;
        //trans.GetOrAddComponent<WallHandler>();
    }



    #endregion

    #region Post Process Asset

    void ProcessCollidersAsset(Transform trans)
    {
        bool hasBounds = false;
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);


        foreach (Transform t in trans)
        {
            if (t.GetComponent<MeshRenderer>() != null)
            {
                t.GetOrAddComponent<BoxCollider>();

                Renderer childRenderer = t.GetComponent<Renderer>();
                if (childRenderer != null)
                {
                    if (hasBounds)
                    {
                        bounds.Encapsulate(childRenderer.bounds);
                    }
                    else
                    {
                        bounds = childRenderer.bounds;
                        hasBounds = true;
                    }

                    UnityEngine.Object.Destroy(t.GetComponent<BoxCollider>());
                }
            }
        }

        BoxCollider collider = trans.GetOrAddComponent<BoxCollider>();
        collider.center = bounds.center - trans.position;
        collider.size = bounds.size;
        collider.isTrigger = true;
        
        //Debug.Log(" collider.size : " + collider.size);

    }

    //Assets
    void ProcessAssetMovement(Transform trans)
    {
        AssetTransformManager atm = trans.GetOrAddComponent<AssetTransformManager>();

        atm.entry.entryId = Guid.NewGuid().ToString();
        atm.entry.asset = new IAsset { _id = trans.name };
        atm.entry.objRef = trans.gameObject;
        atm.entry.inuse = true;

        trans.name = atm.entry.entryId;

    }

    #endregion


    #region ==[ Room ]==

    public async Task<IArtifactLoadAsyncOperationResult> DownloadFloorplan(string centerId, Transform parent)
    {
        return await this.DownloadArtifact(centerId, ArtifactType.room, parent);
    }

    public void PostProcessFloorplan(Transform transArtifact, IFloorPlanProcessor floorPlanData)
    {
        this.PostProcessArtifact(transArtifact, ArtifactType.room, floorPlanData);
    }

    #endregion

    #region ==[ Products ]==



    public async Task<IArtifactLoadAsyncOperationResult> DownloadAsset(IAsset assetData, Transform parent, GameObject original = null)
    {
        string assetId = assetData._id;
        var asset = await this.DownloadArtifact(assetData._id, ArtifactType.asset, parent, original,assetData.updatedAt);

        asset.gameObject.name = assetId;

        return asset;
    }

    public void PostProcessAsset(Transform transArtifact, Vector3 position, Vector3? rotation = null)
    {
        //this.prefSelector = prefSelector;
        this.PostProcessArtifact(transArtifact, ArtifactType.asset, null, position, rotation);
    }

    #endregion



    public class RoomObjectData
    {
        public string name;
        public RoomObjectType objectType;
        public Bounds bounds;
        public Vector3 center;
        public string navigatesTo;
        public string motherWall;
    }

    public enum RoomObjectType
    {
        door,
        window,
        light,
        skirt,
        grill,
        switchboard
    }
}
