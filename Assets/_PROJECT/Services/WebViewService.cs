﻿using _unity;
using System;
using UnityEngine;

namespace Homefuly.WebView
{

    public class WebViewService : Singleton<WebViewService>
    {
        protected WebViewService() { }

        // private UniWebView webView;
        private RectTransform refRect;

        public GameObject unityWebViewPrefab;



        public void Init(GameObject viewPref, RectTransform refRect = null)
        {
            unityWebViewPrefab = viewPref;
            this.refRect = refRect;
            // SetWebView();
            // it was opening a blank white page when initing and the only way to close is to
            // destroy the webView gameobject according to their documentation
            //Destroy(webView);
            //webView = null;
            // print("WebViewService Initialized!");
        }

        public UniWebView OpenURL(string url)
        {
            return OpenWebpage(url);
        }


        #region Tools

        private UniWebView OpenWebpage(string url)
        {
            if (!url.Contains("http"))
            {
                url = "https://" + url;
            }

            try
            {
                // SetWebView();

                // webView.Load(url);
                // webView.Show();

                // return webView;
                return null;
            }
            catch (System.Exception e)
            {
                print("[XX] : Err on LOad : " + e.ToString());
                throw new Exception(e.ToString());
            }
        }

        public void EnableTopBar(bool show)
        {
            // SetWebView();
            // webView.SetShowToolbar(show);
        }

        public void EnableImmersive()
        {
            // webView.SetImmersiveModeEnabled(true);
        }

        public void ClearCache()
        {
            // webView.CleanCache();
        }

        public void ClearWebViewObject()
        {
            // Destroy(webView.gameObject);
            // webView = null;
        }

        public void SetWebView()
        {
            return;
            // if (webView == null)
            // {
            //     GameObject goWebView = GameObject.Find("UniWebView");
            //     if (goWebView == null)
            //     {
            //         goWebView = Instantiate(unityWebViewPrefab);
            //         goWebView.name = "UniWebView";
            //     }
            //     webView = goWebView.transform.GetOrAddComponent<UniWebView>();
            //     if (refRect != null)
            //         webView.ReferenceRectTransform = refRect;
            //     else Debug.Log("Ref Rect not found or destroyed");
            // }
        }

        // Service to send arguments 
        // public void SendMessageToWeb(string context, params string[] args)
        // {
        //     string message = String.Join(",", args);
        //     webView.EvaluateJavaScript("responseFromUnity("+ context + ","+ message +")");
        // }

        // public void showWeb()
        // {
        //     webView.Show(true, UniWebViewTransitionEdge.Bottom, 0.35f);
        // }

        // public void hideWeb()
        // {
        //     webView.Hide(true, UniWebViewTransitionEdge.Bottom, 0.35f);
        // }


        public void close()
        {
            // if (webView == null) return;

            // webView.Hide();
        }
        #endregion

    }
}
