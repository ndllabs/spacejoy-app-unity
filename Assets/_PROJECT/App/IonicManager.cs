﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Homefuly;
using Homefuly.Database;
using System;
using Homefuly.Events;

public class IonicManager : MonoBehaviour
{
    AppComponent app;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("IonicManager Start");
        app = AppComponent.Instance;
        AppDatabase.OnDBReady += OnReceiveMessage;
    }

    // Will basically redirect contexts to corresponding scenes
    private async void OnReceiveMessage()
    {
        if(IonicComms.OnMsgLoad == null)
            IonicComms.OnMsgLoad += OnReceiveMessage;

        if(IonicComms.OnMsgReceive == null)
            IonicComms.OnMsgReceive += OnReceiveMessage;

        Debug.Log("Entered [OnReceiveMessage]");

        if(PlayerPrefs.HasKey("launchUnity"))
        {
            Debug.Log("[LaunchUnity] " + PlayerPrefs.GetString("launchUnity"));
            IABResponse ionicMessage = new IABResponse(PlayerPrefs.GetString("launchUnity"));
            PlayerPrefs.DeleteKey("launchUnity");

            Debug.Log("[IonicMessage Context]" + ionicMessage.Context);
            Debug.Log("[IonicMessage Data]" + ionicMessage.GetData<IOChallenge>());

            // string context = ionicMessage.GetContext<string>();
            
            if(ionicMessage.Context == "challenge")
            {
                var  data = ionicMessage.GetData<IOChallenge>();

                Debug.Log("Challenge ID: " + data.cid);
                app.account.CURRENT_CHALLENGE_ID = data.cid;
                app.CurrentSceneType = SceneType.DesignChallenge;
                AppConfig.PilotDemoConfig.ProjectId = AppConfig.ProjectConfig.PROJ_CHALLENGE;
                
                Debug.Log("[Opening Design Challenge]");
                app.Navigate(AppPage.DesignChallenge);
            }
            else if(ionicMessage.Context == "design")
            {
                var data = ionicMessage.GetData<IODesign>();

                Debug.Log("Version ID: " + data.vid);
                AppConfig.PilotDemoConfig.ProjectId = data.pid;
                app.account.CURRENT_ROOM_ID = data.rid;
                app.account.CURRENT_VERSION_ID = data.vid;
                app.CurrentSceneType = SceneType.UserRoom;

                app.Navigate(AppPage.Designer);
            }
            else if(ionicMessage.Context == "login")
            {
                var data = ionicMessage.GetData<IOLogin>();

                Debug.Log("User Token: " + data.token);
                app.account.UpdateLoginCred(data);
                await app.db.OnUpdateAuth(true);
                data.authProvider = data.authProvider.ToLower();
                HomefulyEvents.OnAuthUpdated?.Invoke(data.authProvider == "google" ? AuthUpdateType.Google : (data.authProvider == "facebook" ? AuthUpdateType.Facebook : (data.authProvider == "homefuly" ? AuthUpdateType.InternalSignIn : AuthUpdateType.Error)));
            }
            else if(ionicMessage.Context == "logout")
            {
                Debug.Log("Logging out");

                await app.account.Logout();
                var resD = await app.account.Connect();
                app.db.ResetDb();
            }

            RemoveDelegates();
        }
        else if(PlayerPrefs.HasKey("minimizeUnity"))
        {
            PlayerPrefs.DeleteKey("minimizeUnity");
            RemoveDelegates();
            IonicComms.FinishActivity("Unity Minimized [Current Scene: Appcomponent]");
        }
    }

    void RemoveDelegates()
    {
            Debug.Log("[Removing Delegates]");

            if(AppDatabase.OnDBReady != null)
            {
                AppDatabase.OnDBReady -= OnReceiveMessage;
            }

            // if(IonicComms.OnMsgLoad != null)
            // {
            //     IonicComms.OnMsgLoad -= ProcessContext;
            // }
    }

    // when unity activity comes to foreground process context info
    void OnApplicationPause(bool pauseStatus)
    {
        // Debug.Log("Entered [OnApplicationPause] [IonicManager]" + pauseStatus + " isUnityRunning: " + AppComponent.Instance.isUnityRunning + " delegateNull? " + (IonicComms.OnMsgLoad==null).ToString());
        // if(!pauseStatus && AppComponent.Instance.isUnityRunning)
        // {
        //     if(IonicComms.OnMsgLoad != null) return;

        //     IonicComms.OnMsgLoad += ProcessContext;
        // }
    }

    // Data classes for ionic messages
    [System.Serializable]
    public class IOChallenge
    {
        public string cid = string.Empty;
    }

    [System.Serializable]
    public class IODesign
    {
        public string pid = string.Empty;
        public string rid = string.Empty;
        public string vid = string.Empty;
    }

}

[System.Serializable]
    public class IOLogin
    {
        public string token;
        public string userid;
        public string name;
        public string useremail;
        public string password;
        public string authProvider;
        public string role;
    }