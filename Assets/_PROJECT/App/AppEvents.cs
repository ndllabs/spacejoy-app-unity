﻿using System;
public static class AppEvents
{
    public static string ASSET_SELECTED = "OP_ASSET_SELECTED";
    public static string ASSET_DESELECTED = "OP_ASSET_DESELECTED";

    public static string ASSET_TAPPED = "OP_ASSET_TAPPED";

    public static string ASSET_DROPPED_INSIDE_ROOM = "OnAssetDroppedInsideRoom";
    public static string ASSET_DROP_COMPLETE = "ASSET_DROP_COMPLETE";
    public static string OnSwapCurrentAsset = "OnSwapCurrentAsset";

    public static string OP_PORTAL_TRANS_INIT = "OP_PORTAL_TRANS_INIT";
    public static string OP_PORTAL_TRANS_LAUNCH = "OP_PORTAL_TRANS_LAUNCH";
}
