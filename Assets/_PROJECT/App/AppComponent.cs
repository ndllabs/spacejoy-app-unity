﻿using _unity;
using Homefuly.Database;
using System;
using System.Collections;
using System.Threading.Tasks;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif


namespace Homefuly
{

    public class AppComponent : Singleton<AppComponent>
    {
        #region Fields
        public WebApi server;
        public AppDatabase db = null;
        public AccountConfigService account;
        public bool init = false;
        public SceneType CurrentSceneType = SceneType.None;
        public string openedTab;
        public AuthManager authMan;
        // public bool isUnityRunning = false;
        #endregion

        public async void Init(Action<bool> Callback = null)
        {
            Housekeeping();

            server = new WebApi(AppConfig.ServerConfig.host, AppConfig.ServerConfig.basicTimeout, AppConfig.ServerConfig.extendedTimeout);
            account = new AccountConfigService(server);

            WidgetManager.Instance.Init();
            authMan = WidgetManager.Instance.gameObject.GetComponent<AuthManager>();
            //PlayerPrefs.SetString("CRED_USEREMAIL", "prawinsg@gmail.com");
            //PlayerPrefs.SetString("CRED_PASSWORD", "");
            //PlayerPrefs.SetString("CRED_USERID", "pasdf");
            //PlayerPrefs.SetString("CRED_JWTTOKEN", "asdfasdfasd");
            //PlayerPrefs.SetString("CRED_ISDEMO", "not demo");

            /******* IMPORTANT *******/
            // temp fix for not using any login, need to be removed later
            if (PlayerPrefs.GetString("CRED_AUTHPROVIDER") == "facebook" || PlayerPrefs.GetString("CRED_AUTHPROVIDER") == "google")
            {
                await account.Logout();

            }

            /*******/

            var resD = await account.Connect();
            if (!resD.err)
            {
                //await PermissionMan();
                ProceedAfterLogin();
            }
            else
            {
                AuthType type = resD.data.ToEnum<AuthType>();
                Debug.Log("[In app login] " + resD.data + " auth type " + AuthType.facebook.ToString() + " " + AuthType.google.ToString());
                // if jwt expired was from facebook or google
                if (resD.data == AuthType.facebook.ToString() || resD.data == AuthType.google.ToString())
                {
                    Debug.Log("[" + nameof(Init) + "] " + "Previous login token is from " + resD.data);
                    authMan.Relogin(resD.data, ProceedAfterLogin);
                }
                // if the auth provider was empty or password was empty
                else if (resD.data == "Authentication Error. Please Relogin.")
                {
                    // WebView.WebViewService.Instance.OpenURL("");
                    // WebView.WebViewService.Instance.close();
                    await Task.Delay(100);
                    PlayerPrefs.SetString("CRED_USERID", string.Empty);
                    PlayerPrefs.SetString("CRED_ISDEMO", string.Empty);
                    WidgetManager.Instance.AuthPanelOpen((bool result) =>
                    {
                        InitialLoginUsingPanel(result);
                    });
                }
                // if there is a connection error
                else
                {
                    Debug.LogError("[" + nameof(AppComponent) + "][" + nameof(Init) + "] Critical, App Cannot Start, Main Reason: " + resD.data + ", Possible Reasons: No Internet Connection | Server Offline | No Authorization");

                    WidgetManager.Instance.LoaderClose();
                    WidgetManager.Instance.Alert("Connection Failed", "Please check your internet connection and restart the app"/*,()=>app.Navigate(AppPage.Preload)*/);

                    return;
                }
            }


            init = true;
            Callback?.Invoke(resD.err);

        }

        // open the login panel and let the user sign ina
        private async void InitialLoginUsingPanel(bool result)
        {
            Debug.Log("[" + nameof(InitialLoginUsingPanel) + "] " + "After returning from auth panel, result is " + result);
            if (result)
            {
                ProceedAfterLogin();
            }
            else
            {
                // demo login and the proceed after login;
                // user id is set to empty so that the second condition in login() function fails
                // and signs in using demo user email and password
                
                var demoResult = await account.DemoLogin();
                if (!demoResult.err)
                {
                    print("Demo result, no error, demo login succeeded");
                    ProceedAfterLogin();
                }
                else
                {
                    print("Demo result, connection failed");
                    WidgetManager.Instance.LoaderClose();
                    WidgetManager.Instance.Alert("Connection Failed", "Please check your internet connection and restart the app"/*,()=>app.Navigate(AppPage.Preload)*/);
                    return;
                }
            }
        }

        /// <summary>
        /// Callback after the login happens when the app is opened
        /// </summary>
        private void ProceedAfterLogin()
        {
            /* Warning: AppDatabase requires Auth and a Internet connection to Init
             * The Server sync will happen in background but the function will return immediately
             * */
            db = new AppDatabase();
            db.Init();

            #if UNITY_EDITOR
                // AppDatabase.OnDBReady += GoToDesignChallenge;
                AppDatabase.OnDBReady += GoToDesigner;
            #endif

            //if (!Debug.isDebugBuild)
            // GoHome();
        }

        private void GoToDesigner()
        {
            AppComponent.Instance.CurrentSceneType = SceneType.UserRoom;
            AppConfig.PilotDemoConfig.ProjectId = "5c2de39dfadece04e4242674";
            AppComponent.Instance.account.CURRENT_ROOM_ID = "5cf52d32610d0969eddf0e56";
            AppComponent.Instance.account.CURRENT_VERSION_ID = "5cf669723dcfbf10ac56a624";
            Navigate(AppPage.Designer);
        }

        private void GoToDesignChallenge()
        {
            Instance.account.CURRENT_CHALLENGE_ID = "5d399f7d9f19645dc0048cd3";
            Instance.CurrentSceneType = SceneType.DesignChallenge;
            AppConfig.PilotDemoConfig.ProjectId = AppConfig.ProjectConfig.PROJ_CHALLENGE;
            Navigate(AppPage.DesignChallenge);

            AppDatabase.OnDBReady -= GoToDesignChallenge;
        }

        private void GoHome()
        {
            Navigate(AppPage.Home); return;
        }

        public void SigninReturn(bool value)
        {
            Debug.Log("Call back returned from auth " + value.ToString());
        }

        #region Navigation

        public void CheckInit()
        {
            if (!init)
            {
                SceneManager.LoadScene("AppComponent");
            }
        }


        public void Navigate(AppPage page)
        {
            switch (page)
            {
                case AppPage.Designer:
                    SceneManager.LoadScene("Designer");
                    break;
                case AppPage.DesignChallenge:
                    SceneManager.LoadScene("DesignChallenge");
                    break;
                case AppPage.Home:
                    SceneManager.LoadScene("Home");
                    break;
                case AppPage.AppComponent:
                    SceneManager.LoadScene("AppComponent");
                    break;
            }
        }

        public void SignOut(bool ConfirmWidget = true)
        {
            if (ConfirmWidget)
            {
                WidgetManager.Instance.Confirm("Sign Out", "Are you sure you want to sign out of the app?", res =>
                {
                    if (res == "Yes")
                    {
                        PlayerPrefs.DeleteAll();
                        Instance.account.CURRENT_ROOM_ID = null;
                        WidgetManager.Instance.AuthPanelOpen(); //Instance.Navigate(AppPage.Auth);
                    }
                });
            }
            else
            {
                PlayerPrefs.DeleteAll();
                Instance.account.CURRENT_ROOM_ID = null;
                WidgetManager.Instance.AuthPanelOpen();
                //Instance.Navigate(AppPage.Auth);
            }

        }

        #endregion


        #region Housekeeping

        public IEnumerator PermissionMan()
        {
            Debug.Log("getting permissions right");
#if UNITY_ANDROID

            if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            }

            yield return null;

            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
#endif

#if UNITY_IOS
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
            if (Application.HasUserAuthorization(UserAuthorization.WebCam))
            {
                Debug.Log("webcam found");
            }
            else
            {
                Debug.Log("webcam not found");
            }
#endif

            yield return null;
            Debug.Log("permissions done");
        }

        public void Housekeeping()
        {
            string tempPath = AppConfig.StorageConfig.TEMP_PATH;
            string assetsPath = AppConfig.StorageConfig.ASSETS_PATH;
            string roomsPath = AppConfig.StorageConfig.ROOMS_PATH;

            if (Directory.Exists(tempPath))
            {
                try
                {
                    Directory.Delete(tempPath, true);
                }
                catch (Exception e)
                {
                    _.l("Nothing to Clean" + e.Message);
                }
                finally
                {
                    Directory.CreateDirectory(tempPath);

                }
            }
            else
            {
                Directory.CreateDirectory(tempPath);
            }
            //Clean Download Directory

            if (!Directory.Exists(assetsPath))
            {

                Directory.CreateDirectory(assetsPath);

            }
            if (!Directory.Exists(roomsPath))
            {
                Directory.CreateDirectory(roomsPath);
            }

        }

        #endregion
    }
}
