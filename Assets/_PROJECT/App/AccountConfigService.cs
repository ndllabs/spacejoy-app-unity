﻿using System;
using System.Threading.Tasks;
using UnityEngine;

public class AccountConfigService
{
    WebApi server;
    ApiService.Auth auth;
    public string internalTestUser = "hello@homefuly.com";
    public const string demoUserEmail = "demo@homefuly.com"; 
    public const string demoPassword = "demo2018@homefuly";
    public const string demoUserID = "5bed19fe81c0eb54a88ffdc6";

    public AccountConfigService()
    { }
    public AccountConfigService(WebApi server)
    {
        this.server = server;
        this.auth = server.auth;
    }

    public string CRED_ROLE
    {
        get
        {
            return PlayerPrefs.GetString("CRED_ROLE");
        }
        set
        {
            PlayerPrefs.SetString("CRED_ROLE", value);
        }
    }

    public int CRED_ISCUSTOMER
    {
        get
        {
            return PlayerPrefs.GetInt("CRED_ISCUSTOMER");
        }
        set
        {
            PlayerPrefs.SetInt("CRED_ISCUSTOMER", value);
        }
    }

    public string CRED_USEREMAIL
    {
        get
        {
            return PlayerPrefs.GetString("CRED_USEREMAIL");
        }
        set
        {
            PlayerPrefs.SetString("CRED_USEREMAIL", value);
        }
    }

    public string CRED_USERNAME
    {
        get
        {
            return PlayerPrefs.GetString("CRED_USERNAME");
        }
        set
        {
            PlayerPrefs.SetString("CRED_USERNAME", value);
        }
    }


    public string CRED_PASSWORD
    {
        get
        {
            return PlayerPrefs.GetString("CRED_PASSWORD");
        }
        set
        {
            PlayerPrefs.SetString("CRED_PASSWORD", value);
        }
    }

    public string CRED_USERID
    {
        get
        {
            return PlayerPrefs.GetString("CRED_USERID");
        }
        set
        {
            PlayerPrefs.SetString("CRED_USERID", value);
        }
    }

    public string CRED_JWTTOKEN
    {
        get
        {
            return PlayerPrefs.GetString("CRED_JWTTOKEN");
        }
        set
        {
            PlayerPrefs.SetString("CRED_JWTTOKEN", value);
        }
    }

    public string CRED_AUTHPROVIDER
    {
        get
        {
            return PlayerPrefs.GetString("CRED_AUTHPROVIDER");
        }
        set
        {
            PlayerPrefs.SetString("CRED_AUTHPROVIDER", value);
        }
    }

    public int GRAPHICS_LEVEL
    {
        get
        {
            if (!PlayerPrefs.HasKey("GRAPHICS_LEVEL"))
            {
                PlayerPrefs.SetInt("GRAPHICS_LEVEL", 0);//QualitySettings.GetQualityLevel());   // RESET TO 0 everytime i guess but gotta test this.
            }
            return PlayerPrefs.GetInt("GRAPHICS_LEVEL");

        }
        set
        {
            PlayerPrefs.SetInt("GRAPHICS_LEVEL", value);// value);
        }
    }

    /// <summary>
    /// If empty : Demo, if not empty: isnt Demo
    /// </summary>
    public string CRED_ISDEMO
    {
        get
        {
            return PlayerPrefs.GetString("CRED_ISDEMO");
        }
        set
        {
            PlayerPrefs.SetString("CRED_ISDEMO", value);
        }
    }

    public string CURRENT_ORG;
    public string CURRENT_RETAIL;
    public string CURRENT_ROOM_ID;
    public string CURRENT_ROOM_NAME;
    public string CURRENT_VERSION_ID;
    public string CURRENT_CHALLENGE_ID;

    public async Task<IApiStatus> DemoLogin()
    {
        CRED_USEREMAIL = demoUserEmail;
        CRED_PASSWORD = demoPassword;

        return await Login(true);
    }


    public async Task<IApiStatus> Login(string email, string password)
    {
        CRED_USEREMAIL = email;
        CRED_PASSWORD = password;

        return await Connect(true);
    }


    public async Task<IApiStatus> Connect(bool forceLogin = false)
    {
        server.JWT = CRED_JWTTOKEN ?? string.Empty;

        var res = await server.auth.CheckServer();

        if (res == null)
        {
            return new IApiStatus { err = true, data = "No Internet Connectivity" };
        }
        if (forceLogin | CRED_ISDEMO != string.Empty)
            return await Login();
        else
            return await DemoLogin();//new IApiStatus { err = false, data = "Connected to server, demo Login" };

    }

    public async Task<IApiStatus> Login(bool isDemo = false)
    {

        var resAuth = await server.auth.CheckAuth();
        var apiStat = new IApiStatus { err = true, data = "Authentication Error. Please Relogin." };
        if (!resAuth)
        {
            if (CRED_AUTHPROVIDER != String.Empty && CRED_AUTHPROVIDER != Homefuly.AuthType.homefuly.ToString())
            {
                apiStat.data = CRED_AUTHPROVIDER;
                return apiStat;
            }
            if (CRED_USEREMAIL == string.Empty || CRED_PASSWORD == string.Empty || (CRED_ISDEMO != string.Empty && CRED_AUTHPROVIDER == string.Empty))
            {
                return apiStat;
            }

            var resAuto = await AutoLogin();

            if (!resAuto)
            {
                return apiStat;
            }

        }

        apiStat.err = false;
        apiStat.data = "Connected to Server";
        if(!isDemo) CRED_ISDEMO = "Not Demo";
        if (!isDemo) CRED_AUTHPROVIDER = Homefuly.AuthType.homefuly.ToString();
        return apiStat;
    }

    public async Task<IApiStatus> Signup(string fullName, string email, string password)
    {
        var names = fullName.Split(' ');
        Debug.Log(names[0] + ", " + names.Length);
        string firstName = names[0];
        string lastName = names.Length > 1 ? names[1] : "";
        
        IUserRegister req = new IUserRegister { firstName = firstName, lastName = lastName, email = email, password = password, role= "customer" };
        Debug.Log("Sign Up Request: " + req.firstName + ", " + req.email + ", " +  req.password);
        var res = await auth.Signup(req);

        if (res != null)
        {
            Debug.Log("Sign up success");

            CRED_JWTTOKEN = server.JWT = res.token;
            CRED_USERID = res.user._id;
            CRED_USEREMAIL = res.user.email;
            CRED_ROLE = res.user.role;
            CRED_ISCUSTOMER = res.user.role == "customer" ? 1 : 0;
            CRED_AUTHPROVIDER = Homefuly.AuthType.homefuly.ToString();
            CRED_ISDEMO = "Not Demo";

            Debug.Log("user ID: " + CRED_USERID);

            // add subscriber to organization list (pending)
            var resSub = await auth.AddCustomer(new ISubscriber{user = res.user._id, orgrole = ""});
            Debug.Log("resSub: " + resSub);
            if(resSub == null) 
                Debug.Log("Failed to add customer to subscriber list");
            
            return new IApiStatus{err=false, data=null};
        }
        else
        {
            Debug.Log("Sign up fail");
            CRED_PASSWORD = string.Empty;
            CRED_USERID = string.Empty;
            CRED_USEREMAIL = string.Empty;
            CRED_JWTTOKEN = string.Empty;
            CRED_ISDEMO = string.Empty;
            return new IApiStatus{err=true, data="Error occured during sign up"};
        }
    }

    public async Task<bool> AutoLogin()
    {
        IUserAuth req = new IUserAuth { email = CRED_USEREMAIL, password = CRED_PASSWORD };
        Debug.Log("In auto login " + CRED_USEREMAIL);
        var res = await auth.Login(req);
        //Debug.Log("Auto login response " + res.ToString());
        if (res != null)
        {
            Debug.Log("auto Loggin success");
            CRED_JWTTOKEN = server.JWT = res.token;
            CRED_USERID = res.user._id;
            CRED_ROLE = res.user.role;
            CRED_ISCUSTOMER = res.user.role == "customer" ? 1 : 0;
            return true;
        }
        else
        {
            Debug.Log("auto Loggin fail");
            CRED_PASSWORD = string.Empty;
            CRED_USERID = string.Empty;
            CRED_JWTTOKEN = string.Empty;
            CRED_ISDEMO = string.Empty;
            return false;

        }
    }

    /// <summary>
    /// Authentication performed using the user access token obtained from google or facebook
    /// Also sets CRED_PASSWORD to empty
    /// </summary>
    /// <param name="tokenProvider"></param>
    /// <param name="accessToken"></param>
    /// <returns></returns>
    public async Task<bool> OAuthLogin(string tokenProvider, string accessToken)
    {
        IUserTokenAuth oauth = new IUserTokenAuth { provider = tokenProvider, token = accessToken };
        var res = await auth.TokenLogin(oauth);
        Debug.Log("Access Token is " + accessToken);
        if (res != null)
        {
            Debug.Log("OAuth Login success");
            CRED_JWTTOKEN = server.JWT = res.token;
            CRED_USERID = res.user._id;
            CRED_USEREMAIL = res.user.email;
            CRED_USERNAME = res.user.name;
            CRED_ISDEMO = res.user.email == demoUserEmail ? string.Empty : "not demo";
            CRED_ROLE = res.user.role;
            CRED_ISCUSTOMER = res.user.role == "customer" ? 1 : 0;
            CRED_PASSWORD = string.Empty;
            CRED_AUTHPROVIDER = tokenProvider;
            return true;
        }
        else
        {
            Debug.Log("OAuth Loggin fail");
            CRED_PASSWORD = string.Empty;
            CRED_USERID = string.Empty;
            CRED_JWTTOKEN = string.Empty;
            CRED_ISDEMO = string.Empty;
            CRED_USERNAME = string.Empty;
            CRED_USEREMAIL = string.Empty;
            CRED_AUTHPROVIDER = string.Empty;
            return false;

        }
    }

    // Login from webuiy
    public void UpdateLoginInfo(IABAppLogin res)
    {
        CRED_JWTTOKEN = server.JWT = res.token;
        CRED_USERID = res.userid;
        CRED_USEREMAIL = res.useremail;
        CRED_PASSWORD = res.password;
        CRED_USERNAME = res.name;
        CRED_ISDEMO = res.useremail == demoUserEmail ? string.Empty : "not demo";
        CRED_ROLE = res.role;
        CRED_ISCUSTOMER = res.role == "customer" ? 1 : 0;
        CRED_AUTHPROVIDER = res.authProvider;
    }

    // Login from webuiy
    public void UpdateLoginCred(IOLogin res)
    {
        CRED_JWTTOKEN = server.JWT = res.token;
        CRED_USERID = res.userid;
        CRED_USEREMAIL = res.useremail;
        CRED_PASSWORD = res.password;
        CRED_USERNAME = res.name;
        CRED_ISDEMO = res.useremail == demoUserEmail ? string.Empty : "not demo";
        CRED_ROLE = res.role;
        CRED_ISCUSTOMER = res.role == "customer" ? 1 : 0;
        CRED_AUTHPROVIDER = res.authProvider;
    }

    public async Task Logout()
    {
        var res = await auth.Logout();

        CRED_PASSWORD = string.Empty;
        CRED_USERID = string.Empty;
        CRED_USERNAME = string.Empty;
        CRED_JWTTOKEN = string.Empty;
        CRED_ISDEMO = string.Empty;
        CRED_AUTHPROVIDER = string.Empty;
    }

}

