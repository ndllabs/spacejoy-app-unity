﻿using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

public static class AppConfig
{
    public static class GameConfig
    {
        public const int DefaultCoins = 500;
        public const int DefaultWallfinishPrice = 30;
        public static bool nextChallenge = false;

        /// <summary>
        /// If Creative Mode is enabled, All the 3D content will be abailable to the user free of costs.
        /// </summary>
        public static bool creativeMode { get; } = true;
    }

    public static class PilotDemoConfig
    {
        public const string OrgId = "5b068f62714382439d32fa04";

        //public const string ProjectId = "5bed205881c0eb54a88ffe09";
        //public const string ProjectId = "5b0694bbbb96c34910af170f";

        public static string ProjectId = "";

        public const string DefaultSegment = ""; //"5bfd44f8bf05724c0cedc6bb";

        public const string RetailerFurniture = ""; //"5bed533e81c0eb54a88fffd4";
        public const string RetailerPaints = ""; //"5bed535981c0eb54a88fffd5";
        public const string RetailerWallpapers = ""; //"5bed535981c0eb54a88fffd5";


        public const string MinimapImage = ""; //"5b068f62714382439d32fa04";

        public const string AssetCategory = ""; //"5ba23435e852e80dad11b653";
        public const string AssetSubCategory = ""; //"5bed54ef81c0eb54a88fffd6";
        public const string AssetVertical = ""; //"5bed550181c0eb54a88fffd7";

        public const string CenterId = "";//"5bed5dc981c0eb54a88ffff9";

        public const string TEMP_EMAIL = ""; //"vikz91.deb@gmail.com";
        public const string TEMP_PWD = ""; //"19@Kaka91";

        public const string NonShoppableCatId = "5ba23435e852e80dad11b653";

    }


    public static class ServerConfig
    {
        #region Internal, Donot Edit
        private const string _host = "https://api.homefuly.com";
        private const string _betaHost = "https://api.beta.homefuly.com";

        private const string _webUIHost = "https://web.homefuly.com";
        private const string _betaWebUIHost = "https://homefuly-staging-web.firebaseapp.com";

        // old ionic site
        private const string _webUIHostIonic = "https://www.homefuly.com";
        #endregion

        public const int basicTimeout = 60;
        public const int extendedTimeout = 1800;

        public static string host
        {
            get
            {
                if (!Debug.isDebugBuild)
                    return _host;
                else
                {
                    Debug.LogWarning("[AppConfig][ServerConfig] CURRENT is a debug session and yet using the main server since beta is offline");
                    return _host;
                }
            }
        }

        public static string webUIHost
        {
            get
            {
                if (!Debug.isDebugBuild)
                    return _webUIHost;
                else
                {
                    //Debug.LogWarning("Currently No Beta Site Availabe for Web UI..");
                    return _betaWebUIHost;
                }
            }
        }
    }

    public static class StorageConfig
    {
        public static string DATA_PATH = UnityEngine.Application.persistentDataPath;
        public static string TEMP_PATH = Path.Combine(DATA_PATH, "TEMP");
        public static string ASSETS_PATH = Path.Combine(DATA_PATH, "Assets");
        public static string ROOMS_PATH = Path.Combine(DATA_PATH, "Rooms");
    }

    // Defined separate public projects for storing user saved designs, user own rooms and challenge designs
    // DO NOT CHANGE
    public static class ProjectConfig
    {
        // Design Challenges roomversions
        public static string PROJ_CHALLENGE = "5cd2b5fcb422813ee34d926f"; 
        
        // User own rooms
        public static string PROJ_USER_ROOM = "5cd045c280cd4c7da58871a6";

        // User saved designs <Old App>
        public static string PROJ_USER_SAVED_OLD = "5cb5b29a4c6813333fef7610";
    }
}




public enum AppPage
{
    //Preload_Deprycated,
    //Auth,
    //Center,
    Designer,
    //Test,
    DesignChallenge,
    //YourDesign_Deprycated,
    Home,
    AppComponent
}

