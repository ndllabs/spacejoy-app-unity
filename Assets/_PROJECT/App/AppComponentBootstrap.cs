﻿using Homefuly;
using Homefuly.Database;
// using Homefuly.WebView;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AppComponentBootstrap : MonoBehaviour
{
    [HideInInspector]
    public AppComponent app;

    [Header("Feedback")]
    public GameObject GFeedback;


    [Header("Debug")]
    public TextMeshProUGUI dbReady;
    public GameObject pnlDebug;
    public string quickRoom = ""; //"5bed5dc981c0eb54a88ffff9";


    [Header("Web View")]
    public GameObject prefWebView;
    public RectTransform referenceRect;
    [HideInInspector]
    // public WebViewService webViewService;

    private void Awake()
    {

        if (Debug.isDebugBuild)
        {
            pnlDebug.SetActive(true);
            foreach (var butt in pnlDebug.GetComponentsInChildren<Button>()) { butt.interactable = false; }
        }
        else
            pnlDebug.SetActive(false);


        DontDestroyOnLoad(GFeedback);
        HomefulyExtensions.SetScreenOrientation(false);
        app = AppComponent.Instance;
        // webViewService = WebViewService.Instance;
    }
    // Start is called before the first frame update
    void Start()
    {
        //DO NOT SLEEP
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        app.Init();
        // webViewService.Init(prefWebView);
        AnalyticsManager.Instance.Init();
        // NotificationMain.Instance.Init();

#if !UNITY_EDITOR
        Debug.Log("[" + nameof(AnalyticsManager) + "] online.!");
#else
        Debug.LogWarning("[" + nameof(AnalyticsManager) + "] Currenlty Running in editor, analytics will go offline");
#endif

        if (!string.IsNullOrEmpty(quickRoom))
        {
            app.account.CURRENT_ROOM_ID = quickRoom;
            app.account.CURRENT_ROOM_NAME = "Demo";
        }
        else
        {
            app.account.CURRENT_ROOM_ID = string.Empty;
            app.account.CURRENT_ROOM_NAME = string.Empty;
        }

        Screen.orientation = ScreenOrientation.Portrait;

    }

    private void OnEnable()
    {
        // AppDatabase.OnDBReady += OnDBReady;
    }

    private void OnDisable()
    {
        // AppDatabase.OnDBReady -= OnDBReady;
    }

    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                OnClickDesignChallenge();
            }

            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                //OnClickCenter();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                OnClickUserRoom();
            }
        }


#endif
    }

    private void OnDBReady()
    {
        dbReady.fontSize = 24;
        dbReady.alignment = TextAlignmentOptions.Left;
        dbReady.text = "Database: Ready\n" +
                       "Analytics: " + (AnalyticsManager.IsAnalyticsOffline() ? "OFFLINE" : "ONLINE") + "\n" +
                       "User: " + app.account.CRED_USEREMAIL + "\n" +
                       "Current Coins: " + app.db.userSavedData.currentCoins + "\n" +
                       "Owned Assets: " + app.db.userSavedData.assetsOwned.Count + "\n" +
                       "Owned Paints: " + app.db.userSavedData.paintOwned.Count;
        foreach (var butt in pnlDebug.GetComponentsInChildren<Button>()) { butt.interactable = true; }
    }

    //public void OnClickCenter()
    //{
    //    Debug.Log("Loading Center");
    //    app.Navigate(AppPage.Center);

    //}

    public void OnClickAuth()
    {
        Debug.Log("Loading Auth Panel");

        WidgetManager.Instance.AuthPanelOpen((bool value) =>
        {
            if (value)
            {
                /* Warning: AppDatabase requires Auth and a Internet connection to Init
                 * The Server sync will happen in background but the function will return immediately
                * */
                app.db = new AppDatabase();
                app.db.Init();
            }
            else
            {
                WidgetManager.Instance.Alert("Login Failed", "Please Login To start your journey", () => { });
            }
        });
    }



    public void OnClickDesignChallenge()
    {
        Debug.Log("Loading DesignChallenge");
        app.account.CURRENT_CHALLENGE_ID = "5d36d74b4c13ea2c8d4ba923";
        // app.account.CURRENT_CHALLENGE_ID = "5cf265e72cd2601a1e38d0ee"; 
        AppConfig.PilotDemoConfig.ProjectId = AppConfig.ProjectConfig.PROJ_CHALLENGE;
        app.Navigate(AppPage.DesignChallenge);
    }

    public void OnClickUserRoom()
    {
        Debug.Log("Loading UserRoom");
        app.CurrentSceneType = SceneType.UserRoom;
        // app.CurrentSceneType = SceneType.ReadOnlyDesign;
        // AppConfig.PilotDemoConfig.ProjectId =  "5cd045c280cd4c7da58871a6"; //"5caf7eda4c6813333fef70e2";
        // app.account.CURRENT_ROOM_ID = "5ce9a0482cd2601a1e38241c";//"5cd0465b80cd4c7da5887279";// "5cd0463c80cd4c7da58871ab"; //"5caf7f554c6813333fef70e6";
        // app.account.CURRENT_VERSION_ID = "5ce9a04b2cd2601a1e38241d";//"5cd5d2788b4e3d04a4e3e097";//"5cd5a8438b4e3d04a4e3da15";//"5caf7f574c6813333fef70e7";

        AppConfig.PilotDemoConfig.ProjectId = "5c2de39dfadece04e4242674"; //"5caf7eda4c6813333fef70e2";
        app.account.CURRENT_ROOM_ID = "5cf52d32610d0969eddf0e56";//"5cd0465b80cd4c7da5887279";// "5cd0463c80cd4c7da58871ab"; //"5caf7f554c6813333fef70e6";
        app.account.CURRENT_VERSION_ID = "5cf669723dcfbf10ac56a624";//"5cd5d2788b4e3d04a4e3e097";//"5cd5a8438b4e3d04a4e3da15";//"5caf7f574c6813333fef70e7";

        Debug.Log("User Room: " + AppConfig.PilotDemoConfig.ProjectId + ", " + app.account.CURRENT_ROOM_ID + ", " + app.account.CURRENT_VERSION_ID);

        app.Navigate(AppPage.Designer);
    }

    public void OnClickHome()
    {
        app.Navigate(AppPage.Home);
    }

    public void OnClickOldSchool()
    {
        Debug.Log("Loading OnClickOldSchool");
        app.CurrentSceneType = SceneType.OldSchool;
        // app.CurrentSceneType = SceneType.ReadOnlyDesign;
        AppConfig.PilotDemoConfig.ProjectId = "5cd2b5fcb422813ee34d926f"; //"5caf7eda4c6813333fef70e2";
        app.account.CURRENT_ROOM_ID = "5cd2b90cb422813ee34d9323"; //"5caf7f554c6813333fef70e6";
        app.account.CURRENT_VERSION_ID = "5cd2b910b422813ee34d9324";//"5caf7f574c6813333fef70e7";


        app.Navigate(AppPage.Designer);
    }

    public void OnClickReadOnly()
    {
        Debug.Log("Loading OnClickReadOnly");
        app.CurrentSceneType = SceneType.ReadOnlyDesign;
        AppConfig.PilotDemoConfig.ProjectId = "5cd2b5fcb422813ee34d926f"; //"5caf7eda4c6813333fef70e2";
        app.account.CURRENT_ROOM_ID = "5cd2b90cb422813ee34d9323"; //"5caf7f554c6813333fef70e6";
        app.account.CURRENT_VERSION_ID = "5cd2b910b422813ee34d9324";//"5caf7f574c6813333fef70e7";

        app.Navigate(AppPage.Designer);
    }

    public void OnClickCreateRoom()
    {
        Debug.Log("Loading OnClickReadOnly");
        app.CurrentSceneType = SceneType.UserRoom;
        AppConfig.PilotDemoConfig.ProjectId = AppConfig.ProjectConfig.PROJ_USER_ROOM;
        app.account.CURRENT_ROOM_ID = "5cd0463c80cd4c7da58871ab"; //"5caf7f554c6813333fef70e6";
        app.account.CURRENT_VERSION_ID = "5cd0463f80cd4c7da58871ac";//"5caf7f574c6813333fef70e7";
        // CreateRoom();
        app.Navigate(AppPage.Designer);
    }

    //   public async void CreateRoom()
    // {
    //     // get current design
    //     // WidgetManager.Instance.LoaderMini("Creating your room... ", 4);
    //     var templateDesign = await app.server.artifact.GetRoomDesignAsync(app.account.CURRENT_ROOM_ID, app.account.CURRENT_VERSION_ID);
    //     templateDesign.project = AppConfig.PilotDemoConfig.ProjectId;
    //     templateDesign.customer = app.account.CRED_USERID;

    //     Debug.Log(templateDesign.project + ", " + templateDesign.customer);

    //     var userCreatedDesign = await app.server.artifact.CreateNewRoomVersion(templateDesign);
    //     app.account.CURRENT_VERSION_ID = userCreatedDesign._id;
    //     app.account.CURRENT_ROOM_ID = userCreatedDesign.room;

    //     Debug.Log("New Design: " + AppConfig.PilotDemoConfig.ProjectId + ", " + app.account.CURRENT_ROOM_ID + ", " + app.account.CURRENT_VERSION_ID);
    //     // WidgetManager.Instance.LoaderMiniClose();
    //     app.Navigate(AppPage.Designer);
    // }
}
