﻿using UnityEngine;

namespace TouchMan
{
    public class TouchManTouch
    {
        public int fingerId;
        public Vector2 position;
        public Vector2 deltaPosition;
        public float deltaTime;
        public TouchPhase phase;

        public TouchManTouch(Touch touch)
        {
            this.fingerId = touch.fingerId;
            this.position = touch.position;
            this.deltaPosition = touch.deltaPosition;
            this.deltaTime = touch.deltaTime;
            this.phase = touch.phase;
        }

        public TouchManTouch(){ }
    }
}
