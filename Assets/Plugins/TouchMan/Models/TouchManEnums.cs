﻿
namespace TouchMan
{
    public enum GestureType
    {
        None,
        Tap,
        DoubleTap,
        LongPress,
        Drag,
        TwoFingerPan,
        PinchOrSpread,
        Rotate
    }

    public enum TState
    {
        Started_1F,
        Moving_1F,
        Stationary_1F,
        Ended_1F,
        Canceled_1F,
        Started_2F,
        Moving_2F,
        OverUI,
        StartedOverUI,
        NoTouch
    }

    public enum GameState
    {
        Camera,
        AssetSelected
    }
    
    public enum dbgType
    {
        msg,
        log,
        err
    }


    public enum MoveMode
    {
        GroundAttached,
        Free,
        WallAttached,
        AssetAttached,
        CeilingAttached
    }

    public enum ObjectMovementUpdate
    {
        BeginMove,
        Moving,
        EndMove,
        BeginRotate,
        Rotating,
        EndRotating
    }
}
