﻿namespace TouchMan
{
    public class TouchManGesture
    {
        public GestureType gesture;
        public TouchManTouch[] touchs;
        public float distanceBwTouch;

        public TouchManGesture(GestureType gesture, params TouchManTouch[] touchs)
        {
            this.gesture = gesture;
            this.touchs = touchs;
        }
    }
}
