﻿/* Touchman, 
 * Update the Layers for better efficiency
 * 
 * prajwalshetty2018@gmail.com
 * */

using System;
using UnityEngine;

namespace TouchMan
{
    [Serializable]
    public class TouchManLayers
    {
        /// <summary>
        /// DefaultLayer
        /// </summary>
        [HideInInspector]
        public string DefaultLayer = "Default";
        
        /// <summary>
        /// Layer Of the Current Character Being Played, Important to seperate out the players own collider
        /// </summary>
        public string PlayerLayer = "Player";

        /// <summary>
        /// List Of layers that can be considered as ground while moving a ground object
        /// </summary>
        public string[] GroundLayerList;

        /// <summary>
        /// List Of layers that can be considered as ceiling while moving a ground object
        /// </summary>
        public string[] CeilingLayerList;

        /// <summary>
        /// List Of layers that can be considered as Walls while moving a Wall Mounter object
        /// </summary>
        public string[] WallLayerList;

        /// <summary>
        /// List Of layers that can be considered as AttachableAsset while moving a groundButFree object
        /// </summary>
        public string[] AttachableAssetsLayerList;

        /// <summary>
        /// List Of layers that can be considered as AttachableAsset while moving a groundButFree object
        /// </summary>
        public string[] SelectableObjectsLayerList;

        /// <summary>
        /// This layer will have the highest priority when a tap happens even when the GO is ocluded by other objects
        /// </summary>
        public string HighPrioritySelectableLayer;

        [SerializeField]
        [Tooltip("If Provided, TouchMan will look for parents and objects only here instead of the whole scene for search optimization")]
        /// <summary>
        /// Advanced Setting For Players
        /// </summary>
        public Transform[] ParentObjectsContainers;
        
        /// <summary>
        /// Warning: represnets the maximum allowed objects in non-physics collision check, please donot alter this value except for debugging and profiling
        /// </summary>
        public int ObjectsBudget = 30;

        #region GlobalSettings
        public static string _DefaultLayer = "Default";
        public static string _PlayerLayer;
        public static string[] _GroundLayerList;
        public static string[] _CeilingLayerList;
        public static string[] _WallLayerList;
        public static string[] _AttachableAssetsLayerList;
        public static string[] _SelectableObjectsLayerList;
        public static string _HighPrioritySelectableLayer;
        public static Transform[] _ParentObjectsContainers;
        public static int _ObjectsBudget;
        public static bool isUpdated = false;
        public void UpdateGlobalSettings()
        {
            _DefaultLayer = DefaultLayer;
            _GroundLayerList = GroundLayerList;
            _CeilingLayerList = CeilingLayerList;
            _WallLayerList = WallLayerList;
            _AttachableAssetsLayerList = AttachableAssetsLayerList;
            _SelectableObjectsLayerList = SelectableObjectsLayerList;
            _HighPrioritySelectableLayer = HighPrioritySelectableLayer;
            _ParentObjectsContainers = ParentObjectsContainers;
            _ObjectsBudget = ObjectsBudget;
            _PlayerLayer = PlayerLayer;
            isUpdated = true;
        }
        #endregion

    }
}
