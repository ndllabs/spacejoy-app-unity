﻿/* Supported Touch Gestures
 * 
 * Can Tap on things
 * Longpress on Things
 * 
 * Normal Mode:
 * Swipe up/down: Move Camera Forward/Backward
 * Swipe right/left: Camera Rotate in y
 * Pinch: Move Camera Forward/Backward
 * Two Finger Swipe Right/Left: move camera Right/Left
 * Two Finger Swipe Up/Down: Rotate Camera in x Axis
 * 
 * Asset Mode:
 * Swipe: Move asset in its respective plane
 * Tap anywhere: deselect asset
 * Pinch: Move Camera Forward/Backward
 * Two Finger Swirl: Rotate Asset
 * 
 * prajwalshetty2018@gmail.com
 * */

using System;
using UnityEngine;

namespace TouchMan
{
    [Serializable]
    public static class TouchManEvents
    {
        /// <summary>
        /// When Pinch
        /// </summary>
        /// <param name="TouchDistanceDelta"></param>
        /// <param name="touch0"></param>
        /// <param name="touch1"></param>
        public delegate void TouchManPinch(float TouchDistanceDelta, Touch touch0, Touch touch1);
        public static TouchManPinch OnPinch;

        /// <summary>
        /// When Two fingers being panned
        /// </summary>
        /// <param name="touch0"></param>
        /// <param name="touch1"></param>
        public delegate void TouchManPan(Touch touch0, Touch touch1);
        public static TouchManPan OnTwoFingerPan;

        /// <summary>
        /// WhenTwoFingersNotMoving
        /// </summary>
        public delegate void TouchManTwoFingerIdle();
        public static TouchManTwoFingerIdle OnTwoFingerStationary;

        /// <summary>
        /// WhenTwoFingersNotMoving
        /// </summary>
        public delegate void TouchManOneFingerIdle();
        public static TouchManOneFingerIdle OnOneFingerStationary;

        /// <summary>
        /// When OneFinger/TwoFinger Touch Ended
        /// </summary>
        public delegate void TouchManEndTouch();
        public static TouchManEndTouch OnEndTouch;

        /// <summary>
        /// When User Does a One Finger Drag in the Scene
        /// </summary>
        /// <param name="dragStartedOver">Gameobject over which drag started</param>
        /// <param name="touchPosition"></param>
        /// <param name="touchDeltaPosition"></param>
        public delegate void TouchManDrag(GameObject dragStartedOver, Vector2 touchPosition, Vector2 touchDeltaPosition);
        public static TouchManDrag OnDrag;
        //public static Action<GameObject, Vector2, Vector2> OnDrag;

        /// <summary>
        /// When User Starts a One Finger Drag in the Scene
        /// </summary>
        /// <param name="dragStartedOver">Gameobject over which drag started</param>
        /// <param name="touchPosition"></param>
        public delegate void TouchManBeginDrag(GameObject dragStartedOver, Vector2 touchPosition);
        public static TouchManBeginDrag OnBeginDrag;

        /// <summary>
        /// When User Ends a One Finger Drag in the Scene
        /// </summary>
        public delegate void TouchManEndDrag();
        public static TouchManEndDrag OnEndDrag;

        /// <summary>
        /// When a gameobject is tapped for a short period of time
        /// </summary>
        /// <param name="tappedObject"></param>
        public delegate void TouchManTap(GameObject tappedObject);
        public static TouchManTap OnTap;

        /// <summary>
        /// When a gameobject is longPressed, 
        /// </summary>
        /// <param name="longPressedObject">Will be null if no Gameobject Exists</param>
        public delegate void TouchManLongPress(GameObject longPressedObject);
        public static TouchManLongPress OnLongPress;
    }
}
