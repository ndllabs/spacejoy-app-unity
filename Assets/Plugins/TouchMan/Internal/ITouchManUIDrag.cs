﻿using UnityEngine;

namespace TouchMan
{
    public interface ITouchManUI
    {
        void OnUIDraggedTowardsWorld();
        void OnUISwiped();
    }
}
