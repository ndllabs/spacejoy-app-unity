﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TouchMan
{
    using Ts = TouchManSettings;

    public static class TouchManDebug
    {
        public const string e06 = "";

        public const string e07 = "";

        public const string e08 = "";

        public const string e09 = "";

        public const string e10 = "";

        public const string e11 = "";

        public static TextMeshProUGUI msgBar = null;
        public static TextMeshProUGUI logBar = null;
        public static TextMeshProUGUI errBar = null;

        public static void Log(string msg, dbgType st = dbgType.log)
        {
            if (!Ts._debugMode)
            {
                if (msgBar) Object.Destroy(msgBar);
                if (logBar) Object.Destroy(logBar);
                return;
            }

            Debug.Log(msg);

            if (!msgBar)
                CreateMessageBar(dbgType.msg);
            if (!logBar)
                CreateMessageBar(dbgType.log);
            
            if(st == dbgType.err)
                CreateMessageBar(dbgType.err);

            if (st == dbgType.log)
                logBar.text = msg;
            else
                msgBar.text = msg;
        }

        static void CreateMessageBar(dbgType tct)
        {
            GameObject go = new GameObject(tct.ToString() + "_box");
            go.layer = 5; //UI

            GameObject can = GameObject.Find("Canvas");

            if (!can)
                Debug.LogWarning("[TouchManError] No Canvas Exists..!");
            else
                go.transform.SetParent(can.transform);

            TextMeshProUGUI txt = go.AddComponent<TextMeshProUGUI>();
            RectTransform rect = go.GetComponent<RectTransform>();
            
            rect.anchorMax = new Vector2(1, 1);
            rect.anchorMin = new Vector2(0, 1);
            rect.pivot = new Vector2(0.5f,0.5f);
            rect.sizeDelta = new Vector2(0, 70);
            rect.anchoredPosition = (tct == dbgType.msg) ? new Vector2(20, -35) : (tct == dbgType.log) ? new Vector2(20, -105): new Vector2(20, -175);
            
            txt.fontSize = (tct == dbgType.msg) ? 35 : 25;
            txt.alignment = TextAlignmentOptions.MidlineLeft;

            //txt.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            //txt.font = Resources.Load("OpenSans-RegularRebuilt SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
            //txt.fontMaterial = Resources.Load<TMP_FontAsset>("rubik/Rubik_Regular_SDF").material;
            //txt.font = Resources.Load<TMP_FontAsset>("rubik/Rubik_Regular_SDF"); //Resources.Load("rubik/Rubik_Regular_SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;

            txt.raycastTarget = false;

            switch (tct)
            {
                case dbgType.msg:
                    txt.fontStyle = FontStyles.Bold;
                    msgBar = txt;
                    break;
                case dbgType.log:
                    logBar = txt;
                    break;
                case dbgType.err:
                    errBar = txt;
                    break;
            }
        }

    }


}

