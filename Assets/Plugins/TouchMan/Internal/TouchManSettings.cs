﻿/* Touchman, 
 * Changes will effect the global touchman settings
 * 
 * prajwalshetty2018@gmail.com
 * */


using System;
using UnityEngine;

namespace TouchMan
{
    [Serializable]
    public class TouchManSettings
    {
        [Header("Speed Modifiers")]
        /// <summary>
        /// Speed Modifier, Keyboard
        /// </summary>
        public Vector2 speedModiferKeyboard = new Vector2(1, 2);

        /// <summary>
        /// Speed Modifier, Mouse
        /// </summary>
        public Vector2 speedModiferMouse = new Vector2(1f, 2f);

        /// <summary>
        /// Speed Modifier, Touch
        /// </summary>
        public Vector2 speedModiferTouch = new Vector2(0.06f, 0.1f);

        [Header("Touch Restrictions")]
        /// <summary>
        /// Maximum seconds user can use to lift the finger back up to consider it as a tap.
        /// </summary>
        public float maxTapTime = 0.2f;
        
        /// <summary>
        /// Maximum seconds user can use to lift the finger back up to consider it as a press.
        /// </summary>
        public float maxPressTime = 0.3f;

        /// <summary>
        /// Maximum seconds user can use to lift the finger back up to consider it as a Longpress.
        /// </summary>
        public float maxLongPressTime = 0.6f;

        /// <summary>
        /// Maximum Movement user can Do Before lifting the finger back up to consider it as a tap.
        /// </summary>
        public Vector2 maxTapMovement = new Vector2(5, 5);

        /// <summary>
        /// Minimum Pan user has to do to consider it as a Pan.
        /// </summary>
        public Vector2 minPanDistance = new Vector2(35f, 35f);
        
        /// <summary>
        /// Minimum Total Pan user has to do to consider it as a Pan.
        /// </summary>
        public Vector2 minTotalPanDistance = new Vector2(125f, 100f);

        /// <summary>
        /// Minimum Pinch user has to do to consider it as a Pinch.
        /// </summary>
        public float minPinchDistance = 35f;

        /// <summary>
        /// Minimum Total Pinch user has to do to consider it as a Pinch.
        /// </summary>
        public float minTotalPinchDistance = 100f;

        /// <summary>
        /// Minimum Turn/Swirl user has to do to consider it as a Swirl.
        /// </summary>
        public float minTurnAngle = 0.5f;

        [SerializeField]
        [Header("Advanced Settings")]
        [Tooltip("Advanced Settings")]
        /// <summary>
        /// Advanced Setting For Players
        /// </summary>
        public TouchSettingsAdvanced advanced;


        #region GlobalSettings
        public static Vector2 _speedModiferKeyboard;
        public static Vector2 _speedModiferMouse;
        public static Vector2 _speedModiferTouch;//new Vector2(0.06f, 0.1f);
        public static float _maxTapTime;
        public static float _maxLongPressTime;
        public static Vector2 _maxTapMovement;
        public static Vector2 _minPanDistance;
        public static Vector2 _minTotalPanDistance;
        public static float _minPinchDistance;
        public static float _minTotalPinchDistance;
        public static float _minTurnAngle;
        public static TouchSettingsAdvanced _advanced = new TouchSettingsAdvanced();
        public static Camera _cam;
        public static bool _debugMode = true;
        public void UpdateGlobalSettings()
        {
            _speedModiferKeyboard = speedModiferKeyboard;
            _speedModiferMouse = speedModiferMouse;
            _speedModiferTouch = speedModiferTouch;
            _maxTapTime = maxTapTime;
            _maxLongPressTime = maxLongPressTime;
            _maxTapMovement = maxTapMovement;
            _minPanDistance = minPanDistance;
            _minTotalPanDistance = minTotalPanDistance;
            _minPinchDistance = minPinchDistance;
            _minTotalPinchDistance = minTotalPinchDistance;
            _minTurnAngle = minTurnAngle;
            _advanced = advanced;
        }
    }


    #endregion

    [Serializable]
    public class TouchSettingsAdvanced
    {
        /// <summary>
        /// MovementSpeed Multiplier
        /// </summary>
        public Vector2 moveSpeed = new Vector2(35,35);


        /// <summary>
        /// 2Finger Multiplier to Sync with one Finger Gestures
        /// </summary>
        public float MultiplyerTwoFingerGestures = 3f;


        /// <summary>
        /// 2Finger Multiplier to Sync with one Finger Gestures
        /// </summary>
        public float MaxRaycastHitDistance = 30f;

        /// <summary>
        /// If you want to consider a bound is submerged insider another, minimum of how much percentile the bound should be inside the parent
        /// </summary>
        public float ToBeConsderedAsSubmerged = 49f;

        /// <summary>
        /// Multiplied to target Direction when an object is being dragged
        /// </summary>
        public float VelocityMultiplier = 70f;

        /// <summary>
        /// 0 being Max Delay, 1 Being Minimum Delay
        /// </summary>
        public float MovementVelocityDelay = 0.85f;
    }


}
