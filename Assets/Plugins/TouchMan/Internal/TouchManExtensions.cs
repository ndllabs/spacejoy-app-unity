﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TouchMan
{
    public static class TouchManExtensions
    {
        public static bool isPointerOverUI_Deprycated()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current);

            if (Input.touchSupported && Input.touchCount > 0)
                eventDataCurrentPosition.position = Input.GetTouch(0).position;
            else
                eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            List<RaycastResult> tempRaycastResults = new List<RaycastResult>();
            tempRaycastResults.Clear();

            EventSystem.current.RaycastAll(eventDataCurrentPosition, tempRaycastResults);

            return tempRaycastResults.Count > 0;
        }

        public static bool IsSmallerThan_ABS(this Vector2 vectorA, Vector2 vectorB)
        {
            return (Math.Abs(vectorA.x) < Math.Abs(vectorB.x)) && (Math.Abs(vectorA.y) < Math.Abs(vectorB.y));
        }


        public static bool IsSmallerThan_ABS(this Vector3 vectorA, Vector3 vectorB)
        {
            return (Math.Abs(vectorA.x) < Math.Abs(vectorB.x)) && (Math.Abs(vectorA.y) < Math.Abs(vectorB.y)) && (Math.Abs(vectorA.z) < Math.Abs(vectorB.z));
        }


        public static bool IsSmallerThan(this Vector2 vectorA, Vector2 vectorB)
        {
            return (vectorA.x < vectorB.x) && (vectorA.y < vectorB.y);
        }

        public static T[] Concat<T>(this T[] x, T[] y)
        {
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");
            int oldLen = x.Length;
            Array.Resize<T>(ref x, x.Length + y.Length);
            Array.Copy(y, 0, x, oldLen, y.Length);
            return x;
        }

        public static TouchManTouch ToTouchManTouch_Deprycated(this Touch touch)
        {
            UnityEngine.Debug.LogWarning("[Deprycation Warning] ToTouchManTouch_Deprycated, Use TouchManTouch class Constructor instead");

            TouchManTouch tm_Touch = new TouchManTouch();
            tm_Touch.fingerId = touch.fingerId;
            tm_Touch.position = touch.position;
            tm_Touch.deltaPosition = touch.deltaPosition;
            tm_Touch.deltaTime = touch.deltaTime;
            tm_Touch.phase = touch.phase;
            return tm_Touch;
        }

        public static bool ContainBounds(this Bounds bounds, Bounds target)
        {
            return bounds.Contains(target.min) && bounds.Contains(target.max);
        }

        public static bool ContainMostOfBounds(this Bounds bounds, Bounds target)
        {
            var tsize = target.size * (1 - (TouchManSettings._advanced.ToBeConsderedAsSubmerged / 100));
            target.size = new Vector3(target.size.x - tsize.x, target.size.y - tsize.y, target.size.z - tsize.y);

            return bounds.Contains(target.min) && bounds.Contains(target.max);
        }

        public static Bounds ExpandBoundsByPercentile(this Bounds bounds, float expandby)
        {
            var temp = bounds.size.y;
            bounds.size = bounds.size + bounds.size * (expandby / 100);
            bounds.size = new Vector3(bounds.size.x, temp, bounds.size.z);
            return bounds;
        }

        public static Bounds ExpandBoundsByValue(this Bounds bounds, Vector3 expandby)
        {
            //Debug.Log("Size: " + bounds.size + " vs the size: " + (bounds.size + expandby));
            var temp = bounds.size.y;
            bounds.size = bounds.size + expandby;
            bounds.size = new Vector3(bounds.size.x, temp, bounds.size.z);
            return bounds;
        }

        public static bool IsOneOf(this int layer, string[] them)
        {
            return Array.IndexOf(them, LayerMask.LayerToName(layer)) > -1;
        }

        public static Vector3 ClampVector(Vector3 vector, Vector3 min, Vector3 max)
        {
            return new Vector3(Mathf.Clamp(vector.x, min.x, max.x), Mathf.Clamp(vector.y, min.y, max.y), Mathf.Clamp(vector.z, min.z, max.z));
        }

        public static Bounds GetTotalRenderBoundsOf(this Transform trans)
        {
            bool hasBounds = false;
            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

            foreach (Transform t in trans)
            {
                if (t.GetComponent<MeshRenderer>() != null)
                {
                    Renderer childRenderer = t.GetComponent<Renderer>();
                    if (childRenderer != null)
                    {
                        if (hasBounds)
                            bounds.Encapsulate(childRenderer.bounds);
                        else {
                            bounds = childRenderer.bounds; hasBounds = true;
                        }
                    }
                }
            }
            
            return bounds;
        }
    }

    public class TTimer
    {
        public bool isTicking = false;
        public Stopwatch timer;

        public TTimer()
        {
            timer = new Stopwatch();
            isTicking = false;
        }

        public void Start()
        {
            timer.Reset();
            timer.Start();
            isTicking = true;
        }

        public double Stop()
        {
            isTicking = false;
            timer.Stop();

            UnityEngine.Debug.Log("Tot Time Spent In Seconds" + timer.Elapsed.TotalSeconds);

            return timer.Elapsed.TotalSeconds;
        }
    }
}
