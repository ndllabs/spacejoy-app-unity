﻿/* Touchman, 
 * Controls Object movement on it parents surface,
 * 
 * prajwalshetty2018@gmail.com
 * */

using System;
using UnityEngine;

namespace TouchMan
{
    using Tl = TouchManLayers;
    using Ts = TouchManSettings;
    using Te = TouchManEvents;
    using dbg = TouchManDebug;

    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(BoxCollider))]
    public class TouchManSkin : MonoBehaviour
    {
        public delegate void MovementUpdate(ObjectMovementUpdate movementType);
        public MovementUpdate OnMovementUpdate;

        [HideInInspector]
        public float clampVal = -1;
        public float clampvalueByLearning = -1;
        public bool autoInit = false;
        public MoveMode moveMode = MoveMode.GroundAttached;

        private bool isMoving = new bool(), usePhysics = new bool(), isInit = new bool(), isCoolingDown = true;

        #region Current Data

        private Plane cPlane, vPlane;
        private GameObject cInSyncWithGO = null;
        private Vector3 cSurfaceNormal = Vector3.zero;
        private Collider cBufferGo = null;
        private int cLayerMask = 0;
        private float cClampHeight = 0f;
        private Bounds c_ClampBounds;
        #endregion

        private float floorHeight = 0f;
        private BoxCollider[] parentObjects;


        #region Internal

        private Vector3 assetPivotDelta;
        private Vector3 pLerpTarget;
        private Vector3 pivotToDragPointDelta, verticalDelta;
        private BoxCollider col;
        private Rigidbody rigidBody;
        private readonly float frictionHitPointThresh = 3f;
        private readonly float vPlaneDistance = 4f;
        #endregion

        #region Debug

        private Bounds debugDrawBound_1;
        private Bounds debugDrawBound_2;
        #endregion


        #region UnityEvents

        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            if (autoInit)
                Init(moveMode, clampVal);
        }

        private void OnDrawGizmos()
        {
            if (!Ts._debugMode && !isInit) return;

            if (isMoving)
            {
                Gizmos.color = new Color(0, 1, 0, 0.7f);
                Gizmos.DrawCube(c_ClampBounds.center, c_ClampBounds.size);
            }

            if (moveMode == MoveMode.AssetAttached && isMoving)
            {
                Gizmos.color = new Color(1, 0, 0, 0.8f);
                Gizmos.DrawCube(transform.position, new Vector3(0.01f, 2.5f, 0.01f));

                /*Gizmos.color = new Color(0, 0, 1, 0.3f);
                Gizmos.DrawCube(debugDrawBound_1.center, debugDrawBound_1.size);*/

                Gizmos.color = new Color(0, 1, 0, 0.3f);
                Gizmos.DrawCube(debugDrawBound_2.center, debugDrawBound_2.size);
            }

        }

        private void LateUpdate()
        {
            if (!usePhysics && isMoving)
                ThereIsALimitToThis();
        }

        private void FixedUpdate()
        {
            if (isMoving && usePhysics)
                rigidBody.velocity = (Vector3.Lerp(transform.position, pLerpTarget, Ts._advanced.MovementVelocityDelay) - transform.position) * Ts._advanced.VelocityMultiplier;
        }

        private void Update()
        {
            if (!usePhysics && isMoving && !isCoolingDown)
                CheckCollision();
        }

        private void OnEnable()
        {
            Te.OnBeginDrag += OnBeginDrag;
            Te.OnDrag += OnDrag;
            Te.OnEndDrag += OnEndDrag;
            Te.OnOneFingerStationary += OnOneFingerIdle;
        }

        private void OnDisable()
        {
            Te.OnBeginDrag -= OnBeginDrag;
            Te.OnDrag -= OnDrag;
            Te.OnEndDrag -= OnEndDrag;
            Te.OnOneFingerStationary -= OnOneFingerIdle;
        }

        private void OnTriggerEnter(Collider other)
        {
            Te.OnEndDrag += OnEndDrag;
            if (!isInit || isCoolingDown || other.gameObject == cInSyncWithGO || moveMode == MoveMode.GroundAttached) return;
            var tms = other.gameObject.GetComponent<TouchManSkin>();

            if (cInSyncWithGO && cBufferGo != other &&
                moveMode == MoveMode.AssetAttached &&
                other.gameObject.layer.IsOneOf(Tl._AttachableAssetsLayerList) &&
                tms && tms.clampVal >= 0)
            {
                cClampHeight = other.bounds.max.y - floorHeight;
                cBufferGo = other;
                CoolDownCollision();

                if (tms.clampVal == 1 && tms.clampvalueByLearning >= 0)
                    cClampHeight = tms.clampvalueByLearning;
                
                if (Ts._debugMode)
                    debugDrawBound_2 = cBufferGo.bounds;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!isInit) return;
        }

        void OnCollisionEnter(Collision cls)
        {
            if (!isInit || isCoolingDown || cls.collider.gameObject == cInSyncWithGO || moveMode == MoveMode.GroundAttached) return;

            if (moveMode == MoveMode.WallAttached &&
                cls.collider.gameObject.layer.IsOneOf(Tl._WallLayerList))
            {
                cBufferGo = cls.collider;
                CoolDownCollision();
            }
        }

        private void OnCollisionExit(Collision cls)
        {
            if (!isInit || isCoolingDown) return;

            if (moveMode == MoveMode.WallAttached &&
                cls.collider.gameObject.layer.IsOneOf(Tl._WallLayerList))
            {
                cBufferGo = null;
            }
        }
        #endregion

        public void Init(MoveMode moveMode, float clampHeight)
        {
            if (isInit)
            {
                Debug.LogWarning("[Touchman] [Skin] You can Call Init Only Once Per Object Instance's Lifetime");
                return;
            }

            this.moveMode = moveMode;
            this.clampVal = clampHeight;

            switch (this.moveMode)
            {
                case MoveMode.GroundAttached:
                    usePhysics = true;
                    break;
                case MoveMode.CeilingAttached:
                    usePhysics = true;
                    break;
                case MoveMode.Free:
                    usePhysics = true;
                    break;
                case MoveMode.WallAttached:
                    usePhysics = true;
                    break;
                case MoveMode.AssetAttached:
                    usePhysics = true;
                    OnLoadUpdateClampHeight();
                    break;
            }

            UpdateParentObjects();

            //Currently collider data is not available in the Init Frame
            Exec(.2f, () => { UpdateColliderData(); OnUpdateMovementPlane(); });

            isCoolingDown = false;
            isInit = true;
        }

        public void UpdateClampValueByLearning()
        {
            TouchManSkin tms = null;
            if (cBufferGo)
                tms = cBufferGo.gameObject.GetComponent<TouchManSkin>();

            if (!isInit || !tms || tms.clampVal != 1 ||
                moveMode != MoveMode.AssetAttached)
                return;

            tms.clampvalueByLearning = transform.position.y - floorHeight;
        }

        #region Custom Collision

        private bool UpdateColliderData()
        {
            if (!col)
                col = GetComponent<BoxCollider>();
            if (col)
            {
                assetPivotDelta = col.bounds.center - transform.position;
                return true;
            }

            return false;
        }

        private void CheckCollision()
        {
            foreach (var parent in parentObjects)
            {
                if (parent.bounds.Intersects(col.bounds) && parent.gameObject != cInSyncWithGO)
                {
                    SyncWithTargetGameObject(parent);
                    CoolDownCollision();
                }
            }
        }

        private void ThereIsALimitToThis()
        {
            var pos = transform.position;

            pos.x = Mathf.Clamp(pos.x, c_ClampBounds.min.x, c_ClampBounds.max.x);
            pos.z = Mathf.Clamp(pos.z, c_ClampBounds.min.z, c_ClampBounds.max.z);
            pos.y = Mathf.Clamp(pos.y, c_ClampBounds.min.y, c_ClampBounds.max.y);
            transform.position = pos;
        }

        void UpdateParentObjects()
        {
            var objectsWithParentTag = new System.Collections.Generic.List<BoxCollider>();

            cLayerMask = LayerMask.GetMask(Tl._DefaultLayer);
            switch (moveMode)
            {
                case MoveMode.GroundAttached:
                    cLayerMask = LayerMask.GetMask(Tl._GroundLayerList);
                    break;
                case MoveMode.Free:
                    cLayerMask = 0;
                    cPlane = new Plane(Vector3.up, transform.position);
                    break;
                case MoveMode.WallAttached:
                    cLayerMask = LayerMask.GetMask(Tl._WallLayerList);
                    break;
                case MoveMode.AssetAttached:
                    cLayerMask = LayerMask.GetMask(Tl._GroundLayerList);
                    break;
                case MoveMode.CeilingAttached:
                    cLayerMask = LayerMask.GetMask(Tl._CeilingLayerList);
                    break;
            }

            for (int i = 0; i < Tl._ParentObjectsContainers.Length; i++)
            {
                foreach (Transform trans in Tl._ParentObjectsContainers[i])
                {
                    //temporary third loop, just cuz room spawned at runtime
                    foreach (Transform trans1 in trans)
                    {
                        if (moveMode == MoveMode.WallAttached
                            && trans1.gameObject.layer.IsOneOf(Tl._WallLayerList)
                            && trans1.GetComponent<BoxCollider>())
                        {
                            objectsWithParentTag.Add(trans1.GetComponent<BoxCollider>());
                        }
                        else if (moveMode == MoveMode.GroundAttached
                            && trans1.gameObject.layer.IsOneOf(Tl._GroundLayerList)
                            && trans1.GetComponent<BoxCollider>())
                        {
                            objectsWithParentTag.Add(trans1.GetComponent<BoxCollider>());
                        }
                    }
                }
            }


            if (objectsWithParentTag.Count <= Tl._ObjectsBudget)
            {
                parentObjects = objectsWithParentTag.ToArray();
            }
            else
                throw new Exception("[TouchMan] Out of Collision Budget Exception, Too many objects in the layer or collsiion check please reduce the object count in the respective layer");

        }

        #endregion

        #region Touch Events

        private void OnBeginDrag(GameObject dragStartedOver, Vector2 touchPosition)
        {
            if (dragStartedOver != gameObject) return;

            OnUpdateMovementPlane();
            
            if (moveMode != MoveMode.Free && cInSyncWithGO == null) { Debug.LogWarning("[Touchman] [Skin] Sync Error, Movement Skipped"); return; }

            Ray ray = Ts._cam.ScreenPointToRay(new Vector3(touchPosition.x, touchPosition.y, 0));

            UpdateTouchDelta(ray);

            pLerpTarget = transform.position;

            isMoving = true;
            OnMovementUpdate?.Invoke(ObjectMovementUpdate.BeginMove);
        }

        private void OnEndDrag()
        {
            if (!isMoving)
                return;

            rigidBody.velocity = Vector3.zero;
            if (moveMode == MoveMode.AssetAttached)
                transform.position = new Vector3(transform.position.x, floorHeight + cClampHeight, transform.position.z);

            pLerpTarget = transform.position;

            isMoving = false;
            OnMovementUpdate?.Invoke(ObjectMovementUpdate.EndMove);
        }

        private void OnDrag(GameObject dragStartedOver, Vector2 touchPosition, Vector2 touchDeltaPosition)
        {
            if (dragStartedOver != gameObject) return;

            Ray ray = Ts._cam.ScreenPointToRay(new Vector3(touchPosition.x, touchPosition.y, 0));
            
            if (cPlane.Raycast(ray, out float hit) && isMoving)
            {
                Vector3 hitPoint = ray.origin + (ray.direction * hit);
                pLerpTarget = hitPoint + pivotToDragPointDelta;
                
                // For movement along the wall
                if (moveMode == MoveMode.AssetAttached || moveMode == MoveMode.GroundAttached || moveMode == MoveMode.CeilingAttached)
                {
                    if (hitPoint.x - transform.position.x > frictionHitPointThresh || hitPoint.z - transform.position.z > frictionHitPointThresh)
                    {
                        vPlane.SetNormalAndPosition(vPlane.normal, transform.position - vPlane.normal * vPlaneDistance * floorHeight);
                        
                        if (vPlane.Raycast(ray, out float vhit))
                        {
                            Vector3 vhitPoint = ray.origin + (ray.direction * vhit);
                            Vector3 pLerpTargetNew = vhitPoint + verticalDelta;
                            pLerpTarget = new Vector3(pLerpTargetNew.x, pLerpTarget.y, pLerpTargetNew.z);
                        }
                    }
                }
                

                if (!usePhysics)
                    transform.position = pLerpTarget; // No smoothing

                if (moveMode == MoveMode.AssetAttached)
                {
                    transform.position = new Vector3(transform.position.x, floorHeight + cClampHeight, transform.position.z);
                }
            }

            if (cBufferGo)
            {
                if (moveMode == MoveMode.WallAttached && cBufferGo.bounds.IntersectRay(ray))
                {
                    SyncWithTargetGameObject(cBufferGo); cBufferGo = null;
                    UpdateTouchDelta(ray); CoolDownCollision();
                }
                else
                if (!isCoolingDown && moveMode == MoveMode.AssetAttached &&
                         !cBufferGo.bounds.ExpandBoundsByValue(col.bounds.size)
                         .Contains(new Vector3(col.bounds.center.x, cBufferGo.bounds.center.y, col.bounds.center.z)))
                {
                    cBufferGo = null; cClampHeight = 0f;
                    CoolDownCollision(); UpdateTouchDelta(ray);

                    if (Ts._debugMode)
                        debugDrawBound_2 = new Bounds();
                }
            }
        }

        private void OnOneFingerIdle()
        {
            if (moveMode == MoveMode.AssetAttached)
                transform.position = new Vector3(transform.position.x, floorHeight + cClampHeight, transform.position.z);
        }


        private void UpdateTouchDelta(Ray ray)
        {
            if (Ts._debugMode)
                Debug.DrawRay(ray.origin, ray.direction * Ts._advanced.MaxRaycastHitDistance, Color.magenta, 4f);

            // Move current plane to point where drag started
            if (moveMode == MoveMode.AssetAttached || moveMode == MoveMode.GroundAttached || moveMode == MoveMode.CeilingAttached)
            {
                vPlane = new Plane(-ray.direction, transform.position);
                if (vPlane.Raycast(ray, out float vhitNew))
                {
                    Vector3 hitPointNew = ray.origin + ray.direction * vhitNew;
                    cPlane.SetNormalAndPosition(cPlane.normal, new Vector3(transform.position.x, hitPointNew.y, transform.position.z));
                }
            }

            vPlane = new Plane(-ray.direction, transform.position + ray.direction * vPlaneDistance * floorHeight);
            if (cPlane.Raycast(ray, out float hit))
            {
                var point = ray.origin + (ray.direction * hit);
                pivotToDragPointDelta = transform.position - point;
                //Debug.Log("Delta: " + pivotToDragPointDelta);
                
               if (vPlane.Raycast(ray, out float vhit))
               {
                    Vector3 hitPoint = ray.origin + ray.direction * vhit;
                    verticalDelta = transform.position - hitPoint;
                    //Debug.Log("vertical Delta: " + verticalDelta);
               }
               else
               {
                    verticalDelta = Vector3.zero;
                    Debug.LogWarning("[Touchman] [Skin] Improper Vertical Plane. Update Required");
                    return;
               }
            }
            else
            {
                pivotToDragPointDelta = Vector3.zero;
                Debug.LogWarning("[Touchman] [Skin] Improper Current Plane, Update Requried, CurrentPlane: " + cPlane);
                return;
            }
        }

        public void OnCancelMovement()
        {
            OnEndDrag();
        }

        #endregion

        #region Core

        /// <summary>
        /// Finds A Perfect nearest Object To Clamp into based on the movemode
        /// Uses Unity Physics Engine.
        /// Warning: Donot Call this everyframe
        /// </summary>
        private void OnUpdateMovementPlane()
        {
            Collider[] objectsInRange = Physics.OverlapSphere(transform.position, 5f, cLayerMask, QueryTriggerInteraction.Collide);

            float curCloseContact = float.MaxValue; int closestPoint = -1;
            for (int i = 0; i < objectsInRange.Length; i++)
            {
                Vector3 pos = objectsInRange[i].ClosestPointOnBounds(transform.position);
                Vector3 directionToTarget = pos - transform.position;

                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < curCloseContact && objectsInRange[i].gameObject != gameObject)
                {
                    curCloseContact = dSqrToTarget;
                    closestPoint = i;
                }
            }

            if (closestPoint != -1)
                SyncWithTargetGameObject(objectsInRange[closestPoint]);
            else Debug.LogWarning("[Touchman] [Skin] No ones Near Me attach, My Name: " + gameObject.name);
        }

        /// <summary>
        /// Attaches The gameobject to the nearest side of the target's collider,
        /// Updates Both Position and rotation
        /// </summary>
        /// <param name="targetCol"></param>
        private void SyncWithTargetGameObject(Collider targetCol)
        {
            RaycastHit hitInfo;

            UpdateColliderData();

            Vector3 closestPointOnBounds = targetCol.ClosestPointOnBounds(col.bounds.center);

            if (closestPointOnBounds == col.bounds.center)
            {
                Debug.LogWarning("[Touchman] [Skin] The Center Of Current Object is Already Inside the bounds, no more useful " + col.bounds.center + " Parent: " + targetCol.gameObject.name + ", parent Center:" + targetCol.transform.position);
                return;
            }

            var dir = closestPointOnBounds - col.bounds.center;

            if (Ts._debugMode)
                Debug.DrawRay(col.bounds.center, dir, Color.green, 30, false);

            if (targetCol.Raycast(new Ray(col.bounds.center, dir.normalized), out hitInfo, 5))
            {
                /* this is the collider surface normal
                 * */
                var normal = hitInfo.normal;

                if (moveMode == MoveMode.WallAttached)
                {
                    /* Turn the Asset to match wall's forward
                     * */
                    var angle = Vector3.SignedAngle(transform.forward, normal, Vector3.up);
                    rigidBody.rotation = Quaternion.Euler(0, transform.eulerAngles.y + angle, 0);
                }
                else if (moveMode == MoveMode.AssetAttached || moveMode == MoveMode.GroundAttached)
                    floorHeight = hitInfo.point.y;

                /* Update the Current Position to wallspoint + assets size towards the wall
                 * */
                AttachToObject(closestPointOnBounds, hitInfo);

                /* Update the CurrentPlane to wallssurface + assets size towards the wall
                * */
                cPlane = new Plane(hitInfo.normal, transform.position);
                cInSyncWithGO = targetCol.gameObject;
                cSurfaceNormal = normal;
                UpdateMovableBounds(hitInfo.collider.gameObject, normal);

                if (Ts._debugMode && moveMode == MoveMode.AssetAttached)
                    debugDrawBound_1 = new Bounds(transform.position, new Vector3((2 * (1 - hitInfo.normal.x)) + 0.01f, (2 * (1 - hitInfo.normal.y)) + 0.01f, (2 * (1 - hitInfo.normal.z) + 0.01f)));
            }
            else
            {
                Debug.LogWarning("[Touchman] [Skin] Failed To Hit Parent Collider");
            }
        }

        private void OnLoadUpdateClampHeight()
        {

            Collider[] objectsInRange = Physics.OverlapBox(transform.position, new Vector3(0.01f, 2.5f, 0.01f), transform.rotation, LayerMask.GetMask(Tl._AttachableAssetsLayerList.Concat(Tl._GroundLayerList)), QueryTriggerInteraction.Collide);
            
            //First Set Floor Height
            for (int i = 0; i < objectsInRange.Length; i++)
            {
                if (objectsInRange[i].gameObject.layer.IsOneOf(Tl._GroundLayerList))
                {
                    floorHeight = objectsInRange[i].ClosestPoint(transform.GetTotalRenderBoundsOf().center).y;
                    break;
                }
            }

            for (int i = 0; i < objectsInRange.Length; i++)
            {
                TouchManSkin tms = objectsInRange[i].gameObject.GetComponent<TouchManSkin>();
                cClampHeight = 0;
                if (tms)
                {
                    if (tms.clampVal == 1)
                    {
                        cClampHeight = transform.position.y - floorHeight;
                        cBufferGo = objectsInRange[i];
                        tms.clampvalueByLearning = cClampHeight;
                        
                        if (Ts._debugMode && cBufferGo)
                            debugDrawBound_2 = cBufferGo.bounds;

                        return;
                    }
                    else if (tms.clampVal == 0)
                    {
                        cClampHeight = objectsInRange[i].bounds.max.y - floorHeight;
                        cBufferGo = objectsInRange[i];
                        
                        if (Ts._debugMode && cBufferGo)
                            debugDrawBound_2 = cBufferGo.bounds;

                        return;
                    }
                    else if (tms.clampVal > 0)
                    {
                        cClampHeight = tms.clampVal;
                        cBufferGo = objectsInRange[i];


                        if (Ts._debugMode && cBufferGo)
                            debugDrawBound_2 = cBufferGo.bounds;

                        return;
                    }
                }
            }

        }


        private void UpdateMovableBounds(GameObject papa, Vector3 papaSurfaceNormal)
        {
            var papaBounds = papa.GetComponent<Collider>().bounds;
            var center = papaBounds.center;
            var size = papaBounds.size;
            var papaForward = papaSurfaceNormal;

            c_ClampBounds = new Bounds();

            c_ClampBounds.center = new Vector3(center.x + (papaForward.x * ((col.size.x / 2) + (size.x / 2))),
                center.y + (papaForward.y * ((col.size.y / 2) + (size.y / 2))),
                center.z + (papaForward.z * ((col.size.z / 2) + (size.z / 2))));

            c_ClampBounds.size = new Vector3(size.x + (Mathf.Abs(papaSurfaceNormal.x) * col.size.x) - (Mathf.Abs(papaSurfaceNormal.x) * size.x),
                size.y + (Mathf.Abs(papaSurfaceNormal.y) * col.size.y) - (Mathf.Abs(papaSurfaceNormal.y) * size.y),
                size.z + (Mathf.Abs(papaSurfaceNormal.z) * col.size.z) - (Mathf.Abs(papaSurfaceNormal.z) * size.z));

            c_ClampBounds.center = c_ClampBounds.center - assetPivotDelta;
            c_ClampBounds.size = c_ClampBounds.size - col.bounds.size;
        }

        /// <summary>
        /// Attaches the Asset To The wall i.e Specified Object
        /// </summary>
        /// <param name="destinationPoint"></param>
        /// <param name="hitinfo"></param>
        void AttachToObject(Vector3 destinationPoint, RaycastHit hitinfo)
        {
            var n = hitinfo.normal;
            var pn = cSurfaceNormal;
            var d = hitinfo.distance;
            var ex = col.bounds.extents;
            transform.position = hitinfo.point - assetPivotDelta + new Vector3(pn.x * ex.x, 0, pn.z * ex.z) + new Vector3(n.x * ex.x, (n.y * ex.y) + cClampHeight, n.z * ex.z);
            //transform.position = transform.position + new Vector3(n.x * ex.x, (n.y * ex.y) + cClampHeight, n.z * ex.z);

            //lol //temp Chnages by Kuns3
            if (n.z == 1 | n.z == -1)
            {
                //Debug.LogError(transform.position.z);
                transform.position= new Vector3(transform.position.x, transform.position.y,hitinfo.point.z + n.z*ex.z);
            }
            else if (n.x == 1 | n.x == -1)
            {
                //Debug.LogError(transform.position.x);
                transform.position = new Vector3(hitinfo.point.x + n.x*ex.x, transform.position.y, transform.position.z);
            }

            CoolDownCollision();
            return;
        }

        #region timer

        System.Collections.IEnumerator _Exec(float time, Action Callback)
        {
            yield return new WaitForSeconds(time);
            Callback();
        }

        public void Exec(float time, Action Callback)
        {
            StartCoroutine(_Exec(time, Callback));
        }

        public void CoolDownCollision(float forSeconds = 0.1f)
        {
            isCoolingDown = true;
            Exec(forSeconds, () => { isCoolingDown = false; });
        }
        #endregion

        #endregion


    }

    /* now few ways to get the face normal of the hit surface
     * 1) if mesh collider, get the hit face and then get the normal
     * 2) else if the custom primitive colliders are not accurate enough, get the closest to point triangle and get its normal
     * 3) or use the Collider raycast to get the surface normal of the hit
     * */
}
