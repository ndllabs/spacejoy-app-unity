﻿/* Touchman
 * Gesture Based Camera Movement
 * 
 * prajwalshetty2018@gmail.com
 * */

using UnityEngine;


namespace TouchMan
{
    using Ts = TouchManSettings;
    using Te = TouchManEvents;
    using Tl = TouchManLayers;
    using dbg = TouchManDebug;

    [RequireComponent(typeof(Rigidbody))]
    public class TouchManCamera : MonoBehaviour
    {
        public new Camera camera;
        /*#region Experimental Rotate SOS
        public float maxAngle = 20f;
        Vector3 center;

        private void Start()
        {
            center = camera.transform.forward;//camera.transform.rotation;
        }
        #endregion*/

        public bool canMove = new bool();

        private Rigidbody rigidBody;

        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
        }

        private void OnEnable()
        {
            Te.OnPinch += OnPinch;
            Te.OnTwoFingerPan += OnTwoFingerPan;
            Te.OnTwoFingerStationary += OnTwoFingerStationary;
            Te.OnEndTouch += OnEndTouch;

            Te.OnBeginDrag += OnBeginDrag;
            Te.OnDrag += OnDrag;
        }

        private void OnDisable()
        {
            Te.OnPinch -= OnPinch;
            Te.OnTwoFingerPan -= OnTwoFingerPan;
            Te.OnTwoFingerStationary -= OnTwoFingerStationary;
            Te.OnEndTouch -= OnEndTouch;

            Te.OnBeginDrag -= OnBeginDrag;
            Te.OnDrag -= OnDrag;
        }

        private void OnBeginDrag(GameObject dragStartedOver, Vector2 touchPosition)
        {
            if (dragStartedOver && !dragStartedOver.GetComponent<Collider>().isTrigger &&
                dragStartedOver.layer.IsOneOf(Tl._SelectableObjectsLayerList))
                canMove = false;
            else
                canMove = true;
        }

        private void OnDrag(GameObject dragStartedOver, Vector2 touchPosition, Vector2 touchDeltaPosition)
        {
            if (!canMove) return;

            /* Move Camera forward and back, if one finger up down
             * */
#if UNITY_IOS
            MoveCamera_Deprycated(touchDeltaPosition * -Ts._speedModiferTouch, Vector2.zero);
#else
              MoveCamera_Deprycated(touchDeltaPosition * Ts._speedModiferTouch, Vector2.zero);
#endif
            /* Rotate Camera forward and back, if one finger right left
             * */
#if UNITY_IOS
            RotateCamera(new Vector2(0, touchDeltaPosition.x * -Ts._speedModiferTouch.x));
#else
             RotateCamera(new Vector2(0, touchDeltaPosition.x * Ts._speedModiferTouch.x));
#endif
        }

        private void OnEndTouch()
        {
            canMove = true;

            //Break Movement
            rigidBody.velocity = Vector3.zero;
        }

        private void OnTwoFingerStationary()
        {
            //De-Accelerate
            rigidBody.velocity = rigidBody.velocity * 0.75f;//0.92f;
        }

        private void OnTwoFingerPan(Touch touch0, Touch touch1)
        {
            if (Mathf.Abs(touch0.deltaPosition.y) <= Mathf.Abs(touch0.deltaPosition.x))
                MoveCamera_Deprycated(Vector2.zero, Mathf.Clamp(touch0.deltaPosition.x, -60f, 60f) * (Ts._advanced.MultiplyerTwoFingerGestures / 2.3f) * Ts._speedModiferTouch);
            else if (Mathf.Abs(touch0.deltaPosition.x) <= Mathf.Abs(touch0.deltaPosition.y))
                RotateCamera_Wrapper(new Vector2((Mathf.Clamp(touch0.deltaPosition.y, -50f, 50f) / 10) * Ts._advanced.MultiplyerTwoFingerGestures * Ts._speedModiferTouch.y, 0));

            
            /*if (Ts._minPanDistance.x <= Mathf.Abs(touch0.deltaPosition.x))
                MoveCamera_Deprycated(Vector2.zero, touch0.deltaPosition.x * Ts._advanced.MultiplyerTwoFingerGestures * Ts._speedModiferTouch);
            else if (Ts._minPanDistance.y <= Mathf.Abs(touch0.deltaPosition.y))
                //RotateCamera_Experimental((touch0.deltaPosition.y / 10) * Ts._advanced.MultiplyerTwoFingerGestures * Ts._speedModiferTouch.y);  //SOS
            RotateCamera_Wrapper(new Vector2((touch0.deltaPosition.y / 10) * Ts._advanced.MultiplyerTwoFingerGestures * Ts._speedModiferTouch.y, 0));*/
        }

        private void OnPinch(float TouchDistanceDelta, Touch touch0, Touch touch1)
        {
            if (!canMove) return;

            MoveCamera_Deprycated((TouchDistanceDelta * (Ts._advanced.MultiplyerTwoFingerGestures / 6)) * Ts._speedModiferTouch, Vector2.zero);
        }

#region Core

        void MoveCamera_Deprycated(Vector2 moveForwardAmount, Vector2 moveRightAmount)
        {
            if (!canMove) return;

            rigidBody.velocity = ((transform.forward * moveForwardAmount.y * Ts._advanced.moveSpeed.y) + (transform.right * moveRightAmount.x * Ts._advanced.moveSpeed.y)) * Time.deltaTime;
        }


        /// <summary>
        /// Moves the Camera in Froward and Right Directions
        /// </summary>
        /// <param name="moveX"> Right Move Amount</param>
        /// <param name="moveY"> Forward Move Amount</param>
        void MoveCamera(float moveX, float moveY)
        {
            if (!canMove) return;

            rigidBody.velocity = ((transform.forward * moveY * Ts._advanced.moveSpeed.y) +
                (transform.right * moveX * Ts._advanced.moveSpeed.x)) * Time.deltaTime;
        }

        /*/// <summary>
        /// Experimental rotate using Quaternions
        /// </summary>
        /// <param name="x"></param>
        void RotateCamera_Experimental(float x)
        {
            if (!canMove) return;

            float rotationXInput = x * Time.deltaTime * Ts._advanced.moveSpeed.x;
            //Debug.Log(rotationXInput);
            // Input.GetAxis("Mouse Y") * sensitivityY * Time.deltaTime;
            Quaternion yQuaternion = Quaternion.AngleAxis(rotationXInput, -Vector3.right);
            //Quaternion temp = camera.transform.localRotation * xQuaternion;
            //Debug.Log(Quaternion.Angle(center, temp));
            //if (Quaternion.Angle(center, temp) < maxAngle) camera.transform.localRotation = temp;

            camera.transform.forward = ClampVector(yQuaternion * camera.transform.forward, center, maxAngle);
        }


        Vector3 ClampVector(Vector3 direction, Vector3 center, float maxAngle)
        {
            float angle = Vector3.Angle(center, direction);
            if (angle > maxAngle)
            {
                Debug.Log("AHA" + angle);
                direction.Normalize();
                center.Normalize();
                Vector3 rotation = (direction - center) / angle;
                return (rotation * maxAngle) + center;

            }

            return direction;
        }*/
        
        /// <summary>
        /// Specialized code to access camera inside Player69FPS and rotate it
        /// </summary>
        /// <param name="rotateXY">X => X Rotation Amount, Y => Y Rotation Amount</param>
        void RotateCamera_Wrapper(Vector2 rotateXY)
        {
            if (!canMove) return;
            Transform transform = camera.transform;//GetComponentInChildren<Camera>().gameObject.transform;

            transform.Rotate((transform.up * rotateXY.y * Time.deltaTime * Ts._advanced.moveSpeed.y) +
                (transform.right * rotateXY.x * Time.deltaTime * Ts._advanced.moveSpeed.x), Space.World);

            var rot = transform.eulerAngles;

            if (rot.x > 300) rot.x = rot.x -360; // e.g. 360 + -40 = 320 which is the same rotation
            rot.x = Mathf.Clamp(rot.x, -20f, 30f);
            rot.y = rot.z = 0;

            transform.localEulerAngles= rot;
        }

        /// <summary>
        /// Rotates the Camera in Right and Up Axis
        /// </summary>
        /// <param name="rotateXY">X => X Rotation Amount, Y => Y Rotation Amount</param>
        void RotateCamera(Vector2 rotateXY)
        {
            if (!canMove) return;

            transform.Rotate((transform.up * rotateXY.y * Time.deltaTime * Ts._advanced.moveSpeed.y) +
                (transform.right * rotateXY.x * Time.deltaTime * Ts._advanced.moveSpeed.x), Space.World);

            var rot = transform.eulerAngles;

            rot.x = Mathf.Clamp(rot.x, 0f, 16f);

            //if (rot.x < 0) rot.x = 360 + rot.x; // e.g. 360 + -40 = 320 which is the same rotation

            transform.eulerAngles = rot;
        }

#endregion
    }
}