﻿/* Touchman, 
 * Detects Gestures,
 * 
 * prajwalshetty2018@gmail.com
 * */

using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TouchMan
{
    using Tl = TouchManLayers;
    using Ts = TouchManSettings;
    using Te = TouchManEvents;
    using dbg = TouchManDebug;

    public class TouchManMain : MonoBehaviour
    {
        #region Vars

        #region Public

        [HideInInspector]
        public TouchManSkin currentSelectedObject = null;

        [Tooltip("Main Game Camera")]
        public new Camera camera;

        [SerializeField]
        [Header("Layers settings")]
        [Tooltip("Layer settings")]
        public Tl layers;
        
        [SerializeField]
        [Header("Touch settings")]
        [Tooltip("Touch settings")]
        public Ts settings;

        [Header("Target Canvas")]
        [Tooltip("On requirement, assigning a canvas results in touch hit on UI, can be used to implement custom ui gestures and clicks")]
        public Canvas uiCastTarget;

        [Header("Debug")]
        [HideInInspector]
        private bool debugMode = false;

        #endregion

        #region Internal
        /* Local Cache
         * */
        bool touchStartedOverUI = false;
        float timeElapsedInTouch;
        float timeElapsedSincePreviousTap;
        Vector2 touch0_PosLastFrame;
        Vector2 touch1_PosLastFrame;
        Vector2 touch0_StartPos;
        Vector2 touch1_StartPos;
        float distBwFingers_OnStart;
        GameObject touchStartedOverGO;
        ITouchManUI touchStartedOverUI_ref;
        Vector3 goPivotToTouchPointDelta;
        GestureType currentGesture = GestureType.None;
        GraphicRaycaster uiCast;

        /* Two Finger
         * */
        private float distanceBwFingers/*, pinchDistanceDelta*/;
        //private float turnAngle/*, turnAngleDelta*/;
        #endregion

        #endregion

        #region UnityEvents

        void Update()
        {
            if (Input.touchSupported)
                RespondToTouch();
            else
                RespondToPointer();
        }
        
        private void Awake()
        {
            layers.UpdateGlobalSettings();
            settings.UpdateGlobalSettings();

            uiCast = null;
            if (uiCastTarget != null)
                uiCast = uiCastTarget.gameObject.GetComponent<GraphicRaycaster>();

            if (!camera)
            {
                if (gameObject.GetComponent<Camera>())
                    camera = gameObject.GetComponent<Camera>();
                else
                    camera = Camera.main;
            }

            Ts._debugMode = debugMode;
            if (camera)
                Ts._cam = camera;
            else
                throw new Exception("[TouchMan][Main] Camera Doesnt Exist in the current Context", new NullReferenceException());


            if (SystemInfo.supportsGyroscope)
                dbg.Log("All Father Give me Sight", dbgType.log);
            else
                dbg.Log("All Father Give me Gyroscope", dbgType.log);

            dbg.Log("TouchMan Debug Mode.", dbgType.msg);
        }

        #endregion

        private void RespondToPointer()
        {
            var touchPosCurrentFrame = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            bool isCtrl = false;

            if (Input.GetKey(KeyCode.LeftControl))
                isCtrl = true;

            if (Input.GetMouseButtonDown(0))
            {
                touchStartedOverUI = IsTouchOverUI(-1);
                touch0_PosLastFrame = touch0_StartPos = touchPosCurrentFrame;

                if (!isCtrl)
                    TouchBegin_1f(touchPosCurrentFrame);
            }
            else if (Input.GetMouseButton(0))
            {
                if (!isCtrl)
                    TouchMoved(touchPosCurrentFrame, touchPosCurrentFrame - touch0_PosLastFrame);
                else
                {
                    Te.OnTwoFingerPan?.Invoke(new Touch()
                    {
                        position = touchPosCurrentFrame,
                        deltaPosition = touchPosCurrentFrame - touch0_PosLastFrame,
                        fingerId = 0,
                        phase = TouchPhase.Began
                    }, new Touch());
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                TouchEnd_1F();
            }
        }

        private void RespondToTouch()
        {
            switch (Input.touchCount)
            {
                case 1:
                    SingleFingerGestures();
                    break;
                case 2:
                    TwoFingerGestures();
                    break;
                default:
                    if (Input.touchCount > 1)
                        dbg.Log("Fingers OnScreen: " + Input.touchCount);
                    break;
            }

        }

        private void SingleFingerGestures()
        {
            switch (Input.GetTouch(0).phase)
            {
                case TouchPhase.Began:

                    touchStartedOverUI = IsTouchOverUI(Input.GetTouch(0).fingerId);

                    TouchBegin_1f(Input.GetTouch(0).position);

                    break;

                case TouchPhase.Moved:

                    TouchMoved(Input.GetTouch(0).position, Input.GetTouch(0).deltaPosition);
                    break;

                case TouchPhase.Stationary:

                    touch0_PosLastFrame = Input.GetTouch(0).position;
                    timeElapsedInTouch += Time.deltaTime;

                    if (touchStartedOverUI) return;

                    Te.OnOneFingerStationary?.Invoke();

                    dbg.Log(TState.Stationary_1F.ToString());
                    break;

                case TouchPhase.Ended:

                    TouchEnd_1F();
                    break;
                case TouchPhase.Canceled:


                    touchStartedOverGO = null;
                    touchStartedOverUI = false;

                    Te.OnEndTouch?.Invoke();

                    touch0_PosLastFrame = Vector2.zero;

                    dbg.Log(TState.Canceled_1F.ToString());
                    break;
            }
        }


        private void TwoFingerGestures()
        {
            Touch touch0 = Input.touches[0];
            Touch touch1 = Input.touches[1];

            if (touch0.phase == TouchPhase.Began)
            {
                touch0_PosLastFrame = touch0_StartPos = touch0.position;
                touch1_StartPos = touch1_PosLastFrame = touch1.position;
                distBwFingers_OnStart = Vector2.Distance(touch0.position, touch1.position);
                touchStartedOverUI = IsTouchOverUI(0);
                currentGesture = GestureType.None;
            }
            if (touch1.phase == TouchPhase.Began)
            {
                touch0_PosLastFrame = touch0_StartPos = touch0.position;
                distBwFingers_OnStart = Vector2.Distance(touch0.position, touch1.position);
                touch1_StartPos = touch1_PosLastFrame = touch1.position;
                touchStartedOverUI = IsTouchOverUI(0, 1);
                currentGesture = GestureType.None;
            }

            if (touch0.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved)
            {
                if (touchStartedOverUI) return;

                switch (currentGesture)
                {
                    case GestureType.None:
                        DetectGesture(touch0, touch1);

                        break;
                    case GestureType.TwoFingerPan:
                        Te.OnTwoFingerPan?.Invoke(touch0, touch1);

                        break;
                    case GestureType.PinchOrSpread:

                        distanceBwFingers = Vector2.Distance(touch0.position, touch1.position);
                        float prevDistance = Vector2.Distance(touch0_PosLastFrame, touch1_PosLastFrame);
                        var distanceBwFingers_Delta = distanceBwFingers - prevDistance;

                        Te.OnPinch?.Invoke(distanceBwFingers_Delta, touch0, touch1);

                        break;
                    case GestureType.Rotate:
                        break;
                }

                touch0_PosLastFrame = touch0.position; touch1_PosLastFrame = touch1.position;
            }
            else if (touch0.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Ended)
            {
                touch0_PosLastFrame = touch1_PosLastFrame = Vector2.zero;
                touchStartedOverGO = null;
                touchStartedOverUI = false;
                currentGesture = GestureType.None;

                Te.OnEndTouch?.Invoke();
            }
            else if (touch0.phase == TouchPhase.Canceled || touch1.phase == TouchPhase.Canceled)
            {
                touch0_PosLastFrame = touch1_PosLastFrame = Vector2.zero;

                touchStartedOverGO = null;
                touchStartedOverUI = false;
                currentGesture = GestureType.None;

                Te.OnEndTouch?.Invoke();
            }
            else if (touch0.phase == TouchPhase.Stationary && touch1.phase == TouchPhase.Stationary)
            {
                switch (currentGesture)
                {
                    case GestureType.None:
                        DetectGesture(touch0, touch1);

                        break;
                    case GestureType.TwoFingerPan:
                        Te.OnTwoFingerPan?.Invoke(touch0, touch1);

                        break;
                    case GestureType.PinchOrSpread:

                        distanceBwFingers = Vector2.Distance(touch0.position, touch1.position);
                        float prevDistance = Vector2.Distance(touch0_PosLastFrame, touch1_PosLastFrame);
                        var distanceBwFingers_Delta = distanceBwFingers - prevDistance;

                        Te.OnPinch?.Invoke(distanceBwFingers_Delta, touch0, touch1);

                        break;
                    case GestureType.Rotate:
                        break;
                }
            }
        }

        #region Two Finger GestureStages

        private void DetectGesture(Touch touch0, Touch touch1)
        {
            distanceBwFingers = /*pinchDistanceDelta =*/ 0f;
            //turnAngle /*= turnAngleDelta*/ = 0;

            // ... check the delta distance between them ...
            distanceBwFingers = Vector2.Distance(touch0.position, touch1.position);
            float prevDistance = Vector2.Distance(touch0_PosLastFrame, touch1_PosLastFrame);
            var distanceBwFingers_Delta = distanceBwFingers - prevDistance;

            Debug.Log("DPan Distance: " + (touch0_StartPos - touch0.position) + " min: " + Ts._minTotalPanDistance + " Delta: " + touch0.deltaPosition);
            Debug.Log("DPinch Distance: " + (distBwFingers_OnStart - distanceBwFingers) + " DBF: " + Mathf.Abs(distanceBwFingers_Delta) + " DBFmin: " + Ts._minPinchDistance);

            if (Mathf.Abs(distanceBwFingers_Delta) > Ts._minPinchDistance ||
                Ts._minTotalPinchDistance < Math.Abs(distBwFingers_OnStart - distanceBwFingers))
            {
                currentGesture = GestureType.PinchOrSpread;
                Te.OnPinch?.Invoke(distanceBwFingers_Delta, touch0, touch1);
            }
            else
            if (!touch0.deltaPosition.IsSmallerThan_ABS(Ts._minPanDistance) ||
                (!(touch0_StartPos - touch0.position).IsSmallerThan_ABS(Ts._minTotalPanDistance) &&
                !(touch1_StartPos - touch1.position).IsSmallerThan_ABS(Ts._minTotalPanDistance)))
            {
                currentGesture = GestureType.TwoFingerPan;
                Te.OnTwoFingerPan?.Invoke(touch0, touch1);
            }
            else
                Te.OnTwoFingerStationary?.Invoke();
        }

        #endregion

        #region One Finger Touchstages

        private void TouchBegin_1f(Vector2 touchPosition)
        {
            touch0_StartPos = touch0_PosLastFrame = touchPosition;
            timeElapsedInTouch = 0f;

            if (touchStartedOverUI)
            {
                SetHitUIObjectRef(touchPosition);
                return;
            }

            int layerMask = ~LayerMask.GetMask(Tl._PlayerLayer);
            Ray raycast = camera.ScreenPointToRay(touchPosition);
            RaycastHit[] multiHitInfo = Physics.RaycastAll(raycast,
                                                            Ts._advanced.MaxRaycastHitDistance,
                                                            layerMask,
                                                            QueryTriggerInteraction.Collide);

            Array.Sort(multiHitInfo, (x, y) => x.distance.CompareTo(y.distance));

            touchStartedOverGO = null;
            for (int i = 0; i < 3 & i < multiHitInfo.Length; i++)
            {
                if (touchStartedOverGO != null && touchStartedOverGO.layer == LayerMask.NameToLayer(Tl._HighPrioritySelectableLayer))
                    continue;

                //IF nothing is Selected so Far
                if (touchStartedOverGO == null ||
                    // or IF Current is Submerged in the Selected one, and the current is a trigger. Where "Current" is "multiHitInfo[i]" && "Selected One" is "touchStartedOverGO"
                    (touchStartedOverGO.GetComponent<Collider>().isTrigger && touchStartedOverGO.GetComponent<Collider>().bounds.ContainMostOfBounds(multiHitInfo[i].collider.bounds)) ||
                    // or IF current is not a Trigger and is one of them. Where "Current" is "multiHitInfo[i]" and "them" is "Objects of layer:Tl._SelectableObjectsLayerList"
                    (!multiHitInfo[i].collider.isTrigger && multiHitInfo[i].collider.gameObject.layer.IsOneOf(Tl._SelectableObjectsLayerList)) ||
                    // or IF its a high Priority Layer, Just staright away consider that, just make sure that something similar is not already selected
                    (multiHitInfo[i].collider.gameObject.layer == LayerMask.NameToLayer(Tl._HighPrioritySelectableLayer)))
                {
                    touchStartedOverGO = multiHitInfo[i].collider.gameObject;
                }
            }

            Te.OnBeginDrag?.Invoke(touchStartedOverGO, touchPosition);

            if (debugMode)
                Debug.DrawRay(raycast.origin,
                              raycast.direction.normalized * Ts._advanced.MaxRaycastHitDistance,
                              Color.red, 10f);
        }

        private void TouchMoved(Vector2 touchPosition, Vector2 touchDeltaPosition)
        {
            touch0_PosLastFrame = touchPosition;
            timeElapsedInTouch += Time.deltaTime;

            if (touchStartedOverUI)
            {
                //If Hit over object who Implements Touchman's interface && Drag now is within some % of screen
                if (touchStartedOverUI_ref != null && (touchPosition.x / Screen.width < 0.70f ||
                    // Or If, A Long Press on the Current UI Object
                    (timeElapsedInTouch > Ts._maxLongPressTime &&
                    // Also, if the finger hasn't moved too far from start pos.
                    (touch0_PosLastFrame - touch0_StartPos).IsSmallerThan_ABS(Ts._maxTapMovement))))
                {
                    touchStartedOverUI_ref.OnUIDraggedTowardsWorld();
                    touchStartedOverUI_ref = null;
                }
            }
            else
                Te.OnDrag?.Invoke(touchStartedOverGO, touchPosition, touchDeltaPosition);
        }

        private void TouchEnd_1F()
        {
            Te.OnEndTouch?.Invoke();
            Te.OnEndDrag?.Invoke();

            if (!touchStartedOverUI && (touch0_PosLastFrame - touch0_StartPos).IsSmallerThan_ABS(Ts._maxTapMovement))
            {
                if (timeElapsedInTouch < Ts._maxTapTime)
                {
                    Te.OnTap?.Invoke(touchStartedOverGO);
                    dbg.Log(nameof(Te.OnTap) + " " + (touchStartedOverGO ? (touchStartedOverGO.name/*+ ( (touchStartedOverGO.GetComponent<TouchManSkin>() ? typeof(touchStartedOverGO.GetComponent<TouchManSkin>().moveMode) + " " +*/ ) : string.Empty), dbgType.msg);
                }
                else if (timeElapsedInTouch < Ts._maxLongPressTime)
                {
                    Te.OnLongPress?.Invoke(touchStartedOverGO);
                    dbg.Log(nameof(Te.OnLongPress) + " " + (touchStartedOverGO ? touchStartedOverGO.name : string.Empty), dbgType.msg);
                }
            }

            touchStartedOverUI = false;

            touch0_PosLastFrame = Vector2.zero;
            touchStartedOverGO = null;
        }

        #endregion

        #region Core

        #region Utils

        bool IsTouchOverUI(int fingerId)
        {
            if (EventSystem.current.IsPointerOverGameObject(fingerId))
            {
                dbg.Log(TState.OverUI.ToString());
                return true;
            }
            return false;
        }

        bool IsTouchOverUI(params int[] fingerIds)
        {
            foreach (int id in fingerIds)
            {
                if (EventSystem.current.IsPointerOverGameObject(id))
                {
                    dbg.Log(TState.OverUI.ToString());
                    return true;
                }
            }
            return false;
        }

        bool isVectorASmallerThanB_ABS(Vector2 vectorA, Vector2 vectorB)
        {
            return (Math.Abs(vectorA.x) < Math.Abs(vectorB.x)) && (Math.Abs(vectorA.y) < Math.Abs(vectorB.y));
        }

        float Angle(Vector2 pos1, Vector2 pos2)
        {
            Vector2 from = pos2 - pos1;
            Vector2 to = new Vector2(1, 0);

            float result = Vector2.Angle(from, to);
            Vector3 cross = Vector3.Cross(from, to);

            if (cross.z > 0)
            {
                result = 360f - result;
            }

            return result;
        }

        #endregion

        void SetHitUIObjectRef(Vector2 touchPosition)
        {
            if (!uiCast) return;

            PointerEventData m_PointerEventData = new PointerEventData(EventSystem.current);
            m_PointerEventData.position = touchPosition;

            var results = new System.Collections.Generic.List<RaycastResult>();

            /* Raycast using the Graphics Raycaster and touch position
             * */
            uiCast.Raycast(m_PointerEventData, results);

            if (results.Count <= 0) return;

            foreach (var comp in results[0].gameObject.GetComponents(typeof(Component)))
            {
                if (comp is ITouchManUI iface)
                    touchStartedOverUI_ref = iface;
            }

        }
    }

    #endregion



}



//RaycastHit raycastHit;
//if (Physics.Raycast(raycast,
//                    out raycastHit,
//                    Ts.__advanced.MaxRaycastHitDistance,
//                    layerMask,
//                    QueryTriggerInteraction.Ignore))
//{
//    touchStartedOverGO = raycastHit.collider.gameObject;
//}
//else { touchStartedOverGO = null; }

//Debug.dbg.Log("I hit: " + multiHitInfo[i].collider.name);
//                if (touchStartedOverGO != null)
//                    Debug.dbg.Log("Detailed Report: " + multiHitInfo[i].collider.name + "\n BedBounds: " + touchStartedOverGO.GetComponent<Collider>().bounds + "\n" +
//                        "Cushie Bounds: " + multiHitInfo[i].collider.bounds);

    
//Debug.Log("SPinch Distance: " + (distBwFingers_OnStart - distanceBwFingers) + " DBF: " + Mathf.Abs(distanceBwFingers_Delta) + " DBFmin: " + Ts._minPinchDistance);
//Debug.Log("---- SPan Distance: " + (touch0_StartPos - touch0.position) + " min: " + Ts._minTotalPanDistance + " Delta: " + touch0.deltaPosition);

/*#region Debug

public void dbg.Log(string msg, debugMsgType st = debugMsgType.log)
{
    if (!debugMode) return;

    Debug.dbg.Log(msg);
    if (!debugDisplay || !debugDisplayMsg) return;

    if (st == debugMsgType.log)
        debugDisplay.text = msg;
    else
        debugDisplayMsg.text = msg;
}
public void dbg.Log(TState msg, debugMsgType st = debugMsgType.log)
{
    if (!debugMode) return;

    Debug.dbg.Log(msg.ToString());
    if (!debugDisplay || !debugDisplayMsg) return;

    if (st == debugMsgType.log)
        debugDisplay.text = msg.ToString();
    else
        debugDisplayMsg.text = msg.ToString();
}

#endregion*/

//WIP
// ... or check the delta angle between them ...
/*turnAngle = Angle(touch0.position, touch1.position);
float prevTurn = Angle(touch0.position - touch0.deltaPosition,
                       touch1.position - touch1.deltaPosition);
turnAngleDelta = Mathf.DeltaAngle(prevTurn, turnAngle);
// ... if it's greater than a minimum threshold, it's a turn!
if (Mathf.Abs(turnAngleDelta) > Ts._minTurnAngle)
{
    //turnAngleDelta *= pinchTurnRatio;
    dbg.Log("Rotated " + turnAngle + " deltea: " + turnAngleDelta, debugMsgType.msg);
}
else
{
    //dbg.Log("failated " + turnAngle + " deltea: " + turnAngleDelta, msgt.msg);
    turnAngle = turnAngleDelta = 0;
}*/
