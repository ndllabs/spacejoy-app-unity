﻿using UnityEngine;
using TouchMan;
using System;

public class TouchManQuad : MonoBehaviour
{
    //bool isMoving = new bool();
    
    private void OnEnable()
    {
        TouchManEvents.OnBeginDrag += OnBeginDrag;
        TouchManEvents.OnDrag += OnDrag;
        TouchManEvents.OnEndDrag += OnEndDrag;

        TouchManEvents.OnPinch += OnPinch;
    }

    private void OnPinch(float TouchDistanceDelta, Touch touch0, Touch touch1)
    {
        //throw new NotImplementedException();
    }

    private void OnEndDrag()
    {
        throw new NotImplementedException();
    }

    private void OnDrag(GameObject dragStartedOver, Vector2 touchPosition, Vector2 touchDeltaPosition)
    {
        //throw new NotImplementedException();
    }

    private void OnBeginDrag(GameObject dragStartedOver, Vector2 touchPosition)
    {
        if (dragStartedOver != gameObject) return;

    }

    public enum QuadTranslateMode
    {
        Move,
        Rotate,
        Scale
    }
}
