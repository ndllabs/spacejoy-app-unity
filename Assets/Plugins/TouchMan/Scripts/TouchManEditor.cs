﻿/* 
 * Editor Script, Touchman Options
 * 
 * prajwalshetty2018@gmail.com
 * */
 
 using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TouchMan
{
    public class TouchManEditor : MonoBehaviour
    {
#if UNITY_EDITOR
        [MenuItem("AllFather/TouchMan/DebugMode", true)]
        static bool CheckMenu_DebugMode()
        {
            return true;
        }
        [MenuItem("AllFather/TouchMan/DebugMode")]
        static void DebugMode()
        {
            TouchManSettings._debugMode = !TouchManSettings._debugMode;
        }
#endif
    }
}
