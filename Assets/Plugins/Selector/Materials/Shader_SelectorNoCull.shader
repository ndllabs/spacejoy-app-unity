﻿/* A Simlple See through shader for world space, that wants to behave as a UI object
 *
 * - no Smooth particle support
 * - no AlphaTest
 * - no ColorMask 
 *
 * prajwalshetty2018@gmail.com
 *
 * */

Shader "Custom/Selector/NoCullQuad" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Particle Texture", 2D) = "white" {}
	}

		Category{
			Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off Lighting Off ZWrite Off ZTest Always Fog { Color(0,0,0,0) }

			BindChannels {
				Bind "Color", color
				Bind "Vertex", vertex
				Bind "TexCoord", texcoord
			}

			SubShader {
				Pass {
					SetTexture[_MainTex] {
						constantColor[_Color]
						combine texture* constant, texture * constant
					}
				}
			}
	}
}