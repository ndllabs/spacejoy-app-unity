﻿using UnityEngine;

namespace Selector
{
    public class Purchasable : MonoBehaviour
    {
        // public bool isPurchaseApproved;

        [HideInInspector]
        public string uniqueID;

        public void UpdatePurchase()
        {
            if (string.IsNullOrEmpty(uniqueID))
            {
                Debug.LogError("[" + nameof(Purchasable) + "][" + nameof(UpdatePurchase) + "] Skipping triggering Approve purchase event because selector doesnt have a uniqueID"); return;
            }

            Debug.Log("Updating Purchase");

            SelectorMain.OnClickPurchase?.Invoke(true, uniqueID);
        }

        public void CancelPurchase()
        {
            if (string.IsNullOrEmpty(uniqueID))
            {
                Debug.LogError("[" + nameof(Purchasable) + "][" + nameof(UpdatePurchase) + "] Skipping triggering Approve purchase event because selector doesnt have a uniqueID"); return;
            }

            Debug.Log("Updating Purchase");

            SelectorMain.OnClickPurchase?.Invoke(false, uniqueID);
        }
    }
}
