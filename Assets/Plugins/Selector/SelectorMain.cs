﻿using TMPro;
using UnityEngine;

namespace Selector
{

    public class SelectorMain : MonoBehaviour
    {
        public delegate void ClickPurchase(bool acceptPurchase, string uniqueID);
        public static ClickPurchase OnClickPurchase;

        GameObject high;
        GameObject over;
        GameObject purchase;

        public GameObject prefPurchasable;

        Camera cam;

        string uniqueID = string.Empty;

        public float safezonePercent = 0.1f;
        float m = 0.00035f;

        SelectorMode mode = SelectorMode.Normal;

        private void Update()
        {
            if (mode == SelectorMode.Purchase && cam)
            {
                purchase.transform.LookAt(2 * purchase.transform.position - cam.transform.position);
                purchase.transform.localScale = Vector3.Distance(cam.transform.position, purchase.transform.position) * new Vector3(m, m, m);
            }
        }

        public void MakeLineRenderersSeethrough()
        {
            high.GetComponent<Line>().MakeLinesSeethrough();
        }

        public void Init(SelectorMode selectorMode, string uniqueID, int price = 0)
        {
            cam = Camera.main;

            //UpdateSelectorMode(selectorMode);
            high = transform.Find("Highlight").gameObject;
            over = transform.Find("Overlapper").gameObject;
            // Debug.Log(selectorMode == SelectorMode.Purchase);
            // purchase = transform.Find("Purchasable(Clone)").gameObject;



            over.SetActive(false);
            this.uniqueID = uniqueID;


            high.GetComponent<Line>().safezonePercent = safezonePercent;
            high.GetComponent<Line>().init();
            high.SetActive(false);

            GameObject g = transform.parent.gameObject;
            high.transform.position = g.transform.position;
            high.transform.rotation = g.transform.rotation;
            high.GetComponent<Line>().UpdateBoxHighlighter(g);


            over.transform.position = g.transform.TransformPoint(g.GetComponent<Collider>().bounds.center);
            over.transform.rotation = g.transform.rotation;
            over.transform.localScale = g.GetComponent<BoxCollider>().size * (1 + safezonePercent);

            UpdatePurchasableMode(selectorMode == SelectorMode.Purchase, price);
            mode = selectorMode;

        }


        public void UpdatePurchasableMode(bool isPurchasable, int price = 0)
        {

            if (isPurchasable)
            {
                purchase = Instantiate(prefPurchasable);
                purchase.transform.SetParent(transform);
                purchase.GetComponent<Canvas>().worldCamera = cam.transform.GetChild(0).GetComponent<Camera>();
                purchase.SetActive(true);

                GameObject g = transform.parent.gameObject;
                var pos = transform.parent.position; //g.GetComponent<BoxCollider>().bounds.center;

                purchase.transform.position = new Vector3(pos.x, pos.y + (g.GetComponent<BoxCollider>().bounds.size.y + 0.4f), pos.z);

                
                foreach (var compPurchasable in gameObject.GetComponentsInChildren<Purchasable>(true))
                {
                    compPurchasable.uniqueID = uniqueID;
                }

                purchase.GetComponentInChildren<TextMeshProUGUI>().text =  price.ToString();
                //var mat = over.GetComponent<MeshRenderer>().material;
                //Color temp = mat.color;
                //temp.a = 0.66666f;

                //mat.SetColor("_Color", temp);

            }

            else
            {
                //var mat = over.GetComponent<MeshRenderer>().material;
                //Color temp = mat.color;
                //temp.a = 0.1333f;

                //mat.SetColor("_Color", temp);
                mode = SelectorMode.Normal;
                 if (purchase) Destroy(purchase);
            }
        }

        public void SelectAsset(GameObject g = null)
        {
            high.SetActive(true);

            #region align VisualQues to renderer

            Renderer rend = over.GetComponent<Renderer>();

            Vector3 d1;
            d1.y = 2.0f;
            if ((rend.bounds.size.z - Camera.main.transform.position.x) > (rend.bounds.size.z - Camera.main.transform.position.z))
            {
                d1.x = -0.6f;
                d1.z = 0.6f;
            }
            else
            {
                d1.x = 0.6f;
                d1.z = -0.6f;
            }

            #endregion

            over.SetActive(true);
            over.GetComponent<MeshRenderer>().enabled = true;
        }

        public void CloseAssetSelection()
        {
            if (mode == SelectorMode.Purchase)
            {
                high.SetActive(false);
                over.SetActive(false);
            }
            else
            {

                high.SetActive(false);
                over.SetActive(false);
                if (purchase  && purchase.activeSelf)
                    purchase.SetActive(false);
            }

        }

        public void ToggleLineHighlighter()
        {
            if (!high.activeSelf) high.SetActive(true);
            else high.SetActive(false);
        }

        public void ToggleBoxHighlighter()
        {
            if (!over.activeSelf) over.SetActive(true);
            else over.SetActive(false);
        }


        public void ApprovePurchase()
        {
            if (string.IsNullOrEmpty(uniqueID))
            {
                Debug.LogError("[" + nameof(SelectorMain) + "][" + nameof(ApprovePurchase) + "] Skipping triggering Approve purchase event because selector doesnt have a uniqueID"); return;
            }

            Debug.Log("updated pruchase list in db " + uniqueID);
        }


        public void RejectPurchase()
        {
            if (string.IsNullOrEmpty(uniqueID))
            {
                Debug.LogError("[" + nameof(SelectorMain) + "][" + nameof(ApprovePurchase) + "] Skipping triggering Approve purchase event because selector doesnt have a uniqueID"); return;
            }


            //Destroy(gameObject.transform.parent);
            Debug.Log("updated REject list in db" + uniqueID);
        }


        public enum SelectorMode
        {
            Purchase,
            Normal
        }

    }
}
