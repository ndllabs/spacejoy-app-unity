﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Selector
{

    public class Line : MonoBehaviour
    {

        #region Var And Init

        private Material lineMaterial;
        public Color lineColor1;
        public Color lineColor2;
        Vector3[] linePositions = new Vector3[8];
        Gradient gradient = new Gradient();
        GradientColorKey[] colorKey = new GradientColorKey[2];
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];

        GameObject down, up, line1, line2;
        private LineRenderer dl, ul, l1l, l2l;

        public float safezonePercent = 0.1f;

        public void init()
        {
            down = transform.Find("Down").gameObject;
            up = transform.Find("Up").gameObject;
            line1 = transform.Find("Line1").gameObject;
            line2 = transform.Find("Line2").gameObject;


            SetupLineRendVars();

            dl = down.AddComponent<LineRenderer>();
            ul = up.AddComponent<LineRenderer>();
            l1l = line1.AddComponent<LineRenderer>();
            l2l = line2.AddComponent<LineRenderer>();



            gameObject.SetActive(false);

        }

        public void MakeLinesSeethrough()
        {
            down.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Selector/NoCullQuad");
            down.GetComponent<Renderer>().material.color = lineColor1;

            up.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Selector/NoCullQuad");
            up.GetComponent<Renderer>().material.color = lineColor1;

            line1.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Selector/NoCullQuad");
            line1.GetComponent<Renderer>().material.color = lineColor1;

            line2.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Selector/NoCullQuad");
            line2.GetComponent<Renderer>().material.color = lineColor1;
        }

        #endregion

        #region Public 

        public void UpdateBoxHighlighter(GameObject g)
        {
            BoxCollider box = g.GetComponent<BoxCollider>();

            if (box == null)
            {
                Debug.Log("Selector cannot be applied to non box collider objects currently, skipping");
                return;
            }

            Bounds b = new Bounds(box.center, box.size * (1 + safezonePercent));

            //BoxCollider b = new BoxCollider();
            //b.size = g.GetComponent<BoxCollider>().size * (1 + safezonePercent);


            float thicnkness = 0.5f;
            //get all 8 Corners Right
            Vector3 minExtreme =/* transform.TransformPoint*/(b.center + new Vector3(-b.size.x, -b.size.y, -b.size.z) * thicnkness);
            Vector3 brbCorner = /*transform.TransformPoint*/(b.center + new Vector3(b.size.x, -b.size.y, -b.size.z) * thicnkness);
            Vector3 brfCorner = /*transform.TransformPoint*/(b.center + new Vector3(b.size.x, -b.size.y, b.size.z) * thicnkness);
            Vector3 blfCorner = /*transform.TransformPoint*/(b.center + new Vector3(-b.size.x, -b.size.y, b.size.z) * thicnkness);
            Vector3 tlbCorner = /*transform.TransformPoint*/(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * thicnkness);
            Vector3 trbCorner = /*transform.TransformPoint*/(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * thicnkness);
            Vector3 maxExtreme =/* transform.TransformPoint*/(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * thicnkness);
            Vector3 tlfCorner = /*transform.TransformPoint*/(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * thicnkness);

            dl.useWorldSpace = false; ul.useWorldSpace = false; l1l.useWorldSpace = false; l2l.useWorldSpace = false;

            DrawLineRenderer(dl, new List<Vector3> { minExtreme, brbCorner, brfCorner, blfCorner, minExtreme, tlbCorner });
            DrawLineRenderer(ul, new List<Vector3> { maxExtreme, trbCorner, tlbCorner, tlfCorner, maxExtreme, brfCorner });
            DrawLineRenderer(l1l, new List<Vector3> { brbCorner, trbCorner });
            DrawLineRenderer(l2l, new List<Vector3> { tlfCorner, blfCorner });


            //if (!gameObject.activeSelf) gameObject.SetActive(true);
        }

        public void CloseHighlight()
        {
            gameObject.SetActive(false);
        }


        #endregion

        #region private

        void DrawLineRenderer(LineRenderer lr, List<Vector3> positions)
        {
            lr.startWidth = 0.027f;  // was 0.01 for both
            lr.endWidth = 0.027f;
            lr.material = lineMaterial;
            lr.positionCount = positions.Count;
            lr.SetPositions(positions.ToArray());
            lr.colorGradient = gradient;
        }

        void SetupLineRendVars()
        {
            lineMaterial = new Material(Shader.Find("Sprites/Default"));
            //lineColor1 = HexToColor("#f05a46"); //5C5C5C //1CB5E0
            //lineColor2 = HexToColor("#f05a46");//5C5C5C //000046

            colorKey[0].color = lineColor1;
            colorKey[0].time = 0.0f;
            colorKey[1].color = lineColor2;
            colorKey[1].time = 1.0f;
            alphaKey[0].alpha = 1.0f;
            alphaKey[0].time = 0.0f;
            alphaKey[1].alpha = 1.0f;
            alphaKey[1].time = 1.0f;
            gradient.SetKeys(colorKey, alphaKey);
        }

        static Color HexToColor(string hex)
        {
            hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
            hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
            byte a = 255;//assume fully visible unless specified in hex
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if (hex.Length == 8)
            {
                a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }
        #endregion
    }
}
