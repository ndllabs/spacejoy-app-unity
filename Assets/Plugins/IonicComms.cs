using System.IO;
using UnityEngine;

public class IonicComms : MonoBehaviour
{
    private static IonicComms Instance = null;

    // public delegate void IonicCommMessage();
    // public static IonicCommMessage OnIonicMessage;

    public delegate void DMsgReceive();
    public static DMsgReceive OnMsgReceive;

    public delegate void DMsgLoad();
    public static DMsgLoad OnMsgLoad;

#if !UNITY_EDITOR && UNITY_IOS
	[System.Runtime.InteropServices.DllImport( "__Internal" )]
	private static extern void uHideUnity();

	[System.Runtime.InteropServices.DllImport( "__Internal" )]
	private static extern void uNotifyUnityReady();

	[System.Runtime.InteropServices.DllImport( "__Internal" )]
	private static extern void uSendMessageToIonic( string message );

	[System.Runtime.InteropServices.DllImport( "__Internal" )]
	private static extern void uSendResultToIonic( string result );
#endif

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static void Init()
    {
        if (Instance == null)
        {
            Instance = new GameObject("IonicComms").AddComponent<IonicComms>();
            DontDestroyOnLoad(Instance.gameObject);
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            gameObject.name = "IonicComms";
            DontDestroyOnLoad(gameObject);
        }
        else if (this != Instance)
            Destroy(this);
    }

    void Start()
    {
        if (Instance != this)
            return;

#if !UNITY_EDITOR && UNITY_ANDROID
		using( AndroidJavaClass activityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
		using( AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>( "currentActivity" ) )
		{
			string data = activity.Get<string>( "commStr" );
			OnMessageReceivedFromIonic( data );
		}
#elif !UNITY_EDITOR && UNITY_IOS
		uNotifyUnityReady();
#endif
    }

    void OnApplicationPause(bool pauseStatus)
    {
        Debug.Log("Entered [OnApplicationPause] [IonicComms]" + pauseStatus);

        if (Instance != this)
            return;
        
        if(!pauseStatus)
        {
#if !UNITY_EDITOR && UNITY_ANDROID
		using( AndroidJavaClass activityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
		using( AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>( "currentActivity" ) )
		{
			string data = activity.Get<string>( "commStr" );
			OnMessageReceivedFromIonic( data );
		}
#elif !UNITY_EDITOR && UNITY_IOS
		uNotifyUnityReady();
#endif
        }
    }

    public void OnMessageReceivedFromIonic(string message)
    {
        if (string.IsNullOrEmpty(message) || PlayerPrefs.HasKey("launchUnity"))
            return;

        PlayerPrefs.SetString("launchUnity", message);
        Debug.Log("[launchUnity] " + PlayerPrefs.GetString("launchUnity"));
        // OnIonicMessage?.Invoke();
        OnMsgLoad?.Invoke();
    }

    public void ReceiveMsg(string val)
    {
        PlayerPrefs.SetString("launchUnity", val);
        OnMsgReceive?.Invoke();
    }

    public static void SendMessageToIonic(string message)
    {
        if (string.IsNullOrEmpty(message))
            return;

#if !UNITY_EDITOR && UNITY_ANDROID
		using( AndroidJavaClass activityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
		using( AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>( "currentActivity" ) )
		{
			activity.Call( "sendMessageToIonic", message );
		}
#elif !UNITY_EDITOR && UNITY_IOS
		uSendMessageToIonic( message );
#endif
    }

    public static void FinishActivity(string returnMessage = null)
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        return;
#endif

        if (returnMessage == null)
            returnMessage = "";

        if (Instance == null)
        {
            Application.Quit();
        }
        else
        {
#if UNITY_EDITOR
#elif UNITY_ANDROID
			using( AndroidJavaClass activityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
			using( AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>( "currentActivity" ) )
			{
				try
                {
                    activity.Set<string>( "commStr", returnMessage );
                    
                    // activity.Call( "closeApp" );
                    activity.Call("hideApp");
                }
                catch
                {
                    Debug.LogError("Unable to call ionic activity from unity [FinishActivity]");
                }
			}
#elif UNITY_IOS
			uSendResultToIonic( returnMessage );
			uHideUnity();
#else
			Application.Quit();
#endif
        }
    }

    // public void RaiseOnMsgLoad(Action func) {
    //     if (OnMsgLoad != null) OnMsgLoad += func;
    // }

//     public static void FinishActivity(string returnMessage = null)
//     {
// #if UNITY_EDITOR
//         UnityEditor.EditorApplication.isPlaying = false;
//         return;
// #endif

//         if (returnMessage == null)
//             returnMessage = "";

//         if (Instance == null)
//         {
//             Application.Quit();
//         }
//         else
//         {
// #if UNITY_EDITOR
// #elif UNITY_ANDROID
// 			using( AndroidJavaClass activityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
// 			using( AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>( "currentActivity" ) )
// 			{
//                 try
//                 {
//                     activity.Set<string>( "commStr", returnMessage );
//                     activity.Call( "hideApp" );
//                 }
//                 catch
//                 {
//                     Debug.LogError("Unable to call ionic activity from unity [FinishActivity]");
//                 }
// 			}
// #elif UNITY_IOS
// 			uSendResultToIonic( returnMessage );
// 			uHideUnity();
// #else
// 			Application.Quit();
// #endif
//         }
//     }

    // Called natively from cordova activity
    // Can be used for background tasks such as unity login, logout
    // public void IonicToUnityBroadcastReceiver(string message)
    // {
    //     PlayerPrefs.SetString("launchUnity", message);
    //     Debug.Log("[launchUnity using broadcastreceiver] " + PlayerPrefs.GetString("launchUnity"));
    //     OnIonicMessage?.Invoke();
    // }
}
