﻿/* 
 * WIP GO Serializer
 * 
 * Data Format
 * 
 *  1 Byte Serializer Version Specifier: 
    => Can Hold Int 0 - 255
    => 0 Current Version
    => https://docs.google.com/document/d/14A9V8xSUwe-IfN7JTAMCAwkIGc6LH8wGFj-JVkR7XcA/ 

 *  1 Byte Payload Name Char Array Size
	=> Can be int 0 - 255
 *  “n” number of Chars
	=> Each Char 2 byte, 16 bit, UTF-16
 
 *  1 Byte Payload type Specifier:
	=>  Can Hold Int 0 - 255
	=>  0 for full RawAssetData, 1 for Mesh, 2 for material, 3 for jpeg 
 
 * if 0, Full RAD,
 
 * if 1, Mesh,

 *  1 Byte Mesh Name Char Array Size
	=> Can be int 0 - 255
 *  “n” number of Chars
	=> Each Char 2 byte, 16 bit, UTF-16

*  4 bytes vertex count
*  4 bytes triangle count
*  1 bytes vertex format (bits: 0=vertices, 1=normals, 2=tangents, 3=uvs)

*  After that come vertex component arrays, each optional except for positions.

 * Positions
        Bounding box is before the array (xmin,xmax,ymin,ymax,zmin,zmax)
        Then each vertex component is 2 byte unsigned short, interpolated between the bound axis
    Normals
        One byte per component
    Tangents
        One byte per component
    UVs (8 bytes/vertex - 2 floats)
        Bounding box is before the array (xmin,xmax,ymin,ymax)
        Then each UV component is 2 byte unsigned short, interpolated between the bound axis



 *  1 Byte Child Count:
	=>  Can be int 0 - 255
	=>  How many Child the Current Mesh Has

 * 
 * prajwalshetty2018@gmail.com
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PayloadSerializer
{

    

}
