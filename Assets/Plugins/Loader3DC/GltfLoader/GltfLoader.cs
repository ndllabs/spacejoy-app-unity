﻿/* 
 * Loads the .glb File to Scene
 * Works Both During Editor and Runtime
 * 
 * prajwalshetty2018@gmail.com
 * */

using System.IO;
using UnityEngine;

//using UnityGLTF.Loader;
//using GLTF;
//using GLTF.Schema;
//using UnityGLTF;

//namespace Loader3DC.Gltf
//{
//    public static class GltfLoader
//    {
//        [SerializeField] private static int _maximumLod = 300;

//        public static GameObject CreateGLTFScene(string projectFilePath)
//        {
//            ILoader fileLoader = new FileLoader(Path.GetDirectoryName(projectFilePath));
//            using (var stream = File.OpenRead(projectFilePath))
//            {
//                GLTFRoot gLTFRoot;
//                GLTFParser.ParseJson(stream, out gLTFRoot);
//                var loader = new GLTFSceneImporter(gLTFRoot, fileLoader, null, stream);
//                loader.MaximumLod = _maximumLod;
//                loader.IsMultithreaded = true;

//                loader.LoadSceneAsync().Wait();
//                return loader.LastLoadedScene;
//            }
//        }
//    }
//}
