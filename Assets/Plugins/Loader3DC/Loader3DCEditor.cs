﻿/* 
 * Editor Script, That extended Unity engine's export and import capabalities
 * 
 * prajwalshetty2018@gmail.com
 * */

using System.Threading.Tasks;
using System.Diagnostics;
using UnityEngine;
using dbg = UnityEngine.Debug;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Loader3DC
{
    using Ldr = Loader3DCMain;

    public class Loader3DCEditor : MonoBehaviour
    {

#if UNITY_EDITOR

        #region Import
        [MenuItem("AllFather/Loader3DC/Import/OBJ", true)]
        static bool CheckMenu_ObjLoad()
        {
            return true;
        }
        [MenuItem("AllFather/Loader3DC/Import/OBJ")]
        static async void ObjLoadMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import OBJ", "", "obj");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                await Ldr.ImportObj(pth);
                s.Stop();
                dbg.Log("Action: " + nameof(ObjLoadMenu) + ", Time Elapsed: " + s.Elapsed.TotalSeconds + ".");
            }
        }

        [MenuItem("AllFather/Loader3DC/Import/Materials", true)]
        static bool CheckMenu_MtlLoad()
        {
            return false;
        }
        [MenuItem("AllFather/Loader3DC/Import/Materials")]
        static async void MtlLoadMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import Material", "", "mtl");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                await Task.Delay(10);
                s.Stop();
            }
        }

        [MenuItem("AllFather/Loader3DC/Import/GLTF", true)]
        static bool CheckMenu_GLTFLoad()
        {
            return true;
        }
        [MenuItem("AllFather/Loader3DC/Import/GLTF")]
        static void GLTFLoadMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import GLTF2.0/GLB", "", "glb");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
//                Ldr.ImportGLTF(pth);
                s.Stop();
                dbg.Log("Action: " + nameof(GLTFLoadMenu) + ", Time Elapsed: " + s.Elapsed.TotalSeconds + ".");
            }
        }

        [MenuItem("AllFather/Loader3DC/Import/Bin", true)]
        static bool CheckMenu_BinLoad()
        {
            return true;
        }
        [MenuItem("AllFather/Loader3DC/Import/Bin")]
        static void BinLoadMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import Raw Data", "", "bin");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                Ldr.ImportBin(pth);
                s.Stop();
                dbg.Log("Action: " + nameof(BinLoadMenu) + ", Time Elapsed: " + s.Elapsed.TotalSeconds + ".");
            }
        }
        #endregion

        #region Export
        [MenuItem("AllFather/Loader3DC/Export/OBJ", true)]
        static bool CheckMenu_ObjExport()
        {
            return false;
        }
        [MenuItem("AllFather/Loader3DC/Export/OBJ")]
        static async void ObjExportMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import OBJ", "", "obj");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                await Task.Delay(10);
                s.Stop();
            }
        }

        [MenuItem("AllFather/Loader3DC/Export/Materials", true)]
        static bool CheckMenu_MtlExport()
        {
            return false;
        }
        [MenuItem("AllFather/Loader3DC/Export/Materials")]
        static async void MtlExportMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import Material", "", "mtl");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                await Task.Delay(10);
                s.Stop();
            }
        }

        [MenuItem("AllFather/Loader3DC/Export/GLTF", true)]
        static bool CheckMenu_GLTFExport()
        {
            return false;
        }
        [MenuItem("AllFather/Loader3DC/Export/GLTF")]
        static async void GLTFExportMenu()
        {
            string pth = EditorUtility.OpenFilePanel("Import OBJ", "", "gltf");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                await Task.Delay(10);
                s.Stop();
            }
        }

        [MenuItem("AllFather/Loader3DC/Export/Bin", true)]
        static bool CheckMenu_BinExport()
        {
            return Selection.transforms.Length != 0;
        }
        [MenuItem("AllFather/Loader3DC/Export/Bin")]
        static void BinExportMenu()
        {
            string pth = EditorUtility.OpenFolderPanel("Export Mesh Bin", "", "");
            if (!string.IsNullOrEmpty(pth))
            {
                Stopwatch s = new Stopwatch();
                s.Start();
                foreach (var trans in Selection.transforms)
                {
                    if (trans.GetComponent<MeshFilter>())
                    {
                        Ldr.ExportBin(pth, trans.GetComponent<MeshFilter>().mesh);
                        break;
                    }
                }
                s.Stop();
                dbg.Log("Action: " + nameof(BinExportMenu) + ", Time Elapsed: " + s.Elapsed.TotalSeconds + ".");
            }
        }
        #endregion
#endif
    }
}
