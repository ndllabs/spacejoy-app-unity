﻿/*
 * Loads 3D Content To Scene,
 * Capable Of loading Content Both During Runtime and in Editor
 * 
 * 
 * prajwalshetty2018@gmail.com
 * */

using UnityEngine;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Loader3DC
{
    using Obj;
    using Texture;
//    using Gltf;
    using Bin;
    using System;

    /// <summary>
    /// Loads 3D Content To Scene,
    /// Capable Of loading Content Both During Runtime and in Editor
    /// </summary>
    public static class Loader3DCMain
    {
        public static async Task<GameObject> Load3DContentFrom(DirectoryInfo dir, Vector3 pos = default(Vector3))
        {

            var files = dir.EnumerateFiles("*.*", SearchOption.AllDirectories)
            .Where(s => (s.Extension == ".obj" || s.Extension == ".bin" || s.Extension == ".glb") && !s.FullName.Contains("_MACOSX"));
            
            foreach (var file in files)
            {
                if (file.Extension != ".bin")
                    continue;

                return ImportBin(file.FullName);
            }

            foreach (var file in files)
            {
                if (file.Extension != ".glb")
                    continue;

//                return ImportGLTF(file.FullName);
            }

            foreach (var file in files)
            {
                if (file.Extension != ".obj")
                    continue;
                
                return await ImportObj(file.FullName);
            }

            return null;
        }

        public static async Task<GameObject> ImportObj(string filepath)
        {
            return await ObjLoader.QuickLoadOBJ(filepath, Vector3.zero);
        }

        public static void ExportObj(string filepath)
        {
            throw new NotImplementedException();
        }

        //public static GameObject ImportGLTF(string filepath)
        //{
        //    return GltfLoader.CreateGLTFScene(filepath);
        //}

        public static void ExportGlb(string filepath)
        {
            throw new NotImplementedException();
        }


        public static GameObject ImportBin(string filepath)
        {
            byte[] rawData = File.ReadAllBytes(filepath);

            GameObject go = new GameObject("test");
            MeshFilter mf = go.AddComponent<MeshFilter>();
            MeshRenderer mr = go.AddComponent<MeshRenderer>();

            mf.mesh = MeshSerializer.ReadMesh(rawData);

            return go;
        }

        public static void ExportBin(string filepath, Mesh mesh)
        {
            byte[] rawData = MeshSerializer.WriteMesh(mesh, false);

            File.WriteAllBytes(filepath + Path.DirectorySeparatorChar + "Foo.bin", rawData);
        }

    }
}
