﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FeedbackFactory
{
    public enum FeedbackMode
    {
        Feedback,
        Bug
    }

    public static FeedbackMode feedbackMode = FeedbackMode.Feedback;
}
