﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyFeedback;
using Homefuly;

public class UserCredentialField : FormField
{
    public override void FormClosed()
    {
        // throw new System.NotImplementedException();
    }

    public override void FormOpened()
    {
        // throw new System.NotImplementedException();
    }

    public override void FormSubmitted()
    {
        // add section if it doesn't exist already
        if (!Form.CurrentReport.HasSection(SectionTitle))
            Form.CurrentReport.AddSection(SectionTitle, SortOrder);

        // set section text
        string str = "ID: " + AppComponent.Instance.account.CRED_USERID + ", Project ID: " + AppConfig.PilotDemoConfig.ProjectId;
        Form.CurrentReport[SectionTitle].SetText(str);
        
    }

}
