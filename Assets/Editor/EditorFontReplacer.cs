﻿using TMPro;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


public class EditorFontReplacer : MonoBehaviour
{
    ///<summary>
    /// NOW SAVES CHANGES AS WELL!
    /// </summary>
    [MenuItem("Homefuly/Replace TextMeshProFonts")]
    public static void ReplaceFonts()//GoToDesignScene()
    {
        TextMeshProUGUI[] GetText;
        TMP_FontAsset myFont;
        myFont = Resources.Load("rubik/Rubik_Medium_SDF") as TMP_FontAsset;

        GetText = Resources.FindObjectsOfTypeAll<TextMeshProUGUI>();

        foreach (TextMeshProUGUI go in GetText)
        {
            Debug.Log("Replacing " + go.name);
            go.font = myFont;
            go.material = myFont.material;
        }
        foreach (TextMeshProUGUI ui in GetText)
            EditorUtility.SetDirty(ui.gameObject);



    }

    [MenuItem("Homefuly/Go to Center Scene %\\")]
    public static void GotoCenterScene()
    {

        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        EditorSceneManager.OpenScene("Assets/_PROJECT/Pages/Center/Center.unity");
    }


}