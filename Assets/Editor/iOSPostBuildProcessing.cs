﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR_OSX || UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;
using System.Diagnostics;

public class iOSPostBuildProcessing : MonoBehaviour
{
    private const string entitlements = @"
     <?xml version=""1.0"" encoding=""UTF-8\""?>
     <!DOCTYPE plist PUBLIC ""-//Apple//DTD PLIST 1.0//EN"" ""http://www.apple.com/DTDs/PropertyList-1.0.dtd"">
     <plist version=""1.0"">
         <dict>
             <key>aps-environment</key>
             <string>development</string>
         </dict>
     </plist>";

    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {

#if UNITY_EDITOR_OSX || UNITY_IOS
        UnityEngine.Debug.Log("Post Processing Build");
        if (buildTarget == BuildTarget.iOS)
        {
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));
            string target = proj.TargetGuidByName("Unity-iPhone");
            proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");
            UnityEngine.Debug.Log("Setting Bit code to NO");
            proj.AddCapability(target, PBXCapabilityType.PushNotifications);
            UnityEngine.Debug.Log("Enabling push notifications");
            proj.AddCapability(target, PBXCapabilityType.BackgroundModes);
            UnityEngine.Debug.Log("Enabling background modes");
            File.WriteAllText(projPath, proj.WriteToString());
            UnityEngine.Debug.Log("Writing Project Settings");
            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
            PlistElementDict rootDict = plist.root;
            UnityEngine.Debug.Log("Enabling remote notifications, Camera and photos library usage desicriptions");
            var buildKey = "UIBackgroundModes";
            rootDict.CreateArray(buildKey).AddString("remote-notification");
            var camKey = "NSCameraUsageDescription";
            rootDict.SetString(camKey, "Use the camera to capture room images");
            var libKey = "NSPhotoLibraryUsageDescription";
            rootDict.SetString(libKey, "Access photos library to choose room images");
            string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
            if (rootDict.values.ContainsKey(exitsOnSuspendKey))
            {
                rootDict.values.Remove(exitsOnSuspendKey);
                UnityEngine.Debug.Log("Removing deprecated key");
            }
            File.WriteAllText(plistPath, plist.WriteToString());
            UnityEngine.Debug.Log("Writing To info.plist file");
            var file_name = "beta.entitlements";
            var target_name = PBXProject.GetUnityTargetName();
            var target_guid = proj.TargetGuidByName(target_name);
            var dst = path + "/" + target_name + "/" + file_name;
            try
            {
                File.WriteAllText(dst, entitlements);
                proj.AddFile(target_name + "/" + file_name, file_name);
                proj.AddBuildProperty(target_guid, "CODE_SIGN_ENTITLEMENTS", target_name + "/" + file_name);
                proj.WriteToFile(projPath);
            }
            catch (IOException e)
            {
                UnityEngine.Debug.LogWarning($"Could not copy entitlements. Probably already exists. ({e})");
            }

        }
#endif
    }
}
