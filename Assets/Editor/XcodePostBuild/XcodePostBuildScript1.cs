﻿using UnityEngine;

#if UNITY_EDITOR_OSX || UNITY_IOS
using System.IO;
using UnityEditor.iOS.Xcode;
#endif

using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;

public class XcodePostBuildScript1 : MonoBehaviour
{
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {
#if UNITY_EDITOR_OSX || UNITY_IOS

        if (buildTarget == BuildTarget.iOS)
        {
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));

            string target = proj.TargetGuidByName("Unity-iPhone");

            proj.SetBuildProperty(target, "ENABLE_BITCODE", "false");

            File.WriteAllText(projPath, proj.WriteToString());
        }

#endif
    }
}
