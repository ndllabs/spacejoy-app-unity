﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEditor;
using UnityEditor.SceneManagement;

public class PlayFromApp : MonoBehaviour
{
    static string currentScene = "Assets/_PROJECT/Pages/Designer/Designer.unity";
    static string testScene = "Assets/_PROJECT/Pages/Test/APITest/APITestScene.unity";

    [MenuItem("Homefuly/Play-Stop, But From Prelaunch Scene %[")]
    public static void PlayFromPrelaunchScene()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/_PROJECT/App/AppComponent.unity");
        EditorApplication.isPlaying = true;
    }

    [MenuItem("Homefuly/Go to Design Scene %]")]
    public static void GoToDesignScene()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        //EditorSceneManager.OpenScene("Assets/_PROJECT/Pages/Designer/Designer.unity");
        EditorSceneManager.OpenScene(currentScene);
    }

    [MenuItem("Homefuly/Go to Test Scene %'")]
    public static void GoToTestScene()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        //EditorSceneManager.OpenScene("Assets/_PROJECT/Pages/Designer/Designer.unity");
        EditorSceneManager.OpenScene(testScene);
    }
}
